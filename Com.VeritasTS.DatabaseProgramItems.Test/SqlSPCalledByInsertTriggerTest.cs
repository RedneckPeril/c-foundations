﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Com.VeritasTS.DatabaseUtilities;
using Com.VeritasTS.DatabaseProgramItems;
using Com.VeritasTS.DataTableDef;
using Com.VeritasTS.DatabaseComm;


namespace Com.VeritasTS.DatabaseProgramItems.Test
{
    [TestFixture]
    public class SqlSPCalledByInsertTriggerTest
    {
        Database _db = new Database(new Server(new Vendor { Name = "SQLServer" }))
        { Name = "Mazi_test", ConnectionString = @"Data Source=SERVER02\SQLEXPRESS;Initial Catalog=Mazi_test;Integrated Security=True" };

        [Test]
        public void GetKeysSqlTest()
        {
            IPrimaryKeySequence keySequence = new PrimaryKeySequence
            {
                PrimaryKeyColumn = new Column("Id" , owner: new Nations() ),
                SequenceTable = new Location_SequenceTable(),
            };
            InsertTriggerUtils util = new InsertTriggerUtils("#tempTest");
            string sql = util.GetKeysSql(keySequence);
            System.Diagnostics.Debug.Print(sql);
            Assert.IsTrue(sql.Length > 0);
        }
        [Test]
        public void GetParentPassthroughValueTest()
        {
            testTable2 table = new testTable2 { Database = _db };
            InsertTriggerUtils util = new InsertTriggerUtils("");
            util.GetParentPassthroughValue(table);
            Assert.IsTrue(table.ParentPassthrough.ParentPassthroughValue.Length > 0);

            string sql = "delete from LocationVables where name = 'testTable2'";
            table.Database.SqlExecuteNonQuery(sql);
            table.Database.DisposeDbConnection();
        }
        [Test]
        public void GetFirstOfNextKeysTest()
        {
            testTable2 table = new testTable2 { Database = _db };
            string sql = "select Abbrev, Name into ##tempTest from dbo.nations";
            table.Database.SqlExecuteNonQuery(sql);
            InsertTriggerUtils util = new InsertTriggerUtils("##tempTest");
            sql = util.GetKeysSql(table.PrimaryKeySequence);

            int id = util.GetFirstOfNextKeys(sql, table);
            Assert.IsTrue(id > 0);

            sql = "drop table ##tempTest";
            table.Database.SqlExecuteNonQuery(sql);
            table.Database.DisposeDbConnection();
        }
        [Test]
        public void CheckContraintsTest()
        {
            testTable2 table = new testTable2 { Database = _db };
            string sql = "select Abbrev, Name into ##tempTest from dbo.nations";
            table.Database.SqlExecuteNonQuery(sql);
            InsertTriggerUtils util = new InsertTriggerUtils("##tempTest");
            util.CheckContraints(table);

            sql = "drop table ##tempTest";
            table.Database.SqlExecuteNonQuery(sql);
            table.Database.DisposeDbConnection();
        }

        [Test]
        public void UpdateTempTableIdTest()
        {
            testTable2 table = new testTable2 { Database = _db };
            string sql = "select id, Abbrev, Name into ##tempTest from dbo.nations";
            table.Database.SqlExecuteNonQuery(sql);
            InsertTriggerUtils util = new InsertTriggerUtils("##tempTest");
            util.UpdateTempTableId(1008, table);

            sql = "select min(id) from ##tempTest";
            var ret = table.Database.SqlExecuteReader(sql);
            string value = "";
            if (ret.Count > 0)
                value = ret.First()[0].ToString();
            Assert.IsTrue(value.Equals("1008"));

            sql = "drop table ##tempTest";
            table.Database.SqlExecuteNonQuery(sql);
            table.Database.DisposeDbConnection();
        }
        [Test]
        public void InsertIntoParentTest()
        {
            testTable2 table = new testTable2 { Database = _db };
            string sql = "delete from testTable";
            table.Database.SqlExecuteNonQuery(sql);
            sql = "select * into ##tempTest from dbo.nations";
            table.Database.SqlExecuteNonQuery(sql);
            InsertTriggerUtils util = new InsertTriggerUtils("##tempTest");
            util.UpdateTempTableId(22, table); // give a number not in table Locations
            table.ParentPassthrough.ParentPassthroughValue = "8"; // a valid nationid
            util.InsertIntoParent(table);

            //sql = "select min(id) from testTable";
            //var ret = table.Database.SqlExecuteReader(sql);
            //string value = "";
            //if (ret.Count > 0)
            //    value = ret.First()[0].ToString();
            //Assert.IsTrue(value.Equals("22"));

            sql = "drop table ##tempTest";
            table.Database.SqlExecuteNonQuery(sql);
            table.Database.DisposeDbConnection();
        }
        [Test]
        public void GetTableTest()
        {
            ITable t = new aTable();
            t = null;
            string tableName = "testTable2";
            Type type = CommUtils.GetTypeByName(tableName);
            ITable table = (ITable)Activator.CreateInstance(type);
            Assert.IsTrue(table != null);
        }
    }

    //class testTable2 : Table
    //{
    //    public testTable2() : base()
    //    {
    //        Name = "testTable2";
    //        Schema = new Schema { Name = "dbo" };
    //        LoadColumn(new Column { Name = "Id", DataType = "int", IsNullable = false, Owner = this });
    //        LoadColumn(new Column { Name = "Abbrev", DataType = "nvarchar(50)", IsNullable = false, Owner = this });
    //        LoadColumn(new Column { Name = "Name", DataType = "nvarchar(100)", IsNullable = false, Owner = this });
    //        LoadColumn(new Column { Name = "NationId", DataType = "int", IsNullable = false, Owner = this });
    //        LoadColumn(new Column { Name = "LastUpdateDtm", DataType = "datetime", IsNullable = false, Owner = this });
    //        LoadColumn(new Column { Name = "LastUpdater", DataType = "nvarchar(100)", IsNullable = false, Owner = this });

    //        PrimaryKey = new PrimaryKey { Name = "PK_testTable2", ColumnNames = new List<string> { "id" }, Owner = this };
    //        ParentTable = new testTable();
    //        PrimaryKeySequence = new PrimaryKeySequence
    //        {
    //            PrimaryKeyColumn = new Column { Name = "Id" },
    //            SequenceTable = new Table_1(),
    //        };
    //        ParentPassthrough = new ParentPassthroughInfo
    //        {
    //            ParentPassthroughColumn = new Column { Name = "NationId", DataType = "int"},
    //            ParentPassthroughSourceTable = new LocationVables(),
    //            ParentPassthroughSourceColumn = new Column { Name = "Id", DataType = "int" },
    //            ParentPassthroughMatchingSourceColumn = new Column { Name = "Name", DataType = "nvarchar(100)" },
    //            ParentPassthroughLookup = "testTable2"
    //        };
    //        LoadForeignKey(new ForeignKey { Name = "FK_testTable2_PoliticalLocations", Owner = this, KeyColumn = new Column { Name = "Id" }, ReferencedTable = new PoliticalLocations(), ReferenceColumn = new Column { Name = "Id" } });
    //        LoadForeignKey(new ForeignKey { Name = "FK_testTable2_Nations", Owner = this, KeyColumn = new Column { Name = "NationId" }, ReferencedTable = new Nations(), ReferenceColumn = new Column { Name = "Id" } , IsCascadeDelete = true});
    //        LoadIndex(new Index { Name = "idx_testTable2", ColumnNames = new List<string> { "Abbrev", "NationId" }, Owner = this, IsUnique = true, IsClustered = false });
    //        LoadCheckConstraint(new CheckConstraint { Name = "ck_testTable2", Owner = this, ConstraintText = "len(Abbrev) > 0 and len(Name) > 0" });
    //        LoadTrigger(new Trigger { Owner = this, BeforeInsert = true, Procedure = new StoredProcedure { Name = "InsertLocations", Schema = new Schema { Name = "dbo" }, Owner = this } });
    //        LoadTrigger(new Trigger { Owner = this, BeforeDelete = true, Procedure = new StoredProcedure { Name = "SPCalledByDeleteTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
    //    }
    //}


}
