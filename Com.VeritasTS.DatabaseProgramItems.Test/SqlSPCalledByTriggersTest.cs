﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Com.VeritasTS.DatabaseUtilityInterfaces;
using Com.VeritasTS.DataTableDef;

using Com.VeritasTS.TextAndErrorUtils;
using Com.VeritasTS.DbUtilitiesSqlServer;


namespace Com.VeritasTS.DatabaseProgramItems.Test
{
    [TestFixture]
    class SqlSPCalledByTriggersTest
    {
        IDatabaseSafe _db;
        ISqlTranslator _sqlTranslator;
        public SqlSPCalledByTriggersTest()
        {
            _sqlTranslator = new sqlTranslatorSqlServer2014();
            _db = new DatabaseMutable(
                "Mazi_test"
                , new ServerImmutable(
                    "SERVER02"
                    , Vendors.SQLServer()
                    )
                , new ConnectionFactorSqlServer(@"Data Source=SERVER02\SQLEXPRESS;Initial Catalog=Mazi_test;Integrated Security=True")
                , _sqlTranslator
                );
        }

        [Test]
        public void CascadeDeleteTest()
        {
            string tempTable = "##testTable_d";
            testTable2 table = new testTable2 { Database = _db };
            string sql = "select id into " + tempTable + " from testTable2 where id = 1";
            table.Database.SqlExecuteNonQuery(sql);
            sql = "disable trigger testTable3_ioD on testTable3";
            table.Database.SqlExecuteNonQuery(sql);

            TriggerUtils util = new TriggerUtils();
            util.CascadeDelete(table, tempTable);

            sql = "select count(*) cnt from testTable3";
            var ret = table.Database.SqlExecuteReader(sql);
            string value = "";
            if (ret.Count > 0)
                value = ret.First()[0].ToString();
            Assert.IsTrue(value.Equals("0"));

            sql = "drop table " + tempTable;
            table.Database.SqlExecuteNonQuery(sql);
            sql = "enable trigger testTable3_ioD on testTable3";
            table.Database.SqlExecuteNonQuery(sql);

            table.Database.DisposeDbConnection();

        }
        [Test]
        public void CheckImmutableChangesTest()
        {
            string tempTableI = "##testTable_i";
            string tempTableD = "##testTable_d";
            testTable2 table = new testTable2 { Database = _db };
            string sql = "select *  into " + tempTableI + " from nations where abbrev in ('USA','CAN')";
            table.Database.SqlExecuteNonQuery(sql);
            sql = "select * into " + tempTableD + " from nations where abbrev in ('USA','CAN')";
            table.Database.SqlExecuteNonQuery(sql);

            TriggerUtils util = new TriggerUtils();
            var b = util.IsImmutableChanged(table, tempTableI, tempTableD);
            Assert.IsFalse(b);

            sql = "update " + tempTableI + " set abbrev = 'CA' where abbrev = 'CAN'";
            table.Database.SqlExecuteNonQuery(sql);
            b = util.IsImmutableChanged(table, tempTableI, tempTableD);
            Assert.IsTrue(b);

            sql = "drop table " + tempTableI;
            table.Database.SqlExecuteNonQuery(sql);
            sql = "drop table " + tempTableD;
            table.Database.SqlExecuteNonQuery(sql);

            table.Database.DisposeDbConnection();
        }
        [Test]
        public void AfterAnythingInsertAuditTableSqlTest()
        {
            testTable table = new testTable { Database = _db};
            TriggerUtils util = new TriggerUtils();
            string sql = util.AfterAnythingInsertAuditTableSql(table, "#tempTest_i", "");
            Assert.IsFalse(sql.Contains("Old"));
            sql = util.AfterAnythingInsertAuditTableSql(table, "", "#tempTest_d");
            Assert.IsFalse(sql.Contains("New"));
            sql = util.AfterAnythingInsertAuditTableSql(table, "tempTest_i", "#tempTest_d");
            Assert.IsTrue(sql.Contains("New") && sql.Contains("Old"));
        }

        //[Test]
        //public void MakeQueueJobsTest()
        //{
        //    Table table = new testQueueTable { Database = _db };
        //    string sql = "select 10 id, 'testAbbrev' Abbrev,'testName' Name,'100' NationId, getdate() LastUpdateDtm, 'tester' LastUpdater into ##tempTestTable";
        //    table.Database.SqlExecuteNonQuery(sql);
        //    TriggerUtils util = new TriggerUtils();
        //    util.MakeQueueJobs(table, "##tempTestTable", "");
        //    sql = "select count(*) cnt from " + table.Queue.JobsTable.Name + " where testQueueid = 10";
        //    var ret = table.Database.SqlExecuteReader(sql);
        //    int n = Convert.ToInt32(ret.First()[0]);
        //    Assert.True(n > 0);
        //    sql = "delete from " + table.Queue.JobsTable.Name + " where testQueueid = 10";
        //    table.Database.SqlExecuteNonQuery(sql);
        //    table.Database.DisposeDbConnection();
        //}
        [Test]
        public void MakeQueueJobsSqlTest()
        {
            Table table = new testQueueTable { Database = _db };
            TriggerUtils util = new TriggerUtils();
            string sql = util.MakeQueueJobsSql(table, "#tempTestTable", "");
            Assert.True(sql.Length > 0);
            sql = util.MakeQueueJobsSql(table, "#temp_i", "#temp_d");
            Assert.True(sql.Length > 0);
            sql = util.MakeQueueJobsSql(table, "", "#temp_d");
            Assert.True(sql.Length > 0);
        }

        [Test]
        public void SPCalledByAfterIudTriggerTest()
        {
            StoredProcedures.SPCalledByAfterIudTrigger("tempIns", "", "testQueueTable");
        }

    }


}
