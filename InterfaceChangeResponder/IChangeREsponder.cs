﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceChangeResponder
{
    public interface IChangeResponder
    {
        // jobNum: value of JobNum column in the jobs table for the batch
        // qiId :  value of Owner column in the jobs table for the batch
        void RespondToChange(int jobNum, int qiId, object TriggerQueue = null);
    }
}
