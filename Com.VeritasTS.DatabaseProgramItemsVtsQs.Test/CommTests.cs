﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using NUnit.Framework;


namespace Com.VeritasTS.DatabaseProgramItemsVtsQs.Test
{
    [TestFixture]

    public class CommTests
    {
        string connStr = @"Data Source=SERVER02\SQLEXPRESS;Initial Catalog=Mazi_test;Integrated Security=True";

        [Test]
        public void GetQueueInfoTest()
        {
            using (SqlConnection conn = new SqlConnection { ConnectionString = connStr })
            {
                conn.Open();
                string sql = "select qisVableName from vtsqs.queues where id = 1";
                string table = StoredProcedures.GetQueueInfo<string>(conn, sql);
                Assert.IsTrue(table.Length > 0);
                sql = "select logName from vtsqs.queues where id = 1";
                table = StoredProcedures.GetQueueInfo<string>(conn, sql);
                Assert.IsTrue(table.Length > 0);
                sql = "select maxRowsInLog from vtsqs.queues where id = 1";
                int n = StoredProcedures.GetQueueInfo<int>(conn, sql);
                Assert.IsTrue(n > 0);
            }
        }
        [Test]
        public void QiPingTest()
        {
            //StoredProcedures.QiPing(1, "", 1);
        }
        [Test]
        public void LogFromQueueTest()
        {
            //StoredProcedures.LogFromQueue(1, 1, "test message", queueID: 1);
        }
        [Test]
        public void killQITest()
        {
            //StoredProcedures.killQI(1, queueID: 1);
        }
    }
}
