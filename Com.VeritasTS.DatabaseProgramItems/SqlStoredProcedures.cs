//using System;
//using System.Data;
//using System.Data.SqlClient;
//using System.Data.SqlTypes;
//using Microsoft.SqlServer.Server;
//using System.Collections.Generic;
//using System.Reflection;
//using System.Linq;
//using Com.VeritasTS.DataTableDef;
//using Com.VeritasTS.DatabaseUtilityInterfaces;
//using Com.VeritasTS.DbUtilitiesSqlServer;


//public partial class StoredProcedures
//{
////    [Microsoft.SqlServer.Server.SqlProcedure]
////    public static void SPCalledByInsertTrigger(
////        string schemaName
////        , string tableObjectName
////        )
////    {
////#if !DEBUG
////        SqlPipe sp = SqlContext.Pipe;
////        sp.Send("Insert " + tableName + ", temptable = " + tempTable);
////#endif
////        using (SqlConnection rawConn = new SqlConnection("context connection=true"))
////        {
////            rawConn.Open();
////            if (
////                new SqlCommand("select top(1) 1 from inserted", rawConn)
////                .ExecuteReader()
////                .HasRows
////                )
////            {
////                ITable table = (ITable)CommUtils.GetTableByTableName(tableObjectName);
////                if (table == null)
////                {
////#if !DEBUG
////            sp.Send("CreateInstance: can not create the table object");
////#endif
////                    throw (new Exception("can not create the table object"));
////                }
////                ISqlTranslator sqlTranslator = new SqlTranslatorSqlServer2014();
////                IConnection conn = new ConnectionSqlServer( rawConn );
////                IDatabaseSafe db
////                    = new DatabaseSafeMutable(
////                        conn.DatabaseName()
////                        , null
////                        , null
////                        , sqlTranslator
////                        );
////                ISchema schema
////                    = new SchemaMutable(
////                        schemaName
////                        , sqlTranslator
////                        , db
////                        , retrieveIDsFromDatabase: false
////                        , assert: false
////                        );
////                table.SetParentObject(schema);  // this also sets the database and the SQL translator
////                                                //table.Database = new Database(new Server(new Vendor { Name = "SQLServer" }));
////                table.ConstructAfterConstruction();
////                table.CompleteConstructionIfType(
////                    table.GetType()
////                    , false
////                    , false
////                    );
////#if !DEBUG
////            sp.Send("database opened for " + table.Name + "  db: " + table.FullName);
////#endif
////                table.PerformBeforeDeleteTriggerLogicWithInsertedTable( conn );
////            }
////        }
////            IPrimaryKeySequence keySequence = table.PrimaryKeySequence;
////        }

////            if (tempTable.Length > 0)
////            {
////                util.CheckConstraints(table);
////                if (keySequence != null)
////                {
////                    int id = util.GetFirstOfNextKeys(util.GetKeysSql(keySequence), table);
////#if !DEBUG
////                    sp.Send("GetFirstOfNextKeys = " + id.ToString());
////#endif
////                    util.UpdateTempTableId(id, table);
////#if !DEBUG
////                    sp.Send("after UpdateTempTableId " );
////#endif
////                }

////                util.GetParentPassthroughValue(table);
////                util.InsertIntoParent(table);
////#if !DEBUG
////                sp.Send("after InsertIntoParent ");
////#endif
////            }
////        }
//    }
//    [Microsoft.SqlServer.Server.SqlProcedure]
//    public static void SPCalledByDeleteTrigger(string tempTable, string tableName)
//    {
//#if !DEBUG
//        SqlPipe sp = SqlContext.Pipe;
//        sp.Send("Delete from " + tableName + ", temptable = " + tempTable);
//#endif
//        ITable t = new aTable();
//        t = null;
//        ITable table = (ITable)CommUtils.GetTableByTableName(tableName);
//        if (table == null)
//        {
//#if !DEBUG
//            sp.Send("CreateInstance: can not create the table object");
//#endif
//            throw (new Exception("can not create the table object"));
//        }

//        table.Database = new Database(new Server(new Vendor { Name = "SQLServer" }));
//        using (SqlConnection conn = new SqlConnection("context connection=true"))
//        {
//            conn.Open();
//            table.Database.Connection = conn;
//#if !DEBUG
//            sp.Send("database openned for " + table.Name + "  db: " + table.Database.Server.Vendor.Name);
//#endif
//            Com.VeritasTS.DatabaseProgramItems.TriggerUtils util = new Com.VeritasTS.DatabaseProgramItems.TriggerUtils();
//            util.CascadeDelete(table, tempTable);
//#if !DEBUG
//            sp.Send("end of SPCalledByDeleteTrigger");
//#endif
//        }

//    }

//    [Microsoft.SqlServer.Server.SqlProcedure]
//    public static void SPCalledByUpdateTrigger(string tempTableIns, string tempTableDel, string tableName)
//    {
//#if !DEBUG
//        SqlPipe sp = SqlContext.Pipe;
//        sp.Send("Update " + tableName + ", temptableIns = " + tempTableIns + ", tempTableDel = " + tempTableDel);
//#endif
//        ITable t = new aTable();
//        t = null;
//        ITable table = (ITable)CommUtils.GetTableByTableName(tableName);
//        if (table == null)
//        {
//#if !DEBUG
//            sp.Send("CreateInstance: can not create the table object");
//#endif
//            throw (new Exception("can not create the table object"));
//        }

//        table.Database = new Database(new Server(new Vendor { Name = "SQLServer" }));
//        using (SqlConnection conn = new SqlConnection("context connection=true"))
//        {
//            conn.Open();
//            table.Database.Connection = conn;
//#if !DEBUG
//            sp.Send("database openned for " + table.Name + "  db: " + table.Database.Server.Vendor.Name);
//#endif
//            Com.VeritasTS.DatabaseProgramItems.TriggerUtils util = new Com.VeritasTS.DatabaseProgramItems.TriggerUtils();
//            if (util.IsImmutableChanged(table, tempTableIns, tempTableDel))
//                throw (new Exception("Immutable column value has been changed"));
//#if !DEBUG
//            sp.Send("end of SPCalledByUpdateTrigger");
//#endif
//        }
//    }

//    [Microsoft.SqlServer.Server.SqlProcedure]
//    public static void SPCalledByAfterIudTrigger(string tempTableIns, string tempTableDel, string tableName)
//    {
//#if !DEBUG
//        SqlPipe sp = SqlContext.Pipe;
//        sp.Send("After IUD of " + tableName + ", temptableIns = " + tempTableIns + ", tempTableDel = " + tempTableDel);
//#endif
//        if (tempTableIns.Length == 0 && tempTableDel.Length == 0)
//            return;
//        ITable t = new aTable();
//        t = null;
//        ITable table = (ITable)CommUtils.GetTableByTableName(tableName);
//        if (table == null)
//        {
//#if !DEBUG
//            sp.Send("CreateInstance: can not create the table object");
//#endif
//            throw (new Exception("can not create the table object"));
//        }

//        table.Database = new Database(new Server(new Vendor { Name = "SQLServer" }));
//        using (SqlConnection conn = new SqlConnection("context connection=true"))
//        {
//            conn.Open();
//            table.Database.Connection = conn;
//#if !DEBUG
//            sp.Send("database openned for " + table.Name + "  db: " + table.Database.Server.Vendor.Name);
//#endif
//            Com.VeritasTS.DatabaseProgramItems.TriggerUtils util = new Com.VeritasTS.DatabaseProgramItems.TriggerUtils();

//            string sql = util.AfterAnythingInsertAuditTableSql(table, tempTableIns, tempTableDel);
//            if (sql.Length > 0) table.Database.SqlExecuteNonQuery(sql);

//            sql = util.MakeQueueJobsSql(table, tempTableIns, tempTableDel);
//            if (sql.Length > 0) table.Database.SqlExecuteNonQuery(sql);

//#if !DEBUG
//            sp.Send("end of SPCalledByAfterIudTrigger");
//#endif
//        }
//    }

//    [Microsoft.SqlServer.Server.SqlProcedure]
//    public static void DropTempTableIfExists(string tableName)
//    {
//        using (SqlConnection conn = new SqlConnection("context connection=true"))
//        {
//            conn.Open();
//            string sql = "if exists (select * from tempdb.sys.objects o where o.name = '" + tableName + "' )";
//            sql += "drop table " + tableName + ";";
//            ExecNonQuery(conn, sql);
//        }
//    }
//    private static void ExecNonQuery(SqlConnection conn, string sql)
//    {
//        using (SqlCommand cmd = new SqlCommand())
//        {
//            cmd.CommandText = sql;
//            cmd.Connection = conn;
//            cmd.ExecuteNonQuery();
//        }
//    }
//}

//*/

////public partial class StoredProcedures_Deprecated
////{
////    [Microsoft.SqlServer.Server.SqlProcedure]
////    public static void SPCalledByInsertTrigger (string tempTable, string tableObjectName)
////    {
////#if !DEBUG
////        SqlPipe sp = SqlContext.Pipe;
////        sp.Send("Insert " + tableName + ", temptable = " + tempTable);
////#endif
////        ITable t = new aTable();
////        t = null;
////        Com.VeritasTS.DatabaseProgramItems.InsertTriggerUtils util = new Com.VeritasTS.DatabaseProgramItems.InsertTriggerUtils(tempTable);
////        ITable table = (ITable)CommUtils.GetTableByTableName(tableObjectName);
////        if (table == null)
////        {
////#if !DEBUG
////            sp.Send("CreateInstance: can not create the table object");
////#endif
////            throw (new Exception("can not create the table object"));
////        }
////        table.Database = new Database(new Server(new Vendor { Name = "SQLServer" })) ;
////        IPrimaryKeySequence keySequence = table.PrimaryKeySequence;

////        using (SqlConnection conn = new SqlConnection("context connection=true"))
////        //using (SqlConnection conn = new SqlConnection(@"Data Source=SERVER02\SQLEXPRESS;Initial Catalog=Mazi_test;Integrated Security=True"))
////        {
////            conn.Open();
////            table.Database.Connection = conn;
////#if !DEBUG
////            sp.Send("database openned for " + table.Name + "  db: " + table.Database.Server.Vendor.Name);
////#endif
////            if (tempTable.Length > 0)
////            {
////                util.CheckConstraints(table);
////                if (keySequence != null)
////                {
////                    int id = util.GetFirstOfNextKeys(util.GetKeysSql(keySequence), table);
////#if !DEBUG
////                    sp.Send("GetFirstOfNextKeys = " + id.ToString());
////#endif
////                    util.UpdateTempTableId(id, table);
////#if !DEBUG
////                    sp.Send("after UpdateTempTableId " );
////#endif
////                }

////                util.GetParentPassthroughValue(table);
////                util.InsertIntoParent(table);
////#if !DEBUG
////                sp.Send("after InsertIntoParent ");
////#endif
////            }
////        }
////    }
////    [Microsoft.SqlServer.Server.SqlProcedure]
////    public static void SPCalledByDeleteTrigger(string tempTable, string tableName)
////    {
////#if !DEBUG
////        SqlPipe sp = SqlContext.Pipe;
////        sp.Send("Delete from " + tableName + ", temptable = " + tempTable);
////#endif
////        ITable t = new aTable();
////        t = null;
////        ITable table = (ITable)CommUtils.GetTableByTableName(tableName);
////        if (table == null)
////        {
////#if !DEBUG
////            sp.Send("CreateInstance: can not create the table object");
////#endif
////            throw (new Exception("can not create the table object"));
////        }

////        table.Database = new Database(new Server(new Vendor { Name = "SQLServer" }));
////        using (SqlConnection conn = new SqlConnection("context connection=true"))
////        {
////            conn.Open();
////            table.Database.Connection = conn;
////#if !DEBUG
////            sp.Send("database openned for " + table.Name + "  db: " + table.Database.Server.Vendor.Name);
////#endif
////            Com.VeritasTS.DatabaseProgramItems.TriggerUtils util = new Com.VeritasTS.DatabaseProgramItems.TriggerUtils();
////            util.CascadeDelete(table, tempTable);
////#if !DEBUG
////            sp.Send("end of SPCalledByDeleteTrigger");
////#endif
////        }

////    }

////    [Microsoft.SqlServer.Server.SqlProcedure]
////    public static void SPCalledByUpdateTrigger(string tempTableIns, string tempTableDel, string tableName)
////    {
////#if !DEBUG
////        SqlPipe sp = SqlContext.Pipe;
////        sp.Send("Update " + tableName + ", temptableIns = " + tempTableIns + ", tempTableDel = " + tempTableDel);
////#endif
////        ITable t = new aTable();
////        t = null;
////        ITable table = (ITable)CommUtils.GetTableByTableName(tableName);
////        if (table == null)
////        {
////#if !DEBUG
////            sp.Send("CreateInstance: can not create the table object");
////#endif
////            throw (new Exception("can not create the table object"));
////        }

////        table.Database = new Database(new Server(new Vendor { Name = "SQLServer" }));
////        using (SqlConnection conn = new SqlConnection("context connection=true"))
////        {
////            conn.Open();
////            table.Database.Connection = conn;
////#if !DEBUG
////            sp.Send("database openned for " + table.Name + "  db: " + table.Database.Server.Vendor.Name);
////#endif
////            Com.VeritasTS.DatabaseProgramItems.TriggerUtils util = new Com.VeritasTS.DatabaseProgramItems.TriggerUtils();
////            if (util.IsImmutableChanged(table, tempTableIns, tempTableDel))
////                throw (new Exception("Immutable column value has been changed"));
////#if !DEBUG
////            sp.Send("end of SPCalledByUpdateTrigger");
////#endif
////        }
////    }

////    [Microsoft.SqlServer.Server.SqlProcedure]
////    public static void SPCalledByAfterIudTrigger(string tempTableIns, string tempTableDel, string tableName)
////    {
////#if !DEBUG
////        SqlPipe sp = SqlContext.Pipe;
////        sp.Send("After IUD of " + tableName + ", temptableIns = " + tempTableIns + ", tempTableDel = " + tempTableDel);
////#endif
////        if (tempTableIns.Length == 0 && tempTableDel.Length == 0)
////            return;
////        ITable t = new aTable();
////        t = null;
////        ITable table = (ITable)CommUtils.GetTableByTableName(tableName);
////        if (table == null)
////        {
////#if !DEBUG
////            sp.Send("CreateInstance: can not create the table object");
////#endif
////            throw (new Exception("can not create the table object"));
////        }

////        table.Database = new Database(new Server(new Vendor { Name = "SQLServer" }));
////        using (SqlConnection conn = new SqlConnection("context connection=true"))
////        {
////            conn.Open();
////            table.Database.Connection = conn;
////#if !DEBUG
////            sp.Send("database openned for " + table.Name + "  db: " + table.Database.Server.Vendor.Name);
////#endif
////            Com.VeritasTS.DatabaseProgramItems.TriggerUtils util = new Com.VeritasTS.DatabaseProgramItems.TriggerUtils();

////            string sql = util.AfterAnythingInsertAuditTableSql(table, tempTableIns, tempTableDel);
////            if (sql.Length > 0) table.Database.SqlExecuteNonQuery(sql);

////            sql = util.MakeQueueJobsSql(table, tempTableIns, tempTableDel);
////            if (sql.Length > 0) table.Database.SqlExecuteNonQuery(sql);

////#if !DEBUG
////            sp.Send("end of SPCalledByAfterIudTrigger");
////#endif
////        }
////    }

////    [Microsoft.SqlServer.Server.SqlProcedure]
////    public static void DropTempTableIfExists(string tableName)
////    {
////        using (SqlConnection conn = new SqlConnection("context connection=true"))
////        {
////            conn.Open();
////            string sql = "if exists (select * from tempdb.sys.objects o where o.name = '" + tableName + "' )";
////            sql += "drop table " + tableName + ";";
////            ExecNonQuery(conn, sql);
////        }
////    }
////    private static void ExecNonQuery(SqlConnection conn, string sql)
////    {
////        using (SqlCommand cmd = new SqlCommand())
////        {
////            cmd.CommandText = sql;
////            cmd.Connection = conn;
////            cmd.ExecuteNonQuery();
////        }
////    }
////}
