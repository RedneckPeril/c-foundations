using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using Com.VeritasTS.DatabaseUtilityInterfaces;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

using Com.VeritasTS.TextAndErrorUtils;


namespace Com.VeritasTS.DatabaseProgramItems
{
//    public class InsertTriggerUtils
//    {
//        private string _tempTable;
//        public InsertTriggerUtils(string tempTable)
//        {
//            _tempTable = tempTable;
//        }

//        public int GetFirstOfNextKeys(string keysSql, ITable table)
//        {
//#if !DEBUG
//            SqlPipe sp = SqlContext.Pipe;
//            sp.Send("GetFirstOfNextKeys.keysSql = " + keysSql);
//#endif
//            int minId = 0;
//            IDatabaseSafe db = table.Database;
//            IConnection conn = db.GetAConnection();
//            var trans = db.BeginTransaction(conn);
//            db.SqlExecuteNonQuery(keysSql, conn, trans);

//            string sql = "select min(" + table.PrimaryKeySequenceMap.Sequence.SequenceTableIdentityColumn.Name + ") id from "
//                + table.PrimaryKeySequenceMap.Sequence.Schema.Name + "." + table.PrimaryKeySequenceMap.Sequence.Name + ";";
//#if !DEBUG
//            sp.Send("GetFirstOfNextKeys.sql = " + sql);
//#endif
//            minId = Convert.ToInt32(db.SqlExecuteSingleValue(sql, conn, trans));

//            sql = "delete from " + table.PrimaryKeySequenceMap.Sequence.Schema.Name + "." + table.PrimaryKeySequenceMap.Sequence.Name;
//            db.SqlExecuteNonQuery(sql, conn, trans);
//            trans.Commit();

//            return minId;
//        }

//        public void UpdateTempTableId(int minId, ITable table)
//        {
//#if !DEBUG
//            SqlPipe sp = SqlContext.Pipe;
//            sp.Send("Enter UpdateTempTableId");
//#endif
//            string primaryKeyColumnName = table.PrimaryKeySequenceMap.PrimaryKeyColumn.Name;
//            string sql = "update t set t." + primaryKeyColumnName + " = n+ " + minId.ToString() + " from (select *, row_number() over (order by (select 1)) - 1 n from " + _tempTable + ") t ";

//#if !DEBUG
//            sp.Send("UpdateTempTableId.sql = " + sql);
//#endif
//            table.Database.SqlExecuteNonQuery(sql);
//        }

//        public string GetKeysSql(IPrimaryKeySequenceMap keySequence)
//        {
//            string sql = "insert into " + keySequence.Sequence.Schema.Name + "." + keySequence.Sequence.Name
//                       + " (" + keySequence.Sequence.SequenceTableInsertColumn.Name + ") select 0 from " + _tempTable + "; ";
//            return sql;
//        }

//        public void InsertIntoParent(ITable table)
//        {
//#if !DEBUG
//            SqlPipe sp = SqlContext.Pipe;
//            sp.Send("Enter InsertIntoParent");
//#endif
//            ITable parent = table.ParentTable;
//            if (parent == null)
//                return;

//            IParentPassThroughInfo passThrough = table.ParentPassThrough;
//            string columnString = parent.InsertColumnListSql();
//            string[] columns = columnString.Split(',');
//            string passThroughColumnName = "!";

//            if (passThrough != null)
//                passThroughColumnName = passThrough.ParentPassthroughColumn.Name;

//            string sql1 = "insert into " + parent.Schema.Name + "." + parent.Name + " (";
//            string sql2 = " select ";
//            foreach (var col in columns)
//            {
//                sql1 += col + ",";
//                if (!col.Equals(passThroughColumnName))
//                    sql2 += col + ",";
//                else
//                    sql2 += passThrough.ParentPassthroughValue.ToString() + ",";
//            }
//            sql1 = sql1.Substring(0, sql1.Length - 1);
//            sql2 = sql2.Substring(0, sql2.Length - 1);

//            string sql = sql1 + ")" + sql2 + " from " + _tempTable;
//#if !DEBUG
//            sp.Send("InsertIntoParent.sql = " + sql);
//#endif
//            table.Database.SqlExecuteNonQuery(sql);
//        }

//        public void GetParentPassthroughValue(ITable table)
//        {
//#if !DEBUG
//            SqlPipe sp = SqlContext.Pipe;
//            sp.Send("Enter GetParentPassthroughValue");
//#endif
//            IParentPassThroughInfo parentPassThrough = table.ParentPassThrough;
//            if (parentPassThrough == null)
//                return;

//            IDatabase db = table.Database;
//            object lookup = parentPassThrough.ParentPassthroughLookup;

//            string columnDataType = parentPassThrough.ParentPassthroughMatchingSourceColumn.DataType;
//            if (columnDataType.Contains("varchar"))
//                lookup = "'" + lookup.ToString() + "'";


//            string sql = "select " + parentPassThrough.ParentPassthroughSourceColumn.Name + " from "
//                + parentPassThrough.ParentPassthroughSourceTable.Schema.Name + "." + parentPassThrough.ParentPassthroughSourceTable.Name
//                + " where " + parentPassThrough.ParentPassthroughMatchingSourceColumn.Name + " = " + lookup;
//#if !DEBUG
//            sp.Send("GetParentPassthroughValue.sql = " + sql);
//#endif
//            string value = "";
//            var ret = db.SqlExecuteSingleValue(sql);
//            if (ret != null)
//                value = ret.ToString();

//            if (value.Length == 0)
//            {
//                sql = "declare @p table(col1 " + parentPassThrough.ParentPassthroughColumn.DataType + "); ";
//                sql += "insert into " + parentPassThrough.ParentPassthroughSourceTable.Schema.Name + "." + parentPassThrough.ParentPassthroughSourceTable.Name 
//                    + " (" + parentPassThrough.ParentPassthroughMatchingSourceColumn.Name + ") output INSERTED." + parentPassThrough.ParentPassthroughSourceColumn.Name
//                    + " into @p values (" + lookup.ToString() + "); ";
//                sql += "select * from @p;";
//#if !DEBUG
//            sp.Send("GetParentPassthroughValue.sql = " + sql);
//#endif
//                value = db.SqlExecuteSingleValue(sql).ToString();
//            }

//            columnDataType = parentPassThrough.ParentPassthroughColumn.DataType;
//            if (columnDataType.Equals("int") || columnDataType.Equals("long"))
//                parentPassThrough.ParentPassthroughValue = value;
//            else
//                parentPassThrough.ParentPassthroughValue = "'" + value + "'";

//#if !DEBUG
//             sp.Send("GetParentPassthroughValue = " + table.ParentPassthrough.ParentPassthroughValue.ToString());
//#endif
//        }

//        public void CheckConstraints(ITable table)
//        {
//#if !DEBUG
//            SqlPipe sp = SqlContext.Pipe;
//            sp.Send("Enter CheckConstraints");
//#endif
//            if (!table.CheckConstraintsIU)
//                return;

//            var constraints = table.CheckConstraints;
//            foreach(var cons in constraints)
//            {
//                string sql = "select count(*) cnt from " + _tempTable;
//                int cnt = Convert.ToInt32(table.Database.SqlExecuteSingleValue(sql));

//                string ckText = cons.ConstraintText;
//                sql = "select * from " + _tempTable + " where not (" + ckText + ")";
//#if !DEBUG
//            sp.Send("CheckContraints.sql = " + sql);
//#endif
//                var ret = table.Database.SqlExecuteReader(sql);
//                if (cnt > ret.Count && ret.Count > 0)
//                {
//                    sql = "delete from " + _tempTable + " where not (" + ckText + ")";
//#if !DEBUG
//            sp.Send("CheckContraints.sql = " + sql);
//#endif
//                    table.Database.SqlExecuteNonQuery(sql);
//                }
//            }
//        }
//    }
}
