using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;

using Com.VeritasTS.DatabaseUtilityInterfaces;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

/*
namespace Com.VeritasTS.DatabaseProgramItems
{
    public class TriggerUtils
    {
        public void CascadeDelete(ITable table, string tempTable)
        {
#if !DEBUG
        SqlPipe sp = SqlContext.Pipe;
#endif
            List<ICascadingForeignKey> cfks = table.CascadingOnDeleteForeignKeys;
            if (cfks.Count == 0)
                return;

            IConnection conn = table.Database.GetAConnection();
            foreach (ICascadingForeignKey cfk in cfks)
            {
                ITransaction trans = conn.BeginTransaction();
                string dependentTableName = cfk.DependentTableSchemaName + "." + cfk.DependentTableName;
                string sql =
                    ""
                    + "delete from " + dependentTableName + "\n"
                    + " where exists ( \n"
                    + "              select top(1) 1 \n"
                    + "                from " + tempTable + " deleted \n";
                string whereSql = "";
                foreach( KeyValuePair<string,string> pair in cfk.KeyReferenceColumnMappings)
                {
                    if (whereSql.Length == 0)
                        whereSql = "               where deleted." + pair.Value + " = " + dependentTableName + "." + pair.Key + " \n";
                    else
                        whereSql = "                 and deleted." + pair.Value + " = " + dependentTableName + "." + pair.Key + " \n";
                }
                sql += whereSql + "              ) \n";
#if !DEBUG
            sp.Send("DeleteTriggerUtils.CascadeDelete.sql = " + sql);
#endif
                table.Database.SqlExecuteNonQuery(sql, conn, trans);
                trans.Commit();
            }
        }

        public bool IsImmutableChanged(ITable table, string tempTableI, string tempTableD)
        {
            var invariants = table.Columns.Where(c => c.IsImmutable);
            string invariantColumns = "";
            foreach (var c in invariants)
                invariantColumns += c.Name + ",";
            invariantColumns = invariantColumns.Substring(0, invariantColumns.Length - 1);

            string sql = "select " + invariantColumns + " from " + tempTableI + " except "
                    + "select " + invariantColumns + " from " + tempTableD;
            var ret = table.Database.SqlExecuteReader(sql);
            if (ret.Count > 0) return true;
            else return false;
        }

        public string AfterAnythingInsertAuditTableSql(ITable table, string tempTableI, string tempTableD)
        {
#if !DEBUG
        SqlPipe sp = SqlContext.Pipe;
        sp.Send("enter AfterAnythingInsertAuditTableSql");
#endif
            if (!table.IsAudited) return "";
            if (tempTableI.Length == 0 && tempTableD.Length == 0) return "";

            string audit = table.AuditTable.Schema.Name + "." + table.AuditTable.Name;
            string sql = "insert into " + audit + " (Action, LastUpdateDtm, LastUpdater, Id, ";

            if (tempTableD.Length == 0)
            {
                sql += GetColumnList(table.AuditOrJobsColumns, "New") + ") "
                    + "select 'insert', getdate(), cur_user, Id, " + GetColumnList(table.AuditOrJobsColumns, "")
                    + " from " + tempTableI;
            }

            if (tempTableI.Length == 0)
            {
                sql += GetColumnList(table.AuditOrJobsColumns, "Old") + ") "
                    + "select 'delete', getdate(), cur_user, Id, " + GetColumnList(table.AuditOrJobsColumns, "")
                    + " from " + tempTableD;
            }

            if (tempTableD.Length > 0 && tempTableI.Length > 0)
            {
                sql += GetColumnList(table.AuditOrJobsColumns, "New") + "," + GetColumnList(table.AuditOrJobsColumns, "Old") + ") "
                    + "select 'update', getdate(), i.cur_user, i.Id, "
                    + GetColumnList(table.AuditOrJobsColumns, "i.") + ", " + GetColumnList(table.AuditOrJobsColumns, "d.")
                    + " from " + tempTableI + " i"
                    + " join " + tempTableD + " d"
                    + " on i.id = d.id";
            }
#if !DEBUG
            sp.Send("AfterAnythingInsertAuditTableSql.sql = " + sql);
#endif
            return sql;
        }

//        public void MakeQueueJobs(ITable table, string tempTableI, string tempTableD)
//        {
//#if !DEBUG
//        SqlPipe sp = SqlContext.Pipe;
//        sp.Send("enter MakeQueueJobs ");
//#endif
//            //if (table.Queue == null ) return ;

//            //table.Queue.MakeJobs(tempTableI, tempTableD);
//        }

        public string MakeQueueJobsSql(ITable table, string tempTableI, string tempTableD)
        {
#if !DEBUG
                SqlPipe sp = SqlContext.Pipe;
                sp.Send("enter MakeQueueJobsSql");
#endif

            if (!table.IsQueued) return "";
            if (tempTableI.Length == 0 && tempTableD.Length == 0) return "";

            string jobs = table.QueueJobsTable.Schema.Name + "." + table.QueueJobsTable.Name;
            string KeyColumnSrcNames = "";
            string KeyColumnSrcNamesI = "";

            string sql = "insert into " + jobs + " (JobNum, JobDtm, Status, Action, ";

            foreach (var c in table.QueueKeyColumns)
            {
                sql += c.Name + ", ";
                KeyColumnSrcNames += c.SourceColumn.Name + ", ";
                KeyColumnSrcNamesI += "i." + c.SourceColumn.Name + ", ";
            }

            if (tempTableD.Length == 0)
            {
                sql += GetColumnList(table.AuditOrJobsColumns, "New") + ") "
                    + "select -1, getdate(), 'U', 'insert', " + KeyColumnSrcNames + GetColumnList(table.AuditOrJobsColumns, "")
                    + " from " + tempTableI;
            }

            if (tempTableI.Length == 0)
            {
                sql += GetColumnList(table.AuditOrJobsColumns, "Old") + ") "
                    + "select -1, getdate(), 'U', 'delete', " + KeyColumnSrcNames + GetColumnList(table.AuditOrJobsColumns, "")
                    + " from " + tempTableD;
            }

            if (tempTableD.Length > 0 && tempTableI.Length > 0)
            {
                sql += GetColumnList(table.AuditOrJobsColumns, "New") + "," + GetColumnList(table.AuditOrJobsColumns, "Old") + ") "
                    + "select -1, getdate(), 'U', 'update', " + KeyColumnSrcNamesI
                    + GetColumnList(table.AuditOrJobsColumns, "i.") + ", " + GetColumnList(table.AuditOrJobsColumns, "d.")
                    + " from " + tempTableI + " i"
                    + " join " + tempTableD + " d"
                    + " on i.id = d.id";
            }

            //string tempTable = tempTableI;
            //string action = "update";
            //if (tempTableD.Length == 0) action = "insert";

            //int jobNum = CommUtils.GetRandomNumber(10000000, 1000000000);
            //string sql = "insert into " + table.QueueJobsTable.Schema.Name + "." + table.QueueJobsTable.Name + "(JobNum, JobDtm, Status, Action, ";
            //string keyCols = "";

            //foreach (var c in table.QueueJobsTable.QueueKeyColumns)
            //{
            //    sql += c.Name + ",";
            //    keyCols += c.SourceColumn.Name + ",";
            //}
            //sql = sql.Substring(0, sql.Length - 1) + ") select " + jobNum.ToString() + ", getdate(), 'U','" + action + "', ";
            //keyCols = keyCols.Substring(0, keyCols.Length - 1);
            //sql += keyCols + " from " + tempTable;
#if !DEBUG
                    sp.Send("MakeQueueJobs.sql = " + sql);
#endif
            return sql;
        }

        private string GetColumnList(List<IColumn> cloumns, string OldNewEtc)
        {
            string list = "";
            foreach (var c in cloumns)
                list += OldNewEtc + c.Name + ",";
            return list.Substring(0, list.Length - 1);
        }

    }
}
*/