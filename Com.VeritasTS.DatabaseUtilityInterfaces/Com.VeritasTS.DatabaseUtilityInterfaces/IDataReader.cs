﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface IDataReader : ISelfAsserter, IDisposable
    {
        bool Read();
        int FieldCount { get; }
        object GetValue(int i);
        void Close();
    }

    // Note: we don't have a default implementation of this because
    //   any implementation is vendor-specific.

}
