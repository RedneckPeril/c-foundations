﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{
    public interface IView : IDatabaseObject
    {
        string SelectStatement { get; }
    }

    //public class ViewMutable : DatabaseObjectMutable, IView
    //{

    //    List<IColumn> _columns;

    //    public ViewMutable(
    //        string name
    //        , ISchema schema
    //        , string selectStatement
    //        , int? id = null
    //        , bool retrieveIDFromDatabase
    //        ) : base(
    //            name
    //            , schema.Database
    //            , schema: schema
    //            , owner: schema
    //            , parentObject: schema
    //            , objectType: ObjectTypes.View()
    //            , physicalObjectType: ObjectTypes.View()
    //            , id: id
    //            , retrieveIDFromDatabase: retrieveIDFromDatabase
    //            )
    //    {
    //        SelectStatement = selectStatement;
    //    }

    //    public string SelectStatement { get; set; }

    //    public override ISafeMemberOfImmutableOwner GetSafeReference()
    //    {
    //        return new ViewMutable(Name, (ITable)Owner.GetSafeReference(), ConstraintText, ID);
    //    }

    //}

    //public class ViewImmutable : DatabaseObjectImmutable, IView
    //{

    //    public ViewImmutable(
    //        string name
    //        , ITable owner
    //        , string constraintText
    //        , int? id = null
    //        )
    //    {
    //        _delegee = new ViewMutable(name, owner, constraintText, id);
    //    }

    //    // ISafeMemberOfImmutableOwner methods
    //    public override ISafeMemberOfImmutableOwner GetSafeReference()
    //    {
    //        return new ViewImmutable(Name, (ITable)Owner, ConstraintText, ID);
    //    }

    //    // IView properties
    //    public string ConstraintText { get { return ((IView)_delegee).ConstraintText; } }

    //}
}
