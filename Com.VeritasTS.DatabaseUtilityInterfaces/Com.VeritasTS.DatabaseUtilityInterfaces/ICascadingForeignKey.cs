﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface ICascadingForeignKey : ISelfAsserter, IMemberMatcher, ISafeMemberOfImmutableOwner
    {
        string DependentTableName { get; }
        string DependentTableSchemaName { get; }
        Dictionary<string, string> KeyReferenceColumnMappings { get; }
        Dictionary<string, string> GetKeyReferenceColumnMappingsSafely();
    }

    public interface ICascadingForeignKeyMutable : ICascadingForeignKey
    {
        void SetDependentTableName(string value);
        void SetDependentTableSchemaName(string value);
        void SetKeyReferenceColumnMappings(Dictionary<string, string> value);
        void AddOrOverwriteKeyReferenceColumnMapping(string dependentColumnName, string referenceColumnName);
        bool RemoveKeyReferenceColumnMapping(string dependentColumnName);
    }

    public class CascadingForeignKeyMutable : ICascadingForeignKeyMutable
    {

        private string _dependentTableName;
        private string _dependentTableSchemaName;
        Dictionary<string, string> _keyReferenceColumnMappings;

        public CascadingForeignKeyMutable(
            string dependentTableName = null
            , string dependentTableSchemaName = null
            , Dictionary<string, string> keyReferenceColumnMappings = null
            )
        {
            SetDependentTableName(dependentTableName, isFullyConstructed: false);
            SetDependentTableSchemaName(dependentTableSchemaName, isFullyConstructed: false);
            SetKeyReferenceColumnMappings(keyReferenceColumnMappings, isFullyConstructed: false);
            Debug.Assert(PassesSelfAssertion(typeof(CascadingForeignKeyMutable)));
        }

        public CascadingForeignKeyMutable(
            string dependentTableName
            , string dependentTableSchemaName
            , string dependentColumnName
            , string referenceColumnName
            )
        {
            SetDependentTableName(dependentTableName, isFullyConstructed: false);
            SetDependentTableSchemaName(dependentTableSchemaName, isFullyConstructed: false);
            _keyReferenceColumnMappings = new Dictionary<string, string>();
            _keyReferenceColumnMappings[dependentColumnName] = referenceColumnName;
            Debug.Assert(PassesSelfAssertion(typeof(CascadingForeignKeyMutable)));
        }

        // Object method overrides
        public override bool Equals(object obj)
        {
            return EqualityUtilities.MemberMatchwiseEqualOrBothNull(this, obj);
        }

        public override int GetHashCode()
        {
            return DependentTableName.GetHashCode()
                + 7 * DependentTableSchemaName.GetHashCode()
                + 11 * KeyReferenceColumnMappings.GetHashCode();
        }

        // ISelfAsserter methods
        public virtual bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(
                    DependentTableName != null
                    , "The dependent table in a cascading foreign key link must have a non-null name."
                    );
                Debug.Assert(
                    DependentTableName.Length > 0
                    , "The dependent column name in a cascading foreign key link cannot be an empty string."
                    );
                Debug.Assert(
                    DependentTableSchemaName != null
                    , "The dependent table's schema in a cascading foreign key link must have a non-null name."
                    );
                Debug.Assert(
                    DependentTableSchemaName.Length > 0
                    , "The dependent column name in a cascading foreign key link cannot be an empty string."
                    );
                Debug.Assert(
                    _keyReferenceColumnMappings != null
                    , "The inner key/reference column dictionary cannot be null."
                    );
                Debug.Assert(
                    _keyReferenceColumnMappings.Count > 0
                    , "The inner key/reference column dictionary cannot be empty."
                    );
                foreach( KeyValuePair<string,string> pair in _keyReferenceColumnMappings)
                {
                    Debug.Assert(
                        pair.Key.Length > 0
                        , "The dependent column name in a cascading foreign key link cannot be an empty string."
                        );
                    Debug.Assert(
                        pair.Value != null
                        , "The referenced column name in a cascading foreign key link cannot be null."
                        );
                    Debug.Assert(
                        pair.Value.Length > 0
                        , "The referenced column name in a cascading foreign key link cannot be an empty string."
                        );
                }
            }
            return true;
        }

        public bool SkipSelfAssertionUnlessThisType(Type testableType = null)
        {
            if (testableType == null) return false;
            return (!testableType.Equals(this.GetType()));
        }

        // IMemberMatcher methods
        public virtual bool MembersMatch(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(ICascadingForeignKey).IsAssignableFrom(obj.GetType())) return false;
            ICascadingForeignKey other = (ICascadingForeignKey)obj;
            if (!EqualityUtilities.EqualOrBothNull(this.DependentTableName, other.DependentTableName)) return false;
            if (!EqualityUtilities.EqualOrBothNull(this.DependentTableSchemaName, other.DependentTableSchemaName)) return false;
            if (!EqualityUtilities.EqualOrBothNull(this.KeyReferenceColumnMappings, other.KeyReferenceColumnMappings)) return false;
            return true;
        }

        // ISafeMemberOfImmutableOwner methods
        public ISafeMemberOfImmutableOwner GetSafeReference()
        {
            return new CascadingForeignKeyMutable(
                _dependentTableName
                , _dependentTableSchemaName
                , GetKeyReferenceColumnMappingsSafely()
                );
        }

        // ICascadingForeignKey properties
        public String DependentTableName
        {
            get { return _dependentTableName; }
            set { SetDependentTableName(value, isFullyConstructed: true); }
        }
        public String DependentTableSchemaName
        {
            get { return _dependentTableSchemaName; }
            set { SetDependentTableSchemaName(value, isFullyConstructed: true); }
        }
        public Dictionary<string, string> KeyReferenceColumnMappings
        {
            get { return _keyReferenceColumnMappings; }
            set { SetKeyReferenceColumnMappings(value, isFullyConstructed: true); }
        }
        public Dictionary<string, string> GetKeyReferenceColumnMappingsSafely()
        {
            Dictionary < string, string> retval = new Dictionary<string, string>();
            foreach (KeyValuePair<string,string> pair in _keyReferenceColumnMappings)
                retval[pair.Key] = pair.Value;
            return retval;
        }

        // ICascadingForeignKeyMutable methods

        public void SetDependentTableName(string value)
        {
            SetDependentTableName(value, isFullyConstructed: true);
        }
        protected void SetDependentTableName(string value, bool isFullyConstructed)
        {
            _dependentTableName = value;
            if (isFullyConstructed) Debug.Assert(PassesSelfAssertion());
        }

        public void SetDependentTableSchemaName(string value)
        {
            SetDependentTableSchemaName(value, isFullyConstructed: true);
        }
        protected void SetDependentTableSchemaName(string value, bool isFullyConstructed)
        {
            _dependentTableSchemaName = value;
            if (isFullyConstructed) Debug.Assert(PassesSelfAssertion());
        }

        public void SetKeyReferenceColumnMappings(Dictionary<string, string> value)
        {
            SetKeyReferenceColumnMappings(value, isFullyConstructed: true);
        }
        protected void SetKeyReferenceColumnMappings(Dictionary<string, string> value, bool isFullyConstructed)
        {
            if (value == null) _keyReferenceColumnMappings = new Dictionary<string, string>();
            else _keyReferenceColumnMappings = value;
            if (isFullyConstructed) Debug.Assert(PassesSelfAssertion());
        }

        public void AddOrOverwriteKeyReferenceColumnMapping(string referenceColumnName, string dependentColumnName)
        {
            AddOrOverwriteKeyReferenceColumnMapping(referenceColumnName, dependentColumnName, isFullyConstructed: true);
        }
        protected void AddOrOverwriteKeyReferenceColumnMapping(string referenceColumnName, string dependentColumnName, bool isFullyConstructed)
        {
            if (referenceColumnName != null)
                _keyReferenceColumnMappings[referenceColumnName] = dependentColumnName;
            if (isFullyConstructed) Debug.Assert(PassesSelfAssertion());
        }

        public bool RemoveKeyReferenceColumnMapping(string referenceColumnName)
        {
            return RemoveKeyReferenceColumnMapping(referenceColumnName, isFullyConstructed: true);
        }
        protected bool RemoveKeyReferenceColumnMapping(string referenceColumnName, bool isFullyConstructed)
        {
            bool retval = _keyReferenceColumnMappings.Remove(referenceColumnName);
            if (isFullyConstructed) Debug.Assert(PassesSelfAssertion());
            return retval;
        }

    }

    public class CascadingForeignKeyImmutable : ICascadingForeignKey
    {
        // Private members

        private ICascadingForeignKey _delegee;

        // Constructors

        public CascadingForeignKeyImmutable(
            string dependentTableName = null
            , string dependentTableSchemaName = null
            , Dictionary<string, string> keyReferenceColumnMappings = null
            )
        {
            _delegee =
                new CascadingForeignKeyMutable(dependentTableName, dependentTableSchemaName, keyReferenceColumnMappings);
            Debug.Assert(PassesSelfAssertion(typeof(CascadingForeignKeyImmutable)));
        }

        public CascadingForeignKeyImmutable(
            string dependentTableName
            , string dependentTableSchemaName
            , string dependentColumnName
            , string referenceColumnName
            )
        {
            _delegee =
                new CascadingForeignKeyMutable(DependentTableName, dependentTableSchemaName, dependentColumnName, referenceColumnName);
            Debug.Assert(PassesSelfAssertion(typeof(CascadingForeignKeyImmutable)));
        }

        public CascadingForeignKeyImmutable( ICascadingForeignKey delegee)
        {
            _delegee = delegee;
            Debug.Assert(PassesSelfAssertion(typeof(CascadingForeignKeyImmutable)));
        }

        // Object method overrides
        public override bool Equals(object obj) { return _delegee.Equals(obj); }

        public override int GetHashCode() { return _delegee.GetHashCode(); }

        // ISelfAsserter methods
        public virtual bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(
                    _delegee != null
                    , "The delegee within a CascadingForeignKeyImmutable object cannot be null."
                    );
                Debug.Assert(_delegee.PassesSelfAssertion());
            }
            return true;
        }

        public bool SkipSelfAssertionUnlessThisType(Type testableType = null)
        {
            if (testableType == null) return false;
            return (!testableType.Equals(this.GetType()));
        }

        // IMemberMatcher methods
        public virtual bool MembersMatch(Object obj) { return _delegee.MembersMatch(obj); }

        // ISafeMemberOfImmutableOwner methods
        public ISafeMemberOfImmutableOwner GetSafeReference() { return this; }

        // ICascadingForeignKey properties
        public String DependentTableName { get { return _delegee.DependentTableName; } }
        public String DependentTableSchemaName { get { return _delegee.DependentTableSchemaName; } }
        public Dictionary<string, string> KeyReferenceColumnMappings {  get { return _delegee.GetKeyReferenceColumnMappingsSafely(); } }
        public Dictionary<string, string> GetKeyReferenceColumnMappingsSafely() { return _delegee.GetKeyReferenceColumnMappingsSafely(); }

    }

}
