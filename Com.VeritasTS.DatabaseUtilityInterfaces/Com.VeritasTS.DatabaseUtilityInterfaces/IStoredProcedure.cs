﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface IStoredProcedure : IChildOfSchema
    {
        Dictionary<string,Tuple<int,ISqlParameter>> Parameters { get; }
        string BodyText { get; }
        Dictionary<string, Tuple<int, ISqlParameter>> GetParametersSafely();
    }

    public interface IStoredProcedureMutable : IStoredProcedure, IChildOfSchemaMutable
    {
        void SetParameters(
            Dictionary<string, Tuple<int, ISqlParameter>> value
            , bool assert = true
            );
        void AddOrOverwriteParameter(
            ISqlParameter parameter
            , bool normalizePositions = true
            , int? position = null
            , bool assert = true
            );
        void AddOrOverwriteParameterAfter(
            ISqlParameter parameter
            , string nameOfParameterToFollow
            , bool normalizePositions = true
            , bool assert = true
            );
        void AddOrOverwriteParameterBefore(
            ISqlParameter parameter
            , string nameOfParameterToPrecede
            , bool normalizePositions = true
            , bool assert = true
            );
        void NormalizeParameterPositions(bool assert = true);
        void SetBodyText(string bodyText, bool assert = true);
    }


    /// <summary>
    /// Boilerplate implementation of IChildOfSchemaMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidOwnerTypesWhenNotOwner, occasionally if not abstract type
    ///   and if Owner is not required to be ParentObject.</item>
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>SetOwner, rarely, e.g. when Owner should always be ParentObject 
    /// and vice versa.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>ParentObjectMustBeOwner, if true rather than false.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaMutable method overrides
    ///   <list type="bullet">
    ///   <item>SetParentObject, rarely, e.g. when Owner should always be ParentObject 
    /// and vice versa.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class StoredProcedureMutable: ChildOfSchemaMutable, IStoredProcedureMutable
    {

        Dictionary<string, Tuple<int, ISqlParameter>> _parameters;
        private string _bodyText;

        public StoredProcedureMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ISchema schema = null
            , IDataProcObject owner = null
            , Dictionary<string, Tuple<int, ISqlParameter>> parameters = null
            , string bodyText = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            ) : base(
                name
                , sqlTranslator
                , schema
                , ObjectTypes.StoredProcedure()
                , ObjectTypes.StoredProcedure()
                , owner
                , nameWhenPrefixed
                , id
            )
        {
            SetParameters(parameters, false);
            SetBodyText(bodyText, false);
            CompleteConstructionIfType(typeof(StoredProcedureImmutable), retrieveIDsFromDatabase, assert);
        }

        // Object method overrides and overridden GetHasCode assistants

        public override int GetHashCode()
        {
            int p1 = NextPrimes[base.HighestPrimeUsedInHashCode];
            int p2 = NextPrimes[p1];
            return base.GetHashCode()
                + p1 * (Parameters == null ? 0 : Parameters.GetHashCode())
                + p2 * (BodyText == null ? 0 : BodyText.GetHashCode());
        }

        public override int HighestPrimeUsedInHashCode
        {
            get
            {
                int p1 = NextPrimes[base.HighestPrimeUsedInHashCode];
                return NextPrimes[p1];
            }
        }

        // DataProcObjectMutable method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    Parameters != null
                    , "The Parameters property of an IStoredProcedure can be empty, but cannot be null."
                    );
                foreach (KeyValuePair<string, Tuple<int, ISqlParameter>> pair in Parameters)
                {
                    Debug.Assert(
                        pair.Value.Item2 != null
                        , "An IStoredProcedure cannot contain, in its Parameters dictionary, "
                        + "a pointer to a null IColumn."
                        );
                    Debug.Assert(
                        pair.Key.Equals(pair.Value.Item2.Name)
                        , "In the Parameters dictionary of an IStoredProcedure, every "
                        + "key must be the name of the column to which the key "
                        + "gives access."
                        );
                    Debug.Assert(pair.Value.Item2.PassesSelfAssertion());
                }
                Debug.Assert(
                    BodyText != null
                    , "An IStoredProcedure's body text cannot be null."
                    );
                Debug.Assert(
                    BodyText.Length > 0
                    , "An IStoredProcedure's body text cannot be the empty string."
                    );
                Debug.Assert(
                    HasNonWhitespace(BodyText)
                    , "An IStoredProcedure's body text cannot consist "
                    + "entirely of whitespace."
                    );
            }
            return true;
        }

        public override bool MembersMatch(object obj)
        {
            if (!base.MembersMatch(obj)) return false;
            if (!typeof(IStoredProcedure).IsAssignableFrom(obj.GetType())) return false;
            IStoredProcedure other = (IStoredProcedure)obj;
            return
                EqualityUtilities.EqualOrBothNull(Parameters, other.Parameters)
                && EqualityUtilities.EqualOrBothNull(BodyText, other.BodyText);
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new StoredProcedureMutable(
                Name
                , GetSqlTranslatorSafely()
                , GetSchemaSafely()
                , GetOwnerSafely()
                , GetParametersSafely()
                , BodyText
                , NameWhenPrefixed
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        // DatabaseObjectMutable method overrides

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = null
            , bool assert = true
            )
        {
            RejectIfInvalidParentObjectType(parentObject);
            bool retrieveIDs
                = retrieveIDsFromDatabase != null && retrieveIDsFromDatabase.HasValue
                ? retrieveIDsFromDatabase.Value
                : parentObject != null && parentObject.ID != null;
            IStoredProcedureMutable retval
                = new StoredProcedureMutable(
                Name
                , SqlTranslator
                , (ISchema)parentObject
                , Owner
                , Parameters
                , BodyText
                , NameWhenPrefixed
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
            if (retrieveIDs) retval.RetrieveIDsFromDatabase(assert: false);
            if (assert) Debug.Assert(retval.PassesSelfAssertion());
            return retval;
        }

        // IStoredProcedure methods

        public Dictionary<string, Tuple<int, ISqlParameter>> Parameters
        {
            get { return _parameters; }
            set { SetParameters(_parameters, true); }
        }
        public string BodyText
        {
            get { return _bodyText; }
            set { SetBodyText(_bodyText, true); }
        }
        public Dictionary<string, Tuple<int, ISqlParameter>> GetParametersSafely()
        {
            Dictionary<string, Tuple<int, ISqlParameter>> retval
                = new Dictionary<string, Tuple<int, ISqlParameter>>();
            foreach (
                KeyValuePair<string, Tuple<int, ISqlParameter>> pair
                in Parameters
                )
                retval[pair.Key] = pair.Value;
            return retval;
        }

        // IStoredProcedureMutable methods

        public virtual void SetParameters(
            Dictionary<string, Tuple<int, ISqlParameter>> value
            , bool assert = true
            )
        {
            if (_parameters != value)
            {
                _parameters = new Dictionary<string, Tuple<int, ISqlParameter>>();
                foreach (KeyValuePair<string, Tuple<int, ISqlParameter>> pair in value.Where(p => p.Value != null).Where(p => p.Value.Item2 != null))
                    AddOrOverwriteParameter(pair.Value.Item2, false, null, assert: false);
            }
            NormalizeParameterPositions(false);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void AddOrOverwriteParameter(
            ISqlParameter parameter
            , bool normalizePositions = false
            , int? position = null
            , bool assert = true
            )
        {
            if (parameter != null)
            {
                Debug.Assert(
                    parameter.Name != null
                    , "Cannot add parameters with null names to vables."
                    );
                string parameterName = parameter.Name;
                bool containsKey = _parameters.ContainsKey(parameterName);
                bool alreadyInPosition
                    = (
                    containsKey
                    ? position == null || _parameters[parameterName].Item1 == position.Value
                    : false
                    );
                bool appendToEnd = (position == null && !alreadyInPosition);
                if (appendToEnd)
                {
                    position
                        = (
                        _parameters.Count > 0
                        ? _parameters.Values.OrderByDescending(t => t.Item1).First().Item1 + 1
                        : 1
                        );
                    _parameters[parameterName] = new Tuple<int, ISqlParameter>(position.Value, parameter);
                }
                else if (alreadyInPosition)
                {
                    position = _parameters[parameterName].Item1;
                    _parameters[parameterName] = new Tuple<int, ISqlParameter>(position.Value, parameter);
                }
                else
                {
                    if (containsKey) _parameters.Remove(parameterName);
                    foreach (
                        KeyValuePair<string, Tuple<int, ISqlParameter>> pair
                        in _parameters.Where(kvp => kvp.Value.Item1 >= position).OrderByDescending(kvp => kvp.Value.Item1)
                        )
                        _parameters[pair.Key]
                            = new Tuple<int, ISqlParameter>(
                                pair.Value.Item1 + 1
                                , pair.Value.Item2
                                );
                    _parameters[parameterName]
                        = new Tuple<int, ISqlParameter>(position.Value, parameter);

                }
            }
            if (normalizePositions) NormalizeParameterPositions(false);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void AddOrOverwriteParameterAfter(
            ISqlParameter parameter
            , string nameOfParameterToFollow
            , bool normalizePositions = false
            , bool assert = true
            )
        {
            if (parameter != null)
            {
                if (!_parameters.ContainsKey(nameOfParameterToFollow))
                    throw new NonexistentParameterException(
                        nameOfParameterToFollow
                        , this
                        );
                int position = _parameters[nameOfParameterToFollow].Item1 + 1;
                foreach (
                    KeyValuePair<string, Tuple<int, ISqlParameter>> pair
                    in _parameters.Where(p => p.Value.Item1 >= position).OrderBy(p => p.Value.Item1).Take(1)
                    )
                {
                    if (pair.Value.Item1 > position && pair.Key.Equals(parameter.Name))
                        position = pair.Value.Item1;
                }
                AddOrOverwriteParameter(parameter, normalizePositions, position, assert);
            }
        }

        public virtual void AddOrOverwriteParameterBefore(
            ISqlParameter parameter
            , string nameOfParameterToPrecede
            , bool normalizePositions = false
            , bool assert = true
            )
        {
            if (parameter != null)
            {
                if (!_parameters.ContainsKey(nameOfParameterToPrecede))
                    throw new NonexistentParameterException(
                        nameOfParameterToPrecede
                        , this
                        );
                int position = _parameters[nameOfParameterToPrecede].Item1;
                foreach (
                    KeyValuePair<string, Tuple<int, ISqlParameter>> pair
                    in _parameters.Where(p => p.Value.Item1 < position).OrderByDescending(p => p.Value.Item1).Take(1)
                    )
                {
                    if (pair.Key.Equals(parameter.Name))
                        position = pair.Value.Item1;
                    else if (pair.Value.Item1 < position - 1)
                        position = position - 1;
                }
                AddOrOverwriteParameter(parameter, normalizePositions, position, assert);
            }
        }

        public void NormalizeParameterPositions(bool assert = true)
        {
            int i = 1;
            foreach (
                KeyValuePair<string, Tuple<int, ISqlParameter>> pair
                in Parameters.OrderBy(p => p.Value.Item1)
                )
            {
                Parameters[pair.Key] = new Tuple<int, ISqlParameter>(i, Parameters[pair.Key].Item2);
                i++;
            }
        }

        public virtual bool RemoveParameter(
            string nameOfDoomed
            , bool normalizePositions = false
            , bool assert = true
            )
        {
            bool retval = false;
            if (nameOfDoomed != null)
                retval = _parameters.Remove(nameOfDoomed);
            if (normalizePositions) NormalizeParameterPositions(false);
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }

        public bool RemoveParameter(
            ISqlParameter doomed
            , bool normalizePositions = false
            , bool assert = true
            )
        {
            if (doomed == null || doomed.Name == null)
            {
                if (normalizePositions) NormalizeParameterPositions(assert);
                return false;
            }
            else
                return RemoveParameter(doomed.Name, normalizePositions, assert);
        }
        public void SetBodyText(string bodyText, bool assert = true)
        {
            _bodyText = bodyText;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

    }

    /// <summary>
    /// Boilerplate implementation of IStoredProcedure without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public class StoredProcedureImmutable : ChildOfSchemaImmutable, IStoredProcedure
    {

        private IStoredProcedure Delegee { get { return (IStoredProcedure)_delegee; } }

        protected StoredProcedureImmutable() { }

        public StoredProcedureImmutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ISchema schema = null
            , IDataProcObject owner = null
            , Dictionary<string, Tuple<int, ISqlParameter>> parameters = null
            , string bodyText = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            Dictionary<string, Tuple<int, ISqlParameter>> safeParameters = null;
            if (parameters != null)
            {
                safeParameters = new Dictionary<string, Tuple<int, ISqlParameter>>();
                foreach (KeyValuePair<string, Tuple<int, ISqlParameter>> pair in parameters)
                    safeParameters[pair.Key]
                        = new Tuple<int, ISqlParameter>(
                            pair.Value.Item1
                            , (ISqlParameter)pair.Value.Item2.GetSafeReference()
                            );
            }
            _delegee = new StoredProcedureMutable(
                name
                , sqlTranslator == null ? null : (ISqlTranslator)sqlTranslator.GetSafeReference()
                , schema == null ? null : (ISchema)schema.GetSafeReference()
                , owner == null ? null : (IDataProcObject)owner.GetSafeReference()
                , safeParameters
                , bodyText
                , nameWhenPrefixed
                , id: id
                , retrieveIDsFromDatabase: retrieveIDsFromDatabase
                , assert: false
                );
            CompleteConstructionIfType(typeof(StoredProcedureMutable), retrieveIDsFromDatabase: false, assert: assert);
        }

        public StoredProcedureImmutable(IStoredProcedure delegee, bool assert = true)
        {
            _delegee = delegee;
            CompleteConstructionIfType(typeof(StoredProcedureMutable), retrieveIDsFromDatabase: false, assert: assert);
        }

        // DataProcObject method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(IStoredProcedure); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            IObjectInSchema retval
                = new StoredProcedureImmutable(
                (IStoredProcedure)
                    ((IObjectInSchema)_delegee).GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, assert)
                    );
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }
        public override IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            IObjectInSchema retval
                = new StoredProcedureImmutable(
                (IStoredProcedure)
                    ((IObjectInSchema)_delegee).GetCopyForParentObject(parentObject, retrieveIDsFromDatabase, assert)
                    );
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }

        // IStoredProcedure properties and methods

        public Dictionary<string, Tuple<int, ISqlParameter>> Parameters { get { return Delegee.Parameters; } }
        public string BodyText { get { return Delegee.BodyText; } }
        public Dictionary<string, Tuple<int, ISqlParameter>> GetParametersSafely()
        {
            return Delegee.GetParametersSafely();
        }

    }

    //public interface IStoredProcedureForTriggers : IStoredProcedure { }

    //public class StoredProcedureForTriggers : StoredProcedureMutable, IStoredProcedureForTriggers
    //{
    //    override public IObjectType ObjectType { get; set; } = new ObjectType { Name = "StoredProcedur", IsVable = false };
    //    override public IObjectType PhysicalObjectType { get; set; } = new ObjectType { Name = "StoredProcedur", IsVable = false };
    //    override public object GetThreadsafeClone() { return this; } // need a deep copy

    //    //public TriggerStoredProcedureParametersStringOtherThanDefaults AdditionalParametersString { get; set; }
    //    override public string ExistsSql()
    //    {
    //        return SqlTranslator.GetProcedureExistsSql(this);
    //    }
    //    override public void EnsureExistenceInDatabase()
    //    {
    //        Console.WriteLine("StoredProcedur are created from another app");
    //    }
    //}

    public class NonexistentParameterException : Exception
    {
        private NonexistentParameterException() { }
        public NonexistentParameterException(string missingParameterName, IStoredProcedure storedProcedure)
        {
            MissingParameterName = missingParameterName;
            StoredProcedure = storedProcedure;
        }
        public IStoredProcedure StoredProcedure { get; }
        public string MissingParameterName { get; }
    }

}
