﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;


namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface IPrimaryKey : IUniqueKey
    {
        bool IsNullable { get; }
    }

    public interface IPrimaryKeyMutable : IPrimaryKey, IUniqueKeyMutable { }

    /// <summary>
    /// Boilerplate implementation of IPrimaryKeyMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides.</item>
    ///   <list type="bullet">
    ///   <item>RetrieveIDsFromDatabase, if new IDatabaseObject members have been
    /// added as members.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaMutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidParentObjectTypes, if narrower than ITable.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   <item>SetParentObject, rarely, e.g. when ParentObject and Owner should always
    ///   equal not only each other but some third member as well.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public abstract class PrimaryKeyMutable : UniqueKeyMutable, IPrimaryKeyMutable
    {

        public PrimaryKeyMutable(
            string name
            , ISqlTranslator sqlTranslator
            , ITable table
            , IObjectType physicalObjectType = null
            , ITableColumn constrainedColumn = null
            , List<string> columnNames = null
            , bool isClustered = true
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , sqlTranslator
                , table
                , ObjectTypes.PrimaryKey()
                , physicalObjectType
                , constrainedColumn
                , columnNames != null ? columnNames
                : constrainedColumn != null && constrainedColumn.Name != null ? new List<string> { constrainedColumn.Name }
                : null
                , isClustered
                , nameWhenPrefixed
                , id
                , retrieveIDsFromDatabase: false
                , assert: false
                )
        {
            CompleteConstructionIfType(typeof(PrimaryKeyMutable), retrieveIDsFromDatabase, assert);
        }

        public PrimaryKeyMutable(
            string name
            , IObjectType physicalObjectType = null
            , ITableColumn constrainedColumn = null
            , List<string> columnNames = null
            , bool isClustered = true
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , ObjectTypes.PrimaryKey()
                , physicalObjectType
                , constrainedColumn
                , columnNames != null ? columnNames
                : constrainedColumn != null && constrainedColumn.Name != null ? new List<string> { constrainedColumn.Name }
                : null
                , isClustered
                , nameWhenPrefixed
                , id
                , retrieveIDsFromDatabase: false
                , assert: false
                )
        {
            CompleteConstructionIfType(typeof(PrimaryKeyMutable), retrieveIDsFromDatabase, assert);
        }

        public PrimaryKeyMutable(
            IObjectType physicalObjectType = null
            , ITableColumn constrainedColumn = null
            , List<string> columnNames = null
            , bool isClustered = true
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                ObjectTypes.PrimaryKey()
                , physicalObjectType
                , constrainedColumn
                , columnNames != null ? columnNames
                : constrainedColumn != null && constrainedColumn.Name != null ? new List<string> { constrainedColumn.Name }
                : null
                , isClustered
                , nameWhenPrefixed
                , id
                , retrieveIDsFromDatabase: false
                , assert: false
                )
        {
            CompleteConstructionIfType(typeof(PrimaryKeyMutable), retrieveIDsFromDatabase, assert);
        }

        // IDataProcObject method overrides
        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                if (ParentObject != null)
                foreach (string name in ColumnNames)
                    Debug.Assert(
                        !(((ITable)ParentObject).Columns[name].Item2.IsNullable)
                        , "Column '" + name + "' cannot be part of the primary "
                        + "key for table '" + ParentObject.Name + "', because it is nullable."
                        );
            }
            return true;
        }

        // IPrimaryKey properties

        public virtual bool IsNullable
        {
            get { return typeof(INullablePrimaryKey).IsAssignableFrom(this.GetType()); }
        }

    }


    /// <summary>
    /// Boilerplate implementation of IUniqueKey without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>PotentiallySingleColumnConstraintImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetCopyForColumn, always unless abstract class.</item>
    ///   <item>GetCopyForTable, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public abstract class PrimaryKeyImmutable : UniqueKeyImmutable, IPrimaryKey
    {

        private IPrimaryKey Delegee { get { return (IPrimaryKey)_delegee; } }

        protected PrimaryKeyImmutable() : base() { }

        public PrimaryKeyImmutable(
            IPrimaryKey delegee
            , bool assert = true
            ) : base(delegee, false)
        {
            CompleteConstructionIfType(typeof(PrimaryKeyImmutable), retrieveIDsFromDatabase: false);
        }

        // IPrimaryKey properties

        public virtual bool IsNullable { get { return Delegee.IsNullable; } }

    }

}
