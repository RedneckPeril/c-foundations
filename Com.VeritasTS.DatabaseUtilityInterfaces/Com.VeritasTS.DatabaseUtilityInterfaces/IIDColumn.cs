﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface IIDColumn : IColumn { }
    public interface IIDColumnMutable : IIDColumn, IColumnMutable { }

    /// <summary>
    /// Boilerplate implementation of IIDColumnMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode(), if additional members are added.</item>
    ///   <item>HighestPrimeUsedInHashCode, if GetHashCode() is overridden.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>SetOwner, if (as is occasionally the case) Owner
    /// is constrained to be an instance of a particular object type.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>ObjectIsValidTypeForParentObject, rarely, and generally only for
    ///   highly special-purpose column classes dedicated to a particular type
    ///   of special-purpose table class.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class IDColumnMutable : ColumnMutable, IIDColumnMutable
    {

        public IDColumnMutable(): base()
        {
            SetName("id");
            SetDataType("int");
        }
        
        public IDColumnMutable(
            string name = "id"
            , ISqlTranslator sqlTranslator = null
            , IVable vable = null
            , bool isNullable = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
            : base(
                  name == null ? "id" : name
                  , "int"
                  , sqlTranslator
                  , vable
                  , isNullable
                  , nameWhenPrefixed
                  , id
                  )
        {
            CompleteConstructionIfType(typeof(IDColumnMutable), retrieveIDsFromDatabase);
        }

        // DataProcObjectMutable method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    "int".Equals(DataType)
                    , "A IDColumn must be typed as int, not "
                    + (DataType == null ? "[null]" : DataType)
                    + "."
                    );
            }
            return true;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new IDColumnMutable(
                Name
                , GetSqlTranslatorSafely()
                , GetVableSafely()
                , IsNullable
                , NameWhenPrefixed
                , ID
                , retrieveIDsFromDatabase: false 
                , assert: assert
                );
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: "id"
                , sqlTranslator: null
                , vable: null
                , isNullable: false
                , nameWhenPrefixed: null
                , id: null
                );
        }

        protected virtual void ConstructAfterConstruction(
            string name 
            , ISqlTranslator sqlTranslator 
            , IVable vable 
            , bool isNullable 
            , string nameWhenPrefixed 
            , int? id 
            )
        {
            if (Name != null) name = Name;
            if (name == null) name = "id";
            base.ConstructAfterConstruction(
                name
                , "int"
                , sqlTranslator
                , vable
                , isNullable
                , null
                , null
                );
        }

        // ObjectInSchemaMutable method overrides

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true)
        {
            RejectIfInvalidParentObjectType(parentObject);
            return new IDColumnMutable(
                Name
                , SqlTranslator
                , (IVable)parentObject
                , IsNullable
                , NameWhenPrefixed
                , ID
                , retrieveIDsFromDatabase
                , assert
                );
        }

        // IIDColumn methods

        public void PopulateValuesInNullIDRows() { }

    }

    /// <summary>
    /// Boilerplate implementation of IIDColumn without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObject method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public class IDColumnImmutable : TableColumnImmutable, IIDColumn
    {

        private IIDColumn Delegee { get { return (IIDColumn)_delegee; } }

        public IDColumnImmutable(
            string name = "id"
            , ISqlTranslator sqlTranslator = null
            , IVable vable = null
            , bool isNullable = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
            : base( delegee: null)
        {
            _delegee
                = new IDColumnMutable(
                    name
                    , sqlTranslator
                    , vable
                    , IsNullable
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , assert: false
                    );
            CompleteConstructionIfType(
                typeof(IDColumnImmutable)
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        public IDColumnImmutable(IIDColumn delegee, bool assert = true)
            : base(delegee)
        {
            CompleteConstructionIfType(
                typeof(IDColumnImmutable)
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        // DataProcObject method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(IIDColumn); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new IDColumnImmutable(
                (IIDColumn)
                Delegee.GetReferenceForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , assert: false
                    )
                , assert: assert
                );
        }

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new IDColumnImmutable(
                (IIDColumn)
                Delegee.GetCopyForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , assert: false
                    )
                , assert: assert
                );
        }

    }

}