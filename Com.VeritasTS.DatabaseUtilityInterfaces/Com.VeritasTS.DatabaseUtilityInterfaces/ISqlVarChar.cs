﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// This interface identifies the "smallint"
    /// SQL type.
    /// </summary>
    /// <remarks>
    /// We ordinarily just retrieve the static object
    /// SqlVarChar.Standard(). Objects are immutable
    /// and therefore threadsafe.
    /// </remarks>
    public interface ISqlVarChar : ISqlStringType { }

    /// <summary>
    /// A basic, immutable implementation of the ISqlVarChar interface.
    /// </summary>
    public sealed class SqlVarChar : SqlStringType, ISqlVarChar
    {

        /****************
         *  Private members
         ***************/

        private string FullTypeNameForCodeErrors { get { return typeof(SqlVarChar).FullName; } }
        private static SqlVarChar _standard;

        /****************
         *  Constructor
         ***************/

        public SqlVarChar(int maxLength = 1) : base(maxLength) { }

        /****************
         *  ISqlVarChar properties and methods
         ***************/

        public override string DefaultSql
        {
            get { return "varchar(" + MaxLength + ")"; }
        }

        public static SqlVarChar Standard()
        {
            if (_standard == null) _standard = new SqlVarChar();
            return _standard;
        }

    }

}
