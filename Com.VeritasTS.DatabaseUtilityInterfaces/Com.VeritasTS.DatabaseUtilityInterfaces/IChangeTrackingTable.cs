﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{
    public interface IChangeTrackingTable : ITable
    {
        ITable TrackedTable { get; }
        ITable GetTrackedTableSafely();
        bool HasChangeResponderColumn { get; }
        IChangeResponderColumn ChangeResponderColumn { get; }
    }

    public interface IChangeTrackingTableMutable : IChangeTrackingTable, ITableMutable
    {
        void SetTrackedTable(
            ITable value
            , bool? retrieveTrackedTableIDFromDatabase = default(bool?)
            , bool assert = true
            );
        void SetHasChangeResponderColumn(
            bool value
            , bool? retrieveChangeResponderColumnIDFromDatabase = default(bool?)
            , bool assert = true
            );
    }

    public static class UtilitiesForChangeTrackingTables
    {

        public static void AddAuditColumns(ITableMutable table, ITable trackedTable, ISqlTranslator sql = null, bool retrieveIDsFromDatabase = false)
        {
            if (sql == null) sql = table.Database.SqlTranslator;
            if (sql == null) sql = trackedTable.Database.SqlTranslator;
            string trueSql = sql.TrueSql();
            string falseSql = sql.FalseSql();
            string boolSql = sql.BooleanTypeSql();
            if (trackedTable.IDColumns.Count() > 0)
            {
                // Here we deal with the possibility that the ID column has already been added as
                //   a QueueKeyColumn, if we are populating a ChangeTrackingTable. In that case
                //   we would not want to overwrite it.
                if (!table.Columns.ContainsKey(trackedTable.IDColumns.First().Key))
                    table.AddOrOverwriteColumn(
                        new ColumnMutable(
                            trackedTable.IDColumns.First().Key
                            , dataType: "int"
                            , isNullable: false
                            , isImmutable: true
                            , nameWhenPrefixed: trackedTable.IDColumns.First().Value.Item2.NameWhenPrefixed
                            )
                        );
                table.AddOrOverwriteColumn(
                    new TableColumnMutable(
                        "change" + trackedTable.IDColumns.First().Value.Item2.NameWhenPrefixed
                        , dataType: boolSql
                        , calcFormula:
                        "cast( case action when 'update' then "
                        + falseSql + " else " + trueSql + " end as "
                        + boolSql + " )"
                        , isPersisted: true
                    )
                    );
            }
            foreach (Tuple<int, ITableColumn> tuple in trackedTable.InsertableColumns.Values.OrderBy(t => t.Item1))
            {
                int position = tuple.Item1;
                IColumn col = tuple.Item2;
                string oldCol = "old" + col.NameWhenPrefixed;
                string newCol = "new" + col.NameWhenPrefixed;
                table.AddOrOverwriteColumn(new ColumnMutable(oldCol, col.DataType));
                table.AddOrOverwriteColumn(new ColumnMutable(newCol, col.DataType));
                table.AddOrOverwriteColumn(
                    new TableColumnMutable(
                        "change" + col.NameWhenPrefixed
                        , dataType: boolSql
                        , calcFormula:
                        "cast( case action when 'update' then "
                        + "( case when " + oldCol + " = " + newCol
                        + " or coalesce(" + oldCol + ", " + newCol + ") is null then "
                        + falseSql
                        + " else " + trueSql
                        + " end ) else " + trueSql + " end as " + boolSql + " )"
                        , isPersisted: true
                        )
                    );
            }
            if (retrieveIDsFromDatabase) table.RetrieveIDsFromDatabase();
        }

        public static void AddStandardChangeTrackingColumns(ITableMutable table, ITable trackedTable, ISqlTranslator sqlT = null, bool retrieveIDsFromDatabase = false)
        {
            if (sqlT == null) sqlT = table.Database.SqlTranslator;
            if (sqlT == null) sqlT = trackedTable.Database.SqlTranslator;
            string trueSql = sqlT.TrueSql();
            string falseSql = sqlT.FalseSql();
            string boolSql = sqlT.BooleanTypeSql();
            string actionConstraintName = sqlT.TableConstraintName("_ckA", table.Name);
            table.AddOrOverwriteColumn(
                new TableColumnMutable(
                    "action"
                    , "varchar(6)"
                    , isNullable: false
                    , isImmutable: true
                    , checkConstraints:
                    new Dictionary<string, ISingleColumnCheckConstraint>
                    {
                        [actionConstraintName]
                        = new SingleColumnCheckConstraintMutable(
                            name: actionConstraintName
                            , sqlTranslator: sqlT
                            , constraintText: "action in ( 'insert', 'update', 'delete' )"
                            )
                    }
                    )
                );
            table.AddOrOverwriteColumn(new ColumnMutable("updateDtm", "datetime", isNullable: false, isImmutable: true));
            table.AddOrOverwriteColumn(new ColumnMutable("updater", "nvarchar(100)", isNullable: false, isImmutable: true));
            AddAuditColumns(table, trackedTable, sqlT, retrieveIDsFromDatabase: false);
            if (retrieveIDsFromDatabase) table.RetrieveIDsFromDatabase();
        }
    }

    /// <summary>
    /// Boilerplate implementation of ITableMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    ///   affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidOwnerTypesWhenNotOwner, occasionally if not abstract type
    ///   and if Owner is not required to be ParentObject.</item>
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>ParentObjectMustBeOwner, if true rather than false.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class ChangeTrackingTableMutable : TableMutable, IChangeTrackingTableMutable
    {

        public ChangeTrackingTableMutable() : base() { }

        public ChangeTrackingTableMutable(
            string name
            , ISqlTranslator sqlTranslator
            , ISchema schema
            , ITable trackedTable
            , bool allowsUpdates = true
            , bool allowsDeletes = true
            , IIdentityColumn identityColumn = null
            , bool hasChangeResponderColumn = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , sqlTranslator
                , schema
                , ObjectTypes.Table()
                , owner: trackedTable
                , allowsUpdates: allowsUpdates
                , allowsDeletes: allowsDeletes
                , nameWhenPrefixed: nameWhenPrefixed
                , id: id
                )
        {
            ConstructNewMembers(
                identityColumn
                , hasChangeResponderColumn
                , true
                );
            CompleteConstructionIfType(typeof(ChangeTrackingTableMutable), retrieveIDsFromDatabase, assert);
        }

        // DataProcObjectMutable method overrides
        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (IsFullyConstructed)
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    Owner != null
                    , "A change-tracking table must have a non-null tracked table."
                    );
                Debug.Assert(
                    IDColumns.Count == 1
                    , "A change-tracking table must have exactly one identity column."
                    );
                Debug.Assert(
                    Columns
                    .Where(p=>typeof(IChangeResponderColumn).IsAssignableFrom(p.Value.Item2.GetType()))
                    .Count() < 2
                    , "A change-tracking table can have at most one change responder column."
                    );
            }
            return true;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            IIDColumn idColumn = IDColumns.First().Value.Item2;
            return new ChangeTrackingTableMutable(
                Name
                , GetSqlTranslatorSafely()
                , (ISchema)GetParentObjectSafely()
                , GetTrackedTableSafely()
                , AllowsUpdates
                , AllowsDeletes
                , GetIdentityColumnSafely()
                , HasChangeResponderColumn
                , NameWhenPrefixed
                , ID
                , false
                , assert
                );
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , schema: null
                , trackedTable: null
                , allowsUpdates: true
                , allowsDeletes: true
                , identityColumn: null
                , hasChangeResponderColumn: false
                , nameWhenPrefixed: null
                , id: null
                );
        }

        protected virtual void ConstructAfterConstruction(
           string name
            , ISqlTranslator sqlTranslator
            , ISchema schema
            , ITable trackedTable
            , bool allowsUpdates
            , bool allowsDeletes
            , IIdentityColumn identityColumn
            , bool hasChangeResponderColumn
            , string nameWhenPrefixed
            , int? id
            )
        {
            base.ConstructAfterConstruction(
                name: name
                , sqlTranslator: sqlTranslator
                , schema: schema
                , objectType: ObjectTypes.Table()
                , owner: trackedTable
                , allowsUpdates: allowsUpdates
                , allowsDeletes: allowsDeletes
                , columns: null
                , indexes: null
                , hasIdentityColumn: true
                , identityColumn: identityColumn
                , primaryKey: null
                , uniqueKeys: null
                , foreignKeys: null
                , cascadingForeignKeyRegistry: null
                , triggers: null
                , checkConstraints: null
                , defaultConstraints: null
                , isAudited: false
                , auditTable: null
                , publishesChanges: false
                , publishedChangesTable: null
                , changeSubscriberRegistry: null
                , parentTable: null
                , parentInsertColumnListExpressions: null
                , canHaveChildren: false
                , leavesTable: null
                , maintainChildVablesTable: false
                , childVablesTable: null
                , hasIDColumn: false
                , idColumn: null
                , hasInputterColumn: false
                , inputterColumn: null
                , validInputtersTable: null
                , hasLastUpdateDtmColumn: false
                , hasLastUpdaterColumn: false
                , nameWhenPrefixed: null
                , id: default(int?)
                );
            ConstructNewMembers(
                identityColumn
                , hasChangeResponderColumn
                , false
                );
        }

        private void ConstructNewMembers(
            IIdentityColumn identityColumn
            , bool hasChangeResponderColumn
            , bool forceUpdates = true
            )
        {
            ISqlTranslator sql = SqlTranslator;
            if (sql == null)
                throw new ConstructionRequiresSqlTranslatorException(
                    this
                    , typeof(VableMutable).FullName
                    );
            ITable trackedTable = TrackedTable;
            if (trackedTable == null)
                throw new ConstructionRequiresParentObjectException(
                    this
                    , typeof(VableMutable).FullName
                    );
            if (!HasIdentityColumn)
                SetHasIdentityColumn(
                    true
                    , identityColumn
                    , retrieveIdentityColumnIDFromDatabase: false
                    , assert: false
                    );
            SetHasChangeResponderColumn(hasChangeResponderColumn, false, false);
            UtilitiesForChangeTrackingTables.AddStandardChangeTrackingColumns(
                this
                , trackedTable: trackedTable
                , sqlT: sql
                , retrieveIDsFromDatabase: false
                );
            if (PrimaryKey == null)
                SetPrimaryKey(
                    new NonnullablePrimaryKeyMutable(
                        name: Name + "_pk"
                        , sqlTranslator: SqlTranslator
                        , table: this
                        , constrainedColumn: IdentityColumn
                        , columnNames: new List<string> { IdentityColumnName }
                        , retrieveIDsFromDatabase: false
                        , assert: false
                        )
                    , retrievePrimaryKeyIDFromDatabase: false
                    , assert: false
                    );
            if (!hasChangeResponderColumn)
                SetHasChangeResponderColumn(hasChangeResponderColumn, false, false);
        }

        public override void SetOwner(IDataProcObject value, bool assert = true)
        {
            RejectIfInvalidOwnerType(value);
            SetTrackedTable((ITable)value, assert: assert);
        }

        // IDatabaseObjectMutable method overrides

        public override List<Type> ValidOwnerTypesWhenNotParentObject
        {
            get { return new List<Type> { typeof(ITable) }; }
        }

        public override void RetrieveIDsFromDatabase(bool assert = true)
        {
            if (Database != null && Owner != null)
            {
                if (typeof(ITableMutable).IsAssignableFrom(TrackedTable.GetType()))
                    ((ITableMutable)Owner).RetrieveIDsFromDatabase(assert: false);
                else
                    SetTrackedTable(
                        (ITable)Owner
                        , true
                        , false
                        );
            }

            base.RetrieveIDsFromDatabase(assert);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        // ObjectInSchemaMutable method overrides

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            RejectIfInvalidParentObjectType(parentObject);
            IIDColumn idColumn = IDColumn;
            return new ChangeTrackingTableMutable(
                Name
                , SqlTranslator
                , (ISchema)parentObject
                , TrackedTable
                , AllowsUpdates
                , AllowsDeletes
                , IdentityColumn
                , HasChangeResponderColumn
                , NameWhenPrefixed
                , ID
                , false
                , assert
                );
        }

        // IChangeTrackingTable properties and methods
        public ITable TrackedTable { get { return (ITable)Owner; } set { SetTrackedTable(value); } }
        public ITable GetTrackedTableSafely()
        {
            return (ITable)GetOwnerSafely();
        }
        public bool HasChangeResponderColumn
        {
            get
            {
                return Columns
                        .Where(p => typeof(IChangeResponderColumn).IsAssignableFrom(p.Value.Item2.GetType()))
                    .Count() > 0;
            }
        }
        public IChangeResponderColumn ChangeResponderColumn
        {
            get
            {
                foreach (
                    Tuple<int,IColumn> t
                    in Columns
                    .Values
                    .Where(t => typeof(IChangeResponderColumn).IsAssignableFrom(t.Item2.GetType()))
                    )
                    return (IChangeResponderColumn)t.Item2;
                return null; 
            }
        }

        // IChangeTrackingTableMutable properties and methods

        public virtual void SetTrackedTable(
            ITable value
            , bool? retrieveTrackedTableIDFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            if (value == null)
                throw new NullArgumentException(
                    "value"
                    , typeof(ChangeTrackingTableMutable).FullName
                    + ".SetTrackedTable( value, retrieveTrackedTableIDFromDatabase, assert)"
                    );
            bool retrieveID
                = retrieveTrackedTableIDFromDatabase != null && retrieveTrackedTableIDFromDatabase.HasValue
                ? retrieveTrackedTableIDFromDatabase.Value
                : ID != null && value.ID == null;
            if (retrieveID) 
            {
                if (typeof(ITableMutable).IsAssignableFrom(value.GetType()))
                    ((ITableMutable)value).RetrieveIDsFromDatabase(false);
                else
                    value
                        = (ITable)
                        value.GetReferenceForParentObject(
                            value.ParentObject
                            , true
                            , false
                            );
            }
            base.SetOwner(value, false);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public void SetHasChangeResponderColumn(
            bool value
            , bool? retrieveChangeResponderColumnIDFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            if (value != HasChangeResponderColumn)
            {
                if (value)
                    AddOrOverwriteColumn(
                        new ChangeResponderColumnMutable(
                            null
                            , SqlTranslator
                            , this
                            , retrieveIDsFromDatabase: false
                            , assert: false
                            )
                        , position: 2
                        , retrieveColumnIDFromDatabase:
                        retrieveChangeResponderColumnIDFromDatabase != null && retrieveChangeResponderColumnIDFromDatabase.HasValue
                        ? retrieveChangeResponderColumnIDFromDatabase.Value
                        : ID != null
                        , assert: false
                        );
                else
                    RemoveColumn(ChangeResponderColumn, false);
                if (assert) Debug.Assert(PassesSelfAssertion());
            }
        }

    }

    /// <summary>
    /// Boilerplate implementation of IChangeTrackingTable without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public class ChangeTrackingTableImmutable : TableImmutable, IChangeTrackingTable
    {
        private IChangeTrackingTable Delegee { get { return (IChangeTrackingTable)_delegee; } }

        //Disable default constructor
        private ChangeTrackingTableImmutable() : base(delegee: null) { }

        public ChangeTrackingTableImmutable(
            string name
            , ISqlTranslator sqlTranslator
            , ISchema schema
            , ITable trackedTable
            , bool allowsUpdates = true
            , bool allowsDeletes = true
            , IIdentityColumn identityColumn = null
            , bool hasChangeResponderColumn = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(delegee: null)
        {
            _delegee =
                new ChangeTrackingTableMutable(
                    name
                    , sqlTranslator
                    , schema
                    , trackedTable
                    , allowsUpdates
                    , allowsDeletes
                    , identityColumn
                    , hasChangeResponderColumn
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    );
            CompleteConstructionIfType(typeof(ChangeTrackingTableImmutable), false, true);
        }

        public ChangeTrackingTableImmutable(IChangeTrackingTable delegee) : base(delegee: null)
        {
            _delegee = delegee;
            CompleteConstructionIfType(typeof(ChangeTrackingTableImmutable), false, true);
        }

        // DataProcObject method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(IChangeTrackingTable); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            IObjectInSchema retval
                = new ChangeTrackingTableImmutable(
                (IChangeTrackingTable)
                    ((IObjectInSchema)_delegee).GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, assert)
                    );
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }
        public override IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            IObjectInSchema retval
                = new ChangeTrackingTableImmutable(
                (IChangeTrackingTable)
                    ((IObjectInSchema)_delegee).GetCopyForParentObject(parentObject, retrieveIDsFromDatabase, assert)
                    );
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }

        // IChangeTrackingTable properties and methods
        public ITable TrackedTable { get { return Delegee.TrackedTable; } }
        public ITable GetTrackedTableSafely() { return Delegee.GetTrackedTableSafely(); }
        public bool HasChangeResponderColumn { get { return Delegee.HasChangeResponderColumn; } }
        public IChangeResponderColumn ChangeResponderColumn { get { return Delegee.ChangeResponderColumn; } }

    }

}
