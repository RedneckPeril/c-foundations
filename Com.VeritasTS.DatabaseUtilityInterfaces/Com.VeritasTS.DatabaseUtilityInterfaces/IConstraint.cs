﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface IConstraint : IChildOfOwningTable
    {
        string ConstraintTypeTag { get; }
    }

    public interface IConstraintMutable : IConstraint, IChildOfOwningTableMutable { }

    /// <summary>
    /// Boilerplate implementation of IConstraintMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides.</item>
    ///   <list type="bullet">
    ///   <item>ValidOwnerTypesWhenNotOwner, occasionally if not abstract type
    ///   and if Owner is not required to be ParentObject.</item>
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>SetOwner, rarely, e.g. when Owner should always be ParentObject 
    /// and vice versa.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>ParentObjectMustBeOwner, if true rather than false.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaMutable method overrides
    ///   <list type="bullet">
    ///   <item>SetParentObject, rarely, e.g. when Owner should always be ParentObject 
    /// and vice versa.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public abstract class ConstraintMutable : ChildOfOwningTableMutable, IConstraintMutable
    {

        private string FullTypeNameForCodeErrors { get { return typeof(ConstraintMutable).FullName; } }

        protected ConstraintMutable() : base() { }

        public ConstraintMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , IDataProcObject owner = null
            , string nameWhenPrefixed = null
            , int? id = null
            ) : base(
                name
                , sqlTranslator
                , table
                , objectType
                , physicalObjectType
                , owner != null ? owner : table
                , nameWhenPrefixed
                , id
            )
        { }

        // DataProcObjectMutable method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (IsFullyConstructed)
            {
                Debug.Assert(base.PassesSelfAssertion());
                if (ObjectType != null)
                    Debug.Assert(
                        ObjectType.Equals(ObjectTypes.CheckConstraint())
                        || ObjectType.Equals(ObjectTypes.DefaultConstraint())
                        || ObjectType.Equals(ObjectTypes.ForeignKey())
                        || ObjectType.Equals(ObjectTypes.PrimaryKey())
                        || ObjectType.Equals(ObjectTypes.UniqueKey())
                        , "The only valid object types for an IConstraint object "
                        + "are CheckConstraint, DefaultConstraint, ForeignKey, PrimaryKey, "
                        + "and UniqueKey."
                        );
                if (PhysicalObjectType != null)
                    Debug.Assert(
                        PhysicalObjectType.Equals(ObjectTypes.CheckConstraint())
                        || PhysicalObjectType.Equals(ObjectTypes.DefaultConstraint())
                        || PhysicalObjectType.Equals(ObjectTypes.ForeignKey())
                        || PhysicalObjectType.Equals(ObjectTypes.PrimaryKey())
                        || PhysicalObjectType.Equals(ObjectTypes.UniqueKey())
                        , "The only valid physical object types for an IConstraint object "
                        + "are CheckConstraint, DefaultConstraint, ForeignKey, PrimaryKey, "
                        + "and UniqueKey."
                        );
            }
            return true;
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , vable: null
                , objectType: null
                , physicalObjectType: null
                , owner: null
                , nameWhenPrefixed: null
                , id: default(int?)
                );
        }

        protected override void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator
            , IVable vable
            , IObjectType objectType
            , IObjectType physicalObjectType
            , IDataProcObject owner
            , string nameWhenPrefixed
            , int? id
            )
        {
            base.ConstructAfterConstruction(
                name
                , sqlTranslator
                , vable
                , objectType
                , physicalObjectType
                , owner
                , nameWhenPrefixed
                , id
                );
        }

        // IConstraint methods

        public virtual string ConstraintTypeTag
        {
            get
            {
                if (PhysicalObjectType.Equals(ObjectTypes.CheckConstraint()))
                    return "_ck";
                else if (PhysicalObjectType.Equals(ObjectTypes.DefaultConstraint()))
                    return "_d";
                else if (PhysicalObjectType.Equals(ObjectTypes.ForeignKey()))
                    return "_fk";
                else if (PhysicalObjectType.Equals(ObjectTypes.PrimaryKey()))
                    return "_pk";
                else if (PhysicalObjectType.Equals(ObjectTypes.UniqueKey()))
                    return "_u";
                else
                    throw new InvalidConstraintTypeException(
                        FullTypeNameForCodeErrors
                        , PhysicalObjectType.GetType().FullName
                        , "PhysicalObjectType"
                        , IsFullyConstructed ? Name : null
                        );
            }
        }

    }

    /// <summary>
    /// Boilerplate implementation of IConstraint without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public abstract class ConstraintImmutable : ChildOfOwningTableImmutable, IConstraint
    {

        private IConstraint Delegee { get { return (IConstraint)_delegee; } }

        protected ConstraintImmutable() : base() { }

        public ConstraintImmutable(
            IConstraint _delegee
            , bool assert = true
            ) : base( _delegee, false)
        {
            CompleteConstructionIfType(typeof(ConstraintImmutable), false, assert);
        }

        // IConstraintMutable methods

        public string ConstraintTypeTag { get { return ConstraintTypeTag; } }

    }

    public class InvalidConstraintTypeException : Exception
    {
        public InvalidConstraintTypeException(
            string typeName
            , string badObjectTypeName
            , string badPropertyName = "PhysicalObjectType"
            , string badObjectName = null
            , string validObjectTypes = "CheckConstraint, DefaultConstraint, ForeignKey, PrimaryKey and UniqueKey"
            ) : base(
                message:
                "Constraints of type " 
                + typeName != null ? typeName : "[unknown]"
                + badObjectName == null || badObjectName.Trim().Length == 0 ? "" : ", such as '" + badObjectName + "',"
                + " cannot have their "
                + badPropertyName != null ? badPropertyName : "PhysicalObjectType"
                + "properties set to "
                + badObjectTypeName != null ? badObjectTypeName : "[unknown]"
                + ", as the only valid object types for table constraints are "
                + validObjectTypes != null ? validObjectTypes : "CheckConstraint, DefaultConstraint, ForeignKey, PrimaryKey and UniqueKey"
                )
        {
            TypeName = typeName;
            BadObjectTypeName = badObjectTypeName;
            BadPropertyName = badPropertyName;
            BadObjectName = badObjectName;
            ValidObjectTypes = validObjectTypes;
        }
        public string TypeName { get; }
        public string BadObjectTypeName { get; }
        public string BadPropertyName { get; }
        public string BadObjectName { get; }
        public string ValidObjectTypes { get; }
    }

}
