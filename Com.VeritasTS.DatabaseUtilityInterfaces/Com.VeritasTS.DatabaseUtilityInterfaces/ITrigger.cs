﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Com.VeritasTS.TextAndErrorUtils;


namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public enum TriggerTimings
    {
        InsteadOf
            , Before
            , After
    }

    public enum TriggerActions
    {
        Insert
            , Update
            , Delete
    }

    //public class TriggerStoredProcedureParametersStringOtherThanDefaults
    //{
    //    // some parameters other than temptable(s) and tablename in the trigger creation code
    //    // eg. ", @param1 = 'value1', @param2 = 2"
    //    public string ParametersString { get; set; }
    //}
    public interface ITableTrigger : IChildOfOwningTable
    {
        TriggerTimings TriggerTiming { get; }
        bool ApplyToInsert { get; }
        bool ApplyToUpdate { get; }
        bool ApplyToDelete { get; }
        bool IsInsteadOfInsert { get; }
        bool IsInsteadOfUpdate { get; }
        bool IsInsteadOfDelete { get; }
        bool IsInsteadOfInsertUpdate { get; }
        bool IsInsteadOfAnything { get; }
        bool IsBeforeInsert { get; }
        bool IsBeforeUpdate { get; }
        bool IsBeforeDelete { get; }
        bool IsBeforeInsertUpdate { get; }
        bool IsBeforeAnything { get; }
        bool IsAfterInsert { get; }
        bool IsAfterUpdate { get; }
        bool IsAfterDelete { get; }
        bool IsAfterInsertUpdate { get; }
        bool IsAfterAnything { get; }
        string TimingTag { get; }
        string IUDTag { get; }
        string TriggerText { get; }

        void ExecuteLogic( IConnection conn);

    }
    public interface ITableTriggerMutable : ITableTrigger, IChildOfOwningTableMutable
    {
        void SetTriggerTiming(TriggerTimings value, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true);
        void SetApplyToInsert(bool value, bool assert = true);
        void SetApplyToUpdate(bool value, bool assert = true);
        void SetApplyToDelete(bool value, bool assert = true);
        void SetTriggerText(string value, bool assert = true);
    }

    /// <summary>
    /// Boilerplate implementation of ITableTriggerMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides.</item>
    ///   <list type="bullet">
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaMutable method overrides
    ///   <list type="bullet">
    ///   <item>ObjectIsValidTypeForParentObject, usually unless abstract type.</item>
    ///   <item>ParentObjectMustBeOwner, if true rather than false.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always.</item>
    ///   <item>SetParentObject, rarely, e.g. when ParentObject should always be some
    ///   member other than Owner and vice versa.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class TriggerMutable : ChildOfOwningTableMutable, ITableTriggerMutable
    {

        private TriggerTimings _triggerTiming;
        private bool _applyToInsert;
        private bool _applyToUpdate;
        private bool _applyToDelete;
        private string _triggerText;

        protected TriggerTimings Before { get { return TriggerTimings.Before; } }
        protected TriggerTimings After { get { return TriggerTimings.After; } }
        protected TriggerTimings InsteadOf { get { return TriggerTimings.InsteadOf; } }

        // Construction
        public TriggerMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , TriggerTimings triggerTiming = TriggerTimings.Before
            , bool applyToInsert = false
            , bool applyToUpdate = false
            , bool applyToDelete = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , sqlTranslator
                , table
                , ObjectTypes.Trigger()
                , ObjectTypes.Trigger()
                , table
                , nameWhenPrefixed
                , id
                )
        {
            SetTriggerTiming(triggerTiming, false, false);
            SetApplyToInsert(applyToInsert, false);
            SetApplyToUpdate(applyToUpdate, false);
            SetApplyToDelete(applyToDelete, false);
            SetTriggerText(null);
            Debug.Assert(PassesSelfAssertion(typeof(TriggerMutable)));
        }

        // Object method overrides and overridden GetHasCode assistants

        public override int GetHashCode()
        {
            int p1 = NextPrimes[base.HighestPrimeUsedInHashCode];
            int p2 = NextPrimes[p1];
            int p3 = NextPrimes[p2];
            int p4 = NextPrimes[p3];
            int p5 = NextPrimes[p4];
            return base.GetHashCode()
                + p1 * TriggerTiming.GetHashCode()
                + (ApplyToInsert ? p2 : 0)
                + (ApplyToUpdate ? p3 : 0)
                + (ApplyToDelete ? p4 : 0)
                + p5 * TriggerText.GetHashCode();
        }

        public override int HighestPrimeUsedInHashCode
        {
            get
            {
                int p1 = NextPrimes[base.HighestPrimeUsedInHashCode];
                int p2 = NextPrimes[p1];
                int p3 = NextPrimes[p2];
                int p4 = NextPrimes[p3];
                return NextPrimes[p4];
            }
        }

        // DataProcObjectMutable method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    TriggerText != null
                    , "TriggerText property of ITableTrigger logic "
                    + "cannot be null."
                    );
                Debug.Assert(
                    TriggerText.Length > 0
                    , "TriggerText property of ITableTrigger logic "
                    + "cannot be the empty string."
                    );
                Debug.Assert(
                    new Regex("\\S").IsMatch(TriggerText)
                    , "TriggerText property of ITableTrigger logic "
                    + "cannot be purely whitespace."
                    );
                Debug.Assert(
                    ApplyToInsert || ApplyToUpdate || ApplyToDelete
                    , "Trigger must apply to at least one of insert, "
                    + "update, or delete."
                    );
            }
            return true;
        }

        public override bool MembersMatch(object obj)
        {
            if (!base.MembersMatch(obj)) return false;
            if (!typeof(ITableTrigger).IsAssignableFrom(obj.GetType())) return false;
            ITableTrigger other = (ITableTrigger)obj;
            return
                TriggerTiming == other.TriggerTiming
                && ApplyToInsert == other.ApplyToInsert
                && ApplyToUpdate == other.ApplyToUpdate
                && ApplyToDelete == other.ApplyToDelete
                && TriggerText == other.TriggerText;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new TriggerMutable(
                Name
                , GetSqlTranslatorSafely()
                , GetTableSafely()
                , TriggerTiming
                , ApplyToInsert
                , ApplyToUpdate
                , ApplyToDelete
                , NameWhenPrefixed
                , ID
                , false
                , true
                );
        }

        // IObjectInSchema method overrides

        public override bool ParentObjectMustBeOwner { get { return true; } }

        public override IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            RejectIfInvalidParentObjectType(parentObject);
            bool retrieveIDs
                = retrieveIDsFromDatabase != null && retrieveIDsFromDatabase.HasValue
                ? retrieveIDsFromDatabase.Value
                : ID != null;
            return new TriggerMutable(
                    Name
                    , SqlTranslator
                    , (ITable)parentObject
                    , TriggerTiming
                    , ApplyToInsert
                    , ApplyToUpdate
                    , ApplyToDelete
                    , NameWhenPrefixed
                    , ID
                    , retrieveIDs
                    , assert
                    );
        }

        // ITableTrigger properties and methods

        public TriggerTimings TriggerTiming
        {
            get { return _triggerTiming; } 
            set { SetTriggerTiming(value); }
        }
        public bool ApplyToInsert { get { return _applyToInsert; } set { SetApplyToInsert(value); } }
        public bool ApplyToUpdate { get { return _applyToUpdate; } set { SetApplyToUpdate(value); } }
        public bool ApplyToDelete { get { return _applyToDelete; } set { SetApplyToDelete(value); } }
        public bool IsInsteadOfInsert { get { return (TriggerTiming.Equals(InsteadOf) && ApplyToInsert && !ApplyToUpdate && !ApplyToDelete); } }
        public bool IsInsteadOfUpdate { get { return (TriggerTiming.Equals(InsteadOf) && !ApplyToInsert && ApplyToUpdate && !ApplyToDelete); } }
        public bool IsInsteadOfDelete { get { return (TriggerTiming.Equals(InsteadOf) && !ApplyToInsert && !ApplyToUpdate && ApplyToDelete); } }
        public bool IsInsteadOfInsertUpdate { get { return (TriggerTiming.Equals(InsteadOf) && ApplyToInsert && ApplyToUpdate && !ApplyToDelete); } }
        public bool IsInsteadOfAnything { get { return (TriggerTiming.Equals(InsteadOf) && ApplyToInsert && ApplyToUpdate && ApplyToDelete); } }
        public bool IsBeforeInsert { get { return (TriggerTiming.Equals(Before) && ApplyToInsert && !ApplyToUpdate && !ApplyToDelete); } }
        public bool IsBeforeUpdate { get { return (TriggerTiming.Equals(Before) && !ApplyToInsert && ApplyToUpdate && !ApplyToDelete); } }
        public bool IsBeforeDelete { get { return (TriggerTiming.Equals(Before) && !ApplyToInsert && !ApplyToUpdate && ApplyToDelete); } }
        public bool IsBeforeInsertUpdate { get { return (TriggerTiming.Equals(Before) && ApplyToInsert && ApplyToUpdate && !ApplyToDelete); } }
        public bool IsBeforeAnything { get { return (TriggerTiming.Equals(Before) && ApplyToInsert && ApplyToUpdate && ApplyToDelete); } }
        public bool IsAfterInsert { get { return (TriggerTiming.Equals(After) && ApplyToInsert && !ApplyToUpdate && !ApplyToDelete); } }
        public bool IsAfterUpdate { get { return (TriggerTiming.Equals(After) && !ApplyToInsert && ApplyToUpdate && !ApplyToDelete); } }
        public bool IsAfterDelete { get { return (TriggerTiming.Equals(After) && !ApplyToInsert && !ApplyToUpdate && ApplyToDelete); } }
        public bool IsAfterInsertUpdate { get { return (TriggerTiming.Equals(After) && ApplyToInsert && ApplyToUpdate && !ApplyToDelete); } }
        public bool IsAfterAnything { get { return (TriggerTiming.Equals(After) && ApplyToInsert && ApplyToUpdate && ApplyToDelete); } }
        public string TimingTag
        {
            get
            {
                TriggerTimings timing = TriggerTiming;
                if (timing.Equals(Before))
                    return "_b";
                else if (timing.Equals(After))
                    return "_a";
                else return "_io";
            }
        }
        public string IUDTag
        {
            get
            {
                return
                    (ApplyToInsert ? "I" : "")
                    + (ApplyToUpdate ? "U" : "")
                    + (ApplyToDelete ? "D" : "");
            }
        }
        public string TriggerText
        {
            get { return _triggerText; }
            set { SetTriggerText(value); }
        }

        public void ExecuteLogic(IConnection conn) { }

        /****************
         * ITableTriggerMutable methods
        ****************/

        public void SetTriggerTiming(TriggerTimings value, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            _triggerTiming = value;
            string currentName = Name;
            bool retrieveIDs = false;
            if (
                currentName == null
                || new Regex("_((io)|[ab])(IUD){1,3}\\s*$").IsMatch(currentName)
                )
            {
                retrieveIDs
                    = retrieveIDsFromDatabase == null || !retrieveIDsFromDatabase.HasValue
                    ? true
                    : retrieveIDsFromDatabase.Value;
                SetName(
                    ParentObject.Name.TrimEnd() + TimingTag + IUDTag
                    , false
                    );
            }
            if (retrieveIDs) RetrieveIDsFromDatabase(false);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }
        public void SetApplyToInsert(bool value, bool assert = true) { _applyToInsert = value; }
        public void SetApplyToUpdate(bool value, bool assert = true) { _applyToUpdate = value; }
        public void SetApplyToDelete(bool value, bool assert = true) { _applyToDelete = value; }

        public void SetTriggerText(string value, bool assert = true)
        {
            if (_triggerText != null) _triggerText = value;
            else
                _triggerText = "@exec SP_TriggerLauncher '" + NameWithSchema + "';";
            if (assert) Debug.Assert(PassesSelfAssertion());
        }
    }
}
