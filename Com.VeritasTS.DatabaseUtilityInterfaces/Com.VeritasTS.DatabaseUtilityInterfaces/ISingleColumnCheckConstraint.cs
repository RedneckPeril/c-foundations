﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{
    public interface ISingleColumnCheckConstraint : ICheckConstraint, ISingleColumnConstraint
    {
    }

    public interface ISingleColumnCheckConstraintMutable : ISingleColumnCheckConstraint, ICheckConstraintMutable, ISingleColumnConstraintMutable
    {
    }

    /// <summary>
    /// Boilerplate implementation of ISingleColumnCheckConstraintMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides.</item>
    ///   <list type="bullet">
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class SingleColumnCheckConstraintMutable : CheckConstraintMutable, ISingleColumnCheckConstraintMutable
    {

        private string FullNameForCodeErrors
        {
            get { return typeof(SingleColumnCheckConstraintMutable).FullName; }
        }

        // Construction
        public SingleColumnCheckConstraintMutable() : base() { }

        public SingleColumnCheckConstraintMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , ITableColumn constrainedColumn = null
            , string constraintText = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , sqlTranslator
                , table
                , constrainedColumn
                , constraintText
                , nameWhenPrefixed
                , id
                )
        {
            CompleteConstructionIfType(typeof(SingleColumnCheckConstraintMutable), retrieveIDsFromDatabase, assert);
        }

        public SingleColumnCheckConstraintMutable(
            string name
            , ITableColumn constrainedColumn = null
            , string constraintText = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , constrainedColumn
                , constraintText
                , nameWhenPrefixed
                , id
                )
        {
            CompleteConstructionIfType(typeof(SingleColumnCheckConstraintMutable), retrieveIDsFromDatabase, assert);
        }

        public SingleColumnCheckConstraintMutable(
            ITableColumn constrainedColumn = null
            , string constraintText = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                constrainedColumn
                , constraintText
                , nameWhenPrefixed
                , id
                )
        {
            CompleteConstructionIfType(typeof(SingleColumnCheckConstraintMutable), retrieveIDsFromDatabase, assert);
        }

        // DataProcObjectMutable method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    ConstrainedColumn != null
                    , "In a single-column check constraint, the "
                    + "ConstrainedColumn property cannot be null."
                    );
            }
            return true;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new SingleColumnCheckConstraintMutable(
                Name
                , GetSqlTranslatorSafely()
                , GetTableSafely()
                , GetConstrainedColumnSafely()
                , ConstraintText
                , NameWhenPrefixed
                , ID
                , false
                , assert
                );
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , table: null
                , constrainedColumn: null
                , nameWhenPrefixed: null
                , id: default(int?)
                );
        }

        protected override void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator
            , ITable table
            , ITableColumn constrainedColumn
            , string constraintText
            , string nameWhenPrefixed
            , int? id
            )
        {
            base.ConstructAfterConstruction(
                name
                , sqlTranslator
                , table
                , constrainedColumn
                , constraintText
                , nameWhenPrefixed
                , id
                );
        }

        public virtual void ConstructNewMembers(bool forceUpdates) { }

        // ObjectInSchema method overrides

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            RejectIfInvalidParentObjectType(parentObject);
            bool retrieveIDs
                = retrieveIDsFromDatabase != null && retrieveIDsFromDatabase.HasValue
                ? retrieveIDsFromDatabase.Value
                : parentObject != null && parentObject.ID != null;
            return new SingleColumnCheckConstraintMutable(
                Name
                , SqlTranslator
                , (ITable)parentObject
                , (ITableColumn)ConstrainedColumn.GetCopyForVable(
                    (IVable)parentObject
                    , retrieveIDs
                    , false
                    )
                , ConstraintText
                , NameWhenPrefixed
                , ID
                , false
                , assert
                );
        }

    }

    /// <summary>
    /// Boilerplate implementation of IChildOfVable without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <item>SingleColumnCheckConstraintImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetCopyForColumn, always unless abstract class.</item>
    ///   <item>GetCopyForTable, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public class SingleColumnCheckConstraintImmutable : CheckConstraintImmutable, ISingleColumnCheckConstraint
    {

        private ISingleColumnCheckConstraint Delegee { get { return (ISingleColumnCheckConstraint)_delegee; } }

        public SingleColumnCheckConstraintImmutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , ITableColumn constrainedColumn = null
            , string constraintText = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
        {
            _delegee
                = new SingleColumnCheckConstraintMutable(
                    name
                    , sqlTranslator == null ? null : (ISqlTranslator)sqlTranslator.GetSafeReference()
                    , table == null ? null : (ITable)table.GetSafeReference()
                    , constrainedColumn == null ? null : (ITableColumn)constrainedColumn.GetSafeReference()
                    , constraintText
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    );
            CompleteConstructionIfType(typeof(SingleColumnCheckConstraintImmutable), false, assert);
        }

        public SingleColumnCheckConstraintImmutable(
            string name
            , ITableColumn constrainedColumn = null
            , string constraintText = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
        {
            _delegee
                = new SingleColumnCheckConstraintMutable(
                    name
                    , constrainedColumn == null ? null : (ITableColumn)constrainedColumn.GetSafeReference()
                    , constraintText
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    );
            CompleteConstructionIfType(typeof(SingleColumnCheckConstraintImmutable), false, assert);
        }

        public SingleColumnCheckConstraintImmutable(
            ITableColumn constrainedColumn = null
            , string constraintText = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
        {
            _delegee
                = new SingleColumnCheckConstraintMutable(
                    constrainedColumn == null ? null : (ITableColumn)constrainedColumn.GetSafeReference()
                    , constraintText
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    );
            CompleteConstructionIfType(typeof(SingleColumnCheckConstraintImmutable), false, assert);
        }

        public SingleColumnCheckConstraintImmutable(ISingleColumnCheckConstraint delegee, bool assert = true)
        {
            _delegee = delegee;
            CompleteConstructionIfType(typeof(SingleColumnCheckConstraintImmutable), false, assert);
        }

        // DataProcObjectImmutable method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(ISingleColumnCheckConstraint); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new SingleColumnCheckConstraintImmutable(
                (ISingleColumnCheckConstraint)
                Delegee.GetReferenceForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new SingleColumnCheckConstraintImmutable(
                (ISingleColumnCheckConstraint)
                Delegee.GetCopyForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }

        // PotentiallySingleColumnConstraintImmutable method overrides

        public override IPotentiallySingleColumnConstraint GetCopyForColumn(ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return new SingleColumnCheckConstraintImmutable(
                (ISingleColumnCheckConstraint)
                Delegee.GetCopyForColumn(
                    constrainedColumn
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }
        public override IPotentiallySingleColumnConstraint GetCopyForTable(ITable table, ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return new SingleColumnCheckConstraintImmutable(
                (ISingleColumnCheckConstraint)
                Delegee.GetCopyForTable(
                    table
                    , constrainedColumn
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }

    }

}

