﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.VeritasTS.TextAndErrorUtils;


namespace Com.VeritasTS.DatabaseUtilityInterfaces
{
    public interface ITableInputter : 
        ISelfAsserter
        , IMemberMatcher
        , ISafeMemberOfImmutableOwner
        , IComparableByName
        , IComparableByFullName
    {
    }

    public interface ITableInputterMutable: ITableInputter { }

    public class TableInputterMutable: ComparableByNameMutable, ITableInputterMutable
    {

        private TableInputterMutable(): base("") { }

        public TableInputterMutable(
            string name
            ): base( name )
        {
        }

        // Object method overrides
        public override bool Equals(object obj)
        {
            return EqualityUtilities.MemberMatchwiseEqualOrBothNull(this, obj);
        }

        public override int GetHashCode()
        {
            return (Name == null ? 0 : Name.GetHashCode());
        }

        // ISelfAsserter methods
        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert( base.PassesSelfAssertion());
            }
            return true;
        }

        public bool SkipSelfAssertionUnlessThisType(Type testableType = null)
        {
            if (testableType == null) return false;
            return (!testableType.Equals(GetType()));
        }

        // IMethodMatcher methods
        public virtual bool MembersMatch(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(IDatabase).IsAssignableFrom(obj.GetType())) return false;
            IDatabase other = (IDatabase)obj;
            return EqualityUtilities.EqualOrBothNull(this.Name, other.Name);
        }

        // ISafeMemberOfImmutableOwner methods
        public virtual ISafeMemberOfImmutableOwner GetSafeReference()
        {
            return new TableInputterMutable(Name);
        }

        // IComparableByFullName methods and properties

        public virtual string FullName {  get { return Name; } }

        public int CompareByFullNameTo(IComparableByFullName other)
        {
            return ComparableByFullNameMutable.CompareByFullName(this, other);
        }
        public bool EqualsInFullName(IComparableByFullName other)
        {
            return ComparableByFullNameMutable.AreEqualInFullName(this, other);
        }
    }
}
