﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{
    public interface ICascadingForeignKeyRelationship : ISelfAsserter, IMemberMatcher, ISafeMemberOfImmutableOwner
    {
        string MasterDataTableName { get; }
        string MasterDataTableColumnList { get; }
        int PlaceInDeleteSequence { get; }
        string ConstrainedTableName { get; }
        string ConstrainedTableColumnList { get; }
        bool CascadeOnDelete { get; }
        bool CascadeOnUpdate { get; }
    }
    public sealed class CascadingForeignKeyRelationship : ICascadingForeignKeyRelationship
    {
        private CascadingForeignKeyRelationship() { }

        public CascadingForeignKeyRelationship(
            string masterDataTableName
            , string masterDataTableColumnList
            , int placeInDeleteSequence
            , string constrainedTableName
            , string constrainedTableColumnList
            , bool cascadeOnDelete
            , bool cascadeOnUpdate
            )
        {
            MasterDataTableName = masterDataTableName;
            MasterDataTableColumnList = masterDataTableColumnList;
            PlaceInDeleteSequence = placeInDeleteSequence;
            ConstrainedTableName = constrainedTableName;
            ConstrainedTableColumnList = constrainedTableColumnList;
            CascadeOnDelete = cascadeOnDelete;
            CascadeOnUpdate = cascadeOnUpdate;
        }

        // Object method overrides

        /// <summary>
        /// Overriden Object.Equals object.
        /// </summary>
        /// <remarks>
        /// Should not be overridden by descendant classes.
        /// </remarks>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return EqualityUtilities.MemberMatchwiseEqualOrBothNull(this, obj);
        }

        /// <summary>
        /// Overriden Object.GetHashCode object.
        /// </summary>
        /// <remarks>
        /// Should be overridden by all descendant classes,
        /// and only those descendant classes, that add
        /// new members into MemberMatchwiseEqualOrBothNull.
        /// The general rule is that each new member's hash code
        /// is added to the hash code that would be generated
        /// by the nearest ancestor, after having been multiplied
        /// by a prime number not used by other members.
        /// Since descendants will need to know what
        /// prime numbers have already been used, each
        /// class must also publish its HighestPrimeUsedInHashCode.
        /// The descendant class then starts using prime numbers
        /// starting with NextPrimes[base.HighestPrimeUsedInHashCode].
        /// </remarks>
        /// <param name="obj">The object to which this is
        /// being compared for equality.</param>
        /// <returns>The object's hash code.</returns>
        public override int GetHashCode()
        {
            return MasterDataTableName.GetHashCode()
                + 3 * MasterDataTableColumnList.GetHashCode()
                + 5 * PlaceInDeleteSequence
                + 7 * ConstrainedTableName.GetHashCode()
                + 11 * ConstrainedTableColumnList.GetHashCode()
                + (CascadeOnDelete ? 13 : 0)
                + (CascadeOnUpdate ? 17 : 0);
        }

        //  ISelfAsserter methods

        public bool PassesSelfAssertion(Type testableType = null)
        {
            Debug.Assert(
                MasterDataTableName != null
                , "TriggerName property of CascadingForeignKeyRelationship cannot be null."
                );
            Debug.Assert(
                new Regex("\\S").IsMatch(MasterDataTableName)
                , "TriggerName property of CascadingForeignKeyRelationship must contain "
                + "something other than whitespace."
                );
            Debug.Assert(
                MasterDataTableColumnList != null
                , "Trigger property of CascadingForeignKeyRelationship cannot be null."
                );
            Debug.Assert(
                new Regex("\\S").IsMatch(MasterDataTableColumnList)
                , "Trigger property of CascadingForeignKeyRelationship must contain "
                + "something other than whitespace."
                );
            Debug.Assert(
                ConstrainedTableName != null
                , "ConstrainedTableName property of CascadingForeignKeyRelationship cannot be null."
                );
            Debug.Assert(
                new Regex("\\S").IsMatch(ConstrainedTableName)
                , "ConstrainedTableName property of CascadingForeignKeyRelationship must contain "
                + "something other than whitespace."
                );
            Debug.Assert(
                ConstrainedTableColumnList != null
                , "ConstrainedTableColumnList property of CascadingForeignKeyRelationship cannot be null."
                );
            Debug.Assert(
                new Regex("\\S").IsMatch(ConstrainedTableColumnList)
                , "ConstrainedTableColumnList property of CascadingForeignKeyRelationship must contain "
                + "something other than whitespace."
                );
            Debug.Assert(
                CascadeOnDelete || CascadeOnUpdate
                , "A cascading foreign key relationship has to cascade on something -- "
                + "on update, on delete, or on both, but not on neither."
                );
            return true;
        }

        // IMemberMatcher methods
        public bool MembersMatch(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(CascadingForeignKeyRelationship).IsAssignableFrom(obj.GetType())) return false;
            CascadingForeignKeyRelationship other = (CascadingForeignKeyRelationship)obj;
            return
                MasterDataTableName == other.MasterDataTableName
                && MasterDataTableColumnList == other.MasterDataTableColumnList
                && PlaceInDeleteSequence == other.PlaceInDeleteSequence
                && ConstrainedTableName == other.ConstrainedTableName
                && ConstrainedTableColumnList == other.ConstrainedTableColumnList
                && CascadeOnDelete == other.CascadeOnDelete
                && CascadeOnUpdate == other.CascadeOnUpdate;
        }

        // ISafeMemberOfImmutableOwner methods

        public ISafeMemberOfImmutableOwner GetSafeReference()
        {
            return GetSafeReference(assert: true);
        }

        public ISafeMemberOfImmutableOwner GetSafeReference(bool assert)
        {
            if (assert)
                Debug.Assert(PassesSelfAssertion());
            return this;
        }

        // CascadingForeignKeyRelationship properties

        public string MasterDataTableName { get; }
        public string MasterDataTableColumnList { get; }
        public int PlaceInDeleteSequence { get; }
        public string ConstrainedTableName { get; }
        public string ConstrainedTableColumnList { get; }
        public bool CascadeOnDelete { get; }
        public bool CascadeOnUpdate { get; }
    }
}
