﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface ICommand : ISelfAsserter, IDisposable
    {
        object NativeCommand { get; }
        string CommandText { get; }
        IConnection Connection { get; }
        ITransaction Transaction { get; }
        IDataReader ExecuteReader();
        void ExecuteNonQuery();
        object ExecuteSingleValue();
    }

    // Note: we don't have a default implementation of this because
    //   any implementation is vendor-specific.

}
