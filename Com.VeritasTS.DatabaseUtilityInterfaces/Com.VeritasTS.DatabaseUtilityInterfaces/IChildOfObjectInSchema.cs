﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// Base interface for database objects whose
    /// ParentObject is an IObjectInSchema.
    /// </summary>
    /// <remarks>
    /// Extends IObjectInSchema.
    /// </remarks>
    public interface IChildOfObjectInSchema : IObjectInSchema { }

    /// <summary>
    /// Base interface for database objects whose
    /// ParentObject is an IObjectInSchema and that can be altered
    /// after construction.
    /// </summary>
    /// <remarks>
    /// Merely adds variability to IChildOfObjectInSchema.
    /// <para>
    /// Extends IObjectInSchemaMutable.</para>
    /// </remarks>
    public interface IChildOfObjectInSchemaMutable : IChildOfObjectInSchema, IObjectInSchemaMutable { }

    /// <summary>
    /// Boilerplate implementation of IChildOfObjectInSchemaMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be protected, and simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>Protected ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides.</item>
    ///   <list type="bullet">
    ///   <item>GetCopyForOwner, always if not abstract type.</item>
    ///   <item>ValidOwnerTypesWhenNotOwner, occasionally if not abstract type
    ///   and if Owner is not required to be ParentObject.</item>
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>SetOwner, rarely, e.g. when Owner should always be ParentObject 
    /// and vice versa.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>ValidParentObjectTypes, usually unless abstract type.</item>
    ///   <item>ParentObjectMustBeOwner, if true rather than false.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaMutable method overrides
    ///   <list type="bullet">
    ///   <item>SetParentObject, rarely, e.g. when Owner should always be ParentObject 
    /// and vice versa.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    abstract public class ChildOfObjectInSchemaMutable : ObjectInSchemaMutable, IChildOfObjectInSchemaMutable
    {

        private IDatabaseObject _parentObject;

        protected ChildOfObjectInSchemaMutable() : base() { }

        public ChildOfObjectInSchemaMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , IDatabaseObject parentObject = null
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , IDataProcObject owner = null
            , string nameWhenPrefixed = null
            , int? id = null
            ) : base(
                name
                , sqlTranslator
                , parentObject
                , objectType
                , physicalObjectType
                , owner
                , nameWhenPrefixed
                , id
            )
        { }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , parentObject: null
                , objectType: null
                , physicalObjectType: null
                , owner: null
                , nameWhenPrefixed: null
                , id: default(int?)
                );
        }

        protected virtual void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator
            , IDatabaseObject parentObject
            , IObjectType objectType
            , IObjectType physicalObjectType
            , IDataProcObject owner
            , string nameWhenPrefixed
            , int? id
            )
        {
            base.ConstructAfterConstruction(
                name
                , sqlTranslator
                , parentObject
                , objectType
                , physicalObjectType
                , owner
                , nameWhenPrefixed
                , id
                );
        }

    }

    /// <summary>
    /// Boilerplate implementation of IChildOfObjectInSchema without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    abstract public class ChildOfObjectInSchemaImmutable : ObjectInSchemaImmutable, IChildOfObjectInSchema
    {

        private IChildOfObjectInSchema Delegee { get { return (IChildOfObjectInSchema)_delegee; } }

        protected ChildOfObjectInSchemaImmutable() : base() { }

        public ChildOfObjectInSchemaImmutable(
            IChildOfObjectInSchema _delegee
            , bool assert = true
            ) : base( _delegee, false)
        {
            CompleteConstructionIfType(typeof(ChildOfObjectInSchemaImmutable), false, assert);
        }

        // IObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForSchema(ISchema schema, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return GetReferenceForParentObject(
                ((IObjectInSchema)ParentObject).GetReferenceForSchema(schema, retrieveIDsFromDatabase: false, assert: false)
                , retrieveIDsFromDatabase
                , assert
                );
        }
        public override IObjectInSchema GetCopyForSchema(ISchema schema, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return GetCopyForParentObject(
                ((IObjectInSchema)ParentObject).GetCopyForSchema(schema, retrieveIDsFromDatabase: false, assert: false)
                , retrieveIDsFromDatabase
                , assert
                );
        }

    }

}
