﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface ILastUpdaterColumn : ITableColumn { }
    public interface ILastUpdaterColumnMutable : ILastUpdaterColumn, ITableColumnMutable { }

    /// <summary>
    /// Boilerplate implementation of ILastUpdaterColumnMutable.
    /// </summary>
    /// <remarks>
    /// Sealed class for now.
    /// <remarks>
    public sealed class LastUpdaterColumnMutable : TableColumnMutable, ILastUpdaterColumnMutable
    {

        public LastUpdaterColumnMutable(
            ISqlTranslator sqlTranslator
            , ITable table
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
            : base(
                  "lastUpdater"
                  , "nvarchar(100)"
                  , sqlTranslator
                  , table: table
                  , isIdentity: false
                  , isNullable: true
                  , nameWhenPrefixed: "LastUpdater"
                  , constraintTag: "LU"
                  , id: id
                  , retrieveIDsFromDatabase: false
                  )
        {
            if (typeof(ITable).IsAssignableFrom(table.GetType()))
                AddOrOverwriteCheckConstraint(
                    new SingleColumnCheckConstraintMutable(
                        name: Database.SqlTranslator.TableConstraintName("_ckLU", ParentObject.Name)
                        , sqlTranslator: SqlTranslator
                        , table: (ITable)ParentObject
                        , constraintText: "lastUpdater is not null"
                        , constrainedColumn: this
                        , id: null
                        , retrieveIDsFromDatabase: false
                        , assert: false
                        )
                       );
            CompleteConstructionIfType(typeof(LastUpdaterColumnMutable), retrieveIDsFromDatabase, assert);
        }

        // IDataProcObject method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    "lastUpdater".Equals(Name)
                    , "A LastUpdaterColumn must be named 'lastUpdater', not "
                    + (Name == null ? "[null]" : "'" + Name + "'")
                    + "."
                    );
                Debug.Assert(
                    "nvarchar(100)".Equals(DataType)
                    , "A LastUpdaterColumn must be typed as nvarchar(100), not "
                    + (DataType == null ? "[null]" : DataType)
                    + "."
                    );
                Debug.Assert(
                    !IsIdentity
                    , "A LastUpdaterColumn cannot be an identity column."
                    );
                Debug.Assert(
                    IsNullable
                    , "A LastUpdaterColumn must be nullable (though constrained non-null)."
                    );
                Debug.Assert(
                    !IsComputed
                    , "A LastUpdaterColumn cannot be a computed column."
                    );
                Debug.Assert(
                    CalcFormula == null
                    , "A LastUpdaterColumn cannot be a computed column "
                    + "and therefore cannot have a computed definition."
                    );
                Debug.Assert(
                    !IsToBeTrimmed
                    , "A LastUpdaterColumn cannot be a trimmed column."
                    );
                Debug.Assert(
                    !IsImmutable
                    , "A LastUpdaterColumn cannot be an invariant column."
                    );
                Debug.Assert(
                    !IsPersisted
                    , "A LastUpdaterColumn cannot be a computed column "
                    + "and therefore cannot be flagged as persistent."
                    );
                Debug.Assert(
                    DefaultConstraint == null
                    , "A LastUpdaterColumn cannot have a default constraint."
                    );
                if (typeof(ITable).IsAssignableFrom(Owner.GetType()))
                {
                    Debug.Assert(
                        CheckConstraints != null
                        , "A LastUpdaterColumn must have at least one check constraint, "
                        + "enforcing non-nullability."
                        );
                    Debug.Assert(
                        CheckConstraints.Values.Where(c => c.ConstraintText.Equals("lastUpdater is not null")).Count() > 0
                        , "At least one of the check constraints on a LastUpdaterColumn "
                        + "must enforce non-nullability."
                        );
                }
            }
            return true;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new LastUpdaterColumnMutable(
                GetSqlTranslatorSafely()
                , GetTableSafely()
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        // ObjectInSchemaMutable method overrides
        ///   <list type="bullet">
        ///   <item>SetParentObject, if (as is usually the case) ParentObject
        /// is constrained to be an instance of a particular object type.</item>
        ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
        /// added as members.</item>
        ///   <item>GetCopyForParentObject
        ///   

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            if (!typeof(ITable).IsAssignableFrom(parentObject.GetType()))
                throw new BadParentObjectTypeException(
                    this
                    , this.GetType().FullName
                    , parentObject.GetType().FullName
                    , typeof(ITable).FullName
                    );
            return new LastUpdaterColumnMutable(
               SqlTranslator
                , (ITable)parentObject
                , ID
                , retrieveIDsFromDatabase
                , assert
                );
        }

    }

    /// <summary>
    /// Boilerplate implementation of ITableColumn without variability.
    /// </summary>
    /// <remarks>
    /// Sealed class for now.
    /// <remarks>
    public sealed class LastUpdaterColumnImmutable : TableColumnImmutable, ILastUpdaterColumn
    {

        private ILastUpdaterColumn Delegee { get { return (ILastUpdaterColumn)_delegee; } }

        public LastUpdaterColumnImmutable(
            ISqlTranslator sqlTranslator
            , ITable table
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
            : base()
        {
            _delegee = new LastUpdaterColumnMutable(
                sqlTranslator
                , table
                , id
                , retrieveIDsFromDatabase
                , assert: false
                );
            CompleteConstructionIfType(
                typeof(LastUpdaterColumnImmutable)
                , retrieveIDsFromDatabase: false
                , assert: false
                );
        }

        public LastUpdaterColumnImmutable(ILastUpdaterColumn delegee, bool assert)
            : base(delegee)
        {
            CompleteConstructionIfType(
                typeof(LastUpdaterColumnImmutable)
                , retrieveIDsFromDatabase: false
                , assert: false
                );
        }

        // DataProcObject method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(ILastUpdaterColumn); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new LastUpdaterColumnImmutable(
                (ILastUpdaterColumn)
                Delegee.GetReferenceForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , assert: false
                    )
                , assert: assert
                );
        }

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new LastUpdaterColumnImmutable(
                (ILastUpdaterColumn)
                Delegee.GetCopyForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , assert: false
                    )
                , assert: assert
                );
        }

    }

}