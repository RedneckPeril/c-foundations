﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface IParentPassThroughInfo: ISelfAsserter, IMemberMatcher, ISafeMemberOfImmutableOwner
    {
        IColumn ParentPassthroughColumn { get; }
        ITable ParentPassthroughSourceTable { get; }
        IColumn ParentPassthroughSourceColumn { get; }
        IColumn ParentPassthroughMatchingSourceColumn { get; }
        object ParentPassthroughLookup { get; }
        string ParentPassthroughValue { get; }
        IColumn GetParentPassthroughColumnSafely();
        ITable GetParentPassthroughSourceTableSafely();
        IColumn GetParentPassthroughSourceColumnSafely();
        IColumn GetParentPassthroughMatchingSourceColumnSafely();
        object GetParentPassthroughLookupSafely();
        void CompleteConstruction(Type t);
    }

    public interface IParentPassThroughInfoMutable: IParentPassThroughInfo
    {
        void SetParentPassthroughColumn(IColumn value, bool assert = true);
        void SetParentPassthroughSourceTable(ITable value, bool assert = true);
        void SetParentPassthroughSourceColumn(IColumn value, bool assert = true);
        void SetParentPassthroughMatchingSourceColumn(IColumn value, bool assert = true);
        void SetParentPassthroughLookup(object value, bool assert = true);
        void SetParentPassthroughValue(string value, bool assert = true);
    }
    public class ParentPassthroughInfoMutable : IParentPassThroughInfoMutable
    {
        private bool _isFullyConstructed = false;
        private IColumn _parentPassthroughColumn;
        private ITable _parentPassthroughSourceTable;
        private IColumn _parentPassthroughSourceColumn;
        private IColumn _parentPassthroughMatchingSourceColumn;
        private object _parentPassthroughLookup;
        private string _parentPassthroughValue;

        // Constructors
        public ParentPassthroughInfoMutable(
            IColumn parentPassthroughColumn
            , ITable parentPassthroughSourceTable
            , IColumn parentPassthroughSourceColumn
            , IColumn parentPassthroughMatchingSourceColumn
            , object parentPassthroughLookup
            , string parentPassthroughValue
            )
        {
            _isFullyConstructed = false;
            SetParentPassthroughColumn(parentPassthroughColumn);
            SetParentPassthroughSourceTable(_parentPassthroughSourceTable);
            SetParentPassthroughSourceColumn(_parentPassthroughSourceColumn);
            SetParentPassthroughMatchingSourceColumn(_parentPassthroughMatchingSourceColumn);
            SetParentPassthroughLookup(_parentPassthroughLookup);
            SetParentPassthroughValue(_parentPassthroughValue);
            CompleteConstruction(typeof(ParentPassthroughInfoMutable));
        }

        // Object method overrides
        public override bool Equals(object obj)
        {
            return EqualityUtilities.MemberMatchwiseEqualOrBothNull(this, obj);
        }

        public override int GetHashCode()
        {
            return (ParentPassthroughColumn == null ? 0 : ParentPassthroughColumn.GetHashCode())
                + 7 * (ParentPassthroughSourceTable == null ? 0 : ParentPassthroughSourceTable.GetHashCode())
                + 11 * (ParentPassthroughSourceColumn == null ? 0 : ParentPassthroughSourceColumn.GetHashCode())
                + 13 * (ParentPassthroughMatchingSourceColumn == null ? 0 : ParentPassthroughMatchingSourceColumn.GetHashCode())
                + 17 * (ParentPassthroughLookup == null ? 0 : ParentPassthroughLookup.GetHashCode())
                + 19 * (ParentPassthroughValue == null ? 0 : ParentPassthroughValue.GetHashCode());
        }

        // ISelfAsserter methods
        public virtual bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                if (ParentPassthroughColumn != null) Debug.Assert(ParentPassthroughColumn.PassesSelfAssertion());
                if (ParentPassthroughSourceTable != null) Debug.Assert(ParentPassthroughSourceTable.PassesSelfAssertion());
                if (ParentPassthroughSourceColumn != null) Debug.Assert(ParentPassthroughSourceColumn.PassesSelfAssertion());
                if (ParentPassthroughMatchingSourceColumn != null) Debug.Assert(ParentPassthroughMatchingSourceColumn.PassesSelfAssertion());
                if (ParentPassthroughLookup != null)
                {
                    if (typeof(ISelfAsserter).IsAssignableFrom(ParentPassthroughLookup.GetType()))
                        Debug.Assert(((ISelfAsserter)ParentPassthroughLookup).PassesSelfAssertion());
                    else
                        Debug.Assert(
                            typeof(string).Equals(ParentPassthroughLookup.GetType())
                            , "ParentPassthroughLookup property of a ParentPassthroughInfo object "
                            + "must be either a string or an ISafeMemberOfImmutableObject object."
                            );
                }
            }
            return true;
        }


        public bool SkipSelfAssertionUnlessThisType(Type testableType = null)
        {
            if (testableType == null) return false;
            return (!testableType.Equals(this.GetType()));
        }

        // IMethodMatcher methods
        public virtual bool MembersMatch(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(IParentPassThroughInfo).IsAssignableFrom(obj.GetType())) return false;
            IParentPassThroughInfo other = (IParentPassThroughInfo)obj;
            if (!EqualityUtilities.EqualOrBothNull(this.ParentPassthroughColumn, other.ParentPassthroughColumn)) return false;
            if (!EqualityUtilities.EqualOrBothNull(this.ParentPassthroughSourceTable, other.ParentPassthroughSourceTable)) return false;
            if (!EqualityUtilities.EqualOrBothNull(this.ParentPassthroughSourceColumn, other.ParentPassthroughSourceColumn)) return false;
            if (!EqualityUtilities.EqualOrBothNull(this.ParentPassthroughMatchingSourceColumn, other.ParentPassthroughMatchingSourceColumn)) return false;
            if (!EqualityUtilities.EqualOrBothNull(this.ParentPassthroughLookup, other.ParentPassthroughLookup)) return false;
            if (!EqualityUtilities.EqualOrBothNull(this.ParentPassthroughValue, other.ParentPassthroughValue)) return false;
            return true;
        }

        // ISafeMemberOfImmutableOwner methods
        public virtual ISafeMemberOfImmutableOwner GetSafeReference()
        {
            return new ParentPassthroughInfoMutable(
                GetParentPassthroughColumnSafely()
                , GetParentPassthroughSourceTableSafely()
                , GetParentPassthroughSourceColumnSafely()
                , GetParentPassthroughMatchingSourceColumnSafely()
                , GetParentPassthroughLookupSafely()
                , ParentPassthroughValue
                );
        }

        // IParentPassthroughInfo properties and methods

        public IColumn ParentPassthroughColumn
        {
            get { return _parentPassthroughColumn; }
            set { SetParentPassthroughColumn(value); }
        }
        public ITable ParentPassthroughSourceTable
        {
            get { return _parentPassthroughSourceTable; }
            set { SetParentPassthroughSourceTable(value); }
        }
        public IColumn ParentPassthroughSourceColumn
        {
            get { return _parentPassthroughSourceColumn; }
            set { SetParentPassthroughSourceColumn(value); }
        }
        public IColumn ParentPassthroughMatchingSourceColumn
        {
            get { return _parentPassthroughMatchingSourceColumn; }
            set { SetParentPassthroughMatchingSourceColumn(value); }
        }
        public object ParentPassthroughLookup
        {
            get { return _parentPassthroughLookup; }
            set { SetParentPassthroughLookup(value); }
        }
        public string ParentPassthroughValue
        {
            get { return _parentPassthroughValue; }
            set { SetParentPassthroughValue(value); }
        }
        public IColumn GetParentPassthroughColumnSafely()
        {
            if (_parentPassthroughColumn == null) return null;
            else return (IColumn)_parentPassthroughColumn.GetSafeReference();
        }
        public ITable GetParentPassthroughSourceTableSafely()
        {
            if (_parentPassthroughSourceTable == null) return null;
            else return (ITable)_parentPassthroughSourceTable.GetSafeReference();
        }
        public IColumn GetParentPassthroughSourceColumnSafely()
        {
            if (_parentPassthroughSourceColumn == null) return null;
            else return (IColumn)_parentPassthroughSourceColumn.GetSafeReference();
        }
        public IColumn GetParentPassthroughMatchingSourceColumnSafely()
        {
            if (_parentPassthroughMatchingSourceColumn == null) return null;
            else return (IColumn)_parentPassthroughMatchingSourceColumn.GetSafeReference();
        }
        public object GetParentPassthroughLookupSafely()
        {
            if (_parentPassthroughLookup == null) return null;
            else if (typeof(ISafeMemberOfImmutableOwner).IsAssignableFrom(_parentPassthroughLookup.GetType()))
                return ((ISafeMemberOfImmutableOwner)_parentPassthroughLookup).GetSafeReference();
            else return _parentPassthroughLookup;
        }
        public void CompleteConstruction(Type t)
        {
            if (t.Equals(GetType()))
            {
                _isFullyConstructed = true;
                Debug.Assert(PassesSelfAssertion());
            }
        }

        // IParentPassthroughInfoMutable methods

        public void SetParentPassthroughColumn(IColumn value, bool assert = true)
        {
            _parentPassthroughColumn = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }
        public void SetParentPassthroughSourceTable(ITable value, bool assert = true)
        {
            _parentPassthroughSourceTable = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }
        public void SetParentPassthroughSourceColumn(IColumn value, bool assert = true)
        {
            _parentPassthroughSourceColumn = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }
        public void SetParentPassthroughMatchingSourceColumn(IColumn value, bool assert = true)
        {
            _parentPassthroughMatchingSourceColumn = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }
        public void SetParentPassthroughLookup(object value, bool assert = true)
        {
            _parentPassthroughLookup = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }
        public void SetParentPassthroughValue(string value, bool assert = true)
        {
            _parentPassthroughValue = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }
    }
}


