﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// This interface groups together the SQL data
    /// types for which IsIntType is true.
    /// </summary>
    public interface ISqlFloat : ISqlDataType { }

    /// <summary>
    /// A basic, immutable implementation of the IFloat interface.
    /// </summary>
    public sealed class SqlFloat : SqlDataType, ISqlFloat
    {

        /****************
         *  Private members
         ***************/

        private string FullTypeNameForCodeErrors { get { return typeof(SqlFloat).FullName; } }
        private static SqlFloat _standard;

        /****************
         *  ISelfAsserter methods via ISelfUsabilityAsserter
         ***************/

        /// <summary>
        /// Asserts the class invariants for this class.
        /// </summary>
        /// <returns>True, unless an assertion failure exception is thrown.</returns>
        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertion(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
            }
            return true;
        }

        /****************
         *  SqlDataType method overrides
         ***************/

        public override string DefaultSql { get { return "float"; } }

        public override bool IsFloatType { get { return true; } }

        /****************
         *  IFloat methods
         ***************/

        public static SqlFloat Standard()
        {
            if (_standard == null) _standard = new SqlFloat();
            return _standard;
        }

    }

}
