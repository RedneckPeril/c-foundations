﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface ITableAsSequence : ITable, IDatabaseSequence
    {
        IColumn DummyInsertColumn { get; }
    }

    public interface ITableAsSequenceMutable : ITableAsSequence, ITableMutable, IDatabaseSequenceMutable { }

    /// <summary>
    /// Boilerplate implementation of ITableAsSequenceMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    ///   affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidOwnerTypesWhenNotOwner, occasionally if not abstract type
    ///   and if Owner is not required to be ParentObject.</item>
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>SetOwner, rarely, e.g. when Owner should always be ParentObject 
    /// and vice versa.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaMutable method overrides
    ///   <list type="bullet">
    ///   <item>ParentObjectMustBeOwner, if true rather than false.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always.</item>
    ///   <item>SetParentObject, rarely, e.g. when Owner should always be ParentObject 
    /// and vice versa.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class TableAsSequenceMutable : TableMutable, ITableAsSequenceMutable
    {

        // Construction

        public TableAsSequenceMutable() : base() { }

        public TableAsSequenceMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ISchema schema = null
            , IDataProcObject owner = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , sqlTranslator
                , schema
                , ObjectTypes.Sequence()
                , owner
                , allowsUpdates: true
                , allowsDeletes: true
                , nameWhenPrefixed: nameWhenPrefixed
                , id: id
                )
        {
            ConstructNewMembers(true);
            CompleteConstructionIfType(typeof(TableAsSequenceMutable), retrieveIDsFromDatabase, assert);
        }

        // IDataProcObject method overrides
        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (IsFullyConstructed)
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    ObjectType.Equals(ObjectTypes.Sequence())
                    , "The conceptual object type of a table-as-sequence "
                    + "must be ObjectTypes.Sequence()."
                    );
                Debug.Assert(
                    PhysicalObjectType.Equals(ObjectTypes.Sequence())
                    || PhysicalObjectType.Equals(ObjectTypes.Table())
                    , "The physical object type of a table-as-sequence "
                    + "must be ObjectTypes.Table()."
                    );
                Debug.Assert(
                    IDColumns.Count == 1
                    , "A table-as-sequence must have exactly one ID column."
                    );
                Debug.Assert(
                    Columns.Count == 2
                    , "A table-as-sequence must have exactly two columns."
                    );
                Debug.Assert(
                    Columns.Where(p => p.Key.Equals(IDColumn.Name)).First().Value.Item1 == 0
                    , "A table-as-sequence's id column must have index 0."
                    );
                Debug.Assert(
                    Columns.Where(p => !p.Key.Equals(IDColumn.Name)).First().Value.Item1 == 1
                    , "A table-as-sequence's dummy insert column must have index 1."
                    );
            }
            return true;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new TableAsSequenceMutable(
                Name
                , GetSqlTranslatorSafely()
                , (ISchema)GetParentObjectSafely()
                , GetOwnerSafely()
                , NameWhenPrefixed
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , schema: null
                , owner: null
                , nameWhenPrefixed: null
                , id: default(int?)
                );
        }

        protected virtual void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator
            , ISchema schema
            , IDataProcObject owner
            , string nameWhenPrefixed
            , int? id
            )
        {
            base.ConstructAfterConstruction(
                name: name
                , sqlTranslator: sqlTranslator
                , schema: schema
                , objectType: ObjectTypes.Sequence()
                , owner: owner
                , allowsUpdates: true
                , allowsDeletes: true
                , columns: null
                , indexes: null
                , primaryKey: null
                , primaryKeySequence: null
                , uniqueKeys: null
                , foreignKeys: null
                , triggers: null
                , checkConstraints: null
                , defaultConstraints: null
                , isAudited: false
                , auditTableName: null
                , publishesChanges: false
                , changeTrackingTableName: null
                , parentTable: null
                , parentPassThrough: null
                , canHaveChildren: false
                , leavesTableName: null
                , maintainChildVablesTable: false
                , childVablesTableName: null
                , hasIDColumn: true
                , idColumnName: null
                , hasInputterColumn: false
                , inputtersColumnName: null
                , validInputtersTableName: null
                , hasLastUpdateDtmColumn: false
                , hasLastUpdaterColumn: false
                , nameWhenPrefixed: null
                , id: default(int?)
                );
            ConstructNewMembers(false);
        }

        private void ConstructNewMembers(bool forceUpdates = true)
        {
            ISqlTranslator sql = SqlTranslator;
            if (SqlTranslator == null)
                throw new ConstructionRequiresSqlTranslatorException(
                    this
                    , typeof(TableMutable).FullName
                    );
            SetHasIDColumn(true, retrieveIDColumnIDFromDatabase: false, assert: false);
            AddOrOverwriteColumn(
                new ColumnMutable(
                    "dummy"
                    , "bit"
                    , sql
                    , this
                    , retrieveIDsFromDatabase: false
                    , assert: false
                    )
                );
        }

        // ObjectInSchemaMutable method overrides

        public override IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            RejectIfInvalidParentObjectType(parentObject);
            bool retrieveIDs = WillNeedToRetrieveIDs(ParentObject, parentObject, retrieveIDsFromDatabase);
            ITableAsSequenceMutable retval =
                new TableAsSequenceMutable(
                    Name
                    , SqlTranslator
                    , (ISchema)ParentObject
                    , Owner
                    , NameWhenPrefixed
                    , ID
                    , retrieveIDsFromDatabase: false
                    , assert: false
                    );
            if (retrieveIDs) retval.RetrieveIDsFromDatabase(assert: false);
            if (assert) Debug.Assert(retval.PassesSelfAssertion());
            return retval;
        }

        // IDatabaseSequence methods

        public int GetNextKey()
        {
            ISqlTranslator translator = SqlTranslator;
            IDatabaseSafe db = Database;
            IConnection conn = db.GetAConnection();
            ITransaction trans = db.BeginTransaction(conn);
            int retval = (int)db.SqlExecuteSingleValue(SqlTranslator.GetNextKeySql(this, 0), conn, trans);
            db.CommitFully(conn, trans);
            db.ReassumeControlOfConnection(conn);
            return retval;
        }

        public int GetFirstKeyInNextBlockOfNKeys(int n)
        {
            ISqlTranslator translator = SqlTranslator;
            IDatabaseSafe db = Database;
            IConnection conn = db.GetAConnection();
            ITransaction trans = db.BeginTransaction(conn);
            int retval = (int)db.SqlExecuteSingleValue(SqlTranslator.GetFirstKeyInNextBlockOfNKeysSql(this, n, 0), conn, trans);
            db.CommitFully(conn, trans);
            db.ReassumeControlOfConnection(conn);
            return retval;
        }

        // ITableAsSequence properties
        public IColumn DummyInsertColumn
        {
            get { return Columns.Values.Where(t => t.Item1 == 1).First().Item2; }
        }

    }

    /// <summary>
    /// Boilerplate implementation of ITableAsSequence without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public class TableAsSequenceImmutable : TableImmutable, ITableAsSequence
    {

        private ITableAsSequence Delegee { get { return (ITableAsSequence)_delegee; } }

        //Disable default constructor
        protected TableAsSequenceImmutable(): base() { }

        public TableAsSequenceImmutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ISchema schema = null
            , IDataProcObject owner = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool retrieveIDsFromDatabase = false
            , bool assert = true
            )
        {
            _delegee =
                new TableAsSequenceMutable(
                    name
                    , sqlTranslator
                    , schema
                    , owner
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    );
            CompleteConstructionIfType(typeof(TableAsSequenceImmutable), false, assert);
        }

        public TableAsSequenceImmutable(ITableAsSequence delegee, bool assert = true)
        {
            _delegee = delegee;
            CompleteConstructionIfType(typeof(TableAsSequenceImmutable), false, assert);
        }

        // DataProcObject method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(ITableAsSequence); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            IObjectInSchema retval
                = new TableAsSequenceImmutable(
                (ITableAsSequence)
                    ((IObjectInSchema)_delegee).GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, assert)
                    );
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }
        public override IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            IObjectInSchema retval
                = new TableAsSequenceImmutable(
                (ITableAsSequence)
                    ((IObjectInSchema)_delegee).GetCopyForParentObject(parentObject, retrieveIDsFromDatabase, assert)
                    );
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }

        // ITableAsSequence methods

        public int GetNextKey() { return Delegee.GetNextKey(); }
        public int GetFirstKeyInNextBlockOfNKeys(int n) { return Delegee.GetFirstKeyInNextBlockOfNKeys(n); }

        // ITableAsSequence properties
        public IColumn DummyInsertColumn { get { return Delegee.DummyInsertColumn; } }

    }

}
