﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{
    public interface IChangeSubscription : ISelfAsserter, IMemberMatcher, ISafeMemberOfImmutableOwner
    {
        string PublishingTableNameWithSchema { get; }
        string ResponderClassFullName { get; }
    }
    public sealed class ChangeSubscription : IChangeSubscription
    {
        private ChangeSubscription() { }

        public ChangeSubscription(
            string publishingTableName
            , string responderClassFullName
            )
        {
            PublishingTableNameWithSchema = publishingTableName;
            ResponderClassFullName = responderClassFullName;
        }

        // Object method overrides

        /// <summary>
        /// Overriden Object.Equals object.
        /// </summary>
        /// <remarks>
        /// Should not be overridden by descendant classes.
        /// </remarks>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return EqualityUtilities.MemberMatchwiseEqualOrBothNull(this, obj);
        }

        /// <summary>
        /// Overriden Object.GetHashCode object.
        /// </summary>
        /// <remarks>
        /// Should be overridden by all descendant classes,
        /// and only those descendant classes, that add
        /// new members into MemberMatchwiseEqualOrBothNull.
        /// The general rule is that each new member's hash code
        /// is added to the hash code that would be generated
        /// by the nearest ancestor, after having been multiplied
        /// by a prime number not used by other members.
        /// Since descendants will need to know what
        /// prime numbers have already been used, each
        /// class must also publish its HighestPrimeUsedInHashCode.
        /// The descendant class then starts using prime numbers
        /// starting with NextPrimes[base.HighestPrimeUsedInHashCode].
        /// </remarks>
        /// <param name="obj">The object to which this is
        /// being compared for equality.</param>
        /// <returns>The object's hash code.</returns>
        public override int GetHashCode()
        {
            return PublishingTableNameWithSchema.GetHashCode()
                + 3 * ResponderClassFullName.GetHashCode();
        }

        //  ISelfAsserter methods

        public bool PassesSelfAssertion(Type testableType = null)
        {
            Debug.Assert(
                PublishingTableNameWithSchema != null
                , "PublishingTableNameWithSchema property of ChangeSubscription cannot be null."
                );
            Debug.Assert(
                PublishingTableNameWithSchema.Length > 0
                , "PublishingTableNameWithSchema property of ChangeSubscription cannot be the empty string."
                );
            Debug.Assert(
                !(new Regex("\\s").IsMatch(PublishingTableNameWithSchema))
                , "PublishingTableNameWithSchema property of ChangeSubscription cannot contain whitespace."
                );
            Debug.Assert(
                ResponderClassFullName != null
                , "ResponderClassFullName property of ChangeSubscription cannot be null."
                );
            Debug.Assert(
                ResponderClassFullName.Length > 0
                , "ResponderClassFullName property of ChangeSubscription cannot be the empty string."
                );
            Debug.Assert(
                !(new Regex("\\s").IsMatch(ResponderClassFullName))
                , "ResponderClassFullName property of ChangeSubscription cannot contain whitespace."
                );
            return true;
        }

        // IMemberMatcher methods
        public bool MembersMatch(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(ChangeSubscription).IsAssignableFrom(obj.GetType())) return false;
            ChangeSubscription other = (ChangeSubscription)obj;
            return
                PublishingTableNameWithSchema == other.PublishingTableNameWithSchema
                && ResponderClassFullName == other.ResponderClassFullName;
        }

        // ISafeMemberOfImmutableOwner methods

        public ISafeMemberOfImmutableOwner GetSafeReference()
        {
            return GetSafeReference(assert: true);
        }

        public ISafeMemberOfImmutableOwner GetSafeReference(bool assert)
        {
            if (assert) Debug.Assert(PassesSelfAssertion());
            return this;
        }

        // ChangeSubscription properties

        public string PublishingTableNameWithSchema { get; }
        public string ResponderClassFullName { get; }
    }
}
