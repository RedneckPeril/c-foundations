﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// This interface identifies the "tinyint"
    /// SQL type.
    /// </summary>
    /// <remarks>
    /// We ordinarily just retrieve the static object
    /// SqlSmallint.Standard(). Objects are immutable
    /// and therefore threadsafe.
    /// </remarks>
    public interface ISqlTinyint : ISqlIntType { }

    /// <summary>
    /// A basic, immutable implementation of the ISqlTinyint interface.
    /// </summary>
    public sealed class SqlTinyint : SqlIntType, ISqlTinyint
    {

        /****************
         *  Private methods
         ***************/

        private static SqlTinyint _standard;

        /****************
         *  Constructor
         ***************/

        private SqlTinyint() { }

        /****************
         *  ISqlTinyint properties and methods
         ***************/

        public override string DefaultSql { get { return "smallint"; } }

        public static SqlTinyint Standard()
        {
            if (_standard == null) _standard = new SqlTinyint();
            return _standard;
        }

    }

}
