﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface IStoredProcedureCommand : ICommand 
    {
        List<ISqlParameter> Parameters { get; }
        List<GenNameValue> Execute();
    }

}
