﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface ISingleColumnForeignKey : ISingleColumnConstraint, IForeignKey { }

    public interface ISingleColumnForeignKeyMutable : ISingleColumnForeignKey, ISingleColumnConstraintMutable, IForeignKeyMutable { }

    /// <summary>
    /// Boilerplate implementation of ISingleColumnForeignKeyMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides.</item>
    ///   <list type="bullet">
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class SingleColumnForeignKeyMutable : SingleColumnConstraintMutable, ISingleColumnForeignKeyMutable
    {

        private IForeignKeyMutable _delegee;
        private string FullTypeNameForCodeErrors { get { return typeof(ISingleColumnForeignKeyMutable).FullName; } }

        // Construction

        public SingleColumnForeignKeyMutable() : base() { }

        public SingleColumnForeignKeyMutable(
            string name
            , ISqlTranslator sqlTranslator
            , ITable table
            , ITableColumn constrainedColumn
            , Dictionary<string, string> keyReferenceColumnMappings = null
            , ITable referenceTable = null
            , bool cascadeOnDelete = false
            , bool autoCascadeOnDeleteIfPossible = false
            , bool cascadeOnUpdate = false
            , bool autoCascadeOnUpdateIfPossible = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name == null ? "" : name
                , sqlTranslator
                , table
                , ObjectTypes.ForeignKey()
                , ObjectTypes.ForeignKey()
                , constrainedColumn
                , nameWhenPrefixed
                , id
                )
        {
            if (Name == null || Name.Trim().Length == 0)
                SetName(null, false);
            CompleteConstructionIfType(typeof(SingleColumnConstraintMutable), retrieveIDsFromDatabase, assert);
        }

        public SingleColumnForeignKeyMutable(
            string name
            , ITableColumn constrainedColumn
            , Dictionary<string, string> keyReferenceColumnMappings = null
            , ITable referenceTable = null
            , bool cascadeOnDelete = false
            , bool autoCascadeOnDeleteIfPossible = false
            , bool cascadeOnUpdate = false
            , bool autoCascadeOnUpdateIfPossible = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , ObjectTypes.ForeignKey()
                , ObjectTypes.ForeignKey()
                , constrainedColumn
                , nameWhenPrefixed
                , id
                )
        {
            CompleteConstructionIfType(typeof(SingleColumnConstraintMutable), retrieveIDsFromDatabase, assert);
        }

        public SingleColumnForeignKeyMutable(
            ITableColumn constrainedColumn
            , Dictionary<string, string> keyReferenceColumnMappings = null
            , ITable referenceTable = null
            , bool cascadeOnDelete = false
            , bool autoCascadeOnDeleteIfPossible = false
            , bool cascadeOnUpdate = false
            , bool autoCascadeOnUpdateIfPossible = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                ""
                , ObjectTypes.ForeignKey()
                , ObjectTypes.ForeignKey()
                , constrainedColumn
                , nameWhenPrefixed
                , id
                )
        {
            CompleteConstructionIfType(typeof(SingleColumnConstraintMutable), retrieveIDsFromDatabase, assert);
        }

        // DataProcObjectMutable method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    _delegee != null
                    , "In a single-column foreign key, the "
                    + "delegee cannot be null."
                    );
                Debug.Assert(_delegee.PassesSelfAssertion());
            }
            return true;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new SingleColumnForeignKeyMutable(
                Name
                , GetSqlTranslatorSafely()
                , GetTableSafely()
                , GetConstrainedColumnSafely()
                , GetKeyReferenceColumnMappingsSafely()
                , GetReferencedTableSafely()
                , CascadeOnDelete
                , AutoCascadeOnDeleteIfPossible
                , CascadeOnUpdate
                , AutoCascadeOnUpdateIfPossible
                , NameWhenPrefixed
                , ID
                , false
                , assert
                );
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , table: null
                , constrainedColumn: null
                , keyReferenceColumnMappings: null
                , referenceTable: null
                , cascadeOnDelete: false
                , autoCascadeOnDeleteIfPossible: false
                , cascadeOnUpdate: false
                , autoCascadeOnUpdateIfPossible: false
                , nameWhenPrefixed: null
                , id: default(int?)
                );
        }

        protected virtual void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator
            , ITable table
            , ITableColumn constrainedColumn
            , Dictionary<string, string> keyReferenceColumnMappings = null
            , ITable referenceTable = null
            , bool cascadeOnDelete = false
            , bool autoCascadeOnDeleteIfPossible = false
            , bool cascadeOnUpdate = false
            , bool autoCascadeOnUpdateIfPossible = false
            , string nameWhenPrefixed = null
            , int? id = null
            )
        {
            base.ConstructAfterConstruction(
                name
                , sqlTranslator
                , table
                , ObjectTypes.ForeignKey()
                , ObjectTypes.ForeignKey()
                , constrainedColumn
                , nameWhenPrefixed: nameWhenPrefixed
                , id: id
                );
            ConstructNewMembers(
                name
                , keyReferenceColumnMappings
                , referenceTable
                , cascadeOnDelete
                , autoCascadeOnDeleteIfPossible
                , cascadeOnUpdate
                , autoCascadeOnUpdateIfPossible
                , false
                );
        }

        private void ConstructNewMembers(
            string name
            , Dictionary<string, string> keyReferenceColumnMappings = null
            , ITable referenceTable = null
            , bool cascadeOnDelete = false
            , bool autoCascadeOnDeleteIfPossible = false
            , bool cascadeOnUpdate = false
            , bool autoCascadeOnUpdateIfPossible = false
            , bool forceUpdates = true
            )
        {
            if (Name != null) name = Name;
            else SetName(name, false);
            _delegee
                = new ForeignKeyMutable(
                    name
                    , SqlTranslator
                    , Table
                    , ConstrainedColumn
                    , keyReferenceColumnMappings
                    , referenceTable
                    , cascadeOnDelete
                    , autoCascadeOnDeleteIfPossible
                    , cascadeOnUpdate
                    , autoCascadeOnUpdateIfPossible
                    , NameWhenPrefixed
                    , ID
                    , false
                    , false
                    );
        }

        // ObjectInSchema method overrides

        public override IObjectInSchema GetCopyForParentObject(
                IDatabaseObject parentObject
                , bool? retrieveIDsFromDatabase = default(bool?)
                , bool assert = true
                )
        {
            RejectIfInvalidParentObjectType(parentObject);
            return new SingleColumnForeignKeyMutable(
                Name
                , (ITableColumn)ConstrainedColumn.GetCopyForTable((ITable)parentObject)
                , KeyReferenceColumnMappings
                , (ITable)ReferencedTable.GetCopyForDatabase(parentObject.Database)
                , CascadeOnDelete
                , AutoCascadeOnDeleteIfPossible
                , CascadeOnUpdate
                , AutoCascadeOnUpdateIfPossible
                , NameWhenPrefixed
                , ID
                , false
                , assert
                );
        }

        // IForeignKey methods

        public Dictionary<string, string> KeyReferenceColumnMappings { get { return _delegee.KeyReferenceColumnMappings; } }
        public List<string> KeyColumnNames { get { return _delegee.KeyColumnNames; } }
        public List<string> ReferenceColumnNames { get { return _delegee.ReferenceColumnNames; } }
        public ITable ReferencedTable { get { return _delegee.ReferencedTable; } }
        public bool CascadeOnDelete { get { return _delegee.CascadeOnDelete; } }
        public bool AutoCascadeOnDeleteIfPossible { get { return _delegee.AutoCascadeOnDeleteIfPossible; } }
        public bool CascadeOnUpdate { get { return _delegee.CascadeOnUpdate; } }
        public bool AutoCascadeOnUpdateIfPossible { get { return _delegee.AutoCascadeOnUpdateIfPossible; } }
        public Dictionary<string, string> GetKeyReferenceColumnMappingsSafely()
        {
            return _delegee.GetKeyReferenceColumnMappingsSafely();
        }
        public List<string> GetKeyColumnNamesSafely()
        {
            return _delegee.GetKeyColumnNamesSafely();
        }
        public List<string> GetReferenceColumnNamesSafely()
        {
            return _delegee.GetReferenceColumnNamesSafely();
        }
        public ITable GetReferencedTableSafely()
        {
            return _delegee.GetReferencedTableSafely();
        }

        // IForeignKeyMutable methods

        public void SetKeyReferenceColumnMappings(Dictionary<string, string> value, bool assert = true)
        {
            _delegee.SetKeyReferenceColumnMappings(value, assert);
        }
        public void AddOrOverwriteKeyReferenceMapping(string key, string value, bool assert = true)
        {
            _delegee.AddOrOverwriteKeyReferenceMapping(key, value, assert);
        }
        public bool RemoveKeyReferenceMapping(string key, bool assert = true)
        {
            return _delegee.RemoveKeyReferenceMapping(key, assert);
        }
        public void SetReferencedTable(ITable value, bool assert = true)
        {
            _delegee.SetReferencedTable(value, assert);
        }
        public void SetCascadeOnDelete(bool value, bool assert = true)
        {
            _delegee.SetCascadeOnDelete(value, assert);
        }
        public void SetAutoCascadeOnDeleteIfPossible(bool value, bool assert = true)
        {
            _delegee.SetAutoCascadeOnDeleteIfPossible(value, assert);
        }
        public void SetCascadeOnUpdate(bool value, bool assert = true)
        {
            _delegee.SetCascadeOnUpdate(value, assert);
        }
        public void SetAutoCascadeOnUpdateIfPossible(bool value, bool assert = true)
        {
            _delegee.SetAutoCascadeOnUpdateIfPossible(value, assert);
        }

    }

    /// <summary>
    /// Boilerplate implementation of IChildOfVable without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <item>SingleColumnConstraintImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetCopyForColumn, always unless abstract class.</item>
    ///   <item>GetCopyForTable, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public class SingleColumnForeignKeyImmutable : SingleColumnConstraintImmutable, ISingleColumnForeignKey
    {

        private ISingleColumnForeignKey Delegee { get { return (ISingleColumnForeignKey)_delegee; } }

        public SingleColumnForeignKeyImmutable(
            string name
            , ISqlTranslator sqlTranslator
            , ITable table
            , ITableColumn constrainedColumn
            , Dictionary<string, string> keyReferenceColumnMappings = null
            , ITable referenceTable = null
            , bool cascadeOnDelete = false
            , bool autoCascadeOnDeleteIfPossible = false
            , bool cascadeOnUpdate = false
            , bool autoCascadeOnUpdateIfPossible = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) :base()
        {
            Dictionary<string, string> safeKeyReferenceColumnMappings
                = new Dictionary<string, string>();
            foreach (KeyValuePair<string, string> pair in keyReferenceColumnMappings)
                safeKeyReferenceColumnMappings[pair.Key] = pair.Value;
            _delegee
                = new SingleColumnForeignKeyMutable(
                    name
                    , sqlTranslator == null ? null : (ISqlTranslator)sqlTranslator.GetSafeReference()
                    , table == null ? null : (ITable)table.GetSafeReference()
                    , constrainedColumn == null ? null : (ITableColumn)constrainedColumn.GetSafeReference()
                    , keyReferenceColumnMappings == null ? null : safeKeyReferenceColumnMappings
                    , referenceTable == null ? null : (ITable)referenceTable.GetSafeReference()
                    , cascadeOnDelete
                    , autoCascadeOnDeleteIfPossible
                    , cascadeOnUpdate
                    , autoCascadeOnUpdateIfPossible
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    );
            CompleteConstructionIfType(typeof(SingleColumnConstraintImmutable), false, assert);
        }

        public SingleColumnForeignKeyImmutable(
            string name
            , ITableColumn constrainedColumn
            , Dictionary<string, string> keyReferenceColumnMappings = null
            , ITable referenceTable = null
            , bool cascadeOnDelete = false
            , bool autoCascadeOnDeleteIfPossible = false
            , bool cascadeOnUpdate = false
            , bool autoCascadeOnUpdateIfPossible = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base()
        {
            Dictionary<string, string> safeKeyReferenceColumnMappings
                = new Dictionary<string, string>();
            foreach (KeyValuePair<string, string> pair in keyReferenceColumnMappings)
                safeKeyReferenceColumnMappings[pair.Key] = pair.Value;
            _delegee
                = new SingleColumnForeignKeyMutable(
                    name
                    , constrainedColumn == null ? null : (ITableColumn)constrainedColumn.GetSafeReference()
                    , keyReferenceColumnMappings == null ? null : safeKeyReferenceColumnMappings
                    , referenceTable == null ? null : (ITable)referenceTable.GetSafeReference()
                    , cascadeOnDelete
                    , autoCascadeOnDeleteIfPossible
                    , cascadeOnUpdate
                    , autoCascadeOnUpdateIfPossible
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    );
            CompleteConstructionIfType(typeof(SingleColumnConstraintImmutable), false, assert);
        }

        public SingleColumnForeignKeyImmutable(
            ITableColumn constrainedColumn
            , Dictionary<string, string> keyReferenceColumnMappings = null
            , ITable referenceTable = null
            , bool cascadeOnDelete = false
            , bool autoCascadeOnDeleteIfPossible = false
            , bool cascadeOnUpdate = false
            , bool autoCascadeOnUpdateIfPossible = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base()
        {
            Dictionary<string, string> safeKeyReferenceColumnMappings
                = new Dictionary<string, string>();
            foreach (KeyValuePair<string, string> pair in keyReferenceColumnMappings)
                safeKeyReferenceColumnMappings[pair.Key] = pair.Value;
            _delegee
                = new SingleColumnForeignKeyMutable(
                    constrainedColumn == null ? null : (ITableColumn)constrainedColumn.GetSafeReference()
                    , keyReferenceColumnMappings == null ? null : safeKeyReferenceColumnMappings
                    , referenceTable == null ? null : (ITable)referenceTable.GetSafeReference()
                    , cascadeOnDelete
                    , autoCascadeOnDeleteIfPossible
                    , cascadeOnUpdate
                    , autoCascadeOnUpdateIfPossible
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    );
            CompleteConstructionIfType(typeof(SingleColumnConstraintImmutable), false, assert);
        }

        public SingleColumnForeignKeyImmutable(ISingleColumnForeignKey delegee, bool assert = true)
        {
            _delegee = delegee;
            CompleteConstructionIfType(typeof(SingleColumnConstraintImmutable), false, assert);
        }

        // DataProcObjectImmutable method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(ISingleColumnForeignKey); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new SingleColumnForeignKeyImmutable(
                (ISingleColumnForeignKey)
                Delegee.GetReferenceForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new SingleColumnForeignKeyImmutable(
                (ISingleColumnForeignKey)
                Delegee.GetCopyForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }

        // SingleColumnConstraintImmutable method overrides

        public override IPotentiallySingleColumnConstraint GetCopyForColumn(ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return new SingleColumnForeignKeyImmutable(
                (ISingleColumnForeignKey)
                Delegee.GetCopyForColumn(
                    constrainedColumn
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }
        public override IPotentiallySingleColumnConstraint GetCopyForTable(ITable table, ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return new SingleColumnForeignKeyImmutable(
                (ISingleColumnForeignKey)
                Delegee.GetCopyForTable(
                    table
                    , constrainedColumn
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }

        // IForeignKey methods

        public Dictionary<string, string> KeyReferenceColumnMappings { get { return Delegee.GetKeyReferenceColumnMappingsSafely(); } }
        public List<string> KeyColumnNames { get { return Delegee.GetKeyColumnNamesSafely(); } }
        public List<string> ReferenceColumnNames { get { return Delegee.GetReferenceColumnNamesSafely(); } }
        public ITable ReferencedTable { get { return Delegee.GetReferencedTableSafely(); } }
        public bool CascadeOnDelete { get { return Delegee.CascadeOnDelete; } }
        public bool AutoCascadeOnDeleteIfPossible { get { return Delegee.AutoCascadeOnDeleteIfPossible; } }
        public bool CascadeOnUpdate { get { return Delegee.CascadeOnUpdate; } }
        public bool AutoCascadeOnUpdateIfPossible { get { return Delegee.AutoCascadeOnUpdateIfPossible; } }
        public Dictionary<string, string> GetKeyReferenceColumnMappingsSafely()
        {
            return Delegee.GetKeyReferenceColumnMappingsSafely();
        }
        public List<string> GetKeyColumnNamesSafely()
        {
            return Delegee.GetKeyColumnNamesSafely();
        }
        public List<string> GetReferenceColumnNamesSafely()
        {
            return Delegee.GetReferenceColumnNamesSafely();
        }
        public ITable GetReferencedTableSafely()
        {
            return Delegee.GetReferencedTableSafely();
        }

    }

}
