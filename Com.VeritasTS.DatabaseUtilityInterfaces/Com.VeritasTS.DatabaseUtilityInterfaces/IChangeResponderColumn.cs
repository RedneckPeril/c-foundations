﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface IChangeResponderColumn : ITableColumn
    {
    }
    public interface IChangeResponderColumnMutable : IChangeResponderColumn, ITableColumnMutable
    {
    }

    /// <summary>
    /// Boilerplate implementation of IChangeResponderColumnMutable.
    /// </summary>
    /// <remarks>
    /// Sealed class for now.
    /// <remarks>
    public sealed class ChangeResponderColumnMutable : TableColumnMutable, IChangeResponderColumnMutable
    {

        public ChangeResponderColumnMutable(
            string name
            , ISqlTranslator sqlTranslator
            , ITable parentObject
            , string nameWhenPrefixed = null
            , string constraintTag = null
            , int? id = null
            , bool retrieveIDsFromDatabase = false
            , bool assert = true
            )
            : base(
                  (name == null ? "changeResponder" : name)
                  , "nvarchar(255)"
                  , sqlTranslator: sqlTranslator
                  , table: parentObject
                  , isNullable: false
                  , isImmutable: true
                  , nameWhenPrefixed: nameWhenPrefixed
                  , constraintTag: constraintTag
                  , id: id
                  )
        {
            CompleteConstructionIfType(
                typeof(ChangeResponderColumnMutable)
                , retrieveIDsFromDatabase
                , assert
                );
        }

        // DataProcObjectMutable method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    "nvarchar(255)".Equals(DataType)
                    , "A ChangeResponderColumn must be typed as nvarchar(255), not "
                    + (DataType == null ? "[null]" : DataType)
                    + "."
                    );
                Debug.Assert(
                    !IsIdentity
                    , "A ChangeResponderColumn cannot be an identity column."
                    );
                Debug.Assert(
                    !IsNullable
                    , "A ChangeResponderColumn cannot be nullable."
                    );
                Debug.Assert(
                    IsImmutable
                    , "A ChangeResponderColumn must be invariant."
                    );
                Debug.Assert(
                    CalcFormula == null
                    , "A ChangeResponderColumn cannot be a computed column "
                    + "and therefore cannot have a computed definition."
                    );
                Debug.Assert(
                    ParentObject == null || CheckConstraints.Count > 0
                    , "If an ChangeResponderColumn has a non-null parent object, "
                    + "it must have at least one check constraint."
                    );
            }
            return true;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new ChangeResponderColumnMutable(
                Name
                , GetSqlTranslatorSafely()
                , GetTableSafely()
                , NameWhenPrefixed
                , ConstraintTag
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        // ObjectInSchemaMutable method overrides

        public override IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return base.GetCopyForParentObject(parentObject, retrieveIDsFromDatabase, assert);
        }

    }

    /// <summary>
    /// Boilerplate implementation of ITableColumn without variability.
    /// </summary>
    /// <remarks>
    /// Sealed class for now.
    /// <remarks>
    public sealed class ChangeResponderColumnImmutable : TableColumnImmutable, IChangeResponderColumn
    {

        private IChangeResponderColumn Delegee { get { return (IChangeResponderColumn)_delegee; } }

        public ChangeResponderColumnImmutable(
            string name
            , ISqlTranslator sqlTranslator
            , ITable parentObject
            , string nameWhenPrefixed = null
            , string constraintTag = null
            , int? id = null
            , bool retrieveIDsFromDatabase = false
            , bool assert = true
            )
            : base(delegee: null)
        {
            _delegee
                = new ChangeResponderColumnMutable(
                    name
                    , sqlTranslator
                    , parentObject
                    , nameWhenPrefixed
                    , constraintTag
                    , id
                    , retrieveIDsFromDatabase
                    , assert: false
                    );
            CompleteConstructionIfType(typeof(ChangeResponderColumnImmutable), retrieveIDsFromDatabase: false, assert: assert);
        }

        public ChangeResponderColumnImmutable(IChangeResponderColumn delegee, bool assert)
            : base()
        {
            _delegee = delegee;
            Debug.Assert(PassesSelfAssertion(typeof(ChangeResponderColumnImmutable)));
        }

        // DataProcObject method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(IChangeResponderColumn); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new ChangeResponderColumnImmutable(
                (IChangeResponderColumn)
                Delegee.GetReferenceForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , assert: false
                    )
                , assert: assert
                );
        }

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new ChangeResponderColumnImmutable(
                (IChangeResponderColumn)
                Delegee.GetCopyForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , assert: false
                    )
                , assert: assert
                );
        }

    }

}