﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface IConstraintWithText : IPotentiallySingleColumnConstraint
    {
        string ConstraintText { get; }
    }

    public interface IConstraintWithTextMutable : IConstraintWithText, IPotentiallySingleColumnConstraintMutable
    {
        void SetConstraintText(string value, bool assert = true);
    }

    /// <summary>
    /// Boilerplate implementation of IConstraintWithTextMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides.</item>
    ///   <list type="bullet">
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>ParentObjectMustBeOwner, if true rather than false.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public abstract class ConstraintWithTextMutable : PotentiallySingleColumnConstraintMutable, IConstraintWithTextMutable
    {

        private string _constraintText;
        private string FullTypeNameForCode { get { return typeof(ConstraintWithTextMutable).FullName; } }

        // Construction

        public ConstraintWithTextMutable() : base() { }

        public ConstraintWithTextMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , ITableColumn constrainedColumn = null
            , string constraintText = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , sqlTranslator
                , table
                , objectType
                , physicalObjectType
                , constrainedColumn
                , nameWhenPrefixed
                , id
                )
        {
            ConstructNewMembers(constraintText, true);
            CompleteConstructionIfType(typeof(ConstraintWithTextMutable), retrieveIDsFromDatabase, assert);
        }

        public ConstraintWithTextMutable(
            string name
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , ITableColumn constrainedColumn = null
            , string constraintText = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , objectType
                , physicalObjectType
                , constrainedColumn
                , nameWhenPrefixed
                , id
                )
        {
            ConstructNewMembers(constraintText, true);
            CompleteConstructionIfType(typeof(ConstraintWithTextMutable), retrieveIDsFromDatabase, assert);
        }

        // Object method overrides and overridden GetHasCode assistants

        public override int GetHashCode()
        {
            int p1 = NextPrimes[base.HighestPrimeUsedInHashCode];
            return base.GetHashCode()
                + p1 * (ConstraintText == null ? 0 : ConstraintText.GetHashCode());
        }

        public override int HighestPrimeUsedInHashCode
        {
            get
            {
                return NextPrimes[base.HighestPrimeUsedInHashCode];
            }
        }

        // IDataProcObject method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (IsFullyConstructed)
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    ConstraintText != null
                    , "An IConstraintWithText object cannot have null constraint text."
                    );
                Debug.Assert(
                    ConstraintText.Length > 0
                    , "An IConstraintWithText object's constraint text "
                    + "cannot be the empty string."
                    );
                Debug.Assert(
                    HasNonWhitespace(ConstraintText)
                    , "An IConstraintWithText object's constraint text "
                    + "must contain something other than whitespace."
                    );
                if (ObjectType != null)
                    Debug.Assert(
                        ObjectType.Equals(ObjectTypes.CheckConstraint())
                        || ObjectType.Equals(ObjectTypes.DefaultConstraint())
                        , "An IConstraintWithText object must be either a check constraint "
                        + "or a default constraint."
                        );
                if (PhysicalObjectType != null)
                    Debug.Assert(
                        PhysicalObjectType.Equals(ObjectTypes.CheckConstraint())
                        || PhysicalObjectType.Equals(ObjectTypes.DefaultConstraint())
                        , "An IConstraintWithText object's PhysicalObjectType property "
                        + "must be either ObjectTypes.CheckConstraint() "
                        + "or ObjectTypes.DefaultConstraint()."
                        );
                if (ConstrainedColumn != null)
                    Debug.Assert(
                        ConstraintText != null
                        , "When a single-column constraint has a constrained column, "
                        + "it must have non-null constraint text."
                        );
            }
            return true;
        }

        public override bool MembersMatch(object obj)
        {
            if (!base.MembersMatch(obj)) return false;
            if (!typeof(IConstraintWithText).IsAssignableFrom(obj.GetType())) return false;
            IConstraintWithText other = (IConstraintWithText)obj;
            if (!EqualityUtilities.EqualOrBothNull(this.ConstraintText, other.ConstraintText)) return false;
            return true;
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , table: null
                , objectType: null
                , physicalObjectType: null
                , constraintText: null
                , nameWhenPrefixed: null
                , id: default(int?)
                );
        }

        protected virtual void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , string constraintText = null
            , string nameWhenPrefixed = null
            , int? id = null
            )
        {
            base.ConstructAfterConstruction(
                name
                , sqlTranslator
                , table
                , objectType
                , physicalObjectType
                , nameWhenPrefixed: nameWhenPrefixed
                , id: id
                );
            ConstructNewMembers(
                constraintText
                , false
                );
        }

        private void ConstructNewMembers(
            string constraintText
            , bool forceUpdates
            )
        {
            if (Name == null || Name.Length == 0)
                SetName(null, false);
            if (forceUpdates || ConstraintText == null || !HasNonWhitespace(ConstraintText) )
                SetConstraintText(constraintText, false);
        }

        public override void SetName(string value, bool assert = true)
        {
            if (value != null) base.SetName(value, assert);
            else
            {
                Debug.Assert(
                    Table != null
                    , "Cannot derive default name for IConstraintWithText "
                    + "if the constraint's Table property is null."
                    );
                Debug.Assert(
                    Table.Name != null
                    , "Cannot derive default name for IConstraintWithText "
                    + "if the constraint's table's Name property is null."
                    );
                Debug.Assert(
                    ConstrainedColumn != null
                    , "Cannot derive default name for IConstraintWithText "
                    + "if the constraint's ConstrainedColumn property is null."
                    );
                Debug.Assert(
                    ConstrainedColumn.ConstraintTag != null
                    , "Cannot derive default name for IConstraintWithText "
                    + "if the constraint's ConstrainedColumn property "
                    + "has a null ConstraintTag."
                    );
                base.SetName(
                    Table.Name 
                    + ConstraintTypeTag
                    + ConstrainedColumn.ConstraintTag
                    );
            }
        }

        // IConstraintWithText properties and methods
        public string ConstraintText { get { return _constraintText; } set { SetConstraintText(value); } }

        // IConstraintWithTextMutable properties and methods

        public virtual void SetConstraintText(string value, bool assert = true)
        {
            if (value == null)
                throw new NullArgumentException(
                    "value"
                    , FullTypeNameForCode
                    );
            if (value.Length == 0)
                throw new EmptyStringConstraintTextException(
                    "value"
                    , FullTypeNameForCode
                    );
            if (!HasNonWhitespace(value))
                throw new WhitespaceOnlyConstraintTextException(
                    "value"
                    , FullTypeNameForCode
                    , value
                    );
            _constraintText = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

    }

    /// <summary>
    /// Boilerplate implementation of IConstraintWithText without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <item>ConstraintWithTextImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetCopyForColumn, always unless abstract class.</item>
    ///   <item>GetCopyForTable, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public abstract class ConstraintWithTextImmutable : PotentiallySingleColumnConstraintImmutable, IConstraintWithText
    {

        private IConstraintWithText Delegee { get { return (IConstraintWithText)_delegee; } }

        // IConstraintWithText properties
        public string ConstraintText { get { return Delegee.ConstraintText; } }

    }

    public class EmptyStringConstraintTextException : Exception
    {
        public EmptyStringConstraintTextException(
            string typeName
            , string objectName
            ) : base(
                message:
                "Constraints of type "
                + typeName != null ? typeName : "[unknown]"
                + objectName != null && objectName.Trim().Length > 0 ? ", such as '" + objectName + "'," : ""
                + " cannot have their ConstraintText "
                + "properties set to an empty string."
                )
        {
            TypeName = typeName;
            ObjectName = objectName;
        }
        public string TypeName { get; }
        public string ObjectName { get; }
    }

    public class WhitespaceOnlyConstraintTextException : Exception
    {
        public WhitespaceOnlyConstraintTextException(
            string typeName
            , string objectName
            , string constraintText
            ) : base(
                message:
                "Constraints of type "
                + typeName != null ? typeName : "[unknown]"
                + objectName != null && objectName.Trim().Length > 0 ? ", such as '" + objectName + "'," : ""
                + " cannot have their ConstraintText "
                + "properties set to strings that consist only of whitespace."
                )
        {
            TypeName = typeName;
            ObjectName = objectName;
            ConstraintText = constraintText;
        }
        public string TypeName { get; }
        public string ObjectName { get; }
        public string ConstraintText { get; }
    }

    public class ExplicitConstraintTextException : Exception
    {
        public ExplicitConstraintTextException(
            string typeName
            , string objectName
            , string constraintText
            ) : base(
                message:
                "Constraints of type " + typeName + " cannot have their ConstraintText "
                + "properties set to arbitrary values, as the ConstraintText property "
                + "for such objects is a determinate function of the other properties. "
                + "Therefore the SetConstraintText(value, assert) method can only be "
                + "called with the 'value' parameter set to null."
                )
        {
            TypeName = typeName;
            ObjectName = objectName;
            ConstraintText = constraintText;
        }
        public string TypeName { get; }
        public string ObjectName { get; }
        public string ConstraintText { get; }
    }

}
