﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface ICheckConstraint : IPotentiallySingleColumnConstraint, IConstraintWithText { }

    public interface ICheckConstraintMutable : ICheckConstraint, IPotentiallySingleColumnConstraintMutable, IConstraintWithTextMutable { }

    /// <summary>
    /// Boilerplate implementation of ICheckConstraintMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides.</item>
    ///   <list type="bullet">
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class CheckConstraintMutable : PotentiallySingleColumnConstraintMutable, ICheckConstraintMutable
    {

        private string _constraintText;
        private string FullTypeNameForCode { get { return typeof(CheckConstraintMutable).FullName; } }

        // Construction
        public CheckConstraintMutable() : base() { }

        public CheckConstraintMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , ITableColumn constrainedColumn = null
            , string constraintText = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , sqlTranslator
                , table
                , ObjectTypes.CheckConstraint()
                , ObjectTypes.CheckConstraint()
                , constrainedColumn
                , nameWhenPrefixed
                , id
                )
        {
            ConstructNewMembers(constraintText, true);
            CompleteConstructionIfType(typeof(CheckConstraintMutable), retrieveIDsFromDatabase, assert);
        }

        public CheckConstraintMutable(
            string name
            , ITableColumn constrainedColumn
            , string constraintText = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , ObjectTypes.CheckConstraint()
                , ObjectTypes.CheckConstraint()
                , constrainedColumn
                , nameWhenPrefixed
                , id
                )
        {
            ConstructNewMembers(constraintText, true);
            CompleteConstructionIfType(typeof(CheckConstraintMutable), retrieveIDsFromDatabase, assert);
        }

        public CheckConstraintMutable(
            ITableColumn constrainedColumn
            , string constraintText = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                ""
                , ObjectTypes.CheckConstraint()
                , ObjectTypes.CheckConstraint()
                , constrainedColumn
                , nameWhenPrefixed
                , id
                )
        {
            ConstructNewMembers(constraintText, true);
            CompleteConstructionIfType(typeof(CheckConstraintMutable), retrieveIDsFromDatabase, assert);
        }

        // Object method overrides and overridden GetHasCode assistants

        public override int GetHashCode()
        {
            int p1 = NextPrimes[base.HighestPrimeUsedInHashCode];
            return base.GetHashCode()
                + p1 * (ConstraintText == null ? 0 : ConstraintText.GetHashCode());
        }

        public override int HighestPrimeUsedInHashCode
        {
            get
            {
                return NextPrimes[base.HighestPrimeUsedInHashCode];
            }
        }

        // IDataProcObject method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (IsFullyConstructed)
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    ConstraintText != null
                    , "A check constraint cannot have null constraint text."
                    );
                Debug.Assert(
                    HasNonWhitespace(ConstraintText)
                    , "A check constraint's constraint text "
                    + "must contain something other than whitespace."
                    );
                if (ObjectType != null)
                    Debug.Assert(
                        ObjectType.Equals(ObjectTypes.CheckConstraint())
                        , "An ICheckConstraint object's ObjectType property "
                        + "must be ObjectTypes.CheckConstraint()."
                        );
                if (PhysicalObjectType != null)
                    Debug.Assert(
                        PhysicalObjectType.Equals(ObjectTypes.CheckConstraint())
                        , "An ICheckConstraint object's PhysicalObjectType property "
                        + "must be ObjectTypes.CheckConstraint()."
                        );
            }
            return true;
        }

        public override bool MembersMatch(object obj)
        {
            if (!base.MembersMatch(obj)) return false;
            if (!typeof(ICheckConstraint).IsAssignableFrom(obj.GetType())) return false;
            ICheckConstraint other = (ICheckConstraint)obj;
            if (!EqualityUtilities.EqualOrBothNull(this.ConstraintText, other.ConstraintText)) return false;
            return true;
        }
        
        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new CheckConstraintMutable(
                Name
                , GetSqlTranslatorSafely()
                , GetTableSafely()
                , GetConstrainedColumnSafely()
                , ConstraintText
                , NameWhenPrefixed
                , ID
                , false
                , assert
                );
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , table: null
                , constrainedColumn: null
                , constraintText: null
                , nameWhenPrefixed: null
                , id: default(int?)
                );
        }

        protected virtual void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator
            , ITable table
            , ITableColumn constrainedColumn
            , string constraintText
            , string nameWhenPrefixed
            , int? id
            )
        {
            base.ConstructAfterConstruction(
                name
                , sqlTranslator
                , table
                , ObjectTypes.CheckConstraint()
                , ObjectTypes.CheckConstraint()
                , constrainedColumn
                , nameWhenPrefixed: nameWhenPrefixed
                , id: id
                );
        }

        private void ConstructNewMembers(
            string constraintText
            , bool forceUpdates
            )
        {
            if (Name == null || Name.Length == 0)
                SetName(null, false);
            if (forceUpdates || ConstraintText == null || !HasNonWhitespace(ConstraintText))
                SetConstraintText(constraintText, false);
        }

        public override void SetName(string value, bool assert = true)
        {
            if (value != null) base.SetName(value, assert);
            else
            {
                Debug.Assert(
                    SqlTranslator != null
                    , "Cannot derive default name for IConstraintWithText "
                    + "if the constraint's SqlTranslator property is null."
                    );
                Debug.Assert(
                    Table != null
                    , "Cannot derive default name for IConstraintWithText "
                    + "if the constraint's Table property is null."
                    );
                Debug.Assert(
                    Table.Name != null
                    , "Cannot derive default name for IConstraintWithText "
                    + "if the constraint's table's Name property is null."
                    );
                Debug.Assert(
                    ConstrainedColumn != null
                    , "Cannot derive default name for IConstraintWithText "
                    + "if the constraint's ConstrainedColumn property is null."
                    );
                Debug.Assert(
                    ConstrainedColumn.ConstraintTag != null
                    , "Cannot derive default name for IConstraintWithText "
                    + "if the constraint's ConstrainedColumn property "
                    + "has a null ConstraintTag."
                    );
                base.SetName(
                    SqlTranslator.TableConstraintName(
                        ConstraintTypeTag + ConstrainedColumn.ConstraintTag
                        , Table.Name
                        )
                    );
            }
        }

        // IDatabaseObjectMutable method overrides

        public override void SetOwner(IDataProcObject value, bool assert = true)
        {
            SetConstrainedColumn((ITableColumn)value, assert);
        }

        // ObjectInSchema method overrides

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            RejectIfInvalidParentObjectType(parentObject);
            return new CheckConstraintMutable(
                Name
                , SqlTranslator
                , (ITable)parentObject
                , ConstrainedColumn
                , ConstraintText
                , NameWhenPrefixed
                , ID
                , false
                , assert
                );
        }

        // IConstraintWithText properties and methods
        public string ConstraintText { get { return _constraintText; } set { SetConstraintText(value); } }

        // IConstraintWithTextMutable properties and methods

        public virtual void SetConstraintText(string value, bool assert = true)
        {
            if (value == null)
                throw new NullArgumentException(
                    "value"
                    , FullTypeNameForCode
                    );
            if (value.Length == 0)
                throw new EmptyStringConstraintTextException(
                    "value"
                    , FullTypeNameForCode
                    );
            if (!HasNonWhitespace(value))
                throw new WhitespaceOnlyConstraintTextException(
                    "value"
                    , FullTypeNameForCode
                    , value
                    );
            _constraintText = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

    }

    /// <summary>
    /// Boilerplate implementation of IChildOfVable without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <item>CheckConstraintImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetCopyForColumn, always unless abstract class.</item>
    ///   <item>GetCopyForTable, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public class CheckConstraintImmutable : PotentiallySingleColumnConstraintImmutable, ICheckConstraint
    {

        private ICheckConstraint Delegee { get { return (ICheckConstraint)_delegee; } }

        public CheckConstraintImmutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , string constraintText = null
            , ITableColumn constrainedColumn = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
        {
            _delegee
                = new CheckConstraintMutable(
                    name
                    , sqlTranslator == null ? null : (ISqlTranslator)sqlTranslator.GetSafeReference()
                    , table == null ? null : (ITable)table.GetSafeReference()
                    , constrainedColumn == null ? null : (ITableColumn)constrainedColumn.GetSafeReference()
                    , constraintText
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    );
            CompleteConstructionIfType(typeof(CheckConstraintImmutable), false, assert);
        }

        public CheckConstraintImmutable(
            string name
            , string constraintText = null
            , ITableColumn constrainedColumn = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
        {
            _delegee
                = new CheckConstraintMutable(
                    name
                    , constrainedColumn == null ? null : (ITableColumn)constrainedColumn.GetSafeReference()
                    , constraintText
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    );
            CompleteConstructionIfType(typeof(CheckConstraintImmutable), false, assert);
        }

        public CheckConstraintImmutable(
            string constraintText = null
            , ITableColumn constrainedColumn = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
        {
            _delegee
                = new CheckConstraintMutable(
                    constrainedColumn == null ? null : (ITableColumn)constrainedColumn.GetSafeReference()
                    , constraintText
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    );
            CompleteConstructionIfType(typeof(CheckConstraintImmutable), false, assert);
        }

        public CheckConstraintImmutable(ICheckConstraint delegee, bool assert = true)
        {
            _delegee = delegee;
            CompleteConstructionIfType(typeof(CheckConstraintImmutable), false, assert);
        }

        // DataProcObjectImmutable method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(ICheckConstraint); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new CheckConstraintImmutable(
                (ICheckConstraint)
                Delegee.GetReferenceForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new CheckConstraintImmutable(
                (ICheckConstraint)
                Delegee.GetCopyForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }

        // PotentiallySingleColumnConstraintImmutable method overrides

        public override IPotentiallySingleColumnConstraint GetCopyForColumn(ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return new CheckConstraintImmutable(
                (ICheckConstraint)
                Delegee.GetCopyForColumn(
                    constrainedColumn
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }
        public override IPotentiallySingleColumnConstraint GetCopyForTable(ITable table, ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return new CheckConstraintImmutable(
                (ICheckConstraint)
                Delegee.GetCopyForTable(
                    table
                    , constrainedColumn
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }

        // IConstraintWithText properties
        public string ConstraintText { get { return Delegee.ConstraintText; } }

    }

}
