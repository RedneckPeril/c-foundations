﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// This interface identifies the "smallint"
    /// SQL type.
    /// </summary>
    /// <remarks>
    /// We ordinarily just retrieve the static object
    /// SqlDateTime.Standard(). Objects are immutable
    /// and therefore threadsafe.
    /// </remarks>
    public interface ISqlDateTime : ISqlIntType { }

    /// <summary>
    /// A basic, immutable implementation of the ISqlDateTime interface.
    /// </summary>
    public sealed class SqlDateTime : SqlDateTimeType, ISqlDateTime
    {

        /****************
         *  Private methods
         ***************/

        private string FullTypeNameForCodeErrors { get { return typeof(SqlDateTime).FullName; } }
        private static SqlDateTime _standard;

        /****************
         *  Constructor
         ***************/

        private SqlDateTime() { }

        /****************
         *  SqlDataType method overrides
         ***************/

        public override string DefaultSql { get { return "datetime"; } }

        /****************
         *  ISqlDateTime properties and methods
         ***************/

        public static SqlDateTime Standard()
        {
            if (_standard == null) _standard = new SqlDateTime();
            return _standard;
        }

    }

}
