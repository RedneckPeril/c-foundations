﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;


namespace Com.VeritasTS.DatabaseUtilityInterfaces
{
    /*
    public interface ITable : IVable
    {
        IPrimaryKey PrimaryKey { get; }
        List<ITableTrigger> Triggers { get; }
        List<IColumn> Columns { get; }
        List<ICheckConstraint> CheckConstraints { get; }
        bool CheckConstraintsIU { get; }
        IPrimaryKeySequenceMap PrimaryKeySequenceMap { get; }

        ITable Table { get; }
        IParentPassThroughInfo ParentPassThrough { get; }
        ////////
        List<ICascadingForeignKey> CascadingForeignKeys { get; }
        bool IsAudited { get; }
        IAuditTable AuditTable { get; }
        List<IMappedColumn> QueueKeyColumns { get; }
        List<IColumn> InsertableColumns { get; }
        bool CanHaveChildren { get;}
        bool IsQueued { get; }
        QueueJobsTable QueueJobsTable { get; }
        BlackListTable BlackListTable { get; }
        QueueLogTable QueueLogTable { get; }
        QIsTable QIsTable { get; }

        string InsertColumnListSql();
        string UpdateColumnListSql();
        void MakeTable();
        //void AddColumnIfNotExists(IColumn column);
        //void LoadForeignKey(IForeignKey key);
        //void LoadIndex(IIndex index);
        //void LoadUniqueKey(IUniqueKey key);
        //void LoadTrigger(ITableTrigger trigger);
        //void LoadCheckConstraint(ICheckConstraint constraint);
        //void LoadDefault(IDefaultConstraint aDefault);
        List<ICascadingForeignKey> GetCascadingForeignKeys();
        void RefreshColumnsFromDb();
    }

    public interface ITableMutable : IVable
    {
        List<ITableTrigger> Triggers { get; }
        List<IColumn> Columns { get; }
        List<ICheckConstraint> CheckConstraints { get; }
        bool CheckConstraintsIU { get; set; }
        IPrimaryKeySequenceMap PrimaryKeySequenceMap { get; set; }
        IPrimaryKey PrimaryKey { get; set; }

        //List<IConstraint> Constraints { get; set; }
        //List<IColumnCheckConstraint> ColumnCheckConstraints { get; set; }
        //List<IMultiColumnCheckConstraint> MultiColumnCheckConstraints { get; set; }
        List<IDefaultConstraint> DefaultConstraints { get; }

        ////////
        ITable Table { get; set; }
        IParentPassThroughInfo ParentPassThrough { get; set; }
        ////////
        List<ICascadingForeignKey> CascadingForeignKeys { get; }
        bool IsAudited { get; set; }
        IAuditTable AuditTable { get; }
        List<IMappedColumn> QueueKeyColumns { get; set; }
        List<IColumn> InsertableColumns { get; }
        //IQueue Queue { get; set; }
        bool IsQueued { get; set; }
        QueueJobsTable QueueJobsTable { get; }
        BlackListTable BlackListTable { get; }
        QueueLogTable QueueLogTable { get; }
        QIsTable QIsTable { get; }

        //string InsertColumnListSql();
        //string UpdateColumnListSql();
        IAuditTable GetAuditTable();
        List<IColumn> GetAuditOrJobsColumns();
        void MakeTable();
        void AddColumnIfNotExists(IColumn column);
        void LoadForeignKey(IForeignKey key);
        void LoadIndex(IIndex index);
        void LoadUniqueKey(IUniqueKey key);
        void LoadTrigger(ITableTrigger trigger);
        void LoadCheckConstraint(ICheckConstraint constraint);
        void LoadDefault(IDefaultConstraint aDefault);
        //List<ICascadingForeignKey> GetCascadingForeignKeys();
        //void RefreshColumnsFromDb();
    }

    public class TableMutable : DatabaseObject, ITableMutable
    {
        protected List<IColumn> _columns;
        protected List<IForeignKey> _foreignKeys;
        protected List<IIndex> _indexes;
        protected List<IUniqueKey> _uniqueKeys;
        protected List<ITableTrigger> _triggers;
        protected List<ICheckConstraint> _checkConstraints;
        protected List<ICascadingForeignKey> _cascadingFks;
        protected IAuditTable _auditTable = null;
        protected List<IDefaultConstraint> _defaults = null;
        protected List<IColumn> _auditOrJobsColumns = null;

        protected QueueJobsTable _queueJobsTable = null;
        protected BlackListTable _blackListTable = null;
        protected QueueLogTable _queueLogTable = null;
        protected QIsTable _qIsTable = null;

        public List<IColumn> Columns { get { return _columns; } }
        public IPrimaryKey PrimaryKey { get; set; }
        public List<IUniqueKey> UniqueKeys { get; set; }
        public List<IForeignKey> ForeignKeys { get { return _foreignKeys; } }
        public List<IIndex> Indexes { get { return _indexes; } }
        public List<ITableTrigger> Triggers { get { return _triggers; } }
        public List<ICascadingForeignKey> CascadingForeignKeys { get { return _cascadingFks; } }
        public List<ICheckConstraint> CheckConstraints { get { return _checkConstraints; } }
        public bool CheckConstraintsIU { get; set; } = false;
        public bool IsAudited { get; set; } = false;
        public IAuditTable AuditTable { get { return GetAuditTable(); } }
        public List<IColumn> InsertableColumns { get { return GetAuditOrJobsColumns(); } }
        public List<IMappedColumn> QueueKeyColumns { get; set; }


        //public List<IColumnCheckConstraint> ColumnCheckConstraints { get; set; }
        //public List<IMultiColumnCheckConstraint> MultiColumnCheckConstraints { get; set; }
        //public List<IConstraint> Constraints { get; set; }
        public List<IDefaultConstraint> DefaultConstraints { get { return _defaults; } }

        public IPrimaryKeySequenceMap PrimaryKeySequenceMap { get; set; }
        ////////
        public ITable Table { get; set; }
        public IParentPassThroughInfo ParentPassThrough { get; set; }
        ////////
        //public IQueue Queue { get; set; } = null;
        public bool IsQueued { get; set; } = false;
        public QueueJobsTable QueueJobsTable
        {
            get
            {
                if (!IsQueued) return null;
                if (_queueJobsTable == null)
                    _queueJobsTable = new QueueJobsTable(this, QueueKeyColumns, InsertableColumns);
                return _queueJobsTable;
            }
        }
        public BlackListTable BlackListTable
        {
            get
            {
                if (!IsQueued) return null;
                if (_blackListTable == null)
                    _blackListTable = new BlackListTable(this, QueueKeyColumns);
                return _blackListTable;
            }
        }
        public QueueLogTable QueueLogTable
        {
            get
            {
                if (!IsQueued) return null;
                if (_queueLogTable == null)
                    _queueLogTable = new QueueLogTable(this);
                return _queueLogTable;
            }
        }
        public QIsTable QIsTable
        {
            get
            {
                if (!IsQueued) return null;
                if (_qIsTable == null)
                    _qIsTable = new QIsTable(this);
                return _qIsTable;
            }
        }

        override public IObjectType ObjectType { get; set; } = new ObjectType { Name = "Table", IsVable = false };
        override public IObjectType PhysicalObjectType { get; set; } = new ObjectType { Name = "Table", IsVable = false };
        override public object GetThreadsafeClone() { return this; } // need a deep copy

        public TableMutable()
        {
            _columns = new List<IColumn>();
            _foreignKeys = new List<IForeignKey>();
            _indexes = new List<IIndex>();
            _uniqueKeys = new List<IUniqueKey>();
            _triggers = new List<ITableTrigger>();
            _checkConstraints = new List<ICheckConstraint>();
            _cascadingFks = new List<ICascadingForeignKey>();
            _defaults = new List<IDefaultConstraint>();
        }

        override public string ExistsSql()
        {
            return SqlTranslator.GetTableExistsSql(this);
        }

        override public void EnsureExistenceInDatabase()
        {
            if (!ExistsInDatabase())
            {
                string sql = "create table " + Schema.Name + "." + Name + " (toBeDeleted int null);";
                DatabaseMutable.SqlExecuteNonQuery(sql);
            }
        }


        public void AddColumnIfNotExists(IColumn column) { _columns.Add(column); }
        public void LoadIndex(IIndex index) { _indexes.Add(index); }
        public void LoadUniqueKey(IUniqueKey key) { _uniqueKeys.Add(key); }
        public void LoadTrigger(ITableTrigger trigger) { _triggers.Add(trigger); }
        public void LoadCheckConstraint(ICheckConstraint constraint) { _checkConstraints.Add(constraint); }
        public void LoadForeignKey(IForeignKey key) { _foreignKeys.Add(key); }
        public void LoadDefault(IDefaultConstraint aDefault) { _defaults.Add(aDefault); }

        public void MakeTable()
        {
            foreach (var col in _columns)
                col.EnsureExistenceInDatabase();
            IColumn toBeDeleted = new Column("toBeDeleted", "int", (ITable)Owner) { DatabaseImmutable = this.DatabaseImmutable };
            if (toBeDeleted.ExistsInDatabase())
                toBeDeleted.DropFromDatabaseIfExists(SqlTranslator.GetDropTableColumnSql(toBeDeleted));
            //toBeDeleted.DropFromDatabaseIfExists(toBeDeleted.GetDatabase().GetDropTableColumnSql(toBeDeleted));

            if (PrimaryKey != null)
                PrimaryKey.EnsureExistenceInDatabase();
            foreach (var key in _foreignKeys)
                key.EnsureExistenceInDatabase();
            foreach (var index in _indexes)
                index.EnsureExistenceInDatabase();
            foreach (var key in _uniqueKeys)
                key.EnsureExistenceInDatabase();
            foreach (var trigger in _triggers)
                trigger.EnsureExistenceInDatabase();
            foreach (var ckCons in _checkConstraints)
                ckCons.EnsureExistenceInDatabase();

            if (IsAudited)
            {
                AuditTable.EnsureExistenceInDatabase();
                AuditTable.MakeTable();
            }
            if (IsQueued)
            {
                QueueJobsTable.EnsureExistenceInDatabase();
                QueueJobsTable.MakeTable();
                BlackListTable.EnsureExistenceInDatabase();
                BlackListTable.MakeTable();
                QueueLogTable.EnsureExistenceInDatabase();
                QueueLogTable.MakeTable();
                QIsTable.EnsureExistenceInDatabase();
                QIsTable.MakeTable();
                // insert the table and the job into queues table
            }
        }

        //public string InsertColumnListSql()
        //{
        //    string columns = "";
        //    foreach (var col in _columns)
        //    {
        //        if (!col.IsIdentity && !col.IsComputed)
        //            columns += col.Name + ",";
        //    }
        //    columns = columns.Substring(0, columns.Length - 1);
        //    return columns;
        //}

        //public string UpdateColumnListSql()
        //{
        //    string columns = "";
        //    foreach (var col in _columns)
        //    {
        //        if (!col.IsComputed)
        //        {
        //            columns += col.Name + ",";
        //        }
        //    }
        //    columns = columns.Substring(0, columns.Length - 1);
        //    return columns;
        //}

        public IAuditTable GetAuditTable()
        {
            if (IsAudited && _auditTable == null)
                _auditTable = new AuditTable((ITable)Owner);
            return _auditTable;
        }

        public List<IColumn> GetAuditOrJobsColumns()
        {
            if (!IsAudited && !IsQueued) return null;
            if (_auditOrJobsColumns != null) return _auditOrJobsColumns;

            _auditOrJobsColumns = new List<IColumn>();
            foreach (var col in Columns)
                if (!col.IsComputed && !col.Name.Equals("Id") && !col.Name.Equals("LastUpdateDtm") && !col.Name.Equals("LastUpdater"))
                    _auditOrJobsColumns.Add(col);
            return _auditOrJobsColumns;
        }


        //public List<ICascadingForeignKey> GetCascadingForeignKeys()
        //{
        //    _cascadingFks.Clear();
        //    string sql = SqlTranslator.GetTableForeignKeyDependentsSql(this);
        //    var rows = DatabaseMutable.SqlExecuteReader(sql);
        //    foreach (var row in rows)
        //    {
        //        string keyName = row[3];
        //        string dependent = row[0];
        //        ITable table = (ITable)CommUtils.GetObjectByObjectName(dependent);
        //        IForeignKey key = table.ForeignKeys.FirstOrDefault(k => k.Name == keyName);
        //        if (key.CascadeOnDelete)
        //        {
        //            ICascadingForeignKey cfk = new CascadingForeignKey { DependentTable = table, DependentTableColumn = key.KeyColumn, ReferenceColumn = key.ReferenceColumn };
        //            _cascadingFks.Add(cfk);
        //        }
        //    }
        //    return _cascadingFks;
        //}

        //public void RefreshColumnsFromDb()
        //{
        //    _columns.Clear();
        //    string sql = SqlTranslator.GetTableColumnsSql(this);
        //    var columns = DatabaseMutable.SqlExecuteReader(sql);

        //    sql = SqlTranslator.GetTableIdentityColumnSql(this);
        //    var identity = DatabaseMutable.SqlExecuteReader(sql);

        //    sql = SqlTranslator.GetTableComputedColumnsSql(this);
        //    var computed = DatabaseMutable.SqlExecuteReader(sql);


        //    foreach (var col in columns)
        //    {
        //        bool isNullable = true;
        //        string dataType = col[2];
        //        bool isIdentity = false;
        //        bool isCalc = false;
        //        string calcFormula = "";
        //        if (col[1].Equals("NO")) isNullable = false;
        //        if (col[3].Length > 0) dataType += "(" + col[3] + ")";

        //        var v = identity.Where(i => i[0] == col[0]);
        //        if (v != null) isIdentity = true;

        //        v = computed.Where(c => c[0] == col[0]);
        //        if (v != null)
        //        {
        //            isCalc = true;
        //            calcFormula = v.FirstOrDefault()[1];
        //        }


        //        Column column = new Column(col[0], dataType, isCalc: isCalc, isNullable: isNullable, isIdentity: isIdentity, calcFormula: calcFormula);
        //        _columns.Add(column);
        //    }

       // }

    }






    public class Table : DatabaseObject, ITable
    {
        protected List<IColumn> _columns;
        protected List<IForeignKey> _foreignKeys;
        protected List<IIndex> _indexes;
        protected List<IUniqueKey> _uniqueKeys;
        protected List<ITableTrigger> _triggers;
        protected List<ICheckConstraint> _checkConstraints;
        protected List<ICascadingForeignKey> _cascadingFks;
        protected IAuditTable _auditTable = null;
        protected List<IDefaultConstraint> _defaults = null;
        protected List<IColumn> _auditOrJobsColumns = null;

        protected QueueJobsTable _queueJobsTable = null;
        protected BlackListTable _blackListTable = null;
        protected QueueLogTable _queueLogTable = null;
        protected QIsTable _qIsTable = null;

        protected ITableMutable _tableMutable; 

        public List<IColumn> Columns { get { return _columns; } }
        public IPrimaryKey PrimaryKey { get { return _tableMutable.PrimaryKey; } }
        public List<IUniqueKey> UniqueKeys { get { return _uniqueKeys; } }
        public List<IForeignKey> ForeignKeys { get { return _foreignKeys; } }
        public List<IIndex> Indexes { get { return _indexes; } }
        public List<ITableTrigger> Triggers { get {return _triggers; } }
        public List<ICascadingForeignKey> CascadingForeignKeys { get { return _cascadingFks; } }
        public List<ICheckConstraint> CheckConstraints { get { return _checkConstraints; } }
        public bool CheckConstraintsIU { get { return _tableMutable.CheckConstraintsIU; } }
        public bool IsAudited { get { return _tableMutable.IsAudited; } }
        public IAuditTable AuditTable { get { return GetAuditTable(); } }
        public List<IColumn> InsertableColumns { get { return GetAuditOrJobsColumns(); } }
        public List<IMappedColumn> QueueKeyColumns { get { return _tableMutable.QueueKeyColumns; } }


        //public List<IColumnCheckConstraint> ColumnCheckConstraints { get; set; }
        //public List<IMultiColumnCheckConstraint> MultiColumnCheckConstraints { get; set; }
        //public List<IConstraint> Constraints { get; set; }
        //public List<IDefaultConstraint> DefaultConstraints { get { return _defaults; } }

        public IPrimaryKeySequenceMap PrimaryKeySequenceMap { get { return _tableMutable.PrimaryKeySequenceMap; } }
        ////////
        public ITable Table { get { return _tableMutable.Table; } }
        public IParentPassThroughInfo ParentPassThrough { get { return _tableMutable.ParentPassThrough; }  }
        ////////
        //public IQueue Queue { get; set; } = null;
        public bool IsQueued { get { return _tableMutable.IsQueued; }  } 
        public QueueJobsTable QueueJobsTable { get { return _tableMutable.QueueJobsTable; } }
        //        if (!IsQueued) return null;
        //        if (_queueJobsTable == null)
        //            _queueJobsTable = new QueueJobsTable(this, QueueKeyColumns, InsertableColumns);
        //        return _queueJobsTable;
        //    }
        //}
        public BlackListTable BlackListTable { get { return _tableMutable.BlackListTable; } }
            //    if (!IsQueued) return null;
            //    if (_blackListTable == null)
            //        _blackListTable = new BlackListTable(this, QueueKeyColumns);
            //    return _blackListTable;
            //} }
        public QueueLogTable QueueLogTable { get { return _tableMutable.QueueLogTable; } }
            //    if (!IsQueued) return null;
            //    if (_queueLogTable == null)
            //        _queueLogTable = new QueueLogTable(this);
            //    return _queueLogTable;
            //} }
        public QIsTable QIsTable { get { return _tableMutable.QIsTable; } }
            //    if (!IsQueued) return null;
            //    if (_qIsTable == null)
            //        _qIsTable = new QIsTable(this);
            //    return _qIsTable;
            //} }

        override public IObjectType ObjectType { get; set; } = new ObjectType { Name = "Table", IsVable = false };
        override public IObjectType PhysicalObjectType { get; set; } = new ObjectType { Name = "Table", IsVable = false };
        override public object GetThreadsafeClone() { return this; } // need a deep copy

        public Table()
        {
            _tableMutable = new TableMutable();
            _tableMutable.Owner = this;
            _tableMutable.Name = Name;
            _columns = _tableMutable.Columns;
            _foreignKeys = _tableMutable.ForeignKeys;
            _indexes = _tableMutable.Indexes;
            _uniqueKeys = _tableMutable.UniqueKeys;
            _triggers = _tableMutable.Triggers;
            _checkConstraints = _tableMutable.CheckConstraints;
            _cascadingFks = _tableMutable.CascadingForeignKeys;
            _defaults = _tableMutable.DefaultConstraints;
        }

        override public string ExistsSql()
        {
            //return SqlTranslator.GetTableExistsSql(this);
            return _tableMutable.ExistsSql();
        }

        override public void EnsureExistenceInDatabase()
        {
            _tableMutable.Name = Name;
            _tableMutable.Schema = Schema;
            _tableMutable.EnsureExistenceInDatabase();
            //if (!ExistsInDatabase())
            //{
            //    string sql = "create table " + Schema.Name + "." + Name + " (toBeDeleted int null);";
            //    DatabaseMutable.SqlExecuteNonQuery(sql);
            //}
        }


        //public void AddColumnIfNotExists(IColumn column) { _columns.Add(column); }
        //public void LoadIndex(IIndex index) { _indexes.Add(index); }
        //public void LoadUniqueKey(IUniqueKey key) { _uniqueKeys.Add(key); }
        //public void LoadTrigger(ITableTrigger trigger) { _triggers.Add(trigger); }
        //public void LoadCheckConstraint(ICheckConstraint constraint) { _checkConstraints.Add(constraint); }
        //public void LoadForeignKey(IForeignKey key) { _foreignKeys.Add(key);  }
        //public void LoadDefault(IDefaultConstraint aDefault) { _defaults.Add(aDefault); }

        public void MakeTable()
        {
            _tableMutable.MakeTable();
            //foreach (var col in _columns)
            //    col.EnsureExistenceInDatabase();
            //IColumn toBeDeleted = new Column("toBeDeleted", "int", this) { DatabaseMutable = this.DatabaseMutable };
            //if (toBeDeleted.ExistsInDatabase())
            //    toBeDeleted.DropFromDatabaseIfExists(SqlTranslator.GetDropTableColumnSql(toBeDeleted));
            ////toBeDeleted.DropFromDatabaseIfExists(toBeDeleted.GetDatabase().GetDropTableColumnSql(toBeDeleted));

            //if (PrimaryKey != null)
            //    PrimaryKey.EnsureExistenceInDatabase();
            //foreach (var key in _foreignKeys)
            //    key.EnsureExistenceInDatabase();
            //foreach (var index in _indexes)
            //    index.EnsureExistenceInDatabase();
            //foreach (var key in _uniqueKeys)
            //    key.EnsureExistenceInDatabase();
            //foreach (var trigger in _triggers)
            //    trigger.EnsureExistenceInDatabase();
            //foreach (var ckCons in _checkConstraints)
            //    ckCons.EnsureExistenceInDatabase();

            //if (IsAudited)
            //{
            //    AuditTable.EnsureExistenceInDatabase();
            //    AuditTable.MakeTable();
            //}
            //if (IsQueued)
            //{
            //    QueueJobsTable.EnsureExistenceInDatabase();
            //    QueueJobsTable.MakeTable();
            //    BlackListTable.EnsureExistenceInDatabase();
            //    BlackListTable.MakeTable();
            //    QueueLogTable.EnsureExistenceInDatabase();
            //    QueueLogTable.MakeTable();
            //    QIsTable.EnsureExistenceInDatabase();
            //    QIsTable.MakeTable();
            //    // insert the table and the job into queues table
            //}
        }

        public string InsertColumnListSql()
        {
            string columns = "";
            foreach (var col in _columns)
            {
                if (!col.IsIdentity && !col.IsComputed)
                    columns += col.Name + ",";
            }
            columns = columns.Substring(0, columns.Length - 1);
            return columns;
        }

        public string UpdateColumnListSql()
        {
            string columns = "";
            foreach (var col in _columns)
            {
                if (!col.IsComputed)
                {
                    columns += col.Name + ",";
                }
            }
            columns = columns.Substring(0, columns.Length - 1);
            return columns;
        }

        protected IAuditTable GetAuditTable()
        {
            return _tableMutable.GetAuditTable();
            //if (IsAudited && _auditTable == null)
            //    _auditTable = new AuditTable (this);
            //return _auditTable;
        }

        protected List<IColumn> GetAuditOrJobsColumns()
        {
            return _tableMutable.GetAuditOrJobsColumns();
            //if (!IsAudited && !IsQueued) return null;
            //if (_auditOrJobsColumns != null) return _auditOrJobsColumns;

            //_auditOrJobsColumns = new List<IColumn>();
            //foreach (var col in Columns)
            //    if (!col.IsComputed && !col.Name.Equals("Id") && !col.Name.Equals("LastUpdateDtm") && !col.Name.Equals("LastUpdater"))
            //        _auditOrJobsColumns.Add(col);
            //return _auditOrJobsColumns;
        }


        public List<ICascadingForeignKey> GetCascadingForeignKeys()
        {
            _cascadingFks.Clear();
            string sql = SqlTranslator.GetTableForeignKeyDependentsSql(this);
            var rows = DatabaseMutable.SqlExecuteReader(sql);
            foreach (var row in rows)
            {
                string keyName = row[3];
                string dependent = row[0];
                ITable table = (ITable)CommUtils.GetObjectByObjectName(dependent);
                IForeignKey key = table.ForeignKeys.FirstOrDefault(k => k.Name == keyName);
                if (key.CascadeOnDelete)
                {
                    ICascadingForeignKey cfk = new CascadingForeignKey { DependentTable = table, DependentTableColumn = key.KeyColumn, ReferenceColumn = key.ReferenceColumn };
                    _cascadingFks.Add(cfk);
                }
            }
            return _cascadingFks;
        }

        public void RefreshColumnsFromDb()
        {
            _columns.Clear();
            string sql = SqlTranslator.GetTableColumnsSql(this);
            var columns = DatabaseMutable.SqlExecuteReader(sql);

            sql = SqlTranslator.GetTableIdentityColumnSql(this);
            var identity = DatabaseMutable.SqlExecuteReader(sql);

            sql = SqlTranslator.GetTableComputedColumnsSql(this);
            var computed = DatabaseMutable.SqlExecuteReader(sql);


            foreach (var col in columns)
            {
                bool isNullable = true;
                string dataType = col[2];
                bool isIdentity = false;
                bool isCalc = false;
                string calcFormula = "";
                if (col[1].Equals("NO")) isNullable = false;
                if (col[3].Length > 0) dataType += "(" + col[3] + ")";

                var v = identity.Where(i => i[0] == col[0]);
                if (v != null) isIdentity = true;

                v = computed.Where(c => c[0] == col[0]);
                if (v != null)
                {
                    isCalc = true;
                    calcFormula = v.FirstOrDefault()[1];
                }


                Column column = new Column(col[0], dataType, isCalc: isCalc, isNullable: isNullable, isIdentity: isIdentity, calcFormula: calcFormula) ;
                _columns.Add(column);
            }

        }

    }
    */
}
