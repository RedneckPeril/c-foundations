﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface IUniqueKey : IPotentiallySingleColumnConstraint, IIndex
    {
        bool IsPrimaryKey { get; }
    }

    public interface IUniqueKeyMutable : IUniqueKey, IPotentiallySingleColumnConstraint, IIndexMutable { }

    /// <summary>
    /// Boilerplate implementation of IUniqueKeyMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides.</item>
    ///   <list type="bullet">
    ///   <item>RetrieveIDsFromDatabase, if new IDatabaseObject members have been
    /// added as members.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaMutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidParentObjectTypes, if narrower than ITable.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   <item>SetParentObject, rarely, e.g. when ParentObject and Owner should always
    ///   equal not only each other but some third member as well.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class UniqueKeyMutable : PotentiallySingleColumnConstraintMutable, IUniqueKeyMutable
    {

        private List<string> _columnNames;
        private bool _isClustered;
        private string FullTypeNameForCodeErrors { get { return typeof(UniqueKeyMutable).FullName; } }

        // Constructors

        public UniqueKeyMutable() : base() { }

        public UniqueKeyMutable(
            string name
            , ISqlTranslator sqlTranslator
            , ITable table
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , ITableColumn constrainedColumn = null
            , List<string> columnNames = null
            , bool isClustered = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , sqlTranslator
                , table
                , objectType == null ? ObjectTypes.UniqueKey() : objectType
                , physicalObjectType == null ? ObjectTypes.UniqueKey() : physicalObjectType
                , constrainedColumn
                , nameWhenPrefixed
                , id
                )
        {
            ConstructNewMembers(
                columnNames
                , isClustered
                , true
                );
            CompleteConstructionIfType(typeof(UniqueKeyMutable), retrieveIDsFromDatabase, assert);
        }

        public UniqueKeyMutable(
            string name
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , ITableColumn constrainedColumn = null
            , List<string> columnNames = null
            , bool isClustered = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , objectType == null ? ObjectTypes.UniqueKey() : objectType
                , physicalObjectType == null ? ObjectTypes.UniqueKey() : physicalObjectType
                , constrainedColumn
                , nameWhenPrefixed
                , id
                )
        {
            ConstructNewMembers(
                columnNames
                , isClustered
                , true
                );
            CompleteConstructionIfType(typeof(UniqueKeyMutable), retrieveIDsFromDatabase, assert);
        }

        public UniqueKeyMutable(
            IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , ITableColumn constrainedColumn = null
            , List<string> columnNames = null
            , bool isClustered = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                ""
                , objectType == null ? ObjectTypes.UniqueKey() : objectType
                , physicalObjectType == null ? ObjectTypes.UniqueKey() : physicalObjectType
                , constrainedColumn
                , nameWhenPrefixed
                , id
                )
        {
            ConstructNewMembers(
                columnNames
                , isClustered
                , true
                );
            CompleteConstructionIfType(typeof(UniqueKeyMutable), retrieveIDsFromDatabase, assert);
        }

        // Object method overrides and overridden GetHasCode assistants

        public override int GetHashCode()
        {
            return base.GetHashCode()
                + NextPrimes[base.HighestPrimeUsedInHashCode] * ColumnNames.GetHashCode();
        }

        public override int HighestPrimeUsedInHashCode
        {
            get { return NextPrimes[base.HighestPrimeUsedInHashCode]; }
        }

        // IDataProcObject method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (IsFullyConstructed)
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    ParentObject != null
                    , "ParentObject property of IUniqueKey object "
                    + "cannot be null."
                    );
                Debug.Assert(
                    Owner != null
                    , "The Owner property of an IUniqueKey object "
                    + "cannot be null."
                    );
                Debug.Assert(
                    ObjectType != null
                    , "The ObjectType property of an IUniqueKey object "
                    + "cannot be null."
                    );
                Debug.Assert(
                    ObjectType == ObjectTypes.UniqueKey()
                    || ObjectType == ObjectTypes.PrimaryKey()
                    , "The ObjectType property of an IUniqueKey object "
                    + "must be either PrimaryKey or UniqueKey."
                    );
                Debug.Assert(
                    PhysicalObjectType != null
                    , "The PhysicalObjectType property of an IUniqueKey object "
                    + "cannot be null."
                    );
                Debug.Assert(
                    PhysicalObjectType == ObjectTypes.UniqueKey()
                    || PhysicalObjectType == ObjectTypes.PrimaryKey()
                    , "The PhysicalObjectType property of an IUniqueKey object "
                    + "must be either PrimaryKey or UniqueKey."
                    );
                Debug.Assert(
                    ColumnNames != null
                    , "ColumnNames property of IUniqueKey object "
                    + (Name == null ? "" : "'" + Name + "' ")
                    + "cannot be null."
                    );
                Debug.Assert(
                    ColumnNames.Count > 0
                    , "ColumnNames property of IUniqueKey object "
                    + (Name == null ? "" : "'" + Name + "' ")
                    + "cannot be an empty list."
                    );
                IDatabaseSafe db = Database;
                String databaseDescription
                    = db == null ? null : db.Description;
                foreach (string name in ColumnNames)
                {
                    Debug.Assert(
                        name != null
                        , "All column names for an IUniqueKey object "
                        + (Name == null ? "" : "'" + Name + "' ")
                        + "must be non-null."
                        );
                    Debug.Assert(
                        HasNonWhitespace(name)
                        , "No column name for an IUniqueKey object "
                        + (Name == null ? "" : "'" + Name + "' ")
                        + "may consist of whitespace only."
                        );
                    if (db != null)
                        Debug.Assert(
                            db.IsValidObjectName(name)
                            , "Column "
                            + (name == null ? "[null]" : "'" + name + "'")
                            + " cannot be part of a unique key on "
                            + "table '" + Owner.Name + "', because "
                            + (name == null ? "[null]" : "'" + name + "'")
                            + " is not a valid name within "
                            + databaseDescription
                            + "."
                            );
                    Debug.Assert(
                        Table.Columns.ContainsKey(name)
                        , "Column '" + name + "' cannot be part of a unique key on "
                            + "table '" + Table.Name + "', because there is no "
                        + "such column in the table."
                        );
                }
                if (ConstrainedColumn == null)
                {
                    Debug.Assert(
                        Owner == ParentObject
                        , "When a single-column unique constraint has no constrained column, "
                        + "the parent table must be its owner."
                        );
                }
                if (ConstrainedColumn != null)
                {
                    Debug.Assert(ConstrainedColumn.PassesSelfAssertion());
                    Debug.Assert(
                        ColumnNames.Count == 1
                        , "If a unique key's ConstrainedColumn property is not null, "
                        + "then the ColumnNames list must have exactly one name, "
                        + "that being (naturally enough) the name of the constrained column."
                        );
                    Debug.Assert(
                        ConstrainedColumn.Name.Equals(ColumnNames[0])
                        , "If a unique key's ConstrainedColumn property is not null, "
                        + "then the constrained column's name must be "
                        + "Item 0 in the ColumnNames list."
                        );
                    Debug.Assert(
                        Owner == ConstrainedColumn
                        , "When a single-column unique constraint has a constrained column, "
                        + "the constrained column must be its owner."
                        );
                    // Do not assert ConstrainedColumn, in order to avoid infinite self-assertion loop
                    if (ConstrainedColumn.ParentObject != null)
                        Debug.Assert(
                            ConstrainedColumn.ParentObject.Equals(ParentObject)
                            , "If the constrained column of a single-column unique constraint "
                            + "has a non-null parent object, the constraint "
                            + "must have that same parent object."
                            );
                }
            }
            return true;
        }

        public override bool MembersMatch(object obj)
        {
            if (!base.MembersMatch(obj)) return false;
            if (!typeof(UniqueKeyMutable).IsAssignableFrom(obj.GetType())) return false;
            UniqueKeyMutable other = (UniqueKeyMutable)obj;
            if (!EqualityUtilities.EqualOrBothNull(this.ColumnNames, other.ColumnNames)) return false;
            if (!EqualityUtilities.EqualOrBothNull(this.IsUnique, other.IsUnique)) return false;
            if (!EqualityUtilities.EqualOrBothNull(this.IsClustered, other.IsClustered)) return false;
            return true;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new UniqueKeyMutable(
                Name
                , GetSqlTranslatorSafely()
                , GetTableSafely()
                , GetObjectTypeSafely()
                , GetPhysicalObjectTypeSafely()
                , GetConstrainedColumnSafely()
                , GetColumnNamesSafely()
                , IsClustered
                , NameWhenPrefixed
                , ID
                );
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , table: null
                , objectType: null
                , physicalObjectType: null
                , constrainedColumn: null
                , columnNames: null
                , isClustered: false
                , nameWhenPrefixed: null
                , id: default(int?)
                );
        }

        protected virtual void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator
            , ITable table
            , IObjectType objectType
            , IObjectType physicalObjectType
            , ITableColumn constrainedColumn 
            , List<string> columnNames
            , bool isClustered
            , string nameWhenPrefixed
            , int? id
            )
        {
            base.ConstructAfterConstruction(
                name
                , sqlTranslator
                , table
                , objectType
                , physicalObjectType
                , constrainedColumn
                , nameWhenPrefixed
                , id
                );
            ConstructNewMembers(
                columnNames
                , isClustered
                , false
                );
        }

        private void ConstructNewMembers(
            List<string> columnNames
            , bool isClustered
            , bool forceUpdates
            )
        {
            if (forceUpdates || ColumnNames == null)
                SetColumnNames(columnNames, assert: false);
            if (forceUpdates || !IsClustered)
                SetIsClustered(isClustered, assert: false);
        }

        // ObjectInSchemaMutable method overrides

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = null
            , bool assert = true
            )
        {
            RejectIfInvalidParentObjectType(parentObject);
            bool retrieveIDs
                = retrieveIDsFromDatabase != null && retrieveIDsFromDatabase.HasValue
                ? retrieveIDsFromDatabase.Value
                : parentObject != null && parentObject.ID != null;
            IUniqueKeyMutable retval
                = new UniqueKeyMutable(
                    Name
                    , SqlTranslator
                    , Table
                    , ObjectType
                    , PhysicalObjectType
                    , ConstrainedColumn
                    , ColumnNames
                    , IsClustered
                    , NameWhenPrefixed
                    , ID
                    , retrieveIDsFromDatabase: false
                    , assert: false
                    );
            if (retrieveIDs) retval.RetrieveIDsFromDatabase(assert: false);
            if (assert) Debug.Assert(retval.PassesSelfAssertion());
            return retval;
        }

        // IUniqueKey methods and properties

        public List<string> ColumnNames
        {
            get { return _columnNames; }
            set { SetColumnNames(value); }
        }

        public bool IsUnique
        {
            get { return true; }
            set { SetIsUnique(value); }
        }

        public bool IsClustered
        {
            get { return _isClustered; }
            set { SetIsClustered(value); }
        }

        public bool IsConstraint
        {
            get { return typeof(IConstraint).IsAssignableFrom(this.GetType()); }
        }

        public List<string> GetColumnNamesSafely()
        {
            return _columnNames.ToList();
        }

        // IIndexMutable methods and properties

        public virtual void SetColumnNames(List<string> value, bool assert = true)
        {
            if (value == null) _columnNames = new List<string>();
            else _columnNames = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void AddOrOverwriteColumnName(string name, bool assert = true)
        {
            if (Database != null)
                Debug.Assert(
                    Database.IsValidObjectName(name)
                    , (name == null ? "[null]" : "'" + name + "'")
                    + " is not a valid database object name for "
                    + DatabaseDescription
                    + "."
                    );
            if (!_columnNames.Contains(name))
                _columnNames.Add(name);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual bool RemoveColumnName(string name, bool assert = true)
        {
            bool retval = _columnNames.Remove(name);
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }

        public virtual void SetIsUnique(bool value, bool assert = true)
        {
            // The answer must always be true; you cannot set IsUnique
            //   to false for UniqueKeys.
            if (!value)
                throw new NonuniqueUniqueKeyException(
                    IsFullyConstructed ? this : null
                    , FullTypeNameForCodeErrors
                    , Name != null ? Name : null
                    );
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void SetIsClustered(bool value, bool assert = true)
        {
            _isClustered = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        // IUniqueKey properties
        
        public bool IsPrimaryKey { get { return typeof(IPrimaryKey).IsAssignableFrom(GetType()); } }

        // Exception classes

        public class NonuniqueUniqueKeyException: Exception
        {
            private NonuniqueUniqueKeyException() { }
            public NonuniqueUniqueKeyException(
                IUniqueKey badObject
                , string badObjectTypeName
                , string badObjectName
                , string message = null
                ): base(
                    message:
                    message != null ? message
                    : badObject == null || badObject.Name == null
                    ? "An object of type "
                    + badObjectTypeName != null ? badObjectTypeName : typeof(IUniqueKey).FullName
                    + " cannot have its IsUnique property set to false."
                    : "Object '" + badObject.Name + "', being an object of type "
                    + badObjectTypeName != null ? badObjectTypeName : typeof(IUniqueKey).FullName
                    + ", cannot have its IsUnique property set to false."
                    )
            {
                BadObject = badObject;
                BadObjectTypeName = badObjectTypeName;
                BadObjectName = badObjectName;
            }
            public IUniqueKey BadObject { get; }
            public string BadObjectTypeName { get; }
            public string BadObjectName { get; }

        }

    }

    /// <summary>
    /// Boilerplate implementation of IUniqueKey without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>PotentiallySingleColumnConstraintImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetCopyForColumn, always unless abstract class.</item>
    ///   <item>GetCopyForTable, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public class UniqueKeyImmutable : PotentiallySingleColumnConstraintImmutable, IUniqueKey
    {

        private IUniqueKey Delegee { get { return (IUniqueKey)_delegee; } }

        public UniqueKeyImmutable(
            string name
            , ISqlTranslator sqlTranslator
            , ITable table
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , ITableColumn constrainedColumn = null
            , List<string> columnNames = null
            , bool isClustered = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base()
        {
            _delegee
                = (IUniqueKey)
                new UniqueKeyMutable(
                    name
                    , sqlTranslator
                    , table
                    , objectType
                    , physicalObjectType
                    , constrainedColumn
                    , columnNames
                    , isClustered
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    ).GetSafeReference();
            CompleteConstructionIfType(typeof(UniqueKeyImmutable), retrieveIDsFromDatabase: false, assert: assert);
        }

        public UniqueKeyImmutable(
            string name
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , ITableColumn constrainedColumn = null
            , List<string> columnNames = null
            , bool isClustered = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base()
        {
            _delegee
                = (IUniqueKey)
                new UniqueKeyMutable(
                    name
                    , objectType
                    , physicalObjectType
                    , constrainedColumn
                    , columnNames
                    , isClustered
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    ).GetSafeReference();
            CompleteConstructionIfType(typeof(UniqueKeyImmutable), retrieveIDsFromDatabase: false, assert: assert);
        }

        public UniqueKeyImmutable(
            IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , ITableColumn constrainedColumn = null
            , List<string> columnNames = null
            , bool isClustered = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base()
        {
            _delegee
                = (IUniqueKey)
                new UniqueKeyMutable(
                    objectType
                    , physicalObjectType
                    , constrainedColumn
                    , columnNames
                    , isClustered
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    ).GetSafeReference();
            CompleteConstructionIfType(typeof(UniqueKeyImmutable), retrieveIDsFromDatabase: false, assert: assert);
        }

        public UniqueKeyImmutable(
            IUniqueKey delegee
            , bool assert = true
            ) : base(delegee, false)
        {
            CompleteConstructionIfType(typeof(UniqueKeyImmutable), retrieveIDsFromDatabase: false);
        }

        // DataProcObjectImmutable method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(IUniqueKey); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return
                new UniqueKeyImmutable(
                    (IUniqueKey)
                    Delegee.GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, false)
                    );
        }

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return
                new UniqueKeyImmutable(
                    (IUniqueKey)
                    Delegee.GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, false)
                    );
        }

        // PotentiallySingleColumnConstraintImmutable method overrides

        public override IPotentiallySingleColumnConstraint GetCopyForColumn(ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return new UniqueKeyImmutable(
                (IUniqueKey)
                Delegee.GetCopyForColumn(
                    constrainedColumn
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }
        public override IPotentiallySingleColumnConstraint GetCopyForTable(ITable table, ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return new UniqueKeyImmutable(
                (IUniqueKey)
                Delegee.GetCopyForTable(
                    table
                    , constrainedColumn
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }

        // IUniqueKey methods and properties

        public List<string> ColumnNames { get { return Delegee.ColumnNames; } }
        public bool IsUnique { get { return Delegee.IsUnique; } }
        public bool IsClustered { get { return Delegee.IsClustered; } }
        public bool IsConstraint { get { return Delegee.IsConstraint; } }
        public List<string> GetColumnNamesSafely() { return Delegee.GetColumnNamesSafely(); }

        // IUniqueKey properties

        public bool IsPrimaryKey { get { return typeof(IPrimaryKey).IsAssignableFrom(GetType()); } }

    }

}
