﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// This interface groups together the SQL data
    /// types for which IsIntType is true.
    /// </summary>
    public interface ISqlBoolean : ISqlDataType { }

    /// <summary>
    /// A basic, immutable implementation of the IBoolean interface.
    /// </summary>
    public sealed class SqlBoolean : SqlDataType, ISqlBoolean
    {

        /****************
         *  Private members
         ***************/

        private string FullTypeNameForCodeErrors { get { return typeof(SqlBoolean).FullName; } }
        private static SqlBoolean _standard;

        /****************
         *  ISelfAsserter methods via ISelfUsabilityAsserter
         ***************/

        /// <summary>
        /// Asserts the class invariants for this class.
        /// </summary>
        /// <returns>True, unless an assertion failure exception is thrown.</returns>
        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertion(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
            }
            return true;
        }

        /****************
         *  SqlDataType method overrides
         ***************/

        public override string DefaultSql { get { return "bit"; } }

        /****************
         *  IBoolean methods
         ***************/

        public static SqlBoolean Standard()
        {
            if (_standard == null) _standard = new SqlBoolean();
            return _standard;
        }

    }

}
