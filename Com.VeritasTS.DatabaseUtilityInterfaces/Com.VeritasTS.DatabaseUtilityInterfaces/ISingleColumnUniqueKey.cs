﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface ISingleColumnUniqueKey : IUniqueKey, ISingleColumnConstraint { }

    public interface ISingleColumnUniqueKeyMutable : ISingleColumnUniqueKey, IUniqueKey, ISingleColumnConstraintMutable { }

    /// <summary>
    /// Boilerplate implementation of ISingleColumnUniqueKeyMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides.</item>
    ///   <list type="bullet">
    ///   <item>RetrieveIDsFromDatabase, if new IDatabaseObject members have been
    /// added as members.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaMutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidParentObjectTypes, if narrower than ITable.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   <item>SetParentObject, rarely, e.g. when ParentObject and Owner should always
    ///   equal not only each other but some third member as well.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class SingleColumnUniqueKeyMutable : UniqueKeyMutable, ISingleColumnUniqueKeyMutable
    {
        
        private string FullTypeNameForCodeErrors { get { return typeof(SingleColumnUniqueKeyMutable).FullName; } }

        // Constructors

        public SingleColumnUniqueKeyMutable() : base() { }

        public SingleColumnUniqueKeyMutable(
            string name
            , ISqlTranslator sqlTranslator
            , ITable table
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , ITableColumn constrainedColumn = null
            , List<string> columnNames = null
            , bool isClustered = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , sqlTranslator
                , table
                , objectType
                , physicalObjectType
                , constrainedColumn
                , columnNames
                , isClustered
                , nameWhenPrefixed
                , id
                )
        {
            CompleteConstructionIfType(typeof(SingleColumnUniqueKeyMutable), retrieveIDsFromDatabase, assert);
        }

        public SingleColumnUniqueKeyMutable(
            string name
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , ITableColumn constrainedColumn = null
            , List<string> columnNames = null
            , bool isClustered = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , objectType
                , physicalObjectType
                , constrainedColumn
                , columnNames
                , isClustered
                , nameWhenPrefixed
                , id
                )
        {
            CompleteConstructionIfType(typeof(SingleColumnUniqueKeyMutable), retrieveIDsFromDatabase, assert);
        }

        public SingleColumnUniqueKeyMutable(
            IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , ITableColumn constrainedColumn = null
            , List<string> columnNames = null
            , bool isClustered = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                ""
                , objectType
                , physicalObjectType
                , constrainedColumn
                , columnNames
                , isClustered
                , nameWhenPrefixed
                , id
                )
        {
            CompleteConstructionIfType(typeof(SingleColumnUniqueKeyMutable), retrieveIDsFromDatabase, assert);
        }

        // IDataProcObject method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (IsFullyConstructed)
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    ConstrainedColumn != null
                    , "In a single-column unique key, the "
                    + "ConstrainedColumn property cannot be null."
                    );
            }
            return true;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new SingleColumnUniqueKeyMutable(
                Name
                , GetSqlTranslatorSafely()
                , GetTableSafely()
                , GetObjectTypeSafely()
                , GetPhysicalObjectTypeSafely()
                , GetConstrainedColumnSafely()
                , GetColumnNamesSafely()
                , IsClustered
                , NameWhenPrefixed
                , ID
                );
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , table: null
                , objectType: null
                , physicalObjectType: null
                , constrainedColumn: null
                , columnNames: null
                , isClustered: false
                , nameWhenPrefixed: null
                , id: default(int?)
                );
        }

        protected override void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator
            , ITable table
            , IObjectType objectType
            , IObjectType physicalObjectType
            , ITableColumn constrainedColumn
            , List<string> columnNames
            , bool isClustered
            , string nameWhenPrefixed
            , int? id
            )
        {
            base.ConstructAfterConstruction(
                name
                , sqlTranslator
                , table
                , objectType
                , physicalObjectType
                , constrainedColumn
                , columnNames
                , isClustered
                , nameWhenPrefixed
                , id
                );
        }

        // ObjectInSchemaMutable method overrides

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = null
            , bool assert = true
            )
        {
            RejectIfInvalidParentObjectType(parentObject);
            bool retrieveIDs
                = retrieveIDsFromDatabase != null && retrieveIDsFromDatabase.HasValue
                ? retrieveIDsFromDatabase.Value
                : parentObject != null && parentObject.ID != null;
            ISingleColumnUniqueKeyMutable retval
                = new SingleColumnUniqueKeyMutable(
                    Name
                    , SqlTranslator
                    , Table
                    , ObjectType
                    , PhysicalObjectType
                    , ConstrainedColumn
                    , ColumnNames
                    , IsClustered
                    , NameWhenPrefixed
                    , ID
                    , retrieveIDsFromDatabase: false
                    , assert: false
                    );
            if (retrieveIDs) retval.RetrieveIDsFromDatabase(assert: false);
            if (assert) Debug.Assert(retval.PassesSelfAssertion());
            return retval;
        }

    }

    /// <summary>
    /// Boilerplate implementation of ISingleColumnUniqueKey without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>PotentiallySingleColumnConstraintImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetCopyForColumn, always unless abstract class.</item>
    ///   <item>GetCopyForTable, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public class SingleColumnUniqueKeyImmutable : UniqueKeyImmutable, ISingleColumnUniqueKey
    {

        private ISingleColumnUniqueKey Delegee { get { return (ISingleColumnUniqueKey)_delegee; } }

        public SingleColumnUniqueKeyImmutable(
            string name
            , ISqlTranslator sqlTranslator
            , ITable table
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , ITableColumn constrainedColumn = null
            , List<string> columnNames = null
            , bool isClustered = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base()
        {
            _delegee
                = (ISingleColumnUniqueKey)
                new SingleColumnUniqueKeyMutable(
                    name
                    , sqlTranslator
                    , table
                    , objectType
                    , physicalObjectType
                    , constrainedColumn
                    , columnNames
                    , isClustered
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    ).GetSafeReference();
            CompleteConstructionIfType(typeof(SingleColumnUniqueKeyImmutable), retrieveIDsFromDatabase: false, assert: assert);
        }

        public SingleColumnUniqueKeyImmutable(
            string name
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , ITableColumn constrainedColumn = null
            , List<string> columnNames = null
            , bool isClustered = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base()
        {
            _delegee
                = (ISingleColumnUniqueKey)
                new SingleColumnUniqueKeyMutable(
                    name
                    , objectType
                    , physicalObjectType
                    , constrainedColumn
                    , columnNames
                    , isClustered
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    ).GetSafeReference();
            CompleteConstructionIfType(typeof(SingleColumnUniqueKeyImmutable), retrieveIDsFromDatabase: false, assert: assert);
        }

        public SingleColumnUniqueKeyImmutable(
            IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , ITableColumn constrainedColumn = null
            , List<string> columnNames = null
            , bool isClustered = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base()
        {
            _delegee
                = (ISingleColumnUniqueKey)
                new SingleColumnUniqueKeyMutable(
                    objectType
                    , physicalObjectType
                    , constrainedColumn
                    , columnNames
                    , isClustered
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    ).GetSafeReference();
            CompleteConstructionIfType(typeof(SingleColumnUniqueKeyImmutable), retrieveIDsFromDatabase: false, assert: assert);
        }

        public SingleColumnUniqueKeyImmutable(
            ISingleColumnUniqueKey delegee
            , bool assert = true
            ) : base(delegee, false)
        {
            CompleteConstructionIfType(typeof(SingleColumnUniqueKeyImmutable), retrieveIDsFromDatabase: false);
        }

        // DataProcObjectImmutable method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(ISingleColumnUniqueKey); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return
                new SingleColumnUniqueKeyImmutable(
                    (ISingleColumnUniqueKey)
                    Delegee.GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, false)
                    );
        }

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return
                new SingleColumnUniqueKeyImmutable(
                    (ISingleColumnUniqueKey)
                    Delegee.GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, false)
                    );
        }

        // PotentiallySingleColumnConstraintImmutable method overrides

        public override IPotentiallySingleColumnConstraint GetCopyForColumn(ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return new SingleColumnUniqueKeyImmutable(
                (ISingleColumnUniqueKey)
                Delegee.GetCopyForColumn(
                    constrainedColumn
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }
        public override IPotentiallySingleColumnConstraint GetCopyForTable(ITable table, ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return new SingleColumnUniqueKeyImmutable(
                (ISingleColumnUniqueKey)
                Delegee.GetCopyForTable(
                    table
                    , constrainedColumn
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }

    }

}
