﻿using System;
using System.Collections.Generic;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface IConnection : ISelfAsserter, IDisposable
    {
        bool IsOpen { get; }
        void Open();
        void Close();
        object NativeConnection { get; }
        ICommand GetNewCommand(
            string commandText
            , ITransaction transaction = null
            , List<ISqlParameter> parameters = null
            );
        IStoredProcedureCommand GetNewStoredProcedure(string commandText, List<ISqlParameter> parameters, ITransaction transaction = null);
        ITransaction BeginTransaction();
        string DatabaseName();
    }

    // Note: we don't have a default implementation of this because
    //   any implementation is vendor-specific.

}
