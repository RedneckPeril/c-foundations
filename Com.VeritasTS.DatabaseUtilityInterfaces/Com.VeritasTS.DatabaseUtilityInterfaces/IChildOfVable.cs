﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// Base interface for database objects whose
    /// ParentObject is an IVable.
    /// </summary>
    /// <remarks>
    /// Extends IChildOfObjectInSchema.
    /// </remarks>
    public interface IChildOfVable : IChildOfObjectInSchema
    {
        IVable Vable { get; }
        IVable GetVableSafely();
        IChildOfVable GetReferenceForVable(IVable vable, bool? retrieveIDsFromDatabase = null, bool assert = true);
        IChildOfVable GetCopyForVable(IVable vable, bool? retrieveIDsFromDatabase = null, bool assert = true);
        IChildOfVable GetSafeReferenceForVable(IVable vable, bool? retrieveIDsFromDatabase = null, bool assert = true);
    }

    /// <summary>
    /// Base interface for database objects whose
    /// ParentObject is an IVable and that can be altered
    /// after construction.
    /// </summary>
    /// <remarks>
    /// Merely adds variability to IChildOfVable.
    /// <para>
    /// Extends IChildOfObjectInSchemaMutable.</para>
    /// </remarks>
    public interface IChildOfVableMutable : IChildOfVable, IChildOfObjectInSchemaMutable
    {
        void SetVable(
            IVable value
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            );
    }

    /// <summary>
    /// Boilerplate implementation of IChildOfVableMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides.</item>
    ///   <list type="bullet">
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides.</item>
    ///   <list type="bullet">
    ///   <item>GetCopyForOwner, always if not abstract type.</item>
    ///   <item>ValidOwnerTypesWhenNotOwner, occasionally if not abstract type
    ///   and if Owner is not required to be ParentObject.</item>
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>SetOwner, rarely, e.g. when Owner should always be ParentObject 
    /// and vice versa.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaMutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidParentObjectTypes, usually unless abstract type.</item>
    ///   <item>ParentObjectMustBeOwner, if true rather than false.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always.</item>
    ///   <item>SetParentObject, rarely, e.g. when ParentObject should always be some
    ///   member other than Owner and vice versa.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    abstract public class ChildOfVableMutable : ChildOfObjectInSchemaMutable, IChildOfVableMutable
    {

        private IDatabaseObject _parentObject;

        protected ChildOfVableMutable() : base() { }

        public ChildOfVableMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , IVable vable = null
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , IDataProcObject owner = null
            , string nameWhenPrefixed = null
            , int? id = null
            ) : base(
                name
                , sqlTranslator
                , vable
                , objectType
                , physicalObjectType
                , owner
                , nameWhenPrefixed
                , id
            )
        { }

        // DataProcObjectMutable method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (IsFullyConstructed)
            {
                Debug.Assert(base.PassesSelfAssertion());
                if (ParentObject != null)
                    Debug.Assert(
                        typeof(IVable).IsAssignableFrom(GetType())
                        , "The parent object of an IChildOfVable object "
                        + "must be an IVable."
                        );
            }
            return true;
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , vable: null
                , objectType: null
                , physicalObjectType: null
                , owner: null
                , nameWhenPrefixed: null
                , id: default(int?)
                );
        }
        
        protected virtual void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator
            , IVable vable
            , IObjectType objectType
            , IObjectType physicalObjectType
            , IDataProcObject owner
            , string nameWhenPrefixed
            , int? id
            )
        {
            base.ConstructAfterConstruction(
                name
                , sqlTranslator
                , vable
                , objectType
                , physicalObjectType
                , owner
                , nameWhenPrefixed
                , id
                );
        }

        // IDatabaseObject method overrides

        public override List<Type> ValidOwnerTypesWhenNotParentObject
        {
            get { return new List<Type>(); }
        }

        public override IDatabaseObject GetCopyForOwner(
            IDataProcObject owner
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true)
        {
            RejectIfInvalidOwnerType(owner);
            return GetCopyForParentObject((IDatabaseObject)owner);
        }

        // ObjectInSchema method overrides

        // IChildOfVable methods

        public IVable Vable { get { return (ITable)ParentObject; } }
        public IVable GetVableSafely() { return (ITable)GetParentObjectSafely(); }
        public IChildOfVable GetReferenceForVable(IVable vable, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return (IChildOfVable)GetReferenceForParentObject(vable, retrieveIDsFromDatabase, assert);
        }
        public IChildOfVable GetCopyForVable(IVable vable, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return (IChildOfVable)GetCopyForParentObject(vable, retrieveIDsFromDatabase, assert);
        }
        public IChildOfVable GetSafeReferenceForVable(IVable vable, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return (IChildOfVable)GetSafeReferenceForParentObject(vable, retrieveIDsFromDatabase, assert);
        }

        // IChildOfVableMutable methods

        public void SetVable(
            IVable value
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            SetParentObject(value, retrieveIDsFromDatabase, assert);
        }

    }

    /// <summary>
    /// Boilerplate implementation of IChildOfVable without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    abstract public class ChildOfVableImmutable : ChildOfObjectInSchemaImmutable, IChildOfVable
    {

        private IChildOfVable Delegee { get { return (IChildOfVable)_delegee; } }

        protected ChildOfVableImmutable() : base() { }

        public ChildOfVableImmutable(
            IChildOfVable _delegee
            , bool assert = true
            ) : base( _delegee, false)
        {
            CompleteConstructionIfType(typeof(ChildOfVableImmutable), false, assert);
        }

        // IDatabaseObject method overrides

        public override IDatabaseObject GetReferenceForOwner(
            IDataProcObject owner
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true)
        {
            RejectIfInvalidOwnerType(owner);
            return (IChildOfVable)GetReferenceForParentObject((IVable)owner, retrieveIDsFromDatabase, assert);
        }

        public override IDatabaseObject GetCopyForOwner(
            IDataProcObject owner
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true)
        {
            RejectIfInvalidOwnerType(owner);
            return (IChildOfVable)GetCopyForParentObject((IVable)owner, retrieveIDsFromDatabase, assert);
        }

        // ObjectInSchema method overrides

        // IChildOfVableMutable methods

        public IVable Vable { get { return (ITable)ParentObject; } }
        public IVable GetVableSafely() { return (ITable)GetParentObjectSafely(); }
        public IChildOfVable GetReferenceForVable(IVable vable, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return (IChildOfVable)GetReferenceForParentObject(vable, retrieveIDsFromDatabase, assert);
        }
        public IChildOfVable GetCopyForVable(IVable vable, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return (IChildOfVable)GetCopyForParentObject(vable, retrieveIDsFromDatabase, assert);
        }
        public IChildOfVable GetSafeReferenceForVable(IVable vable, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return (IChildOfVable)GetSafeReferenceForParentObject(vable, retrieveIDsFromDatabase, assert);
        }

    }

}
