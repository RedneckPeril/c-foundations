﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public static class CommUtils
    {
        public static Type GetTypeByName(string typeName)
        {
            Type type = Type.GetType(typeName);
            if (type == null)
            {
                foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
                {
                    string aName = a.FullName.Substring(0, a.FullName.IndexOf(","));
                    type = a.GetType(aName + "." + typeName);
                    if (type != null)
                        break;
                }
            }
            return type;
        }
        public static ITable GetTableByTableName(string typeName)
        {
            Type type = GetTypeByName(typeName);
            if (type != null)
                return (ITable)Activator.CreateInstance(type);
            else
                return null;
        }

        public static int GetRandomNumber(int startNum, int endNum)
        {
            Random random = new Random();
            return random.Next(startNum, endNum);
        }

        public static void GetClone<T>(T source, ref T target)
        {
            Type type = source.GetType();
            foreach (var prop in type.GetProperties())
            {
                prop.SetValue(target, prop.GetValue(source));
            }
        }

        public static string GetColumnList(List<IColumn> columns, string OldNewEtc)
        {
            string list = "";
            foreach (var c in columns)
                list += OldNewEtc + c.Name + ",";
            return list.Substring(0, list.Length - 1);
        }
    }

}
