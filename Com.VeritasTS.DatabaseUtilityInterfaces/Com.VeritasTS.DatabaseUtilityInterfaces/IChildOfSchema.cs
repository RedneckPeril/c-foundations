﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// Base interface for database objects whose
    /// ParentObject is an IObjectInSchema.
    /// </summary>
    /// <remarks>
    /// Extends IObjectInSchema.
    /// </remarks>
    public interface IChildOfSchema : IObjectInSchema { }

    /// <summary>
    /// Base interface for database objects whose
    /// ParentObject is an IObjectInSchema and that can be altered
    /// after construction.
    /// </summary>
    /// <remarks>
    /// Merely adds variability to IChildOfSchema.
    /// <para>
    /// Extends IObjectInSchemaMutable.</para>
    /// </remarks>
    public interface IChildOfSchemaMutable : IChildOfSchema, IObjectInSchemaMutable { }

    /// <summary>
    /// Boilerplate implementation of IChildOfSchemaMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidOwnerTypesWhenNotOwner, occasionally if not abstract type
    ///   and if Owner is not required to be ParentObject.</item>
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>SetOwner, rarely, e.g. when Owner should always be ParentObject 
    /// and vice versa.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>ParentObjectMustBeOwner, if true rather than false.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaMutable method overrides
    ///   <list type="bullet">
    ///   <item>SetParentObject, rarely, e.g. when Owner should always be ParentObject 
    /// and vice versa.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    abstract public class ChildOfSchemaMutable : ObjectInSchemaMutable, IChildOfSchemaMutable
    {

        private IDatabaseObject _parentObject;

        // Constructors

        /// <summary>
        /// Default constructor, which may not fully
        /// construct the object into a useful state,
        /// depending upon how a specific descent class
        /// is designed.
        /// </summary>
        /// <remarks>
        /// The default constructor is used by frameworks and does
        /// NOT mark the object has having been fully constructed.
        /// This means that assertions do not fire until the 
        /// creator explicitly completes the construction, allowing
        /// for objects to be constructed from the class name only
        /// using the null constructor, then initialized with
        /// Set method calls, and finally marked as ready for use.
        /// </remarks>
        protected ChildOfSchemaMutable() : base() { }

        public ChildOfSchemaMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ISchema schema = null
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , IDataProcObject owner = null
            , string nameWhenPrefixed = null
            , int? id = null
            ) : base(
                name
                , sqlTranslator
                , schema
                , objectType
                , physicalObjectType
                , owner
                , nameWhenPrefixed
                , id
            )
        { }

        // DataProcObjectMutable overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (IsFullyConstructed)
            {
                Debug.Assert(base.PassesSelfAssertion());
                if (ParentObject != null)
                    Debug.Assert(
                        typeof(ISchema).IsAssignableFrom(GetType())
                        , "The parent object of an IChildOfSchema object "
                        + "must be an ISchema."
                        );
            }
            return true;
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , schema: null
                , objectType: null
                , physicalObjectType: null
                , owner: null
                , nameWhenPrefixed: null
                , id: default(int?)
                );
        }

        protected virtual void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator
            , ISchema schema
            , IObjectType objectType
            , IObjectType physicalObjectType
            , IDataProcObject owner
            , string nameWhenPrefixed
            , int? id
            )
        {
            base.ConstructAfterConstruction(
                name
                , sqlTranslator
                , schema
                , objectType
                , physicalObjectType
                , owner
                , nameWhenPrefixed
                , id
                );
        }

        // DatabaseObjectMutable overrides

        public override ISchema Schema { get { return (ISchema)ParentObject; } }

        // ObjectInSchemaMutable method overrides

        public override List<Type> ValidParentObjectTypes
        {
            get { return new List<Type> { typeof(ISchema) }; }
        }

    }

    /// <summary>
    /// Boilerplate implementation of IChildOfObjectInSchema without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    abstract public class ChildOfSchemaImmutable : ObjectInSchemaImmutable, IChildOfSchema
    {

        private IChildOfSchema Delegee { get { return (IChildOfSchema)_delegee; } }

        // IChildDatabaseObject method overrides

        public override IObjectInSchema GetReferenceForSchema(ISchema schema, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return GetReferenceForParentObject(schema, retrieveIDsFromDatabase: false, assert: false);
        }
        public override IObjectInSchema GetCopyForSchema(ISchema schema, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return GetCopyForParentObject(schema, retrieveIDsFromDatabase: false, assert: false);
        }

    }

}
