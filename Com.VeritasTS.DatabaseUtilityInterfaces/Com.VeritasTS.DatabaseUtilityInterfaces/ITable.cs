﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// Basic interface for standard RDBMS table.
    /// </summary>
    /// <remarks>
    /// We care about which process has the right
    /// to alter data in the table. These processes
    /// are either user interfaces (which includes
    /// direct population through SQL statements by
    /// script) or queues. The general rule is that
    /// we should always know which rows are which --
    /// because there will be cases in which a table 
    /// is partly populated by UI and partly by multiple
    /// independent queues.
    /// <para>
    /// So we have a dictionary of ITableInputters,
    /// each of which is associated with a set of key
    /// columns from the table that serve as job
    /// identifiers for a particular ITableInputter.
    /// There is also the IInputterSpecificationColumn;
    /// this column determines which ITableInputter owns
    /// a particular row. If there is no such column,
    /// then the dictionary of ITableInputters can
    /// have at most one entry; if the dictionary
    /// is empty then it is assumed that the table
    /// is populated entirely by a UI.
    /// </para>
    /// </remarks>
    public interface ITable : IVable
    {

        Dictionary<string, Tuple<int, ITableColumn>> TableColumns { get; }
        bool HasIdentityColumn { get; }
        IIdentityColumn IdentityColumn { get; }
        string IdentityColumnName { get; }
        bool HasPrimaryKey { get; }
        IPrimaryKey PrimaryKey { get; }
        Dictionary<string, IUniqueKey> UniqueKeys { get; }
        bool HasNonPrimaryUniqueKeys { get; }
        Dictionary<string, IUniqueKey> NonPrimaryUniqueKeys { get; }
        Dictionary<string, IForeignKey> ForeignKeys { get; }
        ICascadingForeignKeyRegistry CascadingForeignKeyRegistry { get; }
        List<ICascadingForeignKey> CascadingOnDeleteForeignKeys { get; }
        List<ICascadingForeignKey> CascadingOnUpdateForeignKeys { get; }
        Dictionary<string, ITableTrigger> Triggers { get; }
        Dictionary<string, ICheckConstraint> CheckConstraints { get; }
        Dictionary<string, IDefaultConstraint> DefaultConstraints { get; }
        bool IsAudited { get; }
        string AuditTableName { get; }
        IAuditTable AuditTable { get; }
        Dictionary<string, Tuple<int, ITableColumn>> InsertableColumns { get; }
        Dictionary<string, Tuple<int, ITableColumn>> UpdatableColumns { get; }
        bool PublishesChanges { get; }
        IChangeTrackingTable PublishedChangesTable { get; }
        string PublishedChangesTableName { get; }
        IChangeSubscriberRegistry ChangeSubscriberRegistry { get; }
        ITable ParentTable { get; }
        Dictionary<string, string> ParentInsertColumnListExpressions { get; }
        // IParentPassThroughInfo ParentPassThrough { get; }
        bool CanHaveChildren { get; }
        IHomeVableNameColumn HomeVableNameColumn { get; }
        string LeavesTableName { get; }
        ILeavesTable LeavesTable { get; }
        bool MaintainChildVablesTable { get; }
        string ChildVablesTableName { get; }
        IChildVablesTable ChildVablesTable { get; }
        bool HasIDColumn { get; }
        ITableIDColumn IDColumn { get; }
        string IDColumnName { get; }
        string TempInsertedIDBaseColumnName { get; }
        bool HasInputterColumn { get; }
        IInputterColumn InputterColumn { get; }
        string InputterColumnName { get; }
        IValidInputtersTable ValidInputtersTable { get; }
        string ValidInputtersTableName { get; }
        bool HasLastUpdateDtmColumn { get; }
        ILastUpdateDtmColumn LastUpdateDtmColumn { get; }
        string LastUpdateDtmColumnName { get; }
        bool HasLastUpdaterColumn { get; }
        ILastUpdaterColumn LastUpdaterColumn { get; }
        string LastUpdaterColumnName { get; }
        Dictionary<string, Tuple<int, ITableColumn>> GetTableColumnsSafely();
        IIdentityColumn GetIdentityColumnSafely();
        IPrimaryKey GetPrimaryKeySafely();
        Dictionary<string, IUniqueKey> GetUniqueKeysSafely();
        Dictionary<string, IUniqueKey> GetNonPrimaryUniqueKeysSafely();
        Dictionary<string, IForeignKey> GetForeignKeysSafely();
        ICascadingForeignKeyRegistry GetCascadingForeignKeyRegistrySafely();
        List<ICascadingForeignKey> GetCascadingOnDeleteForeignKeysSafely();
        List<ICascadingForeignKey> GetCascadingOnUpdateForeignKeysSafely();
        Dictionary<string, ITableTrigger> GetTriggersSafely();
        Dictionary<string, ICheckConstraint> GetCheckConstraintsSafely();
        Dictionary<string, IDefaultConstraint> GetDefaultConstraintsSafely();
        IAuditTable GetAuditTableSafely();
        Dictionary<string, Tuple<int, ITableColumn>> GetInsertableColumnsSafely();
        Dictionary<string, Tuple<int, ITableColumn>> GetUpdatableColumnsSafely();
        IChangeTrackingTable GetPublishedChangesTableSafely();
        IChangeSubscriberRegistry GetChangeSubscriberRegistrySafely();
        ITable GetParentTableSafely();
        Dictionary<string, string> GetParentInsertColumnListExpressionsSafely();
        // IParentPassThroughInfo GetParentPassThroughSafely();
        IHomeVableNameColumn GetHomeVableNameColumnSafely();
        ILeavesTable GetLeavesTableSafely();
        IChildVablesTable GetChildVablesTableSafely();
        ITableIDColumn GetIDColumnSafely();
        IInputterColumn GetInputterColumnSafely();
        IValidInputtersTable GetValidInputtersTableSafely();
        ILastUpdateDtmColumn GetLastUpdateDtmColumnSafely();
        ILastUpdaterColumn GetLastUpdaterColumnSafely();
        string InsertColumnListSql(
            bool useLineBreaks = false
            , bool includeDefs = false
            , bool includeDefaultColumnConstraints = false
            , bool applyTrims = false
            , bool includeMutableColumns = true
            , bool prettify = false
            , int minDatatypeOffset = 0
            , int minNullabilityOffset = 0
            , int spacesToIndent = 0
            );
        string UpdateColumnListSql();
        string ParentColumnListInsertExpressions(
            bool useLineBreaks = false
            , int spacesToIndent = 0
            );

        // Trigger methods

        void PerformInsteadOfInsertTriggerLogic(IConnection conn);
        void PerformInsteadOfInsertTriggerLogicWithInsertedTable(IConnection conn);
        void PerformInsteadOfInsertTriggerLogicForEachRow(IConnection conn);
        void PerformInsteadOfUpdateTriggerLogic(IConnection conn);
        void PerformInsteadOfUpdateTriggerLogicWithInsertedTable(IConnection conn);
        void PerformInsteadOfUpdateTriggerLogicForEachRow(IConnection conn);
        void PerformInsteadOfDeleteTriggerLogic(IConnection conn);
        void PerformInsteadOfDeleteTriggerLogicWithInsertedTable(IConnection conn);
        void PerformInsteadOfDeleteTriggerLogicForEachRow(IConnection conn);
        void PerformInsteadOfInsertUpdateTriggerLogic(IConnection conn);
        void PerformInsteadOfInsertUpdateTriggerLogicWithInsertedTable(IConnection conn);
        void PerformInsteadOfInsertUpdateTriggerLogicForEachRow(IConnection conn);
        void PerformInsteadOfAnythingTriggerLogic(IConnection conn);
        void PerformInsteadOfAnythingTriggerLogicWithInsertedTable(IConnection conn);
        void PerformInsteadOfAnythingTriggerLogicForEachRow(IConnection conn);
        void PerformBeforeInsertTriggerLogic(IConnection conn);
        void PerformBeforeInsertTriggerLogicWithInsertedTable(IConnection conn);
        void PerformBeforeInsertTriggerLogicForEachRow(IConnection conn);
        void PerformBeforeUpdateTriggerLogic(IConnection conn);
        void PerformBeforeUpdateTriggerLogicWithInsertedTable(IConnection conn);
        void PerformBeforeUpdateTriggerLogicForEachRow(IConnection conn);
        void PerformBeforeDeleteTriggerLogic(IConnection conn);
        void PerformBeforeDeleteTriggerLogicWithInsertedTable(IConnection conn);
        void PerformBeforeDeleteTriggerLogicForEachRow(IConnection conn);
        void PerformBeforeInsertUpdateTriggerLogic(IConnection conn);
        void PerformBeforeInsertUpdateTriggerLogicWithInsertedTable(IConnection conn);
        void PerformBeforeInsertUpdateTriggerLogicForEachRow(IConnection conn);
        void PerformBeforeAnythingTriggerLogic(IConnection conn);
        void PerformBeforeAnythingTriggerLogicWithInsertedTable(IConnection conn);
        void PerformBeforeAnythingTriggerLogicForEachRow(IConnection conn);
        void PerformAfterInsertTriggerLogic(IConnection conn);
        void PerformAfterInsertTriggerLogicWithInsertedTable(IConnection conn);
        void PerformAfterInsertTriggerLogicForEachRow(IConnection conn);
        void PerformAfterUpdateTriggerLogic(IConnection conn);
        void PerformAfterUpdateTriggerLogicWithInsertedTable(IConnection conn);
        void PerformAfterUpdateTriggerLogicForEachRow(IConnection conn);
        void PerformAfterDeleteTriggerLogic(IConnection conn);
        void PerformAfterDeleteTriggerLogicWithInsertedTable(IConnection conn);
        void PerformAfterDeleteTriggerLogicForEachRow(IConnection conn);
        void PerformAfterInsertUpdateTriggerLogic(IConnection conn);
        void PerformAfterInsertUpdateTriggerLogicWithInsertedTable(IConnection conn);
        void PerformAfterInsertUpdateTriggerLogicForEachRow(IConnection conn);
        void PerformAfterAnythingTriggerLogic(IConnection conn);
        void PerformAfterAnythingTriggerLogicWithInsertedTable(IConnection conn);
        void PerformAfterAnythingTriggerLogicForEachRow(IConnection conn);
        string TriggerTimingTag(TriggerTimings timing);
        string LFAndLeadingSpaces(int spacesToIndent);
        string TriggerTableLoadSql(
            string tempTableName
            , bool includeMutableColumns
            , int spacesToIndent = 0
            );
        int? ForcedUseOfTempTriggerTableThreshold(TriggerTimings timing);
        // Support for before-insert triggers
        string NameOfNewlyCreatedInsertedTriggerTable(
            string tempTableTag
            , TriggerTimings timing
            , IConnection conn
            );
        string InsertedTriggerTableCreateSql(
            string tempTableName
            , bool globallyAvailable
            , bool createInMemoryTable
            , TriggerTimings timing
            , int spacesToIndent = 0
            );
        void LoadInsertedTriggerTable(
            string tempTableName
            , IConnection conn
            );
        string InsertedTriggerTableLoadSql(string tempTableName, int spacesToIndent = 0);
        string IndexTempTableByIdentityColumnSql(
            string tempTableName
            , string tempTableIndexName = null
            );
        string IndexTempTableByIDColumnSql(
            string tempTableName
            , string tempTableIndexName = null
            );
        string InsertedTriggerTableAddToParentSql(
            string tempTableName
            , int spacesToIndent = 0
            );
        string InsertFromInsertedTriggerTableSql(
            string tempTableName
            , string currentTimestampSql
            , string userSql
            , int spacesToIndent = 0
            );
        void LoadChangeTableAfterInsert(
            IChangeTrackingTable changeTable
            , string updateDtmSql
            , string updaterSql
            , string tempInsertedName
            , IConnection conn
            );
        // Support for before-update triggers
        string NameOfNewlyCreatedDeletedTriggerTable(
            string tempTableTag
            , bool includeMutableColumns
            , TriggerTimings timing
            , IConnection conn
            );
        string DeletedTriggerTableCreateSql(
            string tempTableName
            , bool includeMutableColumns
            , bool globallyAvailable
            , bool createInMemoryTable
            , TriggerTimings timing
            , int spacesToIndent = 0
            );
        string NameOfNewlyCreatedMergedTriggerTable(
            string tempTableTag
            , IConnection conn
            );
        string MergedTriggerTableCreateSql(
            string tempTableName
            , bool globallyAvailable
            , bool createInMemoryTable
            , bool prettify = false
            , int minDatatypeOffset = 0
            , int minNullabilityOffset = 0
            , int spacesToIndent = 0
            );
        void LoadDeletedTriggerTable(
            string tempTableName
            , bool includeMutableColumns
            , IConnection conn
            );
        string DeletedTriggerTableLoadSql(
            string tempTableName
            , bool includeMutableColumns
            , int spacesToIndent = 0
            );
        void LoadMergedTriggerTable(
            string tempMergedName
            , bool tempInsertedName
            , bool tempDeletedName
            , IConnection conn
            );
        string MergedTriggerTableLoadSql(
            string tempMergedName
            , bool tempInsertedName
            , bool tempDeletedName
            , int spacesToIndent = 0
            );
        void InterceptUpdateOfImmutableColumn(
            string tempMergedTableName
            , IConnection conn
            );
        void InterceptUpdateOfImmutableColumn(
            string tempInsertedName
            , string tempDeletedName
            , IConnection conn
            );
        string UpdateFromInsertedTriggerTableSql(
            string tempTableName
            , string currentTimestampSql
            , string userSql
            , int spacesToIndent = 0
            );
        void CascadeUpdate(
            ICascadingForeignKeyRelationship relationship
            , string tempMergedName
            , IConnection conn
            );
        void CascadeUpdate(
            ICascadingForeignKeyRelationship relationship
            , string tempInsertedName
            , string tempDeletedName
            , IConnection conn
            );
        void LoadChangeTableAfterUpdate(
            IChangeTrackingTable changeTable
            , string updateDtmSql
            , string updaterSql
            , string tempMergedTableName
            , IConnection conn
            );
        void LoadChangeTableAfterUpdate(
            IChangeTrackingTable changeTable
            , string updateDtmSql
            , string updaterSql
            , string tempInsertedName
            , string tempDeletedName
            , IConnection conn
            );
        // Support for before-delete triggers
        string DeleteFromDeletedTriggerTableSql(
            string tempTableName
            , int spacesToIndent = 0
            );
        void CascadeDelete(
            ICascadingForeignKeyRelationship relationship
            , string tempDeletedName
            , IConnection conn
            );
        void LoadChangeTableAfterDelete(
            IChangeTrackingTable changeTable
            , string updateDtmSql
            , string updaterSql
            , string tempDeletedName
            , IConnection conn
            );

    }

    public interface ITableMutable : ITable, IVableMutable
    {
        void SetTableColumns(
            Dictionary<string, Tuple<int, ITableColumn>> value
            , bool? retrieveColumnIDsFromDatabase = default(bool?)
            , bool assert = true
            );
        void SetHasIdentityColumn(
            bool value
            , IIdentityColumn identityColumn = null
            , bool? retrieveIdentityColumnIDFromDatabase = default(bool?)
            , bool assert = true
            );
        void SetIdentityColumn(
            IIdentityColumn value
            , bool? retrieveIdentityColumnIDFromDatabase = default(bool?)
            , bool assert = true
            );
        void SetPrimaryKey(
            IPrimaryKey value
            , bool? retrievePrimaryKeyIDFromDatabase = null
            , bool assert = true
            );
        void SetUniqueKeys(
            Dictionary<string, IUniqueKey> value
            , bool? retrieveUniqueKeyIDsFromDatabase = null
            , bool assert = true
            );
        void AddOrOverwriteUniqueKey(
            IUniqueKey key
            , bool? retrieveUniqueKeyIDFromDatabase = null
            , bool assert = true
            );
        bool RemoveUniqueKey(string nameOfDoomed, bool assert = true);
        bool RemoveUniqueKey(IUniqueKey doomed, bool assert = true);
        void SetForeignKeys(
            Dictionary<string, IForeignKey> value
            , ICascadingForeignKeyRegistry cascadingForeignKeyRegistry = null
            , bool? retrieveIDsFromDatabase = null
            , bool assert = true
            );
        void SetCascadingForeignKeyRegistry(
            ICascadingForeignKeyRegistry value
            , bool assert = true
            );
        void AddOrOverwriteForeignKey(
            IForeignKey key
            , bool? retrieveForeignKeyIDFromDatabase = null
            , bool assert = true
            );
        bool RemoveForeignKey(string nameOfDoomed, bool assert = true);
        bool RemoveForeignKey(IForeignKey doomed, bool assert = true);
        void SetTriggers(
            Dictionary<string, ITableTrigger> value
            , bool? retrieveTriggerIDsFromDatabase = null
            , bool assert = true
            );
        void AddOrOverwriteTrigger(
            ITableTrigger trigger
            , bool? retrieveTriggerIDFromDatabase = null
            , bool assert = true
            );
        bool RemoveTrigger(string nameOfDoomed, bool assert = true);
        bool RemoveTrigger(ITableTrigger doomed, bool assert = true);
        void SetCheckConstraints(
            Dictionary<string, ICheckConstraint> value
            , bool? retrieveConstraintIDsFromDatabase = null
            , bool assert = true
            );
        void AddOrOverwriteCheckConstraint(
            ICheckConstraint constraint
            , bool? retrieveConstraintIDFromDatabase = null
            , bool assert = true
            );
        bool RemoveCheckConstraint(string nameOfDoomed, bool assert = true);
        bool RemoveCheckConstraint(ICheckConstraint doomed, bool assert = true);
        void SetDefaultConstraints(
            Dictionary<string, IDefaultConstraint> value
            , bool? retrieveConstraintIDsFromDatabase = null
            , bool assert = true
            );
        void AddOrOverwriteDefaultConstraint(
            IDefaultConstraint constraint
            , bool? retrieveConstraintIDFromDatabase = null
            , bool assert = true
            );
        bool RemoveDefaultConstraint(string nameOfDoomed, bool assert = true);
        bool RemoveDefaultConstraint(IDefaultConstraint doomed, bool assert = true);
        void SetIsAudited(
            bool value
            , IAuditTable auditTable = null
            , bool? retrieveAuditTableIDFromDatabase = null
            , bool assert = true
            );
        void SetPublishesChanges(
            bool value
            , IChangeTrackingTable publishedChangesTable = null
            , IChangeSubscriberRegistry changeSubscriberRegistry = null
            , bool? retrieveChangeTrackingTableIDFromDatabase = null
            , bool assert = true
            );
        void SetChangeSubscriberRegistry(
            IChangeSubscriberRegistry value
            , bool assert = true
            );
        void SetParentTable(
            ITable value
            , Dictionary<string, string> parentInsertColumnListExpressions = null
            , bool? retrieveParentTableIDFromDatabase = null
            , bool assert = true
            );
        void SetParentInsertColumnListExpressions(
            Dictionary<string, string> value
            , bool assert = true
            );
        // void SetParentPassThrough(IParentPassThroughInfo value, bool assert = true);
        void SetCanHaveChildren(
            bool value
            , ILeavesTable leavesTable = null
            , bool? retrieveLeavesTableIDFromDatabase = null
            , bool assert = true
            );
        void SetMaintainChildVablesTable(
            bool value
            , IChildVablesTable childVablesTable = null
            , bool? retrieveChildVablesTableIDFromDatabase = null
            , bool assert = true
            );
        void SetHasIDColumn(
            bool value
            , ITableIDColumn idColumn = null
            , bool? retrieveIDColumnIDFromDatabase = null
            , bool assert = true
            );
        void SetHasInputterColumn(
            bool value
            , IInputterColumn inputterColumn = null
            , IValidInputtersTable validInputtersTable = null
            , bool? retrieveValidInputtersTableIDFromDatabase = null
            , bool assert = true
            );
        void SetHasLastUpdateDtmColumn(
            bool value
            , bool? retrieveLastUpdateDtmColumnIDFromDatabase = null
            , bool assert = true
            );
        void SetHasLastUpdaterColumn(
            bool value
            , bool? retrieveLastUpdaterColumnIDFromDatabase = null
            , bool assert = true
            );
        void RefreshColumnsFromDb(bool assert = true);
    }

    /// <summary>
    /// Boilerplate implementation of ITableMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>GetCopyForOwner, always unless abstract class.</item>
    ///   <item>ValidOwnerTypesWhenNotOwner, occasionally if not abstract type
    ///   and if Owner is not required to be ParentObject.</item>
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>ParentObjectMustBeOwner, if true rather than false.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class TableMutable : VableMutable, ITableMutable
    {

        /***************************
         * Private properties and members
        ***************************/

        private string FullTypeNameForCodeErrors { get { return typeof(TableMutable).FullName; } }

        private Dictionary<string, IUniqueKey> _uniqueKeys;
        private Dictionary<string, IForeignKey> _foreignKeys;
        private ICascadingForeignKeyRegistry _cascadingForeignKeyRegistry;
        private Dictionary<string, ITableTrigger> _triggers;
        private Dictionary<string, ICheckConstraint> _checkConstraints;
        private Dictionary<string, IDefaultConstraint> _defaultConstraints;
        private IAuditTable _auditTable;
        private IChangeTrackingTable _publishedChangesTable;
        private IChangeSubscriberRegistry _changeSubscriberRegistry;
        private ITable _parentTable;
        private Dictionary<string, string> _parentInsertColumnListExpressions;
        // private IParentPassThroughInfo _parentPassThrough;
        private IChildVablesTable _childVablesTable;
        private ILeavesTable _leavesTable;
        private IValidInputtersTable _validInputtersTable;

        /***************************
         * Constructors
        ***************************/

        /// <summary>
        /// Default constructor, which may not fully
        /// construct the object into a useful state,
        /// depending upon how a specific descent class
        /// is designed.
        /// </summary>
        /// <remarks>
        /// The default constructor is used by frameworks and does
        /// NOT mark the object has having been fully constructed.
        /// This means that assertions do not fire until the 
        /// creator explicitly completes the construction, allowing
        /// for objects to be constructed from the class name only
        /// using the null constructor, then initialized with
        /// Set method calls, and finally marked as ready for use.
        /// </remarks>
        public TableMutable() : base() { }

        public TableMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ISchema schema = null
            , IObjectType objectType = null
            , IDataProcObject owner = null
            , bool allowsUpdates = true
            , bool allowsDeletes = true
            , Dictionary<string, Tuple<int, ITableColumn>> columns = null
            , Dictionary<string, IIndex> indexes = null
            , bool hasIdentityColumn = true
            , IIdentityColumn identityColumn = null
            , IPrimaryKey primaryKey = null
            , Dictionary<string, IUniqueKey> uniqueKeys = null
            , Dictionary<string, IForeignKey> foreignKeys = null
            , ICascadingForeignKeyRegistry cascadingForeignKeyRegistry = null
            , Dictionary<string, ITableTrigger> triggers = null
            , Dictionary<string, ICheckConstraint> checkConstraints = null
            , Dictionary<string, IDefaultConstraint> defaultConstraints = null
            , bool isAudited = false
            , IAuditTable auditTable = null
            , bool publishesChanges = false
            , IChangeTrackingTable publishedChangesTable = null
            , IChangeSubscriberRegistry changeSubscriberRegistry = null
            , ITable parentTable = null
            , Dictionary<string, string> parentInsertColumnListExpressions = null
            // , IParentPassThroughInfo parentPassThrough = null
            , bool canHaveChildren = false
            , ILeavesTable leavesTable = null
            , bool maintainChildVablesTable = false
            , IChildVablesTable childVablesTable = null
            , bool hasIDColumn = false
            , ITableIDColumn idColumn = null
            , bool hasInputterColumn = false
            , IInputterColumn inputterColumn = null
            , IValidInputtersTable validInputtersTable = null
            , bool hasLastUpdateDtmColumn = false
            , bool hasLastUpdaterColumn = false
            , string nameWhenPrefixed = null
            , int? id = default(int?)
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , sqlTranslator
                , schema
                , objectType == null ? ObjectTypes.Table() : objectType
                , ObjectTypes.Table()
                , owner
                , allowsUpdates
                , allowsDeletes
                , null
                , null
                , nameWhenPrefixed
                , id
                )
        {
            ConstructNewMembers(
                columns
                , indexes
                , hasIdentityColumn
                , identityColumn
                , primaryKey
                , uniqueKeys
                , foreignKeys
                , cascadingForeignKeyRegistry
                , triggers
                , checkConstraints
                , defaultConstraints
                , isAudited
                , auditTable
                , publishesChanges
                , publishedChangesTable
                , changeSubscriberRegistry
                , parentTable
                , parentInsertColumnListExpressions
                // , parentPassThrough
                , canHaveChildren
                , leavesTable
                , maintainChildVablesTable
                , childVablesTable
                , hasIDColumn
                , idColumn
                , hasInputterColumn
                , inputterColumn
                , validInputtersTable
                , hasLastUpdateDtmColumn
                , hasLastUpdaterColumn
                , true
                );
            if (!retrieveIDsFromDatabase.HasValue || retrieveIDsFromDatabase.Value)
                RetrieveIDsFromDatabase(false);
            CompleteConstructionIfType(typeof(TableMutable), retrieveIDsFromDatabase, assert);
        }

        /***************************
         * Object method overrides and overridden GetHasCode assistants
        ***************************/

        public override int GetHashCode()
        {
            int p1 = NextPrimes[base.HighestPrimeUsedInHashCode];
            int p2 = NextPrimes[p1];
            int p3 = NextPrimes[p2];
            int p4 = NextPrimes[p3];
            int p5 = NextPrimes[p4];
            int p6 = NextPrimes[p5];
            int p7 = NextPrimes[p6];
            int p8 = NextPrimes[p7];
            int p9 = NextPrimes[p8];
            int p10 = NextPrimes[p9];
            int p11 = NextPrimes[p10];
            int p12 = NextPrimes[p11];
            int p13 = NextPrimes[p12];
            int p14 = NextPrimes[p13];
            return base.GetHashCode()
                + p1 * (UniqueKeys == null ? 0 : UniqueKeys.GetHashCode())
                + p2 * (ForeignKeys == null ? 0 : ForeignKeys.GetHashCode())
                + p3 * (CascadingForeignKeyRegistry == null ? 0 : CascadingForeignKeyRegistry.GetHashCode())
                + p4 * (Triggers == null ? 0 : Triggers.GetHashCode())
                + p5 * (CheckConstraints == null ? 0 : CheckConstraints.GetHashCode())
                + p6 * (DefaultConstraints == null ? 0 : DefaultConstraints.GetHashCode())
                + p7 * (AuditTable == null ? 0 : AuditTable.GetHashCode())
                + p8 * (PublishedChangesTable == null ? 0 : PublishedChangesTable.GetHashCode())
                + p9 * (ChangeSubscriberRegistry == null ? 0 : ChangeSubscriberRegistry.GetHashCode())
                + p10 * (ParentTable == null ? 0 : ParentTable.GetHashCode())
                + p11 * (ParentInsertColumnListExpressions == null ? 0 : ParentInsertColumnListExpressions.GetHashCode())
                // + p11 * (ParentPassThrough == null ? 0 : ParentPassThrough.GetHashCode())
                + p12 * (LeavesTable == null ? 0 : LeavesTable.GetHashCode())
                + p13 * (ChildVablesTable == null ? 0 : ChildVablesTable.GetHashCode())
                + p14 * (ValidInputtersTable == null ? 0 : ValidInputtersTable.GetHashCode());
        }

        public override int HighestPrimeUsedInHashCode
        {
            get
            {
                int p1 = NextPrimes[base.HighestPrimeUsedInHashCode];
                int p2 = NextPrimes[p1];
                int p3 = NextPrimes[p2];
                int p4 = NextPrimes[p3];
                int p5 = NextPrimes[p4];
                int p6 = NextPrimes[p5];
                int p7 = NextPrimes[p6];
                int p8 = NextPrimes[p7];
                int p9 = NextPrimes[p8];
                int p10 = NextPrimes[p9];
                int p11 = NextPrimes[p10];
                int p12 = NextPrimes[p11];
                int p13 = NextPrimes[p12];
                return NextPrimes[p13];
            }
        }

        /***************************
         * DataProcObjectMutable method overrides
        ***************************/

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    ObjectType != null
                    , "The ObjectTable property of an ITable "
                    + "cannot be null."
                    );
                Debug.Assert(
                    PhysicalObjectType != null
                    , "The PhysicalObjectTable property of an ITable "
                    + "cannot be null; it must be ObjectTypes.Table()."
                    );
                Debug.Assert(
                    PhysicalObjectType == ObjectTypes.Table()
                    , "The PhysicalObjectTable property of an ITable "
                    + "must be ObjectTypes.Table()."
                    );
                foreach (
                    Tuple<int, IColumn> t
                    in Columns.Values
                    .Where(t => !typeof(ITableColumn).IsAssignableFrom(t.Item2.GetType()))
                    )
                    Debug.Assert(
                        false
                        , "Every column in an ITable must be an ITableColumn."
                        );
                if (PrimaryKey != null)
                    Debug.Assert(PrimaryKey.PassesSelfAssertion());
                Debug.Assert(
                    UniqueKeys != null
                    , "The UniqueKeys property of an IVable can be empty, but cannot be null."
                    );
                foreach (KeyValuePair<string, IUniqueKey> pair in UniqueKeys)
                {
                    Debug.Assert(
                        pair.Value != null
                        , "An ITable cannot contain, in its UniqueKeys dictionary, "
                        + "a pointer to a null IUniqueKey."
                        );
                    Debug.Assert(
                        pair.Key.Equals(pair.Value.Name)
                        , "In the UniqueKeys dictionary of an ITable, every "
                        + "dictionary key must be the name of the IUniqueKey to which the "
                        + "dictionary key gives access."
                        );
                    Debug.Assert(pair.Value.PassesSelfAssertion());
                }
                Debug.Assert(
                    ForeignKeys != null
                    , "The ForeignKeys property of an ITable can be empty, but cannot be null."
                    );
                if (ForeignKeys.Count > 0)
                {
                    int cascaderCount = 0;
                    foreach (KeyValuePair<string, IForeignKey> pair in ForeignKeys)
                    {
                        Debug.Assert(
                            pair.Value != null
                            , "An ITable cannot contain, in its ForeignKeys dictionary, "
                            + "a pointer to a null IForeignKey."
                            );
                        Debug.Assert(
                            pair.Key.Equals(pair.Value.Name)
                            , "In the ForeignKeys dictionary of an ITable, every "
                            + "dictionary key must be the name of the IForeignKey to which the "
                            + "dictionary key gives access."
                            );
                        Debug.Assert(pair.Value.PassesSelfAssertion());
                        if (pair.Value.CascadeOnDelete || pair.Value.CascadeOnUpdate)
                            cascaderCount++;
                    }
                    if (cascaderCount > 0)
                        Debug.Assert(
                            CascadingForeignKeyRegistry != null
                            , "If there is at least one cascading foreign key, "
                            + "then the CascadingForeignKeyRegistry must not be null."
                            );
                }
                if (CascadingForeignKeyRegistry != null)
                    Debug.Assert(CascadingForeignKeyRegistry.PassesSelfAssertion());
                Debug.Assert(
                    Triggers != null
                    , "The Triggers property of an IVable can be empty, but cannot be null."
                    );
                foreach (KeyValuePair<string, ITableTrigger> pair in Triggers)
                {
                    Debug.Assert(
                        pair.Value != null
                        , "An IVable cannot contain, in its Triggers dictionary, "
                        + "a pointer to a null ITableTrigger."
                        );
                    Debug.Assert(
                        pair.Key.Equals(pair.Value.Name)
                        , "In the Triggers dictionary of an IVable, every "
                        + "dictionary key must be the name of the ITableTrigger to which the "
                        + "dictionary key gives access."
                        );
                    Debug.Assert(pair.Value.PassesSelfAssertion());
                }

                Debug.Assert(
                    CheckConstraints != null
                    , "The CheckConstraints property of an IVable can be empty, but cannot be null."
                    );
                foreach (KeyValuePair<string, ICheckConstraint> pair in CheckConstraints)
                {
                    Debug.Assert(
                        pair.Value != null
                        , "An IVable cannot contain, in its CheckConstraints dictionary, "
                        + "a pointer to a null ICheckConstraint."
                        );
                    Debug.Assert(
                        pair.Key.Equals(pair.Value.Name)
                        , "In the CheckConstraints dictionary of an IVable, every "
                        + "dictionary key must be the name of the ICheckConstraint to which the "
                        + "dictionary key gives access."
                        );
                    Debug.Assert(pair.Value.PassesSelfAssertion());
                }

                Debug.Assert(
                    DefaultConstraints != null
                    , "The DefaultConstraints property of an IVable can be empty, but cannot be null."
                    );
                foreach (KeyValuePair<string, IDefaultConstraint> pair in DefaultConstraints)
                {
                    Debug.Assert(
                        pair.Value != null
                        , "An IVable cannot contain, in its DefaultConstraints dictionary, "
                        + "a pointer to a null IDefaultConstraint."
                        );
                    Debug.Assert(
                        pair.Key.Equals(pair.Value.Name)
                        , "In the DefaultConstraints dictionary of an IVable, every "
                        + "dictionary key must be the name of the IDefaultConstraint to which the "
                        + "dictionary key gives access."
                        );
                    Debug.Assert(pair.Value.PassesSelfAssertion());
                }
                if (AuditTable != null) Debug.Assert(AuditTable.PassesSelfAssertion());
                if (PublishedChangesTable != null)
                {
                    Debug.Assert(PublishedChangesTable.PassesSelfAssertion());
                    Debug.Assert(
                        ChangeSubscriberRegistry != null
                        , "If a table is publishing its changes, it has to have "
                        + "access to a ChangeSubscriberRegistry, i.e., that "
                        + "property cannot be null."
                        );
                }
                if (ChangeSubscriberRegistry != null)
                    Debug.Assert(ChangeSubscriberRegistry.PassesSelfAssertion());
                if (ParentTable != null)
                {
                    Debug.Assert(ParentTable.PassesSelfAssertion());
                    Debug.Assert(
                        ParentTable.HasPrimaryKey
                        , "A table cannot have a parent table unless the "
                        + "parent table has a primary key."
                        );
                    Debug.Assert(
                        ParentTable.PrimaryKey.ConstrainedColumn != null
                        , "A table cannot have a parent table unless the "
                        + "parent table has a single-column primary key."
                        );
                    Debug.Assert(
                        ParentTable.PrimaryKey.ConstrainedColumn.DataType == "int"
                        , "A table cannot have a parent table unless the "
                        + "parent table has a single-column primary key "
                        + "constraining a column of type int."
                        );
                    Debug.Assert(
                        ParentInsertColumnListExpressions != null
                        , "The ParentInsertColumnListExpressions property of an ITable "
                        + "can be an empty dictionary, but cannot be null."
                        );
                    //Debug.Assert(
                    //    ParentPassThrough != null
                    //    , "A table cannot have a parent table without also specifying "
                    //    + "parent pass-through information."
                    //    );
                    Debug.Assert(
                        HasPrimaryKey
                        , "A table cannot have a parent table without also "
                        + "having a primary key."
                        );
                    Debug.Assert(
                        PrimaryKey.IsNullable
                        , "A table cannot have a parent table without also "
                        + "having a nullable primary key."
                        );
                    Debug.Assert(
                        PrimaryKey.ConstrainedColumn != null
                        , "A table cannot have a parent table without also "
                        + "having a single-column nullable primary key."
                        );
                    Debug.Assert(
                        PrimaryKey.ConstrainedColumn.DataType == "int"
                        , "A table cannot have a parent table without also "
                        + "having a single-column nullable primary key "
                        + "constraining a column of datatype int."
                        );
                    Debug.Assert(
                        PrimaryKey.ConstrainedColumn.ForeignKey != null
                        , "A table cannot have a parent table without also "
                        + "having a single-column nullable primary key "
                        + "constraining a column that is also constrained "
                        + "by a foreign key."
                        );
                    Debug.Assert(
                        ParentTable.Equals(
                            PrimaryKey.ConstrainedColumn.ForeignKey.ReferencedTable
                            )
                        , "A table cannot have a parent table without also "
                        + "having a single-column nullable primary key "
                        + "constraining a column that is also constrained "
                        + "by a foreign key referencing the parent table."
                        );
                    Debug.Assert(
                        ParentTable.PrimaryKey.ConstrainedColumn.Name.Equals(
                            PrimaryKey.ConstrainedColumn.ForeignKey.ReferenceColumnNames[0]
                            )
                        , "A table cannot have a parent table without also "
                        + "having a single-column nullable primary key "
                        + "constraining a column that is also constrained "
                        + "by a foreign key referencing the parent table's "
                        + "single primary key column."
                        );
                }
                else
                {
                    Debug.Assert(
                        ParentInsertColumnListExpressions.Count == 0
                        , "A table cannot have parent insert column list expressions "
                        + "if it does not have a parent table."
                        );
                    //Debug.Assert(
                    //    ParentPassThrough == null
                    //    , "A table cannot have a set of parent pass-through information "
                    //    + "if it does not have a parent table."
                    //    );
                }
                if (LeavesTable != null)
                {
                    Debug.Assert(LeavesTable.PassesSelfAssertion());
                    Debug.Assert(
                        HasPrimaryKey
                        , "A table cannot have potential children unless it "
                        + "has a primary key."
                        );
                    Debug.Assert(
                        PrimaryKey.ConstrainedColumn != null
                        , "A table cannot have potential children unless it "
                        + "has a single-column primary key."
                        );
                    Debug.Assert(
                        PrimaryKey.ConstrainedColumn.DataType == "int"
                        , "A table cannot have potential children unless it "
                        + "has a single-column primary key "
                        + "constraining a column of type int."
                        );
                    Debug.Assert(
                        HomeVableNameColumn != null
                        , "A table cannot have potential children unless it "
                        + "has a home vable name column."
                        );
                    Debug.Assert(
                        HomeVableNameColumn.DataType.Equals(SqlTranslator.SysnameTypeSql)
                        , "A table cannot have potential children unless it "
                        + "has a home vable name column whose type is "
                        + "the database's standard type for system object names."
                        );
                }
                else
                {
                    Debug.Assert(
                        ChildVablesTable == null
                        , "A table that cannot have children cannot have a child vables table."
                        );
                }
                if (ChildVablesTable != null) Debug.Assert(ChildVablesTable.PassesSelfAssertion());
                Debug.Assert(
                    Columns.Where(p => typeof(IInputterColumn).IsAssignableFrom(p.Value.Item2.GetType())).Count() < 2
                    , "A table cannot have more than one IInputterColumn."
                    );
                if (InputterColumn == null)
                    Debug.Assert(
                        ValidInputtersTable == null
                        , "A table cannot have valid inputters table unless it "
                        + "also has an inputter column."
                        );
                if (InputterColumn != null)
                    Debug.Assert(
                        ValidInputtersTable != null
                        , "A table cannot have an inputter column unless it "
                        + "also has a valid inputters table."
                        );
            }
            return true;
        }

        public override bool MembersMatch(object obj)
        {
            if (!base.MembersMatch(obj)) return false;
            if (!typeof(ITable).IsAssignableFrom(obj.GetType())) return false;
            ITable other = (ITable)obj;
            if (
                !EqualityUtilities.EqualOrBothNull(this.UniqueKeys, other.UniqueKeys)
                || !EqualityUtilities.EqualOrBothNull(this.ForeignKeys, other.ForeignKeys)
                || !EqualityUtilities.EqualOrBothNull(this.CascadingForeignKeyRegistry, other.CascadingForeignKeyRegistry)
                || !EqualityUtilities.EqualOrBothNull(this.Triggers, other.Triggers)
                || !EqualityUtilities.EqualOrBothNull(this.CheckConstraints, other.CheckConstraints)
                || !EqualityUtilities.EqualOrBothNull(this.DefaultConstraints, other.DefaultConstraints)
                || !EqualityUtilities.EqualOrBothNull(this.AuditTable, other.AuditTable)
                || !EqualityUtilities.EqualOrBothNull(this.PublishedChangesTable, other.PublishedChangesTable)
                || !EqualityUtilities.EqualOrBothNull(this.ChangeSubscriberRegistry, other.ChangeSubscriberRegistry)
                || !EqualityUtilities.EqualOrBothNull(this.ParentTable, other.ParentTable)
                || !EqualityUtilities.EqualOrBothNull(this.ParentInsertColumnListExpressions, other.ParentInsertColumnListExpressions)
                // || !EqualityUtilities.EqualOrBothNull(this.ParentPassThrough, other.ParentPassThrough)
                || !EqualityUtilities.EqualOrBothNull(this.LeavesTable, other.LeavesTable)
                || !EqualityUtilities.EqualOrBothNull(this.ChildVablesTable, other.ChildVablesTable)
                || !EqualityUtilities.EqualOrBothNull(this.ValidInputtersTable, other.ValidInputtersTable)
                ) return false;
            return true;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new TableMutable(
                Name
                , GetSqlTranslatorSafely()
                , GetSchemaSafely()
                , GetObjectTypeSafely()
                , GetOwnerSafely()
                , AllowsUpdates
                , AllowsDeletes
                , GetTableColumnsSafely()
                , GetIndexesSafely()
                , HasIdentityColumn
                , GetIdentityColumnSafely()
                , GetPrimaryKeySafely()
                , GetUniqueKeysSafely()
                , GetForeignKeysSafely()
                , GetCascadingForeignKeyRegistrySafely()
                , GetTriggersSafely()
                , GetCheckConstraintsSafely()
                , GetDefaultConstraintsSafely()
                , IsAudited
                , GetAuditTableSafely()
                , PublishesChanges
                , GetPublishedChangesTableSafely()
                , GetChangeSubscriberRegistrySafely()
                , GetParentTableSafely()
                , GetParentInsertColumnListExpressionsSafely()
                // , GetParentPassThroughSafely()
                , CanHaveChildren
                , GetLeavesTableSafely()
                , MaintainChildVablesTable
                , GetChildVablesTableSafely()
                , HasIDColumn
                , GetIDColumnSafely()
                , HasInputterColumn
                , GetInputterColumnSafely()
                , GetValidInputtersTableSafely()
                , HasLastUpdateDtmColumn
                , HasLastUpdaterColumn
                , NameWhenPrefixed
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , schema: null
                , objectType: null
                , owner: null
                , allowsUpdates: true
                , allowsDeletes: true
                , columns: null
                , indexes: null
                , hasIdentityColumn: true
                , identityColumn: null
                , primaryKey: null
                , uniqueKeys: null
                , foreignKeys: null
                , cascadingForeignKeyRegistry: null
                , triggers: null
                , checkConstraints: null
                , defaultConstraints: null
                , isAudited: false
                , auditTable: null
                , publishesChanges: false
                , publishedChangesTable: null
                , changeSubscriberRegistry: null
                , parentTable: null
                , parentInsertColumnListExpressions: null
                // , parentPassThrough: null
                , canHaveChildren: false
                , leavesTable: null
                , maintainChildVablesTable: false
                , childVablesTable: null
                , hasIDColumn: false
                , idColumn: null
                , hasInputterColumn: false
                , inputterColumn: null
                , validInputtersTable: null
                , hasLastUpdateDtmColumn: false
                , hasLastUpdaterColumn: false
                , nameWhenPrefixed: null
                , id: default(int?)
                );
        }

        protected virtual void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator
            , ISchema schema
            , IObjectType objectType
            , IDataProcObject owner
            , bool allowsUpdates
            , bool allowsDeletes
            , Dictionary<string, Tuple<int, ITableColumn>> columns
            , Dictionary<string, IIndex> indexes
            , bool hasIdentityColumn
            , IIdentityColumn identityColumn
            , IPrimaryKey primaryKey
            , Dictionary<string, IUniqueKey> uniqueKeys
            , Dictionary<string, IForeignKey> foreignKeys
            , ICascadingForeignKeyRegistry cascadingForeignKeyRegistry
            , Dictionary<string, ITableTrigger> triggers
            , Dictionary<string, ICheckConstraint> checkConstraints
            , Dictionary<string, IDefaultConstraint> defaultConstraints
            , bool isAudited
            , IAuditTable auditTable
            , bool publishesChanges
            , IChangeTrackingTable publishedChangesTable
            , IChangeSubscriberRegistry changeSubscriberRegistry
            , ITable parentTable
            , Dictionary<string, string> parentInsertColumnListExpressions
            // , IParentPassThroughInfo parentPassThrough
            , bool canHaveChildren
            , ILeavesTable leavesTable
            , bool maintainChildVablesTable
            , IChildVablesTable childVablesTable
            , bool hasIDColumn
            , ITableIDColumn idColumn
            , bool hasInputterColumn
            , IInputterColumn inputterColumn
            , IValidInputtersTable validInputtersTable
            , bool hasLastUpdateDtmColumn
            , bool hasLastUpdaterColumn
            , string nameWhenPrefixed
            , int? id
            )
        {
            base.ConstructAfterConstruction(
                name
                , sqlTranslator
                , schema
                , objectType == null ? ObjectTypes.Table() : objectType
                , ObjectTypes.Table()
                , owner
                , nameWhenPrefixed
                , id
                );
            ConstructNewMembers(
                columns
                , indexes
                , hasIdentityColumn
                , identityColumn
                , primaryKey
                , uniqueKeys
                , foreignKeys
                , cascadingForeignKeyRegistry
                , triggers
                , checkConstraints
                , defaultConstraints
                , isAudited
                , auditTable
                , publishesChanges
                , publishedChangesTable
                , changeSubscriberRegistry
                , parentTable
                , parentInsertColumnListExpressions
                // , parentPassThrough
                , canHaveChildren
                , leavesTable
                , maintainChildVablesTable
                , childVablesTable
                , hasIDColumn
                , idColumn
                , hasInputterColumn
                , inputterColumn
                , validInputtersTable
                , hasLastUpdateDtmColumn
                , hasLastUpdaterColumn
                , false
                );
        }

        private void ConstructNewMembers(
            Dictionary<string, Tuple<int, ITableColumn>> columns
            , Dictionary<string, IIndex> indexes
            , bool hasIdentityColumn
            , IIdentityColumn identityColumn
            , IPrimaryKey primaryKey
            , Dictionary<string, IUniqueKey> uniqueKeys
            , Dictionary<string, IForeignKey> foreignKeys
            , ICascadingForeignKeyRegistry cascadingForeignKeyRegistry
            , Dictionary<string, ITableTrigger> triggers
            , Dictionary<string, ICheckConstraint> checkConstraints
            , Dictionary<string, IDefaultConstraint> defaultConstraints
            , bool isAudited
            , IAuditTable auditTable
            , bool publishesChanges
            , IChangeTrackingTable publishedChangesTable
            , IChangeSubscriberRegistry changeSubscriberRegistry
            , ITable parentTable
            , Dictionary<string, string> parentInsertColumnListExpressions
            // , IParentPassThroughInfo parentPassThrough
            , bool canHaveChildren
            , ILeavesTable leavesTable
            , bool maintainChildVablesTable
            , IChildVablesTable childVablesTable
            , bool hasIDColumn
            , ITableIDColumn idColumn
            , bool hasInputterColumn
            , IInputterColumn inputterColumn
            , IValidInputtersTable validInputtersTable
            , bool hasLastUpdateDtmColumn
            , bool hasLastUpdaterColumn
            , bool forceUpdates
            )
        {
            if (SqlTranslator == null)
                throw new ConstructionRequiresSqlTranslatorException(
                    this
                    , typeof(TableMutable).FullName
                    );
            // If something is no longer at the default, don't change it
            //   unless the forceUpdates flag is switched on
            if (forceUpdates || Columns == null)
            {
                foreach (
                    KeyValuePair<string, Tuple<int, ITableColumn>> pair
                    in columns.OrderBy(p => p.Value.Item1)
                    )
                    AddOrOverwriteColumn(
                        pair.Value.Item2
                        , retrieveColumnIDFromDatabase: false
                        , assert: false
                        );
            }
            if (forceUpdates || PrimaryKey == null) SetPrimaryKey(primaryKey, false, false);
            if (forceUpdates || UniqueKeys == null) SetUniqueKeys(uniqueKeys);
            if (forceUpdates || ForeignKeys == null) SetForeignKeys(foreignKeys);
            if (forceUpdates || CascadingForeignKeyRegistry == null) SetCascadingForeignKeyRegistry(cascadingForeignKeyRegistry);
            if (forceUpdates || Triggers == null) SetTriggers(triggers, false, false);
            if (forceUpdates || CheckConstraints == null) SetCheckConstraints(checkConstraints, false, false);
            if (forceUpdates || DefaultConstraints == null) SetDefaultConstraints(defaultConstraints, false, false);
            if (forceUpdates || AuditTable == null) SetIsAudited(isAudited, auditTable, false, false);
            if (forceUpdates || PublishedChangesTableName == null)
                SetPublishesChanges(
                    publishesChanges
                    , publishedChangesTable
                    , changeSubscriberRegistry
                    , false
                    , false
                    );
            if (forceUpdates || ParentTable == null) SetParentTable(parentTable, parentInsertColumnListExpressions, false, false);
            // if (forceUpdates || ParentPassThrough == null) SetParentPassThrough(parentPassThrough, false);
            if (forceUpdates || LeavesTable == null) SetCanHaveChildren(canHaveChildren, leavesTable, false, false);
            if (forceUpdates || ChildVablesTable == null) SetMaintainChildVablesTable(maintainChildVablesTable, childVablesTable, false, false);
            if (forceUpdates || ValidInputtersTable == null)
                SetHasInputterColumn(
                    hasInputterColumn
                    , inputterColumn
                    , validInputtersTable
                    , false
                    , false
                    );
            columns = TableColumns;
            if (
                SqlTranslator.RequiresIdentityColumn
                && columns.Values.Where(t => t.Item2.IsIdentity).Count() == 0
                )
            {
                string identityColumnName = "rowid";
                int tagNum = 0;
                while (columns.Values.Where(t => t.Item2.Name.Equals(identityColumnName)).Count() > 0)
                    identityColumnName = "rowid_" + tagNum++;
                AddOrOverwriteColumn(
                    new TableColumnMutable(
                            identityColumnName
                            , "int"
                            , SqlTranslator
                            , this
                            , isNullable: true
                            , isIdentity: true
                            , isImmutable: true
                            , uniqueKey: new SingleColumnUniqueKeyMutable(
                                name: null
                                , sqlTranslator: SqlTranslator
                                , table: this
                                , objectType: ObjectTypes.UniqueKey()
                                , physicalObjectType: ObjectTypes.UniqueKey()
                                , constrainedColumn: null
                                , columnNames: new List<string> { "rowid" }
                                , retrieveIDsFromDatabase: false
                                , assert: false
                                )
                            , constraintTag: "Rowid"
                            , retrieveIDsFromDatabase: false
                            , assert: false
                            )
                    , 0
                    , retrieveColumnIDFromDatabase: false
                    , assert: false
                    );
            }
        }

        /***************************
         * DatabaseObjectMutable method overrides
        ***************************/

        public override IDatabaseObject GetCopyForOwner(
            IDataProcObject owner
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            RejectIfInvalidOwnerType(owner);
            return new TableMutable(
                Name
                , SqlTranslator
                , Schema
                , ObjectType
                , owner
                , AllowsUpdates
                , AllowsDeletes
                , TableColumns
                , Indexes
                , HasIdentityColumn
                , IdentityColumn
                , PrimaryKey
                , UniqueKeys
                , ForeignKeys
                , CascadingForeignKeyRegistry
                , Triggers
                , CheckConstraints
                , DefaultConstraints
                , IsAudited
                , AuditTable
                , PublishesChanges
                , PublishedChangesTable
                , ChangeSubscriberRegistry
                , ParentTable
                , ParentInsertColumnListExpressions
                // , ParentPassThrough
                , CanHaveChildren
                , LeavesTable
                , MaintainChildVablesTable
                , ChildVablesTable
                , HasIDColumn
                , IDColumn
                , HasInputterColumn
                , InputterColumn
                , ValidInputtersTable
                , HasLastUpdateDtmColumn
                , HasLastUpdaterColumn
                , NameWhenPrefixed
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        public override void RetrieveIDsFromDatabase(bool assert = true)
        {
            if (Database != null)
                if (Database.IsConnected)
                {
                    foreach (KeyValuePair<string, IUniqueKey> pair in UniqueKeys)
                    {
                        if (typeof(IUniqueKeyMutable).IsAssignableFrom(pair.Value.GetType()))
                            ((IUniqueKeyMutable)pair.Value).RetrieveIDsFromDatabase(false);
                        else
                            AddOrOverwriteUniqueKey(
                                (IUniqueKey)pair.Value.GetReferenceForParentObject(this, true, false)
                                );
                    }
                    foreach (KeyValuePair<string, IForeignKey> pair in ForeignKeys)
                    {
                        if (typeof(IForeignKeyMutable).IsAssignableFrom(pair.Value.GetType()))
                            ((IForeignKeyMutable)pair.Value).RetrieveIDsFromDatabase(false);
                        else
                            AddOrOverwriteForeignKey(
                                (IForeignKey)pair.Value.GetReferenceForParentObject(this, true, false)
                                );
                    }
                    foreach (KeyValuePair<string, ITableTrigger> pair in Triggers)
                    {
                        if (typeof(ITableTriggerMutable).IsAssignableFrom(pair.Value.GetType()))
                            ((ITableTriggerMutable)pair.Value).RetrieveIDsFromDatabase(false);
                        else
                            AddOrOverwriteTrigger(
                                (ITableTrigger)pair.Value.GetReferenceForParentObject(this, true, false)
                                );
                    }
                    foreach (KeyValuePair<string, ICheckConstraint> pair in CheckConstraints)
                    {
                        if (typeof(ICheckConstraintMutable).IsAssignableFrom(pair.Value.GetType()))
                            ((ICheckConstraintMutable)pair.Value).RetrieveIDsFromDatabase(false);
                        else
                            AddOrOverwriteCheckConstraint(
                                    (ICheckConstraint)pair.Value.GetReferenceForParentObject(this, true, false)
                                    );
                    }
                    foreach (KeyValuePair<string, IDefaultConstraint> pair in DefaultConstraints)
                    {
                        if (typeof(IDefaultConstraintMutable).IsAssignableFrom(pair.Value.GetType()))
                            ((IDefaultConstraintMutable)pair.Value).RetrieveIDsFromDatabase(false);
                        else
                            AddOrOverwriteDefaultConstraint(
                                    (IDefaultConstraint)pair.Value.GetReferenceForParentObject(this, true, false)
                                    );
                    }
                    IAuditTable auditTable = AuditTable;
                    if (auditTable != null)
                    {
                        if (typeof(IAuditTableMutable).IsAssignableFrom(auditTable.GetType()))
                            ((IAuditTableMutable)auditTable).RetrieveIDsFromDatabase(false);
                        else
                            SetIsAudited(
                                true
                                , auditTable
                                , true
                                , false
                                );
                    }
                    IChangeTrackingTable publishedChangesTable = PublishedChangesTable;
                    if (publishedChangesTable != null)
                    {
                        if (typeof(IChangeTrackingTableMutable).IsAssignableFrom(publishedChangesTable.GetType()))
                            ((IChangeTrackingTableMutable)publishedChangesTable).RetrieveIDsFromDatabase(false);
                        else
                            SetPublishesChanges(
                                true
                                , publishedChangesTable
                                , ChangeSubscriberRegistry
                                , true
                                , false
                                );
                    }
                    ILeavesTable leavesTable = LeavesTable;
                    if (leavesTable != null)
                    {
                        if (typeof(ILeavesTableMutable).IsAssignableFrom(leavesTable.GetType()))
                            ((ILeavesTableMutable)leavesTable).RetrieveIDsFromDatabase(false);
                        else
                            SetCanHaveChildren(
                                true
                                , leavesTable
                                , true
                                , false
                                );
                    }
                    IChildVablesTable childVablesTable = ChildVablesTable;
                    if (childVablesTable != null)
                    {
                        if (typeof(IChildVablesTableMutable).IsAssignableFrom(childVablesTable.GetType()))
                            ((IChildVablesTableMutable)childVablesTable).RetrieveIDsFromDatabase(false);
                        else
                            SetMaintainChildVablesTable(
                                true
                                , childVablesTable
                                , true
                                , false
                                );
                    }
                    IValidInputtersTable validInputtersTable = ValidInputtersTable;
                    if (validInputtersTable != null)
                    {
                        if (typeof(IValidInputtersTableMutable).IsAssignableFrom(validInputtersTable.GetType()))
                            ((IValidInputtersTableMutable)validInputtersTable).RetrieveIDsFromDatabase(false);
                        else
                            SetHasInputterColumn(
                                true
                                , InputterColumn
                                , validInputtersTable
                                , true
                                , false
                                );
                    }
                }
            base.RetrieveIDsFromDatabase(assert);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        /***************************
         * ObjectInSchemaMutable method overrides
        ***************************/

        public override void PropagateSelfAsParentObjectToAllChildren()
        {
            base.PropagateSelfAsParentObjectToAllChildren();
            foreach (string key in _uniqueKeys.Keys)
                _uniqueKeys[key]
                    = (IUniqueKey)
                    _uniqueKeys[key].GetReferenceForParentObject(this, false, false);
            foreach (string key in _foreignKeys.Keys)
                _foreignKeys[key]
                    = (IForeignKey)
                    _foreignKeys[key].GetReferenceForParentObject(this, false, false);
            foreach (string key in _triggers.Keys)
                _triggers[key]
                    = (ITableTrigger)
                    _triggers[key].GetReferenceForParentObject(this, false, false);
            foreach (string key in _checkConstraints.Keys)
                _checkConstraints[key]
                    = (ICheckConstraint)
                    _checkConstraints[key].GetReferenceForParentObject(this, false, false);
            foreach (string key in _defaultConstraints.Keys)
                _defaultConstraints[key]
                    = (IDefaultConstraint)
                    _defaultConstraints[key].GetReferenceForParentObject(this, false, false);
            if (_auditTable != null)
                _auditTable
                    = (IAuditTable)
                    _auditTable.GetReferenceForParentObject(this, false, false);
            if (_publishedChangesTable != null)
                _publishedChangesTable
                    = (IChangeTrackingTable)
                    _publishedChangesTable.GetReferenceForParentObject(this, false, false);
            if (_leavesTable != null)
                _leavesTable
                    = (ILeavesTable)
                    _leavesTable.GetReferenceForParentObject(this, false, false);
            if (_childVablesTable != null)
                _childVablesTable
                    = (IChildVablesTable)
                    _childVablesTable.GetReferenceForParentObject(this, false, false);
            if (_validInputtersTable != null)
                _validInputtersTable
                    = (IValidInputtersTable)
                    _validInputtersTable.GetReferenceForParentObject(this, false, false);
        }

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            RejectIfInvalidParentObjectType(parentObject);
            return new TableMutable(
                Name
                , SqlTranslator
                , (ISchema)parentObject
                , ObjectType
                , Owner
                , AllowsUpdates
                , AllowsDeletes
                , TableColumns
                , Indexes
                , HasIdentityColumn
                , IdentityColumn
                , PrimaryKey
                , UniqueKeys
                , ForeignKeys
                , CascadingForeignKeyRegistry
                , Triggers
                , CheckConstraints
                , DefaultConstraints
                , IsAudited
                , AuditTable
                , PublishesChanges
                , PublishedChangesTable
                , ChangeSubscriberRegistry
                , ParentTable
                , ParentInsertColumnListExpressions
                // , ParentPassThrough
                , CanHaveChildren
                , LeavesTable
                , MaintainChildVablesTable
                , ChildVablesTable
                , HasIDColumn
                , IDColumn
                , HasInputterColumn
                , InputterColumn
                , ValidInputtersTable
                , HasLastUpdateDtmColumn
                , HasLastUpdaterColumn
                , NameWhenPrefixed
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        /***************************
         * ITable properties and methods
        ***************************/

        public Dictionary<string, Tuple<int, ITableColumn>> TableColumns
        {
            get
            {
                Dictionary<string, Tuple<int, ITableColumn>> retval
                    = new Dictionary<string, Tuple<int, ITableColumn>>();
                foreach (
                    KeyValuePair<string, Tuple<int, IColumn>> p
                    in Columns.OrderBy(p => p.Value.Item1)
                    )
                    retval[p.Key]
                        = new Tuple<int, ITableColumn>(
                            p.Value.Item1
                            , (ITableColumn)p.Value.Item2
                            );
                return retval;
            }
        }

        public bool HasIdentityColumn { get { return IdentityColumn != null; } }

        public IIdentityColumn IdentityColumn
        {
            get
            {
                IIdentityColumn retval = null;
                foreach (
                    Tuple<int, ITableColumn> t
                    in TableColumns.Values.Where(t => t.Item2.IsIdentity).Take(1)
                    )
                    retval = (IIdentityColumn)t.Item2;
                return retval;
            }
        }

        public string IdentityColumnName
        {
            get
            {
                IIdentityColumn identityColumn = IdentityColumn;
                if (identityColumn == null) return null;
                return identityColumn.Name;
            }
        }

        public bool HasPrimaryKey
        {
            get
            {
                if (_uniqueKeys.Count == 0) return false;
                else
                    return _uniqueKeys.Values.Where(k => k.IsPrimaryKey).Take(1).Count() > 0;
            }
        }

        public IPrimaryKey PrimaryKey
        {
            get
            {
                IPrimaryKey retval = null;
                foreach (IUniqueKey pk in _uniqueKeys.Values.Where(k => k.IsPrimaryKey).Take(1))
                    retval = (IPrimaryKey)pk;
                return retval;
            }
            set { SetPrimaryKey(value); }
        }

        public Dictionary<string, IUniqueKey> UniqueKeys
        {
            get { return _uniqueKeys; }
            set { SetUniqueKeys(value); }
        }

        public bool HasNonPrimaryUniqueKeys
        {
            get
            {
                if (_uniqueKeys.Count == 0) return false;
                else
                    return _uniqueKeys.Values.Where(k => !k.IsPrimaryKey).Take(1).Count() > 0;
            }
        }
        public Dictionary<string, IUniqueKey> NonPrimaryUniqueKeys
        {
            get
            {
                Dictionary<string, IUniqueKey> retval = new Dictionary<string, IUniqueKey>();
                foreach (
                    KeyValuePair<string, IUniqueKey> pair
                    in _uniqueKeys.Where(p => !p.Value.IsPrimaryKey)
                    )
                    retval[pair.Key] = pair.Value;
                return retval;
            }
        }

        public Dictionary<string, IForeignKey> ForeignKeys
        {
            get { return _foreignKeys; }
            set { SetForeignKeys(value); }
        }

        public ICascadingForeignKeyRegistry CascadingForeignKeyRegistry
        {
            get { return _cascadingForeignKeyRegistry; }
        }

        public List<ICascadingForeignKey> CascadingOnDeleteForeignKeys
        {
            get { return GetCascadingOnDeleteForeignKeysSafely(); }
        }

        public List<ICascadingForeignKey> CascadingOnUpdateForeignKeys
        {
            get { return GetCascadingOnUpdateForeignKeysSafely(); }
        }
        public Dictionary<string, ITableTrigger> Triggers
        {
            get { return _triggers; }
            set { SetTriggers(value); }
        }
        public Dictionary<string, ICheckConstraint> CheckConstraints
        {
            get { return _checkConstraints; }
            set { SetCheckConstraints(value); }
        }
        public Dictionary<string, IDefaultConstraint> DefaultConstraints
        {
            get { return _defaultConstraints; }
            set { SetDefaultConstraints(value); }
        }
        public bool IsAudited
        {
            get { return (_auditTable != null); }
            set { SetIsAudited(value, auditTable: null); }
        }
        public string AuditTableName { get { return UtilitiesForINamed.NameOf(_auditTable); } }
        public IAuditTable AuditTable { get { return _auditTable; } }

        public Dictionary<string, Tuple<int, ITableColumn>> InsertableColumns
        {
            get
            {
                Dictionary<string, Tuple<int, ITableColumn>> retval
                    = new Dictionary<string, Tuple<int, ITableColumn>>();
                foreach (KeyValuePair<string, Tuple<int, ITableColumn>> pair in TableColumns.Where(
                        p => p.Value.Item2.IsInsertable
                        ).OrderBy(p => p.Value.Item2.ID)
                    )
                    retval[pair.Key]
                        = new Tuple<int, ITableColumn>(
                            pair.Value.Item1
                            , pair.Value.Item2
                            );
                return retval;
            }
        }

        public Dictionary<string, Tuple<int, ITableColumn>> UpdatableColumns
        {
            get
            {
                Dictionary<string, Tuple<int, ITableColumn>> retval
                    = new Dictionary<string, Tuple<int, ITableColumn>>();
                foreach (KeyValuePair<string, Tuple<int, ITableColumn>> pair in TableColumns.Where(
                        p => p.Value.Item2.IsInsertable && !p.Value.Item2.IsImmutable
                        ).OrderBy(p => p.Value.Item2.ID)
                    )
                    retval[pair.Key]
                        = new Tuple<int, ITableColumn>(
                            pair.Value.Item1
                            , pair.Value.Item2
                            );
                return retval;
            }
        }

        public bool PublishesChanges { get { return (_publishedChangesTable != null); } }
        public string PublishedChangesTableName { get { return _publishedChangesTable == null ? null : _publishedChangesTable.Name; } }
        public IChangeTrackingTable PublishedChangesTable { get { return _publishedChangesTable; } }
        public IChangeSubscriberRegistry ChangeSubscriberRegistry { get { return _changeSubscriberRegistry; } }
        public ITable ParentTable
        {
            get { return _parentTable; }
            set { SetParentTable(value); }
        }
        public Dictionary<string, string> ParentInsertColumnListExpressions
        {
            get { return _parentInsertColumnListExpressions; }
            set { SetParentInsertColumnListExpressions(value, true); }
        }
        //public IParentPassThroughInfo ParentPassThrough
        //{
        //    get { return _parentPassThrough; }
        //    set { SetParentPassThrough(value); }
        //}
        public bool CanHaveChildren { get { return (_leavesTable != null); } }
        public IHomeVableNameColumn HomeVableNameColumn
        {
            get
            {
                if (!CanHaveChildren) return null;
                foreach (
                    Tuple<int, IColumn> t
                    in Columns.Values
                    .Where(t => typeof(IHomeVableNameColumn).IsAssignableFrom(t.Item2.GetType()))
                    .Take(1)
                    )
                    return (IHomeVableNameColumn)t.Item2;
                return null;
            }
        }
        public string LeavesTableName { get { return _leavesTable == null ? null : _leavesTable.Name; } }
        public ILeavesTable LeavesTable { get { return _leavesTable; } }
        public bool MaintainChildVablesTable { get { return (_childVablesTable != null); } }
        public string ChildVablesTableName { get { return _childVablesTable == null ? null : _childVablesTable.Name; } }
        public IChildVablesTable ChildVablesTable { get { return _childVablesTable; } }

        public bool HasIDColumn { get { return InputterColumn != null; } }

        public ITableIDColumn IDColumn
        {
            get
            {
                ITableIDColumn retval = null;
                foreach (
                    Tuple<int, IColumn> t
                    in Columns.Values.Where(
                        t => typeof(ITableIDColumn).IsAssignableFrom(t.Item2.GetType())
                        ).Take(1)
                    )
                    retval = (ITableIDColumn)t.Item2;
                return retval;
            }
        }

        public string IDColumnName
        {
            get
            {
                IIDColumn column = IDColumn;
                return column == null ? null : column.Name;
            }
        }

        public string TempInsertedIDBaseColumnName
        {
            get
            {
                IIDColumn column = IDColumn;
                return
                    column == null
                    || typeof(IIDColumnWithSequence)
                    .IsAssignableFrom(column.GetType()) ? null
                    : ((IIDColumnWithSequence)column).TempInsertedIDBaseColumnName;
            }
        }

        public bool HasInputterColumn { get { return InputterColumn != null; } }

        public IInputterColumn InputterColumn
        {
            get
            {
                KeyValuePair<string, Tuple<int, IColumn>> pair
                    = Columns.Where(p => typeof(IInputterColumn).IsAssignableFrom(p.Value.Item2.GetType())).FirstOrDefault();
                if (pair.Equals(default(KeyValuePair<string, Tuple<int, ITableColumn>>))) return null;
                else return (IInputterColumn)pair.Value.Item2;
            }
        }

        public string InputterColumnName
        {
            get
            {
                IInputterColumn column = InputterColumn;
                return column == null ? null : column.Name;
            }
        }

        public IValidInputtersTable ValidInputtersTable { get { return _validInputtersTable; } }

        public string ValidInputtersTableName
        {
            get { return _validInputtersTable == null ? null : _validInputtersTable.Name; }
        }

        public bool HasLastUpdateDtmColumn { get { return LastUpdateDtmColumn != null; } }

        public ILastUpdateDtmColumn LastUpdateDtmColumn
        {
            get
            {
                KeyValuePair<string, Tuple<int, IColumn>> pair
                    = Columns.Where(p => typeof(ILastUpdateDtmColumn).IsAssignableFrom(p.Value.Item2.GetType())).FirstOrDefault();
                if (pair.Equals(default(KeyValuePair<string, Tuple<int, ITableColumn>>))) return null;
                else return (ILastUpdateDtmColumn)pair.Value.Item2;
            }
        }

        public string LastUpdateDtmColumnName
        {
            get
            {
                ILastUpdateDtmColumn column = LastUpdateDtmColumn;
                return column == null ? null : column.Name;
            }
        }

        public bool HasLastUpdaterColumn { get { return LastUpdaterColumn != null; } }

        public ILastUpdaterColumn LastUpdaterColumn
        {
            get
            {
                KeyValuePair<string, Tuple<int, IColumn>> pair
                    = Columns.Where(p => typeof(ILastUpdaterColumn).IsAssignableFrom(p.Value.Item2.GetType())).FirstOrDefault();
                if (pair.Equals(default(KeyValuePair<string, Tuple<int, ITableColumn>>))) return null;
                else return (ILastUpdaterColumn)pair.Value.Item2;
            }
        }

        public string LastUpdaterColumnName
        {
            get
            {
                ILastUpdaterColumn column = LastUpdaterColumn;
                return column == null ? null : column.Name;
            }
        }

        // public bool CheckConstraintsIU { get; set; } = false;

        public Dictionary<string, Tuple<int, ITableColumn>> GetTableColumnsSafely()
        {
            Dictionary<string, Tuple<int, ITableColumn>> retval
                = new Dictionary<string, Tuple<int, ITableColumn>>();
            foreach (
                KeyValuePair<string, Tuple<int, IColumn>> p
                in Columns.OrderBy(p => p.Value.Item1)
                )
                retval[p.Key]
                    = new Tuple<int, ITableColumn>(
                        p.Value.Item1
                        , (ITableColumn)p.Value.Item2.GetSafeReference()
                        );
            return retval;
        }

        public IIdentityColumn GetIdentityColumnSafely()
        {
            IIdentityColumn identityColumn = IdentityColumn;
            return
                identityColumn != null ? (IIdentityColumn)identityColumn.GetSafeReference()
                : null;
        }

        public IPrimaryKey GetPrimaryKeySafely()
        {
            IPrimaryKey primaryKey = PrimaryKey;
            if (primaryKey == null) return null;
            else return (IPrimaryKey)primaryKey.GetSafeReference();
        }

        public Dictionary<string, IUniqueKey> GetUniqueKeysSafely()
        {
            Dictionary<string, IUniqueKey> retval = new Dictionary<string, IUniqueKey>();
            foreach (KeyValuePair<string, IUniqueKey> pair in _uniqueKeys)
                retval[pair.Key] = (IUniqueKey)pair.Value.GetSafeReference();
            return retval;
        }
        public Dictionary<string, IUniqueKey> GetNonPrimaryUniqueKeysSafely()
        {
            Dictionary<string, IUniqueKey> retval = new Dictionary<string, IUniqueKey>();
            foreach (KeyValuePair<string, IUniqueKey> pair in NonPrimaryUniqueKeys)
                retval[pair.Key] = (IUniqueKey)pair.Value.GetSafeReference();
            return retval;
        }
        public Dictionary<string, IForeignKey> GetForeignKeysSafely()
        {
            Dictionary<string, IForeignKey> retval = new Dictionary<string, IForeignKey>();
            foreach (KeyValuePair<string, IForeignKey> pair in _foreignKeys)
                retval[pair.Key] = (IForeignKey)pair.Value.GetSafeReference();
            return retval;
        }
        public ICascadingForeignKeyRegistry GetCascadingForeignKeyRegistrySafely()
        {
            if (CascadingForeignKeyRegistry == null) return null;
            else return (ICascadingForeignKeyRegistry)CascadingForeignKeyRegistry.GetSafeReference();
        }
        private List<ICascadingForeignKey> GetCascadingForeignKeysSafely(char deleteUpdateFlag)
        {
            List<ICascadingForeignKey> retval = new List<ICascadingForeignKey>();
            if (Database != null)
            {
                List<string> sql = Database.SqlTranslator.VableForeignKeyDependentsSql(this);
                var rows = Database.SqlExecuteReader(sql);
                if (rows.Count > 0)
                {
                    bool include = false;
                    string currentKeyName = "";
                    ICascadingForeignKeyMutable cfk = null;
                    Dictionary<string, int> columnNames = Database.SqlTranslator.VableForeignKeyDependentsOutputStructure();
                    foreach (var row in rows)
                    {
                        string keyName = row[columnNames["foreign_key_name"]];
                        string dependentTableName = row[columnNames["dependent_name"]];
                        string dependentColumnName = row[columnNames["dependent_col_name"]];
                        string dependentTableSchemaName = row[columnNames["dependent_schema_name"]];
                        string referencedColumnName = row[columnNames["referenced_col_name"]];
                        ITable dependent = (ITable)CommUtils.GetTableByTableName(dependentTableName);
                        IForeignKey key;
                        if (keyName != currentKeyName)
                        {
                            if (cfk != null) retval.Add(cfk);
                            try { key = dependent.ForeignKeys.First(k => k.Value.Name == keyName).Value; }
                            catch
                            {
                                throw new DatabaseCodeMismatchException(
                                    "In database "
                                    + (Database.Name == null ? "" : "'" + Database.Name + "'")
                                    + ", table '" + dependentTableName + "' has a foreign key named '"
                                    + keyName + "', but no such foreign key is specified by that "
                                    + "table's C# model."
                                    );
                            }
                            include =
                                (deleteUpdateFlag.Equals('D') && key.CascadeOnDelete)
                                || (deleteUpdateFlag.Equals('U') && key.CascadeOnUpdate);
                            if (include)
                                cfk = new CascadingForeignKeyMutable(
                                    dependentTableName
                                    , dependentTableSchemaName
                                    , dependentColumnName
                                    , referencedColumnName
                                    );
                            else cfk = null;
                        }
                        else
                        {
                            if (include)
                                cfk.AddOrOverwriteKeyReferenceColumnMapping(dependentColumnName, referencedColumnName);
                        }
                    }
                    if (cfk != null) retval.Add(cfk);
                }
            }
            return retval;
        }
        public List<ICascadingForeignKey> GetCascadingOnDeleteForeignKeysSafely()
        {
            return GetCascadingForeignKeysSafely('D');
        }
        public List<ICascadingForeignKey> GetCascadingOnUpdateForeignKeysSafely()
        {
            return GetCascadingForeignKeysSafely('U');
        }
        public Dictionary<string, ITableTrigger> GetTriggersSafely()
        {
            Dictionary<string, ITableTrigger> retval = new Dictionary<string, ITableTrigger>();
            foreach (KeyValuePair<string, ITableTrigger> pair in _triggers)
                retval[pair.Key] = (ITableTrigger)pair.Value.GetSafeReference();
            return retval;
        }
        public Dictionary<string, ICheckConstraint> GetCheckConstraintsSafely()
        {
            Dictionary<string, ICheckConstraint> retval = new Dictionary<string, ICheckConstraint>();
            foreach (KeyValuePair<string, ICheckConstraint> pair in _checkConstraints)
                retval[pair.Key] = (ICheckConstraint)pair.Value.GetSafeReference();
            return retval;
        }
        public Dictionary<string, IDefaultConstraint> GetDefaultConstraintsSafely()
        {
            Dictionary<string, IDefaultConstraint> retval = new Dictionary<string, IDefaultConstraint>();
            foreach (KeyValuePair<string, IDefaultConstraint> pair in _defaultConstraints)
                retval[pair.Key] = (IDefaultConstraint)pair.Value.GetSafeReference();
            return retval;
        }
        public IAuditTable GetAuditTableSafely()
        {
            if (_auditTable == null) return null;
            else return (IAuditTable)_auditTable.GetSafeReference();
        }
        public Dictionary<string, Tuple<int, ITableColumn>> GetInsertableColumnsSafely()
        {
            Dictionary<string, Tuple<int, ITableColumn>> retval = new Dictionary<string, Tuple<int, ITableColumn>>();
            foreach (KeyValuePair<string, Tuple<int, ITableColumn>> pair in InsertableColumns)
                retval[pair.Key] = new Tuple<int, ITableColumn>(pair.Value.Item1, (ITableColumn)pair.Value.Item2.GetSafeReference());
            return retval;
        }
        public Dictionary<string, Tuple<int, ITableColumn>> GetUpdatableColumnsSafely()
        {
            Dictionary<string, Tuple<int, ITableColumn>> retval = new Dictionary<string, Tuple<int, ITableColumn>>();
            foreach (KeyValuePair<string, Tuple<int, ITableColumn>> pair in UpdatableColumns)
                retval[pair.Key] = new Tuple<int, ITableColumn>(pair.Value.Item1, (ITableColumn)pair.Value.Item2.GetSafeReference());
            return retval;
        }
        public IChangeTrackingTable GetPublishedChangesTableSafely()
        {
            if (_publishedChangesTable == null) return null;
            else return (IChangeTrackingTable)_publishedChangesTable.GetSafeReference();
        }
        public IChangeSubscriberRegistry GetChangeSubscriberRegistrySafely()
        {
            if (_changeSubscriberRegistry == null) return null;
            else return (IChangeSubscriberRegistry)_changeSubscriberRegistry.GetSafeReference();
        }
        public ITable GetParentTableSafely()
        {
            if (_parentTable == null) return null;
            else return (ITable)_parentTable.GetSafeReference();
        }
        public Dictionary<string, string> GetParentInsertColumnListExpressionsSafely()
        {
            Dictionary<string, string> retval = new Dictionary<string, string>();
            foreach (
                KeyValuePair<string, string> pair
                in ParentInsertColumnListExpressions
                )
                retval[pair.Key] = pair.Value;
            return retval;
        }
        //public IParentPassThroughInfo GetParentPassThroughSafely()
        //{
        //    if (_parentPassThrough == null) return null;
        //    else return (IParentPassThroughInfo)_parentPassThrough.GetSafeReference();
        //}
        public IHomeVableNameColumn GetHomeVableNameColumnSafely()
        {
            IHomeVableNameColumn retval = HomeVableNameColumn;
            if (retval != null)
                retval = (IHomeVableNameColumn)retval.GetSafeReference();
            return retval;
        }
        public ILeavesTable GetLeavesTableSafely()
        {
            if (_leavesTable == null) return null;
            else return (ILeavesTable)_leavesTable.GetSafeReference();
        }
        public IChildVablesTable GetChildVablesTableSafely()
        {
            if (_childVablesTable == null) return null;
            else return (IChildVablesTable)_childVablesTable.GetSafeReference();
        }
        public ITableIDColumn GetIDColumnSafely()
        {
            ITableIDColumn idColumn = IDColumn;
            if (idColumn == null) return null;
            else return (ITableIDColumn)idColumn.GetSafeReference();
        }
        public IInputterColumn GetInputterColumnSafely()
        {
            IInputterColumn inputterColumn = InputterColumn;
            if (inputterColumn == null) return null;
            else return (IInputterColumn)inputterColumn.GetSafeReference();
        }
        public IValidInputtersTable GetValidInputtersTableSafely()
        {
            if (_leavesTable == null) return null;
            else return (IValidInputtersTable)_validInputtersTable.GetSafeReference();
        }
        public ILastUpdateDtmColumn GetLastUpdateDtmColumnSafely()
        {
            ILastUpdateDtmColumn lastUpdateDtmColumn = LastUpdateDtmColumn;
            if (lastUpdateDtmColumn == null) return null;
            else return (ILastUpdateDtmColumn)lastUpdateDtmColumn.GetSafeReference();
        }
        public ILastUpdaterColumn GetLastUpdaterColumnSafely()
        {
            ILastUpdaterColumn lastUpdaterColumn = LastUpdaterColumn;
            if (lastUpdaterColumn == null) return null;
            else return (ILastUpdaterColumn)lastUpdaterColumn.GetSafeReference();
        }

        public string InsertColumnListSql(
            bool useLineBreaks = false
            , bool includeDefs = false
            , bool includeDefaultColumnConstraints = false
            , bool applyTrims = false
            , bool includeMutableColumns = true
            , bool prettify = false
            , int minDatatypeOffset = 0
            , int minNullabilityOffset = 0
            , int spacesToIndent = 0
            )
        {
            string retval = "";
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            ISqlTranslator sqlT = SqlTranslator;
            int trimSqlAdditionalLength = sqlT.TrimSqlAdditionalLength();
            if (includeDefs && prettify)
            {
                foreach (
                    Tuple<int, IColumn> t
                    in Columns.Values
                    )
                {
                    IColumn column = t.Item2;
                    int nameOffset = column.Name.Length + 1;
                    if (applyTrims && ((ITableColumn)column).IsToBeTrimmed)
                        nameOffset += trimSqlAdditionalLength;
                    if (nameOffset > minDatatypeOffset) minDatatypeOffset = nameOffset;
                    int dataTypeOffset = column.DataType.Length + 1;
                    if (dataTypeOffset > minNullabilityOffset) minNullabilityOffset = dataTypeOffset;
                }
            }
            else
            {
                minDatatypeOffset = 0;
                minNullabilityOffset = 0;
            }
            foreach (
                Tuple<int, ITableColumn> t in InsertableColumns.Values.OrderBy(t => t.Item1))
            {
                ITableColumn col = t.Item2;
                if (retval.Length == 0)
                    retval = col.IsToBeTrimmed ? sqlT.TrimSql(col.Name) : col.Name;
                else
                {
                    if (useLineBreaks) retval += lfAndLeadingSpaces;
                    retval += ", " + (col.IsToBeTrimmed ? sqlT.TrimSql(col.Name) : col.Name);
                }
                if (includeDefs)
                {
                    if (prettify)
                        retval
                            += new string(' ', minDatatypeOffset - col.Name.Length)
                            + col.DataType + new string(' ', minNullabilityOffset - col.DataType.Length)
                            + (col.IsNullable ? "     null" : " not null");
                    else
                        retval
                            += " " + col.DataType + " "
                            + (col.IsNullable ? "     null" : " not null");
                    if (includeDefaultColumnConstraints)
                    {
                        IDefaultConstraint dc = col.DefaultConstraint;
                        if (dc != null) retval += " default " + dc.ConstraintText;
                    }
                }
            }
            return retval;
        }

        public string UpdateColumnListSql()
        {
            string columnData = "";
            foreach (Tuple<int, ITableColumn> t in UpdatableColumns.Values.OrderBy(t => t.Item2))
                columnData += t.Item2.Name + ", ";
            columnData = columnData.Substring(0, columnData.Length - 2);
            return columnData;
        }

        public string ParentColumnListInsertExpressions(
            bool useLineBreaks = false
            , int spacesToIndent = 0
            )
        {
            string retval = "";
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            Dictionary<string, string> nameChanger = ParentInsertColumnListExpressions;
            ISqlTranslator sqlT = SqlTranslator;
            foreach (
                ITableColumn col in ParentTable.InsertableColumns.Values.OrderBy(t => t.Item1))
            {
                if (retval.Length == 0)
                    retval = nameChanger.ContainsKey(col.Name) ? nameChanger[col.Name] : col.Name;
                else
                {
                    if (useLineBreaks) retval += lfAndLeadingSpaces;
                    retval += ", " + (nameChanger.ContainsKey(col.Name) ? nameChanger[col.Name] : col.Name);
                }
            }
            return retval;
        }

        // Trigger methods

        public void PerformInsteadOfInsertTriggerLogic(IConnection conn)
        {
            ISqlTranslator sqlT = SqlTranslator;
            Debug.Assert(
                sqlT != null
                , "SqlTranslator cannot be null for ITable object "
                + "that is in the middle of executing a before-insert trigger body."
                );
            if (sqlT.UsesForEachRowTriggers)
                PerformInsteadOfInsertTriggerLogicForEachRow(conn);
            else
                PerformInsteadOfInsertTriggerLogicWithInsertedTable(conn);
        }
        public void PerformInsteadOfInsertTriggerLogicWithInsertedTable(IConnection conn)
        {
            Debug.Assert(
                conn.IsOpen
                , "Connection should already be open when "
                + "table is asked to perform its before-insert trigger logic."
                );
            IDatabaseSafe db = Database;
            Debug.Assert(
                db != null
                , "Database should already be set before "
                + "table is asked to perform its before-insert trigger logic."
                );
            if ((int)db.SqlExecuteReader("select top(1) 1 from inserted", conn).Count > 0)
            {
                ISqlTranslator sqlT = SqlTranslator;
                Debug.Assert(
                    sqlT != null
                    , "SqlTranslator should already be set before "
                    + "table is asked to perform its before-insert trigger logic."
                    );
                string updateDtmSql = "'" + sqlT.CurrentTimestamp(db, conn).ToString("yyyy-mmm-dd") + "'";
                string updaterSql = "'" + sqlT.CurrentUser(db, conn).Replace("'", "''") + "'";
                bool reallyPublishesChanges = PublishesChanges;
                if (reallyPublishesChanges)
                {
                    IChangeSubscriberRegistry subscriberRegistry = ChangeSubscriberRegistry;
                    Debug.Assert(
                        subscriberRegistry != null
                        , "If table is set for publishing changes then its "
                        + "ChangeSubscriberRegistry property cannot be null."
                        );
                    reallyPublishesChanges
                        = subscriberRegistry.HasAtLeastOneSubscription(
                            this.NameWithSchema
                            , conn
                            );
                }
                bool mustTrackChanges = IsAudited || reallyPublishesChanges;
                string identityColumnName = IdentityColumnName;
                ITableIDColumn idColumn = (ITableIDColumn)IDColumn;
                string idColumnName = idColumn != null ? idColumn.Name : null;
                ITable parentTable = ParentTable;
                string tempTableTag = new Random().Next(1000000).ToString();
                int? threshold = ForcedUseOfTempTriggerTableThreshold(TriggerTimings.InsteadOf);
                string tempInsertedName = "inserted";
                int lastIdentityValueGenerated = -1;
                bool? requiresTempTriggerTables
                    = ParentTable != null || threshold == 0 || mustTrackChanges ? true
                    : threshold == null || !threshold.HasValue ? false
                    : default(bool?);
                if (requiresTempTriggerTables == default(bool?))
                    requiresTempTriggerTables
                        = threshold.Value
                        <= (int)db.SqlExecuteSingleValue(
                            "select count(*) from inserted;"
                            , conn
                            );
                if (requiresTempTriggerTables.Value)
                {
                    tempInsertedName
                        = NameOfNewlyCreatedInsertedTriggerTable(
                            tempTableTag
                            , TriggerTimings.InsteadOfInsert
                            , conn
                            );
                    LoadInsertedTriggerTable(tempInsertedName, conn);
                    db.SqlExecuteNonQuery(
                        IndexTempTableByIdentityColumnSql(tempInsertedName)
                        );
                    if (idColumn != null && idColumn.IsNullable)
                    {
                        db.SqlExecuteNonQuery(
                            IndexTempTableByIDColumnSql(tempInsertedName)
                            );
                        idColumn.PopulateValuesInNullIDRows(
                            tempInsertedName
                            , TempInsertedIDBaseColumnName
                            , conn
                            );
                    }
                    if (ParentTable != null)
                        db.SqlExecuteReader(
                            InsertedTriggerTableAddToParentSql(tempInsertedName)
                            , conn
                            );
                }
                lastIdentityValueGenerated
                    = (int)db.SqlExecuteSingleValue(
                            InsertFromInsertedTriggerTableSql(
                                tempInsertedName
                                , updateDtmSql
                                , updaterSql
                                )
                            , conn
                            );
                if (IsAudited)
                    LoadChangeTableAfterInsert(
                        AuditTable
                        , updateDtmSql
                        , updaterSql
                        , tempInsertedName
                        , conn
                        );
                if (PublishesChanges)
                    LoadChangeTableAfterInsert(
                        PublishedChangesTable
                        , updateDtmSql
                        , updaterSql
                        , tempInsertedName
                        , conn
                        );
                if (db.Server.Vendor.Equals(Vendors.SQLServer()))
                    sqlT.ResetIdentity(lastIdentityValueGenerated, db, conn);
            }
        }
        public void PerformInsteadOfInsertTriggerLogicForEachRow(IConnection conn)
        {
            throw new NotImplementedException();
        }
        public void PerformInsteadOfUpdateTriggerLogic(IConnection conn)
        {
            ISqlTranslator sqlT = SqlTranslator;
            Debug.Assert(
                sqlT != null
                , "SqlTranslator cannot be null for ITable object "
                + "that is in the middle of executing a before-update trigger body."
                );
            if (sqlT.UsesForEachRowTriggers)
                PerformInsteadOfUpdateTriggerLogicForEachRow(conn);
            else
                PerformInsteadOfUpdateTriggerLogicWithInsertedTable(conn);
        }
        public void PerformInsteadOfUpdateTriggerLogicWithInsertedTable(IConnection conn)
        {
            Debug.Assert(
                conn.IsOpen
                , "Connection should already be open when "
                + "table is asked to perform its before-update trigger logic."
                );
            IDatabaseSafe db = Database;
            Debug.Assert(
                db != null
                , "Database should already be set before "
                + "table is asked to perform its before-update trigger logic."
                );
            if ((int)db.SqlExecuteReader("select top(1) 1 from inserted", conn).Count > 0)
            {
                if (!AllowsUpdates)
                    throw new AttemptedUpdateOfNonupdatableTableException(
                        IsFullyConstructed ? this : null
                        , Name
                        , GetType().FullName
                        );
                ISqlTranslator sqlT = SqlTranslator;
                Debug.Assert(
                    sqlT != null
                    , "SqlTranslator should already be set before "
                    + "table is asked to perform its before-update trigger logic."
                    );
                string updateDtmSql = "'" + sqlT.CurrentTimestamp(db, conn).ToString("yyyy-mmm-dd") + "'";
                string updaterSql = "'" + sqlT.CurrentUser(db, conn).Replace("'", "''") + "'";
                bool reallyPublishesChanges = PublishesChanges;
                if (reallyPublishesChanges)
                {
                    IChangeSubscriberRegistry subscriberRegistry = ChangeSubscriberRegistry;
                    Debug.Assert(
                        subscriberRegistry != null
                        , "If table is set for publishing changes then its "
                        + "ChangeSubscriberRegistry property cannot be null."
                        );
                    reallyPublishesChanges
                        = subscriberRegistry.HasAtLeastOneSubscription(
                            this.NameWithSchema
                            , conn
                            );
                }
                bool mustTrackChanges = IsAudited || reallyPublishesChanges;
                bool requiresMergedTable
                    = (IsAudited ? 1 : 0)
                    + (reallyPublishesChanges ? 1 : 0)
                    + (HasAtLeastOneImmutableColumn ? 1 : 0)
                    + (CascadingForeignKeyRegistry.HasAtLeastOneRelationship(
                        NameWithSchema
                        , actionIsDelete: false
                        , conn: conn
                        ) ? 1 : 0)
                    > 1;
                string identityColumnName = null;
                foreach (
                    KeyValuePair<string, Tuple<int, ITableColumn>> pair
                    in TableColumns.Where(p => p.Value.Item2.IsIdentity).Take(1)
                    )
                    identityColumnName = pair.Key;
                ITableIDColumn idColumn = (ITableIDColumn)IDColumn;
                string idColumnName = idColumn != null ? idColumn.Name : null;
                ITable parentTable = ParentTable;
                string tempTableTag = new Random().Next(1000000).ToString();
                int? threshold = ForcedUseOfTempTriggerTableThreshold(TriggerTimings.InsteadOfUpdate);
                string tempInsertedName = "inserted";
                string tempDeletedName = "deleted";
                string tempMergedName = null;
                bool? requiresTempTriggerTables
                    = ParentTable != null || threshold == 0 || mustTrackChanges ? true
                    : threshold == null || !threshold.HasValue ? false
                    : default(bool?);
                if (requiresTempTriggerTables == default(bool?))
                    requiresTempTriggerTables
                        = threshold.Value
                        <= (int)db.SqlExecuteSingleValue(
                            "select count(*) from inserted;"
                            , conn
                            );
                if (requiresTempTriggerTables.Value)
                {
                    tempInsertedName
                        = NameOfNewlyCreatedInsertedTriggerTable(
                            tempTableTag
                            , TriggerTimings.InsteadOfUpdate
                            , conn
                            );
                    LoadInsertedTriggerTable(tempInsertedName, conn);
                    db.SqlExecuteNonQuery(
                        IndexTempTableByIdentityColumnSql(tempInsertedName)
                        );
                    if (parentTable != null)
                        db.SqlExecuteNonQuery(
                            IndexTempTableByIDColumnSql(tempInsertedName)
                            );
                    tempDeletedName
                        = NameOfNewlyCreatedDeletedTriggerTable(
                            tempTableTag
                            , mustTrackChanges
                            , TriggerTimings.InsteadOfUpdate
                            , conn
                            );
                    LoadDeletedTriggerTable(
                        tempDeletedName
                        , reallyPublishesChanges
                        , conn
                        );
                    db.SqlExecuteNonQuery(
                        IndexTempTableByIdentityColumnSql(tempDeletedName)
                        );
                }
                if (requiresMergedTable)
                {
                    tempMergedName
                        = NameOfNewlyCreatedMergedTriggerTable(
                            tempTableTag
                            , conn
                            );
                    InterceptUpdateOfImmutableColumn(
                        tempMergedName
                        , conn
                        );
                }
                else
                    InterceptUpdateOfImmutableColumn(
                        tempInsertedName
                        , tempDeletedName
                        , conn
                        );
                db.SqlExecuteNonQuery(
                    UpdateFromInsertedTriggerTableSql(
                        tempInsertedName
                        , updateDtmSql
                        , updaterSql
                        )
                    );
                if (ParentTable != null)
                    db.SqlExecuteNonQuery(
                        ParentTable.UpdateFromInsertedTriggerTableSql(
                            tempInsertedName
                            , updateDtmSql
                            , updaterSql
                            )
                        );
                if (requiresMergedTable)
                {
                    foreach (
                        ICascadingForeignKeyRelationship r
                        in CascadingForeignKeyRegistry.RelationshipsList(
                            NameWithSchema
                            , actionIsDelete: false
                            , conn: conn
                            )
                        )
                        CascadeUpdate(r, tempMergedName, conn);
                    if (IsAudited)
                        LoadChangeTableAfterUpdate(
                            AuditTable
                            , updateDtmSql
                            , updaterSql
                            , tempMergedName
                            , conn
                            );
                    if (PublishesChanges)
                        LoadChangeTableAfterUpdate(
                            PublishedChangesTable
                            , updateDtmSql
                            , updaterSql
                            , tempMergedName
                            , conn
                            );
                }
                else
                {
                    foreach (
                        ICascadingForeignKeyRelationship r
                        in CascadingForeignKeyRegistry.RelationshipsList(
                            NameWithSchema
                            , actionIsDelete: false
                            , conn: conn
                            )
                        )
                        CascadeUpdate(r, tempInsertedName, tempDeletedName, conn);
                    if (IsAudited)
                        LoadChangeTableAfterUpdate(
                            AuditTable
                            , updateDtmSql
                            , updaterSql
                            , tempInsertedName
                            , tempDeletedName
                            , conn
                            );
                    if (PublishesChanges)
                        LoadChangeTableAfterUpdate(
                            PublishedChangesTable
                            , updateDtmSql
                            , updaterSql
                            , tempInsertedName
                            , tempDeletedName
                            , conn
                            );
                }
            }
        }

        public void PerformInsteadOfUpdateTriggerLogicForEachRow(IConnection conn)
        {
            throw new NotImplementedException();
        }
        public void PerformInsteadOfDeleteTriggerLogic(IConnection conn)
        {
            ISqlTranslator sqlT = SqlTranslator;
            Debug.Assert(
                sqlT != null
                , "SqlTranslator cannot be null for ITable object "
                + "that is in the middle of executing a before-delete trigger body."
                );
            if (sqlT.UsesForEachRowTriggers)
                PerformInsteadOfDeleteTriggerLogicForEachRow(conn);
            else
                PerformInsteadOfDeleteTriggerLogicWithInsertedTable(conn);
        }
        public void PerformInsteadOfDeleteTriggerLogicWithInsertedTable(IConnection conn)
        {
            Debug.Assert(
                conn.IsOpen
                , "Connection should already be open when "
                + "table is asked to perform its before-insert trigger logic."
                );
            IDatabaseSafe db = Database;
            Debug.Assert(
                db != null
                , "Database should already be set before "
                + "table is asked to perform its before-insert trigger logic."
                );
            if ((int)db.SqlExecuteReader("select top(1) 1 from deleted", conn).Count > 0)
            {
                if (!AllowsDeletes)
                    throw new AttemptedDeleteFromInvariableTableException(
                        IsFullyConstructed ? this : null
                        , Name
                        , GetType().FullName
                        );
                ISqlTranslator sqlT = SqlTranslator;
                Debug.Assert(
                    sqlT != null
                    , "SqlTranslator should already be set before "
                    + "table is asked to perform its before-delete trigger logic."
                    );
                string updateDtmSql = "'" + sqlT.CurrentTimestamp(db, conn).ToString("yyyy-mmm-dd") + "'";
                string updaterSql = "'" + sqlT.CurrentUser(db, conn).Replace("'", "''") + "'";
                bool reallyPublishesChanges = PublishesChanges;
                if (reallyPublishesChanges)
                {
                    IChangeSubscriberRegistry subscriberRegistry = ChangeSubscriberRegistry;
                    Debug.Assert(
                        subscriberRegistry != null
                        , "If table is set for publishing changes then its "
                        + "ChangeSubscriberRegistry property cannot be null."
                        );
                    reallyPublishesChanges
                        = subscriberRegistry.HasAtLeastOneSubscription(
                            this.NameWithSchema
                            , conn
                            );
                }
                bool mustTrackChanges = IsAudited || reallyPublishesChanges;
                string identityColumnName = IdentityColumnName;
                ITableIDColumn idColumn = (ITableIDColumn)IDColumn;
                string idColumnName = idColumn != null ? idColumn.Name : null;
                ITable parentTable = ParentTable;
                string tempTableTag = new Random().Next(1000000).ToString();
                int? threshold = ForcedUseOfTempTriggerTableThreshold(TriggerTimings.InsteadOfDelete);
                string tempDeletedName = "deleted";
                bool? requiresTempTriggerTables
                    = ParentTable != null || threshold == 0 || mustTrackChanges ? true
                    : threshold == null || !threshold.HasValue ? false
                    : default(bool?);
                if (requiresTempTriggerTables == default(bool?))
                    requiresTempTriggerTables
                        = threshold.Value
                        <= (int)db.SqlExecuteSingleValue(
                            "select count(*) from deleted;"
                            , conn
                            );
                if (requiresTempTriggerTables.Value)
                {
                    tempDeletedName
                        = NameOfNewlyCreatedDeletedTriggerTable(
                            tempTableTag
                            , mustTrackChanges
                            , TriggerTimings.InsteadOfDelete
                            , conn
                            );
                    LoadDeletedTriggerTable(tempDeletedName, true, conn);
                    db.SqlExecuteNonQuery(
                        IndexTempTableByIdentityColumnSql(tempDeletedName)
                        );
                    if (!idColumnName.Equals(identityColumnName))
                        db.SqlExecuteNonQuery(
                            IndexTempTableByIDColumnSql(tempDeletedName)
                            );
                }
                db.SqlExecuteNonQuery(
                    DeleteFromDeletedTriggerTableSql(tempDeletedName)
                    );
                if (ParentTable != null)
                    db.SqlExecuteNonQuery(
                        ParentTable.DeleteFromDeletedTriggerTableSql(tempDeletedName)
                        );
                foreach (
                    ICascadingForeignKeyRelationship r
                    in CascadingForeignKeyRegistry.RelationshipsList(
                        NameWithSchema
                        , actionIsDelete: true
                        , conn: conn
                        )
                    )
                    CascadeDelete(r, tempDeletedName, conn);
                if (IsAudited)
                    LoadChangeTableAfterDelete(
                        AuditTable
                        , updateDtmSql
                        , updaterSql
                        , tempDeletedName
                        , conn
                        );
                if (PublishesChanges)
                    LoadChangeTableAfterDelete(
                        PublishedChangesTable
                        , updateDtmSql
                        , updaterSql
                        , tempDeletedName
                        , conn
                        );
            }
        }
        public void PerformInsteadOfDeleteTriggerLogicForEachRow(IConnection conn)
        {
            throw new NotImplementedException();
        }
        public void PerformAfterInsertTriggerLogic(IConnection conn) { }
        public void PerformAfterInsertTriggerLogicWithInsertedTable(IConnection conn) { }
        public void PerformAfterInsertTriggerLogicForEachRow(IConnection conn) { }
        public void PerformAfterAnythingTriggerLogic(IConnection conn) { }
        public void PerformAfterAnythingTriggerLogicWithInsertedTable(IConnection conn) { }
        public void PerformAfterAnythingTriggerLogicForEachRow(IConnection conn) { }

        public string TriggerTimingTag(TriggerTimings timing)
        {
            switch (timing)
            {
                case TriggerTimings.BeforeInsert: return "_ioI";
                case TriggerTimings.BeforeUpdate: return "_ioU";
                case TriggerTimings.BeforeDelete: return "_ioD";
                case TriggerTimings.AfterInsert: return "_aI";
                case TriggerTimings.AfterAnything: return "_aIUD";
                default: return "";
            }
        }

        public string LFAndLeadingSpaces(int spacesToIndent) { return "\n" + new string(' ', spacesToIndent); }

        /// <summary>
        /// Builds the SQL string required to load
        /// a temporary inserted/deleted table.
        /// </summary>
        /// <remarks>
        /// The default logic is an ANSI-standard
        /// insert statement and is quite simple.
        /// Where there are tables that do
        /// things like establishing defaults by
        /// joining to other database tables, it is best
        /// to override the insert-/delete-specific
        /// functions that are this functions clients,
        /// <em>i.e.</em>, InsertedTriggerTableLoadSql
        /// and DeletedTriggerTableLoadSql.
        /// </remarks>
        /// <param name="tempTableName">The name of the
        /// temporary table to be loaded.</param>
        /// <param name="includeMutableColumns">Flag
        /// specifing whether variant columns are
        /// included in the table.</param>
        /// <param name="spacesToIndent">Specifies 
        /// how many leading spaces to put in front of
        /// each line.</param>
        /// <returns></returns>
        public string TriggerTableLoadSql(
            string tempTableName
            , bool includeMutableColumns
            , int spacesToIndent = 0
            )
        {
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string identityColumnName = IdentityColumnName;
            string sql = ""
                + lfAndLeadingSpaces + "insert into " + tempTableName
                + lfAndLeadingSpaces + "       ( ";
            if (identityColumnName != null)
                sql += ""
                    + lfAndLeadingSpaces + "       " + identityColumnName
                    + lfAndLeadingSpaces + "     , ";
            else
                sql += lfAndLeadingSpaces + "       ";
            sql +=
                InsertColumnListSql(
                    useLineBreaks: true
                    , includeDefs: false
                    , includeDefaultColumnConstraints: false
                    , applyTrims: false
                    , includeMutableColumns: includeMutableColumns
                    , prettify: false
                    , spacesToIndent: 5 + spacesToIndent
                    )
                + lfAndLeadingSpaces + "       ) "
                + lfAndLeadingSpaces + "select ";
            if (identityColumnName != null)
                sql += identityColumnName
                    + lfAndLeadingSpaces + "     , ";
            sql +=
                InsertColumnListSql(
                    useLineBreaks: true
                    , includeDefs: false
                    , includeDefaultColumnConstraints: false
                    , applyTrims: true
                    , includeMutableColumns: true
                    , prettify: false
                    , spacesToIndent: 5
                    )
                + lfAndLeadingSpaces + "  from inserted";
            return sql;
        }

        /// <summary>
        /// Allows the table to force the use of a
        /// temporary inserted table in an insert
        /// or update trigger.
        /// </summary>
        /// <remarks>
        /// The most common reason to force the use
        /// of a temporary table is that one is using
        /// default values that are drawn from other 
        /// tables -- for example, user preferences --
        /// and therefore the inserted table must be
        /// joined to other tables. Since the inserted
        /// table is not indexed this can have disastrous
        /// effects on performance with large inserts
        /// or updates. 
        /// <para>
        /// On the other hand, for a small insert
        /// such as a single row, the overhead
        /// of a temporary table puts a significant
        /// drag on performance. So you can specify a
        /// minimum number of rows at which the 
        /// inserted table will be created and return
        /// that as the value of the method. Returning
        /// 0 causes a temporary table always to be
        /// used. Returning default(int?) means
        /// that you never force the use of the
        /// temporary table (though its use
        /// may be forced through other methods
        /// that automatically cause the temporary
        /// table to be created, such as
        /// when there is a polymorphic parent
        /// table with multiple logical primary
        /// key fields).</para>
        /// </remarks>
        /// <param name="timing">Specifies when the trigger fires.</param>
        /// <returns>Number of rows at which use of
        /// a temporary inserted table is forced.</returns>
        public int? ForcedUseOfTempTriggerTableThreshold(
            TriggerTimings timing
            , 
            )
        {
            return default(int?);
        }

        protected virtual void LoadChangeTable(
            IChangeTrackingTable changeTable
            , string action
            , string updateDtmSql
            , string updaterSql
            , string sourceTempTableName
            , string tempDeletedTableName
            , IConnection conn
            )
        {
            Debug.Assert(
                action.Equals("insert") || action.Equals("update") || action.Equals("delete")
                , "Action must be either 'insert', 'update', or 'delete'."
                );
            Debug.Assert(
                conn.IsOpen
                , "Connection should already be open when "
                + "table is asked to perform its before-insert trigger logic."
                );
            IDatabaseSafe db = Database;
            Debug.Assert(
                db != null
                , "Database should already be set before "
                + "table is asked to perform its before-insert trigger logic."
                );
            ISqlTranslator sqlT = SqlTranslator;
            Debug.Assert(
                sqlT != null
                , "SqlTranslator should already be set before "
                + "table is asked to perform its before-insert trigger logic."
                );
            Debug.Assert(
                changeTable != null
                , "LoadChangeTable should not be called "
                + "with a null PublishedChangesTable parameter."
                );
            string sql;
            string selectSql = "select ";
            bool usesTwo = tempDeletedTableName != null;
            string identityColumnName = usesTwo ? IdentityColumnName : "";
            string oldAlias = action == "update" && usesTwo ? "i" : "d";
            string primaryAlias = action == "delete" ? "d" : "i";
            bool alreadyDidOne = false;
            sql = ""
                + "insert into " + changeTable.FullName + "\n"
                + "       ( \n"
                + "        \n";
            foreach (
                Tuple<int, ITableColumn> t
                in changeTable.TableColumns.Values
                .Where(t => t.Item2.IsInsertable).OrderBy(t => t.Item1)
                )
            {
                string name = t.Item2.Name;
                string nameInLowerCase = name.ToLower();
                sql += alreadyDidOne ? "      , " : "        " + name + " \n";
                if (typeof(IChangeResponderColumn).IsAssignableFrom(t.Item2.GetType()))
                    selectSql
                         += alreadyDidOne ? "      , " : "        " + "'registry.responderClassFullName' \n";
                else if (nameInLowerCase.Equals("action"))
                    selectSql
                         += alreadyDidOne ? "      , " : "        '" + action + "' \n";
                else if (nameInLowerCase.Equals("updatedtm"))
                    selectSql
                         += alreadyDidOne ? "      , " : "        " + updateDtmSql + " \n";
                else if (nameInLowerCase.Equals("updater"))
                    selectSql
                         += alreadyDidOne ? "      , " : "        " + updaterSql + " \n";
                else if (nameInLowerCase.Substring(0, 3).Equals("new") && action == "delete")
                    selectSql
                         += alreadyDidOne ? "      , " : "        null \n";
                else if (nameInLowerCase.Substring(0, 3).Equals("new") && (action == "insert" || usesTwo))
                    selectSql
                         += alreadyDidOne ? "      , " : "        i." + name.Substring(3) + " \n";
                else if (nameInLowerCase.Substring(0, 3).Equals("old") && action == "insert")
                    selectSql
                         += alreadyDidOne ? "      , " : "        null \n";
                else if (nameInLowerCase.Substring(0, 3).Equals("old") && (action == "delete" || usesTwo))
                    selectSql
                         += alreadyDidOne ? "      , " : "        d." + name.Substring(3) + " \n";
                else
                    selectSql
                         += alreadyDidOne ? "      , " : "        i." + name + " \n";
                alreadyDidOne = true;
            }
            sql += ""
                + "       ) \n"
                + selectSql
                + "  from " + sourceTempTableName + " " + oldAlias + " \n";
            if (usesTwo)
                sql += ""
                    + "  from " + sourceTempTableName + " " + oldAlias + " \n"
                    + "         join " + tempDeletedTableName + " d \n"
                    + "           on d." + identityColumnName + " = i." + identityColumnName + " \n";
            sql += ""
                + "         join " + ChangeSubscriberRegistry.FullName + " registry \n"
                + "           on registry.publishingTableNameWithSchema = '" + NameWithSchema.Replace("'", "''") + "' \n"
                + ";"
                ;
            db.SqlExecuteNonQuery(sql, conn);
        }

        // Support for before-insert triggers

        /// <summary>
        /// In collaboration with SqlTranslator, creates
        /// a temporary inserted table appropriate for
        /// the specified trigger timing and returns
        /// the name of the table.
        /// </summary>
        /// <param name="tempTableTag">The randomized
        /// string to be applied to the end of the
        /// temporary table's name in order to
        /// ensure that it does not collide with
        /// a temp table created by the same trigger
        /// for another simultaneous user.</param>
        /// <param name="conn">An open database connection to
        /// be used for the creation of the temp table.</param>
        /// <param name="tempTableTag">The randomized tag,
        /// if any, to be applied to the standard root name
        /// for such tables.</param>
        /// <param name="timing">Specifies when the trigger fires.</param>
        /// <returns>The name given to the temporary table.</returns>
        public string NameOfNewlyCreatedInsertedTriggerTable(
            string tempTableTag
            , TriggerTimings timing
            , IConnection conn
            )
        {
            Debug.Assert(
                conn.IsOpen
                , "Connection should already be open when "
                + "table is asked to load its temp inserted table."
                );
            ISqlTranslator sqlT = SqlTranslator;
            Debug.Assert(
                sqlT != null
                , "SqlTranslator is null; table is not in a usable state "
                + "and therefore ITable.NameOfNewlyCreatedInsertedTriggerTable should "
                + "not have been called for table '"
                + Name != null ? Name : "[unnamed]"
                + "' of type "
                + GetType().FullName
                + "."
                );
            // bool makeInMemory = sqlT.SupportsInMemoryTempTables;
            bool makeInMemory = false;
            string tempTableNameBase
                = makeInMemory ? "i"
                : Name
                + TriggerTimingTag(timing)
                + "_i"
                + (tempTableTag == null ? "" : tempTableTag.Trim());
            string retval
                = sqlT.TempTableName(
                    tempTableNameBase
                    , true
                    , makeInMemory
                    );
            Database.SqlExecuteNonQuery(
                InsertedTriggerTableCreateSql(
                    retval
                    , !makeInMemory
                    , makeInMemory
                    , timing
                    )
                );
            return retval;
        }

        /// <summary>
        /// In collaboration with SqlTranslator, builds
        /// the SQL string required to create
        /// a temporary inserted table appropriate for
        /// the specified trigger timing.
        /// </summary>
        /// <remarks>
        /// The SQL translator can provide a default
        /// version that is adequate for most tables,
        /// and the default implementation takes the
        /// default provided by the SQL translator.
        /// However, where there are tables that do
        /// things like establishing defaults by
        /// joining to other database tables, the
        /// method can be overriden to provide
        /// more complex logic as required.
        /// </remarks>
        /// <param name="tempTableName">The name to be
        /// given to the temporary table.</param>
        /// <param name="timing">Specifies when the trigger fires.</param>
        /// <returns></returns>
        public string InsertedTriggerTableCreateSql(
            string tempTableName
            , bool globallyAvailable
            , bool createInMemoryTable
            , TriggerTimings timing
            , int spacesToIndent = 0
            )
        {
            ISqlTranslator sqlT = SqlTranslator;
            Debug.Assert(
                sqlT != null
                , "SqlTranslator is null; table is not in a usable state "
                + "and therefore ITable.InsertedTriggerTableCreateSql should "
                + "not have been called for table '"
                + Name != null ? Name : "[unnamed]"
                + "' of type "
                + GetType().FullName
                + "."
                );
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string identityColumnName = IdentityColumnName;
            if (identityColumnName == null && timing == TriggerTimings.BeforeUpdate)
                throw new NoIdentityColumnException(
                    this
                    , Name
                    , GetType().FullName
                    );
            string sql = ""
                + lfAndLeadingSpaces + "       "
                + sqlT.TempTableCreateSqlSnip(
                    tempTableName
                    , globallyAvailable
                    , createInMemoryTable
                    )
                + lfAndLeadingSpaces + "       ( ";
            if (identityColumnName != null)
            {
                sql += ""
                    + lfAndLeadingSpaces + "       " + identityColumnName + " int not null primary key "
                    + lfAndLeadingSpaces + "     , ";
            }
            else
                sql += lfAndLeadingSpaces + "       ";
            sql +=
                InsertColumnListSql(
                    useLineBreaks: true
                    , includeDefs: true
                    , includeDefaultColumnConstraints: false
                    , applyTrims: false
                    , includeMutableColumns: true
                    , prettify: false
                    , spacesToIndent: 5
                    )
                + lfAndLeadingSpaces + "       ); ";
            return sql;
        }

        /// <summary>
        /// Loads a temporary inserted table appropriate for
        /// the specified trigger timing and returns
        /// the name of the table.
        /// </summary>
        /// <param name="tempTableName">The name
        /// of the temporary inserted table.</param>
        /// <param name="conn">An open database connection to
        /// be used for loading the temp table.</param>
        public void LoadInsertedTriggerTable(
            string tempTableName
            , IConnection conn
            )
        {
            Debug.Assert(
                conn.IsOpen
                , "Connection should already be open when "
                + "table is asked to load its temp inserted table."
                );
            ISqlTranslator sqlT = SqlTranslator;
            Debug.Assert(
                sqlT != null
                , "SqlTranslator should already be set before "
                + "table is asked to load its temp inserted table."
                );
            IDatabaseSafe db = Database;
            Debug.Assert(
                db != null
                , "Database should already be set before "
                + "table is asked to load its temp inserted table."
                );
            db.SqlExecuteNonQuery(
                InsertedTriggerTableLoadSql(tempTableName)
                );
        }

        /// <summary>
        /// Builds the SQL string required to load
        /// a temporary inserted table appropriate for
        /// the specified trigger timing.
        /// </summary>
        /// <remarks>
        /// The default logic is an ANSI-standard
        /// insert statement and is quite simple.
        /// However, where there are tables that do
        /// things like establishing defaults by
        /// joining to other database tables, the
        /// method can be overriden to provide
        /// more complex logic as required.
        /// </remarks>
        /// <param name="tempTableName">The name of the
        /// temporary table to be loaded.</param>
        /// <param name="spacesToIndent">Specifies 
        /// how many leading spaces to put in front of
        /// each line.</param>
        /// <returns></returns>
        public string InsertedTriggerTableLoadSql(string tempTableName, int spacesToIndent = 0)
        {
            return TriggerTableLoadSql(
                tempTableName
                , true
                , spacesToIndent
                );
        }

        /// <summary>
        /// In collaboration with SqlTranslator, builds
        /// the SQL string required to index a temporary
        /// table by the identity column.
        /// </summary>
        /// <remarks>
        /// The ANSI standard version is adequate for most vendors;
        /// so we do not bother with consulting the SqlTranslator.
        /// <para>
        /// The default name of the index is the temporary
        /// table name, plus _i, plus the constraint tag
        /// of the permanent table's identity column.</para>
        /// </remarks>
        /// <param name="tempTableName">The name of the
        /// temporary table to be loaded.</param>
        /// <param name="tempTableIndexName">The name
        /// of the index, in the extremely rare case
        /// where the default is not acceptable.</param>
        /// <returns></returns>
        public string IndexTempTableByIdentityColumnSql(
            string tempTableName
            , string tempTableIndexName = null
            )
        {
            if (tempTableName == null || tempTableName.Trim().Length == 0)
                throw new NullArgumentException(
                    "tempTableName"
                    , FullTypeNameForCodeErrors
                    + ".IndexTempTableByIdentityColumnSql(tempTableName, tempTableIndexName)"
                    );
            ITableColumn identityColumn = (ITableColumn)IdentityColumn;
            if (tempTableIndexName == null || tempTableIndexName.Trim().Length == 0)
            {
                tempTableIndexName
                    = tempTableName
                    + "_i"
                    + identityColumn != null
                    && identityColumn.ConstraintTag != null
                    ? identityColumn.ConstraintTag
                    : "ID";
            }
            return
                "create index "
                + tempTableIndexName
                + " on " + tempTableName + "( " + identityColumn.Name + " );";
        }

        /// <summary>
        /// In collaboration with SqlTranslator, builds
        /// the SQL string required to index a temporary
        /// table by ID.
        /// </summary>
        /// <remarks>
        /// The ANSI standard version is adequate for most vendors;
        /// so we do not bother with consulting the SqlTranslator.
        /// <para>
        /// The default name of the index is the temporary
        /// table name, plus _i, plus the constraint tag
        /// of the permanent table's ID column.</para>
        /// </remarks>
        /// <param name="tempTableName">The name of the
        /// temporary table to be loaded.</param>
        /// <param name="tempTableIndexName">The name
        /// of the index, in the extremely rare case
        /// where the default is not acceptable.</param>
        /// <returns></returns>
        public string IndexTempTableByIDColumnSql(
            string tempTableName
            , string tempTableIndexName = null
            )
        {
            if (tempTableName == null || tempTableName.Trim().Length == 0)
                throw new NullArgumentException(
                    "tempTableName"
                    , FullTypeNameForCodeErrors
                    + ".IndexTempTableByIDColumnSql(tempTableName, tempTableIndexName)"
                    );
            ITableColumn idColumn = IDColumn;
            if (tempTableIndexName == null || tempTableIndexName.Trim().Length == 0)
            {
                tempTableIndexName
                    = tempTableName
                    + "_i"
                    + idColumn != null
                    && idColumn.ConstraintTag != null
                    ? idColumn.ConstraintTag
                    : "ID";
            }
            return
                "create index "
                + tempTableIndexName
                + " on " + tempTableName + "( " + idColumn.Name + " );";
        }

        /// <summary>
        /// In collaboration with SqlTranslator, builds
        /// the SQL string required to synch a parent table
        /// to newly inserted rows in a child table.
        /// </summary>
        /// <remarks>
        /// The SQL translator can provide a default
        /// version that is adequate for most tables,
        /// and the default implementation takes the
        /// default provided by the SQL translator.
        /// However, where there are tables that do
        /// things like establishing defaults by
        /// joining to other database tables, the
        /// method can be overriden to provide
        /// more complex logic as required.
        /// </remarks>
        /// <param name="tempTableName">The name of the
        /// temporary table to be loaded.</param>
        /// <param name="timing">Specifies when the trigger fires.</param>
        /// <returns></returns>
        public string InsertedTriggerTableAddToParentSql(
            string tempTableName
            , int spacesToIndent = 0
            )
        {
            if (tempTableName == null)
                throw new NullArgumentException(
                    "tempTableName"
                    , FullTypeNameForCodeErrors
                    + "DefaultInsertedTriggerTableAddToParentSql(table, tempTableName, spacesToIndent)"
                    );
            ITable parentTable = ParentTable;
            if (parentTable == null)
                throw new NotAPolymorphicChildException(
                    this
                    , Name
                    , GetType().FullName
                    , "InsertedTriggerTableAddToParentSql(tempTableName, spacesToIndent)"
                    );
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string identityColumnName = IdentityColumnName;
            string nameWithSchema = NameWithSchema;
            string parentTableFullName = parentTable.FullName;
            string parentTableIDColumnName = parentTable.IDColumnName;
            string parentTableInsertColumnList
                = parentTable.InsertColumnListSql(
                    useLineBreaks: true
                    , includeDefs: false
                    , includeDefaultColumnConstraints: false
                    , applyTrims: false
                    , includeMutableColumns: true
                    , prettify: false
                    , spacesToIndent: 5 + spacesToIndent
                    );
            ILastUpdateDtmColumn parentLastUpdateDtmColumn = parentTable.LastUpdateDtmColumn;
            string parentLastUpdateDtmColumnName = parentLastUpdateDtmColumn.Name;
            ILastUpdaterColumn parentLastUpdaterColumn = parentTable.LastUpdaterColumn;
            string parentLastUpdaterColumnName = parentLastUpdaterColumn.Name;
            string sql = ""
                + lfAndLeadingSpaces + "insert into " + parentTableFullName
                + lfAndLeadingSpaces + "       ( "
                + lfAndLeadingSpaces + "       " + parentTableIDColumnName
                + lfAndLeadingSpaces + "     , " + parentTableInsertColumnList
                + lfAndLeadingSpaces + "       ) "
                + lfAndLeadingSpaces + "select i." + identityColumnName
                + lfAndLeadingSpaces + "     , " + ParentColumnListInsertExpressions(true, spacesToIndent)
                + lfAndLeadingSpaces + "   from " + tempTableName
                + lfAndLeadingSpaces + "; "
                ;
            return sql;
        }

        /// <summary>
        /// In collaboration with SqlTranslator, builds
        /// the SQL string required to insert into
        /// a table using the data from an inserted
        /// table or its double-hash replacement.
        /// </summary>
        /// <remarks>
        /// The SQL translator can provide a default
        /// version that is adequate for most tables,
        /// and the default implementation takes the
        /// default provided by the SQL translator.
        /// However, where there are tables that do
        /// things like establishing defaults by
        /// joining to other database tables, the
        /// method can be overriden to provide
        /// more complex logic as required.
        /// </remarks>
        /// <param name="tempTableName">The name of the
        /// temporary table to be loaded.</param>
        /// <param name="timing">Specifies when the trigger fires.</param>
        /// <returns></returns>
        public string InsertFromInsertedTriggerTableSql(
            string tempTableName
            , string currentTimestampSql
            , string userSql
            , int spacesToIndent = 0
            )
        {
            ISqlTranslator sqlT = SqlTranslator;
            Debug.Assert(
                sqlT != null
                , "SqlTranslator should already be set before "
                + "table's InsertFromInsertedTriggerTableSql is called."
                );
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string sql = ""
                + lfAndLeadingSpaces + "insert into " + Name
                + lfAndLeadingSpaces + "       ( "
                + lfAndLeadingSpaces + "       "
                + InsertColumnListSql(
                    useLineBreaks: true
                    , includeDefs: false
                    , includeDefaultColumnConstraints: false
                    , applyTrims: false
                    , includeMutableColumns: true
                    , prettify: false
                    , spacesToIndent: 5 + spacesToIndent
                    )
                +
                (HasLastUpdateDtmColumn
                ? lfAndLeadingSpaces + "       " + LastUpdateDtmColumn.Name
                : ""
                )
                +
                (
                HasLastUpdaterColumn
                ? lfAndLeadingSpaces + "       " + LastUpdaterColumn.Name
                : ""
                )
                + lfAndLeadingSpaces + "       ) "
                + lfAndLeadingSpaces + "select "
                + InsertColumnListSql(
                    useLineBreaks: true
                    , includeDefs: false
                    , includeDefaultColumnConstraints: false
                    , applyTrims: false
                    , includeMutableColumns: true
                    , prettify: false
                    , spacesToIndent: 5 + spacesToIndent
                    )
                +
                (HasLastUpdateDtmColumn
                ? lfAndLeadingSpaces + "       " + (currentTimestampSql != null ? currentTimestampSql : sqlT.CurrentTimestampSql)
                : ""
                )
                +
                (
                HasLastUpdaterColumn
                ? lfAndLeadingSpaces + "       " + (userSql != null ? userSql : sqlT.CurrentUserSql)
                : ""
                )
                + lfAndLeadingSpaces + "  from " + tempTableName
                + lfAndLeadingSpaces + ";"
                ;
            if (sqlT.Vendor.Equals(Vendors.SQLServer()))
                sql += ""
                + lfAndLeadingSpaces + "select @@SCOPE_IDENTITY;";
            return sql;
        }

        public virtual void LoadChangeTableAfterInsert(
            IChangeTrackingTable changeTable
            , string updateDtmSql
            , string updaterSql
            , string tempInsertedName
            , IConnection conn
            )
        {
            if (changeTable == null)
                throw new NullArgumentException(
                    "changeTable"
                    , FullTypeNameForCodeErrors
                    + ".LoadChangeTableAfterInsert(changeTable, updateDtmSql, updaterSql, sourceTempTableName, conn)"
                    );
            LoadChangeTable(
                changeTable
                , "insert"
                , updateDtmSql
                , updaterSql
                , tempInsertedName
                , null
                , conn
                );
        }

        // Support for before-update triggers

        /// <summary>
        /// In collaboration with SqlTranslator, creates
        /// a temporary deleted table appropriate for
        /// the specified trigger timing and returns
        /// the name of the table.
        /// </summary>
        /// <param name="tempTableTag">The randomized
        /// string to be applied to the end of the
        /// temporary table's name in order to
        /// ensure that it does not collide with
        /// a temp table created by the same trigger
        /// for another simultaneous user.</param>
        /// <param name="conn">An open database connection to
        /// be used for the creation of the temp table.</param>
        /// <param name="tempTableTag">The randomized tag,
        /// if any, to be applied to the standard root name
        /// for such tables.</param>
        /// <param name="timing">Specifies when the trigger fires.</param>
        /// <returns>The name given to the temporary table.</returns>
        public string NameOfNewlyCreatedDeletedTriggerTable(
            string tempTableTag
            , bool includeMutableColumns
            , TriggerTimings timing
            , IConnection conn
            )
        {
            Debug.Assert(
                conn.IsOpen
                , "Connection should already be open when "
                + "table is asked to load its temp deleted table."
                );
            ISqlTranslator sqlT = SqlTranslator;
            Debug.Assert(
                sqlT != null
                , "SqlTranslator is null; table is not in a usable state "
                + "and therefore ITable.NameOfNewlyCreatedDeletedTriggerTable should "
                + "not have been called for table '"
                + Name != null ? Name : "[unnamed]"
                + "' of type "
                + GetType().FullName
                + "."
                );
            bool makeInMemory = sqlT.SupportsInMemoryTempTables;
            string tempTableNameBase
                = makeInMemory ? "d"
                : Name
                + TriggerTimingTag(timing)
                + "_d"
                + (tempTableTag == null ? "" : tempTableTag.Trim());
            string retval
                = sqlT.TempTableName(
                    tempTableNameBase
                    , false
                    , makeInMemory
                    );
            Database.SqlExecuteNonQuery(
                DeletedTriggerTableCreateSql(
                    retval
                    , includeMutableColumns
                    , !makeInMemory
                    , makeInMemory
                    , timing
                    )
                );
            return retval;
        }

        /// <summary>
        /// In collaboration with SqlTranslator, builds
        /// the SQL string required to create
        /// a temporary deleted table appropriate for
        /// the specified trigger timing.
        /// </summary>
        /// <remarks>
        /// The SQL translator can provide a default
        /// version that is adequate for most tables,
        /// and the default implementation takes the
        /// default provided by the SQL translator.
        /// However, where there are tables that do
        /// things like establishing defaults by
        /// joining to other database tables, the
        /// method can be overriden to provide
        /// more complex logic as required.
        /// <para>
        /// The default version assumes that if
        /// the trigger is a BeforeUpdate trigger then
        /// we need only the primary keys and the 
        /// invariants; if the trigger is an AfterAnything
        /// trigger then we need all columns; and otherwise
        /// it can simply return the empty string.</para>
        /// </remarks>
        /// <param name="tempTableName">The name to be
        /// given to the temporary table.</param>
        /// <param name="timing">Specifies when the trigger fires.</param>
        /// <returns></returns>
        public string DeletedTriggerTableCreateSql(
            string tempTableName
            , bool includeMutableColumns
            , bool globallyAvailable
            , bool createInMemoryTable
            , TriggerTimings timing
            , int spacesToIndent = 0
            )
        {
            ISqlTranslator sqlT = SqlTranslator;
            Debug.Assert(
                sqlT != null
                , "SqlTranslator is null; table is not in a usable state "
                + "and therefore ITable.DeletedTriggerTableCreateSql should "
                + "not have been called for table '"
                + Name != null ? Name : "[unnamed]"
                + "' of type "
                + GetType().FullName
                + "."
                );
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string identityColumnName = IdentityColumnName;
            if (identityColumnName == null && timing == TriggerTimings.BeforeUpdate)
                throw new NoIdentityColumnException(
                    this
                    , Name
                    , GetType().FullName
                    );
            string sql = ""
                + lfAndLeadingSpaces + "       "
                + sqlT.TempTableCreateSqlSnip(
                    tempTableName
                    , globallyAvailable
                    , createInMemoryTable
                    )
                + lfAndLeadingSpaces + "       ( ";
            if (identityColumnName != null)
            {
                sql += ""
                    + lfAndLeadingSpaces + "       " + identityColumnName + " int not null primary key "
                    + lfAndLeadingSpaces + "     , ";
            }
            else
                sql += lfAndLeadingSpaces + "       ";
            sql +=
                InsertColumnListSql(
                    useLineBreaks: true
                    , includeDefs: true
                    , includeDefaultColumnConstraints: false
                    , applyTrims: false
                    , includeMutableColumns: includeMutableColumns
                    , prettify: false
                    , spacesToIndent: 5 + spacesToIndent
                    )
                + lfAndLeadingSpaces + "       ); ";
            return sql;
        }

        /// <summary>
        /// In collaboration with SqlTranslator, creates
        /// a temporary inserted table, merging the insert
        /// and deleted tables into a single table with
        /// "old" and "new" records for everything other than
        /// the identity column; and finally returns
        /// the name of the new table.
        /// </summary>
        /// <param name="tempTableTag">The randomized
        /// string to be applied to the end of the
        /// temporary table's name in order to
        /// ensure that it does not collide with
        /// a temp table created by the same trigger
        /// for another simultaneous user.</param>
        /// <param name="conn">An open database connection to
        /// be used for the creation of the temp table.</param>
        /// <param name="tempTableTag">The randomized tag,
        /// if any, to be applied to the standard root name
        /// for such tables.</param>
        /// <param name="timing">Specifies when the trigger fires.</param>
        /// <returns>The name given to the temporary table.</returns>
        public string NameOfNewlyCreatedMergedTriggerTable(
            string tempTableTag
            , IConnection conn
            )
        {
            Debug.Assert(
                conn.IsOpen
                , "Connection should already be open when "
                + "table is asked to load its temp merged table."
                );
            ISqlTranslator sqlT = SqlTranslator;
            Debug.Assert(
                sqlT != null
                , "SqlTranslator is null; table is not in a usable state "
                + "and therefore ITable.NameOfNewlyCreatedMergedTriggerTable should "
                + "not have been called for table '"
                + Name != null ? Name : "[unnamed]"
                + "' of type "
                + GetType().FullName
                + "."
                );
            // bool makeInMemory = sqlT.SupportsInMemoryTempTables;
            bool makeInMemory = false;
            string tempTableNameBase
                = makeInMemory ? "m"
                : Name
                + TriggerTimingTag(TriggerTimings.BeforeUpdate)
                + "_m"
                + (tempTableTag == null ? "" : tempTableTag.Trim());
            string retval
                = sqlT.TempTableName(
                    tempTableNameBase
                    , true
                    , makeInMemory
                    );
            Database.SqlExecuteNonQuery(
                MergedTriggerTableCreateSql(
                    retval
                    , !makeInMemory
                    , makeInMemory
                    )
                );
            return retval;
        }

        /// <summary>
        /// In collaboration with SqlTranslator, builds
        /// the SQL string required to create
        /// a temporary table merging the inserted and
        /// deleted tables.
        /// </summary>
        /// <remarks>
        /// The SQL translator can provide a default
        /// version that is adequate for most tables,
        /// and the default implementation takes the
        /// default provided by the SQL translator.
        /// However, where there are tables that do
        /// things like establishing defaults by
        /// joining to other database tables, the
        /// method can be overriden to provide
        /// more complex logic as required.
        /// </remarks>
        /// <param name="tempTableName">The name to be
        /// given to the temporary table.</param>
        /// <param name="timing">Specifies when the trigger fires.</param>
        /// <returns></returns>
        public string MergedTriggerTableCreateSql(
            string tempTableName
            , bool globallyAvailable
            , bool createInMemoryTable
            , bool prettify = false
            , int minDatatypeOffset = 0
            , int minNullabilityOffset = 0
            , int spacesToIndent = 0
            )
        {
            string retval;
            ISqlTranslator sqlT = SqlTranslator;
            Debug.Assert(
                sqlT != null
                , "SqlTranslator is null; table is not in a usable state "
                + "and therefore ITable.MergedTriggerTableCreateSql should "
                + "not have been called for table '"
                + Name != null ? Name : "[unnamed]"
                + "' of type "
                + GetType().FullName
                + "."
                );
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string boolSql = sqlT.BooleanTypeSql();
            string trueSql = sqlT.TrueSql();
            string falseSql = sqlT.FalseSql();
            if (prettify)
            {
                foreach (
                    Tuple<int, IColumn> t
                    in Columns.Values
                    )
                {
                    IColumn column = t.Item2;
                    int nameOffset = column.Name.Length + 6; // remember we will prefix some with change
                    if (nameOffset > minDatatypeOffset) minDatatypeOffset = nameOffset;
                    int dataTypeOffset = column.DataType.Length + 1;
                    if (dataTypeOffset > minNullabilityOffset) minNullabilityOffset = dataTypeOffset;
                }
            }
            else
            {
                minDatatypeOffset = 0;
                minNullabilityOffset = 0;
            }
            IIdentityColumn identityColumn = IdentityColumn;
            if (identityColumn == null)
                throw new NoIdentityColumnException(
                    this
                    , Name
                    , GetType().FullName
                    );
            retval = ""
                + lfAndLeadingSpaces + "       "
                + sqlT.TempTableCreateSqlSnip(
                    tempTableName
                    , globallyAvailable
                    , createInMemoryTable
                    )
                + lfAndLeadingSpaces + "       ( "
                + lfAndLeadingSpaces + "       " + identityColumn.Name + " int not null primary key ";
            string changeColumns = "";
            foreach (
                Tuple<int, ITableColumn> t
                in TableColumns.Values.Where(
                    t => !t.Item2.IsIdentity && !t.Item2.IsLastUpdateDtm && !t.Item2.IsLastUpdater
                    )
                .OrderBy(t => t.Item1))
            {
                ITableColumn col = t.Item2;
                string colDefWithoutPrefix = col.NameWhenPrefixed;
                if (prettify)
                    colDefWithoutPrefix
                        += new string(' ', minDatatypeOffset - col.Name.Length - 3)
                        + col.DataType + new string(' ', minNullabilityOffset - col.DataType.Length)
                        + "     null";
                else
                    colDefWithoutPrefix
                        += " " + col.DataType + "      null";
                retval += lfAndLeadingSpaces + ", old" + colDefWithoutPrefix;
                retval += lfAndLeadingSpaces + ", new" + colDefWithoutPrefix;
                changeColumns
                    += lfAndLeadingSpaces + ", change" + col.NameWhenPrefixed
                    + (prettify ? new string(' ', minDatatypeOffset - col.Name.Length - 5) : "")
                    + " as cast( case when old" + col.NameWhenPrefixed + " = new" + col.NameWhenPrefixed
                    + "or coalesce( old" + col.NameWhenPrefixed + ", new" + col.NameWhenPrefixed + " ) is null "
                    + "then " + falseSql + " else " + trueSql + " end as " + boolSql + " ) persisted";
            }
            retval += ""
                + changeColumns
                + lfAndLeadingSpaces + "       ) "
                + lfAndLeadingSpaces + ";";
            return retval;
        }

        /// <summary>
        /// Loads a temporary deleted table appropriate for
        /// the specified trigger timing and returns
        /// the name of the table.
        /// </summary>
        /// <param name="tempTableName">The name
        /// of the temporary deleted table.</param>
        /// <param name="conn">An open database connection to
        /// be used for loading the temp table.</param>
        public void LoadDeletedTriggerTable(
            string tempTableName
            , bool includeMutableColumns
            , IConnection conn
            )
        {
            Debug.Assert(
                conn.IsOpen
                , "Connection should already be open when "
                + "table is asked to load its temp deleted table."
                );
            ISqlTranslator sqlT = SqlTranslator;
            Debug.Assert(
                sqlT != null
                , "SqlTranslator should already be set before "
                + "table is asked to load its temp deleted table."
                );
            IDatabaseSafe db = Database;
            Debug.Assert(
                db != null
                , "Database should already be set before "
                + "table is asked to load its temp deleted table."
                );
            db.SqlExecuteNonQuery(
                DeletedTriggerTableLoadSql(tempTableName, includeMutableColumns)
                );
        }

        /// <summary>
        /// Builds the SQL string required to load
        /// a temporary inserted table appropriate for
        /// the specified trigger timing.
        /// </summary>
        /// <remarks>
        /// The default logic is an ANSI-standard
        /// insert statement and is quite simple.
        /// However, where there are tables that do
        /// things like establishing defaults by
        /// joining to other database tables, the
        /// method can be overriden to provide
        /// more complex logic as required.
        /// </remarks>
        /// <param name="tempTableName">The name of the
        /// temporary table to be loaded.</param>
        /// <param name="includeMutableColumns">Flag
        /// specifing whether variant columns are
        /// included in the table.</param>
        /// <param name="spacesToIndent">Specifies 
        /// how many leading spaces to put in front of
        /// each line.</param>
        /// <returns></returns>
        public string DeletedTriggerTableLoadSql(
            string tempTableName
            , bool includeMutableColumns
            , int spacesToIndent = 0
            )
        {
            return TriggerTableLoadSql(
                tempTableName
                , includeMutableColumns
                , spacesToIndent
                );
        }

        /// <summary>
        /// Loads a temporary merged trigger table.
        /// </summary>
        /// <param name="tempTableName">The name
        /// of the temporary deleted table.</param>
        /// <param name="conn">An open database connection to
        /// be used for loading the temp table.</param>
        public void LoadMergedTriggerTable(
            string tempMergedName
            , bool tempInsertedName
            , bool tempDeletedName
            , IConnection conn
            )
        {
            Debug.Assert(
                conn.IsOpen
                , "Connection should already be open when "
                + "table is asked to load its temp deleted table."
                );
            ISqlTranslator sqlT = SqlTranslator;
            Debug.Assert(
                sqlT != null
                , "SqlTranslator should already be set before "
                + "table is asked to load its temp deleted table."
                );
            IDatabaseSafe db = Database;
            Debug.Assert(
                db != null
                , "Database should already be set before "
                + "table is asked to load its temp deleted table."
                );
            db.SqlExecuteNonQuery(
                MergedTriggerTableLoadSql(
                    tempMergedName
                    , tempInsertedName
                    , tempDeletedName
                    )
                );
        }

        /// <summary>
        /// Builds the SQL string required to load
        /// a temporary inserted table appropriate for
        /// the specified trigger timing.
        /// </summary>
        /// <remarks>
        /// The default logic is an ANSI-standard
        /// insert statement and is quite simple.
        /// However, where there are tables that do
        /// things like establishing defaults by
        /// joining to other database tables, the
        /// method can be overriden to provide
        /// more complex logic as required.
        /// </remarks>
        /// <param name="tempTableName">The name of the
        /// temporary table to be loaded.</param>
        /// <param name="includeMutableColumns">Flag
        /// specifing whether variant columns are
        /// included in the table.</param>
        /// <param name="spacesToIndent">Specifies 
        /// how many leading spaces to put in front of
        /// each line.</param>
        /// <returns></returns>
        public string MergedTriggerTableLoadSql(
            string tempMergedName
            , bool tempInsertedName
            , bool tempDeletedName
            , int spacesToIndent = 0
            )
        {
            string retval;
            ISqlTranslator sqlT = SqlTranslator;
            Debug.Assert(
                sqlT != null
                , "SqlTranslator is null; table is not in a usable state "
                + "and therefore ITable.MergedTriggerTableLoadSql should "
                + "not have been called for table '"
                + Name != null ? Name : "[unnamed]"
                + "' of type "
                + GetType().FullName
                + "."
                );
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string identityColumnName = IdentityColumnName;
            if (identityColumnName == null)
                throw new NoIdentityColumnException(
                    this
                    , Name
                    , GetType().FullName
                    );
            retval = ""
                + lfAndLeadingSpaces + "insert into " + tempMergedName
                + lfAndLeadingSpaces + "       ( "
                + lfAndLeadingSpaces + "       " + identityColumnName;
            string selectClause = "select i." + identityColumnName;
            foreach (
                Tuple<int, ITableColumn> t
                in TableColumns.Values.Where(
                    t => !t.Item2.IsIdentity && !t.Item2.IsLastUpdateDtm && !t.Item2.IsLastUpdater
                    )
                .OrderBy(t => t.Item1))
            {
                string colName = t.Item2.Name;
                string colNameWhenPrefixed = t.Item2.NameWhenPrefixed;
                retval += lfAndLeadingSpaces + ", old" + colNameWhenPrefixed;
                retval += lfAndLeadingSpaces + ", new" + colNameWhenPrefixed;
                selectClause += lfAndLeadingSpaces + ", d." + colName + " as old" + colNameWhenPrefixed;
                selectClause += lfAndLeadingSpaces + ", i." + colName + " as new" + colNameWhenPrefixed;
            }
            retval += ""
                + lfAndLeadingSpaces + "       ) "
                + lfAndLeadingSpaces + selectClause
                + lfAndLeadingSpaces + "  from " + tempInsertedName + " i"
                + lfAndLeadingSpaces + "         join " + tempDeletedName + " d"
                + lfAndLeadingSpaces + "           on d." + identityColumnName + " = i." + identityColumnName
                + lfAndLeadingSpaces + ";"
                ;
            return retval;
        }

        /// <summary>
        /// Builds the SQL string required to load
        /// a temporary inserted table appropriate for
        /// the specified trigger timing, using
        /// a single merged temp table.
        /// </summary>
        /// <remarks>
        /// The default logic is an ANSI-standard
        /// insert statement and is quite simple.
        /// However, where there are tables that do
        /// things like establishing defaults by
        /// joining to other database tables, the
        /// method can be overriden to provide
        /// more complex logic as required.
        /// </remarks>
        /// <param name="tempTableName">The name of the
        /// temporary table to be loaded.</param>
        /// <param name="includeMutableColumns">Flag
        /// specifing whether variant columns are
        /// included in the table.</param>
        /// <param name="spacesToIndent">Specifies 
        /// how many leading spaces to put in front of
        /// each line.</param>
        /// <returns></returns>
        public void InterceptUpdateOfImmutableColumn(
            string tempMergedTableName
            , IConnection conn
            )
        {
            Debug.Assert(
                conn.IsOpen
                , "Connection should already be open when "
                + "InterceptUpdateOfImmutableColumn method is called."
                );
            IDatabaseSafe db = Database;
            Debug.Assert(
                db != null
                , "Database should already be set before "
                + "InterceptUpdateOfImmutableColumn method is called."
                );
            ISqlTranslator sqlT = SqlTranslator;
            Debug.Assert(
                sqlT != null
                , "SqlTranslator should already be set before "
                + "InterceptUpdateOfImmutableColumn method is called."
                );
            string sql;
            string trueSql = sqlT.TrueSql();
            bool vendorIsSqlServer = sqlT.Vendor.Equals(Vendors.SQLServer());
            List<Tuple<string, bool>> testableColumnNames = new List<Tuple<string, bool>>();
            foreach (
                Tuple<int, IColumn> t
                in ImmutableColumns.Values
                )
            {
                if (vendorIsSqlServer
                    ? (short)db.SqlExecuteSingleValue(
                        "if update( " + t.Item2.Name + " ) select 1 else select 0;"
                        , conn
                        ) == 1
                    : true
                    )
                    testableColumnNames.Add(
                        new Tuple<string, bool>(
                            t.Item2.Name
                            , t.Item2.IsNullable
                            )
                        );
            }
            int count = testableColumnNames.Count;
            if (testableColumnNames.Count > 0)
            {
                string identityColumnName = IdentityColumnName;

                sql = ""
                    + "select top(1) i." + identityColumnName + " \n"
                    + "     , 'Changed column(s): '";
                string whereClauses = "";
                foreach (Tuple<string, bool> t in testableColumnNames)
                {
                    string s = t.Item1;
                    bool b = t.Item2;
                    string matches = "change" + s + " = " + trueSql;
                    sql += ""
                        + "         + case \n"
                        + "             when " + matches + " \n"
                        + "               then '' \n"
                        + "             else '" + s + ", from ' + cast( old" + s + " as nvarchar ) \n"
                        + "                    + ' to ' + cast new" + s + " as nvarchar ) \n"
                        + "                    + ', ' \n"
                        + "             end \n";
                    whereClauses +=
                        whereClauses.Length > 0
                        ? "    or " + matches + " \n"
                        : " where " + matches + " \n";
                }
                sql += ""
                    + "  from " + tempMergedTableName + " i \n"
                    + whereClauses
                    + ";";
                List<string[]> output
                    = db.SqlExecuteReader(sql, conn);
                if (output.Count > 0)
                    throw new AttemptedUpdateOfNonupdatableColumnException(
                        this
                        , Name
                        , GetType().FullName
                        , Int32.Parse(output[1][0])
                        , output[1][1]
                        );
            }
        }

        /// <summary>
        /// Builds the SQL string required to load
        /// a temporary inserted table appropriate for
        /// the specified trigger timing, using
        /// separate inserted and deleted temp tables.
        /// </summary>
        /// <remarks>
        /// The default logic is an ANSI-standard
        /// insert statement and is quite simple.
        /// However, where there are tables that do
        /// things like establishing defaults by
        /// joining to other database tables, the
        /// method can be overriden to provide
        /// more complex logic as required.
        /// </remarks>
        /// <param name="tempTableName">The name of the
        /// temporary table to be loaded.</param>
        /// <param name="includeMutableColumns">Flag
        /// specifing whether variant columns are
        /// included in the table.</param>
        /// <param name="spacesToIndent">Specifies 
        /// how many leading spaces to put in front of
        /// each line.</param>
        /// <returns></returns>
        public void InterceptUpdateOfImmutableColumn(
            string tempInsertedName
            , string tempDeletedName
            , IConnection conn
            )
        {
            Debug.Assert(
                conn.IsOpen
                , "Connection should already be open when "
                + "InterceptUpdateOfImmutableColumn method is called."
                );
            IDatabaseSafe db = Database;
            Debug.Assert(
                db != null
                , "Database should already be set before "
                + "InterceptUpdateOfImmutableColumn method is called."
                );
            ISqlTranslator sqlT = SqlTranslator;
            Debug.Assert(
                sqlT != null
                , "SqlTranslator should already be set before "
                + "InterceptUpdateOfImmutableColumn method is called."
                );
            string sql;
            bool vendorIsSqlServer = sqlT.Vendor.Equals(Vendors.SQLServer());
            List<Tuple<string, bool>> testableColumnNames = new List<Tuple<string, bool>>();
            foreach (
                Tuple<int, IColumn> t
                in ImmutableColumns.Values
                )
            {
                if (vendorIsSqlServer
                    ? (short)db.SqlExecuteSingleValue(
                        "if update( " + t.Item2.Name + " ) select 1 else select 0;"
                        , conn
                        ) == 1
                    : true
                    )
                    testableColumnNames.Add(
                        new Tuple<string, bool>(
                            t.Item2.Name
                            , t.Item2.IsNullable
                            )
                        );
            }
            int count = testableColumnNames.Count;
            if (testableColumnNames.Count > 0)
            {
                int currentSpot;
                string identityColumnName = IdentityColumnName;
                sql = ""
                    + "create index " + tempInsertedName + "_i_BadUpdate \n"
                    + "on " + tempInsertedName + "( ";
                currentSpot = 1;
                foreach (Tuple<string, bool> t in testableColumnNames)
                {
                    sql += t.Item1 + (currentSpot < count ? ", " : "");
                    currentSpot++;
                }
                sql += ");";
                db.SqlExecuteNonQuery(sql, conn);

                sql = ""
                    + "create index " + tempDeletedName + "_i_BadUpdate \n"
                    + "on " + tempDeletedName + "( ";
                currentSpot = 1;
                foreach (Tuple<string, bool> t in testableColumnNames)
                {
                    sql += t.Item1 + (currentSpot < count ? ", " : "");
                    currentSpot++;
                }
                sql += ");";
                db.SqlExecuteNonQuery(sql, conn);

                sql = ""
                    + "select top(1) i." + identityColumnName + " \n"
                    + "     , 'Changed column(s): '";
                string whereClauses = " where i." + identityColumnName + " = d." + identityColumnName + " \n";
                foreach (Tuple<string, bool> t in testableColumnNames)
                {
                    string s = t.Item1;
                    bool b = t.Item2;
                    string matches
                        = "i." + s + " = d." + s +
                        (b ? " or coalesce( i." + s + ", d." + s + " ) is null " : "");
                    sql += ""
                        + "         + case \n"
                        + "             when " + matches + " \n"
                        + "               then '' \n"
                        + "             else '" + s + ", from ' + cast( i." + s + " as nvarchar ) \n"
                        + "                    + ' to ' + cast d." + s + " as nvarchar ) \n"
                        + "                    + ', ' \n"
                        + "             end \n";
                    whereClauses += ""
                        + "          and ( " + matches + " ) \n";
                }
                sql += ""
                    + "  from " + tempInsertedName + " i \n"
                    + "         join " + tempDeletedName + " d \n"
                    + "           on " + whereClauses
                    + ";";
                List<string[]> output
                    = db.SqlExecuteReader(sql, conn);
                if (output.Count > 0)
                    throw new AttemptedUpdateOfNonupdatableColumnException(
                        this
                        , Name
                        , GetType().FullName
                        , Int32.Parse(output[1][0])
                        , output[1][1]
                        );
            }
        }

        /// <summary>
        /// Builds the SQL string required to insert into
        /// a table using the data from an inserted
        /// table or its double-hash replacement.
        /// </summary>
        /// <remarks>
        /// The default implementation is adequate for most tables,
        /// but, where there are tables that do
        /// things like establishing defaults by
        /// joining to other database tables, the
        /// method can be overriden to provide
        /// more complex logic as required.
        /// </remarks>
        /// <param name="tempTableName">The name of the
        /// temporary table from which the updated data are taken.</param>
        /// <param name="currentTimestampSql">SQL snippet with
        /// time at which update was initiated by user.</param>
        /// <param name="userSql">SQL snippet with
        /// user who initiated update.</param>
        /// <param name="spacesToIndent">Number of spaces
        /// to indent (when prettifying script, e.g. for
        /// logging/debugging purposes).</param>
        /// <returns></returns>
        public string UpdateFromInsertedTriggerTableSql(
            string tempTableName
            , string currentTimestampSql
            , string userSql
            , int spacesToIndent = 0
            )
        {
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string identityColumnName = IdentityColumnName;
            string sql = ""
                + lfAndLeadingSpaces + "merge into " + Name + " as target"
                + lfAndLeadingSpaces + "using      " + tempTableName + " as i"
                + lfAndLeadingSpaces + "             on i." + identityColumnName + " = target." + identityColumnName
                + lfAndLeadingSpaces + "when matched then "
                + lfAndLeadingSpaces + "update "
                + lfAndLeadingSpaces + "   set ";
            bool afterFirstColumn = false;
            foreach (
                Tuple<int, ITableColumn> t
                in TableColumns.Values.OrderBy(t => t.Item1)
                )
            {
                if (afterFirstColumn)
                    sql += ""
                        + lfAndLeadingSpaces + "    , target." + t.Item2.Name + " = i." + t.Item2.Name;
                else
                {
                    afterFirstColumn = true;
                    sql += "target." + t.Item2.Name + " = i." + t.Item2.Name;
                }
            }
            if (HasLastUpdateDtmColumn)
                sql += ""
                    + lfAndLeadingSpaces + "    , target." + LastUpdateDtmColumnName + " = " + currentTimestampSql;
            if (HasLastUpdaterColumn)
                sql += ""
                    + lfAndLeadingSpaces + "    , target." + LastUpdaterColumnName + " = " + userSql;
            ;
            return sql;
        }

        /// <summary>
        /// Updates any children constrained to cascade update
        /// from the table under update, where necessary,
        /// using a single merged temp trigger table.
        /// </summary>
        /// <remarks>
        /// The default implementation is adequate for most tables,
        /// but, where there are tables that do
        /// things like establishing defaults by
        /// joining to other database tables, the
        /// method can be overriden to provide
        /// more complex logic as required.
        /// </remarks>
        /// <param name="relationship">The 
        /// cascading foreign key relationship that
        /// necessitates updating.</param>
        /// <param name="tempMergedName">The name of the
        /// merged temporary trigger table.</param>
        /// <param name="conn">A previously opened
        /// connection to be used for the updates.</param>
        /// <returns></returns>
        public void CascadeUpdate(
            ICascadingForeignKeyRelationship relationship
            , string tempMergedName
            , IConnection conn
            )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Updates any children constrained to cascade update
        /// from the table under update, where necessary,
        /// using separate inserted and deleted temp tables.
        /// </summary>
        /// <remarks>
        /// The default implementation is adequate for most tables,
        /// but, where there are tables that do
        /// things like establishing defaults by
        /// joining to other database tables, the
        /// method can be overriden to provide
        /// more complex logic as required.
        /// </remarks>
        /// <param name="relationship">The 
        /// cascading foreign key relationship that
        /// necessitates updating.</param>
        /// <param name="tempInsertedName">The name of the
        /// temporary inserted table.</param>
        /// <param name="tempDeletedName">The name of the
        /// temporary deleted table.</param>
        /// <param name="conn">A previously opened
        /// connection to be used for the updates.</param>
        /// <returns></returns>
        public void CascadeUpdate(
            ICascadingForeignKeyRelationship relationship
            , string tempInsertedName
            , string tempDeletedName
            , IConnection conn
            )
        {
            throw new NotImplementedException();
        }

        public virtual void LoadChangeTableAfterUpdate(
            IChangeTrackingTable changeTable
            , string updateDtmSql
            , string updaterSql
            , string tempMergedTableName
            , IConnection conn
            )
        {
            if (changeTable == null)
                throw new NullArgumentException(
                    "changeTable"
                    , FullTypeNameForCodeErrors
                    + ".LoadChangeTableAfterUpdate(changeTable, updateDtmSql, updaterSql, tempMergedTableName, conn)"
                    );
            LoadChangeTable(
                changeTable
                , "update"
                , updateDtmSql
                , updaterSql
                , tempMergedTableName
                , null
                , conn
                );
        }

        public virtual void LoadChangeTableAfterUpdate(
            IChangeTrackingTable changeTable
            , string updateDtmSql
            , string updaterSql
            , string tempInsertedName
            , string tempDeletedName
            , IConnection conn
            )
        {
            if (changeTable == null)
                throw new NullArgumentException(
                    "changeTable"
                    , FullTypeNameForCodeErrors
                    + ".LoadChangeTableAfterUpdate(changeTable, updateDtmSql, updaterSql, tempInsertedName, tempDeletedName, conn)"
                    );
            LoadChangeTable(
                changeTable
                , "update"
                , updateDtmSql
                , updaterSql
                , tempInsertedName
                , tempDeletedName
                , conn
                );
        }

        /// <summary>
        /// In collaboration with SqlTranslator, builds
        /// the SQL string required to delete from
        /// a table using the data from a deleted
        /// trigger table or its double-hash replacement.
        /// </summary>
        /// <remarks>
        /// The default implementation is adequate 
        /// for tables that use identity columns
        /// or single integer ID columns.
        /// For other tables overrides would be required.
        /// </remarks>
        /// <param name="tempTableName">The name of the
        /// temporary table to be loaded.</param>
        /// <param name="timing">Specifies when the trigger fires.</param>
        /// <returns></returns>
        public string DeleteFromDeletedTriggerTableSql(
            string tempTableName
            , int spacesToIndent = 0
            )
        {
            ISqlTranslator sqlT = SqlTranslator;
            Debug.Assert(
                sqlT != null
                , "SqlTranslator should already be set before "
                + "table's DeleteFromDeletedTriggerTableSql is called."
                );
            string matchColumnName = IdentityColumnName;
            if (matchColumnName == null) matchColumnName = IDColumnName;
            Debug.Assert(
                false
                , "Tables with delete triggers must have an integer "
                + "ID or identity column so that the before-delete "
                + "trigger can tell which rows to delete."
                );
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string sql = ""
                + lfAndLeadingSpaces + "delete from " + Name
                + lfAndLeadingSpaces + " where " + matchColumnName + " in "
                + lfAndLeadingSpaces + "       ( "
                + lfAndLeadingSpaces + "       select " + matchColumnName
                + lfAndLeadingSpaces + "         from " + tempTableName + " doomed "
                + lfAndLeadingSpaces + "       ) "
                + lfAndLeadingSpaces + ";"
                ;
            return sql;
        }

        /// <summary>
        /// Updates any children constrained to cascade update
        /// from the table under update, where necessary,
        /// using a single merged temp trigger table.
        /// </summary>
        /// <remarks>
        /// The default implementation is adequate for most tables,
        /// but, where there are tables that do
        /// things like establishing defaults by
        /// joining to other database tables, the
        /// method can be overriden to provide
        /// more complex logic as required.
        /// </remarks>
        /// <param name="relationship">The 
        /// cascading foreign key relationship that
        /// necessitates updating.</param>
        /// <param name="tempMergedName">The name of the
        /// merged temporary trigger table.</param>
        /// <param name="conn">A previously opened
        /// connection to be used for the updates.</param>
        /// <returns></returns>
        public void CascadeDelete(
            ICascadingForeignKeyRelationship relationship
            , string tempDeletedName
            , IConnection conn
            )
        {
            throw new NotImplementedException();
        }

        public virtual void LoadChangeTableAfterDelete(
            IChangeTrackingTable changeTable
            , string updateDtmSql
            , string updaterSql
            , string tempDeletedName
            , IConnection conn
            )
        {
            if (changeTable == null)
                throw new NullArgumentException(
                    "changeTable"
                    , FullTypeNameForCodeErrors
                    + ".LoadChangeTableAfterDelete(changeTable, updateDtmSql, updaterSql, sourceTempTableName, conn)"
                    );
            LoadChangeTable(
                changeTable
                , "delete"
                , updateDtmSql
                , updaterSql
                , tempDeletedName
                , null
                , conn
                );
        }

        /***************************
         * ITableMutable properties and methods
        ***************************/

        public virtual void SetTableColumns(
            Dictionary<string, Tuple<int, ITableColumn>> value
            , bool? retrieveColumnIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            SetColumns(new Dictionary<string, Tuple<int, IColumn>>(), false, false);
            foreach (
                Tuple<int, ITableColumn> t
                in value.Values.OrderBy(t => t.Item1)
                )
                AddOrOverwriteColumn(t.Item2);
        }
        public void SetHasIdentityColumn(
            bool value
            , IIdentityColumn identityColumn = null
            , bool? retrieveIdentityColumnIDFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            string existingName = IdentityColumnName;
            if (value)
            {
                if (identityColumn == null)
                {
                    if (!HasIdentityColumn) // otherwise we leave in place the one that's already there
                        AddOrOverwriteColumn(
                            new IdentityColumnMutable(
                                "id"
                                , SqlTranslator
                                , this
                                , "ID"
                                , "ID"
                                , null
                                , false
                                , false
                                )
                            , 0
                            , retrieveIdentityColumnIDFromDatabase != null && retrieveIdentityColumnIDFromDatabase.HasValue
                            ? retrieveIdentityColumnIDFromDatabase.Value
                            : ID != null
                            , true
                            );
                }
                else
                    SetIdentityColumn(
                        (IIdentityColumn)identityColumn.GetReferenceForParentObject(this)
                        , retrieveIdentityColumnIDFromDatabase != null && retrieveIdentityColumnIDFromDatabase.HasValue
                        ? retrieveIdentityColumnIDFromDatabase.Value
                        : ID != null
                        , false
                        );
            }
            else if (existingName != null)
                RemoveColumn(existingName, false);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public void SetIdentityColumn(
            IIdentityColumn value
            , bool? retrieveIdentityColumnIDFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            string existingName = IdentityColumnName;
            if (value == null)
            {
                if (existingName != null)
                    RemoveColumn(existingName, false);
            }
            else if (existingName != null && !existingName.Equals(value.Name))
                RemoveColumn(existingName, false);
            AddOrOverwriteColumn(
                (IIdentityColumn)value.GetReferenceForParentObject(this)
                , 0
                , false
                );
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void SetPrimaryKey(
                IPrimaryKey value
                , bool? retrievePrimaryKeyIDFromDatabase = null
                , bool assert = true
                )
        {
            IPrimaryKey primaryKey = PrimaryKey;
            if (!EqualityUtilities.EqualOrBothNull(primaryKey, value))
            {
                if (value == null)
                    _uniqueKeys.Remove(primaryKey.Name);
                else
                    AddOrOverwriteUniqueKey(value, retrievePrimaryKeyIDFromDatabase, false);
            }
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void SetUniqueKeys(
            Dictionary<string, IUniqueKey> value
            , bool? retrieveUniqueKeyIDsFromDatabase = null
            , bool assert = true
            )
        {
            if (_uniqueKeys != value)
            {
                _uniqueKeys = new Dictionary<string, IUniqueKey>();
                foreach (KeyValuePair<string, IUniqueKey> pair in value.Where(p => p.Value != null))
                    AddOrOverwriteUniqueKey(pair.Value, retrieveUniqueKeyIDsFromDatabase, assert: false);
            }
            else
            {
                foreach (KeyValuePair<string, IUniqueKey> pair in _uniqueKeys.Where(p => p.Value.ParentObject == null))
                    AddOrOverwriteUniqueKey(pair.Value, assert: false);
            }
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void AddOrOverwriteUniqueKey(
            IUniqueKey key
            , bool? retrieveUniqueKeyIDFromDatabase = null
            , bool assert = true
            )
        {
            if (key != null)
            {
                string keyName = key.Name;
                Debug.Assert(
                    keyName != null
                    , "Cannot add unique keys with null names to vables."
                    );
                if (key.IsPrimaryKey && PrimaryKey != null && !key.Equals(PrimaryKey))
                    throw new MultiplePrimaryKeysException(
                        IsFullyConstructed ? this : null
                        , GetType().FullName
                        , PrimaryKey.Name
                        , keyName
                        );
                else
                    _uniqueKeys[keyName]
                        = !Equals(key.ParentObject)
                        ? (IUniqueKey)key.GetReferenceForParentObject(this, retrieveUniqueKeyIDFromDatabase, false)
                        : key;
            }
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual bool RemoveUniqueKey(string nameOfDoomed, bool assert = true)
        {
            bool retval = false;
            if (nameOfDoomed != null)
            {
                retval = _uniqueKeys.Remove(nameOfDoomed);
            }
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }
        public bool RemoveUniqueKey(IUniqueKey doomed, bool assert = true)
        {
            if (UtilitiesForINamed.NameOf(doomed) == null) return false;
            else
                return RemoveUniqueKey(doomed.Name, assert: true);
        }

        public virtual void SetForeignKeys(
            Dictionary<string, IForeignKey> value
            , ICascadingForeignKeyRegistry cascadingForeignKeyRegistry = null
            , bool? retrieveIDsFromDatabase = null
            , bool assert = true
            )
        {
            if (_foreignKeys != value)
            {
                _foreignKeys = new Dictionary<string, IForeignKey>();
                foreach (KeyValuePair<string, IForeignKey> pair in value.Where(p => p.Value != null))
                    AddOrOverwriteForeignKey(pair.Value, retrieveIDsFromDatabase, assert: false);
            }
            else
            {
                foreach (KeyValuePair<string, IForeignKey> pair in _foreignKeys.Where(p => p.Value.ParentObject == null))
                    AddOrOverwriteForeignKey(pair.Value, retrieveIDsFromDatabase, assert: false);
            }
            if (_foreignKeys.Count > 0 && (CascadingForeignKeyRegistry == null || cascadingForeignKeyRegistry != null))
            {
                SetCascadingForeignKeyRegistry(cascadingForeignKeyRegistry, false);
            }
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void SetCascadingForeignKeyRegistry(
            ICascadingForeignKeyRegistry value
            , bool assert = true
            )
        {
            if (value == null)
            {
                IDatabaseSafe db = Database;
                value
                    = db != null ? db.CascadingForeignKeyRegistry : null;
                if (
                    value == null
                    && ForeignKeys.Where(p => p.Value.CascadeOnDelete || p.Value.CascadeOnUpdate).Count() > 0
                    )
                    throw new NullArgumentException(
                        "value"
                        , FullTypeNameForCodeErrors
                        + ".SetCascadingForeignKeyRegistry(value, retrieveIDsFromDatabase, assert)"
                        );
            }
            _cascadingForeignKeyRegistry = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void AddOrOverwriteForeignKey(
            IForeignKey key
            , bool? retrieveForeignKeyIDFromDatabase = null
            , bool assert = true
            )
        {
            if (key != null)
            {
                Debug.Assert(
                    key.Name != null
                    , "Cannot add foreign keys with null names to vables."
                    );
                string keyName = key.Name;
                int countBeforeAddition = _foreignKeys.Count;
                _foreignKeys[keyName]
                    = !Equals(key.ParentObject)
                    ? (IForeignKey)key.GetReferenceForParentObject(this, retrieveForeignKeyIDFromDatabase, false)
                    : key;
                if (countBeforeAddition == 0 && CascadingForeignKeyRegistry == null)
                    SetCascadingForeignKeyRegistry(null, false);  // Retrieves default from schema/database
            }
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual bool RemoveForeignKey(string nameOfDoomed, bool assert = true)
        {
            bool retval = false;
            if (nameOfDoomed != null)
                retval = _foreignKeys.Remove(nameOfDoomed);
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }
        public bool RemoveForeignKey(IForeignKey doomed, bool assert = true)
        {
            if (UtilitiesForINamed.NameOf(doomed) == null) return false;
            else
                return RemoveForeignKey(doomed.Name, assert: true);
        }

        public virtual void SetTriggers(
            Dictionary<string, ITableTrigger> value
            , bool? retrieveTriggerIDsFromDatabase = null
            , bool assert = true
            )
        {
            bool retrieveIDs
                = (!retrieveTriggerIDsFromDatabase.HasValue || retrieveTriggerIDsFromDatabase.Value);
            if (_triggers != value)
            {
                _triggers = new Dictionary<string, ITableTrigger>();
                foreach (KeyValuePair<string, ITableTrigger> pair in value.Where(p => p.Value != null))
                    AddOrOverwriteTrigger(
                        (ITableTrigger)
                        pair.Value.GetReferenceForParentObject(this, retrieveIDs, false)
                        , false
                        , assert: false
                        );
            }
            else
                foreach (
                    KeyValuePair<string, ITableTrigger> pair
                    in _triggers.Where(p => !Equals(p.Value.ParentObject))
                    )
                    AddOrOverwriteTrigger(
                        (ITableTrigger)
                        pair.Value.GetReferenceForParentObject(this, retrieveIDs, false)
                        , false
                        , assert: false
                        );
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void AddOrOverwriteTrigger(
            ITableTrigger trigger
            , bool? retrieveTriggerIDFromDatabase = null
            , bool assert = true
            )
        {
            bool retrieveIDs
                = (!retrieveTriggerIDFromDatabase.HasValue || retrieveTriggerIDFromDatabase.Value);
            if (trigger != null)
            {
                Debug.Assert(
                    trigger.Name != null
                    , "Cannot attach a trigger will null Name property to an ITable."
                    );
                _triggers[trigger.Name]
                    = (ITableTrigger)
                    trigger.GetReferenceForParentObject(this, retrieveIDs, false);
            }
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual bool RemoveTrigger(string nameOfDoomed, bool assert = true)
        {
            bool retval = _triggers.Remove(nameOfDoomed);
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }

        public virtual bool RemoveTrigger(ITableTrigger doomed, bool assert = true)
        {
            bool retval = RemoveTrigger(UtilitiesForINamed.NameOf(doomed), assert);
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }

        public void SetCheckConstraints(
            Dictionary<string, ICheckConstraint> value
            , bool? retrieveConstraintIDsFromDatabase = null
            , bool assert = true
            )
        {
            bool retrieveIDs
                = (!retrieveConstraintIDsFromDatabase.HasValue || retrieveConstraintIDsFromDatabase.Value);
            if (_checkConstraints != value)
            {
                _checkConstraints = new Dictionary<string, ICheckConstraint>();
                foreach (KeyValuePair<string, ICheckConstraint> pair in value.Where(p => p.Value != null))
                    AddOrOverwriteCheckConstraint(
                        (ICheckConstraint)
                        pair.Value.GetReferenceForParentObject(this, retrieveIDs, false)
                        , false
                        , assert: false
                        );
            }
            else
                foreach (
                    KeyValuePair<string, ICheckConstraint> pair
                    in _checkConstraints.Where(p => !Equals(p.Value.ParentObject))
                    )
                    AddOrOverwriteCheckConstraint(
                        (ICheckConstraint)
                        pair.Value.GetReferenceForParentObject(this, retrieveIDs, false)
                        , false
                        , assert: false
                        );
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public void AddOrOverwriteCheckConstraint(
            ICheckConstraint constraint
            , bool? retrieveConstraintIDFromDatabase = null
            , bool assert = true
            )
        {
            bool retrieveIDs
                = (!retrieveConstraintIDFromDatabase.HasValue || retrieveConstraintIDFromDatabase.Value);
            if (constraint != null)
            {
                Debug.Assert(
                    constraint.Name != null
                    , "Cannot attach a trigger will null Name property to an ITable."
                    );
                _checkConstraints[constraint.Name]
                    = (ICheckConstraint)
                    constraint.GetReferenceForParentObject(this, retrieveIDs, false);
            }
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public bool RemoveCheckConstraint(string nameOfDoomed, bool assert = true)
        {
            bool retval = _checkConstraints.Remove(nameOfDoomed);
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }

        public bool RemoveCheckConstraint(ICheckConstraint doomed, bool assert = true)
        {
            bool retval = RemoveCheckConstraint(UtilitiesForINamed.NameOf(doomed), assert);
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }

        public void SetDefaultConstraints(
            Dictionary<string, IDefaultConstraint> value
            , bool? retrieveConstraintIDsFromDatabase = null
            , bool assert = true
            )
        {
            bool retrieveIDs
                = (!retrieveConstraintIDsFromDatabase.HasValue || retrieveConstraintIDsFromDatabase.Value);
            if (_defaultConstraints != value)
            {
                _defaultConstraints = new Dictionary<string, IDefaultConstraint>();
                foreach (KeyValuePair<string, IDefaultConstraint> pair in value.Where(p => p.Value != null))
                    AddOrOverwriteDefaultConstraint(
                        (IDefaultConstraint)
                        pair.Value.GetReferenceForParentObject(this, retrieveIDs, false)
                        , false
                        , assert: false
                        );
            }
            else
                foreach (
                    KeyValuePair<string, IDefaultConstraint> pair
                    in _defaultConstraints.Where(p => !Equals(p.Value.ParentObject))
                    )
                    AddOrOverwriteDefaultConstraint(
                        (IDefaultConstraint)
                        pair.Value.GetReferenceForParentObject(this, retrieveIDs, false)
                        , false
                        , assert: false
                        );
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public void AddOrOverwriteDefaultConstraint(
            IDefaultConstraint constraint
            , bool? retrieveConstraintIDFromDatabase = null
            , bool assert = true
            )
        {
            bool retrieveIDs
                = (!retrieveConstraintIDFromDatabase.HasValue || retrieveConstraintIDFromDatabase.Value);
            if (constraint != null)
            {
                Debug.Assert(
                    constraint.Name != null
                    , "Cannot attach a trigger will null Name property to an ITable."
                    );
                _defaultConstraints[constraint.Name]
                    = (IDefaultConstraint)
                    constraint.GetReferenceForParentObject(this, retrieveIDs, false);
            }
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public bool RemoveDefaultConstraint(string nameOfDoomed, bool assert = true)
        {
            bool retval = _defaultConstraints.Remove(nameOfDoomed);
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }

        public bool RemoveDefaultConstraint(IDefaultConstraint doomed, bool assert = true)
        {
            bool retval = RemoveDefaultConstraint(UtilitiesForINamed.NameOf(doomed), assert);
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }

        public void SetIsAudited(
            bool value
            , IAuditTable auditTable = null
            , bool? retrieveAuditTableIDFromDatabase = null
            , bool assert = true
            )
        {
            bool retrieveIDs
                = retrieveAuditTableIDFromDatabase != null & retrieveAuditTableIDFromDatabase.HasValue
                ? retrieveAuditTableIDFromDatabase.Value
                : ID != null;
            if (!value)
                _auditTable = null;
            else if (auditTable != null)
                _auditTable = (IAuditTable)auditTable.GetReferenceForOwner(this);
            else
                _auditTable
                    = new AuditTableMutable(
                        Name + "Audit"
                        , SqlTranslator
                        , Schema
                        , this
                        , identityColumnName: null
                        , idColumnConstraintTag: null
                        , retrieveIDsFromDatabase: retrieveIDs
                        , assert: false
                        );
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public void SetPublishesChanges(
            bool value
            , IChangeTrackingTable publishedChangesTable = null
            , IChangeSubscriberRegistry changeSubscriberRegistry = null
            , bool? retrieveChangeTrackingTableIDFromDatabase = null
            , bool assert = true
            )
        {
            bool retrieveIDs
                = retrieveChangeTrackingTableIDFromDatabase != null & retrieveChangeTrackingTableIDFromDatabase.HasValue
                ? retrieveChangeTrackingTableIDFromDatabase.Value
                : ID != null;
            if (!value)
            {
                _publishedChangesTable = null;
                _changeSubscriberRegistry = null;
            }
            else
            {
                if (publishedChangesTable != null)
                    _publishedChangesTable = (IChangeTrackingTable)publishedChangesTable.GetReferenceForOwner(this);
                else
                    _publishedChangesTable
                        = new ChangeTrackingTableMutable(
                            Name + "CJobs"
                            , SqlTranslator
                            , Schema
                            , this
                            , idColumnName: null
                            , idColumnConstraintTag: null
                            , allowsUpdates: true
                            , allowsDeletes: false
                            , hasChangeResponderColumn: true
                            , retrieveIDsFromDatabase: retrieveIDs
                            , assert: false
                            );
                if (changeSubscriberRegistry != null || _changeSubscriberRegistry == null)
                    SetChangeSubscriberRegistry(changeSubscriberRegistry, false);
            }
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public void SetChangeSubscriberRegistry(
            IChangeSubscriberRegistry value
            , bool assert = true
            )
        {
            if (value == null)
            {
                if (!PublishesChanges)
                    _changeSubscriberRegistry = null;
                else
                {
                    _changeSubscriberRegistry = Database.ChangeSubscriberRegistry;
                    if (_changeSubscriberRegistry == null)
                        throw new NullArgumentException(
                            "value"
                            , FullTypeNameForCodeErrors
                            + ".SetChangeSubscriberRegistry(value, retrieveRegistryIDFromDatabase, assert) "
                            );
                }
            }
            else
                _changeSubscriberRegistry = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public void SetParentTable(
            ITable value
            , Dictionary<string, string> parentInsertColumnListExpressions = null
            , bool? retrieveParentTableIDFromDatabase = null
            , bool assert = true
            )
        {
            bool retrieveIDs = false;
            if (value == null)
            {
                _parentTable = null;
                _parentInsertColumnListExpressions = new Dictionary<string, string>();
                // _parentPassThrough = null;
            }
            else
            {
                retrieveIDs
                    = retrieveParentTableIDFromDatabase != null && retrieveParentTableIDFromDatabase.HasValue
                    ? retrieveParentTableIDFromDatabase.Value
                    : ID != null && _parentTable.ID == null;
                _parentTable = value;
                SetParentInsertColumnListExpressions(parentInsertColumnListExpressions, false);
                if (retrieveIDs)
                {
                    if (typeof(ITableMutable).IsAssignableFrom(_parentTable.GetType()))
                        ((ITableMutable)_parentTable).RetrieveIDsFromDatabase(false);
                    else
                        _parentTable
                            = (ITable)_parentTable.GetReferenceForParentObject(
                                _parentTable.ParentObject
                                , true
                                , false
                                );
                }
            }
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public void SetParentInsertColumnListExpressions(
            Dictionary<string, string> value
            , bool assert = true
            )
        {
            ITable parentTable = ParentTable;
            if (value == null)
            {
                _parentInsertColumnListExpressions = new Dictionary<string, string>();
                if (parentTable != null)
                {
                    Debug.Assert(
                        parentTable.HomeVableNameColumn != null
                        , "A parent table must have a home vable name column."
                        );
                    _parentInsertColumnListExpressions[parentTable.HomeVableNameColumn.Name]
                        = "'" + NameWithSchema + "'";
                }
            }
            else
            {
                if (parentTable == null)
                    throw new NoParentTableException(
                        IsFullyConstructed ? this : null
                        , Name
                        , GetType().FullName
                        );
                List<string> parentColumnNames = parentTable.Columns.Keys.ToList();
                foreach (string key in value.Keys)
                {
                    if (!parentColumnNames.Contains(key))
                        throw new NotAParentTableColumnException(
                            IsFullyConstructed ? this : null
                            , Name
                            , GetType().FullName
                            , key
                            , parentTable
                            , parentTable.Name
                            , parentTable.GetType().FullName
                            );
                }
                string parentHomeVableColumnName
                    = parentTable.HomeVableNameColumn.Name;
                if (!value.ContainsKey(parentHomeVableColumnName))
                    throw new MissingHomeVableColumnException(
                        IsFullyConstructed ? this : null
                        , Name
                        , GetType().FullName
                        , parentHomeVableColumnName
                        , parentTable
                        , parentTable.Name
                        , parentTable.GetType().FullName
                        );
                _parentInsertColumnListExpressions = value;
            }
        }

        //public void SetParentPassThrough(IParentPassThroughInfo value, bool assert = true)
        //{
        //    _parentPassThrough = value;
        //    if (value == null) SetParentTable(null, assert);
        //    if (assert) Debug.Assert(PassesSelfAssertion());
        //}

        public void SetCanHaveChildren(
            bool value
            , ILeavesTable leavesTable = null
            , bool? retrieveLeavesTableIDFromDatabase = null
            , bool assert = true
            )
        {
            bool retrieveIDs
                = retrieveLeavesTableIDFromDatabase != null & retrieveLeavesTableIDFromDatabase.HasValue
                ? retrieveLeavesTableIDFromDatabase.Value
                : ID != null;
            if (!value) _leavesTable = null;
            else
            {
                if (leavesTable != null)
                    _leavesTable = (ILeavesTable)leavesTable.GetReferenceForOwner(this);
                else if (_leavesTable == null)
                    _leavesTable
                        = new LeavesTableMutable(
                            Name + "LeafVables"
                            , SqlTranslator
                            , Schema
                            , this
                            , allowsUpdates: true
                            , allowsDeletes: false
                            , maintainChildVablesTable: MaintainChildVablesTable
                            , childVablesTableName: MaintainChildVablesTable ? ChildVablesTable.Name : null
                            , retrieveIDsFromDatabase: retrieveIDs
                            , assert: false
                            );
                if (HomeVableNameColumn == null)
                    AddOrOverwriteColumn(
                        new HomeVableNameColumnMutable(
                            null
                            , SqlTranslator
                            , this
                            , _leavesTable
                            , retrieveIDsFromDatabase: retrieveIDs
                            , assert: false
                            )
                        );
            }
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void SetMaintainChildVablesTable(
            bool value
            , IChildVablesTable childVablesTable = null
            , bool? retrieveChildVablesTableIDFromDatabase = null
            , bool assert = true
            )
        {
            bool retrieveIDs
                = retrieveChildVablesTableIDFromDatabase != null & retrieveChildVablesTableIDFromDatabase.HasValue
                ? retrieveChildVablesTableIDFromDatabase.Value
                : ID != null;
            if (!value) _childVablesTable = null;
            else if (childVablesTable != null)
                _childVablesTable = (IChildVablesTable)childVablesTable.GetReferenceForOwner(this);
            else if (_childVablesTable == null)
                _childVablesTable
                    = new ChildVablesTableMutable(
                        Name + "ChildVables"
                        , SqlTranslator
                        , Schema
                        , this
                        , allowsUpdates: true
                        , allowsDeletes: false
                        , retrieveIDsFromDatabase: retrieveIDs
                        , assert: false
                        );
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public void SetHasIDColumn(
            bool value
            , ITableIDColumn idColumn = null
            , bool? retrieveIDColumnIDFromDatabase = null
            , bool assert = true
            )
        {
            ITableIDColumn currentIDColumn = IDColumn;
            bool retrieveIDs
                = retrieveIDColumnIDFromDatabase != null && retrieveIDColumnIDFromDatabase.HasValue
                ? retrieveIDColumnIDFromDatabase.Value
                : ID != null && ID.HasValue;

            if (!value)
                RemoveColumn(currentIDColumn, false);
            else if (idColumn != null)
            {
                if (idColumn.Name.Equals(currentIDColumn.Name))
                    RemoveColumn(currentIDColumn, false);
                AddOrOverwriteColumn(
                    (ITableColumn)idColumn.GetReferenceForParentObject(this)
                    );
            }
            else if (currentIDColumn == null)
                AddOrOverwriteColumn(
                    new TableIDColumnImmutable(
                        "id"
                        , SqlTranslator
                        , this
                        , nameWhenPrefixed: "ID"
                        , constraintTag: "ID"
                        , retrieveIDsFromDatabase: retrieveIDs
                        , assert: false
                        )
                    , null
                    , false
                    , false
                    );
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public void SetHasInputterColumn(
            bool value
            , IInputterColumn inputterColumn = null
            , IValidInputtersTable validInputtersTable = null
            , bool? retrieveIDsFromDatabase = null
            , bool assert = true
            )
        {

            IInputterColumn currentColumn = InputterColumn;
            if (!value)
            {
                if (currentColumn != null) RemoveColumn(currentColumn);
                _validInputtersTable = null;
            }
            else
            {
                if (inputterColumn != null)
                {
                    if (!inputterColumn.Name.Equals(currentColumn.Name))
                        RemoveColumn(currentColumn);
                    AddOrOverwriteColumn(
                        (ITableColumn)inputterColumn.GetReferenceForParentObject(this)
                        );
                }
                else if (currentColumn == null)
                    AddOrOverwriteColumn(
                        new InputterColumnImmutable(
                            null
                            , SqlTranslator
                            , this
                            , retrieveIDsFromDatabase:
                            retrieveIDsFromDatabase != null && retrieveIDsFromDatabase.HasValue
                            ? retrieveIDsFromDatabase.Value
                            : ID != null
                            , assert: false
                            )
                        , null
                        , false
                        , false
                        );
                if (validInputtersTable != null)
                    _validInputtersTable = validInputtersTable;
            }
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public void SetHasLastUpdateDtmColumn(
            bool value
            , bool? retrieveLastUpdateDtmColumnIDFromDatabase = null
            , bool assert = true
            )
        {
            ILastUpdateDtmColumn currentColumn = LastUpdateDtmColumn;
            bool retrieveIDs
                = retrieveLastUpdateDtmColumnIDFromDatabase != null && retrieveLastUpdateDtmColumnIDFromDatabase.HasValue
                ? retrieveLastUpdateDtmColumnIDFromDatabase.Value
                : ID != null && ID.HasValue;

            if (!value)
                RemoveColumn(currentColumn);
            else
                AddOrOverwriteColumn(
                    new LastUpdateDtmColumnImmutable(
                        SqlTranslator
                        , this
                        , retrieveIDsFromDatabase: retrieveIDs
                        , assert: false
                        )
                    , null
                    , false
                    , false
                    );
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public void SetHasLastUpdaterColumn(
            bool value
            , bool? retrieveLastUpdaterColumnIDFromDatabase = null
            , bool assert = true
            )
        {
            ILastUpdaterColumn currentColumn = LastUpdaterColumn;
            bool retrieveIDs
                = retrieveLastUpdaterColumnIDFromDatabase != null && retrieveLastUpdaterColumnIDFromDatabase.HasValue
                ? retrieveLastUpdaterColumnIDFromDatabase.Value
                : ID != null && ID.HasValue;

            if (!value)
                RemoveColumn(currentColumn);
            else
                AddOrOverwriteColumn(
                    new LastUpdaterColumnImmutable(
                        SqlTranslator
                        , this
                        , retrieveIDsFromDatabase: retrieveIDs
                        , assert: false
                        )
                    , null
                    , false
                    , false
                    );
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public void RefreshColumnsFromDb(bool assert = true)
        {

            Dictionary<string, Tuple<int, IColumn>> columns = new Dictionary<string, Tuple<int, IColumn>>();
            Dictionary<string, int> fields = Database.SqlTranslator.TableColumnsOutputStructure();
            List<string> sql = Database.SqlTranslator.TableColumnsSql(this);
            var columnData = Database.SqlExecuteReader(sql);

            string trueSql = Database.SqlTranslator.TrueSql();
            string falseSql = Database.SqlTranslator.FalseSql();

            string idColumnName = IDColumn == null ? null : IDColumn.Name;
            string inputterColumnName = InputterColumn == null ? null : InputterColumn.Name;

            if (idColumnName != null)
            {
                foreach (string[] col in columnData.Where(c => c[fields["column_name"]] == idColumnName))
                    columns[col[fields["column_name"]]]
                        = new Tuple<int, IColumn>(
                            Int32.Parse(col[fields["column_id"]])
                            , new TableIDColumnMutable(
                                col[fields["column_name"]]
                                , SqlTranslator
                                , this
                                , id: Int32.Parse(col[fields["column_id"]])
                                , retrieveIDsFromDatabase: false
                                , assert: false
                                )
                            );
            }
            if (inputterColumnName != null)
            {
                foreach (string[] col in columnData.Where(c => c[fields["column_name"]] == inputterColumnName))
                    columns[col[fields["column_name"]]]
                        = new Tuple<int, IColumn>(
                            Int32.Parse(col[fields["column_id"]])
                            , new InputterColumnMutable(
                                col[fields["column_name"]]
                                , SqlTranslator
                                , this
                                , id: Int32.Parse(col[fields["column_id"]])
                                , retrieveIDsFromDatabase: false
                                , assert: false
                                )
                            );
            }

            foreach (var col in columnData.Where(c => !columns.ContainsKey(c[fields["column_name"]])))
            {
                ITableColumn column = null;
                if (col[fields["is_last_updater"]] == trueSql)
                    column
                        = new LastUpdaterColumnImmutable(
                            SqlTranslator
                            , this
                            , Int32.Parse(col[fields["column_id"]])
                            , retrieveIDsFromDatabase: false
                            , assert: false
                            );
                else
                {
                    bool isLastUpdater
                        = (col[fields["is_last_update_dtm"]] == trueSql ? true : false);
                    bool isNullable
                        = (col[fields["is_nullable"]] == trueSql ? true : false);
                    string dataType = col[fields["data_type"]];
                    bool isIdentity
                        = (col[fields["is_identity"]] == trueSql ? true : false);
                    bool isComputed
                        = (col[fields["is_computed"]] == trueSql ? true : false);
                    string calcFormula = col[fields["definition"]];
                    if (col[fields["character_max_length"]].Length > 0)
                        dataType += "(" + col[fields["character_max_length"]] + ")";
                    else if (dataType.Contains("varchar"))
                        dataType += "(max)";
                    column = new TableColumnImmutable(
                        col[fields["column_name"]]
                        , dataType
                        , sqlTranslator: SqlTranslator
                        , table: this
                        , isNullable: isNullable
                        , isIdentity: isIdentity
                        , calcFormula: calcFormula
                        , id: Int32.Parse(col[fields["column_id"]])
                        , retrieveIDsFromDatabase: false
                        , assert: false
                        );
                }
                columns[column.Name]
                    = new Tuple<int, IColumn>(column.ID.Value, column);
            }

            if (assert) Debug.Assert(PassesSelfAssertion());

        }

        public class MultiplePrimaryKeysException : Exception
        {
            public MultiplePrimaryKeysException(
                ITable badTable
                , string badTableTypeName
                , string existingPrimaryKeyName
                , string additionalPrimaryKeyName
                , string message = null
                ) : base(
                    message:
                    message != null ? message
                    :
                    "Tried to associate two IPrimaryKey objects "
                    + "on table "
                    + badTable != null && badTable.Name != null ? "'" + badTable.Name + "'" : "[unknown]"
                    + " of type "
                    + badTableTypeName != null ? badTableTypeName
                    : badTable != null ? badTable.GetType().FullName
                    : "[unknown]"
                    + ". The existing primary key is "
                    + existingPrimaryKeyName != null ? "'" + existingPrimaryKeyName + "'" : "[unknown]"
                    + "; the primary key that someone attempted to add was "
                    + additionalPrimaryKeyName != null ? "'" + additionalPrimaryKeyName + "'" : "[unknown]"
                    + "."
                    )
            {
                BadTable = badTable;
                BadTableTypeName = badTableTypeName;
                ExistingPrimaryKeyName = existingPrimaryKeyName;
                AdditionalPrimaryKeyName = additionalPrimaryKeyName;
            }
            public ITable BadTable { get; }
            public string BadTableTypeName { get; }
            public string ExistingPrimaryKeyName { get; }
            public string AdditionalPrimaryKeyName { get; }
        }

        public class BadColumnTypeException : Exception
        {
            private BadColumnTypeException() { }
            public BadColumnTypeException(
                ITable badTable
                , string badTableName
                , string badTableType
                , IColumn badColumn
                , string badColumnName
                , string badColumnType
                , string message = null
                ) : base(
                    message:
                    message != null ? message
                    : "For table '"
                    + badTableName != null ? badTableName
                    : badTable != null && badTable.Name != null ? badTable.Name
                    : "[unknown]"
                    + "' of type '"
                    + badTableType != null ? badTableType
                    : badTable != null ? badTable.GetType().FullName
                    : "[unknown]"
                    + "', tried to attach column '"
                    + badColumnName != null ? badColumnName
                    : badColumn != null && badColumn.Name != null ? badColumn.Name
                    : "[unknown]"
                    + "' of type '"
                    + badColumnType != null ? badColumnType
                    : badColumn != null ? badColumn.GetType().FullName
                    : "[unknown]"
                    + ", which is not a valid column type for tables "
                    + "(i.e., it does not implement ITableColumn)."
                    )
            {
                BadTable = badTable;
                BadTableName = badTableName;
                BadTableType = badTableType;
                BadColumn = badColumn;
                BadColumnName = badColumnName;
                BadColumnType = badColumnType;
            }
            ITable BadTable { get; }
            string BadTableName { get; }
            string BadTableType { get; }
            IColumn BadColumn { get; }
            string BadColumnName { get; }
            string BadColumnType { get; }
        }

        public class NoParentTableException : Exception
        {
            private NoParentTableException() { }
            public NoParentTableException(
                ITable badTable
                , string badTableName
                , string badTableType
                , string message = null
                ) : base(
                    message:
                    message != null ? message
                    : "For table '"
                    + badTableName != null ? badTableName
                    : badTable != null && badTable.Name != null ? badTable.Name
                    : "[unknown]"
                    + "' of type '"
                    + badTableType != null ? badTableType
                    : badTable != null ? badTable.GetType().FullName
                    : "[unknown]"
                    + "', tried to set up a non-empty ParentInsertColumnListExpressions "
                    + "list when the table has no parent table to begin with."
                    )
            {
                BadTable = badTable;
                BadTableName = badTableName;
                BadTableType = badTableType;
            }
            ITable BadTable { get; }
            string BadTableName { get; }
            string BadTableType { get; }
        }

        public class NotAParentTableColumnException : Exception
        {
            private NotAParentTableColumnException() { }
            public NotAParentTableColumnException(
                ITable badTable
                , string badTableName
                , string badTableType
                , string badColumnName
                , ITable parentTable
                , string parentTableName
                , string parentTableType
                , string message = null
                ) : base(
                    message:
                    message != null ? message
                    : "For table '"
                    + badTableName != null ? badTableName
                    : badTable != null && badTable.Name != null ? badTable.Name
                    : "[unknown]"
                    + "' of type '"
                    + badTableType != null ? badTableType
                    : badTable != null ? badTable.GetType().FullName
                    : "[unknown]"
                    + "', tried to set up a ParentInsertColumnListExpressions "
                    + "mapping the column name '"
                    + badColumnName != null ? badColumnName : "[unknown]"
                    + ", which does not exist in the parent table '"
                    + parentTableName != null ? parentTableName
                    : parentTable != null && parentTable.Name != null ? parentTable.Name
                    : "[unknown]"
                    + "' of type '"
                    + parentTableType != null ? parentTableType
                    : parentTable != null ? parentTable.GetType().FullName
                    : "[unknown]"
                    + "."
                    )
            {
                BadTable = badTable;
                BadTableName = badTableName;
                BadTableType = badTableType;
                BadColumnName = badColumnName;
                ParentTable = parentTable;
                ParentTableName = parentTableName;
                ParentTableType = parentTableType;
            }
            ITable BadTable { get; }
            string BadTableName { get; }
            string BadTableType { get; }
            string BadColumnName { get; }
            ITable ParentTable { get; }
            string ParentTableName { get; }
            string ParentTableType { get; }
        }

        public class MissingHomeVableColumnException : Exception
        {
            private MissingHomeVableColumnException() { }
            public MissingHomeVableColumnException(
                ITable badTable
                , string badTableName
                , string badTableType
                , string homeVableColumnName
                , ITable parentTable
                , string parentTableName
                , string parentTableType
                , string message = null
                ) : base(
                    message:
                    message != null ? message
                    : "For table '"
                    + badTableName != null ? badTableName
                    : badTable != null && badTable.Name != null ? badTable.Name
                    : "[unknown]"
                    + "' of type '"
                    + badTableType != null ? badTableType
                    : badTable != null ? badTable.GetType().FullName
                    : "[unknown]"
                    + "', tried to set up a ParentInsertColumnListExpressions "
                    + "without mapping the parent table's column name '"
                    + homeVableColumnName != null ? homeVableColumnName
                    : parentTable != null && parentTable.HomeVableNameColumn != null ? parentTable.HomeVableNameColumn.Name
                    : "[unknown]"
                    + "'. The parent table is table '"
                    + parentTableName != null ? parentTableName
                    : parentTable != null && parentTable.Name != null ? parentTable.Name
                    : "[unknown]"
                    + "' of type '"
                    + parentTableType != null ? parentTableType
                    : parentTable != null ? parentTable.GetType().FullName
                    : "[unknown]"
                    + "."
                    )
            {
                BadTable = badTable;
                BadTableName = badTableName;
                BadTableType = badTableType;
                HomeVableColumnName = homeVableColumnName;
                ParentTable = parentTable;
                ParentTableName = parentTableName;
                ParentTableType = parentTableType;
            }
            ITable BadTable { get; }
            string BadTableName { get; }
            string BadTableType { get; }
            string HomeVableColumnName { get; }
            ITable ParentTable { get; }
            string ParentTableName { get; }
            string ParentTableType { get; }
        }

        public class AttemptedUpdateOfNonupdatableTableException : Exception
        {
            private AttemptedUpdateOfNonupdatableTableException() { }
            public AttemptedUpdateOfNonupdatableTableException(
                ITable badTable
                , string badTableName
                , string badTableType
                , string message = null
                ) : base(
                    message:
                    message != null ? message
                    : "Someone attempted to update table '"
                    + badTableName != null ? badTableName
                    : badTable != null && badTable.Name != null ? badTable.Name
                    : "[unknown]"
                    + "' of type '"
                    + badTableType != null ? badTableType
                    : badTable != null ? badTable.GetType().FullName
                    : "[unknown]"
                    + "', which does not accept updates."
                    )
            {
                BadTable = badTable;
                BadTableName = badTableName;
                BadTableType = badTableType;
            }
            ITable BadTable { get; }
            string BadTableName { get; }
            string BadTableType { get; }
        }

        public class AttemptedUpdateOfNonupdatableColumnException : Exception
        {
            private AttemptedUpdateOfNonupdatableColumnException() { }
            public AttemptedUpdateOfNonupdatableColumnException(
                ITable badTable
                , string badTableName
                , string badTableType
                , int badRowID
                , string changedColumns
                , string message = null
                ) : base(
                    message:
                    message != null ? message
                    : "Someone attempted to update one or more non-updatable columns on row #"
                    + badRowID != null ? badRowID.ToString() : "[unknown]"
                    + " on table '"
                    + badTableName != null ? badTableName
                    : badTable != null && badTable.Name != null ? badTable.Name
                    : "[unknown]"
                    + "' of type '"
                    + badTableType != null ? badTableType
                    : badTable != null ? badTable.GetType().FullName
                    : "[unknown]"
                    + "'. "
                    + changedColumns != null ? changedColumns : ""
                    )
            {
                BadTable = badTable;
                BadTableName = badTableName;
                BadTableType = badTableType;
                BadRowID = badRowID;
                ChangedColumns = changedColumns;
            }
            ITable BadTable { get; }
            string BadTableName { get; }
            string BadTableType { get; }
            int BadRowID { get; }
            string ChangedColumns { get; }
        }

        public class AttemptedDeleteFromInvariableTableException : Exception
        {
            private AttemptedDeleteFromInvariableTableException() { }
            public AttemptedDeleteFromInvariableTableException(
                ITable badTable
                , string badTableName
                , string badTableType
                , string message = null
                ) : base(
                    message:
                    message != null ? message
                    : "Someone attempted to delete rows from table '"
                    + badTableName != null ? badTableName
                    : badTable != null && badTable.Name != null ? badTable.Name
                    : "[unknown]"
                    + "' of type '"
                    + badTableType != null ? badTableType
                    : badTable != null ? badTable.GetType().FullName
                    : "[unknown]"
                    + "', which does not allow deletes."
                    )
            {
                BadTable = badTable;
                BadTableName = badTableName;
                BadTableType = badTableType;
            }
            ITable BadTable { get; }
            string BadTableName { get; }
            string BadTableType { get; }
        }

        public class NoIdentityColumnException : Exception
        {
            private NoIdentityColumnException() { }
            public NoIdentityColumnException(
                ITable badTable
                , string badTableName
                , string badTableType
                , string message = null
                ) : base(
                    message:
                    message != null ? message
                    : "Any SQL Server table complex enough to require update triggers, "
                    + "requires by Veritas policy an identity column so that inserted "
                    + "and deleted rows can be matched to each other properly. Alas, "
                    + "table "
                    + badTableName != null ? badTableName
                    : badTable != null && badTable.Name != null ? badTable.Name
                    : "[unknown]"
                    + "' of type '"
                    + badTableType != null ? badTableType
                    : badTable != null ? badTable.GetType().FullName
                    : "[unknown]"
                    + "' has no identity column and yet has an update trigger."
                    )
            {
                BadTable = badTable;
                BadTableName = badTableName;
                BadTableType = badTableType;
            }
            ITable BadTable { get; }
            string BadTableName { get; }
            string BadTableType { get; }
        }

        public class NotAPolymorphicChildException : Exception
        {
            private NotAPolymorphicChildException() { }
            public NotAPolymorphicChildException(
                ITable badTable
                , string badTableName
                , string badTableType
                , string calledMethod
                , string message = null
                ) : base(
                    message:
                    message != null ? message
                    : "For table "
                    + badTableName != null ? badTableName
                    : badTable != null && badTable.Name != null ? badTable.Name
                    : "[unnamed]"
                    + "' of type '"
                    + badTableType != null ? badTableType
                    : badTable != null ? badTable.GetType().FullName
                    : "[unknown]"
                    + "', the method "
                    + calledMethod != null ? calledMethod : "[unknown]"
                    + " was called. This method only applies to "
                    + "tables that are branches and leaves in a polymorphic "
                    + "hierarchy, which is not true for tables of this class."
                    )
            {
                BadTable = badTable;
                BadTableName = badTableName;
                BadTableType = badTableType;
                CalledMethod = calledMethod;
            }
            ITable BadTable { get; }
            string BadTableName { get; }
            string BadTableType { get; }
            string CalledMethod { get; }
        }

    }

    /// <summary>
    /// Boilerplate implementation of ITable without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForOwner, always unless abstract class.</item>
    ///   <item>GetCopyForOwner, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public class TableImmutable : VableImmutable, ITable
    {
        private ITable Delegee { get { return (ITable)_delegee; } }

        //Disable default constructor
        protected TableImmutable() : base() { }

        public TableImmutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ISchema schema = null
            , IObjectType objectType = null
            , IDataProcObject owner = null
            , bool allowsUpdates = true
            , bool allowsDeletes = true
            , Dictionary<string, Tuple<int, ITableColumn>> columns = null
            , Dictionary<string, IIndex> indexes = null
            , bool hasIdentityColumn = true
            , IIdentityColumn identityColumn = null
            , IPrimaryKey primaryKey = null
            , Dictionary<string, IUniqueKey> uniqueKeys = null
            , Dictionary<string, IForeignKey> foreignKeys = null
            , ICascadingForeignKeyRegistry cascadingForeignKeyRegistry = null
            , Dictionary<string, ITableTrigger> triggers = null
            , Dictionary<string, ICheckConstraint> checkConstraints = null
            , Dictionary<string, IDefaultConstraint> defaultConstraints = null
            , bool isAudited = false
            , IAuditTable auditTable = null
            , bool publishesChanges = false
            , IChangeTrackingTable publishedChangesTable = null
            , IChangeSubscriberRegistry changeSubscriberRegistry = null
            , ITable parentTable = null
            , Dictionary<string, string> parentInsertColumnListExpressions = null
            // , IParentPassThroughInfo parentPassThrough = null
            , bool canHaveChildren = false
            , ILeavesTable leavesTable = null
            , bool maintainChildVablesTable = false
            , IChildVablesTable childVablesTable = null
            , bool hasIDColumn = false
            , ITableIDColumn idColumn = null
            , bool hasInputterColumn = false
            , IInputterColumn inputterColumn = null
            , IValidInputtersTable validInputtersTable = null
            , bool hasLastUpdateDtmColumn = false
            , bool hasLastUpdaterColumn = false
            , string nameWhenPrefixed = null
            , int? id = default(int?)
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
        {
            Dictionary<string, Tuple<int, ITableColumn>> safeColumns = new Dictionary<string, Tuple<int, ITableColumn>>();
            foreach (KeyValuePair<string, Tuple<int, ITableColumn>> pair in columns)
                safeColumns[pair.Key]
                    = new Tuple<int, ITableColumn>(
                        pair.Value.Item1
                        , (ITableColumn)pair.Value.Item2.GetSafeReference()
                        );
            Dictionary<string, IIndex> safeIndexes = new Dictionary<string, IIndex>();
            foreach (KeyValuePair<string, IIndex> pair in indexes)
                safeIndexes[pair.Key] = (IIndex)pair.Value.GetSafeReference();
            Dictionary<string, IUniqueKey> safeUniqueKeys = new Dictionary<string, IUniqueKey>();
            foreach (KeyValuePair<string, IUniqueKey> pair in uniqueKeys)
                safeUniqueKeys[pair.Key] = (IUniqueKey)pair.Value.GetSafeReference();
            Dictionary<string, IForeignKey> safeForeignKeys = new Dictionary<string, IForeignKey>();
            foreach (KeyValuePair<string, IForeignKey> pair in foreignKeys)
                safeForeignKeys[pair.Key] = (IForeignKey)pair.Value.GetSafeReference();
            Dictionary<string, ITableTrigger> safeTriggers = new Dictionary<string, ITableTrigger>();
            foreach (KeyValuePair<string, ITableTrigger> pair in triggers)
                safeTriggers[pair.Key] = (ITableTrigger)pair.Value.GetSafeReference();
            Dictionary<string, ICheckConstraint> safeCheckConstraints = new Dictionary<string, ICheckConstraint>();
            foreach (KeyValuePair<string, ICheckConstraint> pair in checkConstraints)
                safeCheckConstraints[pair.Key] = (ICheckConstraint)pair.Value.GetSafeReference();
            Dictionary<string, IDefaultConstraint> safeDefaultConstraints = new Dictionary<string, IDefaultConstraint>();
            foreach (KeyValuePair<string, IDefaultConstraint> pair in defaultConstraints)
                safeDefaultConstraints[pair.Key] = (IDefaultConstraint)pair.Value.GetSafeReference();
            Dictionary<string, string> safeParentInsertColumnListExpressions = new Dictionary<string, string>();
            foreach (KeyValuePair<string, string> pair in parentInsertColumnListExpressions)
                safeParentInsertColumnListExpressions[pair.Key] = pair.Value;
            _delegee
                = new TableMutable(
                    name
                    , sqlTranslator
                    , schema == null ? null : (ISchema)schema.GetSafeReference()
                    , objectType == null ? null : (IObjectType)objectType.GetSafeReference()
                    , owner == null ? null : (IDataProcObject)owner.GetSafeReference()
                    , allowsUpdates
                    , allowsDeletes
                    , safeColumns
                    , safeIndexes
                    , hasIdentityColumn
                    , identityColumn == null ? null : (IIdentityColumn)identityColumn.GetSafeReference()
                    , primaryKey == null ? null : (IPrimaryKey)primaryKey.GetSafeReference()
                    , safeUniqueKeys
                    , safeForeignKeys
                    , cascadingForeignKeyRegistry == null ? null : (ICascadingForeignKeyRegistry)cascadingForeignKeyRegistry.GetSafeReference()
                    , safeTriggers
                    , safeCheckConstraints
                    , safeDefaultConstraints
                    , isAudited
                    , auditTable == null ? null : (IAuditTable)auditTable.GetSafeReference()
                    , publishesChanges
                    , publishedChangesTable == null ? null : (IChangeTrackingTable)publishedChangesTable.GetSafeReference()
                    , changeSubscriberRegistry == null ? null : (IChangeSubscriberRegistry)changeSubscriberRegistry.GetSafeReference()
                    , parentTable == null ? null : (ITable)parentTable.GetSafeReference()
                    , parentInsertColumnListExpressions == null ? null : safeParentInsertColumnListExpressions
                    // , parentPassThrough == null ? null : (IParentPassThroughInfo)parentPassThrough.GetSafeReference()
                    , canHaveChildren
                    , leavesTable == null ? null : (ILeavesTable)leavesTable.GetSafeReference()
                    , maintainChildVablesTable
                    , childVablesTable == null ? null : (IChildVablesTable)childVablesTable.GetSafeReference()
                    , hasIDColumn
                    , idColumn == null ? null : (ITableIDColumn)idColumn.GetSafeReference()
                    , hasInputterColumn
                    , inputterColumn == null ? null : (IInputterColumn)inputterColumn.GetSafeReference()
                    , validInputtersTable == null ? null : (IValidInputtersTable)validInputtersTable.GetSafeReference()
                    , hasLastUpdateDtmColumn
                    , hasLastUpdaterColumn
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , assert: false
                    );
            CompleteConstructionIfType(typeof(TableImmutable), false, true);
        }

        public TableImmutable(ITable delegee, bool assert = true)
        {
            RejectIfInvalidDelegeeType(delegee);
            _delegee = delegee;
            CompleteConstructionIfType(typeof(TableImmutable), false, true);
        }

        // IDataProcObject method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(ITable); } }

        // IDatabaseObject method overrides

        public override IDatabaseObject GetReferenceForOwner(IDataProcObject owner, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            RejectIfInvalidOwnerType(owner);
            return new TableImmutable(
                (ITable)Delegee.GetReferenceForOwner(owner, retrieveIDsFromDatabase, false)
                , assert
                );
        }

        public override IDatabaseObject GetCopyForOwner(IDataProcObject owner, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            RejectIfInvalidOwnerType(owner);
            return new TableImmutable(
                (ITable)Delegee.GetCopyForOwner(owner, retrieveIDsFromDatabase, false)
                , assert
                );
        }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            RejectIfInvalidParentObjectType(parentObject);
            return new TableImmutable(
                (ITable)Delegee.GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, false)
                , assert
                );
        }

        public override IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            RejectIfInvalidParentObjectType(parentObject);
            return new TableImmutable(
                (ITable)Delegee.GetCopyForParentObject(parentObject, retrieveIDsFromDatabase, false)
                , assert
                );
        }

        // ITable properties and methods

        public Dictionary<string, Tuple<int, ITableColumn>> TableColumns { get { return Delegee.GetTableColumnsSafely(); } }
        public bool HasIdentityColumn { get { return Delegee.HasIdentityColumn; } }
        public IIdentityColumn IdentityColumn { get { return Delegee.GetIdentityColumnSafely(); } }
        public string IdentityColumnName { get { return Delegee.IdentityColumnName; } }
        public bool HasPrimaryKey { get { return Delegee.HasPrimaryKey; } }
        public IPrimaryKey PrimaryKey { get { return Delegee.GetPrimaryKeySafely(); } }
        public Dictionary<string, IUniqueKey> UniqueKeys { get { return Delegee.GetUniqueKeysSafely(); } }
        public bool HasNonPrimaryUniqueKeys { get { return Delegee.HasNonPrimaryUniqueKeys; } }
        public Dictionary<string, IUniqueKey> NonPrimaryUniqueKeys { get { return Delegee.GetNonPrimaryUniqueKeysSafely(); } }
        public Dictionary<string, IForeignKey> ForeignKeys { get { return Delegee.GetForeignKeysSafely(); } }
        public ICascadingForeignKeyRegistry CascadingForeignKeyRegistry { get { return Delegee.GetCascadingForeignKeyRegistrySafely(); } }
        public List<ICascadingForeignKey> CascadingOnDeleteForeignKeys { get { return Delegee.GetCascadingOnDeleteForeignKeysSafely(); } }
        public List<ICascadingForeignKey> CascadingOnUpdateForeignKeys { get { return Delegee.GetCascadingOnUpdateForeignKeysSafely(); } }
        public Dictionary<string, ITableTrigger> Triggers { get { return Delegee.GetTriggersSafely(); } }
        public Dictionary<string, ICheckConstraint> CheckConstraints { get { return Delegee.GetCheckConstraintsSafely(); } }
        public Dictionary<string, IDefaultConstraint> DefaultConstraints { get { return Delegee.GetDefaultConstraintsSafely(); } }
        public bool IsAudited { get { return Delegee.IsAudited; } }
        public string AuditTableName { get { return Delegee.AuditTableName; } }
        public IAuditTable AuditTable { get { return Delegee.GetAuditTableSafely(); } }
        public Dictionary<string, Tuple<int, ITableColumn>> InsertableColumns { get { return Delegee.GetInsertableColumnsSafely(); } }
        public Dictionary<string, Tuple<int, ITableColumn>> UpdatableColumns { get { return Delegee.GetUpdatableColumnsSafely(); } }
        public bool PublishesChanges { get { return Delegee.PublishesChanges; } }
        public IChangeTrackingTable PublishedChangesTable { get { return Delegee.GetPublishedChangesTableSafely(); } }
        public string PublishedChangesTableName { get { return Delegee.PublishedChangesTableName; } }
        public IChangeSubscriberRegistry ChangeSubscriberRegistry { get { return Delegee.GetChangeSubscriberRegistrySafely(); } }
        public ITable ParentTable { get { return Delegee.GetParentTableSafely(); } }
        public Dictionary<string, string> ParentInsertColumnListExpressions { get { return Delegee.GetParentInsertColumnListExpressionsSafely(); } }
        // IParentPassThroughInfo ParentPassThrough { get; }
        public bool CanHaveChildren
        { get { return Delegee.CanHaveChildren; } }
        public IHomeVableNameColumn HomeVableNameColumn { get { return Delegee.GetHomeVableNameColumnSafely(); } }
        public string LeavesTableName { get { return Delegee.LeavesTableName; } }
        public ILeavesTable LeavesTable { get { return Delegee.GetLeavesTableSafely(); } }
        public bool MaintainChildVablesTable { get { return Delegee.MaintainChildVablesTable; } }
        public string ChildVablesTableName { get { return Delegee.ChildVablesTableName; } }
        public IChildVablesTable ChildVablesTable { get { return Delegee.GetChildVablesTableSafely(); } }
        public bool HasIDColumn { get { return Delegee.HasIDColumn; } }
        public ITableIDColumn IDColumn { get { return Delegee.GetIDColumnSafely(); } }
        public string IDColumnName { get { return Delegee.IDColumnName; } }
        public string TempInsertedIDBaseColumnName { get { return Delegee.TempInsertedIDBaseColumnName; } }
        public bool HasInputterColumn { get { return Delegee.HasInputterColumn; } }
        public IInputterColumn InputterColumn { get { return Delegee.GetInputterColumnSafely(); } }
        public string InputterColumnName { get { return Delegee.InputterColumnName; } }
        public IValidInputtersTable ValidInputtersTable { get { return Delegee.GetValidInputtersTableSafely(); } }
        public string ValidInputtersTableName { get { return Delegee.ValidInputtersTableName; } }
        public bool HasLastUpdateDtmColumn { get { return Delegee.HasLastUpdateDtmColumn; } }
        public ILastUpdateDtmColumn LastUpdateDtmColumn { get { return Delegee.GetLastUpdateDtmColumnSafely(); } }
        public string LastUpdateDtmColumnName { get { return Delegee.LastUpdateDtmColumnName; } }
        public bool HasLastUpdaterColumn { get { return Delegee.HasLastUpdaterColumn; } }
        public ILastUpdaterColumn LastUpdaterColumn { get { return Delegee.GetLastUpdaterColumnSafely(); } }
        public string LastUpdaterColumnName { get { return Delegee.LastUpdaterColumnName; } }
        public Dictionary<string, Tuple<int, ITableColumn>> GetTableColumnsSafely() { return Delegee.GetTableColumnsSafely(); }
        public IIdentityColumn GetIdentityColumnSafely() { return Delegee.GetIdentityColumnSafely(); }
        public IPrimaryKey GetPrimaryKeySafely() { return Delegee.GetPrimaryKeySafely(); }
        public Dictionary<string, IUniqueKey> GetUniqueKeysSafely() { return Delegee.GetUniqueKeysSafely(); }
        public Dictionary<string, IUniqueKey> GetNonPrimaryUniqueKeysSafely() { return Delegee.GetNonPrimaryUniqueKeysSafely(); }
        public Dictionary<string, IForeignKey> GetForeignKeysSafely() { return Delegee.GetForeignKeysSafely(); }
        public ICascadingForeignKeyRegistry GetCascadingForeignKeyRegistrySafely() { return Delegee.GetCascadingForeignKeyRegistrySafely(); }
        public List<ICascadingForeignKey> GetCascadingOnDeleteForeignKeysSafely() { return Delegee.GetCascadingOnDeleteForeignKeysSafely(); }
        public List<ICascadingForeignKey> GetCascadingOnUpdateForeignKeysSafely() { return Delegee.GetCascadingOnUpdateForeignKeysSafely(); }
        public Dictionary<string, ITableTrigger> GetTriggersSafely() { return Delegee.GetTriggersSafely(); }
        public Dictionary<string, ICheckConstraint> GetCheckConstraintsSafely() { return Delegee.GetCheckConstraintsSafely(); }
        public Dictionary<string, IDefaultConstraint> GetDefaultConstraintsSafely() { return Delegee.GetDefaultConstraintsSafely(); }
        public IAuditTable GetAuditTableSafely() { return Delegee.GetAuditTableSafely(); }
        public Dictionary<string, Tuple<int, ITableColumn>> GetInsertableColumnsSafely() { return Delegee.GetInsertableColumnsSafely(); }
        public Dictionary<string, Tuple<int, ITableColumn>> GetUpdatableColumnsSafely() { return Delegee.GetUpdatableColumnsSafely(); }
        public IChangeTrackingTable GetPublishedChangesTableSafely() { return Delegee.GetPublishedChangesTableSafely(); }
        public IChangeSubscriberRegistry GetChangeSubscriberRegistrySafely() { return Delegee.GetChangeSubscriberRegistrySafely(); }
        public ITable GetParentTableSafely() { return Delegee.GetParentTableSafely(); }
        public Dictionary<string, string> GetParentInsertColumnListExpressionsSafely() { return Delegee.GetParentInsertColumnListExpressionsSafely(); }
        // IParentPassThroughInfo GetParentPassThroughSafely() { return Delegee.GetParentPassThroughSafely(); }
        public IHomeVableNameColumn GetHomeVableNameColumnSafely()
        { return Delegee.GetHomeVableNameColumnSafely(); }
        public ILeavesTable GetLeavesTableSafely() { return Delegee.GetLeavesTableSafely(); }
        public IChildVablesTable GetChildVablesTableSafely() { return Delegee.GetChildVablesTableSafely(); }
        public ITableIDColumn GetIDColumnSafely() { return Delegee.GetIDColumnSafely(); }
        public IInputterColumn GetInputterColumnSafely() { return Delegee.GetInputterColumnSafely(); }
        public IValidInputtersTable GetValidInputtersTableSafely() { return Delegee.GetValidInputtersTableSafely(); }
        public ILastUpdateDtmColumn GetLastUpdateDtmColumnSafely() { return Delegee.GetLastUpdateDtmColumnSafely(); }
        public ILastUpdaterColumn GetLastUpdaterColumnSafely() { return Delegee.GetLastUpdaterColumnSafely(); }
        public string InsertColumnListSql(
            bool useLineBreaks = false
            , bool includeDefs = false
            , bool includeDefaultColumnConstraints = false
            , bool applyTrims = false
            , bool includeMutableColumns = true
            , bool prettify = false
            , int minDatatypeOffset = 0
            , int minNullabilityOffset = 0
            , int spacesToIndent = 0
            )
        {
            return
                Delegee.InsertColumnListSql(
                    useLineBreaks
                    , includeDefs
                    , includeDefaultColumnConstraints
                    , applyTrims
                    , includeMutableColumns
                    , prettify
                    , minDatatypeOffset
                    , minNullabilityOffset
                    , spacesToIndent
                    );
        }
        public string UpdateColumnListSql() { return Delegee.UpdateColumnListSql(); }
        public string ParentColumnListInsertExpressions(
            bool useLineBreaks = false
            , int spacesToIndent = 0
            )
        {
            return
                Delegee.ParentColumnListInsertExpressions(
                    useLineBreaks
                    , spacesToIndent
                    );
        }

        // Trigger methods

        public void PerformBeforeInsertTriggerLogic(IConnection conn)
        {
            Delegee.PerformBeforeInsertTriggerLogic(conn);
        }
        public void PerformBeforeInsertTriggerLogicWithInsertedTable(IConnection conn)
        {
            Delegee.PerformBeforeInsertTriggerLogicWithInsertedTable(conn);
        }
        public void PerformBeforeInsertTriggerLogicForEachRow(IConnection conn)
        {
            Delegee.PerformBeforeInsertTriggerLogicForEachRow(conn);
        }
        public void PerformBeforeUpdateTriggerLogic(IConnection conn)
        {
            Delegee.PerformBeforeUpdateTriggerLogic(conn);
        }
        public void PerformBeforeUpdateTriggerLogicWithInsertedTable(IConnection conn)
        {
            Delegee.PerformBeforeUpdateTriggerLogicWithInsertedTable(conn);
        }
        public void PerformBeforeUpdateTriggerLogicForEachRow(IConnection conn)
        {
            Delegee.PerformBeforeUpdateTriggerLogicForEachRow(conn);
        }
        public void PerformBeforeDeleteTriggerLogic(IConnection conn)
        {
            Delegee.PerformBeforeDeleteTriggerLogic(conn);
        }
        public void PerformBeforeDeleteTriggerLogicWithInsertedTable(IConnection conn)
        {
            Delegee.PerformBeforeDeleteTriggerLogicWithInsertedTable(conn);
        }
        public void PerformBeforeDeleteTriggerLogicForEachRow(IConnection conn)
        {
            Delegee.PerformBeforeDeleteTriggerLogicForEachRow(conn);
        }
        public void PerformAfterInsertTriggerLogic(IConnection conn)
        {
            Delegee.PerformAfterInsertTriggerLogic(conn);
        }
        public void PerformAfterInsertTriggerLogicWithInsertedTable(IConnection conn)
        {
            Delegee.PerformAfterInsertTriggerLogicWithInsertedTable(conn);
        }
        public void PerformAfterInsertTriggerLogicForEachRow(IConnection conn)
        {
            Delegee.PerformAfterInsertTriggerLogicForEachRow(conn);
        }
        public void PerformAfterAnythingTriggerLogic(IConnection conn)
        {
            Delegee.PerformAfterAnythingTriggerLogic(conn);
        }
        public void PerformAfterAnythingTriggerLogicWithInsertedTable(IConnection conn)
        {
            Delegee.PerformAfterAnythingTriggerLogicWithInsertedTable(conn);
        }
        public void PerformAfterAnythingTriggerLogicForEachRow(IConnection conn)
        {
            Delegee.PerformAfterAnythingTriggerLogicForEachRow(conn);
        }
        public string TriggerTimingTag(TriggerTimings timing)
        {
            return
                Delegee.TriggerTimingTag(timing);
        }
        public string LFAndLeadingSpaces(int spacesToIndent)
        {
            return
                Delegee.LFAndLeadingSpaces(spacesToIndent);
        }
        public string TriggerTableLoadSql(
            string tempTableName
            , bool includeMutableColumns
            , int spacesToIndent = 0
            )
        {
            return
                Delegee.TriggerTableLoadSql(
                    tempTableName
                    , includeMutableColumns
                    , spacesToIndent
                    );
        }
        public int? ForcedUseOfTempTriggerTableThreshold(TriggerTimings timing)
        {
            return
                Delegee.ForcedUseOfTempTriggerTableThreshold(timing);
        }
        // Support for before-insert triggers
        public string NameOfNewlyCreatedInsertedTriggerTable(
           string tempTableTag
           , TriggerTimings timing
           , IConnection conn
           )
        {
            return
                Delegee.NameOfNewlyCreatedInsertedTriggerTable(
                    tempTableTag
                    , timing
                    , conn
                    );
        }
        public string InsertedTriggerTableCreateSql(
            string tempTableName
            , bool globallyAvailable
            , bool createInMemoryTable
            , TriggerTimings timing
            , int spacesToIndent = 0
            )
        {
            return
                Delegee.InsertedTriggerTableCreateSql(
                    tempTableName
                    , globallyAvailable
                    , createInMemoryTable
                    , timing
                    , spacesToIndent
                    );
        }
        public void LoadInsertedTriggerTable(
            string tempTableName
            , IConnection conn
            )
        {
            Delegee.LoadInsertedTriggerTable(tempTableName, conn);
        }
        public string InsertedTriggerTableLoadSql(string tempTableName, int spacesToIndent = 0)
        {
            return
                Delegee.InsertedTriggerTableLoadSql(
                    tempTableName
                    , spacesToIndent
                    );
        }
        public string IndexTempTableByIdentityColumnSql(
            string tempTableName
            , string tempTableIndexName = null
            )
        {
            return
                Delegee.IndexTempTableByIdentityColumnSql(
                    tempTableName
                    , tempTableIndexName
                    );
        }
        public string IndexTempTableByIDColumnSql(
            string tempTableName
            , string tempTableIndexName = null
            )
        {
            return
                Delegee.IndexTempTableByIDColumnSql(
                    tempTableName
                    , tempTableIndexName
                    );
        }
        public string InsertedTriggerTableAddToParentSql(
            string tempTableName
            , int spacesToIndent = 0
            )
        {
            return
                Delegee.InsertedTriggerTableAddToParentSql(
                    tempTableName
                    , spacesToIndent
                    );
        }
        public string InsertFromInsertedTriggerTableSql(
            string tempTableName
            , string currentTimestampSql
            , string userSql
            , int spacesToIndent = 0
            )
        {
            return
                Delegee.InsertFromInsertedTriggerTableSql(
                    tempTableName
                    , currentTimestampSql
                    , userSql
                    , spacesToIndent
                    );
        }
        public void LoadChangeTableAfterInsert(
            IChangeTrackingTable changeTable
            , string updateDtmSql
            , string updaterSql
            , string tempInsertedName
            , IConnection conn
            )
        {
            Delegee.LoadChangeTableAfterInsert(
                changeTable
                , updateDtmSql
                , updaterSql
                , tempInsertedName
                , conn
                );
        }
        // Support for before-update triggers
        public string NameOfNewlyCreatedDeletedTriggerTable(
            string tempTableTag
            , bool includeMutableColumns
            , TriggerTimings timing
            , IConnection conn
            )
        {
            return
                Delegee.NameOfNewlyCreatedDeletedTriggerTable(
                    tempTableTag
                    , includeMutableColumns
                    , timing
                    , conn
                    );
        }
        public string DeletedTriggerTableCreateSql(
            string tempTableName
            , bool includeMutableColumns
            , bool globallyAvailable
            , bool createInMemoryTable
            , TriggerTimings timing
            , int spacesToIndent = 0
            )
        {
            return
                Delegee.DeletedTriggerTableCreateSql(
                    tempTableName
                    , includeMutableColumns
                    , globallyAvailable
                    , createInMemoryTable
                    , timing
                    , spacesToIndent
                    );
        }
        public string NameOfNewlyCreatedMergedTriggerTable(
            string tempTableTag
            , IConnection conn
            )
        {
            return
                Delegee.NameOfNewlyCreatedMergedTriggerTable(
                    tempTableTag
                    , conn
                    );
        }
        public string MergedTriggerTableCreateSql(
            string tempTableName
            , bool globallyAvailable
            , bool createInMemoryTable
            , bool prettify = false
            , int minDatatypeOffset = 0
            , int minNullabilityOffset = 0
            , int spacesToIndent = 0
            )
        {
            return
                Delegee.MergedTriggerTableCreateSql(
                    tempTableName
                    , globallyAvailable
                    , createInMemoryTable
                    , prettify
                    , minDatatypeOffset
                    , minNullabilityOffset
                    , spacesToIndent
                    );
        }
        public void LoadDeletedTriggerTable(
            string tempTableName
            , bool includeMutableColumns
            , IConnection conn
            )
        {
            Delegee.LoadDeletedTriggerTable(
                tempTableName
                , includeMutableColumns
                , conn
                );
        }
        public string DeletedTriggerTableLoadSql(
            string tempTableName
            , bool includeMutableColumns
            , int spacesToIndent = 0
            )
        {
            return
                Delegee.DeletedTriggerTableLoadSql(
                    tempTableName
                    , includeMutableColumns
                    , spacesToIndent
                    );
        }
        public void LoadMergedTriggerTable(
            string tempMergedName
            , bool tempInsertedName
            , bool tempDeletedName
            , IConnection conn
            )
        {
            Delegee.LoadMergedTriggerTable(
                tempMergedName
                , tempInsertedName
                , tempDeletedName
                , conn
                );
        }
        public string MergedTriggerTableLoadSql(
            string tempMergedName
            , bool tempInsertedName
            , bool tempDeletedName
            , int spacesToIndent = 0
            )
        {
            return
                Delegee.MergedTriggerTableLoadSql(
                    tempMergedName
                    , tempInsertedName
                    , tempDeletedName
                    , spacesToIndent
                    );
        }
        public void InterceptUpdateOfImmutableColumn(
            string tempMergedTableName
            , IConnection conn
            )
        {
            Delegee.InterceptUpdateOfImmutableColumn(
                tempMergedTableName
                , conn
                );
        }
        public void InterceptUpdateOfImmutableColumn(
            string tempInsertedName
            , string tempDeletedName
            , IConnection conn
            )
        {
            Delegee.InterceptUpdateOfImmutableColumn(
                tempInsertedName
                , tempDeletedName
                , conn
                );
        }
        public string UpdateFromInsertedTriggerTableSql(
            string tempTableName
            , string currentTimestampSql
            , string userSql
            , int spacesToIndent = 0
            )
        {
            return
                Delegee.UpdateFromInsertedTriggerTableSql(
                    tempTableName
                    , currentTimestampSql
                    , userSql
                    , spacesToIndent
                    );
        }
        public void CascadeUpdate(
            ICascadingForeignKeyRelationship relationship
            , string tempMergedName
            , IConnection conn
            )
        {
            Delegee.CascadeUpdate(
                relationship
                , tempMergedName
                , conn
                );
        }
        public void CascadeUpdate(
            ICascadingForeignKeyRelationship relationship
            , string tempInsertedName
            , string tempDeletedName
            , IConnection conn
            )
        {
            Delegee.CascadeUpdate(
                relationship
                , tempInsertedName
                , tempDeletedName
                , conn
                );
        }
        public void LoadChangeTableAfterUpdate(
            IChangeTrackingTable changeTable
            , string updateDtmSql
            , string updaterSql
            , string tempMergedTableName
            , IConnection conn
            )
        {
            Delegee.LoadChangeTableAfterUpdate(
                changeTable
                , updateDtmSql
                , updaterSql
                , tempMergedTableName
                , conn
                );
        }
        public void LoadChangeTableAfterUpdate(
            IChangeTrackingTable changeTable
            , string updateDtmSql
            , string updaterSql
            , string tempInsertedName
            , string tempDeletedName
            , IConnection conn
            )
        {
            Delegee.LoadChangeTableAfterUpdate(
                changeTable
                , updateDtmSql
                , updaterSql
                , tempInsertedName
                , tempDeletedName
                , conn
                );
        }
        // Support for before-delete triggers
        public string DeleteFromDeletedTriggerTableSql(
            string tempTableName
            , int spacesToIndent = 0
            )
        {
            return
                Delegee.DeleteFromDeletedTriggerTableSql(
                    tempTableName
                    , spacesToIndent
                    );
        }
        public void CascadeDelete(
            ICascadingForeignKeyRelationship relationship
            , string tempDeletedName
            , IConnection conn
            )
        {
            Delegee.CascadeDelete(
                relationship
                , tempDeletedName
                , conn
                );
        }
        public void LoadChangeTableAfterDelete(
            IChangeTrackingTable changeTable
            , string updateDtmSql
            , string updaterSql
            , string tempDeletedName
            , IConnection conn
            )
        {
            Delegee.LoadChangeTableAfterDelete(
                changeTable
                , updateDtmSql
                , updaterSql
                , tempDeletedName
                , conn
                );
        }

    }

}
