﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface IPotentiallySingleColumnConstraint : IConstraint
    {
        ITableColumn ConstrainedColumn { get; }
        ITableColumn GetConstrainedColumnSafely();
        IPotentiallySingleColumnConstraint GetReferenceForColumn(ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true);
        IPotentiallySingleColumnConstraint GetCopyForColumn(ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true);
        IPotentiallySingleColumnConstraint GetSafeReferenceForColumn(ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true);
        IPotentiallySingleColumnConstraint GetReferenceForTable(ITable table, ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true);
        IPotentiallySingleColumnConstraint GetCopyForTable(ITable table, ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true);
        IPotentiallySingleColumnConstraint GetSafeReferenceForTable(ITable table, ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true);
    }

    public interface IPotentiallySingleColumnConstraintMutable : IPotentiallySingleColumnConstraint, IConstraintMutable
    {
        void SetConstrainedColumn(ITableColumn value, bool? retrieveConstrainedColumnIDFromDatabase = default(bool?), bool assert = true);
    }

    /// <summary>
    /// Boilerplate implementation of IPotentiallySingleColumnConstraintMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides.</item>
    ///   <list type="bullet">
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>ParentObjectMustBeOwner, if true rather than false.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public abstract class PotentiallySingleColumnConstraintMutable : ConstraintMutable, IPotentiallySingleColumnConstraintMutable
    {

        private ITableColumn _constrainedColumn;

        // Construction

        public PotentiallySingleColumnConstraintMutable() : base() { }

        public PotentiallySingleColumnConstraintMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , IObjectType objectType = null 
            , IObjectType physicalObjectType = null
            , ITableColumn constrainedColumn = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name != null ? name : ""
                , sqlTranslator != null ? sqlTranslator
                : table != null && table.SqlTranslator != null ? table.SqlTranslator
                : constrainedColumn != null && constrainedColumn.SqlTranslator != null ? constrainedColumn.SqlTranslator
                : null
                , table != null ? table
                : constrainedColumn != null && typeof(ITable).IsAssignableFrom(constrainedColumn.Vable.GetType())? (ITable)constrainedColumn.Vable
                : null
                , objectType
                , physicalObjectType
                , owner: constrainedColumn != null ? (IDataProcObject)constrainedColumn : (IDataProcObject)table
                , nameWhenPrefixed: nameWhenPrefixed
                , id: id
                )
        {
            ConstructNewMembers(constrainedColumn, true);
            CompleteConstructionIfType(typeof(PotentiallySingleColumnConstraintMutable), retrieveIDsFromDatabase, assert);
        }

        public PotentiallySingleColumnConstraintMutable(
            string name
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , ITableColumn constrainedColumn = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name != null ? name : ""
                // In the next bit, we force it to go through the constrained column's
                //   table to get to the SQL translator, which ensures that
                //   the constrained column has to HAVE a table properly set in
                //   the first place.
                , constrainedColumn != null
                && typeof(ITable).IsAssignableFrom(constrainedColumn.Vable.GetType()) 
                && constrainedColumn.Vable.SqlTranslator != null ? constrainedColumn.Vable.SqlTranslator
                : null
                , constrainedColumn != null && typeof(ITable).IsAssignableFrom(constrainedColumn.Vable.GetType()) ? (ITable)constrainedColumn.Vable
                : null
                , objectType
                , physicalObjectType
                , owner: constrainedColumn
                , nameWhenPrefixed: nameWhenPrefixed
                , id: id
                )
        {
            ConstructNewMembers(constrainedColumn, true);
            CompleteConstructionIfType(typeof(PotentiallySingleColumnConstraintMutable), retrieveIDsFromDatabase, assert);
        }

        // Object method overrides and overridden GetHasCode assistants

        public override int GetHashCode()
        {
            int p1 = NextPrimes[base.HighestPrimeUsedInHashCode];
            return base.GetHashCode()
                + p1 * (ConstrainedColumn == null ? 0 : ConstrainedColumn.GetHashCode());
        }

        public override int HighestPrimeUsedInHashCode
        {
            get
            {
                return NextPrimes[base.HighestPrimeUsedInHashCode];
            }
        }

        // IDataProcObject method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (IsFullyConstructed)
            {
                Debug.Assert(base.PassesSelfAssertion());
                if (ConstrainedColumn != null)
                {
                    Debug.Assert(
                        Owner == ConstrainedColumn
                        , "When a single-column constraint has a constrained column, "
                        + "the constrained column must be its owner."
                        );
                    // Do not assert ConstrainedColumn, in order to avoid infinite self-assertion loop
                    if (ConstrainedColumn.ParentObject != null)
                        Debug.Assert(
                            ConstrainedColumn.ParentObject.Equals(ParentObject)
                            , "If the constrained column of a single-column constraint "
                            + "has a non-null parent object, the constraint "
                            + "must have that same parent object."
                            );
                }
                if (ParentObject != null)
                {
                    if (ConstrainedColumn == null)
                    {
                        Debug.Assert(
                            Owner == ParentObject
                            , "When a single-column constraint has no constrained column, "
                            + "the parent table must be its owner."
                            );
                    }
                }
                if (ConstrainedColumn == null && ParentObject == null)
                {
                    Debug.Assert(
                        Owner == null
                        , "When a single-column constraint has neither a constrained column "
                        + "nor a parent table, it cannot have an owner."
                        );
                }
            }
            return true;
        }

        public override bool MembersMatch(object obj)
        {
            if (!base.MembersMatch(obj)) return false;
            if (!typeof(IPotentiallySingleColumnConstraint).IsAssignableFrom(obj.GetType())) return false;
            IPotentiallySingleColumnConstraint other = (IPotentiallySingleColumnConstraint)obj;
            if (!EqualityUtilities.EqualOrBothNull(this.ConstrainedColumn, other.ConstrainedColumn)) return false;
            return true;
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , table: null
                , objectType: null
                , physicalObjectType: null
                , constrainedColumn: null
                , nameWhenPrefixed: null
                , id: default(int?)
                );
        }

        protected virtual void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , ITableColumn constrainedColumn = null
            , string nameWhenPrefixed = null
            , int? id = null
            )
        {
            base.ConstructAfterConstruction(
                name
                , sqlTranslator
                , table
                , objectType
                , physicalObjectType
                , owner: constrainedColumn != null ? (IDataProcObject)constrainedColumn : (IDataProcObject)table
                , nameWhenPrefixed: nameWhenPrefixed
                , id: id
                );
            ConstructNewMembers(
                constrainedColumn
                , false
                );
        }

        private void ConstructNewMembers(
            ITableColumn constrainedColumn
            , bool forceUpdates
            )
        {
            if (forceUpdates || ConstrainedColumn == null)
                SetConstrainedColumn(constrainedColumn, false, false);
        }

        // DatabaseObjectMutable method overrides

        public override List<Type> ValidOwnerTypesWhenNotParentObject
        {
            get { return new List<Type> { typeof(ITableColumn) }; }
        }

        public override void RetrieveIDsFromDatabase(bool assert = true)
        {
            if (Database != null)
                if (ConstrainedColumn != null)
                    if (typeof(ITableColumnMutable).IsAssignableFrom(ConstrainedColumn.GetType()))
                        ((ITableColumnMutable)ConstrainedColumn).RetrieveIDsFromDatabase(assert: false);
            base.RetrieveIDsFromDatabase(assert);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public override void SetOwner(IDataProcObject value, bool assert = true)
        {
            if (typeof(ITableColumn).IsAssignableFrom(value.GetType()))
                SetConstrainedColumn((ITableColumn)value, assert);
            else
                SetParentObject((IDatabaseObject)value, assert);
        }

        // ObjectInSchemaMutable method overrides

        public override bool ParentObjectMustBeOwner
        {
            get { return ConstrainedColumn == null; }
        }

        public override void SetParentObject(IDatabaseObject value, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            bool retrieveIDs
                = retrieveIDsFromDatabase != null && retrieveIDsFromDatabase.HasValue
                ? retrieveIDsFromDatabase.Value
                : ID != null;
            base.SetParentObject(value, false, false);
            if (ParentObjectMustBeOwner) SetOwner(value, false);
            if (retrieveIDs) RetrieveIDsFromDatabase(assert: false);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        // IPotentiallySingleColumnConstraint properties and methods

        public ITableColumn ConstrainedColumn { get { return _constrainedColumn; } set { SetConstrainedColumn(value); } }
        public ITableColumn GetConstrainedColumnSafely()
        {
            return (ITableColumn)(_constrainedColumn == null ? null : _constrainedColumn.GetSafeReference());
        }
        public virtual IPotentiallySingleColumnConstraint GetReferenceForColumn(ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            if (constrainedColumn == null)
            {
                SetConstrainedColumn(null, false, false);
                return this;
            }
            else
            {
                ITable table = Table;
                if (table == null && constrainedColumn.Vable != null)
                {
                    RejectIfInvalidParentObjectType(constrainedColumn.Vable);
                    return GetReferenceForTable(
                        (ITable)constrainedColumn.Vable
                        , constrainedColumn
                        , retrieveIDsFromDatabase
                        , assert
                        );
                }
                else if (table == null && constrainedColumn.Vable == null)
                    return GetReferenceForTable(
                        null
                        , constrainedColumn
                        , retrieveIDsFromDatabase
                        , assert
                        );
                else if (table != null && constrainedColumn.Vable == null)
                    return GetReferenceForTable(
                        table
                        , (ITableColumn)constrainedColumn.GetReferenceForVable(table)
                        , retrieveIDsFromDatabase
                        , assert
                        );
                else // (table != null && constrainedColumn.Vable == null)
                    return GetReferenceForTable(
                        table
                        , constrainedColumn
                        , retrieveIDsFromDatabase
                        , assert
                        );
            }
        }
        public virtual IPotentiallySingleColumnConstraint GetCopyForColumn(ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            if (constrainedColumn == null)
            {
                SetConstrainedColumn(null, false, false);
                return this;
            }
            else
            {
                ITable table = Table;
                if (table == null && constrainedColumn.Vable != null)
                {
                    RejectIfInvalidParentObjectType(constrainedColumn.Vable);
                    return GetCopyForTable(
                        (ITable)constrainedColumn.Vable
                        , constrainedColumn
                        , retrieveIDsFromDatabase
                        , assert
                        );
                }
                else if (table == null && constrainedColumn.Vable == null)
                    return GetCopyForTable(
                        null
                        , constrainedColumn
                        , retrieveIDsFromDatabase
                        , assert
                        );
                else if (table != null && constrainedColumn.Vable == null)
                    return GetCopyForTable(
                        table
                        , (ITableColumn)constrainedColumn.GetCopyForVable(table)
                        , retrieveIDsFromDatabase
                        , assert
                        );
                else // (table != null && constrainedColumn.Vable == null)
                    return GetCopyForTable(
                        table
                        , constrainedColumn
                        , retrieveIDsFromDatabase
                        , assert
                        );
            }
        }
        public virtual IPotentiallySingleColumnConstraint GetSafeReferenceForColumn(ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return
                (IPotentiallySingleColumnConstraint)
                GetCopyForColumn(constrainedColumn, retrieveIDsFromDatabase,assert)
                .GetSafeReference();
        }
        public virtual IPotentiallySingleColumnConstraint GetReferenceForTable(ITable table, ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            bool retrieveIDs
                = retrieveIDsFromDatabase != null && retrieveIDsFromDatabase.HasValue
                ? retrieveIDsFromDatabase.Value
                : ID != null;
            IPotentiallySingleColumnConstraintMutable retval = this;
            IVable constrainedColumnVable
                = constrainedColumn != null ? constrainedColumn.Vable : null;
            if (table == null && constrainedColumn != null && constrainedColumnVable != null)
            {
                RejectIfInvalidParentObjectType(constrainedColumnVable);
                table = (ITable)constrainedColumnVable;
            }
            if (table != null && constrainedColumnVable != null && !table.Equals(constrainedColumnVable))
                constrainedColumn
                    = (ITableColumn)constrainedColumn.GetReferenceForVable(table);
            if (table != null && !table.Equals(Table))
                retval
                    = (IPotentiallySingleColumnConstraintMutable)
                    GetReferenceForTable(table, retrieveIDs, false);
            retval.SetConstrainedColumn(constrainedColumn, retrieveIDs, assert);
            return retval;
        }
        public virtual IPotentiallySingleColumnConstraint GetCopyForTable(ITable table, ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            bool retrieveIDs
                = retrieveIDsFromDatabase != null && retrieveIDsFromDatabase.HasValue
                ? retrieveIDsFromDatabase.Value
                : ID != null;
            IPotentiallySingleColumnConstraintMutable retval = this;
            IVable constrainedColumnVable
                = constrainedColumn != null ? constrainedColumn.Vable : null;
            if (table == null && constrainedColumn != null && constrainedColumnVable != null)
            {
                RejectIfInvalidParentObjectType(constrainedColumnVable);
                table = (ITable)constrainedColumnVable;
            }
            if (table != null && constrainedColumnVable != null && !table.Equals(constrainedColumnVable))
                constrainedColumn
                    = (ITableColumn)constrainedColumn.GetCopyForVable(table);
            if (table != null && !table.Equals(Table))
                retval
                    = (IPotentiallySingleColumnConstraintMutable)
                    GetCopyForTable(table, retrieveIDs, false);
            retval.SetConstrainedColumn(constrainedColumn, retrieveIDs, assert);
            return retval;
        }
        public virtual IPotentiallySingleColumnConstraint GetSafeReferenceForTable(ITable table, ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return
                (IPotentiallySingleColumnConstraint)
                GetCopyForTable(table, constrainedColumn, retrieveIDsFromDatabase, assert)
                .GetSafeReference();
        }

        // IPotentiallySingleColumnConstraintMutable properties and methods

        public virtual void SetConstrainedColumn(ITableColumn value, bool? retrieveConstrainedColumnIDFromDatabase = default(bool?), bool assert = true)
        {
            if (value == null)
            {
                _constrainedColumn = null;
                SetOwner(ParentObject, false);
            }
            else
            {
                _constrainedColumn
                    = (ITableColumn)
                    value.GetReferenceForParentObject(ParentObject, retrieveConstrainedColumnIDFromDatabase, false);
                base.SetOwner(_constrainedColumn, false);
            }
            if (assert) Debug.Assert(PassesSelfAssertion());
        }
    }

    /// <summary>
    /// Boilerplate implementation of IPotentiallySingleColumnConstraint without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>PotentiallySingleColumnConstraintImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetCopyForColumn, always unless abstract class.</item>
    ///   <item>GetCopyForTable, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public abstract class PotentiallySingleColumnConstraintImmutable : ConstraintImmutable, IPotentiallySingleColumnConstraint
    {

        private IPotentiallySingleColumnConstraint Delegee { get { return (IPotentiallySingleColumnConstraint)_delegee; } }

        protected PotentiallySingleColumnConstraintImmutable() : base() { }

        public PotentiallySingleColumnConstraintImmutable(
            IPotentiallySingleColumnConstraint _delegee
            , bool assert = true
            ) : base( _delegee, false)
        {
            CompleteConstructionIfType(typeof(PotentiallySingleColumnConstraintImmutable), false, assert);
        }

        // IPotentiallySingleColumnConstraint properties
        public ITableColumn ConstrainedColumn { get { return Delegee.GetConstrainedColumnSafely(); } }
        public ITableColumn GetConstrainedColumnSafely() { return Delegee.GetConstrainedColumnSafely(); }
        public IPotentiallySingleColumnConstraint GetReferenceForColumn(ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return GetCopyForColumn(constrainedColumn, retrieveIDsFromDatabase, assert);
        }
        public abstract IPotentiallySingleColumnConstraint GetCopyForColumn(ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true);
        public IPotentiallySingleColumnConstraint GetSafeReferenceForColumn(ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return
                (IPotentiallySingleColumnConstraint)
                GetCopyForColumn(constrainedColumn, retrieveIDsFromDatabase, assert)
                .GetSafeReference();
        }
        public IPotentiallySingleColumnConstraint GetReferenceForTable(ITable table, ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return GetCopyForTable(table, constrainedColumn, retrieveIDsFromDatabase, assert);
        }
        public abstract IPotentiallySingleColumnConstraint GetCopyForTable(ITable table, ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true);
        public IPotentiallySingleColumnConstraint GetSafeReferenceForTable(ITable table, ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return
                (IPotentiallySingleColumnConstraint)
                GetCopyForTable(table, constrainedColumn, retrieveIDsFromDatabase, assert)
                .GetSafeReference();
        }

    }

}
