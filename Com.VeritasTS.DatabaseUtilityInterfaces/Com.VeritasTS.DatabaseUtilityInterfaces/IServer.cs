﻿using System;
using System.Diagnostics;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// This interface models a database server, at present trivially,
    /// as IServer's only have a Name and a Vendor. 
    /// </summary>
    /// <remarks>
    /// IServer is an an extension of both IComparableByName
    /// and ISafeMemberOfImmutableOwner.  
    /// </remarks>
    public interface IServer : IComparableByName, ISafeMemberOfImmutableOwner,  ISelfAsserter
    {

        /// <summary>
        /// The server's database vendor. 
        /// </summary>
        /// <returns>The database vendor</returns>
        IVendor Vendor { get; }

        /// <summary>
        /// Threadsafe reference to the server's database vendor
        /// or an equivalent IVendor object.
        /// </summary>
        /// <remarks>
        /// Protects invariant state of the IServer object.
        /// </remarks>
        /// <returns>Safe reference to the database vendor</returns>
        IVendor GetVendorSafely();

        /// <summary>
        /// True if the two servers' vendors are both null,
        /// or if they are both non-null and equal each other. 
        /// </summary>
        /// <returns>The truth of the statement, "The two servers'
        /// vendors match."</returns>
        bool VendorsMatch(IServer other);

    }


    /// <summary>
    /// This class implements the IServer interface with
    /// threadsafe invariance -- that is, once constructed,
    /// neither the object nor its member objects can be altered. 
    /// </summary>
    public class ServerImmutable : ComparableByNameImmutable, IServer
    {

        /***
         * Private members
         ***/

        private IVendor _vendor;

        /***
         * Constructors
         ***/

        /// <summary>
        /// Hides default constructor.
        /// </summary>
        private ServerImmutable() : base("") { }

        /// <summary>
        /// Construct from name and vendor.
        /// </summary>
        /// <param name="name">The name of the server -- that is,
        /// the database server notionally, not necessarily
        /// the network name of the physical server. This should
        /// never be null, which is enforced in development
        /// builds by assertion.</param>
        /// <param name="vendor">The RDBMS vendor. This should
        /// never be null, nor should vendor.Name. 
        /// Both conditions are enforced in development
        /// builds by assertion.</param>
        public ServerImmutable(string name, IVendor vendor) : base(name)
        {
            if (vendor != null)
                _vendor = (IVendor)(vendor.GetSafeReference());
            Debug.Assert(PassesSelfAssertion());
        }

        /***
         * ISafeMemberOfImmutableOwner methods
         ***/

        /// <summary>
        /// Returns either a safe reference to itself, or
        /// else a safe reference to a valid copy of itself.
        /// </summary>
        /// <remarks>
        /// The reference returned must be to an object that 
        /// always honors the contract specified for GetSafeReference()
        /// by the ISafeMemberOfImmutableOwner interface.
        /// </remarks>
        /// <returns>Either a safe reference to itself, or
        /// else a safe reference to a valid copy of itself</returns>
        public ISafeMemberOfImmutableOwner GetSafeReference() { return this; }

        /***
         * ISelfAsserter methods
         ***/

        /// <summary>
        /// Asserts the class invariants for
        /// this class.
        /// </summary>
        /// <remarks>
        /// Merely passes on the call to the vendor
        /// object -- as long as the vendor object
        /// is either null or in a sane state, 
        /// we are good.</remarks>
        /// <returns>True if it passes, else false or thrown exception.</returns>
        public bool PassesSelfAssertion()
        {
            if (_vendor != null) return _vendor.PassesSelfAssertion();
            else return false;
        }

        /***
         * IServer methods
         ***/

        /// <summary>
        /// The database vendor for this database server.
        /// </summary>
        /// <returns>The vendor</returns>
        public IVendor Vendor { get { return GetVendorSafely(); } }

        /// <summary>
        /// Threadsafe reference to the server's database vendor
        /// or an equivalent IVendor object.
        /// </summary>
        /// <remarks>
        /// Protects invariant state of the IServer object.
        /// </remarks>
        /// <returns>Safe reference to the database vendor</returns>
        public IVendor GetVendorSafely()
        {
            if (_vendor != null) return (IVendor)(_vendor.GetSafeReference());
            else return null;
        }

        /// <summary>
        /// True if the two servers' vendors are both null,
        /// or if they are both non-null and equal each other. 
        /// </summary>
        /// <returns>The truth of the statement, "The two servers'
        /// vendors match."</returns>
        public bool VendorsMatch(IServer other)
        {
            if (_vendor != null) return _vendor.Equals(other.Vendor);
            else return (other.Vendor == null);
        }

        /// <summary>
        /// Standard Equals method, overridden to be false
        /// if the other object is not an object of the same
        /// class as this or if the either the vendors or names
        /// are not equal.
        /// </summary>
        /// <returns>Truth of statement "other object is an IServer
        /// equal to this in class, name and vendor."</returns>
        public override bool Equals(Object o)
        {
            if (o == null) return false;
            else if (!o.GetType().Equals(GetType())) return false;
            else
            {
                IServer other = (IServer)o;
                if (!VendorsMatch(other)) return false;
                else return EqualsInName(other);
            }
        }

        /// <summary>
        /// Standard GetHashCode method, overridden to be the Name's hash
        /// code if Name is not null and 0 if it is.
        /// </summary>
        /// <returns>The hash code</returns>
        public override int GetHashCode()
        {
            if (Name == null) return 0;
            else return Name.GetHashCode();
        }

    }

}
