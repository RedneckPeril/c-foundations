﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface IInputterColumn : ITableColumn
    {
    }
    public interface IInputterColumnMutable : IInputterColumn, ITableColumnMutable
    {
    }

    /// <summary>
    /// Boilerplate implementation of ITableColumnMutable.
    /// </summary>
    /// <remarks>
    /// Sealed class for now.
    /// <remarks>
    public sealed class InputterColumnMutable : TableColumnMutable, IInputterColumnMutable
    {

        public InputterColumnMutable(
            string name
            , ISqlTranslator sqlTranslator
            , ITable parentObject 
            , string nameWhenPrefixed = null
            , string constraintTag = null
            , int? id = null
            , bool retrieveIDsFromDatabase = false
            , bool assert = true
            )
            : base(
                  (name == null ? "inputter" : name)
                  , "nvarchar(255)"
                  , sqlTranslator: sqlTranslator
                  , parentObject: parentObject
                  , isNullable: false
                  , isImmutable: true
                  , nameWhenPrefixed: nameWhenPrefixed
                  , constraintTag: constraintTag
                  , id: id
                  )
        {
            CompleteConstructionIfType(
                typeof(InputterColumnMutable)
                , retrieveIDsFromDatabase
                , assert
                );
        }

        // DataProcObjectMutable method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    "nvarchar(255)".Equals(DataType)
                    , "A InputterColumn must be typed as nvarchar(255), not "
                    + (DataType == null ? "[null]" : DataType)
                    + "."
                    );
                Debug.Assert(
                    !IsIdentity
                    , "A InputterColumn cannot be an identity column."
                    );
                Debug.Assert(
                    !IsNullable
                    , "A InputterColumn cannot be nullable."
                    );
                Debug.Assert(
                    IsImmutable
                    , "A InputterColumn must be invariant."
                    );
                Debug.Assert(
                    CalcFormula == null
                    , "A InputterColumn cannot be a computed column "
                    + "and therefore cannot have a computed definition."
                    );
                Debug.Assert(
                    ParentObject == null || CheckConstraints.Count > 0
                    , "If an InputterColumn has a non-null parent object, "
                    + "it must have at least one check constraint."
                    );
            }
            return true;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new InputterColumnMutable(
                Name
                , GetSqlTranslatorSafely()
                , GetTableSafely()
                , NameWhenPrefixed
                , ConstraintTag
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        // ObjectInSchemaMutable method overrides

        public override IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return base.GetCopyForParentObject(parentObject, retrieveIDsFromDatabase, assert);
        }

    }

    /// <summary>
    /// Boilerplate implementation of ITableColumn without variability.
    /// </summary>
    /// <remarks>
    /// Sealed class for now.
    /// <remarks>
    public sealed class InputterColumnImmutable : TableColumnImmutable, IInputterColumn
    {

        private IInputterColumn Delegee { get { return (IInputterColumn)_delegee; } }

        public InputterColumnImmutable(
            string name
            , ISqlTranslator sqlTranslator
            , ITable parentObject
            , string nameWhenPrefixed = null
            , string constraintTag = null
            , int? id = null
            , bool retrieveIDsFromDatabase = false
            , bool assert = true
            )
            : base(delegee: null)
        {
            _delegee 
                = new InputterColumnMutable(
                    name
                    , sqlTranslator
                    , parentObject
                    , nameWhenPrefixed
                    , constraintTag
                    , id
                    , retrieveIDsFromDatabase
                    , assert: false
                    );
            CompleteConstructionIfType(typeof(InputterColumnImmutable), retrieveIDsFromDatabase: false, assert: assert);
        }

        public InputterColumnImmutable(IInputterColumn delegee, bool assert)
            : base()
        {
            _delegee = delegee;
            Debug.Assert(PassesSelfAssertion(typeof(InputterColumnImmutable)));
        }

        // DataProcObject method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(IInputterColumn); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new InputterColumnImmutable(
                (IInputterColumn)
                Delegee.GetReferenceForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , assert: false
                    )
                , assert: assert
                );
        }

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new InputterColumnImmutable(
                (IInputterColumn)
                Delegee.GetCopyForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , assert: false
                    )
                , assert: assert
                );
        }

    }

}