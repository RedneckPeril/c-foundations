﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface IIndex : IChildOfVable
    {
        List<string> ColumnNames { get; }
        bool IsUnique { get; }
        bool IsClustered { get; }
        bool IsConstraint { get; }
        List<string> GetColumnNamesSafely();
    }

    public interface IIndexMutable : IIndex, IChildOfVableMutable
    {
        void SetColumnNames(List<string> value, bool assert = true);
        void AddOrOverwriteColumnName(string name, bool assert = true);
        bool RemoveColumnName(string name, bool assert = true);
        void SetIsUnique(bool value, bool assert = true);
        void SetIsClustered(bool value, bool assert = true);
        //void DropIndex();
    }

    /// <summary>
    /// Boilerplate implementation of IIndexMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides.</item>
    ///   <list type="bullet">
    ///   <item>RetrieveIDsFromDatabase, if new IDatabaseObject members have been
    /// added as members.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaMutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidParentObjectTypes, if narrower than IVable.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   <item>SetParentObject, rarely, e.g. when ParentObject and Owner should always
    ///   equal not only each other but some third member as well.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class IndexMutable : ChildOfVableMutable, IIndexMutable
    {
        
        private List<string> _columnNames;
        private bool _isUnique;
        private bool _isClustered;

        protected IndexMutable() : base() { }

        public IndexMutable(
            string name
            , ISqlTranslator sqlTranslator
            , IVable vable
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , IDataProcObject owner = null
            , List<string> columnNames = null
            , bool isUnique = false
            , bool isClustered = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , sqlTranslator
                , vable
                , objectType == null ? ObjectTypes.Index() : objectType
                , physicalObjectType == null ? ObjectTypes.Index() : physicalObjectType
                , owner: vable
                , nameWhenPrefixed: nameWhenPrefixed
                , id: id
                )
        {
            ConstructNewMembers(
                columnNames
                , isUnique
                , isClustered
                , true
                );
            CompleteConstructionIfType(typeof(IndexMutable), retrieveIDsFromDatabase, assert);
        }

        // Object method overrides and overridden GetHasCode assistants

        public override int GetHashCode()
        {
            return base.GetHashCode()
                + NextPrimes[base.HighestPrimeUsedInHashCode] * ColumnNames.GetHashCode();
        }

        public override int HighestPrimeUsedInHashCode
        {
            get { return NextPrimes[base.HighestPrimeUsedInHashCode]; }
        }

        // DataProcObjectMutable method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    Name != null
                    , "The name of an IIndex cannot be null."
                    );
                Debug.Assert(
                    ObjectType != null
                    , "The ObjectType property of an IIndex object "
                    + "cannot be null."
                    );
                Debug.Assert(
                    ObjectType == ObjectTypes.Index()
                    || ObjectType == ObjectTypes.UniqueKey()
                    || ObjectType == ObjectTypes.PrimaryKey()
                    , "The ObjectType property of an IIndex object "
                    + "must be either PrimaryKey or UniqueKey."
                    );
                Debug.Assert(
                    PhysicalObjectType != null
                    , "The PhysicalObjectType property of an IIndex object "
                    + "cannot be null."
                    );
                Debug.Assert(
                    PhysicalObjectType == ObjectTypes.Index()
                    || PhysicalObjectType == ObjectTypes.UniqueKey()
                    || PhysicalObjectType == ObjectTypes.PrimaryKey()
                    , "The PhysicalObjectType property of an IIndex object "
                    + "must be either PrimaryKey or UniqueKey."
                    );
                Debug.Assert(
                    ColumnNames != null
                    , "ColumnNames property of IIndex object "
                    + (Name == null ? "" : "'" + Name + "' ")
                    + "must never be null."
                    );
                Debug.Assert(
                    ColumnNames.Count > 0
                    , "ColumnNames property of IIndex object "
                    + (Name == null ? "" : "'" + Name + "' ")
                    + "must hold at least one column name."
                    );
                foreach (string name in ColumnNames)
                {
                    Debug.Assert(
                        name != null
                        , "All column names for IIndex object "
                        + (Name == null ? "" : "'" + Name + "' ")
                        + "must be non-null."
                        );
                    Debug.Assert(
                        HasNonWhitespace(name)
                        , "No column name for IIndex object "
                        + (Name == null ? "" : "'" + Name + "' ")
                        + "may consist of whitespace only."
                        );
                }
                if (Database != null)
                {
                    string databaseDescription = DatabaseDescription;
                    foreach (string name in ColumnNames)
                        Debug.Assert(
                            Database.IsValidObjectName(name)
                            , "Column name "
                            + (name == null ? "[null]" : "'" + name + "'")
                            + " is not a valid system name in "
                            + databaseDescription
                            + "."
                            );
                }
                if (ParentObject != null)
                {
                    foreach (string name in ColumnNames)
                        Debug.Assert(
                            ((IVable)ParentObject).Columns.ContainsKey("name")
                            , "IIndex '" + Name + "' "
                            + "references a column named '" + name + "', which "
                            + "does not exist on the parent vable."
                            );
                }
            }
            return true;
        }

        public override bool MembersMatch(object obj)
        {
            if (!base.MembersMatch(obj)) return false;
            if (!typeof(IIndex).IsAssignableFrom(obj.GetType())) return false;
            IIndex other = (IIndex)obj;
            if (!EqualityUtilities.EqualOrBothNull(this.ColumnNames, other.ColumnNames)) return false;
            if (!EqualityUtilities.EqualOrBothNull(this.IsUnique, other.IsUnique)) return false;
            if (!EqualityUtilities.EqualOrBothNull(this.IsClustered, other.IsClustered)) return false;
            return true;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new IndexMutable(
                Name
                , GetSqlTranslatorSafely()
                , (IVable)GetParentObjectSafely()
                , GetObjectTypeSafely()
                , GetPhysicalObjectTypeSafely()
                , GetOwnerSafely()
                , GetColumnNamesSafely()
                , IsUnique
                , IsClustered
                , NameWhenPrefixed
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , vable: null
                , objectType: null
                , physicalObjectType: null
                , columnNames: null
                , isUnique: false
                , isClustered: false
                , nameWhenPrefixed: null
                , id: default(int?)
                );
        }

        protected virtual void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator
            , IVable vable
            , IObjectType objectType
            , IObjectType physicalObjectType
            , List<string> columnNames
            , bool isUnique
            , bool isClustered
            , string nameWhenPrefixed
            , int? id
            )
        {
            base.ConstructAfterConstruction(
                name
                , sqlTranslator
                , vable
                , objectType == null ? ObjectTypes.Index() : objectType
                , physicalObjectType == null ? ObjectTypes.Index() : physicalObjectType
                , vable
                , nameWhenPrefixed
                , id
                );
            ConstructNewMembers(
                columnNames 
                , isUnique
                , isClustered
                , false
                );
        }

        private void ConstructNewMembers(
            List<string> columnNames
            , bool isUnique
            , bool isClustered
            , bool forceUpdates
            )
        {
            if (forceUpdates || ColumnNames == null)
                SetColumnNames(columnNames, assert: false);
            if (forceUpdates || !IsUnique)
                SetIsUnique(isUnique, assert: false);
            if (forceUpdates || !IsClustered)
                SetIsClustered(isClustered, assert: false);
        }

        // ObjectInSchemaMutable method overrides

        public override bool ParentObjectMustBeOwner { get { return true; } }

        public override IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            RejectIfInvalidParentObjectType(parentObject);
            bool retrieveIDs
                = retrieveIDsFromDatabase != null & retrieveIDsFromDatabase.HasValue
                ? retrieveIDsFromDatabase.HasValue
                : true;
            IIndexMutable retval
                = new IndexMutable(
                    Name
                    , SqlTranslator
                    , (IVable)parentObject
                    , ObjectType
                    , PhysicalObjectType
                    , Owner
                    , ColumnNames
                    , IsUnique
                    , IsClustered
                    , NameWhenPrefixed
                    , ID
                    , retrieveIDsFromDatabase: retrieveIDs
                    , assert: false
                );
            return retval;
        }

        // IIndex methods and properties

        public List<string> ColumnNames
        {
            get { return _columnNames; }
            set { SetColumnNames(value); }
        }

        public bool IsUnique
        {
            get { return _isUnique; }
            set { SetIsUnique(value); }
        }

        public bool IsClustered
        {
            get { return _isClustered; }
            set { SetIsClustered(value); }
        }

        public bool IsConstraint
        {
            get { return typeof(IConstraint).IsAssignableFrom(this.GetType()); }
        }

        public List<string> GetColumnNamesSafely()
        {
            return _columnNames.ToList();
        }

        // IIndexMutable methods and properties

        public virtual void SetColumnNames(List<string> value, bool assert = true)
        {
            if (value == null) _columnNames = new List<string>();
            else _columnNames = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void AddOrOverwriteColumnName(string name, bool assert = true)
        {
            if (Database != null)
            Debug.Assert(
                Database.IsValidObjectName(name)
                , (name == null ? "[null]" : "'" + name + "'" )
                + " is not a valid database object name for "
                + DatabaseDescription
                + "."
                );
            if (!_columnNames.Contains(name))
                _columnNames.Add(name);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual bool RemoveColumnName(string name, bool assert = true)
        {
            bool retval = _columnNames.Remove(name);
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }

        public virtual void SetIsUnique(bool value, bool assert = true)
        {
            _isUnique = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void SetIsClustered(bool value, bool assert = true)
        {
            _isClustered = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

    }

    /// <summary>
    /// Boilerplate implementation of IIndex without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public class IndexImmutable : ChildOfVableImmutable, IIndex
    {

        private IIndex Delegee { get { return (IIndex)_delegee; } }

        public IndexImmutable(
            string name
            , ISqlTranslator sqlTranslator
            , IVable parentObject
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , IDataProcObject owner = null
            , List<string> columnNames = null
            , bool isUnique = false
            , bool isClustered = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
        {
            _delegee 
                = new IndexMutable(
                    name
                    , sqlTranslator
                    , parentObject
                    , objectType
                    , physicalObjectType
                    , owner
                    , columnNames
                    , isUnique
                    , isClustered
                    , NameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , assert: false
                    );
            CompleteConstructionIfType(
                typeof(IndexImmutable)
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        public IndexImmutable( IIndex delegee )
        {
            _delegee = delegee;
            CompleteConstructionIfType(typeof(IndexImmutable), retrieveIDsFromDatabase: false);
        }

        // IDataProcObject method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(IIndex); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return
                new IndexImmutable(
                    (IIndex)
                    Delegee.GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, false)
                    );
        }

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return 
                new IndexImmutable(
                    (IIndex)
                    Delegee.GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, false)
                    );
        }

        // IIndex properties

        public List<string> ColumnNames { get { return Delegee.GetColumnNamesSafely(); } }

        public bool IsUnique { get { return ((IIndex)_delegee).IsUnique; } }

        public bool IsClustered { get { return ((IIndex)_delegee).IsClustered; } }

        public bool IsConstraint
        {
            get { return typeof(IConstraint).IsAssignableFrom(this.GetType()); }
        }

        public List<string> GetColumnNamesSafely()
        {
            return Delegee.GetColumnNamesSafely();
        }

    }

}
