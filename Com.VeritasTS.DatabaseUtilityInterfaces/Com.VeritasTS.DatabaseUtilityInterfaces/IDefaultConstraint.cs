﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface IDefaultConstraint : ISingleColumnConstraint, IConstraintWithText
    {
        string DefaultValueText { get; }
    }

    public interface IDefaultConstraintMutable : IDefaultConstraint, ISingleColumnConstraintMutable, IConstraintWithTextMutable
    {
        void SetDefaultValueText(string value, bool assert = true);
    }


    /// <summary>
    /// Boilerplate implementation of IDefaultConstraintMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides.</item>
    ///   <list type="bullet">
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>ParentObjectMustBeOwner, if true rather than false.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class DefaultConstraintMutable : SingleColumnConstraintMutable, IDefaultConstraintMutable
    {

        private string _defaultValueText;
        private string FullTypeNameForCode { get { return typeof(DefaultConstraintMutable).FullName; } }

        public DefaultConstraintMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , ITableColumn constrainedColumn = null
            , string defaultValueText = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , sqlTranslator
                , table
                , ObjectTypes.DefaultConstraint()
                , ObjectTypes.DefaultConstraint()
                , constrainedColumn
                , nameWhenPrefixed
                , id: id
                )
        {
            ConstructNewMembers(name, defaultValueText, true);
            CompleteConstructionIfType(typeof(DefaultConstraintMutable), retrieveIDsFromDatabase, assert: true);
        }

        public DefaultConstraintMutable(
            string name
            , ITableColumn constrainedColumn = null
            , string defaultValueText = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , ObjectTypes.DefaultConstraint()
                , ObjectTypes.DefaultConstraint()
                , constrainedColumn
                , nameWhenPrefixed
                , id: id
                )
        {
            ConstructNewMembers(name, defaultValueText, true);
            CompleteConstructionIfType(typeof(DefaultConstraintMutable), retrieveIDsFromDatabase, assert: true);
        }

        public DefaultConstraintMutable(
            ITableColumn constrainedColumn = null
            , string defaultValueText = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                ""
                , ObjectTypes.DefaultConstraint()
                , ObjectTypes.DefaultConstraint()
                , constrainedColumn
                , nameWhenPrefixed
                , id: id
                )
        {
            ConstructNewMembers(null, defaultValueText, true);
            CompleteConstructionIfType(typeof(DefaultConstraintMutable), retrieveIDsFromDatabase, assert: true);
        }

        // Object method overrides and overridden GetHasCode assistants

        public override int GetHashCode()
        {
            int p1 = NextPrimes[base.HighestPrimeUsedInHashCode];
            return base.GetHashCode()
                + p1 * (DefaultValueText == null ? 0 : DefaultValueText.GetHashCode());
        }

        public override int HighestPrimeUsedInHashCode
        {
            get
            {
                return NextPrimes[base.HighestPrimeUsedInHashCode];
            }
        }

        // IDataProcObject method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (IsFullyConstructed)
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    ConstrainedColumn != null
                    , "A default constraint's constrained column "
                    + "cannot be null."
                    );
                Debug.Assert(
                    DefaultValueText != null
                    , "An IDefaultConstraint object cannot have null default value text."
                    );
                Debug.Assert(
                    HasNonWhitespace(DefaultValueText)
                    , "An IDefaultConstraint object's default value text "
                    + "must contain something other than whitespace."
                    );
                Debug.Assert(
                    DefaultValueText.Equals(ConstraintText)
                    , "An IDefaultConstraint object's default value text "
                    + "must be the same thing as its ConstraintText."
                    );
                if (ObjectType != null)
                    Debug.Assert(
                        ObjectType.Equals(ObjectTypes.DefaultConstraint())
                        , "An IDefaultConstraint object must be a default constraint."
                        );
                if (PhysicalObjectType != null)
                    Debug.Assert(
                        PhysicalObjectType.Equals(ObjectTypes.CheckConstraint())
                        || PhysicalObjectType.Equals(ObjectTypes.DefaultConstraint())
                        , "An IDefaultConstraint object's PhysicalObjectType property "
                        + "must be ObjectTypes.DefaultConstraint()."
                        );
            }
            return true;
        }
        
        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new DefaultConstraintMutable(
                Name
                , GetSqlTranslatorSafely()
                , (ITable)GetParentObjectSafely()
                , GetConstrainedColumnSafely()
                , DefaultValueText
                , NameWhenPrefixed
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , table: null
                , constrainedColumn: null
                , defaultValueText: null
                , nameWhenPrefixed: null
                , id: default(int?)
                );
        }

        protected virtual void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator
            , ITable table
            , ITableColumn constrainedColumn
            , string defaultValueText
            , string nameWhenPrefixed
            , int? id
            )
        {
            base.ConstructAfterConstruction(
                name
                , sqlTranslator
                , table
                , ObjectTypes.DefaultConstraint()
                , ObjectTypes.DefaultConstraint()
                , constrainedColumn
                , nameWhenPrefixed: nameWhenPrefixed
                , id: id
                );
        }

        private void ConstructNewMembers(
            string name
            , string defaultValueText
            , bool forceUpdates
            )
        {
            if (Name == null) SetName(name, false);
            SetDefaultValueText(defaultValueText, false);
        }

        // ObjectInSchemaMutable method overrides

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new DefaultConstraintMutable(
                Name
                , SqlTranslator
                , parentObject == null ? null : (ITable)parentObject.GetSafeReference()
                , ConstrainedColumn
                , DefaultValueText
                , NameWhenPrefixed
                , ID
                , retrieveIDsFromDatabase
                , assert
                );
        }

        // ISingleColumnConstraint method overrides

        public override IPotentiallySingleColumnConstraint GetReferenceForColumn(
            ITableColumn constrainedColumn
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            if (constrainedColumn == null)
                throw new NullArgumentException(
                    "constrainedColumn"
                    , typeof(DefaultConstraintMutable).FullName
                    + ".GetReferenceForColumn(constrainedColumn, retrieveIDsFromDatabase)"
                    );
            return base.GetReferenceForColumn(constrainedColumn, retrieveIDsFromDatabase);
        }
        public override IPotentiallySingleColumnConstraint GetCopyForColumn(
            ITableColumn constrainedColumn
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            if (constrainedColumn == null)
                throw new NullArgumentException(
                    "constrainedColumn"
                    , typeof(DefaultConstraintMutable).FullName
                    + ".GetCopyForColumn(constrainedColumn, retrieveIDsFromDatabase)"
                    );
            return base.GetCopyForColumn(constrainedColumn, retrieveIDsFromDatabase);
        }
        public override IPotentiallySingleColumnConstraint GetSafeReferenceForColumn(
            ITableColumn constrainedColumn
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            if (constrainedColumn == null)
                throw new NullArgumentException(
                    "constrainedColumn"
                    , typeof(DefaultConstraintMutable).FullName
                    + ".GetSafeReferenceForColumn(constrainedColumn, retrieveIDsFromDatabase)"
                    );
            return base.GetSafeReferenceForColumn(constrainedColumn, retrieveIDsFromDatabase);
        }
        public override IPotentiallySingleColumnConstraint GetReferenceForTable(
            ITable table
            , ITableColumn constrainedColumn
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            if (constrainedColumn == null)
                throw new NullArgumentException(
                    "constrainedColumn"
                    , typeof(DefaultConstraintMutable).FullName
                    + ".GetReferenceForTable(table, constrainedColumn, retrieveIDsFromDatabase)"
                    );
            return base.GetReferenceForTable(table, constrainedColumn, retrieveIDsFromDatabase);
        }
        public override IPotentiallySingleColumnConstraint GetCopyForTable(
            ITable table
            , ITableColumn constrainedColumn
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            if (constrainedColumn == null)
                throw new NullArgumentException(
                    "constrainedColumn"
                    , typeof(DefaultConstraintMutable).FullName
                    + ".GetCopyForTable(table, constrainedColumn, retrieveIDsFromDatabase)"
                    );
            return base.GetCopyForTable(table, constrainedColumn, retrieveIDsFromDatabase);
        }
        public override IPotentiallySingleColumnConstraint GetSafeReferenceForTable(
            ITable table
            , ITableColumn constrainedColumn
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            if (constrainedColumn == null)
                throw new NullArgumentException(
                    "constrainedColumn"
                    , typeof(DefaultConstraintMutable).FullName
                    + ".GetSafeReferenceForTable(table, constrainedColumn, retrieveIDsFromDatabase)"
                    );
            return base.GetSafeReferenceForTable(table, constrainedColumn, retrieveIDsFromDatabase);
        }

        // IConstraintWithText properties 

        public virtual string ConstraintText { get { return DefaultValueText; } }

        // IConstraintWithTextMutable properties 

        public virtual void SetConstraintText(string value, bool assert = true)
        {
            SetDefaultValueText(value, assert);
        }

        // IDefaultConstraint properties 

        public virtual string DefaultValueText { get { return _defaultValueText; } }

        // IConstraintWithTextMutable properties 

        public virtual void SetDefaultValueText(string value, bool assert = true)
        {
            if (value == null)
                throw new NullConstructorArgumentException(
                    "value"
                    , FullTypeNameForCode
                    );
            value = value.Trim();
            if (value.Length == 0)
                throw new EmptyStringConstraintTextException(
                    FullTypeNameForCode
                    , Name
                    );
            if (!HasNonWhitespace(value))
                throw new WhitespaceOnlyConstraintTextException(
                    FullTypeNameForCode
                    , Name
                    , value
                    );
            _defaultValueText = value;
        }

    }

    /// <summary>
    /// Boilerplate implementation of IDefaultConstraint without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <item>SingleColumnConstraintImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForColumn, always unless abstract class.</item>
    ///   <item>GetCopyForColumn, always unless abstract class.</item>
    ///   <item>GetReferenceForTable, always unless abstract class.</item>
    ///   <item>GetCopyForTable, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public class DefaultConstraintImmutable : SingleColumnConstraintImmutable, IDefaultConstraint
    {

        private IDefaultConstraint Delegee { get { return (IDefaultConstraint)_delegee; } }

        public DefaultConstraintImmutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , ITableColumn constrainedColumn = null
            , string defaultValueText = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
        {
            _delegee
                = new DefaultConstraintMutable(
                    name
                    , sqlTranslator == null ? null : (ISqlTranslator)sqlTranslator.GetSafeReference()
                    , table == null ? null : (ITable)table.GetSafeReference()
                    , constrainedColumn == null ? null : (ITableColumn)constrainedColumn.GetSafeReference()
                    , defaultValueText
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    );
            CompleteConstructionIfType(typeof(DefaultConstraintImmutable), false, assert);
        }

        public DefaultConstraintImmutable(
            string name
            , ITableColumn constrainedColumn = null
            , string defaultValueText = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
        {
            _delegee
                = new DefaultConstraintMutable(
                    name
                    , constrainedColumn == null ? null : (ITableColumn)constrainedColumn.GetSafeReference()
                    , defaultValueText
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    );
            CompleteConstructionIfType(typeof(DefaultConstraintImmutable), false, assert);
        }

        public DefaultConstraintImmutable(
            ITableColumn constrainedColumn = null
            , string defaultValueText = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
        {
            _delegee
                = new DefaultConstraintMutable(
                    constrainedColumn == null ? null : (ITableColumn)constrainedColumn.GetSafeReference()
                    , defaultValueText
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    );
            CompleteConstructionIfType(typeof(DefaultConstraintImmutable), false, assert);
        }

        public DefaultConstraintImmutable( IDefaultConstraint delegee, bool assert = true )
        {
            _delegee = delegee;
            CompleteConstructionIfType(typeof(DefaultConstraintImmutable), false, assert);
        }

        // DataProcObjectImmutable method overrides

        protected override Type ValidTypeForDelegee {  get { return typeof(IDefaultConstraint); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new DefaultConstraintImmutable(
                (IDefaultConstraint)
                Delegee.GetReferenceForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new DefaultConstraintImmutable(
                (IDefaultConstraint)
                Delegee.GetCopyForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }

        // SingleColumnConstraintImmutable method overrides
        
        public override IPotentiallySingleColumnConstraint GetCopyForColumn(ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return new DefaultConstraintImmutable(
                (IDefaultConstraint)
                Delegee.GetCopyForColumn(
                    constrainedColumn
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }
        public override IPotentiallySingleColumnConstraint GetCopyForTable(ITable table, ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return new DefaultConstraintImmutable(
                (IDefaultConstraint)
                Delegee.GetCopyForTable(
                    table
                    , constrainedColumn
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }

        // IConstraintWithText properties 

        public virtual string ConstraintText { get { return Delegee.ConstraintText; } }

        // IDefaultConstraint properties 

        public virtual string DefaultValueText { get { return Delegee.DefaultValueText; } }

    }

}
