﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{
    public interface ITriggerClassMapping : ISelfAsserter, IMemberMatcher, ISafeMemberOfImmutableOwner
    {
        string TriggerName { get; }
        ITableTrigger Trigger { get; }
    }
    public sealed class TableClassMapping : ITriggerClassMapping
    {
        private TableClassMapping() { }

        public TableClassMapping(
            string triggerName
            , ITableTrigger trigger
            )
        {
            TriggerName = triggerName;
            Trigger = trigger;
        }

        // Object method overrides

        /// <summary>
        /// Overriden Object.Equals object.
        /// </summary>
        /// <remarks>
        /// Should not be overridden by descendant classes.
        /// </remarks>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return EqualityUtilities.MemberMatchwiseEqualOrBothNull(this, obj);
        }

        /// <summary>
        /// Overriden Object.GetHashCode object.
        /// </summary>
        /// <remarks>
        /// Should be overridden by all descendant classes,
        /// and only those descendant classes, that add
        /// new members into MemberMatchwiseEqualOrBothNull.
        /// The general rule is that each new member's hash code
        /// is added to the hash code that would be generated
        /// by the nearest ancestor, after having been multiplied
        /// by a prime number not used by other members.
        /// Since descendants will need to know what
        /// prime numbers have already been used, each
        /// class must also publish its HighestPrimeUsedInHashCode.
        /// The descendant class then starts using prime numbers
        /// starting with NextPrimes[base.HighestPrimeUsedInHashCode].
        /// </remarks>
        /// <param name="obj">The object to which this is
        /// being compared for equality.</param>
        /// <returns>The object's hash code.</returns>
        public override int GetHashCode()
        {
            return TriggerName.GetHashCode()
                + 3 * Trigger.GetHashCode();
        }

        //  ISelfAsserter methods

        public bool PassesSelfAssertion(Type testableType = null)
        {
            Debug.Assert(
                TriggerName != null
                , "TriggerName property of TableClassMapping cannot be null."
                );
            Debug.Assert(
                TriggerName.Length > 0
                , "TriggerName property of TableClassMapping cannot be "
                + "the empty string."
                );
            Debug.Assert(
                !(new Regex("\\s").IsMatch(TriggerName))
                , "TriggerName property cannot contain whitespace."
                );
            Debug.Assert(
                Trigger != null
                , "Trigger property of TableClassMapping cannot be null."
                );
            Debug.Assert(
                typeof(ITableTrigger).IsAssignableFrom(Trigger.GetType())
                , "Trigger property of TableClassMapping must implement "
                + "the ITable interface."
                );
            return true;
        }

        // IMemberMatcher methods
        public bool MembersMatch(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(TableClassMapping).IsAssignableFrom(obj.GetType())) return false;
            TableClassMapping other = (TableClassMapping)obj;
            return
                TriggerName.Equals( other.TriggerName )
                && Trigger.Equals( other.Trigger );
        }

        // ISafeMemberOfImmutableOwner methods

        public ISafeMemberOfImmutableOwner GetSafeReference()
        {
            return GetSafeReference(assert: true);
        }

        public ISafeMemberOfImmutableOwner GetSafeReference(bool assert)
        {
            if (assert)
                Debug.Assert(PassesSelfAssertion());
            return this;
        }

        // TableClassMapping properties

        public string TriggerName { get; }
        public ITableTrigger Trigger { get; }
    }
}
