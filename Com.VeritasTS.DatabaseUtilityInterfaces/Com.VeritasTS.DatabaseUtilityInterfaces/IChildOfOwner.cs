﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// Base interface for objects that exist within a
    /// schema in a standard relational database.
    /// </summary>
    /// <remarks>
    /// Extends IObjectInSchema.
    /// </remarks>
    public interface IChildOfOwner : IObjectInSchema
    {
    }

    /// <summary>
    /// Base interface for objects that exist within a
    /// standard relational database and can be altered
    /// after construction.
    /// </summary>
    /// <remarks>
    /// Merely adds variability to IChildOfOwner.
    /// <para>
    /// Extends IObjectInSchemaMutable.</para>
    /// </remarks>
    public interface IChildOfOwnerMutable : IChildOfOwner, IObjectInSchemaMutable
    {
    }

    /// <summary>
    /// Boilerplate implementation of IChildOfOwnerMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaMutable method overrides.</item>
    ///   <list type="bullet">
    ///   <item>RetrieveIDsFromDatabase, if new child IObjectInSchemas have been
    /// added as members.</item>
    ///   </list>
    /// </item>
    /// <item>ChildOfOwner method overrides
    ///   <list type="bullet">
    ///   <item>ValidParentObjectTypes, usually unless abstract type.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IObjectInSchemas have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always unless abstract type.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    abstract public class ChildOfOwnerMutable : ObjectInSchemaMutable, IChildOfOwnerMutable
    {

        private static string FullTypeNameForExceptions
        {
            get { return typeof(ChildOfOwnerMutable).FullName; }
        }

        // Constructors

        /// <summary>
        /// Default constructor, which may not fully
        /// construct the object into a useful state,
        /// depending upon how a specific descent class
        /// is designed.
        /// </summary>
        /// <remarks>
        /// The default constructor is used by frameworks and does
        /// NOT mark the object has having been fully constructed.
        /// This means that assertions do not fire until the 
        /// creator explicitly completes the construction, allowing
        /// for objects to be constructed from the class name only
        /// using the null constructor, then initialized with
        /// Set method calls, and finally marked as ready for use.
        /// </remarks>
        protected ChildOfOwnerMutable() : base() { }

        public ChildOfOwnerMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , IObjectInSchema parentObject = null
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , string nameWhenPrefixed = null
            , int? id = null
            ) : base(
                name
                , sqlTranslator
                , parentObject
                , objectType
                , physicalObjectType
                , owner: parentObject
                , nameWhenPrefixed: nameWhenPrefixed
                , id: id
            )
        {
        }

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (IsFullyConstructed)
            {
                Debug.Assert(base.PassesSelfAssertion());
                if (ParentObject != null)
                    Debug.Assert(
                        ParentObject.Equals(Owner)
                        , "The parent object of an IChildOfOwningTable object "
                        + "must be its owner as well."
                        );
                else
                    Debug.Assert(
                        Owner == null
                        , "The parent object of an IChildOfOwningTable object "
                        + "must be its owner as well; thus if the parent object "
                        + "is null, the owner must also be null."
                        );
            }
            return true;
        }

        // IDatabaseObject method overrides

        public override List<Type> ValidOwnerTypesWhenNotParentObject
        {
            get { return new List<Type>(); }
        }

        public override IDatabaseObject GetCopyForOwner(
            IDataProcObject owner
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true)
        {
            RejectIfInvalidOwnerType(owner);
            return GetCopyForParentObject((IDatabaseObject)owner);
        }

        // ObjectInSchema method overrides

        public override bool ParentObjectMustBeOwner { get { return true; } }

        public override void SetOwner(IDataProcObject value, bool assert = true)
        {
            RejectIfInvalidOwnerType(value);
            SetParentObject((IObjectInSchema)value, assert);
        }

    }

    /// <summary>
    /// Boilerplate implementation of IChildOfOwner without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>ChildOfOwnerImmutable method overrides
    /// <list type="bullet">
    /// <item>GetReferenceForSchema, if an immediate descendant class.</item>
    /// <item>GetCopyForSchema, if an immediate descendant class.</item>
    /// <item>GetReferenceForParentObject.</item>
    /// <item>GetCopyForParentObject.</item>
    /// </list>
    /// </item>
    /// </list>
    /// <remarks>
    abstract public class ChildOfOwnerImmutable : ObjectInSchemaImmutable, IChildOfOwner
    {

        private IChildOfOwner Delegee { get { return (IChildOfOwner)_delegee; } }

        protected ChildOfOwnerImmutable() : base() { }

        public ChildOfOwnerImmutable(
            IChildOfOwner _delegee
            , bool assert = true
            ) : base(_delegee, false)
        {
            CompleteConstructionIfType(typeof(ChildOfOwnerImmutable), false, assert);
        }

    }

}
