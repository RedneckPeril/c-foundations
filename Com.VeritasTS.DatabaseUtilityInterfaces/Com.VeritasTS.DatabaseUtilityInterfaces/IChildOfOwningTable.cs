﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// Base interface for database objects whose
    /// ParentObject is an ITable, and that must
    /// be owned by the parent object.
    /// </summary>
    /// <remarks>
    /// Extends IChildOfTable.
    /// </remarks>
    public interface IChildOfOwningTable : IChildOfTable, IChildOfOwner { }

    /// <summary>
    /// Base interface for database objects whose
    /// ParentObject is an ITable and that can be altered
    /// after construction.
    /// </summary>
    /// <remarks>
    /// Merely adds variability to IChildOfOwningTable.
    /// <para>
    /// Extends IChildOfTableMutable.</para>
    /// </remarks>
    public interface IChildOfOwningTableMutable : IChildOfOwningTable, IChildOfTableMutable, IChildOfOwnerMutable { }

    /// <summary>
    /// Boilerplate implementation of IChildOfOwningTableMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides.</item>
    ///   <list type="bullet">
    ///   <item>ValidOwnerTypesWhenNotOwner, occasionally if not abstract type
    ///   and if Owner is not required to be ParentObject.</item>
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>SetOwner, rarely, e.g. when Owner should always be ParentObject 
    /// and vice versa.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>ParentObjectMustBeOwner, if true rather than false.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaMutable method overrides
    ///   <list type="bullet">
    ///   <item>SetParentObject, rarely, e.g. when Owner should always be ParentObject 
    /// and vice versa.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    abstract public class ChildOfOwningTableMutable : ChildOfTableMutable, IChildOfOwningTableMutable
    {

        private IDatabaseObject _parentObject;

        protected ChildOfOwningTableMutable() : base() { }

        public ChildOfOwningTableMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , IDataProcObject owner = null
            , string nameWhenPrefixed = null
            , int? id = null
            ) : base(
                name
                , sqlTranslator
                , table
                , objectType
                , physicalObjectType
                , owner
                , nameWhenPrefixed
                , id
            )
        { }

        // DataProcObjectMutable method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (IsFullyConstructed)
            {
                Debug.Assert(base.PassesSelfAssertion());
                if (ParentObject != null)
                    Debug.Assert(
                        ParentObject.Equals(Owner)
                        , "The parent object of an IChildOfOwningTable object "
                        + "must be its owner as well."
                        );
                else
                    Debug.Assert(
                        Owner == null
                        , "The parent object of an IChildOfOwningTable object "
                        + "must be its owner as well; thus if the parent object "
                        + "is null, the owner must also be null."
                        );
            }
            return true;
        }

        // IDatabaseObject method overrides

        public override List<Type> ValidOwnerTypesWhenNotParentObject
        {
            get { return new List<Type>(); }
        }

        public override IDatabaseObject GetCopyForOwner(
            IDataProcObject owner
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true)
        {
            RejectIfInvalidOwnerType(owner);
            return GetCopyForParentObject((IDatabaseObject)owner);
        }

        // ObjectInSchema method overrides

        public override bool ParentObjectMustBeOwner { get { return true; } }

    }

    /// <summary>
    /// Boilerplate implementation of IChildOfOwningTable without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    abstract public class ChildOfOwningTableImmutable : ChildOfTableImmutable, IChildOfOwningTable
    {

        private IChildOfOwningTable Delegee { get { return (IChildOfOwningTable)_delegee; } }

        protected ChildOfOwningTableImmutable() : base() { }

        public ChildOfOwningTableImmutable(
            IConstraint _delegee
            , bool assert = true
            ) : base(_delegee, false)
        {
            CompleteConstructionIfType(typeof(ConstraintImmutable), false, assert);
        }

    }

}
