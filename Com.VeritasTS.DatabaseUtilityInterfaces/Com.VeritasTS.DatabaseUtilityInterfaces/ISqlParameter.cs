﻿using System;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public enum SPParamProtocol : byte { Input, Output, ReadOnly }

    public interface ISqlParameter : IDisposable, ISafeMemberOfImmutableOwner, ISelfAsserter
    {
        object NativeParameter { get; }
        string Name { get; }
        string DataType { get; }
        Object DefaultValue { get; }
        SPParamProtocol Protocol { get; }
        bool IsOutput { get; }
    }

}
