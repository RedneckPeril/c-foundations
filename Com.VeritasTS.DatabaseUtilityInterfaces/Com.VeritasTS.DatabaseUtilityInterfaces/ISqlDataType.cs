﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// This interface identifies SQL data types. It is simply
    /// a subtype of IComparableByName and ISafeMemberOfImmutableOwner,
    /// and Name is its only property. 
    /// </summary>
    /// <remarks>
    /// We ordinarily use this type as a sort of enumeration: although
    /// you can define your own custom data types, 99% of the time we
    /// will simply access static members of the sealed pseudoenumeration
    /// class SqlDataTypes. 
    /// </remarks>
    public interface ISqlDataType : ISelfAsserter, ISafeMemberOfImmutableOwner, IImmutable
    {
        bool IsStringType { get; }
        bool IsIntType { get; }
        bool IsDateTimeType { get; }
        bool IsFloatType { get; }
        bool IsDecimalType { get; }
        bool IsLobType { get; }
        string Sql(ISqlTranslator sqlT);
    }

    /// <summary>
    /// A basic, invariant implementation of the ISqlDataType interface.
    /// </summary>
    public abstract class SqlDataType : SelfAsserter, ISqlDataType
    {

        /****************
         *  Private members
         ***************/

        private string FullTypeNameForCodeErrors { get { return typeof(SqlDataType).FullName; } }
        private static Dictionary<int, int> _nextPrimes;

        // Object method overrides and overridden GetHasCode assistants


        /// <summary>
        /// Quick lookup of next prime number
        /// given current prime number; used
        /// in building hash codes as descendant
        /// objects add additional members that
        /// affect the result of Equals.
        /// </summary>
        public static Dictionary<int, int> NextPrimes
        {
            get
            {
                if (_nextPrimes == null)
                    _nextPrimes = new Dictionary<int, int>
                    {
                        [2] = 3,
                        [3] = 5,
                        [5] = 7,
                        [7] = 11,
                        [11] = 13,
                        [13] = 17,
                        [17] = 19,
                        [19] = 23,
                        [23] = 29,
                        [29] = 31,
                        [31] = 37,
                        [37] = 41,
                        [41] = 43,
                        [43] = 47,
                        [47] = 53,
                        [53] = 59,
                        [59] = 61,
                        [61] = 67,
                        [67] = 71,
                        [71] = 73,
                        [73] = 79,
                        [79] = 83,
                        [83] = 89,
                        [89] = 97,
                        [97] = 101,
                        [101] = 103,
                        [103] = 107,
                        [107] = 109,
                        [109] = 113,
                        [113] = 127,
                        [127] = 131,
                        [131] = 137,
                        [137] = 139,
                        [139] = 149,
                        [149] = 151,
                        [151] = 157,
                        [157] = 163,
                        [163] = 167,
                        [167] = 173,
                        [173] = 179,
                        [179] = 181,
                        [181] = 191,
                        [191] = 193,
                        [193] = 197,
                        [197] = 199
                    };
                return _nextPrimes;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode()
                + 3 * GetType().GetHashCode();
        }

        public virtual int HighestPrimeUsedInHashCode { get { return 3; } }

        /****************
         *  ISelfAsserter methods via ISelfUsabilityAsserter
         ***************/

        /// <summary>
        /// Asserts the class invariants for this class.
        /// </summary>
        /// <returns>True, unless an assertion failure exception is thrown.</returns>
        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertion(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
            }
            return true;
        }

        /****************
         *  ISafeMemberOfImmutableOwner methods
         ***************/

        /// <summary>
        /// Returns itself, which is always safe since the
        /// SqlDataType class is invariant.
        /// </summary>
        /// <returns>The object itself.</returns>
        public ISafeMemberOfImmutableOwner GetSafeReference(bool assert = true)
        {
            if (assert) Debug.Assert(PassesSelfAssertion());
            return this;
        }

        /****************
         *  ISqlDataType properties and methods
         ***************/

        public virtual bool IsStringType { get { return false; } }

        public virtual bool IsIntType { get { return false; } }

        public virtual bool IsDateTimeType { get { return false; } }

        public virtual bool IsFloatType { get { return false; } }

        public virtual bool IsDecimalType { get { return false; } }

        public virtual bool IsLobType { get { return false; } }

        public abstract string DefaultSql { get; }

        public string Sql(ISqlTranslator sqlT)
        {
            if (sqlT == null)
                throw new NullArgumentException(
                    "sqlT"
                    , FullTypeNameForCodeErrors
                    + ".Sql(sqlT)"
                    );
            string retval = sqlT.TypeSql(this);
            if (retval == null || retval.Length == 0)
                retval = DefaultSql;
            return retval;
        }

    }

}
