﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// This interface identifies the "smallint"
    /// SQL type.
    /// </summary>
    /// <remarks>
    /// We ordinarily just retrieve the static object
    /// SqlNChar.Standard(). Objects are immutable
    /// and therefore threadsafe.
    /// </remarks>
    public interface ISqlNChar : ISqlStringType { }

    /// <summary>
    /// A basic, immutable implementation of the ISqlNChar interface.
    /// </summary>
    public sealed class SqlNChar : SqlStringType, ISqlNChar
    {

        /****************
         *  Private members
         ***************/

        private string FullTypeNameForCodeErrors { get { return typeof(SqlNChar).FullName; } }
        private static SqlNChar _standard;

        /****************
         *  Constructor
         ***************/

        public SqlNChar(int maxLength = 1) : base(maxLength) { }

        /****************
         *  ISqlNChar properties and methods
         ***************/

        public override string DefaultSql
        {
            get { return "nchar(" + MaxLength + ")"; }
        }

        public static SqlNChar Standard()
        {
            if (_standard == null) _standard = new SqlNChar();
            return _standard;
        }

    }

}
