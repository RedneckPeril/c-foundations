﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// This interface identifies database object types. It is 
    /// a subtype of IComparableByName and ISafeMemberOfImmutableOwner,
    /// with an additional boolean property IsVable. 
    /// </summary>
    /// <remarks>
    /// We ordinarily use this type as a sort of enumeration: although
    /// you can define your own custom vendors, 99% of the time we
    /// will simply access static members of the sealed pseudoenumeration
    /// class Vendors. 
    /// </remarks>
    public interface IObjectType : ISelfAsserter, IMemberMatcher, IComparableByName, ISafeMemberOfImmutableOwner
    {
        /// <summary>
        /// Reports whether this object
        /// type is a vable," where a "vable" is something that
        /// you can select from in a SQL select statement (which
        /// is to say, either a view or a table for most RDBMS's).
        /// </summary>
        /// <returns>The truth of the statement, "Objects of this type are vables."</returns>
        bool IsVable { get; }
    }

    public class ObjectType : ComparableByNameImmutable, IObjectType
    {

        /// <summary>
        /// Hides default constructor.
        /// </summary>
        private ObjectType() : base("") { }

        /// <summary>
        /// Construct from name.
        /// </summary>
        public ObjectType(string name, bool isVable) : base(name)
        {
            IsVable = isVable;
            Debug.Assert(PassesSelfAssertion());
        }

        // Object method overrides

        /// <summary>
        /// Standard Equals method, overridden to reflect member matchwise
        /// equality.
        /// </summary>
        /// <returns>Truth of statement "other object is an IVendor
        /// equal to this in name."</returns>
        public override bool Equals(object obj)
        {
            return EqualityUtilities.MemberMatchwiseEqualOrBothNull(this, obj);
        }

        /// <summary>
        /// Standard GetHashCode method, overridden to be the Name's hash
        /// code.
        /// </summary>
        /// <remarks>
        /// Does not account for null name as "name is not null" is a
        /// class invariant enforced on construction by assertion.</remarks>
        /// <returns>The hash code</returns>
        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        // ISelfAsserter methods
        public override bool PassesSelfAssertion(Type testableType = null)
        {
            bool retval = base.PassesSelfAssertion();
            bool doTest = true;
            if (testableType != null)
            {
                if (!testableType.Equals(this.GetType()))
                    doTest = false;
            }
            if (!doTest)
            {
                Debug.Assert(
                    Name != null
                    , "The Name property of an IObjectType object cannot be null."
                    );
                Debug.Assert(
                    !(new Regex("\\S").IsMatch(Name))
                    , "The Name property of an IObjectType object must contain "
                    + "something other than whitespace."
                    );
            }
            return retval;
        }

        // IMemberMatcher methods
        public virtual bool MembersMatch(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(IObjectType).IsAssignableFrom(obj.GetType())) return false;
            IObjectType other = (IObjectType)obj;
            if (!EqualityUtilities.EqualOrBothNull(this.Name, other.Name)) return false;
            return true;
        } 

        /// <summary>
        /// Returns either a safe reference to itself, or
        /// else a safe reference to a valid copy of itself.
        /// </summary>
        /// <remarks>
        /// The reference returned must be to an object that 
        /// always honors the contract specified for GetSafeReference()
        /// by the ISafeMemberOfImmutableOwner interface.
        /// </remarks>
        /// <returns>Either a safe reference to itself, or
        /// else a safe reference to a valid copy of itself</returns>
        public ISafeMemberOfImmutableOwner GetSafeReference() { return this; }

        /// <summary>
        /// Reports whether this object
        /// type is a vable," where a "vable" is something that
        /// you can select from in a SQL select statement (which
        /// is to say, either a view or a table for most RDBMS's).
        /// </summary>
        /// <returns>The truth of the statement, "Objects of this type are vables."</returns>
        public bool IsVable { get; }

    }

    /// <summary>
    /// A psuedoenumeration holding the standard set of database object types,
    /// each implemented as an invariant ObjectType. 
    /// </summary>
    public sealed class ObjectTypes
    {

        ObjectTypes() { }

        // Public static database object types

        /// <summary>
        /// The Schema database object type. 
        /// </summary>
        public static IObjectType Schema()
        {
            return SchemaGenerator.instance;
        }

        /// <summary>
        /// The Table database object type. 
        /// </summary>
        public static IObjectType Table()
        {
            return TableGenerator.instance;
        }

        /// <summary>
        /// The View database object type. 
        /// </summary>
        public static IObjectType View()
        {
            return ViewGenerator.instance;
        }

        /// <summary>
        /// The Sequence database object type. 
        /// </summary>
        public static IObjectType Sequence()
        {
            return SequenceGenerator.instance;
        }

        /// <summary>
        /// The Column database object type. 
        /// </summary>
        public static IObjectType Column()
        {
            return ColumnGenerator.instance;
        }

        /// <summary>
        /// The Trigger database object type. 
        /// </summary>
        public static IObjectType Trigger()
        {
            return TriggerGenerator.instance;
        }

        /// <summary>
        /// The PrimaryKey database object type. 
        /// </summary>
        public static IObjectType PrimaryKey()
        {
            return PrimaryKeyGenerator.instance;
        }

        /// <summary>
        /// The UniqueKey database object type. 
        /// </summary>
        public static IObjectType UniqueKey()
        {
            return UniqueKeyGenerator.instance;
        }

        /// <summary>
        /// The Index database object type. 
        /// </summary>
        public static IObjectType Index()
        {
            return IndexGenerator.instance;
        }

        /// <summary>
        /// The ForeignKey database object type. 
        /// </summary>
        public static IObjectType ForeignKey()
        {
            return ForeignKeyGenerator.instance;
        }

        /// <summary>
        /// The CheckConstraint database object type. 
        /// </summary>
        public static IObjectType CheckConstraint()
        {
            return CheckConstraintGenerator.instance;
        }

        /// <summary>
        /// The DefaultConstraint database object type. 
        /// </summary>
        public static IObjectType DefaultConstraint()
        {
            return DefaultConstraintGenerator.instance;
        }

        /// <summary>
        /// The ScalarFunction database object type. 
        /// </summary>
        public static IObjectType ScalarFunction()
        {
            return ScalarFunctionGenerator.instance;
        }

        /// <summary>
        /// The StoredProcedure database object type. 
        /// </summary>
        public static IObjectType StoredProcedure()
        {
            return StoredProcedureGenerator.instance;
        }

        /// <summary>
        /// The User database object type. 
        /// </summary>
        public static IObjectType User()
        {
            return CheckConstraintGenerator.instance;
        }

        // Private generator classes

        class SchemaGenerator
        {
            static SchemaGenerator() { }
            internal static readonly ObjectType instance = new ObjectType("Schema", true);
        };
        class TableGenerator
        {
            static TableGenerator() { }
            internal static readonly ObjectType instance = new ObjectType("Table", true);
        }
        class ViewGenerator
        {
            static ViewGenerator() { }
            internal static readonly ObjectType instance = new ObjectType("View", true);
        }
        class SequenceGenerator
        {
            static SequenceGenerator() { }
            internal static readonly ObjectType instance = new ObjectType("Sequence", false);
        }
        class ColumnGenerator
        {
            static ColumnGenerator() { }
            internal static readonly ObjectType instance = new ObjectType("Column", false);
        }
        class TriggerGenerator
        {
            static TriggerGenerator() { }
            internal static readonly ObjectType instance = new ObjectType("Trigger", false);
        }
        class PrimaryKeyGenerator
        {
            static PrimaryKeyGenerator() { }
            internal static readonly ObjectType instance = new ObjectType("Primary Key", false);
        }
        class UniqueKeyGenerator
        {
            static UniqueKeyGenerator() { }
            internal static readonly ObjectType instance = new ObjectType("Unique Key", false);
        }
        class IndexGenerator
        {
            static IndexGenerator() { }
            internal static readonly ObjectType instance = new ObjectType("Index", false);
        }
        class ForeignKeyGenerator
        {
            static ForeignKeyGenerator() { }
            internal static readonly ObjectType instance = new ObjectType("Foreign Key", false);
        }
        class CheckConstraintGenerator
        {
            static CheckConstraintGenerator() { }
            internal static readonly ObjectType instance = new ObjectType("Check Constraint", false);
        }
        class DefaultConstraintGenerator
        {
            static DefaultConstraintGenerator() { }
            internal static readonly ObjectType instance = new ObjectType("Default Constraint", false);
        }
        class ScalarFunctionGenerator
        {
            static ScalarFunctionGenerator() { }
            internal static readonly ObjectType instance = new ObjectType("Scalar Function", false);
        }
        class StoredProcedureGenerator
        {
            static StoredProcedureGenerator() { }
            internal static readonly ObjectType instance = new ObjectType("Stored Procedure", false);
        }
        class UserGenerator
        {
            static UserGenerator() { }
            internal static readonly ObjectType instance = new ObjectType("User", false);
        }
    }

    public class UnsupportedObjectTypeException : Exception
    {
        public IDatabaseObject Object { get; }
        public UnsupportedObjectTypeException(IDatabaseObject obj) { Object = obj; }
    }

    public class BadObjectTypeParameterException : Exception
    {
        public string ParamName { get; }
        public IObjectType BadObjectType { get; }
        public BadObjectTypeParameterException(
            string paramName
            , IObjectType badObjectType
            , string message
            ): base(message)
        {
            ParamName = paramName;
            BadObjectType = badObjectType;
        }
    }

}
