﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{
    public interface INonnullabilityConstraint: ISingleColumnCheckConstraint
    {
    }

    public interface INonnullabilityConstraintMutable : INonnullabilityConstraint, ISingleColumnCheckConstraintMutable
    {
    }

    /// <summary>
    /// Boilerplate implementation of INonnullabilityConstraintMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides.</item>
    ///   <list type="bullet">
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class NonnullabilityConstraintMutable : SingleColumnCheckConstraintMutable, INonnullabilityConstraintMutable
    {

        private string FullNameForCodeErrors
        {
            get { return typeof(NonnullabilityConstraintMutable).FullName; }
        }

        // Construction
        public NonnullabilityConstraintMutable() : base() { }

        public NonnullabilityConstraintMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , IColumn constrainedColumn = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , sqlTranslator
                , table
                , constrainedColumn
                , constrainedColumn != null && constrainedColumn.Name != null
                ? constrainedColumn.Name + " is not null"
                : null
                , nameWhenPrefixed: nameWhenPrefixed
                , id: id
                )
        {
            CompleteConstructionIfType(typeof(NonnullabilityConstraintMutable), retrieveIDsFromDatabase, assert);
        }

        public NonnullabilityConstraintMutable(
            string name
            , IColumn constrainedColumn = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , constrainedColumn
                , constrainedColumn != null && constrainedColumn.Name != null
                ? constrainedColumn.Name + " is not null"
                : null
                , nameWhenPrefixed: nameWhenPrefixed
                , id: id
                )
        {
            CompleteConstructionIfType(typeof(NonnullabilityConstraintMutable), retrieveIDsFromDatabase, assert);
        }

        public NonnullabilityConstraintMutable(
            IColumn constrainedColumn = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                constrainedColumn
                , constrainedColumn != null && constrainedColumn.Name != null
                ? constrainedColumn.Name + " is not null"
                : null
                , nameWhenPrefixed: nameWhenPrefixed
                , id: id
                )
        {
            CompleteConstructionIfType(typeof(NonnullabilityConstraintMutable), retrieveIDsFromDatabase, assert);
        }

        // DataProcObjectMutable method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    ConstraintText.ToLower().Equals(
                        ConstrainedColumn.Name + " is not null"
                    )
                    , "In a non-nullability constraint, the "
                    + "constraint text must consist of the constrained "
                    + "columns name followed by ' is not null'."
                    );
            }
            return true;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new NonnullabilityConstraintMutable(
                Name
                , GetSqlTranslatorSafely()
                , GetTableSafely()
                , GetConstrainedColumnSafely()
                , NameWhenPrefixed
                , ID
                , false
                , assert
                );
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , table: null
                , constrainedColumn: null
                , nameWhenPrefixed: null
                , id: default(int?)
                );
        }

        protected virtual void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , IColumn constrainedColumn = null
            , string nameWhenPrefixed = null
            , int? id = null
            )
        {
            base.ConstructAfterConstruction(
                name
                , sqlTranslator
                , table
                , constrainedColumn
                , constrainedColumn != null && constrainedColumn.Name != null
                ? constrainedColumn.Name + " is not null"
                : null
                , nameWhenPrefixed: nameWhenPrefixed
                , id: id
                );
        }

        public virtual void ConstructNewMembers(
            string name
            , bool forceUpdates
            )
        {
            if (forceUpdates || Name == null )
            {
                if (name != null) SetName(name, false);
                else if (name == null && (forceUpdates || Name == null))
                {
                    if (
                        !(
                        ConstrainedColumn == null
                        || ConstrainedColumn.Name == null
                        || Vable == null
                        || Vable.Name == null
                        )
                        )
                        throw new NullConstructorArgumentException(
                            "name"
                            , FullNameForCodeErrors
                            );
                    SetName(
                        SqlTranslator.TableConstraintName(
                            "_ck" + ConstrainedColumn.ConstraintTag
                            , ConstrainedColumn.Vable.Name
                            )
                        , false
                        );
                }
            }
        }

        // ObjectInSchema method overrides

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            RejectIfInvalidParentObjectType(parentObject);
            bool retrieveIDs
                = retrieveIDsFromDatabase != null && retrieveIDsFromDatabase.HasValue
                ? retrieveIDsFromDatabase.Value
                : parentObject != null && parentObject.ID != null;
            return new NonnullabilityConstraintMutable(
                Name
                , SqlTranslator
                , Table
                , (IColumn)ConstrainedColumn.GetCopyForVable(
                    (IVable)parentObject
                    , retrieveIDs
                    , false
                    )
                , NameWhenPrefixed
                , ID
                , false
                , assert
                );
        }

        // ConstraintVariable overrides

        public override string ConstraintTypeTag {  get { return "_cknn"; } }

    }

    /// <summary>
    /// Boilerplate implementation of IChildOfVable without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <item>NonnullabilityConstraintImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetCopyForColumn, always unless abstract class.</item>
    ///   <item>GetCopyForTable, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public class NonnullabilityConstraintImmutable : SingleColumnCheckConstraintImmutable, INonnullabilityConstraint
    {

        private INonnullabilityConstraint Delegee { get { return (INonnullabilityConstraint)_delegee; } }

        public NonnullabilityConstraintImmutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , IColumn constrainedColumn = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
        {
            _delegee
                = new NonnullabilityConstraintMutable(
                    name
                    , sqlTranslator == null ? null : (ISqlTranslator)sqlTranslator.GetSafeReference()
                    , table == null ? null : (ITable)table.GetSafeReference()
                    , constrainedColumn == null ? null : (IColumn)constrainedColumn.GetSafeReference()
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    );
            CompleteConstructionIfType(typeof(NonnullabilityConstraintImmutable), false, assert);
        }

        public NonnullabilityConstraintImmutable(
            string name
            , IColumn constrainedColumn = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
        {
            _delegee
                = new NonnullabilityConstraintMutable(
                    name
                    , constrainedColumn == null ? null : (IColumn)constrainedColumn.GetSafeReference()
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    );
            CompleteConstructionIfType(typeof(NonnullabilityConstraintImmutable), false, assert);
        }

        public NonnullabilityConstraintImmutable(
            IColumn constrainedColumn = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
        {
            _delegee
                = new NonnullabilityConstraintMutable(
                    constrainedColumn == null ? null : (IColumn)constrainedColumn.GetSafeReference()
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    );
            CompleteConstructionIfType(typeof(NonnullabilityConstraintImmutable), false, assert);
        }

        public NonnullabilityConstraintImmutable(INonnullabilityConstraint delegee, bool assert = true)
        {
            _delegee = delegee;
            CompleteConstructionIfType(typeof(NonnullabilityConstraintImmutable), false, assert);
        }

        // DataProcObjectImmutable method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(INonnullabilityConstraint); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new NonnullabilityConstraintImmutable(
                (INonnullabilityConstraint)
                Delegee.GetReferenceForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new NonnullabilityConstraintImmutable(
                (INonnullabilityConstraint)
                Delegee.GetCopyForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }

        // PotentiallySingleColumnConstraintImmutable method overrides

        public override IPotentiallySingleColumnConstraint GetCopyForColumn(IColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return new NonnullabilityConstraintImmutable(
                (INonnullabilityConstraint)
                Delegee.GetCopyForColumn(
                    constrainedColumn
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }
        public override IPotentiallySingleColumnConstraint GetCopyForTable(ITable table, IColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return new NonnullabilityConstraintImmutable(
                (INonnullabilityConstraint)
                Delegee.GetCopyForTable(
                    table
                    , constrainedColumn
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }

    }

}

