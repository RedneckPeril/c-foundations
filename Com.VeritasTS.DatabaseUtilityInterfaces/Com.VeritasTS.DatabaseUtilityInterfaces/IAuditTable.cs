﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;


namespace Com.VeritasTS.DatabaseUtilityInterfaces
{
    public interface IAuditTable : IChangeTrackingTable
    {
    }
    public interface IAuditTableMutable : IAuditTable, IChangeTrackingTableMutable
    {
    }

    /// <summary>
    /// Boilerplate implementation of IAuditTableMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    ///   affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidOwnerTypesWhenNotOwner, occasionally if not abstract type
    ///   and if Owner is not required to be ParentObject.</item>
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>ParentObjectMustBeOwner, if true rather than false.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class AuditTableMutable : ChangeTrackingTableMutable, IAuditTableMutable
    {

        public AuditTableMutable() : base() { }

        public AuditTableMutable(
            string name
            , ISqlTranslator sqlTranslator
            , ISchema schema
            , ITable trackedTable
            , IIdentityColumn identityColumn = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name == null ? trackedTable.Name + "Audit" : name
                , sqlTranslator
                , schema
                , trackedTable
                , allowsUpdates: false
                , allowsDeletes: false
                , identityColumn: identityColumn
                , hasChangeResponderColumn: false
                , nameWhenPrefixed: null
                , id: id
                , retrieveIDsFromDatabase: false
                , assert: false
                )
        {
            ConstructNewMembers(true);
            CompleteConstructionIfType(typeof(AuditTableMutable), retrieveIDsFromDatabase, assert);
        }

        // DataProcObjectMutable method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (IsFullyConstructed)
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    !AllowsUpdates
                    , "An audit table must not allow updates."
                    );
                Debug.Assert(
                    !AllowsDeletes
                    , "An audit table must not allow deletes."
                    );
                Debug.Assert(
                    !HasChangeResponderColumn
                    , "An audit table cannot have a change responder column."
                    );
            }
            return true;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            IIDColumn idColumn = IDColumn;
            return new AuditTableMutable(
                Name
                , GetSqlTranslatorSafely()
                , (ISchema)GetParentObjectSafely()
                , GetTrackedTableSafely()
                , GetIdentityColumnSafely()
                , NameWhenPrefixed
                , ID
                , false
                , assert
                );
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , schema: null
                , trackedTable: null
                , identityColumn: null
                , nameWhenPrefixed: null
                , id: default(int?)
                );
        }

        protected virtual void ConstructAfterConstruction(
           string name
            , ISqlTranslator sqlTranslator
            , ISchema schema
            , ITable trackedTable
            , IIdentityColumn identityColumn
            , string nameWhenPrefixed
            , int? id
            )
        {
            ISqlTranslator sql = SqlTranslator;
            if (sql == null)
                throw new ConstructionRequiresSqlTranslatorException(
                    this
                    , typeof(VableMutable).FullName
                    );
            if (TrackedTable != null) trackedTable = TrackedTable;
            if (trackedTable == null)
                throw new ConstructionRequiresParentObjectException(
                    this
                    , typeof(VableMutable).FullName
                    );
            if (Name == null) Name = trackedTable.Name + "Audit";
            base.ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , schema: null
                , trackedTable: trackedTable
                , allowsUpdates: false
                , allowsDeletes: false
                , identityColumn: identityColumn
                , hasChangeResponderColumn: false
                , nameWhenPrefixed: null
                , id: id
                );
            ConstructNewMembers(false);
        }

        private void ConstructNewMembers(
            bool forceUpdates = true
            )
        {
            ISqlTranslator sql = SqlTranslator;
            if (sql == null)
                throw new ConstructionRequiresSqlTranslatorException(
                    this
                    , typeof(VableMutable).FullName
                    );
            IIdentityColumn identityColumn = IdentityColumn;
            string identityColumnConstraintTag = identityColumn.ConstraintTag;
            string identityColumnName = identityColumn.Name;
            AddOrOverwriteIndex(
                new IndexMutable(
                    Name + "_i" + identityColumnConstraintTag + "UDtm"
                    , sql
                    , this
                    , ObjectTypes.Index()
                    , ObjectTypes.Index()
                    , this
                    , new List<string> { identityColumnName, "updateDtm" }
                    , false
                    , false
                    )
                );
            AddOrOverwriteIndex(
                new IndexMutable(
                    Name + "_i" + identityColumnConstraintTag + "AUDtm"
                    , sql
                    , this
                    , ObjectTypes.Index()
                    , ObjectTypes.Index()
                    , this
                    , new List<string> { identityColumnName, "action", "updateDtm" }
                    , false
                    , false
                    )
                );
        }

        /***************************
         * IDatabaseObject method overrides
        ***************************/

        public override IDatabaseObject GetCopyForOwner(
            IDataProcObject owner
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            RejectIfInvalidOwnerType(owner);
            return new AuditTableMutable(
                Name
                , SqlTranslator
                , Schema
                , TrackedTable
                , IdentityColumn
                , NameWhenPrefixed
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        // ObjectInSchemaMutable method overrides

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            RejectIfInvalidParentObjectType(parentObject);
            return new AuditTableMutable(
                Name
                , SqlTranslator
                , (ISchema)parentObject
                , TrackedTable
                , IdentityColumn
                , NameWhenPrefixed
                , ID
                , false
                , assert
                );
        }

    }

    /// <summary>
    /// Boilerplate implementation of IAuditTable without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public class AuditTableImmutable : ChangeTrackingTableImmutable, IAuditTable
    {
        private IAuditTable Delegee { get { return (IAuditTable)_delegee; } }

        //Disable default constructor
        private AuditTableImmutable() : base(delegee: null) { }

        public AuditTableImmutable(
            string name
            , ISqlTranslator sqlTranslator
            , ISchema schema
            , ITable trackedTable
            , IIdentityColumn identityColumn = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(delegee: null)
        {
            _delegee =
                new AuditTableMutable(
                    name
                    , sqlTranslator
                    , schema
                    , trackedTable
                    , identityColumn
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    );
            CompleteConstructionIfType(typeof(AuditTableImmutable), false, true);
        }

        public AuditTableImmutable(IAuditTable delegee) : base(delegee: null)
        {
            _delegee = delegee;
            CompleteConstructionIfType(typeof(AuditTableImmutable), false, true);
        }

        // DataProcObject method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(IAuditTable); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            IObjectInSchema retval
                = new AuditTableImmutable(
                (IAuditTable)
                    ((IObjectInSchema)_delegee).GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, assert)
                    );
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }
        public override IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            IObjectInSchema retval
                = new AuditTableImmutable(
                (IAuditTable)
                    ((IObjectInSchema)_delegee).GetCopyForParentObject(parentObject, retrieveIDsFromDatabase, assert)
                    );
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }

    }

}
