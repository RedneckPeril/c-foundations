﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// This interface identifies the "smallint"
    /// SQL type.
    /// </summary>
    /// <remarks>
    /// We ordinarily just retrieve the static object
    /// SqlDate.Standard(). Objects are immutable
    /// and therefore threadsafe.
    /// </remarks>
    public interface ISqlDate : ISqlIntType { }

    /// <summary>
    /// A basic, immutable implementation of the ISqlDate interface.
    /// </summary>
    public sealed class SqlDate : SqlDateTimeType, ISqlDate
    {

        /****************
         *  Private methods
         ***************/

        private string FullTypeNameForCodeErrors { get { return typeof(SqlDate).FullName; } }
        private static SqlDate _standard;

        /****************
         *  Constructor
         ***************/

        private SqlDate() { }

        /****************
         *  SqlDataType method overrides
         ***************/

        public override string DefaultSql { get { return "date"; } }

        /****************
         *  ISqlDate properties and methods
         ***************/

        public static SqlDate Standard()
        {
            if (_standard == null) _standard = new SqlDate();
            return _standard;
        }

    }

}
