﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface ISchema : IDatabaseObject {}

    public interface ISchemaMutable : ISchema, IDatabaseObjectMutable
    {
        void SetDatabase(IDatabaseSafe value, bool? retrieveIDsFromDatabase = null, bool assert = true);
    }

    public class SqlTranslatorDatabaseMismatchException : Exception
    {
        public SqlTranslatorDatabaseMismatchException() : base(
                message:
            "If an IDatabaseObject's database's SQLTranslator property is not null, "
            + "you cannot set IDatabaseObject's own SqlTranslator property to anything else."
                )
        { }
    }

    public class SchemaMutable : DatabaseObjectMutable, ISchemaMutable
    {

        private IDatabaseSafe _database;

        private string FullTypeNameForExceptions {  get { return typeof(SchemaMutable).FullName; } }

        public SchemaMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , IDatabaseSafe database = null
            , int? id = null
            , bool retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , sqlTranslator != null ? sqlTranslator : ( database != null ? database.SqlTranslator : null )
                , owner: null
                , objectType: ObjectTypes.Schema()
                , physicalObjectType: ObjectTypes.Schema()
                , id: id
                )
        {
            // We know, or should know, that when we come back from the base
            //   constructor SqlTranslator is not null, or else an exception has
            //   already been thrown.
            Debug.Assert(SqlTranslator != null, "SqlTranslator escaped DatabaseObjectMutable constructor as null.");
            if (database != null && database.SqlTranslator != null && !database.SqlTranslator.Equals(SqlTranslator))
                throw new SqlTranslatorDatabaseMismatchException();
            SetDatabase(database, retrieveIDsFromDatabase: false, assert: false);
            CompleteConstructionIfType(typeof(SchemaMutable), retrieveIDsFromDatabase, assert);
        }

        // IDataProcObject method overrides

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new SchemaMutable(
                Name
                , GetSqlTranslatorSafely()
                , GetDatabaseSafely()
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , database: null
                , id: null
                , forceUpdates: false
                );
        }

        protected virtual void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator = null
            , IDatabaseSafe database = null
            , int? id = null
            , bool forceUpdates = true
            )
        {
            if (Name == null && name == null)
                throw new ConstructionRequiresNameException(
                    this
                    , FullTypeNameForExceptions
                    );
            if (
                SqlTranslator == null 
                && sqlTranslator == null
                && (Database == null || Database.SqlTranslator == null)
                && (database == null || database.SqlTranslator == null)
                )
                throw new ConstructionRequiresSqlTranslatorException(
                    this
                    , FullTypeNameForExceptions
                    );
            base.ConstructAfterConstruction(
                Name
                , SqlTranslator != null 
                ? SqlTranslator 
                : sqlTranslator != null ? sqlTranslator : (database != null ? database.SqlTranslator : null)
                , ObjectTypes.Schema()
                , ObjectTypes.Schema()
                , owner: null
                , nameWhenPrefixed: null
                , id: id
                );
            ConstructNewMembers(database);
        }

        private void ConstructNewMembers(
            IDatabaseSafe database
            )
        {

            // We know, or should know, that when we come back from the base
            //   constructor SqlTranslator is not null, or else an exception has
            //   already been thrown.
            if (SqlTranslator == null )
                throw new ConstructionRequiresSqlTranslatorException(
                    this
                    , FullTypeNameForExceptions
                    );
            if (database != null && database.SqlTranslator != null && !database.SqlTranslator.Equals(SqlTranslator))
                throw new SqlTranslatorDatabaseMismatchException();
            SetDatabase(database, retrieveIDsFromDatabase: false, assert: false);
        }

        // IDatabaseObjectMutable method overrides

        public override IDatabaseSafe Database
        {
            get { return _database; }
        }

        public override ISchema Schema
        {
            get { return this; }
        }

        public override IDatabaseObject GetReferenceForOwner(IDataProcObject owner, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            if (owner != null)
                throw new BadSchemaOwnerException( this, Name );
            return this;
        }

        public override IDatabaseObject GetCopyForOwner(IDataProcObject owner, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            if (owner != null)
                throw new BadSchemaOwnerException(this, Name);
            return new SchemaMutable(
                name: Name
                , sqlTranslator: SqlTranslator
                , database: Database
                , id: null
                , retrieveIDsFromDatabase: (retrieveIDsFromDatabase.HasValue ? retrieveIDsFromDatabase.Value : (ID != null))
                , assert: assert
                );
        }

        public override IDatabaseObject GetReferenceForDatabase(IDatabaseSafe database, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            bool retrieveIDs = WillNeedToRetrieveIDs(Database, database, retrieveIDsFromDatabase);
            SetDatabase(database, retrieveIDsFromDatabase: false, assert: false);
            if (retrieveIDs) RetrieveIDsFromDatabase(assert: false);
            if (assert) Debug.Assert(PassesSelfAssertion());
            return this;
        }

        public override IDatabaseObject GetCopyForDatabase(IDatabaseSafe database, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            return new SchemaMutable(
                Name
                , ( database != null && database.SqlTranslator != null ? database.SqlTranslator : SqlTranslator )
                , database
                , id: null
                , retrieveIDsFromDatabase: (retrieveIDsFromDatabase.HasValue ? retrieveIDsFromDatabase.Value : (ID != null))
                , assert: assert
                );
        }

        public override void SetSqlTranslator(ISqlTranslator value, bool assert = true)
        {
            if (!(Database != null && Database.SqlTranslator != null))
                base.SetSqlTranslator(value, assert: false);
            else if (!Database.SqlTranslator.Equals(value))
                throw new SqlTranslatorDatabaseMismatchException();
            else if (!SqlTranslator.Equals(value))
                base.SetSqlTranslator(value, assert: false);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        // ISchemaMutable methods

        public void SetDatabase(IDatabaseSafe value, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            bool retrieveIDs = WillNeedToRetrieveIDs(Database, value, retrieveIDsFromDatabase);
            if (!(Database != null && Database.Equals(value)))
            {
                SetDatabase(value, retrieveIDsFromDatabase: false, assert: false);
                if (Database != null && Database.SqlTranslator != null && !SqlTranslator.Equals(Database.SqlTranslator))
                    SetSqlTranslator(Database.SqlTranslator, assert: false);
            }
            if (retrieveIDs) RetrieveIDsFromDatabase(assert: false);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public class BadSchemaOwnerException: Exception
        {
            private BadSchemaOwnerException() { }
            public BadSchemaOwnerException(
                ISchema schema
                , string schemaName
                , string message = null
                ): base(
                    message != null ? message
                    : "Attempted to set the Owner property of ISchema object '"
                    + ( schemaName != null ? schemaName : "[unnamed]" )
                    + "' to something other than null; ISchema objects never "
                    + "have non-null owners."
                    )
            {
                BadSchema = schema;
            }
            public ISchema BadSchema { get; }
        }

    }

    public class SchemaImmutable : DatabaseObjectImmutable, ISchema
    {

        private ISchema Delegee { get { return (ISchema)_delegee; } }

        public SchemaImmutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , IDatabaseSafe database = null
            , int? id = null
            , bool retrieveIDFromDatabase = false
            , bool assert = true
            )
        {
            _delegee
                = new SchemaMutable(
                    name
                    , (sqlTranslator == null ? null : (ISqlTranslator)sqlTranslator.GetSafeReference())
                    , (database == null ? null : (IDatabaseSafe)database.GetSafeReference())
                    , id
                    , retrieveIDFromDatabase
                    , assert: false
                    );
            CompleteConstructionIfType(typeof(SchemaImmutable), retrieveIDsFromDatabase: false, assert: assert);
        }

        public SchemaImmutable( ISchema delegee, bool assert = true )
        {
            RejectIfInvalidDelegeeType(delegee);
            _delegee = delegee;
            CompleteConstructionIfType(typeof(SchemaImmutable), retrieveIDsFromDatabase: false, assert: assert);
        }

        // IDataProcObject method overrides
        
        protected override Type ValidTypeForDelegee { get { return typeof(ISchema); } }

        // IDatabaseObject method overrides
        public override ISchema Schema { get { return this; } }
        public override ISchema GetSchemaSafely() { return this; }
        public override IDatabaseObject GetReferenceForDatabase(IDatabaseSafe database, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            return new SchemaImmutable(
                (ISchema)Delegee.GetSafeReferenceForDatabase(database, retrieveIDsFromDatabase, true)
                );
        }
        public override IDatabaseObject GetCopyForDatabase(IDatabaseSafe database, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            return new SchemaImmutable(
                (ISchema)Delegee.GetSafeReferenceForDatabase(database, retrieveIDsFromDatabase, true)
                );
        }

    }

}
