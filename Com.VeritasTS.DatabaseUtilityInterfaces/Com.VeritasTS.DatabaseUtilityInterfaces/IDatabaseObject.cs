﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// Base interface for objects that exist within a
    /// standard relational database.
    /// </summary>
    /// <remarks>
    /// Of all IDatabaseObjects, only ISchema objects are
    /// not also IObjectInSchema objects.
    /// <para>
    /// Extends IDataProcObject.
    /// </para>
    /// </remarks>
    public interface IDatabaseObject : IDataProcObject
    {
        string NameWhenPrefixed { get; }
        IDatabaseSafe Database { get; }
        ISchema Schema { get; }
        string DatabaseDescription { get; }
        IObjectType ObjectType { get; }
        IObjectType PhysicalObjectType { get; }
        IDataProcObject Owner { get; }
        List<Type> ValidOwnerTypes { get; }
        void RejectIfInvalidOwnerType(IDataProcObject candidate);
        bool ObjectIsValidTypeForOwner(IDataProcObject candidate, bool throwException = false);
        int? ID { get; }
        bool WillNeedToRetrieveIDs(
            object currentObject, object replacementObject, bool? retrieveIDsFromDatabase
            );
        IDatabaseSafe GetDatabaseSafely();
        ISchema GetSchemaSafely();
        IObjectType GetObjectTypeSafely();
        IObjectType GetPhysicalObjectTypeSafely();
        IDataProcObject GetOwnerSafely();
        IDatabaseObject GetReferenceForDatabase(IDatabaseSafe database, bool? retrieveIDsFromDatabase = null, bool assert = true);
        IDatabaseObject GetCopyForDatabase(IDatabaseSafe database, bool? retrieveIDsFromDatabase = null, bool assert = true);
        IDatabaseObject GetSafeReferenceForDatabase(IDatabaseSafe database, bool? retrieveIDsFromDatabase = null, bool assert = true);
        IDatabaseObject GetReferenceForOwner(IDataProcObject owner, bool? retrieveIDsFromOwner = null, bool assert = true);
        IDatabaseObject GetCopyForOwner(IDataProcObject owner, bool? retrieveIDsFromOwner = null, bool assert = true);
        IDatabaseObject GetSafeReferenceForOwner(IDataProcObject owner, bool? retrieveIDsFromOwner = null, bool assert = true);
        IDatabaseObject GetReferenceWithIDFromDatabase(bool forceRetrieval = false, bool assert = true);
        IDatabaseObject GetCopyWithIDFromDatabase(bool forceRetrieval = false, bool assert = true);
        IDatabaseObject GetSafeReferenceWithIDFromDatabase(bool forceRetrieval = false, bool assert = true);
        void EnsureExistenceInDatabase();
        void EnsureExistenceInDatabaseAsDesigned();
        void DropFromDatabaseIfExists();
    }

    /// <summary>
    /// Base interface for objects that exist within a
    /// standard relational database and can be altered
    /// after construction.
    /// </summary>
    /// <remarks>
    /// Simply adds variability to IDatabaseObject.
    /// <para>
    /// Extends IDataProcObjectMutable.
    /// </para>
    /// </remarks>
    public interface IDatabaseObjectMutable : IDatabaseObject, IDataProcObjectMutable
    {
        void SetNameWhenPrefixed(string value, bool assert = true);
        void SetOwner(IDataProcObject value, bool assert = true);
        void SetObjectType(IObjectType value, bool? retrieveIDsFromDatabase = null, bool assert = true);
        void SetPhysicalObjectType(IObjectType value, bool? retrieveIDsFromDatabase = null, bool assert = true);
        void SetID(int? value, bool retrieveIDsFromDatabase = false, bool assert = true);
        void SetIDFromDatabase(bool assert = true);
        void RetrieveIDsFromDatabase(bool assert = true);
        bool ExistsInDatabase();
    }

    public class BadOwnerException : Exception
    {
        public IDatabaseObject BadObject { get; }
        public Type DesiredOwnerType { get; }
        public Type SuppliedOwnerType { get; }
        public BadOwnerException(
            IDatabaseObject badObject
            , Type desiredOwnerType
            , Type suppliedOwnerType
            , string message
            ) : base(message)
        {
            BadObject = badObject;
            DesiredOwnerType = desiredOwnerType;
            SuppliedOwnerType = suppliedOwnerType;
        }
    }

    /// <summary>
    /// Boilerplate implementation of IDatabaseObjectMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides
    /// <list type="bullet">
    ///   <item>Database, if an immediate descendant class.</item>
    ///   <item>Schema, if an immediate descendant class.</item>
    ///   <item>ValidTypesForOwner, occasionally if not abstract type.</item>
    ///   <item>GetReferenceForDatabase, if an immediate descendant class.</item>
    ///   <item>GetCopyForDatabase, if an immediate descendant class.</item>
    ///   <item>GetReferenceForOwner.</item>
    ///   <item>GetCopyForOwner.</item>
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>SetOwner, rarely, e.g. when Owner should always be ParentObject 
    /// and vice versa.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    abstract public class DatabaseObjectMutable : DataProcObjectMutable, IDatabaseObjectMutable
    {

        private IObjectType _objectType;
        private IObjectType _physicalObjectType;
        private IDataProcObject _owner;
        private string _nameWhenPrefixed;
        private int? _id;

        // Constructors

        /// <summary>
        /// Default constructor, which may not fully
        /// construct the object into a useful state,
        /// depending upon how a specific descent class
        /// is designed.
        /// </summary>
        /// <remarks>
        /// The default constructor is used by frameworks and does
        /// NOT mark the object has having been fully constructed.
        /// This means that assertions do not fire until the 
        /// creator explicitly completes the construction, allowing
        /// for objects to be constructed from the class name only
        /// using the null constructor, then initialized with
        /// Set method calls, and finally marked as ready for use.
        /// </remarks>
        protected DatabaseObjectMutable() : base() { }

        public DatabaseObjectMutable(
            string name
            , ISqlTranslator sqlTranslator
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , IDataProcObject owner = null
            , string nameWhenPrefixed = null
            , int? id = null
            ) : base( name, sqlTranslator )
        {
            SetOwner(owner, assert: false);
            SetObjectType(objectType, retrieveIDsFromDatabase: false, assert: false);
            SetPhysicalObjectType(physicalObjectType, retrieveIDsFromDatabase: false, assert: false);
            SetNameWhenPrefixed(nameWhenPrefixed, assert: false);
        }

        // Object method overrides

        public override int GetHashCode()
        {
            int p1 = NextPrimes[base.HighestPrimeUsedInHashCode];
            int p2 = NextPrimes[p1];
            int p3 = NextPrimes[p2];
            int p4 = NextPrimes[p3];
            int p5 = NextPrimes[p4];
            return base.GetHashCode()
                + p1 * (Database == null ? 0 : Database.GetHashCode())
                + p2 * (Owner == null ? 0 : Owner.GetHashCode())
                + p3 * (ObjectType == null ? 0 : ObjectType.GetHashCode())
                + p4 * (PhysicalObjectType == null ? 0 : PhysicalObjectType.GetHashCode())
                + p5 * (ID == null ? 0 : ID.Value);
        }

        protected virtual int GetHashCodeWithoutDatabase()
        {
            int p1 = NextPrimes[base.HighestPrimeUsedInHashCode];
            int p2 = NextPrimes[p1];
            int p3 = NextPrimes[p2];
            int p4 = NextPrimes[p3];
            return base.GetHashCode()
                + p1 * (Owner == null ? 0 : Owner.GetHashCode())
                + p2 * (ObjectType == null ? 0 : ObjectType.GetHashCode())
                + p3 * (PhysicalObjectType == null ? 0 : PhysicalObjectType.GetHashCode())
                + p4 * (ID == null ? 0 : ID.Value);
        }

        // Object method overrides: GetHashCode assistants

        public override int HighestPrimeUsedInHashCode
        {
            get
            {
                int p1 = NextPrimes[base.HighestPrimeUsedInHashCode];
                int p2 = NextPrimes[p1];
                int p3 = NextPrimes[p2];
                int p4 = NextPrimes[p3];
                return NextPrimes[p4];
            }
        }

        protected virtual int HighestPrimeUsedInHashCodeWithoutDatabase
        {
            get
            {
                int p1 = NextPrimes[base.HighestPrimeUsedInHashCode];
                int p2 = NextPrimes[p1];
                int p3 = NextPrimes[p2];
                return NextPrimes[p3];
            }
        }

        // IDataProcObject method overrides

        public override string FullName
        {
            get
            {
                if (Name == null) return null;
                else if (Database == null) return null;
                else if (Database.FullName == null) return null;
                else return Database.FullName + "." + Name;
            }
        }

        public override bool MembersMatch(object obj)
        {
            if (!MembersMatchWithoutDatabase(obj)) return false;
            if (!typeof(IDatabaseObject).IsAssignableFrom(obj.GetType())) return false;
            if (!base.MembersMatch(obj)) return false;
            IDatabaseObject other = (IDatabaseObject)obj;
            return EqualityUtilities.EqualOrBothNull(this.Database, other.Database);
        }

        protected virtual bool MembersMatchWithoutDatabase(object obj)
        {
            if (obj == null) return false;
            if (!typeof(IDatabaseObject).IsAssignableFrom(obj.GetType())) return false;
            if (!base.MembersMatch(obj)) return false;
            IDatabaseObject other = (IDatabaseObject)obj;
            return (
                (EqualityUtilities.EqualOrBothNull(this.Owner, other.Owner))
                && (EqualityUtilities.EqualOrBothNull(this.ObjectType, other.ObjectType))
                && (EqualityUtilities.EqualOrBothNull(this.PhysicalObjectType, other.PhysicalObjectType))
                && (EqualityUtilities.EqualOrBothNull(this.ID, other.ID))
                );
        }

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (IsFullyConstructed)
            {
                Debug.Assert(PassesSelfAssertionWithoutDatabase(testableType));
                if (Database != null) Debug.Assert(Database.PassesSelfAssertion());
            }
            return true;
        }

        protected override void ConstructAfterConstruction() { }

        protected virtual void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , IDataProcObject owner = null
            , string nameWhenPrefixed = null
            , int? id = null
            )
        {
            base.ConstructAfterConstruction(name, sqlTranslator);
            ConstructNewMembers(
                objectType
                , physicalObjectType
                , owner
                , nameWhenPrefixed
                , id
                , false
                );
        }

        private void ConstructNewMembers(
            IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , IDataProcObject owner = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool forceUpdates = true
            )
        {
            if (forceUpdates || ObjectType == null) SetObjectType(objectType, false, false);
            if (forceUpdates || PhysicalObjectType == null) SetPhysicalObjectType(physicalObjectType, false, false);
            if (forceUpdates || Owner == null) SetOwner(owner, false);
            if (forceUpdates || NameWhenPrefixed == null) SetNameWhenPrefixed(nameWhenPrefixed, false);
            if (forceUpdates || ID == null) SetID(id, false, false);
        }

        public virtual bool PassesSelfAssertionWithoutDatabase(Type testableType = null)
        {
            if (IsFullyConstructed)
            {
                Debug.Assert(base.PassesSelfAssertion());
                // Don't assert Owner for fear of infinite loops
                Debug.Assert(
                    ValidOwnerTypes != null
                    , "The valid owner types list for an IDatabaseObject of type "
                    + GetType().FullName
                    + " cannot be null, though it can be empty."
                    );
                if (ObjectType != null) Debug.Assert(ObjectType.PassesSelfAssertion());
                if (PhysicalObjectType != null) Debug.Assert(PhysicalObjectType.PassesSelfAssertion());
            }
            return true;
        }

        public override void CompleteConstructionIfType(Type t, bool? retrieveIDsFromDatabase, bool assert = true)
        {
            if (t.Equals(GetType()) && !_isFullyConstructed)
            {
                if (retrieveIDsFromDatabase.HasValue ? retrieveIDsFromDatabase.Value : !ID.HasValue)
                    RetrieveIDsFromDatabase(assert: false);
                _isFullyConstructed = true;
                if (assert) Debug.Assert(PassesSelfAssertion());
            }
        }

        // IDatabaseObject methods and properties
        public bool WillNeedToRetrieveIDs(
            object currentObject, Object replacementObject, bool? retrieveIDsFromDatabase
            )
        {
            bool retval;
            if (retrieveIDsFromDatabase.HasValue) retval = retrieveIDsFromDatabase.Value;
            else if (!ID.HasValue) retval = false;
            else if (replacementObject == null) retval = true;
            else retval = !replacementObject.Equals(currentObject);
            return retval;
        }

        public string NameWhenPrefixed
        {
            get { return _nameWhenPrefixed; }
            set { SetNameWhenPrefixed(value); }
        }
        public abstract IDatabaseSafe Database { get; }
        public string DatabaseDescription
        {
            get
            {
                if (Database == null) return "database";
                else return Database.Description;
            }
        }
        public abstract ISchema Schema { get; }
        public IObjectType ObjectType
        {
            get { return _objectType; }
            set { SetObjectType(value); }
        }
        public IObjectType PhysicalObjectType
        {
            get { return _physicalObjectType; }
            set { SetPhysicalObjectType(value); }
        }
        public IDataProcObject Owner
        {
            get { return _owner; }
            set { SetOwner(value); }
        }

        public virtual List<Type> ValidOwnerTypes { get { return new List<Type>(); } }

        public void RejectIfInvalidOwnerType(IDataProcObject candidate)
        {
            ObjectIsValidTypeForOwner(candidate, true);
        }

        public bool ObjectIsValidTypeForOwner(IDataProcObject candidate, bool throwException = false)
        {
            bool retval = ValidOwnerTypes.Count == 0;
            if (!retval)
            {
                if (candidate == null)
                {
                    if (throwException)
                        throw new BadOwnerTypeException(
                            (IsFullyConstructed ? this : null)
                            , this.GetType().FullName
                            , "[null]"
                            , typeof(ITable).FullName
                            );
                }
                else
                {
                    foreach (Type t in ValidOwnerTypes)
                    {
                        if (t.IsAssignableFrom(candidate.GetType()))
                        {
                            retval = true;
                            break;
                        }
                    }
                    if (!retval && throwException)
                        throw new BadOwnerTypeException(
                            (IsFullyConstructed ? this : null)
                            , this.GetType().FullName
                            , candidate.GetType().FullName
                            , typeof(ITable).FullName
                            );
                }
            }
            return retval;
        }

        public int? ID
        {
            get { return _id; }
            set { SetID(value); }
        }
        public IDatabaseSafe GetDatabaseSafely()
        {
            return (IDatabaseSafe)(Database == null ? null : Database.GetSafeReference());
        }
        public ISchema GetSchemaSafely()
        {
            return (ISchema)(Schema == null ? null : Schema.GetSafeReference());
        }
        public IObjectType GetObjectTypeSafely()
        {
            return (IObjectType)(ObjectType == null ? null : ObjectType.GetSafeReference());
        }
        public IObjectType GetPhysicalObjectTypeSafely()
        {
            return (IObjectType)(PhysicalObjectType == null ? null : PhysicalObjectType.GetSafeReference());
        }
        public IDataProcObject GetOwnerSafely()
        {
            return (IDataProcObject)(Owner == null ? null : Owner.GetSafeReference());
        }
        public void EnsureExistenceInDatabase()
        {
            Database.EnsureObjectExists(this);
        }
        public void EnsureExistenceInDatabaseAsDesigned()
        {
            Database.EnsureObjectExistsAsDesigned(this);
        }
        public void DropFromDatabaseIfExists()
        {
            Database.DropObjectIfExists(this);
        }

        public abstract IDatabaseObject GetReferenceForDatabase(IDatabaseSafe database, bool? retrieveIDsFromDatabase = null, bool assert = true);

        public abstract IDatabaseObject GetCopyForDatabase(IDatabaseSafe database, bool? retrieveIDsFromDatabase = null, bool assert = true);

        public virtual IDatabaseObject GetSafeReferenceForDatabase(IDatabaseSafe database, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            return
                (IDatabaseObjectMutable)
                GetCopyForDatabase(database, retrieveIDsFromDatabase, assert)
                .GetSafeReference(assert);
        }

        public abstract IDatabaseObject GetReferenceForOwner(IDataProcObject owner, bool? retrieveIDsFromDatabase = null, bool assert = true);

        public abstract IDatabaseObject GetCopyForOwner(IDataProcObject owner, bool? retrieveIDsFromDatabase = null, bool assert = true);

        public virtual IDatabaseObject GetSafeReferenceForOwner(IDataProcObject owner, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            return
                (IDatabaseObjectMutable)
                GetCopyForOwner(owner, retrieveIDsFromDatabase, assert)
                .GetSafeReference(assert);
        }

        public IDatabaseObject GetReferenceWithIDFromDatabase(bool forceRetrieval = false, bool assert = true)
        {
            return GetCopyWithIDFromDatabase(forceRetrieval, assert);
        }
        public IDatabaseObject GetCopyWithIDFromDatabase(bool forceRetrieval = false, bool assert = true)
        {
            if (forceRetrieval || ID == null)
                return GetCopyForDatabase(Database, true, assert);
            else return this;
        }
        public IDatabaseObject GetSafeReferenceWithIDFromDatabase(bool forceRetrieval = false, bool assert = true)
        {
            return (IDatabaseObject)GetCopyWithIDFromDatabase(forceRetrieval, assert).GetSafeReference();
        }

        // IDatabaseObjectMutable methods

        public void SetNameWhenPrefixed(string value, bool assert = true)
        {
            if (value != null) _nameWhenPrefixed = value;
            else if (Name.Equals("id")) _nameWhenPrefixed = "ID";
            else _nameWhenPrefixed = Name.Substring(0, 1).ToUpper() + Name.Substring(1);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void SetOwner(IDataProcObject value, bool assert = true)
        {
            RejectIfInvalidOwnerType(value);
            _owner = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void SetObjectType(IObjectType value, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            bool retrieveIDs
                = WillNeedToRetrieveIDs(ObjectType, value, retrieveIDsFromDatabase);
            _objectType = value;
            if (retrieveIDs) RetrieveIDsFromDatabase(assert: false);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void SetPhysicalObjectType(IObjectType value, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            bool retrieveIDs
                = WillNeedToRetrieveIDs(PhysicalObjectType, value, retrieveIDsFromDatabase);
            _physicalObjectType = value;
            if (retrieveIDs) RetrieveIDsFromDatabase(assert: false);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void SetID(int? value, bool retrieveIDsFromDatabase = true, bool assert = true)
        {
            _id = value;
            if (_id == null && retrieveIDsFromDatabase)
                RetrieveIDsFromDatabase(assert: false);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void SetIDFromDatabase(bool assert = true)
        {
            if (Database == null) _id = null;
            else _id = Database.IDOfObject(this);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void RetrieveIDsFromDatabase(bool assert = true)
        {
            SetIDFromDatabase(assert);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public bool ExistsInDatabase()
        {
            IDatabaseSafe db = Database;
            if (db == null) return false;
            if (Database.IsConnected) return db.ObjectExists(this);
            else return false;
        }

    }

    /// <summary>
    /// Boilerplate implementation of IDatabaseObject without variability.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForDatabase, if an immediate descendant class.</item>
    ///   <item>GetCopyForDatabase, if an immediate descendant class.</item>
    ///   <item>GetReferenceForOwner, always unless abstract class.</item>
    ///   <item>GetCopyForOwner, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    abstract public class DatabaseObjectImmutable : DataProcObjectImmutable, IDatabaseObject
    {

        private IDatabaseObject Delegee { get { return (IDatabaseObject)_delegee; } }

        protected DatabaseObjectImmutable() : base() { }

        public DatabaseObjectImmutable(
            IDatabaseObject _delegee
            , bool assert = true
            ) : base( _delegee, false)
        {
            CompleteConstructionIfType(typeof(DatabaseObjectImmutable), false, assert);
        }

        // IDatabaseObject methods and properties

        public string NameWhenPrefixed { get { return Delegee.NameWhenPrefixed; } }
        public virtual IDatabaseSafe Database { get { return Delegee.GetDatabaseSafely(); } }
        public virtual string DatabaseDescription { get { return Delegee.DatabaseDescription; } }
        public virtual ISchema Schema { get { return Delegee.GetSchemaSafely(); } }
        public virtual IObjectType ObjectType { get { return Delegee.GetObjectTypeSafely(); } }
        public virtual IObjectType PhysicalObjectType { get { return Delegee.GetPhysicalObjectTypeSafely(); } }
        public virtual IDataProcObject Owner { get { return Delegee.GetOwnerSafely(); } }
        public List<Type> ValidOwnerTypes { get { return Delegee.ValidOwnerTypes; } }
        public void RejectIfInvalidOwnerType(IDataProcObject candidate)
        {
            Delegee.ObjectIsValidTypeForOwner(candidate, true);
        }
        public bool ObjectIsValidTypeForOwner(IDataProcObject candidate, bool throwException = false)
        {
            return Delegee.ObjectIsValidTypeForOwner(candidate, throwException = true);
        }
        public virtual int? ID { get { return Delegee.ID; } }
        public IDatabaseSafe GetDatabaseSafely() { return Delegee.GetDatabaseSafely(); }
        public virtual ISchema GetSchemaSafely() { return Delegee.GetSchemaSafely(); }
        public IObjectType GetObjectTypeSafely() { return Delegee.GetObjectTypeSafely(); }
        public IObjectType GetPhysicalObjectTypeSafely() { return Delegee.GetPhysicalObjectTypeSafely(); }
        public IDataProcObject GetOwnerSafely() { return Delegee.GetOwnerSafely(); }
        public bool WillNeedToRetrieveIDs(object currentObject, object replacementObject, bool? retrieveIDsFromDatabase)
        {
            return false;
        }
        public virtual void EnsureExistenceInDatabase() { Delegee.EnsureExistenceInDatabase(); }
        public virtual void EnsureExistenceInDatabaseAsDesigned() { Delegee.EnsureExistenceInDatabaseAsDesigned(); }
        public virtual void DropFromDatabaseIfExists() { Delegee.DropFromDatabaseIfExists(); }
        public abstract IDatabaseObject GetReferenceForDatabase(IDatabaseSafe database, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true);
        public abstract IDatabaseObject GetCopyForDatabase(IDatabaseSafe database, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true);
        public IDatabaseObject GetSafeReferenceForDatabase(IDatabaseSafe database, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return GetCopyForDatabase(database, retrieveIDsFromDatabase, assert);
        }
        public abstract IDatabaseObject GetReferenceForOwner(IDataProcObject owner, bool? retrieveIDsFromOwner = default(bool?), bool assert = true);
        public abstract IDatabaseObject GetCopyForOwner(IDataProcObject owner, bool? retrieveIDsFromOwner = default(bool?), bool assert = true);
        public IDatabaseObject GetSafeReferenceForOwner(IDataProcObject owner, bool? retrieveIDsFromOwner = default(bool?), bool assert = true)
        {
            return GetCopyForOwner(owner, retrieveIDsFromOwner, assert);
        }
        public IDatabaseObject GetReferenceWithIDFromDatabase(bool forceRetrieval = false, bool assert = true)
        {
            return GetCopyWithIDFromDatabase(forceRetrieval, assert);
        }
        public IDatabaseObject GetCopyWithIDFromDatabase(bool forceRetrieval = false, bool assert = true)
        {
            if (forceRetrieval || ID == null)
                return GetCopyForDatabase(Database, true, assert);
            else return this;
        }
        public IDatabaseObject GetSafeReferenceWithIDFromDatabase(bool forceRetrieval = false, bool assert = true)
        {
            return (IDatabaseObject)GetCopyWithIDFromDatabase(forceRetrieval, assert).GetSafeReference();
        }
    }

    public class BadOwnerTypeException : Exception
    {
        public IDatabaseObject BadObject { get; }
        public string BadOwnerTypeName { get; }
        public string AcceptableOwnerTypeName { get; }
        public BadOwnerTypeException(
            IDatabaseObject badObject
            , string badObjectTypeName
            , string badOwnerTypeName
            , string acceptableOwnerTypeName
            , string message = null
            ) : base(
                (
                message != null
                ? message
                : "Cannot set the Owner property of an object "
                + "of type "
                + (badObjectTypeName != null ? badObjectTypeName : badObject.GetType().FullName)
                + " to an "
                + "object of type " + badOwnerTypeName + "; the "
                + "parent object must be an object of type "
                + acceptableOwnerTypeName + "."
                )
                )
        {
            BadObject = badObject;
            BadOwnerTypeName = badOwnerTypeName;
            AcceptableOwnerTypeName = acceptableOwnerTypeName;
        }
    }

    public class NullOwnerException : Exception
    {
        public IDatabaseObject BadObject { get; }
        public NullOwnerException(
            IDatabaseObject badObject
            , string badObjectTypeName
            , string message = null
            ) : base(
                (message != null
                ? message
                : "Attempted an operation on an IDatabaseObject of type "
                + (badObjectTypeName != null ? badObjectTypeName : badObject.GetType().FullName)
                + "that could not be performed because the Owner "
                + "is null."
                )
                )
        {
            BadObject = badObject;
        }
    }

}
