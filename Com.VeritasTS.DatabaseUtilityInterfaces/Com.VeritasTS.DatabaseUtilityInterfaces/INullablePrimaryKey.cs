﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface INullablePrimaryKey : IPrimaryKey
    {
        IDatabaseSequence Sequence { get; }
        IDatabaseSequence GetSequenceSafely();
    }

    public interface INullablePrimaryKeyMutable : INullablePrimaryKey, IPrimaryKeyMutable
    {
        void SetSequence(
            IDatabaseSequence value
            , bool? retrieveSequenceIDFromDatabase = default(bool?)
            , bool assert = true
            );
    }

    /// <summary>
    /// Boilerplate implementation of INullablePrimaryKeyMutable.
    /// </summary>
    /// <remarks>
    /// Sealed at present.
    /// </remarks>
    public sealed class NullablePrimaryKeyMutable : PrimaryKeyMutable, INullablePrimaryKeyMutable
    {

        private IDatabaseSequence _sequence;
        private string FullTypeNameForCodeErrors { get { return typeof(NullablePrimaryKeyMutable).FullName; } }

        public NullablePrimaryKeyMutable() : base() { }

        public NullablePrimaryKeyMutable(
            string name
            , ISqlTranslator sqlTranslator
            , ITable table
            , ITableColumn constrainedColumn = null
            , List<string> columnNames = null
            , bool isClustered = true
            , IDatabaseSequence sequence = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , sqlTranslator
                , table
                , ObjectTypes.UniqueKey()
                , constrainedColumn
                , columnNames
                , isClustered
                , nameWhenPrefixed
                , id
                , retrieveIDsFromDatabase: false
                , assert: false
                )
        {
            ConstructNewMembers(sequence, true);
            CompleteConstructionIfType(typeof(NullablePrimaryKeyMutable), retrieveIDsFromDatabase, assert);
        }

        public NullablePrimaryKeyMutable(
            string name
            , ITableColumn constrainedColumn
            , List<string> columnNames = null
            , bool isClustered = true
            , IDatabaseSequence sequence = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , ObjectTypes.UniqueKey()
                , constrainedColumn
                , columnNames
                , isClustered
                , nameWhenPrefixed
                , id
                , retrieveIDsFromDatabase: false
                , assert: false
                )
        {
            ConstructNewMembers(sequence, true);
            CompleteConstructionIfType(typeof(NullablePrimaryKeyMutable), retrieveIDsFromDatabase, assert);
        }

        public NullablePrimaryKeyMutable(
            ITableColumn constrainedColumn
            , List<string> columnNames = null
            , bool isClustered = true
            , IDatabaseSequence sequence = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                ObjectTypes.UniqueKey()
                , constrainedColumn
                , columnNames
                , isClustered
                , nameWhenPrefixed
                , id
                , retrieveIDsFromDatabase: false
                , assert: false
                )
        {
            ConstructNewMembers(sequence, true);
            CompleteConstructionIfType(typeof(NullablePrimaryKeyMutable), retrieveIDsFromDatabase, assert);
        }

        private void ConstructNewMembers(
            IDatabaseSequence sequence
            , bool forceUpdates
            )
        {
            if (forceUpdates || _sequence == null ) SetSequence(sequence, false, false);
        }

        // IDataProcObject method overrides
        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                string tableName = Table.Name;
                Dictionary<string, Tuple<int, IColumn>> columns = Table.Columns;
                bool foundANullableColumn = false;
                foreach (string name in ColumnNames)
                {
                    if (columns[name].Item2.IsNullable) foundANullableColumn = true;
                }
                Debug.Assert(
                    foundANullableColumn
                    , "None of the columns for nullable primary key '" + Name
                    + "' on table '" + tableName + "' allow nulls."
                    );
                Debug.Assert(
                    Sequence != null
                    , "A nullable primary key must have a non-null "
                    + "Sequence property."
                    );
            }
            return true;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new NullablePrimaryKeyMutable(
                Name
                , GetSqlTranslatorSafely()
                , GetTableSafely()
                , GetConstrainedColumnSafely()
                , GetColumnNamesSafely()
                , IsClustered
                , GetSequenceSafely()
                , NameWhenPrefixed
                , ID
                , false
                , assert
                );
        }

        // ObjectInSchemaMutable method overrides

        public override IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            RejectIfInvalidParentObjectType(parentObject);
            return new NullablePrimaryKeyMutable(
                Name
                , SqlTranslator
                , (ITable)parentObject
                , ConstrainedColumn
                , ColumnNames
                , IsClustered
                , Sequence
                , NameWhenPrefixed
                , ID
                , retrieveIDsFromDatabase
                , assert
                );
        }

        // INullablePrimaryKey properties and methods
        public IDatabaseSequence Sequence
        {
            get { return _sequence; }
            set { SetSequence( value, true); }
        }
        public IDatabaseSequence GetSequenceSafely()
        {
            return (IDatabaseSequence)_sequence.GetSafeReference();
        }

        // INullablePrimaryKeyMutable methods

        public void SetSequence(
            IDatabaseSequence value
            , bool? retrieveSequenceIDFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            bool retrieveIDs
                = retrieveSequenceIDFromDatabase != null && retrieveSequenceIDFromDatabase.HasValue
                ? retrieveSequenceIDFromDatabase.Value
                : ID != null && value != null && value.ID == null;
            if (value == null)
                throw new NullArgumentException(
                    "value"
                    , FullTypeNameForCodeErrors
                    + ".SetSequence(value, retrieveSequenceIDFromDatabase, assert)"
                    );
            _sequence = value;
            if (retrieveIDs)
            {
                if (_sequence.IsMutableType) ((IDatabaseSequenceMutable)_sequence).RetrieveIDsFromDatabase(assert);
                else
                    _sequence
                        = (IDatabaseSequence)_sequence.GetReferenceWithIDFromDatabase();
            }
        }

    }


    /// <summary>
    /// Boilerplate implementation of IPrimaryKey without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>PotentiallySingleColumnConstraintImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetCopyForColumn, always unless abstract class.</item>
    ///   <item>GetCopyForTable, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public sealed class NullablePrimaryKeyImmutable : PrimaryKeyImmutable, INullablePrimaryKey
    {

        private INullablePrimaryKey Delegee { get { return (INullablePrimaryKey)_delegee; } }

        public NullablePrimaryKeyImmutable(
            string name
            , ISqlTranslator sqlTranslator
            , ITable table
            , ITableColumn constrainedColumn = null
            , List<string> columnNames = null
            , bool isClustered = true
            , IDatabaseSequence sequence = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base()
        {
            _delegee
                = (INullablePrimaryKey)
                new NullablePrimaryKeyMutable(
                    name
                    , sqlTranslator
                    , table
                    , constrainedColumn
                    , columnNames
                    , isClustered
                    , sequence
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    ).GetSafeReference();
            CompleteConstructionIfType(typeof(NullablePrimaryKeyImmutable), retrieveIDsFromDatabase: false, assert: true);
        }

        public NullablePrimaryKeyImmutable(
            string name
            , ITableColumn constrainedColumn = null
            , List<string> columnNames = null
            , bool isClustered = true
            , IDatabaseSequence sequence = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base()
        {
            _delegee
                = (INullablePrimaryKey)
                new NullablePrimaryKeyMutable(
                    name
                    , constrainedColumn
                    , columnNames
                    , isClustered
                    , sequence
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    ).GetSafeReference();
            CompleteConstructionIfType(typeof(NullablePrimaryKeyImmutable), retrieveIDsFromDatabase: false, assert: true);
        }

        public NullablePrimaryKeyImmutable(
            ITableColumn constrainedColumn = null
            , List<string> columnNames = null
            , bool isClustered = true
            , IDatabaseSequence sequence = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base()
        {
            _delegee
                = (INullablePrimaryKey)
                new NullablePrimaryKeyMutable(
                    constrainedColumn
                    , columnNames
                    , isClustered
                    , sequence
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    ).GetSafeReference();
            CompleteConstructionIfType(typeof(NullablePrimaryKeyImmutable), retrieveIDsFromDatabase: false, assert: true);
        }

        public NullablePrimaryKeyImmutable(
            INullablePrimaryKey delegee
            , bool assert = true
            ) : base(delegee, false)
        {
            CompleteConstructionIfType(typeof(NullablePrimaryKeyImmutable), retrieveIDsFromDatabase: false, assert: true);
        }

        // DataProcObjectImmutable method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(INullablePrimaryKey); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return
                new NullablePrimaryKeyImmutable(
                    (INullablePrimaryKey)
                    Delegee.GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, false)
                    );
        }

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return
                new NullablePrimaryKeyImmutable(
                    (INullablePrimaryKey)
                    Delegee.GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, false)
                    );
        }

        // PotentiallySingleColumnConstraintImmutable method overrides

        public override IPotentiallySingleColumnConstraint GetCopyForColumn(ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return new NullablePrimaryKeyImmutable(
                    (INullablePrimaryKey)
                Delegee.GetCopyForColumn(
                    constrainedColumn
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }
        public override IPotentiallySingleColumnConstraint GetCopyForTable(ITable table, ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return new NullablePrimaryKeyImmutable(
                    (INullablePrimaryKey)
                Delegee.GetCopyForTable(
                    table
                    , constrainedColumn
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }

        // INullablePrimaryKey properties and methods

        public IDatabaseSequence Sequence { get { return Delegee.GetSequenceSafely(); } }
        public IDatabaseSequence GetSequenceSafely() { return Delegee.GetSequenceSafely(); }

    }

}
