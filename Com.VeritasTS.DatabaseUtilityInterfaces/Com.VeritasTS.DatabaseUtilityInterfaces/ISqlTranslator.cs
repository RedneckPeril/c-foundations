﻿using System;
using System.Collections.Generic;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface ISqlTranslator : ISelfAsserter, ISafeMemberOfImmutableOwner
    {


        /*** 
         * DatabaseImmutable manipulation methods
         ***/

        IVendor Vendor { get; }
        bool UsesForEachRowTriggers { get; }
        bool RequiresIdentityColumn { get; }
        string SingletonVableSql { get; }
        bool SupportsInMemoryTempTables { get; }
        string TempTableName(
            string tempTableNameBase
            , bool globallyAvailable
            , bool inMemoryIfPossible
            );
        string TempTableCreateSqlSnip(
            string tempTableName
            , bool globallyAvailable
            , bool inMemoryIfPossible
            );

        /// <summary>
        /// Returns the SQL statement needed to find the ID
        /// of the database with the specified name, living
        /// on the specified server. 
        /// </summary>
        /// <remarks>
        /// <para>
        /// If the server is null, then the assumption is that
        /// the database is located on the server that is
        /// local to the connection on which the SQL is executed.
        /// </para>
        /// </remarks>
        /// <param name="name">The name of the database.</param>
        /// <param name="server">The server on which the database
        /// should live. If null, the implication is that the
        /// database should live on the server that hosts
        /// the connection on which the SQL is executed.</param>
        /// <returns>The ID of the database if it exists, null
        /// if it does not exist.</returns>
        List<string> IDOfDatabaseSql(string name, IServer server = null, int spacesToIndent = 0, bool terminatesStatement = true);
        List<string> EnsureDatabaseExistsSql(IDatabase database, int spacesToIndent = 0);
        List<string> DropDatabaseIfExistsSql(IDatabase database, int spacesToIndent = 0);

        // SqlDataTypes
        string TypeSql(ISqlDataType theType);

        // CheckConstraint
        List<string> IDOfCheckConstraintSql(
            string name
            , string tableName
            , string schemaName = null
            , IDatabase database = null
            , int spacesToIndent = 0
            , bool terminatesStatement = true
            );
        List<string> EnsureCheckConstraintExistsSql(ICheckConstraint constraint, int spacesToIndent = 0);
        List<string> EnsureCheckConstraintExistsAsDesignedSql(ICheckConstraint constraint, int spacesToIndent = 0);
        List<string> DropCheckConstraintIfExistsSql(ICheckConstraint constraint, int spacesToIndent = 0);

        // Column
        List<string> IDOfColumnSql(
            string name
            , string vableName
            , string schemaName = null
            , IDatabase database = null
            , int spacesToIndent = 0
            , bool terminatesStatement = true
            );
        List<string> IDOfTableColumnSql(
            string name
            , string tableName
            , string schemaName = null
            , IDatabase database = null
            , int spacesToIndent = 0
            , bool terminatesStatement = true
            );
        List<string> EnsureTableColumnExistsSql(ITableColumn column, int spacesToIndent = 0, List<string> nonnullPopulationScript = null);
        List<string> EnsureTableColumnExistsAsDesignedSql(ITableColumn column, int spacesToIndent = 0, List<string> nonnullPopulationScript = null);
        List<string> DropTableColumnIfExistsSql(ITableColumn column, int spacesToIndent = 0);
        string SysnameTypeSql { get; }
        string StringTypeSql { get; }
        string UnicodeStringTypeSql { get; }

        // DefaultConstraint

        List<string> IDOfDefaultConstraintSql(
            string name
            , string tableName
            , string schemaName = null
            , IDatabase database = null
            , int spacesToIndent = 0
            , bool terminatesStatement = true
            );
        List<string> EnsureDefaultConstraintExistsSql(IDefaultConstraint constraint, int spacesToIndent = 0);
        List<string> EnsureDefaultConstraintExistsAsDesignedSql(IDefaultConstraint constraint, int spacesToIndent = 0);
        List<string> DropDefaultConstraintIfExistsSql(IDefaultConstraint constraint, int spacesToIndent = 0);

        // ForeignKey

        List<string> IDOfForeignKeySql(
            string name
            , string tableName
            , string schemaName = null
            , IDatabase database = null
            , int spacesToIndent = 0
            , bool terminatesStatement = true
            );
        List<string> EnsureForeignKeyExistsSql(IForeignKey key, bool cascadeManually, int spacesToIndent = 0);
        List<string> EnsureForeignKeyExistsAsDesignedSql(IForeignKey key, bool cascadeManually, int spacesToIndent = 0);
        List<string> DropForeignKeyIfExistsSql(IForeignKey key, int spacesToIndent = 0);

        // Index

        List<string> IDOfIndexSql(
            string name
            , string vableName
            , string schemaName = null
            , IDatabase database = null
            , int spacesToIndent = 0
            , bool terminatesStatement = true
            );
        List<string> EnsureIndexExistsSql(IIndex index, int spacesToIndent = 0);
        List<string> EnsureIndexExistsAsDesignedSql(IIndex index, int spacesToIndent = 0);
        List<string> DropIndexIfExistsSql(IIndex index, int spacesToIndent = 0);
        string CreateTempTableIndexSql(
             string tempTableIndexName
             , string tempTableName
             , string columnList
             , bool isUnique = false
             , bool isClustered = false
             , int spacesToIndent = 0
             );

        // PrimaryKey

        List<string> IDOfPrimaryKeySql(
            string name
            , string tableName
            , string schemaName = null
            , IDatabase database = null
            , int spacesToIndent = 0
            , bool terminatesStatement = true
            );
        List<string> EnsurePrimaryKeyExistsSql(IPrimaryKey key, int spacesToIndent = 0);
        List<string> EnsurePrimaryKeyExistsAsDesignedSql(IPrimaryKey key, int spacesToIndent = 0);
        List<string> DropPrimaryKeyIfExistsSql(IPrimaryKey key, int spacesToIndent = 0);

        // ScalarFunction
        List<string> IDOfScalarFunctionSql(string name, string schemaName = null, IDatabase database = null, int spacesToIndent = 0);
        List<string> ScalarFunctionExistsSql(IScalarFunction function, int spacesToIndent = 0);
        List<string> EnsureScalarFunctionExistsSql(IScalarFunction function, int spacesToIndent = 0);
        List<string> EnsureScalarFunctionExistsAsDesignedSql(IScalarFunction function, int spacesToIndent = 0);
        List<string> DropScalarFunctionIfExistsSql(IScalarFunction function, int spacesToIndent = 0);

        // Schema

        List<string> IDOfSchemaSql(
            string name
            , IDatabase database = null
            , int spacesToIndent = 0
            , bool terminatesStatement = true
            );
        List<string> EnsureSchemaExistsSql(ISchema schema, int spacesToIndent = 0);
        List<string> DropSchemaIfExistsSql(ISchema schema, int spacesToIndent = 0);

        // Sequence
        List<string> IDOfSequenceSql(string name, string schemaName = null, IDatabase database = null, int spacesToIndent = 0);
        List<string> EnsureSequenceExistsSql(IDatabaseSequence sequence, int spacesToIndent = 0);
        List<string> EnsureSequenceExistsAsDesignedSql(IDatabaseSequence sequence, int spacesToIndent = 0);
        List<string> DropSequenceIfExistsSql(IDatabaseSequence sequence, int spacesToIndent = 0);
        List<string> GetNextKeySql(IDatabaseSequence sequence, int spacesToIndent = 0);
        List<string> GetFirstKeyInNextBlockOfNKeysSql(IDatabaseSequence sequence, int n, int spacesToIndent = 0);
        List<string> GetKeysForTableRowsSql(
            IDatabaseSequence sequence
            , string tempTableName
            , string whereClause
            , int spacesToIndent = 0
            );

        // StoredProcedure
        List<string> IDOfStoredProcedureSql(
            string name
            , string schemaName = null
            , IDatabase database = null
            , int spacesToIndent = 0
            , bool terminatesStatement = true
            );
        List<string> EnsureStoredProcedureExistsSql(IStoredProcedure proc, int spacesToIndent = 0);
        List<string> EnsureStoredProcedureExistsAsDesignedSql(IStoredProcedure proc, int spacesToIndent = 0);
        List<string> DropStoredProcedureIfExistsSql(IStoredProcedure proc, int spacesToIndent = 0);

        // Table

        List<string> IDOfTableSql(
            string name
            , string schemaName = null
            , IDatabase database = null
            , int spacesToIndent = 0
            , bool terminatesStatement = true
            );
        List<string> EnsureTableExistsSql(ITable table, int spacesToIndent = 0);
        List<string> EnsureTableExistsAsDesignedSql(ITable table, int spacesToIndent = 0, bool includeForeignKeys = true);
        List<string> EnsureForeignKeysExistAsDesignedSql(ITable table, int spacesToIndent = 0);
        List<string> DropTableIfExistsSql(ITable table, int spacesToIndent = 0);

        Dictionary<string, int> TableColumnsOutputStructure();
        List<string> TableColumnsSql(ITable table, int spacesToIndent = 0);
        List<string> TableIdentityColumnSql(ITable table, int spacesToIndent = 0);
        List<string> TableIdentityColumnNameSql(ITable table, int spacesToIndent = 0);
        List<string> TableComputedColumnsSql(ITable table, int spacesToIndent = 0);

        Dictionary<string, int> VableForeignKeyDependentsOutputStructure();
        List<string> VableForeignKeyDependentsSql(IVable vable, int spacesToIndent = 0);

        // Trigger

        List<string> IDOfTriggerSql(
            string name
            , string tableName
            , string schemaName = null
            , IDatabase database = null
            , int spacesToIndent = 0
            , bool terminatesStatement = true
            );
        List<string> EnsureTriggerExistsSql(ITableTrigger trigger, int spacesToIndent = 0);
        List<string> EnsureTriggerExistsAsDesignedSql(ITableTrigger trigger, int spacesToIndent = 0);
        List<string> DropTriggerIfExistsSql(ITableTrigger trigger, int spacesToIndent = 0);
        string DefaultBeforeInsertTriggerSql(
            ITable table
            , int spacesToIndent = 0
            );
        string DefaultInsertedTriggerTableCreateSql(
            ITable table
            , string tempTableName
            , TriggerTimings timing
            , int spacesToIndent = 0
            );
        string DefaultInsertedTriggerTableLoadSql(
            ITable table
            , string tempTableName
            , int spacesToIndent = 0
            );
        string DefaultCheckNonnullInsertedTriggerTableIDsAgainstParentSql(
            ITable table
            , string tempTableName
            , int spacesToIndent = 0
            );
        string DefaultInsertedTriggerTableAddToParentSql(
            ITable table
            , string tempTableName
            , int spacesToIndent = 0
            );
        string DefaultInsertFromInsertedTriggerTableSql(
            ITable table
            , string tempTableName
            , string currentTimestampSql = null
            , string userSql = null
            , bool retrieveIdentity = true
            , int spacesToIndent = 0
            );
        string DefaultDeletedTriggerTableCreateSql(
            ITable table
            , string tempTableName
            , bool includeMutableColumns
            , TriggerTimings timing
            , int spacesToIndent = 0
            );
        void ResetIdentity(int newValue, IDatabaseSafe db, IConnection conn);

        // UniqueKey

        List<string> IDOfUniqueKeySql(
            string name
            , string tableName
            , string schemaName = null
            , IDatabase database = null
            , int spacesToIndent = 0
            , bool terminatesStatement = true
            );
        List<string> EnsureUniqueKeyExistsSql(IUniqueKey key, int spacesToIndent = 0);
        List<string> EnsureUniqueKeyExistsAsDesignedSql(IUniqueKey key, int spacesToIndent = 0);
        List<string> DropUniqueKeyIfExistsSql(IUniqueKey key, int spacesToIndent = 0);

        // View
        List<string> IDOfViewSql(
            string name
            , string schemaName = null
            , IDatabase database = null
            , int spacesToIndent = 0
            , bool terminatesStatement = true
            );
        List<string> EnsureViewExistsSql(IView view, int spacesToIndent = 0);
        List<string> EnsureViewExistsAsDesignedSql(IView view, int spacesToIndent = 0);
        List<string> DropViewIfExistsSql(IView view, int spacesToIndent = 0);

        // Queues

        string AddIntoQueuesSql(string columnList, string inputList, string schema);

        // Boolean
        string FalseSql();
        string TrueSql();
        string BooleanTypeSql();

        // Transaction management
        string CommitFullySql();
        string RollbackSql();

        // General utility
        string CurrentTimestampSql { get; }
        DateTime CurrentTimestamp( IDatabase db, IConnection conn);
        string CurrentUserSql { get; }
        string CurrentUser(IDatabase db, IConnection conn);
        string TrimSql(string toBeTrimmed);
        int TrimSqlAdditionalLength();
        string NRowedTableSql(
            int n
            , string selectedExpression = null
            , int? adder = null
            , string columnName = null
            , int spacesToIndent = 0
            );
        string ObjectValueToSql(Object obj, string dataType);
        bool IsValidObjectName(string name);
        string ValidObjectNameFrom(string name);
        string TableConstraintName(string root, string tableName);
        string TrimmingConstraintText(string name);
        IObjectType SequencePhysicalObjectType();

    }

}
    
