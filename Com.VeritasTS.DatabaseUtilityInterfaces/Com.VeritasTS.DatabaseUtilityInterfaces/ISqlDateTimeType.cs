﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// This interface groups together the SQL data
    /// types for which IsDateTimeType is true.
    /// </summary>
    public interface ISqlDateTimeType : ISqlDataType { }

    /// <summary>
    /// A basic, immutable implementation of the ISqlInt interface.
    /// </summary>
    public abstract class SqlDateTimeType : SqlDataType, ISqlDateTimeType
    {

        /****************
         *  Private members
         ***************/

        private string FullTypeNameForCodeErrors { get { return typeof(SqlDateTimeType).FullName; } }

        /****************
         *  ISelfAsserter methods via ISelfUsabilityAsserter
         ***************/

        /// <summary>
        /// Asserts the class invariants for this class.
        /// </summary>
        /// <returns>True, unless an assertion failure exception is thrown.</returns>
        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertion(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
            }
            return true;
        }

        /****************
         *  ISqlInt properties and methods
         ***************/

        public override bool IsDateTimeType { get { return true; } }

    }

}
