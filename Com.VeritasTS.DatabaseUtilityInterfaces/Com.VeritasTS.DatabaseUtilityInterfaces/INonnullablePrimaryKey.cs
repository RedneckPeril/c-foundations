﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;


namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface INonnullablePrimaryKey : IPrimaryKey { }

    public interface INonnullablePrimaryKeyMutable : INonnullablePrimaryKey, IPrimaryKeyMutable { }

    /// <summary>
    /// Boilerplate implementation of INonnullablePrimaryKeyMutable.
    /// </summary>
    /// <remarks>
    /// Sealed at present.
    /// </remarks>
    public sealed class NonnullablePrimaryKeyMutable : PrimaryKeyMutable, INonnullablePrimaryKeyMutable
    {

        public NonnullablePrimaryKeyMutable(
            string name
            , ISqlTranslator sqlTranslator
            , ITable table
            , ITableColumn constrainedColumn = null
            , List<string> columnNames = null
            , bool isClustered = true
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , sqlTranslator
                , table
                , ObjectTypes.PrimaryKey()
                , constrainedColumn
                , columnNames
                , isClustered
                , nameWhenPrefixed
                , id
                , retrieveIDsFromDatabase: false
                , assert: false
                )
        {
            CompleteConstructionIfType(typeof(NonnullablePrimaryKeyMutable), retrieveIDsFromDatabase, assert);
        }

        public NonnullablePrimaryKeyMutable(
            string name
            , ITableColumn constrainedColumn
            , List<string> columnNames = null
            , bool isClustered = true
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , ObjectTypes.PrimaryKey()
                , constrainedColumn
                , columnNames
                , isClustered
                , nameWhenPrefixed
                , id
                , retrieveIDsFromDatabase: false
                , assert: false
                )
        {
            CompleteConstructionIfType(typeof(NonnullablePrimaryKeyMutable), retrieveIDsFromDatabase, assert);
        }

        public NonnullablePrimaryKeyMutable(
            ITableColumn constrainedColumn
            , List<string> columnNames = null
            , bool isClustered = true
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                ObjectTypes.PrimaryKey()
                , constrainedColumn
                , columnNames
                , isClustered
                , nameWhenPrefixed
                , id
                , retrieveIDsFromDatabase: false
                , assert: false
                )
        {
            CompleteConstructionIfType(typeof(NonnullablePrimaryKeyMutable), retrieveIDsFromDatabase, assert);
        }

        // IDataProcObject method overrides
        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                string tableName = Table.Name;
                Dictionary<string, Tuple<int, IColumn>> columns = Table.Columns;
                foreach (string name in ColumnNames)
                {
                    Debug.Assert(
                        !columns[name].Item2.IsNullable
                        , "Column '" + name + "' on table '" + tableName + "' is nullable and therefore "
                        + "cannot be part of nonnullable primary key '" + Name + "'."
                        );
                }
                foreach (
                    KeyValuePair<string,Tuple<int,IColumn>> pair
                    in Table.Columns)
                if (ColumnNames.Count == 1)
                {

                }
            }
            return true;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new NonnullablePrimaryKeyMutable(
                Name
                , GetSqlTranslatorSafely()
                , GetTableSafely()
                , GetConstrainedColumnSafely()
                , GetColumnNamesSafely()
                , IsClustered
                , NameWhenPrefixed
                , ID
                , false
                , assert
                );
        }

        // ObjectInSchemaMutable method overrides

        public override IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            RejectIfInvalidParentObjectType(parentObject);
            return new NonnullablePrimaryKeyMutable(
                Name
                , SqlTranslator
                , (ITable)parentObject
                , ConstrainedColumn
                , ColumnNames
                , IsClustered
                , NameWhenPrefixed
                , ID
                , retrieveIDsFromDatabase
                , assert
                );
        }

    }


    /// <summary>
    /// Boilerplate implementation of IPrimaryKey without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>PotentiallySingleColumnConstraintImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetCopyForColumn, always unless abstract class.</item>
    ///   <item>GetCopyForTable, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public sealed class NonnullablePrimaryKeyImmutable : PrimaryKeyImmutable, INonnullablePrimaryKey
    {

        private INonnullablePrimaryKey Delegee { get { return (INonnullablePrimaryKey)_delegee; } }

        public NonnullablePrimaryKeyImmutable(
            string name
            , ISqlTranslator sqlTranslator
            , ITable table
            , ITableColumn constrainedColumn = null
            , List<string> columnNames = null
            , bool isClustered = true
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base()
        {
            _delegee
                = (INonnullablePrimaryKey)
                new NonnullablePrimaryKeyMutable(
                    name
                    , sqlTranslator
                    , table
                    , constrainedColumn
                    , columnNames
                    , isClustered
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    ).GetSafeReference();
            CompleteConstructionIfType(typeof(NonnullablePrimaryKeyImmutable), retrieveIDsFromDatabase: false, assert: true);
        }

        public NonnullablePrimaryKeyImmutable(
            string name
            , ITableColumn constrainedColumn = null
            , List<string> columnNames = null
            , bool isClustered = true
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base()
        {
            _delegee
                = (INonnullablePrimaryKey)
                new NonnullablePrimaryKeyMutable(
                    name
                    , constrainedColumn
                    , columnNames
                    , isClustered
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    ).GetSafeReference();
            CompleteConstructionIfType(typeof(NonnullablePrimaryKeyImmutable), retrieveIDsFromDatabase: false, assert: true);
        }

        public NonnullablePrimaryKeyImmutable(
            ITableColumn constrainedColumn = null
            , List<string> columnNames = null
            , bool isClustered = true
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base()
        {
            _delegee
                = (INonnullablePrimaryKey)
                new NonnullablePrimaryKeyMutable(
                    constrainedColumn
                    , columnNames
                    , isClustered
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    ).GetSafeReference();
            CompleteConstructionIfType(typeof(NonnullablePrimaryKeyImmutable), retrieveIDsFromDatabase: false, assert: true);
        }

        public NonnullablePrimaryKeyImmutable(
            INonnullablePrimaryKey delegee
            , bool assert = true
            ) : base(delegee, false)
        {
            CompleteConstructionIfType(typeof(NonnullablePrimaryKeyImmutable), retrieveIDsFromDatabase: false, assert: true);
        }

        // DataProcObjectImmutable method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(INonnullablePrimaryKey); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return
                new NonnullablePrimaryKeyImmutable(
                    (INonnullablePrimaryKey)
                    Delegee.GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, false)
                    );
        }

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return
                new NonnullablePrimaryKeyImmutable(
                    (INonnullablePrimaryKey)
                    Delegee.GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, false)
                    );
        }

        // PotentiallySingleColumnConstraintImmutable method overrides

        public override IPotentiallySingleColumnConstraint GetCopyForColumn(ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return new NonnullablePrimaryKeyImmutable(
                    (INonnullablePrimaryKey)
                Delegee.GetCopyForColumn(
                    constrainedColumn
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }
        public override IPotentiallySingleColumnConstraint GetCopyForTable(ITable table, ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return new NonnullablePrimaryKeyImmutable(
                    (INonnullablePrimaryKey)
                Delegee.GetCopyForTable(
                    table
                    , constrainedColumn
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }

    }

}
