﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    //public class TriggerStoredProcedureParametersStringOtherThanDefaults
    //{
    //    // some parameters other than temptable(s) and tablename in the trigger creation code
    //    // eg. ", @param1 = 'value1', @param2 = 2"
    //    public string ParametersString { get; set; }
    //}
    public interface IInsteadOfUpdateTableTrigger : ITableTrigger
    {
    }
    public interface IInsteadOfUpdateTableTriggerMutable : IInsteadOfUpdateTableTrigger, ITableTriggerMutable
    {
    }

    /// <summary>
    /// Boilerplate implementation of ITableTriggerMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides.</item>
    ///   <list type="bullet">
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaMutable method overrides
    ///   <list type="bullet">
    ///   <item>ObjectIsValidTypeForParentObject, usually unless abstract type.</item>
    ///   <item>ParentObjectMustBeOwner, if true rather than false.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always.</item>
    ///   <item>SetParentObject, rarely, e.g. when ParentObject should always be some
    ///   member other than Owner and vice versa.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class InsteadOfUpdateTriggerMutable : TableTriggerMutable, IInsteadOfTableTriggerMutable
    {

        // Construction
        public InsteadOfUpdateTriggerMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , sqlTranslator
                , table
                , TriggerTimings.InsteadOf
                , nameWhenPrefixed
                , id
                )
        {
            SetTriggerText(null);
            Debug.Assert(PassesSelfAssertion(typeof(TriggerMutable)));
        }

        // DataProcObjectMutable method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    Procedure != null
                    , "Procedure property of ITableTrigger logic "
                    + "cannot be null."
                    );
                Debug.Assert(Procedure.PassesSelfAssertion());
                Debug.Assert(
                    TriggerText != null
                    , "TriggerText property of ITableTrigger logic "
                    + "cannot be null."
                    );
                Debug.Assert(
                    TriggerText.Length > 0
                    , "TriggerText property of ITableTrigger logic "
                    + "cannot be the empty string."
                    );
                Debug.Assert(
                    new Regex("\\S").IsMatch(TriggerText)
                    , "TriggerText property of ITableTrigger logic "
                    + "cannot be purely whitespace."
                    );
            }
            return true;
        }

        public override bool MembersMatch(object obj)
        {
            if (!base.MembersMatch(obj)) return false;
            if (!typeof(ITableTrigger).IsAssignableFrom(obj.GetType())) return false;
            ITableTrigger other = (ITableTrigger)obj;
            return
                TriggerTiming == other.TriggerTiming
                && EqualityUtilities.EqualOrBothNull(Procedure, other.Procedure)
                && TriggerText == other.TriggerText;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new TriggerMutable(
                Name
                , GetSqlTranslatorSafely()
                , GetTableSafely()
                , TriggerTiming
                , NameWhenPrefixed
                , ID
                , false
                , true
                );
        }

        // DatabaseObjectMutable method overrides

        public override void RetrieveIDsFromDatabase(bool assert = true)
        {
            if (Database != null && Database.IsConnected)
            {
                if (
                    Procedure != null
                    && typeof(IStoredProcedureMutable).IsAssignableFrom(Procedure.GetType())
                    )
                    ((IStoredProcedureMutable)Procedure).RetrieveIDsFromDatabase(assert);
                base.RetrieveIDsFromDatabase(assert);
            }
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        // IObjectInSchema method overrides

        public override bool ParentObjectMustBeOwner { get { return true; } }

        public override IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            RejectIfInvalidParentObjectType(parentObject);
            bool retrieveIDs
                = retrieveIDsFromDatabase != null && retrieveIDsFromDatabase.HasValue
                ? retrieveIDsFromDatabase.Value
                : ID != null;
            return new TriggerMutable(
                    Name
                    , SqlTranslator
                    , (ITable)parentObject
                    , TriggerTiming
                    , NameWhenPrefixed
                    , ID
                    , retrieveIDs
                    , assert
                    );
        }

        // ITableTrigger properties and methods

        public TriggerTimings TriggerTiming
        {
            get { return _triggerTiming; }
            set { SetTriggerTiming(value); }
        }
        public bool IsBeforeUpdate { get { return TriggerTiming.Equals(BeforeUpdate); } }
        public bool IsBeforeUpdate { get { return TriggerTiming.Equals(BeforeUpdate); } }
        public bool IsBeforeDelete { get { return TriggerTiming.Equals(BeforeDelete); } }
        public bool IsAfterUpdate { get { return TriggerTiming.Equals(AfterUpdate); } }
        public bool IsAfterAnything { get { return TriggerTiming.Equals(AfterAnything); } }
        public IStoredProcedure Procedure
        {
            get { return _procedure; }
            set { SetProcedure(value); }
        }
        public string TriggerText
        {
            get { return _triggerText; }
            set { SetTriggerText(value); }
        }

        public IStoredProcedure GetProcedureSafely()
        {
            IStoredProcedure procedure = Procedure;
            if (procedure == null) return null;
            else return (IStoredProcedure)procedure.GetSafeReference();
        }

        public string UpdateTriggerSql()
        {
            //string tempTable;
            //var temp = GetStoredProcedureParamter("TempTable");
            //if (temp != null) tempTable = temp.ToString();
            //else tempTable = "#" + Name + "_i";

            string tempTable = "#" + Name + "_i";

            ITable table = (ITable)Owner;
            //table.RefreshColumnsFromDb();
            string columns = table.UpdateColumnListSql();
            string callSp = "";
            if (Procedure != null)
            {
                string ParameterList = "@tempTable = '" + tempTable + "', @tableName = '" + Owner.Name + "'";
                callSp = "exec " + Procedure.Schema.Name + "." + Procedure.Name + " " + ParameterList + ";\n\n";
            }

            string sql = "create trigger " + table.Schema.Name + "." + Name + "\n"
                + "on " + table.Schema.Name + "." + table.Name + "\n"
                + "instead of insert as " + "\n"
                + "begin" + "\n"
                + "set nocount on;" + "\n"
                + "set ansi_warnings, ansi_defaults, xact_abort on;" + "\n\n"
                + "declare @user nvarchar(50) = original_login();\n"
                + "declare @dtm datetime = current_timestamp;\n"
                + "exec dbo.DropTempTableIfExists '" + tempTable + "';" + "\n\n"
                + "if exists (select * from inserted) begin\n"
                + "select " + columns + "\n"
                    + " into " + tempTable + "\n"
                    + " from inserted i;" + "\n\n";
            sql += "update " + tempTable + " set LastUpdater = @user; \n";
            sql += "update " + tempTable + " set LastUpdateDtm = @dtm; \n\n";
            sql += callSp;
            sql += "insert into " + table.Schema.Name + "." + table.Name + "(" + columns + ") \n"
                    + "select " + columns + " from " + tempTable + ";\n\n"
                    + "end\n"
                    + "end";
            return sql;
        }

        public string UpdateTriggerSql()
        {
            string tempTableI = "#" + Name + "_i";
            string tempTableD = "#" + Name + "_d";

            ITable table = (ITable)Owner;
            bool updateDtm = table.HasLastUpdateDtmColumn;
            bool updateUser = table.HasLastUpdaterColumn;
            string callSp = "";
            if (Procedure != null)
            {
                string ParameterList = "@tempTableIns = '" + tempTableI + "', @tempTableDel = '" + tempTableD + "', @tableName = '" + Owner.Name + "'";
                callSp = "exec " + Procedure.Schema.Name + "." + Procedure.Name + " " + ParameterList + ";\n\n";
            }

            string sql = "create trigger " + table.Schema.Name + "." + Name + "\n"
                + "on " + table.Schema.Name + "." + table.Name + "\n"
                + "instead of update as " + "\n"
                + "begin" + "\n"
                + "set nocount on;" + "\n"
                + "set ansi_warnings, ansi_defaults, xact_abort on;" + "\n\n"
                + "declare @user nvarchar(50) = original_login();\n\n"
                + "exec dbo.DropTempTableIfExists '" + tempTableI + "';" + "\n"
                + "exec dbo.DropTempTableIfExists '" + tempTableD + "';" + "\n\n"
                + "if exists (select * from inserted) begin\n"
                + "select " + table.UpdateColumnListSql() + " into " + tempTableI + " from inserted;" + "\n"
                + "select " + table.UpdateColumnListSql() + " into " + tempTableD + " from deleted;" + "\n\n";
            if (updateUser) sql += "update " + tempTableI + " set LastUpdater = @user; \n";
            if (updateDtm) sql += "update " + tempTableI + " set LastUpdateDtm = getdate(); \n\n";
            sql += callSp;

            var primaryColumns = table.PrimaryKey.ColumnNames;
            var columns = table.Columns.Where(c => !c.IsComputed && (!c.IsImmutable || primaryColumns.FirstOrDefault(k => k == c.Name) != null));

            sql += "merge into " + table.Schema.Name + "." + table.Name + " o \n"
                + "using " + tempTableI + " i\n"
                + "on (";
            int i = 0;
            foreach (var pc in primaryColumns)
            {
                if (i == 0)
                    sql += "i." + pc + " = o." + pc;
                else
                    sql += " and i." + pc + " = o." + pc;
                i++;
            }
            sql += ")\n"
                + "when matched then update set\n";
            columns = columns.Where(c => !c.IsImmutable);
            i = 0;
            foreach (var c in columns)
            {
                if (i > 0) sql += ", ";
                sql += "o." + c.Name + " = i." + c.Name + "\n";
                i++;
            }
            sql += ";\n"
                + "end\n"
                + "end";
            return sql;
        }

        public string DeleteTriggerSql()
        {
            //string tempTable;
            //var temp = GetStoredProcedureParamter("TempTable");
            //if (temp != null) tempTable = temp.ToString();
            //else tempTable = "#" + Name + "_d";

            string tempTable = "#" + Name + "_d";


            ITable table = (ITable)Owner;
            string tableName = table.Schema.Name + "." + table.Name;
            string callSp = "";
            if (Procedure != null)
            {
                string ParameterList = "@tempTable = '" + tempTable + "', @tableName = '" + Owner.Name + "'";
                callSp = "exec " + Procedure.Schema.Name + "." + Procedure.Name + " " + ParameterList + ";\n\n";
            }

            string sql = "create trigger " + table.Schema.Name + "." + Name + "\n"
                + "on " + tableName + "\n"
                + "instead of delete as " + "\n"
                + "begin" + "\n"
                + "set nocount on;" + "\n"
                + "set ansi_warnings, ansi_defaults, xact_abort on;" + "\n\n"
                + "exec dbo.DropTempTableIfExists '" + tempTable + "';" + "\n\n"
                + "if exists (select * from deleted) begin\n"
                + "select id into " + tempTable + " from deleted;\n";
            sql += callSp;
            sql += "delete from " + tableName + " where id in (select id from " + tempTable + ");\n "
                    + "end\n"
                    + "end";
            return sql;
        }

        public string AfterUpdateTriggerSql()
        {
            string sql = "";
            ITable table = (ITable)Owner;
            if (table.Name.Substring(table.Name.Length - 4).Equals("Jobs"))
            {
                sql = "create trigger " + table.Schema.Name + "." + Name + "\n"
                    + "on " + table.Schema.Name + "." + table.Name + "\n"
                    + "after insert as " + "\n"
                    + "begin" + "\n"
                    + "    exec vtsQs.SendQueueJobsReadyMsgCaller;\n"
                    + "end";
            }
            return sql;
        }

        public string AfterAnythingTriggerSql()
        {
            ITable table = (ITable)Owner;
            if (!table.IsAudited && !table.IsQueued)
                return "";

            string tempTableI = "#" + Name + "_i";
            string tempTableD = "#" + Name + "_d";
            string callSp = "";
            if (Procedure != null)
            {
                string ParameterList = "@tempTableIns = @tempIns, @tempTableDel = @tempDel, @tableName = '" + Owner.Name + "'";
                callSp = "exec " + Procedure.Schema.Name + "." + Procedure.Name + " " + ParameterList + ";\n\n";
            }

            string sql = "create trigger " + table.Schema.Name + "." + Name + "\n"
                + "on " + table.Schema.Name + "." + table.Name + "\n"
                + "after insert, update, delete as " + "\n"
                + "begin" + "\n"
                + "set nocount on;" + "\n"
                + "set ansi_warnings, ansi_defaults, xact_abort on;" + "\n\n"
                + "declare @user nvarchar(50) = original_login();\n"
                + "declare @tempIns nvarchar(100) = '" + tempTableI + "'\n"
                + "declare @tempDel nvarchar(100) = '" + tempTableD + "'\n\n"
                + "exec dbo.DropTempTableIfExists '" + tempTableI + "';" + "\n"
                + "exec dbo.DropTempTableIfExists '" + tempTableD + "';" + "\n\n"
                + "if exists (select * from inserted)\n"
                + "select " + table.UpdateColumnListSql() + ", @user cur_user into " + tempTableI + " from inserted;" + "\n"
                + "else\n"
                + "set @tempIns = '';\n\n"
                + "if exists (select * from deleted)\n"
                + "select " + table.UpdateColumnListSql() + ", @user cur_user into " + tempTableD + " from deleted;" + "\n"
                + "else\n"
                + "set @tempDel = '';\n\n";
            sql += "if @tempDel != '' and @tempIns != '' begin\n"
                + "if not exists (select * from " + tempTableI + " except select * from " + tempTableD + ") begin\n"
                + "  set @tempIns = '';\n"
                + "  set @tempDel = '';\n"
                + "end;\nend;\n\n";
            sql += callSp
                + "end";

            return sql;
        }

        /****************
         * ITableTriggerMutable methods
        ****************/

        public void SetTriggerTiming(TriggerTimings value, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            _triggerTiming = value;
            string currentName = Name;
            bool retrieveIDs = false;
            if (
                currentName == null
                || new Regex("((_io[IUD])|(_aI(UD){0,1}))\\s*$").IsMatch(currentName)
                )
            {
                string extension = "";
                retrieveIDs
                    = retrieveIDsFromDatabase == null || !retrieveIDsFromDatabase.HasValue
                    ? true
                    : retrieveIDsFromDatabase.Value;
                switch (value)
                {
                    case TriggerTimings.BeforeUpdate:
                        extension = "_ioI";
                        break;
                    case TriggerTimings.BeforeUpdate:
                        extension = "_ioU";
                        break;
                    case TriggerTimings.BeforeDelete:
                        extension = "_ioD";
                        break;
                    case TriggerTimings.AfterUpdate:
                        extension = "_aI";
                        break;
                    default: // TriggerTimings.AfterAnything:
                        extension = "_aIUD";
                        break;
                }
                SetName(
                    ParentObject.Name.TrimEnd() + extension
                    , false
                    );
            }
            if (retrieveIDs) RetrieveIDsFromDatabase(false);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public void SetProcedure(
            IStoredProcedure value
            , bool? retrieveProcedureIDFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            bool retrieveID
                = retrieveProcedureIDFromDatabase != null && retrieveProcedureIDFromDatabase.HasValue
                ? retrieveProcedureIDFromDatabase.Value
                : ID != null;
            _procedure = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public void SetTriggerText(string value, bool assert = true)
        {
            if (value == null || value.Trim().Length == 0)
            {
                switch (TriggerTiming)
                {
                    case TriggerTimings.BeforeUpdate:
                        _triggerText = UpdateTriggerSql();
                        break;
                    case TriggerTimings.BeforeUpdate:
                        _triggerText = UpdateTriggerSql();
                        break;
                    case TriggerTimings.BeforeDelete:
                        _triggerText = DeleteTriggerSql();
                        break;
                    case TriggerTimings.AfterUpdate:
                        _triggerText = AfterUpdateTriggerSql();
                        break;
                    default: // TriggerTimings.AfterAnything:
                        _triggerText = AfterAnythingTriggerSql();
                        break;
                }
            }
            _triggerText = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        protected string GetStoredProcedureParamtersList()
        {
            return Procedure.AdditionalParametersString.ParametersString == null ? "" : Procedure.AdditionalParametersString.ParametersString;

            if (Procedure.AdditionalParametersString == null) return "";

            string ParameterList = "";

            Type type = Procedure.AdditionalParametersString.GetType();
            IEnumerable<PropertyInfo> props = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var prop in props)
            {
                if (prop.GetValue(Procedure.AdditionalParametersString) != null)
                {
                    if (prop.PropertyType.Name.Equals("String"))
                        ParameterList += ", @" + prop.Name + " = '" + prop.GetValue(Procedure.AdditionalParametersString) + "'\n";
                    else
                        ParameterList += ", @" + prop.Name + " = " + prop.GetValue(Procedure.AdditionalParametersString) + "\n";
                }
            }
            return ParameterList;
        }

        protected object GetStoredProcedureParamter(string parameterName)
        {
            if (Procedure.Parameters == null) return null;

            Type type = Procedure.Parameters.GetType();
            var value = type.GetProperty(parameterName).GetValue(Procedure.Parameters);
            return value;
        }

        protected string GetStoredProcedureParentPassthroughList(IParentPassThroughInfo passthrough)
        {
            string list = "";
            if (passthrough == null) return list;

            IColumn column = (IColumn)passthrough.GetType().GetProperty("ParentPassthroughColumn").GetValue(passthrough);
            list += "@ParentPassthroughColumn = '" + column.Name + "',\n";
            ITable table = (ITable)passthrough.GetType().GetProperty("ParentPassthroughSourceTable").GetValue(passthrough);
            list += "@ParentPassthroughSourceTable = '" + table.Schema.Name + "." + table.Name + "',\n";
            column = (IColumn)passthrough.GetType().GetProperty("ParentPassthroughSourceColumn").GetValue(passthrough);
            list += "@ParentPassthroughSourceColumn = '" + column.Name + "',\n";
            column = (IColumn)passthrough.GetType().GetProperty("ParentPassthroughMatchingSourceColumn").GetValue(passthrough);
            list += "@ParentPassthroughMatchingSourceColumn = '" + column.Name + "',\n";
            list += "@ParentPassthroughLookup = '" + (string)passthrough.GetType().GetProperty("ParentPassthroughLookup").GetValue(passthrough) + "',\n";

            return list;
        }

        protected string GetStoredProcedurePKSequenceList(IPrimaryKeySequenceMap seq)
        {
            string list = "";
            if (seq == null) return list;

            IColumn column = (IColumn)seq.GetType().GetProperty("PrimaryKeyColumn").GetValue(seq);
            list += "@PrimaryKeyColumn = '" + column.Name + "',\n";
            ITable table = (ITable)seq.GetType().GetProperty("TableAsSequence").GetValue(seq);
            list += "@TableAsSequence = '" + table.Schema.Name + "." + table.Name + "',\n";
            column = (IColumn)seq.GetType().GetProperty("IdentityColumn").GetValue(seq);
            list += "@IdentityColumn = '" + column.Name + "',\n";
            column = (IColumn)seq.GetType().GetProperty("DummyUpdateColumn").GetValue(seq);
            list += "@DummyUpdateColumn = '" + column.Name + "',\n";
            return list;
        }
    }
}
