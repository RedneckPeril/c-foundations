﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using Com.VeritasTS.TextAndErrorUtils;


using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface IDatabase : ISelfAsserter, IMemberMatcher, IComparableByFullName, IComparableByName, IConnectionSupplier
    {

        /// <summary>
        /// True if object is variant after completion of
        /// logical construction, else false.
        /// </summary>
        /// <remarks>
        /// Invariance is by far the best way to ensure
        /// thread-safety.
        /// </remarks>
        bool IsMutableType { get; }

        /// <summary>
        /// The most closely related variant type to the
        /// type of the current object.
        /// </summary>
        /// <remarks>
        /// If the type itself is variant, returns the
        /// object's own type. If no variant type is deemed
        /// adequately similar, returns null. An "adequately
        /// similar" type is one (a) that implements all
        /// interfaces implemented by the object itself,
        /// and (b) whose primary constructor
        /// parameters are interchangeable with the 
        /// parameters of the object's own primary 
        /// constructor.
        /// <para>
        /// By default, the NearestMutableType of invariant type 
        /// ATypeNamedSomethingImmutable is a variant type
        /// ATypeNamedSomethingMutable, and 
        /// the NearestMutableType of invariant type 
        /// ATypeNamedSomething is also a variant type
        /// ATypeNamedSomethingMutable.</para>
        /// </remarks>
        Type NearestMutableType { get; }

        IServer Server { get; }
        IConnectionSupplier ConnectionSupplier { get; }
        ISqlTranslator SqlTranslator { get; }
        bool IsConnected { get; }
        string Description { get; }
        ICascadingForeignKeyRegistry CascadingForeignKeyRegistry { get; }
        IChangeSubscriberRegistry ChangeSubscriberRegistry { get; }
        IServer GetServerSafely();
        ISqlTranslator GetSqlTranslatorSafely();
        ICascadingForeignKeyRegistry GetCascadingForeignKeyRegistrySafely();
        IChangeSubscriberRegistry GetChangeSubscriberRegistrySafely();
        List<string[]> SqlExecuteReader(
            string sql
            , IConnection conn = null
            , ITransaction trans = null
            , List<ISqlParameter> parameters = null
            );
        List<string[]> SqlExecuteReader(
            List<string> script
            , IConnection conn = null
            , ITransaction trans = null
            , List<ISqlParameter> parameters = null
            , bool swallowExceptions = false
            );
        object SqlExecuteSingleValue(
            string sql
            , IConnection conn = null
            , ITransaction trans = null
            , List<ISqlParameter> parameters = null
            );
        /// <summary>
        /// Executes a script, the last statement of which 
        /// returns a single value.
        /// </summary>
        /// <param name="script">The script to be executed.</param>
        /// <param name="conn">Optional connection provided by caller.</param>
        /// <param name="trans">Optional transaction provided by caller.</param>
        /// <returns></returns>
        object SqlExecuteSingleValue(
            List<string> script
            , IConnection conn = null
            , ITransaction trans = null
            , List<ISqlParameter> parameters = null
            , bool swallowExceptions = false
            );
        bool SqlExecuteNonQuery(
            string sql
            , IConnection conn = null
            , ITransaction trans = null
            , List<ISqlParameter> parameters = null
            , bool swallowExceptions = false
            );
        bool SqlExecuteNonQuery(
            List<string> script
            , IConnection conn = null
            , ITransaction trans = null
            , List<ISqlParameter> parameters = null
            , bool swallowExceptions = false
            );

        //object SqlExecScalar(string sql, IConnection conn = null, ITransaction trans = null);
        ///// <summary>
        ///// Executes a script, the last statement of which 
        ///// returns a scalar.
        ///// </summary>
        ///// <param name="script">The script to be executed.</param>
        ///// <param name="conn">Optional connection provided by caller.</param>
        ///// <param name="trans">Optional transaction provided by caller.</param>
        ///// <returns></returns>
        //object SqlExecScalar(
        //    List<string> script
        //    , IConnection conn = null
        //    , ITransaction trans = null
        //    , bool swallowExceptions = false
        //    );
        List<GenNameValue> SqlExecuteStoredProcedure(
            string procedure
            , List<ISqlParameter> parameters = null
            , IConnection conn = null
            , ITransaction trans = null
            );
        ITransaction BeginTransaction(IConnection conn);
        void CommitFully( IConnection conn, ITransaction trans = null);
        void Rollback(IConnection conn, ITransaction trans = null);

        // IDatabase methods: Generic object manipulation methods
        int? IDOfObject(IDatabaseObject obj);
        bool ObjectExists(IDatabaseObject obj);
        void EnsureObjectExists(IDatabaseObject obj);
        void EnsureObjectExistsAsDesigned(IDatabaseObject obj);
        void DropObjectIfExists(IDatabaseObject obj);

        // IDatabase methods: database manipulation methods
        int? IDOfDatabase(string name, IServer server);
        bool DatabaseExists(IDatabase database);
        void EnsureDatabaseExists(IDatabase database);
        void DropDatabaseIfExists(IDatabase database);

        // IDatabase methods: check constraint manipulation methods
        int? IDOfCheckConstraint(string name, string tableName, string schemaName = null);
        bool CheckConstraintExists(ICheckConstraint constraint);
        void EnsureCheckConstraintExists(ICheckConstraint constraint);
        void EnsureCheckConstraintExistsAsDesigned(ICheckConstraint constraint);
        void DropCheckConstraintIfExists(ICheckConstraint constraint);

        // IDatabase methods: column manipulation methods
        int? IDOfColumn(
            string name
            , string vableName
            , string schemaName = null
            );
        bool ColumnExists(IColumn column);
        void EnsureTableColumnExists(ITableColumn column);
        void EnsureTableColumnExistsAsDesigned(ITableColumn column);
        void DropTableColumnIfExists(ITableColumn column);

        // IDatabase methods: Default constraint manipulation methods
        int? IDOfDefaultConstraint(
            string name
            , string tableName
            , string schemaName = null
            );
        bool DefaultConstraintExists(IDefaultConstraint constraint);
        void EnsureDefaultConstraintExists(IDefaultConstraint constraint);
        void EnsureDefaultConstraintExistsAsDesigned(IDefaultConstraint constraint);
        void DropDefaultConstraintIfExists(IDefaultConstraint constraint);

        // IDatabase methods: Foreign key manipulation methods
        int? IDOfForeignKey(
            string name
            , string tableName
            , string schemaName = null
            );
        bool ForeignKeyExists(IForeignKey key);
        void EnsureForeignKeyExists(IForeignKey key, bool cascadeManually = false);
        void EnsureForeignKeyExistsAsDesigned(IForeignKey key, bool cascadeManually = false);
        void DropForeignKeyIfExists(IForeignKey key);

        // IDatabase methods: index manipulation methods
        int? IDOfIndex(
            string name
            , string tableName
            , string schemaName = null
            );
        bool IndexExists(IIndex index);
        void EnsureIndexExists(IIndex index);
        void EnsureIndexExistsAsDesigned(IIndex index);
        void DropIndexIfExists(IIndex index);

        // IDatabase methods: primary key manipulation methods
        int? IDOfPrimaryKey(
            string name
            , string tableName
            , string schemaName = null
            );
        bool PrimaryKeyExists(IPrimaryKey key);
        void EnsurePrimaryKeyExists(IPrimaryKey key);
        void EnsurePrimaryKeyExistsAsDesigned(IPrimaryKey key);
        void DropPrimaryKeyIfExists(IPrimaryKey key);

        // IDatabase methods: scalar function manipulation methods
        int? IDOfScalarFunction(
            string name
            , string schemaName = null
            );
        bool ScalarFunctionExists(IScalarFunction function);
        void EnsureScalarFunctionExists(IScalarFunction function);
        void EnsureScalarFunctionExistsAsDesigned(IScalarFunction function);
        void DropScalarFunctionIfExists(IScalarFunction function);

        // IDatabase methods: schema manipulation methods
        int? IDOfSchema(
           string name
           );
        bool SchemaExists(ISchema schema);
        void EnsureSchemaExists(ISchema schema);
        void DropSchemaIfExists(ISchema schema);

        // IDatabase methods: Sequence manipulation methods
        int? IDOfSequence(
            string name
            , string schemaName = null
            );
        bool SequenceExists(IDatabaseSequence sequence);
        void EnsureSequenceExists(IDatabaseSequence sequence);
        void EnsureSequenceExistsAsDesigned(IDatabaseSequence sequence);
        void DropSequenceIfExists(IDatabaseSequence sequence);

        // IDatabase methods: stored procedure manipulation methods
        int? IDOfStoredProcedure(
           string name
           , string schemaName = null
           );
        bool StoredProcedureExists(IStoredProcedure proc);
        void EnsureStoredProcedureExists(IStoredProcedure proc);
        void EnsureStoredProcedureExistsAsDesigned(IStoredProcedure proc);
        void DropStoredProcedureIfExists(IStoredProcedure proc);

        // IDatabase methods: table manipulation methods
        int? IDOfTable(
           string name
           , string schemaName = null
           );
        bool TableExists(ITable table);
        void EnsureTableExists(ITable table);
        void EnsureTableExistsAsDesigned(ITable table);
        void DropTableIfExists(ITable table);
        List<IColumn> TableColumnsInDatabase(ITable table);
        List<IColumn> TableIdentityColumnInDatabase(ITable table);
        List<IColumn> TableComputedColumnsInDatabase(ITable table);
        List<string[]> TableForeignKeyDependentsInDatabase(ITable table);

        // IDatabase methods: trigger manipulation methods
        int? IDOfTrigger(
            string name
            , string tableName
            , string schemaName = null
            );
        bool TriggerExists(ITableTrigger trigger);
        void EnsureTriggerExists(ITableTrigger trigger);
        void EnsureTriggerExistsAsDesigned(ITableTrigger trigger);
        void DropTriggerIfExists(ITableTrigger trigger);

        // IDatabase methods: unique key manipulation methods
        int? IDOfUniqueKey(
           string name
           , string tableName
           , string schemaName = null
           );
        bool UniqueKeyExists(IUniqueKey key);
        void EnsureUniqueKeyExists(IUniqueKey key);
        void EnsureUniqueKeyExistsAsDesigned(IUniqueKey key);
        void DropUniqueKeyIfExists(IUniqueKey key);

        // IDatabase methods: user manipulation methods
        int? IDOfUser(string name);
        bool UserExists(IUser user);
        void EnsureUserExists(IUser user);
        void EnsureUserExistsAsDesigned(IUser user);
        void DropUserIfExists(IUser user);

        // IDatabase methods: view manipulation methods
        int? IDOfView(
           string name
           , string schemaName = null
           );
        bool ViewExists(IView view);
        void EnsureViewExists(IView view);
        void EnsureViewExistsAsDesigned(IView view);
        void DropViewIfExists(IView view);

        // IDatabase methods: SQL construction methods

        bool IsValidObjectName(string name);
        string ValidObjectNameFrom(string name);
        string TableConstraintName(string root, string tableName);
    }
    public interface IDatabaseSafe : IDatabase, ISafeMemberOfImmutableOwner
    {
        IConnectionSupplierSafe GetConnectionSupplierSafely();
    }
    public interface IDatabaseMutable : IDatabase, IMutable
    {
        void SetServer(IServer value);
        void SetConnectionSupplier(IConnectionSupplier value);
        void SetSqlTranslator(ISqlTranslator value);
        void SetCascadingForeignKeyRegistry(ICascadingForeignKeyRegistry value);
        void SetChangeSubscriberRegistry(IChangeSubscriberRegistry value);
    }

    public class DatabaseCodeMismatchException : Exception
    {
        public DatabaseCodeMismatchException(string message) : base(message) { }
    }

    public class UnsafeConnectionSupplierException : Exception
    {
        public UnsafeConnectionSupplierException(IConnectionSupplier supplier)
            : base(
                  message: "Attempted to get a threadsafe-by-invariance copy of an IConnectionSupplier "
                + "of type " + supplier.GetType().ToString()
                + ", which does not support thread safety by invariance."
                  )
        {
            Supplier = supplier;
        }
        public IConnectionSupplier Supplier { get; }
    }

    public class DirectManipulationOfViewColumnException : Exception
    {
        private DirectManipulationOfViewColumnException() { }
        public DirectManipulationOfViewColumnException(
            IColumn column
            , string columnFullName
            , string columnTypeName
            , string callingMethod
            , string message = null
            ) : base(
                message != null ? message
                : "Attempted to directly manipulate, in the database, column '"
                + (columnFullName != null ? columnFullName : "[unnamed]")
                + "' of non-table-column type "
                + (columnTypeName != null ? columnTypeName : "[unnamed]")
                + " by calling the method "
                + callingMethod
                + ". This method can only be called for table columns."
                )
        {

        }
        IColumn BadColumn { get; }
    }

    public class DatabaseMutable : ComparableByNameMutable, IDatabaseMutable
    {

        private string FullTypeNameForCodeErrors { get { return typeof(DatabaseMutable).FullName; } }

        private DatabaseMutable(): base("") { }

        public DatabaseMutable( 
            string name = null
            , IServer server = null
            , IConnectionSupplier connectionSupplier = null
            , ISqlTranslator translator = null 
            , ICascadingForeignKeyRegistry cascadingForeignKeyRegistry = null
            , IChangeSubscriberRegistry changeSubscriberRegistry = null
            , bool assert = true
            ): base( name )
        {
            Server = server;
            ConnectionSupplier = connectionSupplier;
            SqlTranslator = translator;
            CascadingForeignKeyRegistry = cascadingForeignKeyRegistry;
            ChangeSubscriberRegistry = changeSubscriberRegistry;
            HERE HERE HERE
        }

        // Object method overrides
        public override bool Equals(object obj)
        {
            return EqualityUtilities.MemberMatchwiseEqualOrBothNull(this, obj);
        }

        public override int GetHashCode()
        {
            return (Name == null ? 0 : Name.GetHashCode())
                + 7 * (Server == null ? 0 : Server.GetHashCode())
                + 11 * (ConnectionSupplier == null ? 0 : ConnectionSupplier.GetHashCode())
                + 13 * (SqlTranslator == null ? 0 : SqlTranslator.GetHashCode())
                + 17 * (CascadingForeignKeyRegistry == null ? 0 : CascadingForeignKeyRegistry.GetHashCode())
                + 19 * (ChangeSubscriberRegistry == null ? 0 : ChangeSubscriberRegistry.GetHashCode());
        }

        // ISelfAsserter methods
        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
               Debug.Assert(base.PassesSelfAssertion());
                if (Server != null) Debug.Assert(Server.PassesSelfAssertion());
                if (ConnectionSupplier != null) Debug.Assert(ConnectionSupplier.PassesSelfAssertion());
                if (SqlTranslator != null) Debug.Assert(SqlTranslator.PassesSelfAssertion());
                if (CascadingForeignKeyRegistry != null) Debug.Assert(CascadingForeignKeyRegistry.PassesSelfAssertion());
                if (ChangeSubscriberRegistry != null) Debug.Assert(ChangeSubscriberRegistry.PassesSelfAssertion());
            }
            return true;
        }

        public bool SkipSelfAssertionUnlessThisType(Type testableType = null)
        {
            if (testableType == null) return false;
            return (!testableType.Equals(this.GetType()));
        }

        // IMethodMatcher methods
        public virtual bool MembersMatch(object obj, bool checkOtherSide = true)
        {
            if (obj == null) return false;
            Type typeOfOther = obj.GetType();
            if (!typeof(IDatabase).IsAssignableFrom(typeOfOther)) return false;
            IDatabase other = (IDatabase)obj;
            if (GetHashCode() != other.GetHashCode()) return false;
            Type typeOfThis = other.GetType();
            if (typeOfThis.Equals(typeOfOther)) return ThisClassMembersMatch(other);
            // On next line, other is a descendant class since it can be assigned
            //   to a variable of this class's type but is not of the same type.
            if (typeOfThis.IsAssignableFrom(typeOfOther)) return other.MembersMatch(this, false);
            // On next line, this is a descendant class of other by similar logic
            if (typeOfOther.IsAssignableFrom(typeOfThis)) return ThisClassMembersMatch(other);
            // Otherwise both are IDatabases but from different class trees
            return
                ThisClassMembersMatch(other)
                && (!checkOtherSide || other.MembersMatch(this, false));
        }

        protected virtual bool ThisClassMembersMatch(object obj)
        {
            if (!typeof(IDatabase).IsAssignableFrom(obj.GetType())) return false;
            IDatabase other = (IDatabase)obj;
            if (!EqualityUtilities.EqualOrBothNull(this.Name, other.Name)) return false;
            if (!EqualityUtilities.EqualOrBothNull(this.Server, other.Server)) return false;
            if (!EqualityUtilities.EqualOrBothNull(this.ConnectionSupplier, other.ConnectionSupplier)) return false;
            if (!EqualityUtilities.EqualOrBothNull(this.SqlTranslator, other.SqlTranslator)) return false;
            if (!EqualityUtilities.EqualOrBothNull(this.CascadingForeignKeyRegistry, other.CascadingForeignKeyRegistry)) return false;
            if (!EqualityUtilities.EqualOrBothNull(this.ChangeSubscriberRegistry, other.ChangeSubscriberRegistry)) return false;
            return true;
        }

        // IComparableByFullName methods and properties

        public string FullName
        {
            get
            {
                if (Server == null || Name == null) return null;
                else if (Server.Name == null) return null;
                else return Server.Name + "." + Name;
            }
        }
        public int CompareByFullNameTo(IComparableByFullName other)
        {
            return ComparableByFullNameMutable.CompareByFullName(this, other);
        }
        public bool EqualsInFullName(IComparableByFullName other)
        {
            return ComparableByFullNameMutable.AreEqualInFullName(this, other);
        }

        // IConnectionSupplier methods

        public IConnection GetAConnection()
        {
            return ConnectionSupplier.GetAConnection();
        }

        // IDatabase properties and methods methods

        public bool IsMutableType { get { return true; } }
        public Type NearestMutableType { get { return GetType(); } }
        public IServer Server { get; set; }
        public IConnectionSupplier ConnectionSupplier { get; set; }
        public ISqlTranslator SqlTranslator { get; set; }
        public bool IsConnected { get { return (ConnectionSupplier != null); } }
        public string Description
        {
            get
            {
                string retval = "database";
                if (Name != null)
                    retval += " '" + Name + "'";
                if (Server != null)
                {
                    if (UtilitiesForINamed.NameOf(Server.Vendor) != null)
                        retval += " with vendor '" + Server.Vendor.Name + "'";
                }
                return retval;
            }
        }
        public ICascadingForeignKeyRegistry CascadingForeignKeyRegistry { get; set; }
        public IChangeSubscriberRegistry ChangeSubscriberRegistry { get; set; }
        public IServer GetServerSafely()
        {
            if (Server == null) return null;
            else return (IServer)Server.GetSafeReference();
        }
        public ISqlTranslator GetSqlTranslatorSafely()
        {
            if (SqlTranslator == null) return null;
            else return (ISqlTranslator)SqlTranslator.GetSafeReference();
        }
        public ICascadingForeignKeyRegistry GetCascadingForeignKeyRegistrySafely()
        {
            if (CascadingForeignKeyRegistry == null) return null;
            else return (ICascadingForeignKeyRegistry)CascadingForeignKeyRegistry.GetSafeReference();
        }
        public IChangeSubscriberRegistry GetChangeSubscriberRegistrySafely()
        {
            if (ChangeSubscriberRegistry == null) return null;
            else return (IChangeSubscriberRegistry)ChangeSubscriberRegistry.GetSafeReference();
        }
        private string LastStatementAfterExecutingAllOthers(
            List<string> script
            , IConnection conn
            , ITransaction trans
            , List<ISqlParameter> parameters = null
            , bool swallowExceptions = false
            )
        {
            string previousStatement = null;
            foreach (string s in script)
            {
                if (previousStatement != null)
                    SqlExecuteNonQuery(previousStatement, conn, trans, parameters, swallowExceptions);
                previousStatement = s;
            }
            return previousStatement;
        }

        public List<string[]> SqlExecuteReader(
            string sql
            , IConnection conn = null
            , ITransaction trans = null
            , List<ISqlParameter> parameters = null
            )
        {
            if ( conn == null ) conn = GetAConnection();
            if (!conn.IsOpen) conn.Open();
            using (ICommand cmd = conn.GetNewCommand(sql, trans, parameters) )
            {
                List<string[]> list = new List<string[]>();
                try
                {
                    int fieldCount = 0;
                    IDataReader r = cmd.ExecuteReader();
                    if (r.Read())
                    {
                        fieldCount = r.FieldCount;
                        do
                        {
                            string[] row = new string[fieldCount];
                            for (int i = 0; i < fieldCount; i++)
                            {
                                if (!DBNull.Value.Equals(r.GetValue(i)))
                                    row[i] = r.GetValue(i).ToString();
                                else
                                    row[i] = "";
                            }
                            list.Add(row);
                        } while (r.Read());
                    }
                    r.Close();
                    return list;
                }
                catch (Exception e)
                {
#if DEBUG
                    Console.WriteLine("DatabaseMutable.SqlExecuteReader:" + e.Message);
                    System.Diagnostics.Debug.WriteLine("DatabaseMutable.SqlExecuteReader:" + e.Message);
#endif
                    throw (e);
                }
            }
        }
        public List<string[]> SqlExecuteReader(
            List<string> script
            , IConnection conn = null
            , ITransaction trans = null
            , List<ISqlParameter> parameters = null
            , bool swallowExceptions = false
            )
        {
            if (conn == null) conn = GetAConnection();
            if (!conn.IsOpen) conn.Open();
            return SqlExecuteReader(
                LastStatementAfterExecutingAllOthers(script, conn, trans, parameters, swallowExceptions)
                );
        }
        public object SqlExecuteSingleValue(
            string sql
            , IConnection conn = null
            , ITransaction trans = null
            , List<ISqlParameter> parameters = null
            )
        {
            object retval = null;
            if (conn == null) conn = GetAConnection();
            if (!conn.IsOpen) conn.Open();
            using (ICommand cmd = conn.GetNewCommand(sql, trans, parameters))
            {
                using (IDataReader r = cmd.ExecuteReader())
                {
                    if (r.Read()) retval = r.GetValue(0);
                }
            }
            return retval;
        }
        /// <summary>
        /// Executes a script, the last statement of which 
        /// returns a single value.
        /// </summary>
        /// <param name="script">The script to be executed.</param>
        /// <param name="conn">Optional connection provided by caller.</param>
        /// <param name="trans">Optional transaction provided by caller.</param>
        /// <returns></returns>
        public object SqlExecuteSingleValue(
            List<string> script
            , IConnection conn = null
            , ITransaction trans = null
            , List<ISqlParameter> parameters = null
            , bool swallowExceptions = false
            )
        {
            if (conn == null) conn = GetAConnection();
            if (!conn.IsOpen) conn.Open();
            return SqlExecuteSingleValue(
                LastStatementAfterExecutingAllOthers(script, conn, trans, parameters, swallowExceptions)
                );
        }
        public bool SqlExecuteNonQuery(
            string sql
            , IConnection conn = null
            , ITransaction trans = null
            , List<ISqlParameter> parameters = null
            , bool swallowExceptions = false
            )
        {
            if (conn == null) conn = GetAConnection();
            if (!conn.IsOpen) conn.Open();
            using (ICommand cmd = conn.GetNewCommand(sql, trans, parameters))
            {
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
#if DEBUG
                    System.Diagnostics.Debug.WriteLine("DatabaseMutable.SqlExecuteNonQuery:" + e.Message);
                    Console.WriteLine("DatabaseMutable.SqlExecuteNonQuery:" + e.Message);
#endif
                    if (!swallowExceptions) throw (e);
                    else return false;
                }
                return true;
            }
        }
        public bool SqlExecuteNonQuery(
            List<string> script
            , IConnection conn = null
            , ITransaction trans = null
            , List<ISqlParameter> parameters = null
            , bool swallowExceptions = false
            )
        {
            if (conn == null) conn = GetAConnection();
            if (!conn.IsOpen) conn.Open();
            return SqlExecuteNonQuery(
                LastStatementAfterExecutingAllOthers(script, conn, trans, parameters, swallowExceptions)
                );
        }

        public List<GenNameValue> SqlExecuteStoredProcedure(string procedure, List<ISqlParameter> parameters = null, IConnection conn = null, ITransaction trans = null)
        {
            if (conn == null) conn = GetAConnection();
            if (!conn.IsOpen) conn.Open();
            if (parameters == null) parameters = new List<ISqlParameter>();
            try
            {
                using (IStoredProcedureCommand proc = conn.GetNewStoredProcedure(procedure, parameters, trans))
                {
                    return proc.Execute();
                }
            }
            catch (Exception e)
            {
#if DEBUG
                Console.WriteLine("DatabaseMutable.GetStoredProcedureCommand:" + e.Message);
#endif
                return null;
            }
        }

        public ITransaction BeginTransaction(IConnection conn = null)
        {
            if (conn == null ) conn = GetAConnection();
            if (!conn.IsOpen) conn.Open();
            return conn.BeginTransaction();
        }
        public void CommitFully(IConnection conn, ITransaction trans = null)
        {
            SqlExecuteNonQuery(SqlTranslator.CommitFullySql(), conn, trans);
        }
        public void Rollback(IConnection conn, ITransaction trans = null)
        {
            SqlExecuteNonQuery(SqlTranslator.RollbackSql(), conn, trans);
        }
        public void ResumeControlOfConnection(IConnection conn, ITransaction trans = null)
        {
            Rollback( conn, trans);
            ConnectionSupplier.ResumeControlOfConnection(conn, trans);
        }

        public int? IDOfObject(IDatabaseObject obj)
        {
            if (obj == null || !IsConnected ) return null;
            else
            {
                string objectTypeName = NameOf(obj.PhysicalObjectType);
                if (objectTypeName.Equals(ObjectTypes.CheckConstraint().Name))
                    return IDOfCheckConstraint(obj.Name, NameOf(obj.Owner), NameOf(obj.Schema));
                else if (objectTypeName.Equals(ObjectTypes.Column().Name))
                    return IDOfColumn(obj.Name, NameOf(obj.Owner), NameOf(obj.Schema));
                else if (objectTypeName.Equals(ObjectTypes.DefaultConstraint().Name))
                    return IDOfDefaultConstraint(obj.Name, NameOf(obj.Owner), NameOf(obj.Schema));
                else if (objectTypeName.Equals(ObjectTypes.ForeignKey().Name))
                    return IDOfForeignKey(obj.Name, NameOf(obj.Owner), NameOf(obj.Schema));
                else if (objectTypeName.Equals(ObjectTypes.Index().Name))
                    return IDOfIndex(obj.Name, NameOf(obj.Owner), NameOf(obj.Schema));
                else if (objectTypeName.Equals(ObjectTypes.PrimaryKey().Name))
                    return IDOfPrimaryKey(obj.Name, NameOf(obj.Owner), NameOf(obj.Schema));
                else if (objectTypeName.Equals(ObjectTypes.Schema().Name))
                    return IDOfSchema(obj.Name);
                else if (objectTypeName.Equals(ObjectTypes.Sequence().Name))
                    return IDOfSequence(obj.Name, NameOf(obj.Schema));
                else if (objectTypeName.Equals(ObjectTypes.Table().Name))
                    return IDOfTable(obj.Name, NameOf(obj.Schema));
                else if (objectTypeName.Equals(ObjectTypes.Trigger().Name))
                    return IDOfTrigger(obj.Name, NameOf(obj.Owner), NameOf(obj.Schema));
                else if (objectTypeName.Equals(ObjectTypes.UniqueKey().Name))
                    return IDOfUniqueKey(obj.Name, NameOf(obj.Owner), NameOf(obj.Schema));
                else if (objectTypeName.Equals(ObjectTypes.User().Name))
                    return IDOfUser(obj.Name);
                else if (objectTypeName.Equals(ObjectTypes.View().Name))
                    return IDOfView(obj.Name, NameOf(obj.Schema));
                else return null;
            }
        }
        public bool ObjectExists(IDatabaseObject obj) { return (IDOfObject(obj) == null); }
        public void EnsureObjectExists(IDatabaseObject obj)
        {
            ValidateNamedObjectAsArgument(
                obj
                , "generic database object"
                , "obj"
                , "EnsureObjectExists(obj)"
                );
            ValidateNamedObjectAsArgument(
                obj.PhysicalObjectType
                , "database object type"
                , "obj.PhysicalObjectType"
                , FullTypeNameForCodeErrors
                + ".EnsureObjectExistsAsDesigned(obj)"
                );
            string objectTypeName = obj.PhysicalObjectType.Name;
            if (objectTypeName.Equals(ObjectTypes.CheckConstraint().Name))
                EnsureCheckConstraintExists((ICheckConstraint)obj);
            else if (objectTypeName.Equals(ObjectTypes.Column().Name))
            {
                if (typeof(ITableColumn).IsAssignableFrom(obj.GetType()))
                    EnsureTableColumnExists((ITableColumn)obj);
                else
                    throw new DirectManipulationOfViewColumnException(
                        (IColumn)obj
                        , obj != null && obj.FullName != null ? obj.FullName : null
                        , obj.GetType().FullName
                        , FullTypeNameForCodeErrors
                        + ".EnsureObjectExists(obj)"
                        );
            }
            else if (objectTypeName.Equals(ObjectTypes.DefaultConstraint().Name))
                EnsureDefaultConstraintExists((IDefaultConstraint)obj);
            else if (objectTypeName.Equals(ObjectTypes.ForeignKey().Name))
                EnsureForeignKeyExists((IForeignKey)obj);
            else if (objectTypeName.Equals(ObjectTypes.Index().Name))
                EnsureIndexExists((IIndex)obj);
            else if (objectTypeName.Equals(ObjectTypes.PrimaryKey().Name))
                EnsurePrimaryKeyExists((IPrimaryKey)obj);
            else if (objectTypeName.Equals(ObjectTypes.Schema().Name))
                EnsureSchemaExists((ISchema)obj);
            else if (objectTypeName.Equals(ObjectTypes.Sequence().Name))
                EnsureSequenceExists((IDatabaseSequence)obj);
            else if (objectTypeName.Equals(ObjectTypes.Table().Name))
                EnsureTableExists((ITable)obj);
            else if (objectTypeName.Equals(ObjectTypes.Trigger().Name))
                EnsureTriggerExists((ITableTrigger)obj);
            else if (objectTypeName.Equals(ObjectTypes.UniqueKey().Name))
                EnsureUniqueKeyExists((IUniqueKey)obj);
            else if (objectTypeName.Equals(ObjectTypes.User().Name))
                EnsureUserExists((IUser)obj);
            else if (objectTypeName.Equals(ObjectTypes.View().Name))
                EnsureViewExists((IView)obj);
            else throw new UnsupportedObjectTypeException(obj);
        }
        public void EnsureObjectExistsAsDesigned(IDatabaseObject obj)
        {
            ValidateNamedObjectAsArgument(
                obj
                , "generic database object"
                , "obj"
                , "EnsureObjectExistsAsDesigned(obj)"
                );
            ValidateNamedObjectAsArgument(
                obj.PhysicalObjectType
                , "database object type"
                , "obj.PhysicalObjectType"
                , FullTypeNameForCodeErrors
                + ".EnsureObjectExistsAsDesigned(obj)"
                );
            string objectTypeName = obj.PhysicalObjectType.Name;
            if (objectTypeName.Equals(ObjectTypes.CheckConstraint().Name))
                EnsureCheckConstraintExistsAsDesigned((ICheckConstraint)obj);
            else if (objectTypeName.Equals(ObjectTypes.Column().Name))
            {
                if (typeof(ITableColumn).IsAssignableFrom(obj.GetType()))
                    EnsureTableColumnExistsAsDesigned((ITableColumn)obj);
                else
                    throw new DirectManipulationOfViewColumnException(
                        (IColumn)obj
                        , obj != null && obj.FullName != null ? obj.FullName : null
                        , obj.GetType().FullName
                        , FullTypeNameForCodeErrors
                        + ".EnsureObjectExistsAsDesigned(obj)"
                        );
            }
            else if (objectTypeName.Equals(ObjectTypes.DefaultConstraint().Name))
                EnsureDefaultConstraintExistsAsDesigned((IDefaultConstraint)obj);
            else if (objectTypeName.Equals(ObjectTypes.ForeignKey().Name))
                EnsureForeignKeyExistsAsDesigned((IForeignKey)obj);
            else if (objectTypeName.Equals(ObjectTypes.Index().Name))
                EnsureIndexExistsAsDesigned((IIndex)obj);
            else if (objectTypeName.Equals(ObjectTypes.PrimaryKey().Name))
                EnsurePrimaryKeyExistsAsDesigned((IPrimaryKey)obj);
            else if (objectTypeName.Equals(ObjectTypes.Schema().Name))
                EnsureSchemaExists((ISchema)obj);
            else if (objectTypeName.Equals(ObjectTypes.Sequence().Name))
                EnsureSequenceExistsAsDesigned((IDatabaseSequence)obj);
            else if (objectTypeName.Equals(ObjectTypes.Table().Name))
                EnsureTableExistsAsDesigned((ITable)obj);
            else if (objectTypeName.Equals(ObjectTypes.Trigger().Name))
                EnsureTriggerExistsAsDesigned((ITableTrigger)obj);
            else if (objectTypeName.Equals(ObjectTypes.UniqueKey().Name))
                EnsureUniqueKeyExistsAsDesigned((IUniqueKey)obj);
            else if (objectTypeName.Equals(ObjectTypes.User().Name))
                EnsureUserExistsAsDesigned((IUser)obj);
            else if (objectTypeName.Equals(ObjectTypes.View().Name))
                EnsureViewExistsAsDesigned((IView)obj);
            else throw new UnsupportedObjectTypeException(obj);
        }
        public void DropObjectIfExists(IDatabaseObject obj)
        {
            if (obj != null)
            {
                ValidateNamedObjectAsArgument(
                    obj
                    , "generic database object"
                    , "obj"
                    , FullTypeNameForCodeErrors
                    + ".DropObjectIfExists(obj)"
                    );
                ValidateNamedObjectAsArgument(
                    obj.PhysicalObjectType
                    , "database object type"
                    , "obj.PhysicalObjectType"
                    , FullTypeNameForCodeErrors
                    + ".DropObjectIfExists(obj)"
                    );
                string objectTypeName = obj.PhysicalObjectType.Name;
                if (objectTypeName.Equals(ObjectTypes.CheckConstraint().Name))
                    DropCheckConstraintIfExists((ICheckConstraint)obj);
                else if (objectTypeName.Equals(ObjectTypes.Column().Name))
                {
                    if (typeof(ITableColumn).IsAssignableFrom(obj.GetType()))
                        DropTableColumnIfExists((ITableColumn)obj);
                    else
                        throw new DirectManipulationOfViewColumnException(
                            (IColumn)obj
                            , obj != null && obj.FullName != null ? obj.FullName : null
                            , obj.GetType().FullName
                            , FullTypeNameForCodeErrors
                            + ".DropObjectIfExists(obj)"
                            );
                }
                else if (objectTypeName.Equals(ObjectTypes.DefaultConstraint().Name))
                    DropDefaultConstraintIfExists((IDefaultConstraint)obj);
                else if (objectTypeName.Equals(ObjectTypes.ForeignKey().Name))
                    DropForeignKeyIfExists((IForeignKey)obj);
                else if (objectTypeName.Equals(ObjectTypes.Index().Name))
                    DropIndexIfExists((IIndex)obj);
                else if (objectTypeName.Equals(ObjectTypes.PrimaryKey().Name))
                    DropPrimaryKeyIfExists((IPrimaryKey)obj);
                else if (objectTypeName.Equals(ObjectTypes.Schema().Name))
                    DropSchemaIfExists((ISchema)obj);
                else if (objectTypeName.Equals(ObjectTypes.Sequence().Name))
                    DropSequenceIfExists((IDatabaseSequence)obj);
                else if (objectTypeName.Equals(ObjectTypes.Table().Name))
                    DropTableIfExists((ITable)obj);
                else if (objectTypeName.Equals(ObjectTypes.Trigger().Name))
                    DropTriggerIfExists((ITableTrigger)obj);
                else if (objectTypeName.Equals(ObjectTypes.UniqueKey().Name))
                    DropUniqueKeyIfExists((IUniqueKey)obj);
                else if (objectTypeName.Equals(ObjectTypes.User().Name))
                    DropUserIfExists((IUser)obj);
                else if (objectTypeName.Equals(ObjectTypes.View().Name))
                    DropViewIfExists((IView)obj);
                else throw new UnsupportedObjectTypeException(obj);
            }
        }
        // IDatabase methods: database manipulation methods
        public int? IDOfDatabase(string name, IServer server)
        {
            if (IsConnected)
                return (int?)SqlExecuteSingleValue(SqlTranslator.IDOfDatabaseSql(name, server));
            else return null;
        }
        public bool DatabaseExists(IDatabase database)
        {
            if (database == null) return false;
            else return (IDOfDatabase(database.Name, database.Server) != null);
        }
        public void EnsureDatabaseExists(IDatabase database)
        {
            if (database == null)
                throw new ArgumentNullException(
                    paramName: "database"
                    , message: "Cannot provide null as the 'database' parameter "
                    + "to Com.VeritasTS.DatabaseUtilityInterfaces.DatabaseMutable.EnsureDatabaseExists(database) "
                    + "method. The user interface should have intercepted this request."
                    );
            else SqlExecuteNonQuery(SqlTranslator.EnsureDatabaseExistsSql(database));
        }
        public void DropDatabaseIfExists(IDatabase database)
        {
            if (database != null)
                SqlExecuteNonQuery(SqlTranslator.DropDatabaseIfExistsSql(database));
        }

        // IDatabase methods: check constraint manipulation methods
        public int? IDOfCheckConstraint(string name, string tableName, string schemaName = null)
        {
            if (IsConnected)
                return (int?)SqlExecuteSingleValue(
                    SqlTranslator.IDOfCheckConstraintSql(
                        name
                        , tableName
                        , schemaName
                        , this
                        )
                    );
            else
                return null;
        }
        public bool CheckConstraintExists(ICheckConstraint constraint)
        {
            if (constraint == null) return false;
            else
            {
                ValidateCheckConstraintAsArgument(constraint, "CheckConstraintExists(constraint)");
                return IDOfCheckConstraint(constraint.Name, constraint.Owner.Name, NameOf(constraint.Schema)) != null;
            }
        }
        public void EnsureCheckConstraintExists(ICheckConstraint constraint)
        {
            ValidateCheckConstraintAsArgument(constraint, "CheckConstraintExists(constraint)");
            SqlExecuteNonQuery(SqlTranslator.EnsureCheckConstraintExistsSql(constraint));
        }
        public void EnsureCheckConstraintExistsAsDesigned(ICheckConstraint constraint)
        {
            ValidateCheckConstraintAsArgument(constraint, "CheckConstraintExists(constraint)");
            SqlExecuteNonQuery(SqlTranslator.EnsureCheckConstraintExistsAsDesignedSql(constraint));
        }
        public void DropCheckConstraintIfExists(ICheckConstraint constraint)
        {
            if (constraint != null)
            {
                ValidateCheckConstraintAsArgument(constraint, "CheckConstraintExists(constraint)");
                SqlExecuteNonQuery(SqlTranslator.DropCheckConstraintIfExistsSql(constraint));
            }
        }


        // IDatabase methods: column manipulation methods
        public int? IDOfColumn(
            string name
            , string vableName
            , string schemaName = null
            )
        {
            if (IsConnected)
                return (int?)SqlExecuteSingleValue(
                    SqlTranslator.IDOfTableColumnSql(
                        name
                        , vableName
                        , schemaName
                        , this
                        )
                    );
            else
                return null;
        }
        public bool ColumnExists(IColumn column)
        {
            if (column == null) return false;
            else
            {
                ValidateTableColumnAsArgument(column, "ColumnExists(column)");
                return (IDOfColumn(column.Name, column.Owner.Name, NameOf(column.Schema) ) != null);
            }
        }
        public void EnsureTableColumnExists(ITableColumn column)
        {
            ValidateTableColumnAsArgument(column, "EnsureTableColumnExists(column)");
            SqlExecuteNonQuery(SqlTranslator.EnsureTableColumnExistsSql(column));
        }
        public void EnsureTableColumnExistsAsDesigned(ITableColumn column)
        {
            ValidateTableColumnAsArgument(column, "EnsureTableColumnExistsAsDesigned(column)");
            SqlExecuteNonQuery(SqlTranslator.EnsureTableColumnExistsAsDesignedSql(column));
        }
        public void DropTableColumnIfExists(ITableColumn column)
        {
            if (column != null)
            {
                ValidateTableColumnAsArgument(column, "DropTableColumnIfExists(column)");
                SqlExecuteNonQuery(SqlTranslator.DropTableColumnIfExistsSql(column));
            }
        }

        // IDatabase methods: Default constraint manipulation methods
        public int? IDOfDefaultConstraint(
            string name
            , string tableName
            , string schemaName = null
            )
        {
            if (IsConnected)
                return (int?)SqlExecuteSingleValue(
                    SqlTranslator.IDOfDefaultConstraintSql(
                        name
                        , tableName
                        , schemaName
                        , this
                        )
                    );
            else
                return null;
        }
        public bool DefaultConstraintExists(IDefaultConstraint constraint)
        {
            if (constraint == null) return false;
            {
                ValidateDefaultConstraintAsArgument(constraint, "DefaultConstraintExists(constraint)");
                return (IDOfDefaultConstraint(constraint.Name, constraint.Owner.Name, NameOf(constraint.Schema)) != null);
            }
        }
        public void EnsureDefaultConstraintExists(IDefaultConstraint constraint)
        {
            ValidateDefaultConstraintAsArgument(constraint, "EnsureDefaultConstraintExists(constraint)");
            SqlExecuteNonQuery(SqlTranslator.EnsureDefaultConstraintExistsSql(constraint));
        }
        public void EnsureDefaultConstraintExistsAsDesigned(IDefaultConstraint constraint)
        {
            ValidateDefaultConstraintAsArgument(constraint, "EnsureDefaultConstraintExistsAsDesigned(constraint)");
            SqlExecuteNonQuery(SqlTranslator.EnsureDefaultConstraintExistsAsDesignedSql(constraint));
        }
        public void DropDefaultConstraintIfExists(IDefaultConstraint constraint)
        {
            if (constraint != null)
            {
                ValidateDefaultConstraintAsArgument(constraint, "DropDefaultConstraintIfExists(constraint)");
                SqlExecuteNonQuery(SqlTranslator.DropDefaultConstraintIfExistsSql(constraint));
            }
        }

        // IDatabase methods: Foreign key manipulation methods
        public int? IDOfForeignKey(
            string name
            , string tableName
            , string schemaName = null
            )
        {
            if (IsConnected)
                return (int?)SqlExecuteSingleValue(
                    SqlTranslator.IDOfForeignKeySql(
                        name
                        , tableName
                        , schemaName
                        , this
                        )
                    );
            else
                return null;
        }
        public bool ForeignKeyExists(IForeignKey key)
        {
            if (key == null) return false;
            {
                ValidateForeignKeyAsArgument(key, "ForeignKeyExists(key)");
                return (IDOfForeignKey(key.Name, key.Owner.Name, NameOf(key.Schema)) != null);
            }
        }
        public void EnsureForeignKeyExists(IForeignKey key, bool cascadeManually = false )
        {
            ValidateForeignKeyAsArgument(key, "EnsureForeignKeyExists(key)");
            if (cascadeManually)
                SqlExecuteNonQuery(SqlTranslator.EnsureForeignKeyExistsSql(key, true));
            else
            {
                try
                {
                    SqlExecuteNonQuery(SqlTranslator.EnsureForeignKeyExistsSql(key, false));
                } catch( Exception e )
                {
                    SqlExecuteNonQuery(SqlTranslator.EnsureForeignKeyExistsSql(key, true));
                    if (false) throw e;  // Make compiler error go away
                }
            }
        }
        public void EnsureForeignKeyExistsAsDesigned(IForeignKey key, bool cascadeManually = false)
        {
            ValidateForeignKeyAsArgument(key, "EnsureForeignKeyExistsAsDesigned(key)");
            if (cascadeManually)
                SqlExecuteNonQuery(SqlTranslator.EnsureForeignKeyExistsAsDesignedSql(key, true));
            else
            {
                try
                {
                    SqlExecuteNonQuery(SqlTranslator.EnsureForeignKeyExistsAsDesignedSql(key, false));
                }
                catch (Exception e)
                {
                    SqlExecuteNonQuery(SqlTranslator.EnsureForeignKeyExistsAsDesignedSql(key, true));
                    if (false) throw e;  // Make compiler error go away
                }
            }
        }
        public void DropForeignKeyIfExists(IForeignKey key)
        {
            if (key != null)
            {
                ValidateForeignKeyAsArgument(key, "DropForeignKeyIfExists(key)");
                SqlExecuteNonQuery(SqlTranslator.DropForeignKeyIfExistsSql(key));
            }
        }

        // IDatabase methods: index manipulation methods
        public int? IDOfIndex(
            string name
            , string tableName
            , string schemaName = null
            )
        {
            if (IsConnected)
                return (int?)SqlExecuteSingleValue(
                    SqlTranslator.IDOfIndexSql(
                        name
                        , tableName
                        , schemaName
                        , this
                        )
                    );
            else
                return null;
        }
        public bool IndexExists(IIndex index)
        {
            if (index == null) return false;
            {
                ValidateIndexAsArgument(index, "IndexExists(index)");
                return (IDOfIndex(index.Name, index.Owner.Name, NameOf(index.Schema)) != null);
            }
        }
        public void EnsureIndexExists(IIndex index)
        {
            ValidateIndexAsArgument(index, "EnsureIndexExists(index)");
            SqlExecuteNonQuery(SqlTranslator.EnsureIndexExistsSql(index));
        }
        public void EnsureIndexExistsAsDesigned(IIndex index)
        {
            ValidateIndexAsArgument(index, "EnsureIndexExistsAsDesigned(index)");
            SqlExecuteNonQuery(SqlTranslator.EnsureIndexExistsAsDesignedSql(index));
        }
        public void DropIndexIfExists(IIndex index)
        {
            if (index != null)
            {
                ValidateIndexAsArgument(index, "DropIndexIfExists(index)");
                SqlExecuteNonQuery(SqlTranslator.DropIndexIfExistsSql(index));
            }
        }

        // IDatabase methods: primary key manipulation methods
        public int? IDOfPrimaryKey(
            string name
            , string tableName
            , string schemaName = null
            )
        {
            if (IsConnected)
                return (int?)SqlExecuteSingleValue(
                    SqlTranslator.IDOfPrimaryKeySql(
                        name
                        , tableName
                        , schemaName
                        , this
                        )
                    );
            else
                return null;
        }
        public bool PrimaryKeyExists(IPrimaryKey key)
        {
            if (key == null) return false;
            {
                ValidatePrimaryKeyAsArgument(key, "PrimaryKeyExists(key)");
                return (IDOfPrimaryKey(key.Name, key.Owner.Name, NameOf(key.Schema)) != null);
            }
        }
        public void EnsurePrimaryKeyExists(IPrimaryKey key)
        {
            ValidatePrimaryKeyAsArgument(key, "EnsurePrimaryKeyExists(key)");
            SqlExecuteNonQuery(SqlTranslator.EnsurePrimaryKeyExistsSql(key));
        }
        public void EnsurePrimaryKeyExistsAsDesigned(IPrimaryKey key)
        {
            ValidatePrimaryKeyAsArgument(key, "EnsurePrimaryKeyExistsAsDesigned(key)");
            SqlExecuteNonQuery(SqlTranslator.EnsurePrimaryKeyExistsAsDesignedSql(key));
        }
        public void DropPrimaryKeyIfExists(IPrimaryKey key)
        {
            if (key != null)
            {
                ValidatePrimaryKeyAsArgument(key, "DropPrimaryKeyIfExists(key)");
                SqlExecuteNonQuery(SqlTranslator.DropPrimaryKeyIfExistsSql(key));
            }
        }

        // IDatabase methods: scalar function manipulation methods
        public int? IDOfScalarFunction(
            string name
            , string schemaName = null
            )
        {
            if (IsConnected)
                return (int?)SqlExecuteSingleValue(
                    SqlTranslator.IDOfScalarFunctionSql(
                        name
                        , schemaName
                        , this
                        )
                    );
            else
                return null;
        }
        public bool ScalarFunctionExists(IScalarFunction function)
        {
            if (function == null) return false;
            {
                ValidateScalarFunctionAsArgument(function, "ScalarFunctionExists(function)");
                return (IDOfScalarFunction(function.Name, NameOf(function.Schema)) != null);
            }
        }
        public void EnsureScalarFunctionExists(IScalarFunction function)
        {
            ValidateScalarFunctionAsArgument(function, "EnsureScalarFunctionExists(function)");
            SqlExecuteNonQuery(SqlTranslator.EnsureScalarFunctionExistsSql(function));
        }
        public void EnsureScalarFunctionExistsAsDesigned(IScalarFunction function)
        {
            ValidateScalarFunctionAsArgument(function, "EnsureScalarFunctionExistsAsDesigned(function)");
            SqlExecuteNonQuery(SqlTranslator.EnsureScalarFunctionExistsAsDesignedSql(function));
        }
        public void DropScalarFunctionIfExists(IScalarFunction function)
        {
            if (function != null)
            {
                ValidateScalarFunctionAsArgument(function, "DropScalarFunctionIfExists(function)");
                SqlExecuteNonQuery(SqlTranslator.DropScalarFunctionIfExistsSql(function));
            }
        }

        // IDatabase methods: schema manipulation methods
        public int? IDOfSchema(
            string name
            )
        {
            if (IsConnected)
                return (int?)SqlExecuteSingleValue(
                    SqlTranslator.IDOfSchemaSql(
                        name
                        , this
                        )
                    );
            else
                return null;
        }
        public bool SchemaExists(ISchema schema)
        {
            if (schema == null) return false;
            {
                ValidateSchemaAsArgument(schema, "SchemaExists(schema)");
                return (IDOfSchema(schema.Name) == null);
            }
        }
        public void EnsureSchemaExists(ISchema schema)
        {
            ValidateSchemaAsArgument(schema, "EnsureSchemaExists(schema)");
            SqlExecuteNonQuery(SqlTranslator.EnsureSchemaExistsSql(schema));
        }
        public void DropSchemaIfExists(ISchema schema)
        {
            if (schema != null)
            {
                ValidateSchemaAsArgument(schema, "DropSchemaIfExists(schema)");
                SqlExecuteNonQuery(SqlTranslator.DropSchemaIfExistsSql(schema));
            }
        }

        // IDatabase methods: Sequence manipulation methods
        public int? IDOfSequence(
            string name
            , string schemaName = null
            )
        {
            if (IsConnected)
                return (int?)SqlExecuteSingleValue(
                    SqlTranslator.IDOfSequenceSql(
                        name
                        , schemaName
                        , this
                        )
                    );
            else
                return null;
        }
        public bool SequenceExists(IDatabaseSequence sequence)
        {
            if (sequence == null) return false;
            {
                ValidateSequenceAsArgument(sequence, "SequenceExists(sequence)");
                return (IDOfSequence(sequence.Name, NameOf(sequence.Schema)) != null);
            }
        }
        public void EnsureSequenceExists(IDatabaseSequence sequence)
        {
            ValidateSequenceAsArgument(sequence, "EnsureSequenceExists(sequence)");
            SqlExecuteNonQuery(SqlTranslator.EnsureSequenceExistsSql(sequence));
        }
        public void EnsureSequenceExistsAsDesigned(IDatabaseSequence sequence)
        {
            ValidateSequenceAsArgument(sequence, "EnsureSequenceExistsAsDesigned(sequence)");
            SqlExecuteNonQuery(SqlTranslator.EnsureSequenceExistsAsDesignedSql(sequence));
        }
        public void DropSequenceIfExists(IDatabaseSequence sequence)
        {
            if (sequence != null)
            {
                ValidateSequenceAsArgument(sequence, "DropSequenceIfExists(sequence)");
                SqlExecuteNonQuery(SqlTranslator.DropSequenceIfExistsSql(sequence));
            }
        }

        // IDatabase methods: stored procedure manipulation methods
        public int? IDOfStoredProcedure(
            string name
            , string schemaName = null
            )
        {
            if (IsConnected)
                return (int?)SqlExecuteSingleValue(
                    SqlTranslator.IDOfStoredProcedureSql(
                        name
                        , schemaName
                        , this
                        )
                    );
            else
                return null;
        }
        public bool StoredProcedureExists(IStoredProcedure proc)
        {
            if (proc == null) return false;
            {
                ValidateStoredProcedureAsArgument(proc, "StoredProcedureExists(proc)");
                return (IDOfStoredProcedure(proc.Name, NameOf(proc.Schema)) != null);
            }
        }
        public void EnsureStoredProcedureExists(IStoredProcedure proc)
        {
            ValidateStoredProcedureAsArgument(proc, "EnsureStoredProcedureExists(proc)");
            SqlExecuteNonQuery(SqlTranslator.EnsureStoredProcedureExistsSql(proc));
        }
        public void EnsureStoredProcedureExistsAsDesigned(IStoredProcedure proc)
        {
            ValidateStoredProcedureAsArgument(proc, "EnsureStoredProcedureExistsAsDesigned(proc)");
            SqlExecuteNonQuery(SqlTranslator.EnsureStoredProcedureExistsAsDesignedSql(proc));
        }
        public void DropStoredProcedureIfExists(IStoredProcedure proc)
        {
            if (proc != null)
            {
                ValidateStoredProcedureAsArgument(proc, "DropStoredProcedureIfExists(proc)");
                SqlExecuteNonQuery(SqlTranslator.DropStoredProcedureIfExistsSql(proc));
            }
        }

        // IDatabase methods: table manipulation methods
        public int? IDOfTable(
            string name
            , string schemaName = null
            )
        {
            if (IsConnected)
                return (int?)SqlExecuteSingleValue(
                    SqlTranslator.IDOfTableSql(
                        name
                        , schemaName
                        , this
                        )
                    );
            else
                return null;
        }
        public bool TableExists(ITable table)
        {
            if (table == null) return false;
            {
                ValidateTableAsArgument(table, "TableExists(table)");
                return (IDOfTable(table.Name, NameOf(table.Schema)) != null);
            }
        }
        public void EnsureTableExists(ITable table)
        {
            ValidateTableAsArgument(table, "EnsureTableExists(table)");
            SqlExecuteNonQuery(SqlTranslator.EnsureTableExistsSql(table));
        }
        public void EnsureTableExistsAsDesigned(ITable table)
        {
            ValidateTableAsArgument(table, "EnsureTableExistsAsDesigned(table)");
            // We have to deal with the possibility that some of
            //   the foreign key constraints will fail when the
            //   attempt is made to load them using native RDBMS
            //   support for cascading deletes and updates (SQL Server,
            //   we're lookin' at you). The translator will give us
            //   a single script that attempts to create everything;
            //   but if this script fails then we will step through
            //   the creation of each of the pieces ourselves.
            SqlExecuteNonQuery(SqlTranslator.EnsureTableExistsAsDesignedSql(table, includeForeignKeys: false));
            try
            {
                SqlExecuteNonQuery(SqlTranslator.EnsureForeignKeysExistAsDesignedSql(table));
            } catch (Exception discard)
            {
                foreach (IForeignKey key in table.ForeignKeys.Values)
                {
                    // Here is our 
                    try
                    {
                        SqlExecuteNonQuery(SqlTranslator.EnsureForeignKeyExistsAsDesignedSql(key, false));
                    } catch (Exception discardAgain)
                    {
                        discardAgain = null;
                        SqlExecuteNonQuery(SqlTranslator.EnsureForeignKeyExistsAsDesignedSql(key, true));
                        if (false) throw discardAgain;  // Make compiler error go away
                    }
                    SqlExecuteNonQuery(FullyCommit());
                }
                if (false) throw discard;  // Make compiler error go away
            }
        }
        public void DropTableIfExists(ITable table)
        {
            if (table != null)
            {
                ValidateTableAsArgument(table, "DropTableIfExists(table)");
                SqlExecuteNonQuery(SqlTranslator.DropTableIfExistsSql(table));
            }
        }
        private List<IColumn> TableColumnsInDatabase(ITable table, List<string> script)
        {
            List<IColumn> retval = new List<IColumn>();
            if (table != null)
            {
                Dictionary<string, int> dict = SqlTranslator.TableColumnsOutputStructure();
                Dictionary<int, ITableColumnMutable> colList = new Dictionary<int, ITableColumnMutable>();
                foreach (string[] record in SqlExecuteReader(script))
                {
                    ITableColumnMutable column;
                    int columnID = Int32.Parse(record[dict["column_id"]]);
                    if (colList.ContainsKey(columnID))
                    {
                        // Here we are simply adding a new check constraint
                        column = colList[columnID];
                        column.AddOrOverwriteCheckConstraint(
                            new SingleColumnCheckConstraintImmutable(
                                name: record[dict["check_name"]]
                                , sqlTranslator: SqlTranslator
                                , table: table
                                , constraintText: record[dict["check_definition"]]
                                , constrainedColumn: column
                                , id: Int32.Parse(record[dict["check_id"]])
                                , assert: true
                                )
                            );
                    }
                    else
                    {
                        string dataType = record[dict["data_type"]];
                        if (record[dict["character_max_length"]].Equals("-1"))
                            dataType += "(max)";
                        else if (dataType.Contains("char") || dataType.Contains("float"))
                            dataType += "(" + record[dict["character_max_length"]] + ")";
                        bool isToBeTrimmed = (dataType.Contains("char"));
                        colList[columnID]
                            = new TableColumnMutable(
                                record[dict["column_name"]]
                                , dataType
                                , table: table
                                , isIdentity: BoolFromString(record[dict["is_identity"]])
                                , isNullable: BoolFromString(record[dict["is_nullable"]])
                                , calcFormula: record[dict["definition"]]
                                , isToBeTrimmed: isToBeTrimmed
                                , isImmutable: false
                                , isPersisted: BoolFromString(record[dict["is_persisted"]])
                                , defaultConstraint: null
                                , checkConstraints: null
                                );
                        int? defaultID = null;
                        if (record[dict["default_id"]] != null)
                        {
                            if (record[dict["default_id"]].Length > 0)
                                defaultID = Int32.Parse(record[dict["default_id"]]);
                        }
                        IDefaultConstraint defaultConstraint = null;
                        if (defaultID != null)
                            defaultConstraint
                                = new DefaultConstraintImmutable(
                                    record[dict["default_name"]]
                                    , SqlTranslator
                                    , table
                                    , colList[columnID]
                                    , record[dict["default_definition"]]
                                    , id: defaultID
                                    , assert: true
                                    );
                        colList[columnID].SetDefaultConstraint(defaultConstraint);
                        int? checkID = null;
                        if (record[dict["check_id"]] != null)
                        {
                            if (record[dict["check_id"]].Length > 0)
                                checkID = Int32.Parse(record[dict["check_id"]]);
                        }
                        ISingleColumnCheckConstraint checkConstraint = null;
                        if (checkID != null)
                            checkConstraint
                                = new SingleColumnCheckConstraintImmutable(
                                    record[dict["check_name"]]
                                    , SqlTranslator
                                    , table
                                    , colList[columnID]
                                    , record[dict["check_definition"]]
                                    , id: checkID
                                    );
                        colList[columnID].AddOrOverwriteCheckConstraint(checkConstraint);
                    }

                }
                foreach (IColumn c in colList.Values) retval.Add(c);
            }
            return retval;
        }
        public List<IColumn> TableColumnsInDatabase(ITable table)
        {
            ValidateTableAsArgument(table, "TableColumnsInDatabase(table)");
            return TableColumnsInDatabase(table, SqlTranslator.TableColumnsSql(table));
        }
        public List<IColumn> TableIdentityColumnInDatabase(ITable table)
        {
            ValidateTableAsArgument(table, "TableIdentityColumnInDatabase(table)");
            return TableColumnsInDatabase(table, SqlTranslator.TableIdentityColumnSql(table));
        }
        public List<IColumn> TableComputedColumnsInDatabase(ITable table)
        {
            ValidateTableAsArgument(table, "TableComputedColumnsInDatabase(table)");
            return TableColumnsInDatabase(table, SqlTranslator.TableComputedColumnsSql(table));
        }

        public List<string[]> TableForeignKeyDependentsInDatabase(ITable table)
        {
            ValidateTableAsArgument(table, "TableForeignKeyDependentsInDatabase(table)");
            if (table != null)
                return SqlExecuteReader(SqlTranslator.VableForeignKeyDependentsSql(table));
            else return new List<string[]>();
        }

        // IDatabase methods: trigger manipulation methods
        public int? IDOfTrigger(
            string name
            , string tableName
            , string schemaName = null
            )
        {
            if (IsConnected)
                return (int?)SqlExecuteSingleValue(
                    SqlTranslator.IDOfTriggerSql(
                        name
                        , tableName
                        , schemaName
                        , this
                        )
                    );
            else
                return null;
        }
        public bool TriggerExists(ITableTrigger trigger)
        {
            if (trigger == null) return false;
            {
                ValidateTriggerAsArgument(trigger, "TriggerExists(trigger)");
                return (IDOfTrigger(trigger.Name, trigger.Owner.Name, NameOf(trigger.Schema)) != null);
            }
        }
        public void EnsureTriggerExists(ITableTrigger trigger)
        {
            ValidateTriggerAsArgument(trigger, "EnsureTriggerExists(trigger)");
            SqlExecuteNonQuery(SqlTranslator.EnsureTriggerExistsSql(trigger));
        }
        public void EnsureTriggerExistsAsDesigned(ITableTrigger trigger)
        {
            ValidateTriggerAsArgument(trigger, "EnsureTriggerExistsAsDesigned(trigger)");
            SqlExecuteNonQuery(SqlTranslator.EnsureTriggerExistsAsDesignedSql(trigger));
        }
        public void DropTriggerIfExists(ITableTrigger trigger)
        {
            if (trigger != null)
            {
                ValidateTriggerAsArgument(trigger, "DropTriggerIfExists(trigger)");
                SqlExecuteNonQuery(SqlTranslator.DropTriggerIfExistsSql(trigger));
            }
        }

        // IDatabase methods: unique key manipulation methods
        public int? IDOfUniqueKey(
            string name
            , string tableName
            , string schemaName = null
            )
        {
            if (IsConnected)
                return (int?)SqlExecuteSingleValue(
                    SqlTranslator.IDOfUniqueKeySql(
                        name
                        , tableName
                        , schemaName
                        , this
                        )
                    );
            else
                return null;
        }
        public bool UniqueKeyExists(IUniqueKey key)
        {
            if (key == null) return false;
            {
                ValidateUniqueKeyAsArgument(key, "UniqueKeyExists(key)");
                return (IDOfUniqueKey(key.Name, key.Owner.Name, NameOf(key.Schema)) != null);
            }
        }
        public void EnsureUniqueKeyExists(IUniqueKey key)
        {
            ValidateUniqueKeyAsArgument(key, "EnsureUniqueKeyExists(key)");
            SqlExecuteNonQuery(SqlTranslator.EnsureUniqueKeyExistsSql(key));
        }
        public void EnsureUniqueKeyExistsAsDesigned(IUniqueKey key)
        {
            ValidateUniqueKeyAsArgument(key, "EnsureUniqueKeyExistsAsDesigned(key)");
            SqlExecuteNonQuery(SqlTranslator.EnsureUniqueKeyExistsAsDesignedSql(key));
        }
        public void DropUniqueKeyIfExists(IUniqueKey key)
        {
            if (key != null)
            {
                ValidateUniqueKeyAsArgument(key, "DropUniqueKeyIfExists(key)");
                SqlExecuteNonQuery(SqlTranslator.DropUniqueKeyIfExistsSql(key));
            }
        }

        // IDatabase methods: user manipulation methods
        public int? IDOfUser(string name)
        {
            throw new NotImplementedException(
                "Methods dealing with IUser interface have not been implemented"
                );
        }
        public bool UserExists(IUser user)
        {
            throw new NotImplementedException(
                "Methods dealing with IUser interface have not been implemented"
                );
        }
        public void EnsureUserExists(IUser user)
        {
            throw new NotImplementedException(
                "Methods dealing with IUser interface have not been implemented"
                );
        }
        public void EnsureUserExistsAsDesigned(IUser user)
        {
            throw new NotImplementedException(
                "Methods dealing with IUser interface have not been implemented"
                );
        }
        public void DropUserIfExists(IUser user)
        {
            throw new NotImplementedException(
                "Methods dealing with IUser interface have not been implemented"
                );
        }

        // IDatabase methods: view manipulation methods
        public int? IDOfView(
            string name
            , string schemaName = null
            )
        {
            if (IsConnected)
                return (int?)SqlExecuteSingleValue(
                    SqlTranslator.IDOfViewSql(
                        name
                        , schemaName
                        , this
                        )
                    );
            else
                return null;
        }
        public bool ViewExists(IView view)
        {
            if (view == null) return false;
            {
                ValidateViewAsArgument(view, "ViewExists(view)");
                return (IDOfView(view.Name, NameOf(view.Schema)) != null);
            }
        }
        public void EnsureViewExists(IView view)
        {
            ValidateViewAsArgument(view, "EnsureViewExists(view)");
            SqlExecuteNonQuery(SqlTranslator.EnsureViewExistsSql(view));
        }
        public void EnsureViewExistsAsDesigned(IView view)
        {
            ValidateViewAsArgument(view, "EnsureViewExistsAsDesigned(view)");
            SqlExecuteNonQuery(SqlTranslator.EnsureViewExistsAsDesignedSql(view));
        }
        public void DropViewIfExists(IView view)
        {
            if (view != null)
            {
                ValidateViewAsArgument(view, "DropViewIfExists(view)");
                SqlExecuteNonQuery(SqlTranslator.DropViewIfExistsSql(view));
            }
        }

        // IDatabase methods: SQL construction methods
        public bool IsValidObjectName(string name)
        {
            return SqlTranslator.IsValidObjectName(name);
        }
        public string ValidObjectNameFrom(string name)
        {
            return SqlTranslator.ValidObjectNameFrom(name);
        }
        public string TableConstraintName(string root, string tableName)
        {
            return SqlTranslator.TableConstraintName(root, tableName);
        }

        // IDatabaseMutable methods and properties
        public void SetServer(IServer server)
        {
            Server = server;
            Debug.Assert(PassesSelfAssertion());
        }
        public virtual void SetConnectionSupplier(IConnectionSupplier connectionSupplier)
        {
            ConnectionSupplier = connectionSupplier;
            Debug.Assert(PassesSelfAssertion());
        }
        public void SetSqlTranslator(ISqlTranslator sqlTranslator)
        {
            SqlTranslator = sqlTranslator;
            Debug.Assert(PassesSelfAssertion());
        }
        public void SetCascadingForeignKeyRegistry(ICascadingForeignKeyRegistry cascadingForeignKeyRegistry)
        {
            CascadingForeignKeyRegistry = cascadingForeignKeyRegistry;
            Debug.Assert(PassesSelfAssertion());
        }
        public void SetChangeSubscriberRegistry(IChangeSubscriberRegistry changeSubscriberRegistry)
        {
            ChangeSubscriberRegistry = changeSubscriberRegistry;
            Debug.Assert(PassesSelfAssertion());
        }

        /*** 
         * Private utility functions
         ***/

        private bool BoolFromString(string s)
        {
            if (s != null) return !s.Equals(SqlTranslator.FalseSql());
            else return false;
        }
        private string FullNameOf(IFullNamed obj, string defaultValue = null)
        {
            return UtilitiesForIFullNamed.FullNameOf(obj, defaultValue);
        }
        private string FullyCommit() { return "while @@TRANCOUNT > 0 commit transaction;"; }
        private string If(bool condition, string valueIfTrue, string valueIfFalse)
        {
            if (condition) return valueIfTrue;
            else return valueIfFalse;
        }
        private string NameOf(INamed obj, string defaultValue = null)
        {
            return UtilitiesForINamed.NameOf(obj, defaultValue);
        }

        private void ValidateNamedObjectAsArgument(INamed element, string objectTypeName, string paramName, string callingMethod)
        {
            UtilitiesForINamed.NonnullNamedParameterIsValid(
                element
                , objectTypeName
                , paramName
                , this.GetType().FullName + "." + callingMethod
                );
        }
        private void ValidateTableElementAsArgument(IDatabaseObject element, string objectTypeName, string paramName, string callingMethod)
        {
            ValidateNamedObjectAsArgument(element, objectTypeName, paramName, callingMethod);
            if (element.Owner == null)
                throw new ArgumentNullException(
                    paramName: "index.Owner"
                    , message: ""
                    + "Cannot provide provide " + objectTypeName + " with null Owner as '" + paramName + "' argument to "
                    + this.GetType().FullName + "." + callingMethod + "."
                    );
            if (element.Owner.Name == null)
                throw new ArgumentNullException(
                    paramName: "index.Owner"
                    , message: ""
                    + "Cannot provide provide " + objectTypeName + " whose Owner has null Name as '" + paramName + "' argument to "
                    + this.GetType().FullName + "." + callingMethod + "."
                    );
        }
        private void ValidateCheckConstraintAsArgument(ICheckConstraint constraint, string callingMethod)
        {
            ValidateTableElementAsArgument(
                constraint
                , "check constraint"
                , "constraint"
                , callingMethod
                );
        }
        private void ValidateTableColumnAsArgument(IColumn column, string callingMethod)
        {
            ValidateTableElementAsArgument(
                column
                , "column"
                , "column"
                , callingMethod
                );
            if (((IDatabaseObject)column.Owner).ObjectType == null)
                throw new ArgumentNullException(
                    paramName: "column.ObjectType"
                    , message: ""
                    + "Cannot provide column whose Owner has null ObjectType as 'column' argument to "
                    + this.GetType().FullName + "." + callingMethod + "."
                    );
            if (((IDatabaseObject)column.Owner).ObjectType.Equals(ObjectTypes.View()))
                throw new BadObjectTypeParameterException(
                    paramName: "column.ObjectType"
                    , badObjectType: ObjectTypes.View()
                    , message: ""
                    + "Cannot provide column whose is a view "
                    + " as 'column' argument to "
                    + this.GetType().FullName + "." + callingMethod + "."
                    + "Instead, call EnsureViewExistsAsDesignedSql on the parent view."
                    );
            if (!((IDatabaseObject)column.Owner).ObjectType.Equals(ObjectTypes.Table()))
                throw new BadObjectTypeParameterException(
                    paramName: "column.ObjectType"
                    , badObjectType: ((IDatabaseObject)column.Owner).ObjectType
                    , message: ""
                    + "Cannot provide column whose Owner has ObjectType of "
                    + ((IDatabaseObject)column.Owner).ObjectType.Name.ToString()
                    + " as 'column' argument to "
                    + this.GetType().FullName + "." + callingMethod + "."
                    );
        }
        private void ValidateDefaultConstraintAsArgument(IDefaultConstraint constraint, string callingMethod)
        {
            ValidateTableElementAsArgument(
                constraint
                , "default constraint"
                , "constraint"
                , callingMethod
                );
        }
        private void ValidateForeignKeyAsArgument(IForeignKey key, string callingMethod)
        {
            ValidateTableElementAsArgument(
                key
                , "foreign key"
                , "key"
                , callingMethod 
                );
        }
        private void ValidateIndexAsArgument(IIndex index, string callingMethod)
        {
            ValidateTableElementAsArgument(
                index
                , "index"
                , "index"
                , callingMethod 
                );
        }
        private void ValidatePrimaryKeyAsArgument(IPrimaryKey key, string callingMethod)
        {
            ValidateTableElementAsArgument(
                key
                , "primary key"
                , "key"
                , callingMethod
                );
        }
        private void ValidateScalarFunctionAsArgument(IScalarFunction function, string callingMethod)
        {
            ValidateNamedObjectAsArgument(function, "scalar function", "function", callingMethod);
        }
        private void ValidateSchemaAsArgument(ISchema schema, string callingMethod)
        {
            ValidateNamedObjectAsArgument(schema, "schema", "schema", callingMethod);
        }
        private void ValidateSequenceAsArgument(IDatabaseSequence sequence, string callingMethod)
        {
            ValidateNamedObjectAsArgument(sequence, "sequence", "sequence", callingMethod);
        }
        private void ValidateStoredProcedureAsArgument(IStoredProcedure proc, string callingMethod)
        {
            ValidateNamedObjectAsArgument(proc, "stored procedure", "proc", callingMethod);
        }
        private void ValidateTableAsArgument(ITable table, string callingMethod)
        {
            ValidateNamedObjectAsArgument(table, "table", "table", callingMethod);
        }
        private void ValidateTriggerAsArgument(ITableTrigger trigger, string callingMethod)
        {
            ValidateTableElementAsArgument(
                trigger
                , "trigger"
                , "trigger"
                , callingMethod
                );
        }
        private void ValidateUniqueKeyAsArgument(IUniqueKey key, string callingMethod)
        {
            ValidateTableElementAsArgument(
                key
                , "unique key"
                , "key"
                , callingMethod
                );
        }
        private void ValidateViewAsArgument(IView view, string callingMethod)
        {
            ValidateNamedObjectAsArgument(view, "view", "view", callingMethod);
        }

    }

    public class DatabaseSafeMutable: DatabaseMutable, IDatabaseSafe
    {

        public DatabaseSafeMutable(
            string name = null
            , IServer server = null
            , IConnectionSupplierSafe connectionSupplier = null
            , ISqlTranslator translator = null
            , ICascadingForeignKeyRegistry cascadingForeignKeyRegistry = null
            , IChangeSubscriberRegistry changeSubscriberRegistry = null
            , bool assert = true
            ) : base( 
                name
                , server
                , connectionSupplier
                , translator
                , cascadingForeignKeyRegistry
                , changeSubscriberRegistry
                )
        { }

        public override void SetConnectionSupplier(IConnectionSupplier connectionSupplier)
        {
            if (!typeof(IConnectionSupplierSafe).IsAssignableFrom(connectionSupplier.GetType()))
                throw new UnsafeConnectionSupplierException(connectionSupplier);
            ConnectionSupplier = connectionSupplier;
            Debug.Assert(PassesSelfAssertion());
        }

        public virtual ISafeMemberOfImmutableOwner GetSafeReference(bool assert = true)
        {
            return new DatabaseSafeMutable(
                Name
                , GetServerSafely()
                , GetConnectionSupplierSafely()
                , GetSqlTranslatorSafely()
                , assert
                );
        }
        public IConnectionSupplierSafe GetConnectionSupplierSafely()
        {
            if (ConnectionSupplier == null) return null;
            else if (typeof(ISafeMemberOfImmutableOwner).IsAssignableFrom(ConnectionSupplier.GetType()))
                return (IConnectionSupplierSafe)
                    ((ISafeMemberOfImmutableOwner)ConnectionSupplier).GetSafeReference();
            else throw new UnsafeConnectionSupplierException(ConnectionSupplier);
        }

    }

    public class DatabaseImmutable : ComparableByNameMutable, IDatabaseSafe
    {

        /***
         *  Private members
        ***/

        private IDatabaseSafe _delegee;

        /***
         *  Constructors
         ***/

        /// <summary>
        /// Disable default constructor
        /// </summary>
        private DatabaseImmutable() : base( "" ) { }

        public DatabaseImmutable(
            string name = null
            , IServer server = null
            , IConnectionSupplierSafe connectionSupplier = null
            , ISqlTranslator translator = null
            , ICascadingForeignKeyRegistry cascadingForeignKeyRegistry = null
            , IChangeSubscriberRegistry changeSubscriberRegistry = null
            ) : base(name)
        {
            _delegee
                = new DatabaseSafeMutable(
                    name
                    , server == null ? null : (IServer)server.GetSafeReference()
                    , connectionSupplier == null ? null : (IConnectionSupplierSafe)connectionSupplier.GetSafeReference()
                    , translator == null ? null : (ISqlTranslator)translator.GetSafeReference()
                    , cascadingForeignKeyRegistry == null ? null : (ICascadingForeignKeyRegistry)cascadingForeignKeyRegistry.GetSafeReference()
                    , changeSubscriberRegistry == null ? null : (IChangeSubscriberRegistry)changeSubscriberRegistry.GetSafeReference()
                    );
            Debug.Assert(PassesSelfAssertion(typeof(DatabaseImmutable)));
        }

        public DatabaseImmutable(IDatabaseSafe delegee) : base((delegee == null ? "" : delegee.Name))
        {
            _delegee = delegee;
            Debug.Assert(PassesSelfAssertion(typeof(DatabaseImmutable)));
        }

        // Object method overrides
        public override bool Equals(object obj)
        {
            return _delegee.Equals(obj);
        }

        public override int GetHashCode()
        {
            return _delegee.GetHashCode();
        }

        // ISelfAsserter methods
        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                bool retval = base.PassesSelfAssertion();
                Debug.Assert(
                    _delegee.Server != null
                    , "Server property of delegee for DataProcObjectImmutable object "
                    + "can never be null."
                    );
                Debug.Assert(
                    _delegee.Server.Name != null
                    , "Name property of Server property of delegee for DataProcObjectImmutable object "
                    + "can never be null."
                    );
                Debug.Assert(
                    _delegee.ConnectionSupplier != null
                    , "ConnectionSupplier property of delegee for DataProcObjectImmutable object "
                    + "can never be null."
                    );
                Debug.Assert(
                    typeof(ISafeMemberOfImmutableOwner).IsAssignableFrom(_delegee.ConnectionSupplier.GetType())
                    , "ConnectionSupplier property of delegee for DataProcObjectImmutable object "
                    + "must be an object that can return a safe reference to itself or an equivalent object."
                    );
                return retval;
            }
            else return true;
        }

        public bool SkipSelfAssertionUnlessThisType(Type testableType = null)
        {
            if (testableType == null) return false;
            return (!testableType.Equals(this.GetType()));
        }

        // IComparableByName methods and properties

        public string FullName { get { return _delegee.FullName; } }

        public int CompareByFullNameTo(IComparableByFullName other)
        {
            return ComparableByFullNameMutable.CompareByFullName(this, other);
        }

        public bool EqualsInFullName(IComparableByFullName other)
        {
            return ComparableByFullNameMutable.AreEqualInFullName(this, other);
        }

        // IConnectionSupplier methods

        public IConnection GetAConnection() { return _delegee.ConnectionSupplier.GetAConnection(); }

        public void ResumeControlOfConnection( IConnection conn, ITransaction trans = null )
        {
            _delegee.ConnectionSupplier.ResumeControlOfConnection(conn, trans);
        }

        // IMethodMatcher methods
        public virtual bool MembersMatch(Object obj)
        {
            return _delegee.MembersMatch(obj);
        }

        // ISelfAsserter methods
        public virtual ISafeMemberOfImmutableOwner GetSafeReference() { return this; }

        // IDatabase methods

        public bool IsMutableType { get { return false; } }
        public Type NearestMutableType
        {
            get
            {
                string className
                    = Regex.Replace(
                        GetType().Name
                        , "([Ii]nvariant){0,1}$1"
                        , "Mutable"
                        );
                return
                    Type.GetType(
                        GetType().Namespace + "." + className
                        );
            }
        }
        public IServer Server
        {
            get { return _delegee.GetServerSafely(); }
        }
        public IConnectionSupplier ConnectionSupplier
        {
            get { return _delegee.GetConnectionSupplierSafely(); }
        }
        public ISqlTranslator SqlTranslator
        {
            get { return _delegee.GetSqlTranslatorSafely(); }
        }
        public bool IsConnected
        {
            get { return _delegee.IsConnected; }
        }
        public string Description
        {
            get { return _delegee.Description; }
        }
        public ICascadingForeignKeyRegistry CascadingForeignKeyRegistry
        {
            get { return _delegee.GetCascadingForeignKeyRegistrySafely(); }
        }
        public IChangeSubscriberRegistry ChangeSubscriberRegistry
        {
            get { return _delegee.GetChangeSubscriberRegistrySafely(); }
        }
        public IServer GetServerSafely()
        {
            return _delegee.GetServerSafely();
        }
        public IConnectionSupplierSafe GetConnectionSupplierSafely()
        {
            return _delegee.GetConnectionSupplierSafely();
        }
        public ISqlTranslator GetSqlTranslatorSafely()
        {
            return _delegee.GetSqlTranslatorSafely();
        }
        public ICascadingForeignKeyRegistry GetCascadingForeignKeyRegistrySafely()
        {
            return _delegee.GetCascadingForeignKeyRegistrySafely();
        }
        public IChangeSubscriberRegistry GetChangeSubscriberRegistrySafely()
        {
            return _delegee.GetChangeSubscriberRegistrySafely();
        }
        public List<string[]> SqlExecuteReader(string sql, IConnection conn = null, ITransaction trans = null)
        {
            return _delegee.SqlExecuteReader(sql, conn, trans);
        }
        public List<string[]> SqlExecuteReader(
            List<string> script
            , IConnection conn = null
            , ITransaction trans = null
            , List<ISqlParameter> parameters = null
            , bool swallowExceptions = false)

        {
            return _delegee.SqlExecuteReader(script, conn, trans, parameters, swallowExceptions);
        }
        public object SqlExecuteSingleValue(
            string sql
            , IConnection conn = null
            , ITransaction trans = null
            , List<ISqlParameter> parameters = null
            )
        {
            return _delegee.SqlExecuteSingleValue(sql, conn, trans);
        }
        /// <summary>
        /// Executes a script, the last statement of which 
        /// returns a single value.
        /// </summary>
        /// <param name="script">The script to be executed.</param>
        /// <param name="conn">Optional connection provided by caller.</param>
        /// <param name="trans">Optional transaction provided by caller.</param>
        /// <returns></returns>
        public object SqlExecuteSingleValue(
            List<string> script
            , IConnection conn = null
            , ITransaction trans = null
            , List<ISqlParameter> parameters = null
            , bool swallowExceptions = false
            )
        {
            return _delegee.SqlExecuteSingleValue(script, conn, trans, parameters, swallowExceptions);
        }
        public bool SqlExecuteNonQuery(
            string sql
            , IConnection conn = null
            , ITransaction trans = null
            , List<ISqlParameter> parameters = null
            , bool swallowExceptions = false
            )
        {
            return _delegee.SqlExecuteNonQuery(sql, conn, trans);
        }
        public bool SqlExecuteNonQuery(
            List<string> script
            , IConnection conn = null
            , ITransaction trans = null
            , List<ISqlParameter> parameters = null
            , bool swallowExceptions = false
            )
        {
            return _delegee.SqlExecuteNonQuery(script, conn, trans, parameters, swallowExceptions);
        }

        public List<GenNameValue> SqlExecuteStoredProcedure(
            string procedure
            , List<ISqlParameter> parameters = null
            , IConnection conn = null
            , ITransaction trans = null
            )
        {
            return _delegee.SqlExecuteStoredProcedure(procedure, parameters, conn, trans);
        }

        public ITransaction BeginTransaction(IConnection conn = null)
        {
            return _delegee.BeginTransaction(conn);
        }
        public void CommitFully(IConnection conn, ITransaction trans = null)
        {
            _delegee.CommitFully(conn, trans);
        }
        public void Rollback(IConnection conn, ITransaction trans = null)
        {
            _delegee.Rollback(conn, trans);
        }

        public int? IDOfObject(IDatabaseObject obj)
        {
            return _delegee.IDOfObject(obj);
        }
        public bool ObjectExists(IDatabaseObject obj)
        {
            return _delegee.ObjectExists(obj);
        }
        public void EnsureObjectExists(IDatabaseObject obj)
        {
            _delegee.EnsureObjectExists(obj);
        }
        public void EnsureObjectExistsAsDesigned(IDatabaseObject obj)
        {
            _delegee.EnsureObjectExistsAsDesigned(obj);
        }
        public void DropObjectIfExists(IDatabaseObject obj)
        {
            _delegee.DropObjectIfExists(obj);
        }

        // IDatabase methods: database manipulation methods
        public int? IDOfDatabase(string name, IServer server)
        {
            return _delegee.IDOfDatabase(name, server);
        }
        public bool DatabaseExists(IDatabase database)
        {
            return _delegee.DatabaseExists(database);
        }
        public void EnsureDatabaseExists(IDatabase database)
        {
            _delegee.EnsureDatabaseExists(database);
        }
        public void DropDatabaseIfExists(IDatabase database)
        {
            _delegee.DropDatabaseIfExists(database);
        }

        // IDatabase methods: check constraint manipulation methods
        public int? IDOfCheckConstraint(string name, string tableName, string schemaName = null)
        {
            return _delegee.IDOfCheckConstraint(name, tableName, schemaName);
        }
        public bool CheckConstraintExists(ICheckConstraint constraint)
        {
            return _delegee.CheckConstraintExists(constraint);
        }
        public void EnsureCheckConstraintExists(ICheckConstraint constraint)
        {
            _delegee.EnsureCheckConstraintExists(constraint);
        }
        public void EnsureCheckConstraintExistsAsDesigned(ICheckConstraint constraint)
        {
            _delegee.EnsureCheckConstraintExistsAsDesigned(constraint);
        }
        public void DropCheckConstraintIfExists(ICheckConstraint constraint)
        {
            _delegee.DropCheckConstraintIfExists(constraint);
        }


        // IDatabase methods: column manipulation methods
        public int? IDOfColumn(
            string name
            , string vableName
            , string schemaName = null
            )
        {
            return _delegee.IDOfColumn(name, vableName, schemaName);
        }
        public bool ColumnExists(IColumn column)
        {
            return _delegee.ColumnExists(column);
        }
        public void EnsureTableColumnExists(ITableColumn column)
        {
            _delegee.EnsureTableColumnExists(column);
        }
        public void EnsureTableColumnExistsAsDesigned(ITableColumn column)
        {
            _delegee.EnsureTableColumnExistsAsDesigned(column);
        }
        public void DropTableColumnIfExists(ITableColumn column)
        {
            _delegee.DropTableColumnIfExists(column);
        }

        // IDatabase methods: Default constraint manipulation methods
        public int? IDOfDefaultConstraint(
            string name
            , string tableName
            , string schemaName = null
            )
        {
            return _delegee.IDOfDefaultConstraint(name, tableName, schemaName);
        }
        public bool DefaultConstraintExists(IDefaultConstraint constraint)
        {
            return _delegee.DefaultConstraintExists(constraint);
        }
        public void EnsureDefaultConstraintExists(IDefaultConstraint constraint)
        {
            _delegee.EnsureDefaultConstraintExists(constraint);
        }
        public void EnsureDefaultConstraintExistsAsDesigned(IDefaultConstraint constraint)
        {
            _delegee.EnsureDefaultConstraintExistsAsDesigned(constraint);
        }
        public void DropDefaultConstraintIfExists(IDefaultConstraint constraint)
        {
            _delegee.DropDefaultConstraintIfExists(constraint);
        }

        // IDatabase methods: Foreign key manipulation methods
        public int? IDOfForeignKey(
            string name
            , string tableName
            , string schemaName = null
            )
        {
            return _delegee.IDOfForeignKey(name, tableName, schemaName);
        }
        public bool ForeignKeyExists(IForeignKey key)
        {
            return _delegee.ForeignKeyExists(key);
        }
        public void EnsureForeignKeyExists(IForeignKey key, bool cascadeManually = false)
        {
            _delegee.EnsureForeignKeyExists(key);
        }
        public void EnsureForeignKeyExistsAsDesigned(IForeignKey key, bool cascadeManually = false)
        {
            _delegee.EnsureForeignKeyExistsAsDesigned(key);
        }
        public void DropForeignKeyIfExists(IForeignKey key)
        {
            _delegee.DropForeignKeyIfExists(key);
        }

        // IDatabase methods: index manipulation methods
        public int? IDOfIndex(
            string name
            , string tableName
            , string schemaName = null
            )
        {
            return _delegee.IDOfForeignKey(name, tableName, schemaName);
        }
        public bool IndexExists(IIndex index)
        {
            return _delegee.IndexExists(index);
        }
        public void EnsureIndexExists(IIndex index)
        {
            _delegee.EnsureIndexExists(index);
        }
        public void EnsureIndexExistsAsDesigned(IIndex index)
        {
            _delegee.EnsureIndexExistsAsDesigned(index);
        }
        public void DropIndexIfExists(IIndex index)
        {
            _delegee.DropIndexIfExists(index);
        }

        // IDatabase methods: primary key manipulation methods
        public int? IDOfPrimaryKey(
            string name
            , string tableName
            , string schemaName = null
            )
        {
            return _delegee.IDOfForeignKey(name, tableName, schemaName);
        }
        public bool PrimaryKeyExists(IPrimaryKey key)
        {
            return _delegee.PrimaryKeyExists(key);
        }
        public void EnsurePrimaryKeyExists(IPrimaryKey key)
        {
            _delegee.EnsurePrimaryKeyExists(key);
        }
        public void EnsurePrimaryKeyExistsAsDesigned(IPrimaryKey key)
        {
            _delegee.EnsurePrimaryKeyExistsAsDesigned(key);
        }
        public void DropPrimaryKeyIfExists(IPrimaryKey key)
        {
            _delegee.DropPrimaryKeyIfExists(key);
        }

        // IDatabase methods: scalar function manipulation methods
        public int? IDOfScalarFunction(
            string name
            , string schemaName = null
            )
        {
            return _delegee.IDOfScalarFunction(name, schemaName);
        }
        public bool ScalarFunctionExists(IScalarFunction function)
        {
            return _delegee.ScalarFunctionExists(function);
        }
        public void EnsureScalarFunctionExists(IScalarFunction function)
        {
            _delegee.EnsureScalarFunctionExists(function);
        }
        public void EnsureScalarFunctionExistsAsDesigned(IScalarFunction function)
        {
            _delegee.EnsureScalarFunctionExistsAsDesigned(function);
        }
        public void DropScalarFunctionIfExists(IScalarFunction function)
        {
            _delegee.DropScalarFunctionIfExists(function);
        }

        // IDatabase methods: schema manipulation methods
        public int? IDOfSchema(
            string name
            )
        {
            return _delegee.IDOfSchema(name);
        }
        public bool SchemaExists(ISchema schema)
        {
            return _delegee.SchemaExists(schema);
        }
        public void EnsureSchemaExists(ISchema schema)
        {
            _delegee.EnsureSchemaExists(schema);
        }
        public void DropSchemaIfExists(ISchema schema)
        {
            _delegee.DropSchemaIfExists(schema);
        }

        // IDatabase methods: Sequence manipulation methods
        public int? IDOfSequence(
            string name
            , string schemaName = null
            )
        {
            return _delegee.IDOfSequence(name, schemaName);
        }
        public bool SequenceExists(IDatabaseSequence sequence)
        {
            return _delegee.SequenceExists(sequence);
        }
        public void EnsureSequenceExists(IDatabaseSequence sequence)
        {
            _delegee.EnsureSequenceExists(sequence);
        }
        public void EnsureSequenceExistsAsDesigned(IDatabaseSequence sequence)
        {
            _delegee.EnsureSequenceExistsAsDesigned(sequence);
        }
        public void DropSequenceIfExists(IDatabaseSequence sequence)
        {
            _delegee.DropSequenceIfExists(sequence);
        }

        // IDatabase methods: stored procedure manipulation methods
        public int? IDOfStoredProcedure(
            string name
            , string schemaName = null
            )
        {
            return _delegee.IDOfStoredProcedure(name, schemaName);
        }
        public bool StoredProcedureExists(IStoredProcedure proc)
        {
            return _delegee.StoredProcedureExists(proc);
        }
        public void EnsureStoredProcedureExists(IStoredProcedure proc)
        {
            _delegee.EnsureStoredProcedureExists(proc);
        }
        public void EnsureStoredProcedureExistsAsDesigned(IStoredProcedure proc)
        {
            _delegee.EnsureStoredProcedureExistsAsDesigned(proc);
        }
        public void DropStoredProcedureIfExists(IStoredProcedure proc)
        {
            _delegee.DropStoredProcedureIfExists(proc);
        }

        // IDatabase methods: table manipulation methods
        public int? IDOfTable(
            string name
            , string schemaName = null
            )
        {
            return _delegee.IDOfTable(name, schemaName);
        }
        public bool TableExists(ITable table)
        {
            return _delegee.TableExists(table);
        }
        public void EnsureTableExists(ITable table)
        {
            _delegee.EnsureTableExists(table);
        }
        public void EnsureTableExistsAsDesigned(ITable table)
        {
            _delegee.EnsureTableExistsAsDesigned(table);
        }
        public void DropTableIfExists(ITable table)
        {
            _delegee.DropTableIfExists(table);
        }
        public List<IColumn> TableColumnsInDatabase(ITable table)
        {
            return _delegee.TableColumnsInDatabase(table);
        }
        public List<IColumn> TableIdentityColumnInDatabase(ITable table)
        {
            return _delegee.TableIdentityColumnInDatabase(table);
        }
        public List<IColumn> TableComputedColumnsInDatabase(ITable table)
        {
            return _delegee.TableComputedColumnsInDatabase(table);
        }

        public List<string[]> TableForeignKeyDependentsInDatabase(ITable table)
        {
            return _delegee.TableForeignKeyDependentsInDatabase(table);
        }

        // IDatabase methods: trigger manipulation methods
        public int? IDOfTrigger(
            string name
            , string tableName
            , string schemaName = null
            )
        {
            return _delegee.IDOfTrigger(name, tableName, schemaName);
        }
        public bool TriggerExists(ITableTrigger trigger)
        {
            return _delegee.TriggerExists(trigger);
        }
        public void EnsureTriggerExists(ITableTrigger trigger)
        {
            _delegee.EnsureTriggerExists(trigger);
        }
        public void EnsureTriggerExistsAsDesigned(ITableTrigger trigger)
        {
            _delegee.EnsureTriggerExistsAsDesigned(trigger);
        }
        public void DropTriggerIfExists(ITableTrigger trigger)
        {
            _delegee.DropTriggerIfExists(trigger);
        }

        // IDatabase methods: unique key manipulation methods
        public int? IDOfUniqueKey(
            string name
            , string tableName
            , string schemaName = null
            )
        {
            return _delegee.IDOfUniqueKey(name, tableName, schemaName);
        }
        public bool UniqueKeyExists(IUniqueKey key)
        {
            return _delegee.UniqueKeyExists(key);
        }
        public void EnsureUniqueKeyExists(IUniqueKey key)
        {
            _delegee.EnsureUniqueKeyExists(key);
        }
        public void EnsureUniqueKeyExistsAsDesigned(IUniqueKey key)
        {
            _delegee.EnsureUniqueKeyExistsAsDesigned(key);
        }
        public void DropUniqueKeyIfExists(IUniqueKey key)
        {
            _delegee.DropUniqueKeyIfExists(key);
        }

        // IDatabase methods: user manipulation methods
        public int? IDOfUser(string name)
        {
            return _delegee.IDOfUser(name);
        }
        public bool UserExists(IUser user)
        {
            return _delegee.UserExists(user);
        }
        public void EnsureUserExists(IUser user)
        {
            _delegee.EnsureUserExists(user);
        }
        public void EnsureUserExistsAsDesigned(IUser user)
        {
            _delegee.EnsureUserExistsAsDesigned(user);
        }
        public void DropUserIfExists(IUser user)
        {
            _delegee.DropUserIfExists(user);
        }

        // IDatabase methods: view manipulation methods
        public int? IDOfView(
            string name
            , string schemaName = null
            )
        {
            return _delegee.IDOfUniqueKey(name, schemaName);
        }
        public bool ViewExists(IView view)
        {
            return _delegee.ViewExists(view);
        }
        public void EnsureViewExists(IView view)
        {
            _delegee.EnsureViewExists(view);
        }
        public void EnsureViewExistsAsDesigned(IView view)
        {
            _delegee.EnsureViewExistsAsDesigned(view);
        }
        public void DropViewIfExists(IView view)
        {
            _delegee.DropViewIfExists(view);
        }

        // IDatabase methods: SQL construction methods
        public bool IsValidObjectName(string name)
        {
            return _delegee.IsValidObjectName(name);
        }
        public string ValidObjectNameFrom(string name)
        {
            return _delegee.ValidObjectNameFrom(name);
        }
        public string TableConstraintName(string root, string tableName)
        {
            return _delegee.TableConstraintName(root, tableName);
        }

        // Private utility methods
        private string FullNameOf(IFullNamed obj, string defaultValue = null)
        {
            return UtilitiesForIFullNamed.FullNameOf(obj, defaultValue);
        }
        private string NameOf(INamed obj, string defaultValue = null)
        {
            return UtilitiesForINamed.NameOf(obj, defaultValue);
        }

        private void ValidateNamedObjectAsArgument(INamed element, string objectTypeName, string paramName, string callingMethod)
        {
            UtilitiesForINamed.NonnullNamedParameterIsValid(
                element
                , objectTypeName
                , paramName
                , this.GetType().FullName + "." + callingMethod
                );
        }

    }

}
