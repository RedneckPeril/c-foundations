﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface ILastUpdateDtmColumn : ITableColumn { }
    public interface ILastUpdateDtmColumnMutable : ILastUpdateDtmColumn, ITableColumnMutable { }

    /// <summary>
    /// Boilerplate implementation of ILastUpdateDtmColumnMutable.
    /// </summary>
    /// <remarks>
    /// Sealed class for now.
    /// <remarks>
    public sealed class LastUpdateDtmColumnMutable : TableColumnMutable, ILastUpdateDtmColumnMutable
    {

        public LastUpdateDtmColumnMutable(
            ISqlTranslator sqlTranslator 
            , ITable table
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
            : base(
                  "lastUpdateDtm"
                  , "datetime"
                  , sqlTranslator
                  , table: table
                  , isIdentity: false
                  , isNullable: true
                  , nameWhenPrefixed: "LastUpdateDtm"
                  , constraintTag: "LUD"
                  , id: id
                  , retrieveIDsFromDatabase: false
                  )
        {
            if (typeof(ITable).IsAssignableFrom(table.GetType()))
                AddOrOverwriteCheckConstraint(
                    new SingleColumnCheckConstraintMutable(
                        name: Database.SqlTranslator.TableConstraintName( "_ckLUD", ParentObject.Name )
                        , sqlTranslator: SqlTranslator
                        , table: (ITable)ParentObject
                        , constraintText: "lastUpdateDtm is not null"
                        , constrainedColumn: this
                        , id: null
                        , retrieveIDsFromDatabase: false
                        , assert: false
                        )
                       );
            CompleteConstructionIfType(typeof(LastUpdateDtmColumnMutable), retrieveIDsFromDatabase, assert);
        }

        // IDataProcObject method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    "lastUpdateDtm".Equals(Name)
                    , "A LastUpdateDtmColumn must be named 'lastUpdateDtm', not "
                    + (Name == null ? "[null]" : "'" + Name + "'")
                    + "."
                    );
                Debug.Assert(
                    "datetime".Equals(DataType)
                    , "A LastUpdateDtmColumn must be typed as datetime, not "
                    + (DataType == null ? "[null]" : DataType)
                    + "."
                    );
                Debug.Assert(
                    !IsIdentity
                    , "A LastUpdateDtmColumn cannot be an identity column."
                    );
                Debug.Assert(
                    IsNullable
                    , "A LastUpdateDtmColumn must be nullable (though constrained non-null)."
                    );
                Debug.Assert(
                    !IsComputed
                    , "A LastUpdateDtmColumn cannot be a computed column."
                    );
                Debug.Assert(
                    CalcFormula == null
                    , "A LastUpdateDtmColumn cannot be a computed column "
                    + "and therefore cannot have a computed definition."
                    );
                Debug.Assert(
                    !IsToBeTrimmed
                    , "A LastUpdateDtmColumn cannot be a trimmed column."
                    );
                Debug.Assert(
                    !IsImmutable
                    , "A LastUpdateDtmColumn cannot be an invariant column."
                    );
                Debug.Assert(
                    !IsPersisted
                    , "A LastUpdateDtmColumn cannot be a computed column "
                    + "and therefore cannot be flagged as persistent."
                    );
                Debug.Assert(
                    DefaultConstraint == null
                    , "A LastUpdateDtmColumn cannot have a default constraint."
                    );
                if (typeof(ITable).IsAssignableFrom(Owner.GetType()))
                {
                    Debug.Assert(
                        CheckConstraints != null
                        , "A LastUpdateDtmColumn must have at least one check constraint, "
                        + "enforcing non-nullability."
                        );
                    Debug.Assert(
                        CheckConstraints.Values.Where(c => c.ConstraintText.Equals("lastUpdateDtm is not null")).Count() > 0
                        , "At least one of the check constraints on a LastUpdateDtmColumn "
                        + "must enforce non-nullability."
                        );
                }
            }
            return true;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new LastUpdateDtmColumnMutable(
                GetSqlTranslatorSafely()
                , GetTableSafely()
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        // ObjectInSchemaMutable method overrides
        ///   <list type="bullet">
        ///   <item>SetParentObject, if (as is usually the case) ParentObject
        /// is constrained to be an instance of a particular object type.</item>
        ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
        /// added as members.</item>
        ///   <item>GetCopyForParentObject
        ///   

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            if (!typeof(ITable).IsAssignableFrom(parentObject.GetType()))
                throw new BadParentObjectTypeException(
                    this
                    , this.GetType().FullName
                    , parentObject.GetType().FullName
                    , typeof(ITable).FullName
                    );
            return new LastUpdateDtmColumnMutable(
               SqlTranslator
                , (ITable)parentObject
                , ID
                , retrieveIDsFromDatabase
                , assert
                );
        }

    }

    /// <summary>
    /// Boilerplate implementation of ITableColumn without variability.
    /// </summary>
    /// <remarks>
    /// Sealed class for now.
    /// <remarks>
    public sealed class LastUpdateDtmColumnImmutable : TableColumnImmutable, ILastUpdateDtmColumn
    {

        private ILastUpdateDtmColumn Delegee { get { return (ILastUpdateDtmColumn)_delegee; } }

        public LastUpdateDtmColumnImmutable(
            ISqlTranslator sqlTranslator
            , ITable table
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
            : base()
        {
            _delegee = new LastUpdateDtmColumnMutable(
                sqlTranslator
                , table
                , id
                , retrieveIDsFromDatabase
                , assert: false
                );
            CompleteConstructionIfType(
                typeof(LastUpdateDtmColumnImmutable)
                , retrieveIDsFromDatabase: false
                , assert: false
                );
        }

        public LastUpdateDtmColumnImmutable(ILastUpdateDtmColumn delegee, bool assert)
            : base(delegee)
        {
            CompleteConstructionIfType(
                typeof(LastUpdateDtmColumnImmutable)
                , retrieveIDsFromDatabase: false
                , assert: false
                );
        }

        // DataProcObject method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(ILastUpdateDtmColumn); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new LastUpdateDtmColumnImmutable(
                (ILastUpdateDtmColumn)
                Delegee.GetReferenceForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , assert: false
                    )
                , assert: assert
                );
        }

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new LastUpdateDtmColumnImmutable(
                (ILastUpdateDtmColumn)
                Delegee.GetCopyForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , assert: false
                    )
                , assert: assert
                );
        }

    }

}