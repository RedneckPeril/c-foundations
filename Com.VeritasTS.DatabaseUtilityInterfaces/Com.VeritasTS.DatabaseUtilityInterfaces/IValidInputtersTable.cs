﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{
    public interface IValidInputtersTable : ITable { }
    public interface IValidInputtersTableMutable : IValidInputtersTable, ITableMutable { }

    /// <summary>
    /// Boilerplate implementation of IValidInputtersTableMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    ///   affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidOwnerTypesWhenNotOwner, occasionally if not abstract type
    ///   and if Owner is not required to be ParentObject.</item>
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>ParentObjectMustBeOwner, if true rather than false.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class ValidInputtersTableMutable : TableMutable, IValidInputtersTableMutable
    {

        public static string DefaultNameColumnName { get { return "name"; } }
        public static string DefaultNameColumnConstraintTag { get { return "N"; } }

        // Constructors

        public ValidInputtersTableMutable() : base() { }

        public ValidInputtersTableMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ISchema schema = null
            , ITable clientTable = null
            , bool allowsUpdates = true
            , bool allowsDeletes = true
            , string nameWhenPrefixed = null
            , int? id = default(int?)
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name == null ? "" : name
                , sqlTranslator
                , schema:
                schema != null
                ? schema
                : (clientTable != null ? clientTable.Schema : null)
                , objectType: ObjectTypes.Table()
                , allowsUpdates: allowsUpdates
                , allowsDeletes: allowsDeletes
                , hasIDColumn: true
                , nameWhenPrefixed: nameWhenPrefixed
                , id: id
                , retrieveIDsFromDatabase: false
                , assert: false
                )
        {
            if (Name.Trim().Length == 0)
                SetName(clientTable.Name + "VIs", false);
            ConstructNewMembers();
            CompleteConstructionIfType(typeof(ValidInputtersTableMutable), retrieveIDsFromDatabase, assert);
        }

        // DataProcObjectMutable method overrides

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , schema: null
                , clientTable: null
                , allowsUpdates: true
                , allowsDeletes: true
                , nameWhenPrefixed: null
                , id: default(int?)
                );
        }

        protected virtual void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator
            , ISchema schema
            , ITable clientTable
            , bool allowsUpdates
            , bool allowsDeletes
            , string nameWhenPrefixed
            , int? id = default(int?)
            )
        {
            if (Name.Trim().Length == 0)
                SetName(clientTable.Name + "VIs", false);
            base.ConstructAfterConstruction(
                name: name
                , sqlTranslator: sqlTranslator
                , schema: schema
                , objectType: ObjectTypes.Table()
                , owner: clientTable
                , allowsUpdates: allowsUpdates
                , allowsDeletes: allowsDeletes
                , columns: null
                , indexes: null
                , primaryKey: null
                , primaryKeySequence: null
                , uniqueKeys: null
                , foreignKeys: null
                , triggers: null
                , checkConstraints: null
                , defaultConstraints: null
                , isAudited: false
                , auditTableName: null
                , publishesChanges: false
                , changeTrackingTableName: null
                , parentTable: null
                , parentPassThrough: null
                , canHaveChildren: false
                , leavesTableName: null
                , maintainChildVablesTable: false
                , childVablesTableName: null
                , hasIDColumn: true
                , idColumnName: null
                , hasInputterColumn: false
                , inputtersColumnName: null
                , validInputtersTableName: null
                , hasLastUpdateDtmColumn: false
                , hasLastUpdaterColumn: false
                , nameWhenPrefixed: null
                , id: default(int?)
                );
            ConstructNewMembers();
        }

        private void ConstructNewMembers()
        {
            ISqlTranslator sql = SqlTranslator;
            if (sql == null)
                throw new ConstructionRequiresSqlTranslatorException(
                    this
                    , typeof(TableMutable).FullName
                    );
            string boolSql = sql.BooleanTypeSql();
            SetHasIDColumn(
                true
                , retrieveIDColumnIDFromDatabase: false
                , assert: false
                );
            string nameColumnCheckConstraintName
                = sql.TableConstraintName("_ck" + DefaultNameColumnConstraintTag, Name);
            AddOrOverwriteColumn(
                new ColumnMutable(
                    DefaultNameColumnName
                    , "nvarchar(255)"
                    , sql
                    , this
                    , isNullable: false
                    , isToBeTrimmed: true
                    , isImmutable: true
                    , checkConstraints: new Dictionary<string, ICheckConstraint>
                    {
                        [nameColumnCheckConstraintName]
                        = new CheckConstraintMutable(
                            nameColumnCheckConstraintName
                            , SqlTranslator
                            , this
                            , sql.TrimmingConstraintText(DefaultNameColumnName)
                            , retrieveIDsFromDatabase: false
                            , assert: false
                            )
                    }
                    , retrieveIDsFromDatabase: false
                    , assert: false
                    )
                );
            SetHasLastUpdateDtmColumn(true, false, false);
            SetHasLastUpdaterColumn(true, false, false);
            SetPrimaryKey(
                new PrimaryKeyMutable(
                    sql.TableConstraintName("_pk", Name)
                    , sql
                    , this
                    , new List<string> { IDColumnName }
                    , retrieveIDsFromDatabase: false
                    , assert: false
                    )
                );
            AddOrOverwriteUniqueKey(
                new UniqueKeyMutable(
                    sql.TableConstraintName("_lpk", Name)
                    , sql
                    , this
                    , new List<string> { DefaultNameColumnName }
                    , isClustered: false
                    , retrieveIDsFromDatabase: false
                    , assert: false
                    )
                );
        }

    }

    /// <summary>
    /// Boilerplate implementation of IValidInputtersTable without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public class ValidInputtersTableImmutable : TableImmutable, IValidInputtersTable
    {
        private IValidInputtersTable Delegee { get { return (IValidInputtersTable)_delegee; } }

        //Disable default constructor
        private ValidInputtersTableImmutable() : base(delegee: null) { }

        public ValidInputtersTableImmutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ISchema schema = null
            , ITable clientTable = null
            , bool allowsUpdates = true
            , bool allowsDeletes = true
            , string nameWhenPrefixed = null
            , int? id = default(int?)
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(delegee: null)
        {
            _delegee =
                new ValidInputtersTableMutable(
                    name
                    , sqlTranslator
                    , schema
                    , clientTable
                    , allowsUpdates
                    , allowsDeletes
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    );
            CompleteConstructionIfType(typeof(ValidInputtersTableImmutable), false, true);
        }

        public ValidInputtersTableImmutable(IValidInputtersTable delegee) : base(delegee: null)
        {
            _delegee = delegee;
            CompleteConstructionIfType(typeof(ValidInputtersTableImmutable), false, true);
        }

        // DataProcObject method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(IValidInputtersTable); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            IObjectInSchema retval
                = new ValidInputtersTableImmutable(
                (IValidInputtersTable)
                    ((IObjectInSchema)_delegee).GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, assert)
                    );
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }
        public override IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            IObjectInSchema retval
                = new ValidInputtersTableImmutable(
                (IValidInputtersTable)
                    ((IObjectInSchema)_delegee).GetCopyForParentObject(parentObject, retrieveIDsFromDatabase, assert)
                    );
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }

    }

}