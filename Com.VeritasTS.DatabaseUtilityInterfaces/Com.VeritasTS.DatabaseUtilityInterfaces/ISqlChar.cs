﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// This interface identifies the "smallint"
    /// SQL type.
    /// </summary>
    /// <remarks>
    /// We ordinarily just retrieve the static object
    /// SqlChar.Standard(). Objects are immutable
    /// and therefore threadsafe.
    /// </remarks>
    public interface ISqlChar : ISqlStringType { }

    /// <summary>
    /// A basic, immutable implementation of the ISqlChar interface.
    /// </summary>
    public sealed class SqlChar : SqlStringType, ISqlChar
    {

        /****************
         *  Private members
         ***************/

        private string FullTypeNameForCodeErrors { get { return typeof(SqlChar).FullName; } }
        private static SqlChar _standard;

        /****************
         *  Constructor
         ***************/

        public SqlChar(int maxLength = 1) : base(maxLength) { }

        /****************
         *  ISqlChar properties and methods
         ***************/

        public override string DefaultSql
        {
            get { return "char(" + MaxLength + ")"; }
        }

        public static SqlChar Standard()
        {
            if (_standard == null) _standard = new SqlChar();
            return _standard;
        }

    }

}
