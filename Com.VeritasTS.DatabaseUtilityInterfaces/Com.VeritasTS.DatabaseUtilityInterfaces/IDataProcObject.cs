﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;
using System.Data;
using System.Data.SqlClient;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// Base interface for things that can own objects in database.
    /// </summary>
    /// <remarks>
    /// The interface exists primarily to support the concept
    /// of an "owner" of an object in a database, such that 
    /// the "owner" is not necessarily the parent object --
    /// that is, the "owner" may not be recognized by the 
    /// database as having anything to do with the parent object.
    /// The parent object, on the other hand, is the thing
    /// whose name immediately precedes the object's own name
    /// in the object's full name. Thus, for example, a 
    /// default constraint on a database column would
    /// have the full name server.database.schema.table.constraint,
    /// and therefore its parent object is the table, not the
    /// column it constrains. But the column would be
    /// its owner.
    /// <para>
    /// Because interaction with database objects practically
    /// always will involve SQL, any IDataProcObject must
    /// be able to communicate in SQL and therefore must
    /// have a non-null SqlTranslator.
    /// </para>
    /// <para>
    /// Extends ISelfAsserter, IMemberMatcher, IComparableByName, 
    /// IComparableByFullName, and ISafeMemberOfImmutableOwner.
    /// </para>
    /// </remarks>
    public interface IDataProcObject : ISelfAsserter, IMemberMatcher, IComparableByName, IComparableByFullName, ISafeMemberOfImmutableOwner
    {

        /// <summary>
        /// True if object is variant after completion of
        /// logical construction, else false.
        /// </summary>
        /// <remarks>
        /// Invariance is by far the best way to ensure
        /// thread-safety.
        /// </remarks>
        bool IsMutableType { get; }

        /// <summary>
        /// The most closely related variant type to the
        /// type of the current object.
        /// </summary>
        /// <remarks>
        /// If the type itself is variant, returns the
        /// object's own type. If no variant type is deemed
        /// adequately similar, returns null. An "adequately
        /// similar" type is one (a) that implements all
        /// interfaces implemented by the object itself,
        /// and (b) whose primary constructor
        /// parameters are interchangeable with the 
        /// parameters of the object's own primary 
        /// constructor.
        /// <para>
        /// By default, the NearestMutableType of invariant type 
        /// ATypeNamedSomethingImmutable is a variant type
        /// ATypeNamedSomethingMutable, and 
        /// the NearestMutableType of invariant type 
        /// ATypeNamedSomething is also a variant type
        /// ATypeNamedSomethingMutable.</para>
        /// </remarks>
        Type NearestMutableType { get; }
        ISqlTranslator SqlTranslator { get; }

        /// <summary>
        /// Completes construction of an object
        /// </summary>
        /// <remarks>
        /// The last step in constructing an object
        /// should generally be to mark it as fully
        /// constructed and self-assert it. In the 
        /// case of database objects, the last
        /// true construction step is to look up
        /// the ID in the database (if we are
        /// actually connected to a database, that
        /// is). However, just because a constructor
        /// is ending, is no proof that the object
        /// is fully constructed, for two reasons.
        /// <list type="bullet">
        /// <item>
        /// The constructor may be a base constructor
        /// called from the constructor of a descendant
        /// object. In that case, only the final
        /// constructor should complete construction.
        /// Therefore each constructor passes the
        /// type of object it constructs, and if this
        /// is not the actual type of the object, 
        /// CompleteConstruction null-ops.
        /// </item>
        /// <item>In certain circumstances an
        /// object is not actually fully self-assertible
        /// even after the constructor completes --
        /// things such as database columns self-assert
        /// that their parentObject knows it owns
        /// them, which it doesn't until the object
        /// has been constructed and attached to the
        /// parent object. Furthermore, there is
        /// no point in looking for the ID of a table
        /// column in the database before the table
        /// itself is there; so fetching the ID
        /// also is not always something that should
        /// be done as the last step of the constructor.
        /// <para>
        /// So there are optional arguments to allow
        /// the method that creates the argument to
        /// defer ID lookup and self-assertion to some
        /// indefinite time after construction.
        /// The IsFullyConstructed flag is set to true,
        /// but the ID is left unfetched and the 
        /// object is left un-self-asserted. The
        /// creating method is responsible for
        /// fetching the IDs and self-asserting
        /// at a later point, whenever the parent object
        /// has been fully assembled.
        /// </para>
        /// </item>
        /// </list>
        /// </remarks>
        /// <param name="t">The type of object that
        /// the calling constructor is designed to
        /// create.</param>
        /// <param name="retrieveIDsFromDatabase">
        /// True if the object should retrieve its database ID,
        /// and the IDs of its children if it has any,
        /// as the final step of construction.</param>
        /// <param name="assert">True if the object
        /// should self-assert upon completion of 
        /// construction.</param>
        void CompleteConstructionIfType(Type t, bool? retrieveIDsFromDatabase, bool assert = true);
    }

    /// <summary>
    /// Base interface for things that can own objects in database
    /// and that can be altered after construction.
    /// </summary>
    /// <remarks>
    /// Adds variability to IDataProcObject.
    /// </remarks>
    public interface IDataProcObjectMutable : IDataProcObject
    {
        void SetName(string name, bool assert = true);
        void SetSqlTranslator(ISqlTranslator sqlTranslator, bool assert = true);
    }

    public class NullSqlTranslatorException : Exception
    {
        public NullSqlTranslatorException() : base(
                message: "Cannot set SqlTranslator property of IDataProc object to null."
                )
        { }
    }

    /// <summary>
    /// Boilerplate implementation of IDataProcObjectMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public abstract class DataProcObjectMutable : SelfAsserter, IDataProcObjectMutable
    {

        // Static utility members and functions
        public static bool HasNonWhitespace( string s) { return UtilitiesForINamed.HasNonWhitespace(s); }

        private static Dictionary<int, int> _nextPrimes;
        /// <summary>
        /// Quick lookup of next prime number
        /// given current prime number; used
        /// in building hash codes as descendant
        /// objects add additional members that
        /// affect the result of Equals.
        /// </summary>
        public static Dictionary<int, int> NextPrimes
        {
            get
            {
                if (_nextPrimes == null)
                    _nextPrimes = new Dictionary<int, int>
                    {
                        [2] = 3,
                        [3] = 5,
                        [5] = 7,
                        [7] = 11,
                        [11] = 13,
                        [13] = 17,
                        [17] = 19,
                        [19] = 23,
                        [23] = 29,
                        [29] = 31,
                        [31] = 37,
                        [37] = 41,
                        [41] = 43,
                        [43] = 47,
                        [47] = 53,
                        [53] = 59,
                        [59] = 61,
                        [61] = 67,
                        [67] = 71,
                        [71] = 73,
                        [73] = 79,
                        [79] = 83,
                        [83] = 89,
                        [89] = 97,
                        [97] = 101,
                        [101] = 103,
                        [103] = 107,
                        [107] = 109,
                        [109] = 113,
                        [113] = 127,
                        [127] = 131,
                        [131] = 137,
                        [137] = 139,
                        [139] = 149,
                        [149] = 151,
                        [151] = 157,
                        [157] = 163,
                        [163] = 167,
                        [167] = 173,
                        [173] = 179,
                        [179] = 181,
                        [181] = 191,
                        [191] = 193,
                        [193] = 197,
                        [197] = 199
                    };
                return _nextPrimes;
            }
        }
        protected bool _isFullyConstructed;
        private string _name;
        private ISqlTranslator _sqlTranslator;

        // Constructors

        /// <summary>
        /// Default constructor, which may not fully
        /// construct the object into a useful state,
        /// depending upon how a specific descent class
        /// is designed.
        /// </summary>
        /// <remarks>
        /// The default constructor is used by frameworks and does
        /// NOT mark the object has having been fully constructed.
        /// This means that assertions do not fire until the 
        /// creator explicitly completes the construction, allowing
        /// for objects to be constructed from the class name only
        /// using the null constructor, then initialized with
        /// Set method calls, and finally marked as ready for use.
        /// </remarks>
        protected DataProcObjectMutable()
        {
            _isFullyConstructed = false;
        }

        public DataProcObjectMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            )
        {
            _isFullyConstructed = false;
            ConstructNewMembers(name, sqlTranslator, true);
        }

        // Object method overrides

        /// <summary>
        /// Overriden Object.Equals object.
        /// </summary>
        /// <remarks>
        /// Should not be overridden by descendant classes.
        /// </remarks>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return EqualityUtilities.MemberMatchwiseEqualOrBothNull(this, obj);
        }

        /// <summary>
        /// Overriden Object.GetHashCode object.
        /// </summary>
        /// <remarks>
        /// Should be overridden by all descendant classes,
        /// and only those descendant classes, that add
        /// new members into MemberMatchwiseEqualOrBothNull.
        /// The general rule is that each new member's hash code
        /// is added to the hash code that would be generated
        /// by the nearest ancestor, after having been multiplied
        /// by a prime number not used by other members.
        /// Since descendants will need to know what
        /// prime numbers have already been used, each
        /// class must also publish its HighestPrimeUsedInHashCode.
        /// The descendant class then starts using prime numbers
        /// starting with NextPrimes[base.HighestPrimeUsedInHashCode].
        /// </remarks>
        /// <param name="obj">The object to which this is
        /// being compared for equality.</param>
        /// <returns>The object's hash code.</returns>
        public override int GetHashCode()
        {
            return (Name == null ? 0 : Name.GetHashCode())
                + 3 * (SqlTranslator == null ? 0 : SqlTranslator.GetHashCode());
        }

        // Object method overrides: GetHashCode assistants


        /// <summary>
        /// Highest prime number used with member objects
        /// in GetHashCode(), in this or prior generations.
        /// </summary>
        /// <remarks>
        /// Should be overridden by all descendant classes,
        /// and only those descendant classes, that override GetHashCode.
        /// The general rule is that each new member's hash code
        /// is added to the hash code that would be generated
        /// by the nearest ancestor, after having been multiplied
        /// by a prime number not used by other members.
        /// Since descendants will need to know what
        /// prime numbers have already been used, each
        /// class must also publish its HighestPrimeUsedInHashCode.
        /// The descendant class then starts using prime numbers
        /// starting with NextPrimes[base.HighestPrimeUsedInHashCode].
        /// </remarks>
        /// <returns>The highest prime used so far.</returns>
        public virtual int HighestPrimeUsedInHashCode
        {
            get { return 3; }
        }

        //  ISelfAsserter methods

        public override bool PassesSelfAssertion(Type testableType = null)
        {

            if (!SkipSelfAssertion(testableType))
            {
                base.PassesSelfAssertion();
                Debug.Assert(
                    Name != null
                    , "Name property of IDataProcObject cannot be null."
                    );
                Debug.Assert(
                    new Regex("\\S").IsMatch(Name)
                    , "Name property of IDataProcObject must contain "
                    + "something other than whitespace."
                    );
                Debug.Assert(
                    SqlTranslator != null
                    , "SqlTranslator property of IDataProcObject cannot be null."
                    );
            }
            return true;
        }

        // IMemberMatcher methods
        public virtual bool MembersMatch(Object obj, bool checkOtherSide = true)
        {
            if (obj == null) return false;
            if (!typeof(IDataProcObject).IsAssignableFrom(obj.GetType())) return false;
            IDataProcObject other = (IDataProcObject)obj;
            if (!EqualityUtilities.EqualOrBothNull(this.Name, other.Name)) return false;
            if (!EqualityUtilities.EqualOrBothNull(this.SqlTranslator, other.SqlTranslator)) return false;
            return true;
        }

        // IComparableByName methods and properties

        public string Name
        {
            get { return _name; }
            set { SetName(value); }
        }
        public int CompareByNameTo(IComparableByName other)
        {
            return ComparableByNameMutable.CompareByName(this, other);
        }
        public bool EqualsInName(IComparableByName other)
        {
            return ComparableByNameMutable.AreEqualInName(this, other);
        }

        // IComparableByFullName methods and properties

        public abstract string FullName { get; }
        public int CompareByFullNameTo(IComparableByFullName other)
        {
            return ComparableByFullNameMutable.CompareByFullName(this, other);
        }
        public bool EqualsInFullName(IComparableByFullName other)
        {
            return ComparableByFullNameMutable.AreEqualInFullName(this, other);
        }

        // ISafeMemberOfImmutableOwner methods
        public ISafeMemberOfImmutableOwner GetSafeReference()
        {
            return GetSafeReference(assert: true);
        }

        // Construction helpers

        protected virtual void ConstructAfterConstruction() { }
        
        protected virtual void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator
            )
        {
            ConstructNewMembers(name, sqlTranslator, false);
        }

        private void ConstructNewMembers(
            string name
            , ISqlTranslator sqlTranslator
            , bool forceUpdates
            )
        {
            if (forceUpdates || Name == null) SetName(name, false);
            if (forceUpdates || sqlTranslator == null) SetSqlTranslator(sqlTranslator, false);
        }

        // IDataProcObject properties and methods
        public bool IsMutableType { get { return true; } }
        public Type NearestMutableType {  get { return GetType(); } }
        public ISqlTranslator SqlTranslator
        {
            get { return _sqlTranslator; }
            set { SetSqlTranslator(value); }
        }
        public ISqlTranslator GetSqlTranslatorSafely()
        {
            return (ISqlTranslator)_sqlTranslator.GetSafeReference();
        }
        public abstract ISafeMemberOfImmutableOwner GetSafeReference(bool assert);
        public virtual void CompleteConstructionIfType(Type t, bool? retrieveIDsFromDatabase, bool assert = true)
        {
            if (t.Equals(GetType()))
            {
                _isFullyConstructed = true;
                if (assert) Debug.Assert(PassesSelfAssertion());
            }
        }

        // IDataProcObjectMutable properties and methods

        public virtual void SetName(string value, bool assert = true)
        {
            if (value == null) throw new NullNameException();
            if (value == "") throw new EmptyStringNameException();
            if (!HasNonWhitespace(value)) throw new WhitespaceOnlyNameException(value);
            _name = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void SetSqlTranslator(ISqlTranslator value, bool assert = true)
        {
            if (value == null) throw new NullSqlTranslatorException();
            _sqlTranslator = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

    }

    /// <summary>
    /// Boilerplate implementation of IDataProcObjectImmutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public abstract class DataProcObjectImmutable : IDataProcObject
    {

        protected bool _isFullyConstructed;
        protected IDataProcObject _delegee;

        protected DataProcObjectImmutable() { }

        public DataProcObjectImmutable(IDataProcObject delegee, bool assert = true)
        {
            _isFullyConstructed = false;
            RejectIfInvalidDelegeeType(_delegee);
            _delegee = delegee;
            Debug.Assert(Mark IfType(typeof(DataProcObjectImmutable), false, assert);
        }

        // Object method overrides
        public override bool Equals(object obj)
        {
            return _delegee.Equals(obj);
        }

        public override int GetHashCode()
        {
            return _delegee.GetHashCode();
        }

        // ISelfAsserter methods
        public bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(
                    _delegee != null
                    , "Delegee for DataProcObjectImmutable object "
                    + "can never be null."
                    );
                Debug.Assert(ObjectIsValidTypeForDelegee(_delegee, false));
                Debug.Assert(_delegee.PassesSelfAssertion());
            }
            return true;
        }

        public bool SkipSelfAssertionUnlessThisType(Type testableType = null)
        {
            if (testableType == null) return false;
            return (!testableType.Equals(this.GetType()));
        }

        // IComparableByName methods and properties

        public string Name { get { return _delegee.Name; } }
        public int CompareByNameTo(IComparableByName other)
        {
            return ComparableByNameMutable.CompareByName(this, other);
        }
        public bool EqualsInName(IComparableByName other)
        {
            return ComparableByNameMutable.AreEqualInName(this, other);
        }

        // IComparableByFullName methods and properties

        public string FullName { get { return _delegee.FullName; } }
        public int CompareByFullNameTo(IComparableByFullName other)
        {
            return ComparableByFullNameMutable.CompareByFullName(this, other);
        }
        public bool EqualsInFullName(IComparableByFullName other)
        {
            return ComparableByFullNameMutable.AreEqualInFullName(this, other);
        }

        // ISafeMemberOfImmutableOwner methods
        public ISafeMemberOfImmutableOwner GetSafeReference()
        {
            return GetSafeReference(assert: true);
        }

        // IMemberMatcher methods
        public virtual bool MembersMatch(Object obj)
        {
            return _delegee.MembersMatch(obj);
        }

        // IDataProcObject properties and methods
        public bool IsFullyConstructed { get { return _isFullyConstructed; } }
        public bool IsMutableType { get { return false; } }
        public Type NearestMutableType
        {
            get
            {
                string className
                    = Regex.Replace(
                        GetType().Name
                        , "([Ii]nvariant){0,1}$1"
                        , "Mutable"
                        );
                return
                    Type.GetType(
                        GetType().Namespace + "." + className
                        );
            }
        }
        public ISqlTranslator SqlTranslator { get { return _delegee.SqlTranslator; } }
        public IDataProcObject GetSafeReference(bool assert) { return this; }
        public void CompleteConstructionIfType(Type t, bool? retrieveIDsFromDatabase, bool assert = true)
        {
            if (t.Equals(GetType()))
            {
                _isFullyConstructed = true;
                if (assert) Debug.Assert(PassesSelfAssertion());
            }
        }

        // Additional validation method

        protected void RejectIfInvalidDelegeeType(IDataProcObject candidate)
        {
            ObjectIsValidTypeForDelegee(candidate,true);
        }

        protected abstract Type ValidTypeForDelegee { get; }

        protected bool ObjectIsValidTypeForDelegee(IDataProcObject candidate, bool throwException = false)
        {
            Type validType = ValidTypeForDelegee;
            bool retval = candidate != null && !validType.IsAssignableFrom(candidate.GetType());
            if (!retval && throwException)
                throw new BadDelegeeTypeException(
                    (IsFullyConstructed ? this : null)
                    , this.GetType().FullName
                    , candidate.GetType().FullName
                    , validType.FullName
                    );
            return retval;
        }

    }

    public class BadDelegeeTypeException : Exception
    {
        public IDataProcObject BadObject { get; }
        public string BadDelegeeTypeName { get; }
        public string AcceptableDelegeeTypeName { get; }
        public BadDelegeeTypeException(
            IDataProcObject badObject
            , string badObjectTypeName
            , string badDelegeeTypeName
            , string acceptableDelegeeTypeName
            , string message = null
            ) : base(
                (
                message != null
                ? message
                : "Cannot set the Delegee property of an object "
                + "of type " 
                + (badObjectTypeName != null ? badObjectTypeName : badObject.GetType().FullName )
                + " to an "
                + "object of type " + badDelegeeTypeName + "; the "
                + "parent object must be an object of type "
                + acceptableDelegeeTypeName + "."
                )
                )
        {
            BadObject = badObject;
            BadDelegeeTypeName = badDelegeeTypeName;
            AcceptableDelegeeTypeName = acceptableDelegeeTypeName;
        }
    }

    public class NullDelegeeException : Exception
    {
        public IDataProcObject BadObject { get; }
        public NullDelegeeException(
            IDataProcObject badObject
            , string badObjectTypeName
            , string message = null
            ) : base(
                (message != null
                ? message
                : "Cannot set the Delegee property of an object "
                + "of type "
                + (badObjectTypeName != null ? badObjectTypeName : badObject.GetType().FullName)
                + " to null."
                )
                )
        {
            BadObject = badObject;
        }
    }

    public class ConstructionRequiresNameException : Exception
    {
        public IDataProcObject BadObject { get; }
        public ConstructionRequiresNameException(
            IDataProcObject badObject
            , string badObjectTypeName
            , string message = null
            ) : base(
                (message != null
                ? message
                : "Cannot complete construction of objects "
                + "of type "
                + (badObjectTypeName != null ? badObjectTypeName : badObject.GetType().FullName)
                + " as long as the Name property is null."
                )
                )
        {
            BadObject = badObject;
        }
    }

    public class ConstructionRequiresSqlTranslatorException : Exception
    {
        public IDataProcObject BadObject { get; }
        public ConstructionRequiresSqlTranslatorException(
            IDataProcObject badObject
            , string badObjectTypeName
            , string message = null
            ) : base(
                (message != null
                ? message
                : "Cannot complete construction of objects "
                + "of type "
                + (badObjectTypeName != null ? badObjectTypeName : badObject.GetType().FullName)
                + " as long as the SqlTranslator property is null."
                )
                )
        {
            BadObject = badObject;
        }
    }

    public class NullStringPropertyException : Exception
    {
        public NullStringPropertyException(
            IDataProcObject badObject
            , string propertyName
            , string badObjectTypeName
            , string message = null
            ) : base(
                message != null
                ? message
                : "Cannot set "
                + propertyName != null ? propertyName : "[unknown]"
                + "property of objects of type "
                + badObjectTypeName != null
                ? badObjectTypeName
                : badObject != null ? badObject.GetType().FullName : "[unknown]"
                + " to null."
                )
        {
            BadObject = badObject;
            PropertyName = propertyName;
            BadObjectTypeName = badObjectTypeName;
        }
        public IDataProcObject BadObject { get; }
        public string PropertyName { get; }
        public string BadObjectTypeName { get; }
    }

    public class EmptyStringPropertyException : Exception
    {
        public EmptyStringPropertyException(
            IDataProcObject badObject
            , string propertyName
            , string badObjectTypeName
            , string message = null
            ) : base(
                message != null
                ? message
                : "Cannot set "
                + propertyName != null ? propertyName : "[unknown]" 
                + "property of objects of type "
                + badObjectTypeName != null 
                ? badObjectTypeName 
                : badObject != null ? badObject.GetType().FullName : "[unknown]"
                + " to the empty string."
                )
        {
            BadObject = badObject;
            PropertyName = propertyName;
            BadObjectTypeName = badObjectTypeName;
        }
        public IDataProcObject BadObject { get; }
        public string PropertyName { get; }
        public string BadObjectTypeName { get; }
    }

    public class WhitespaceOnlyStringPropertyException : Exception
    {
        public WhitespaceOnlyStringPropertyException(
            IDataProcObject badObject
            , string propertyName
            , string badObjectTypeName
            , string message = null
            ) : base(
                message != null
                ? message
                : "Cannot set "
                + propertyName != null ? propertyName : "[unknown]"
                + "property of objects of type "
                + badObjectTypeName != null
                ? badObjectTypeName
                : badObject != null ? badObject.GetType().FullName : "[unknown]"
                + " to a string that consists only of whitespace."
                )
        {
            BadObject = badObject;
            PropertyName = propertyName;
            BadObjectTypeName = badObjectTypeName;
        }
        public IDataProcObject BadObject { get; }
        public string PropertyName { get; }
        public string BadObjectTypeName { get; }
    }

}
