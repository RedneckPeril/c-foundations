﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// This interface groups together the SQL data
    /// types for which IsDecimal is true.
    /// </summary>
    public interface ISqlDecimal : ISqlDataType
    {
        int Precision { get; }
        int Scale { get; }
    }

    /// <summary>
    /// A basic, immutable implementation of the ISqlInt interface.
    /// </summary>
    public sealed class SqlDecimal : SqlDataType, ISqlDecimal
    {

        /****************
         *  Private members
         ***************/

        private string FullTypeNameForCodeErrors { get { return typeof(SqlDecimal).FullName; } }

        /****************
         *  Constructors
         ***************/

        public SqlDecimal( int precision = 38, int scale = 0 )
        {
            Precision = precision;
            Scale = scale;
        }

        /****************
         *  ISelfAsserter methods via ISelfUsabilityAsserter
         ***************/

        /// <summary>
        /// Asserts the class invariants for this class.
        /// </summary>
        /// <returns>True, unless an assertion failure exception is thrown.</returns>
        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertion(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
            }
            return true;
        }

        /****************
         *  ISqlDate method overrides
         ***************/

        public override string DefaultSql { get { return "decimal"; } }

        public override bool IsDecimalType { get { return true; } }

        /****************
         *  ISqlDecimal properties and methods
         ***************/

        public int Precision { get; }

        public int Scale { get; }

    }

}
