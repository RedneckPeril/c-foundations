﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// This interface identifies the "smallint"
    /// SQL type.
    /// </summary>
    /// <remarks>
    /// We ordinarily just retrieve the static object
    /// SqlNClob.Standard(). Objects are immutable
    /// and therefore threadsafe.
    /// </remarks>
    public interface ISqlNClob : ISqlIntType { }

    /// <summary>
    /// A basic, immutable implementation of the ISqlNClob interface.
    /// </summary>
    public sealed class SqlNClob : SqlLobType, ISqlNClob
    {

        /****************
         *  Private methods
         ***************/

        private string FullTypeNameForCodeErrors { get { return typeof(SqlNClob).FullName; } }
        private static SqlNClob _standard;

        /****************
         *  Constructor
         ***************/

        private SqlNClob() { }

        /****************
         *  SqlDataType method overrides
         ***************/

        public override string DefaultSql { get { return "nclob"; } }

        public override bool IsStringType { get { return true; } }

        /****************
         *  ISqlNClob properties and methods
         ***************/

        public static SqlNClob Standard()
        {
            if (_standard == null) _standard = new SqlNClob();
            return _standard;
        }

    }

}
