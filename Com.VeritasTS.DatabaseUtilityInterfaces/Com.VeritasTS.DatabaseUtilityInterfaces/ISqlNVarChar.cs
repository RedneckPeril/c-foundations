﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// This interface identifies the "smallint"
    /// SQL type.
    /// </summary>
    /// <remarks>
    /// We ordinarily just retrieve the static object
    /// SqlNVarChar.Standard(). Objects are immutable
    /// and therefore threadsafe.
    /// </remarks>
    public interface ISqlNVarChar : ISqlStringType { }

    /// <summary>
    /// A basic, immutable implementation of the ISqlNVarChar interface.
    /// </summary>
    public sealed class SqlNVarChar : SqlStringType, ISqlNVarChar
    {

        /****************
         *  Private members
         ***************/

        private string FullTypeNameForCodeErrors { get { return typeof(SqlNVarChar).FullName; } }
        private static SqlNVarChar _standard;

        /****************
         *  Constructor
         ***************/

        public SqlNVarChar(int maxLength = 1) : base(maxLength) { }

        /****************
         *  ISqlNVarChar properties and methods
         ***************/

        public override string DefaultSql
        {
            get { return "nvarchar(" + MaxLength + ")"; }
        }

        public static SqlNVarChar Standard()
        {
            if (_standard == null) _standard = new SqlNVarChar();
            return _standard;
        }

    }

}
