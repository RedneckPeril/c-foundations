﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface IIdentityColumn : ITableColumn, IIDColumn { }
    public interface IIdentityColumnMutable : IIdentityColumn, ITableColumnMutable, IIDColumnMutable { }

    /// <summary>
    /// Boilerplate implementation of IIdentityColumnMutable.
    /// </summary>
    /// <remarks>
    /// Sealed class for now.
    /// <remarks>
    public sealed class IdentityColumnMutable : TableColumnMutable, IIdentityColumnMutable
    {

        public IdentityColumnMutable(
            string name = "id"
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , ISingleColumnUniqueKey uniqueKey = null
            , string nameWhenPrefixed = null
            , string constraintTag = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
            : base(
                  name
                  , "int"
                  , sqlTranslator
                  , table
                  , false
                  , true
                  , isImmutable: true
                  , uniqueKey: uniqueKey
                  , nameWhenPrefixed: nameWhenPrefixed
                  , constraintTag: constraintTag
                  , id: id
                  )
        {
            CompleteConstructionIfType(typeof(IIdentityColumnMutable), retrieveIDsFromDatabase);
        }

        // DataProcObjectMutable method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(
                    IsIdentity
                    , "A IIdentityColumn must be an identity column."
                    );
                Debug.Assert(
                    !IsNullable
                    , "A IIdentityColumn cannot be nullable."
                    );
            }
            return true;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new IdentityColumnMutable(
                Name
                , GetSqlTranslatorSafely()
                , GetTableSafely()
                , GetUniqueKeySafely()
                , NameWhenPrefixed
                , ConstraintTag
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        // ObjectInSchemaMutable method overrides

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true)
        {
            RejectIfInvalidParentObjectType(parentObject);
            return new IdentityColumnMutable(
                Name
                , SqlTranslator
                , (ITable)parentObject
                , UniqueKey
                , NameWhenPrefixed
                , ConstraintTag
                , ID
                , retrieveIDsFromDatabase
                , assert
                );
        }

    }

    /// <summary>
    /// Boilerplate implementation of IIdentityColumn without variability.
    /// </summary>
    /// <remarks>
    /// Sealed class for now.
    /// <remarks>
    public sealed class IdentityColumnImmutable : IDColumnImmutable, IIdentityColumn
    {

        private IIdentityColumn Delegee { get { return (IIdentityColumn)_delegee; } }

        public IdentityColumnImmutable(
            string name = "id"
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , ISingleColumnUniqueKey uniqueKey = null
            , string nameWhenPrefixed = null
            , string constraintTag = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
            : base(delegee: null)
        {
            _delegee
                = new IdentityColumnMutable(
                    name
                    , sqlTranslator
                    , table
                    , uniqueKey
                    , nameWhenPrefixed
                    , constraintTag
                    , id
                    , retrieveIDsFromDatabase
                    , assert: false
                    );
            CompleteConstructionIfType(
                typeof(IdentityColumnImmutable)
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        public IdentityColumnImmutable(IIdentityColumn delegee, bool assert = true)
            : base(delegee)
        {
            CompleteConstructionIfType(
                typeof(IdentityColumnImmutable)
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        // DataProcObject method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(IIdentityColumn); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new IdentityColumnImmutable(
                (IIdentityColumn)
                Delegee.GetReferenceForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , assert: false
                    )
                , assert: assert
                );
        }

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new IdentityColumnImmutable(
                (IIdentityColumn)
                Delegee.GetCopyForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , assert: false
                    )
                , assert: assert
                );
        }

    }

}