﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// Base interface for objects that exist within a
    /// schema in a standard relational database.
    /// </summary>
    /// <remarks>
    /// Extends IDatabaseObject.
    /// </remarks>
    public interface IObjectInSchema : IDatabaseObject
    {
        string NameWithSchema { get; }
        IDatabaseObject ParentObject { get; }
        List<Type> ValidParentObjectTypes { get; }
        void RejectIfInvalidParentObjectType(IDatabaseObject candidate);
        bool ObjectIsValidTypeForParentObject(IDatabaseObject candidate, bool throwException = false);
        bool ParentObjectMustBeOwner { get; }
        List<Type> ValidOwnerTypesWhenNotParentObject { get; }
        IDatabaseObject GetParentObjectSafely();
        List<Type> GetValidParentObjectTypesSafely();
        List<Type> GetValidOwnerTypesWhenNotParentObjectSafely();
        IObjectInSchema GetReferenceForSchema(ISchema schema, bool? retrieveIDsFromDatabase = null, bool assert = true);
        IObjectInSchema GetCopyForSchema(ISchema schema, bool? retrieveIDsFromDatabase = null, bool assert = true);
        IObjectInSchema GetSafeReferenceForSchema(ISchema schema, bool? retrieveIDsFromDatabase = null, bool assert = true);
        IObjectInSchema GetReferenceForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = null, bool assert = true);
        IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = null, bool assert = true);
        IObjectInSchema GetSafeReferenceForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = null, bool assert = true);
    }

    /// <summary>
    /// Base interface for objects that exist within a
    /// standard relational database and can be altered
    /// after construction.
    /// </summary>
    /// <remarks>
    /// Merely adds variability to IObjectInSchema.
    /// <para>
    /// Extends IDatabaseObjectMutable.</para>
    /// </remarks>
    public interface IObjectInSchemaMutable : IObjectInSchema, IDatabaseObjectMutable
    {
        void SetParentObject(IDatabaseObject value, bool? retrieveIDsFromDatabase = null, bool assert = true);
        void PropagateSelfAsParentObjectToAllChildren();
    }

    public class SqlTranslatorParentObjectMismatchException : Exception
    {
        public SqlTranslatorParentObjectMismatchException() : base(
                message:
            "If an IObjectInSchema's parent object's SQLTranslator property is not null, "
            + "you cannot set the IObjectInSchema's own SqlTranslator property to anything else."
                )
        { }
    }

    /// <summary>
    /// Boilerplate implementation of IObjectInSchemaMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides.</item>
    ///   <list type="bullet">
    ///   <item>ValidOwnerTypesWhenNotOwner, occasionally if not abstract type
    ///   and if Owner is not required to be ParentObject.</item>
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForOwner, always.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>ValidParentObjectTypes, usually unless abstract type.</item>
    ///   <item>ParentObjectMustBeOwner, if true rather than false.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    abstract public class ObjectInSchemaMutable : DatabaseObjectMutable, IObjectInSchemaMutable
    {
        
        private static string FullTypeNameForExceptions
        {
            get { return typeof(ObjectInSchemaMutable).FullName; }
        }

        private IDatabaseObject _parentObject;

        // Constructors

        /// <summary>
        /// Default constructor, which may not fully
        /// construct the object into a useful state,
        /// depending upon how a specific descent class
        /// is designed.
        /// </summary>
        /// <remarks>
        /// The default constructor is used by frameworks and does
        /// NOT mark the object has having been fully constructed.
        /// This means that assertions do not fire until the 
        /// creator explicitly completes the construction, allowing
        /// for objects to be constructed from the class name only
        /// using the null constructor, then initialized with
        /// Set method calls, and finally marked as ready for use.
        /// </remarks>
        protected ObjectInSchemaMutable() : base() { }

        public ObjectInSchemaMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , IDatabaseObject parentObject = null
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , IDataProcObject owner = null
            , string nameWhenPrefixed = null
            , int? id = null
            ) : base(
                name
                , sqlTranslator: (parentObject != null ? (parentObject.SqlTranslator != null ? parentObject.SqlTranslator : sqlTranslator) : sqlTranslator)
                , objectType: objectType
                , physicalObjectType: physicalObjectType
                , owner: owner
                , nameWhenPrefixed: nameWhenPrefixed
                , id: id
            )
        {
            // We know, or should know, that when we come back from the base
            //   constructor SqlTranslator is not null, or else an exception has
            //   already been thrown.
            Debug.Assert(SqlTranslator != null, "SqlTranslator escaped DatabaseObjectMutable constructor as null.");
            if (parentObject != null && parentObject.SqlTranslator != null && !parentObject.SqlTranslator.Equals(SqlTranslator))
                throw new SqlTranslatorParentObjectMismatchException();
            SetParentObject(parentObject, false, false);
        }

        // Object method overrides

        public override int GetHashCode()
        {
            int p1 = NextPrimes[base.HighestPrimeUsedInHashCodeWithoutDatabase];
            return base.GetHashCode()
                + p1 * (ParentObject == null ? 0 : ParentObject.GetHashCode());
        }

        // Object method overrides: GetHashCode assistants

        public override int HighestPrimeUsedInHashCode
        {
            get { return NextPrimes[base.HighestPrimeUsedInHashCodeWithoutDatabase]; }
        }

        protected override int HighestPrimeUsedInHashCodeWithoutDatabase
        {
            get { return NextPrimes[base.HighestPrimeUsedInHashCodeWithoutDatabase]; }
        }

        // IDataProcObject method overrides

        public override string FullName
        {
            get
            {
                if (Name == null || ParentObject == null || ParentObject.FullName == null ) return null;
                else return ParentObject.FullName + "." + Name;
            }
        }

        public override bool MembersMatch(object obj)
        {
            if (obj == null) return false;
            if (!typeof(IObjectInSchema).IsAssignableFrom(obj.GetType())) return false;
            if (!base.MembersMatchWithoutDatabase(obj)) return false;
            IObjectInSchema other = (IObjectInSchema)obj;
            return EqualityUtilities.EqualOrBothNull(ParentObject, other.ParentObject);
        }

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (IsFullyConstructed)
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    ValidParentObjectTypes != null
                    , "The list of valid parent object types for IObjectInSchema objects of type "
                    + this.GetType().FullName
                    + " cannot be null."
                    );
                Debug.Assert(
                    ValidParentObjectTypes.Count > 0
                    , "The list of valid parent object types for IObjectInSchema objects of type "
                    + this.GetType().FullName
                    + " cannot be empty."
                    );
                Debug.Assert(ObjectIsValidTypeForParentObject(ParentObject, false));
                if (ParentObjectMustBeOwner)
                    Debug.Assert(
                        EqualityUtilities.EqualOrBothNull( ParentObject, Owner )
                        , "For objects of type "
                        + this.GetType().FullName
                        + ", either both the parent object and the owner "
                        + "must be null, or else they must equal each other."
                        );
                // Do not assert ParentObject, in order to avoid infinite self-assertion loop
            }
            return true;
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , parentObject: null
                , objectType: null
                , physicalObjectType: null
                , owner: null
                , nameWhenPrefixed: null
                , id: null
                );
        }

        protected virtual void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator
            , IDatabaseObject parentObject
            , IObjectType objectType
            , IObjectType physicalObjectType
            , IDataProcObject owner
            , string nameWhenPrefixed
            , int? id
            , bool forceUpdates = true
            )
        {
            if (Name == null && name == null)
                throw new ConstructionRequiresNameException(
                    this
                    , FullTypeNameForExceptions
                    );
            if (SqlTranslator == null && sqlTranslator == null)
                throw new ConstructionRequiresSqlTranslatorException(
                    this
                    , FullTypeNameForExceptions
                    );
            if (ParentObject == null && parentObject == null)
                throw new ConstructionRequiresParentObjectException(
                    this
                    , FullTypeNameForExceptions
                    );
            base.ConstructAfterConstruction(
                Name
                , SqlTranslator
                , objectType
                , physicalObjectType
                , owner
                , nameWhenPrefixed
                , id
                );
        }

        // IDatabaseObject method overrides
        public override IDatabaseSafe Database
        {
            get
            {
                if (ParentObject == null) return null;
                else return ParentObject.Database;
            }
        }

        public override ISchema Schema
        {
            get
            {
                if (ParentObject == null) return null;
                else return ParentObject.Schema;
            }
        }

        public override List<Type> ValidOwnerTypes
        {
            get
            {
                if (ParentObjectMustBeOwner) return ValidParentObjectTypes;
                else return ValidOwnerTypesWhenNotParentObject;
            }
        }

        public override IDatabaseObject GetReferenceForDatabase(IDatabaseSafe database, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            IDatabaseObject parentObject = ParentObject;
            if ((parentObject == null || parentObject.Database == null) && database == null)
                return GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, assert);
            else if (parentObject != null && parentObject.Database != null && parentObject.Schema.Equals(database))
                return GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, assert);
            else if (parentObject == null && database != null)
                throw new NullParentObjectException(
                    this
                    , "Cannot call GetReferenceForDatabase on an IObjectInSchema "
                    + "that has not yet established what its parent object is."
                    );
            else
                return GetReferenceForParentObject(
                    ((IObjectInSchema)parentObject).GetReferenceForDatabase(database, false, false)
                    , retrieveIDsFromDatabase
                    , assert
                    );
        }

        public override IDatabaseObject GetCopyForDatabase(IDatabaseSafe database, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            IDatabaseObject parentObject = ParentObject;
            if ((parentObject == null || parentObject.Database == null) && database == null)
                return GetCopyForParentObject(parentObject, retrieveIDsFromDatabase, assert);
            else if (parentObject != null && parentObject.Database != null && parentObject.Schema.Equals(database))
                return GetCopyForParentObject(parentObject, retrieveIDsFromDatabase, assert);
            else if (parentObject == null && database != null)
                throw new NullParentObjectException(
                    this
                    , "Cannot call GetReferenceForDatabase on an IObjectInSchema "
                    + "that has not yet established what its parent object is."
                    );
            else
                return GetCopyForParentObject(
                    ((IObjectInSchema)parentObject).GetReferenceForDatabase(database, false, false)
                    , retrieveIDsFromDatabase
                    , assert
                    );
        }

        // IDatabaseObjectMutable method overrides

        public override IDatabaseObject GetReferenceForOwner(IDataProcObject owner, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            IDatabaseObject retval = null;
            if (!IsMutableType) retval = GetCopyForOwner(owner, retrieveIDsFromDatabase, assert: false);
            else
            {
                SetOwner(owner, assert: false);
                retval = this;
            }
            if (assert) Debug.Assert(retval.PassesSelfAssertion());
            return retval;
        }

        public override void SetOwner(IDataProcObject value, bool assert = true)
        {
            RejectIfInvalidOwnerType(value);
            if (ParentObjectMustBeOwner)
            {
                if (!typeof(IDatabaseObject).IsAssignableFrom(value.GetType()))
                    throw new BadParentObjectTypeException(
                        this
                        , this.GetType().FullName
                        , value.GetType().FullName
                        , typeof(IDatabaseObject).FullName
                        );
                SetParentObject((IDatabaseObject)value, false);
            }
            else base.SetOwner(value, false);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        // IObjectInSchema methods and properties

        public string NameWithSchema
        {
            get
            {
                return Schema != null && Schema.Name != null
                    ? Schema.Name + "."
                    : ""
                    + Name;
            }
        }

        public IDatabaseObject ParentObject
        {
            get { return _parentObject; }
            set { SetParentObject(value); }
        }

        public abstract List<Type> ValidParentObjectTypes { get; }

        public void RejectIfInvalidParentObjectType(IDatabaseObject candidate)
        {
            ObjectIsValidTypeForParentObject(candidate, true);
        }

        public virtual bool ObjectIsValidTypeForParentObject(IDatabaseObject candidate, bool throwException = false)
        {
            bool retval = false;
            if (candidate == null)
            {
                if (throwException)
                    throw new BadParentObjectTypeException(
                        (IsFullyConstructed ? this : null)
                        , this.GetType().FullName
                        , "[null]"
                        , typeof(ITable).FullName
                        );
            }
            else
            {
                foreach (Type t in ValidParentObjectTypes)
                {
                    if (t.IsAssignableFrom(candidate.GetType()))
                    {
                        retval = true;
                        break;
                    }
                }
                if (!retval && throwException)
                    throw new BadParentObjectTypeException(
                        (IsFullyConstructed ? this : null)
                        , this.GetType().FullName
                        , candidate.GetType().FullName
                        , typeof(ITable).FullName
                        );
            }
            return retval;
        }

        public virtual bool ParentObjectMustBeOwner { get { return false; } }

        public virtual List<Type> ValidOwnerTypesWhenNotParentObject { get { return new List<Type>(); } }

        public IDatabaseObject GetParentObjectSafely()
        {
            return (IDatabaseObject)(ParentObject == null ? null : ParentObject.GetSafeReference());
        }

        public virtual List<Type> GetValidParentObjectTypesSafely()
        {
            return ValidParentObjectTypes.ToList();
        }

        public virtual List<Type> GetValidOwnerTypesWhenNotParentObjectSafely()
        {
            return ValidOwnerTypesWhenNotParentObject.ToList();
        }

        public virtual IObjectInSchema GetReferenceForSchema(ISchema schema, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            IDatabaseObject parentObject = ParentObject;
            if ((parentObject == null || parentObject.Schema == null) && schema == null)
                return GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, assert);
            else if (typeof(ISchema).IsAssignableFrom(parentObject.GetType()))
                return GetReferenceForParentObject(schema, retrieveIDsFromDatabase, assert);
            else if (parentObject != null && parentObject.Schema != null && parentObject.Schema.Equals(schema))
                return GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, assert);
            else if (parentObject != null && parentObject.Equals(schema))
                return GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, assert);
            else if (parentObject == null && schema != null)
                throw new NullParentObjectException(
                    this
                    , "Cannot call GetReferenceForSchema on an IObjectInSchema "
                    + "that has not yet established what its parent object is."
                    );
            else
                return GetReferenceForParentObject(
                    ((IObjectInSchema)parentObject).GetReferenceForSchema(schema, false, false)
                    , retrieveIDsFromDatabase
                    , assert
                    );
        }

        public virtual IObjectInSchema GetCopyForSchema(ISchema schema, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            IDatabaseObject parentObject = ParentObject;
            if ((parentObject == null || parentObject.Schema == null) && schema == null)
                return GetCopyForParentObject(parentObject, retrieveIDsFromDatabase, assert);
            else if (typeof(ISchema).IsAssignableFrom(parentObject.GetType()))
                return GetCopyForParentObject(schema, retrieveIDsFromDatabase, assert);
            else if (parentObject != null && parentObject.Schema != null && parentObject.Schema.Equals(schema))
                return GetCopyForParentObject(parentObject, retrieveIDsFromDatabase, assert);
            else if (parentObject != null && parentObject.Equals(schema))
                return GetCopyForParentObject(parentObject, retrieveIDsFromDatabase, assert);
            else if (parentObject == null && schema != null)
                throw new NullParentObjectException(
                    this
                    , "Cannot call GetCopyForSchema on an IObjectInSchema "
                    + "that has not yet established what its parent object is."
                    );
            else
                return GetCopyForParentObject(
                    ((IObjectInSchema)parentObject).GetReferenceForSchema(schema, false, false)
                    , retrieveIDsFromDatabase
                    , assert
                    );
        }

        public virtual IObjectInSchema GetSafeReferenceForSchema(ISchema schema, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            IObjectInSchemaMutable retval
                = (IObjectInSchemaMutable)GetCopyForSchema(schema, retrieveIDsFromDatabase, assert).GetSafeReference(assert: false);
            if (assert) Debug.Assert(retval.PassesSelfAssertion());
            return retval;
        }
        
        public IObjectInSchema GetReferenceForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            IObjectInSchema retval = null;
            if (!IsMutableType) retval = GetCopyForParentObject(parentObject, retrieveIDsFromDatabase, assert: false);
            else
            {
                SetParentObject(parentObject, retrieveIDsFromDatabase, assert: false);
                retval = this;
            }
            if (assert) Debug.Assert(retval.PassesSelfAssertion());
            return retval;
        }

        public abstract IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = null, bool assert = true);

        public IObjectInSchema GetSafeReferenceForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            IObjectInSchemaMutable retval
                = (IObjectInSchemaMutable)GetCopyForParentObject(parentObject, retrieveIDsFromDatabase, assert).GetSafeReference(assert: false);
            if (assert) Debug.Assert(retval.PassesSelfAssertion());
            return retval;
        }

        // IObjectInSchemaMutable methods

        public virtual void SetParentObject(IDatabaseObject value, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            RejectIfInvalidParentObjectType(value);
            bool retrieveIDs = WillNeedToRetrieveIDs(ParentObject, value, retrieveIDsFromDatabase);
            _parentObject = value;
            if (value != null && value.SqlTranslator != null & !value.SqlTranslator.Equals(SqlTranslator))
                base.SetSqlTranslator(value.SqlTranslator);
            if (ParentObjectMustBeOwner) base.SetOwner(_parentObject, false);
            PropagateSelfAsParentObjectToAllChildren();
            if (retrieveIDs) RetrieveIDsFromDatabase(assert: false);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void PropagateSelfAsParentObjectToAllChildren()
        {
            // null-op in this generation
        }

    }

    /// <summary>
    /// Boilerplate implementation of IObjectInSchema without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>ObjectInSchemaImmutable method overrides
    /// <list type="bullet">
    /// <item>GetReferenceForSchema, if an immediate descendant class.</item>
    /// <item>GetCopyForSchema, if an immediate descendant class.</item>
    /// <item>GetReferenceForParentObject.</item>
    /// <item>GetCopyForParentObject.</item>
    /// </list>
    /// </item>
    /// </list>
    /// <remarks>
    abstract public class ObjectInSchemaImmutable : DatabaseObjectImmutable, IObjectInSchema
    {

        private IObjectInSchema Delegee { get { return (IObjectInSchema)_delegee; } }

        protected ObjectInSchemaImmutable() : base() { }

        public ObjectInSchemaImmutable(
            IObjectInSchema _delegee
            , bool assert = true
            ) : base( _delegee, false)
        {
            CompleteConstructionIfType(typeof(ObjectInSchemaImmutable), false, assert);
        }

        // DatabaseObjectImmutable method overrides

        public override IDatabaseObject GetReferenceForDatabase(IDatabaseSafe database, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return GetReferenceForParentObject(
                ((IObjectInSchema)ParentObject).GetReferenceForDatabase(database, retrieveIDsFromDatabase: false, assert: false)
                , retrieveIDsFromDatabase
                , assert
                );
        }
        public override IDatabaseObject GetCopyForDatabase(IDatabaseSafe database, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return GetCopyForParentObject(
                ((IObjectInSchema)ParentObject).GetCopyForDatabase(database, retrieveIDsFromDatabase: false, assert: false)
                , retrieveIDsFromDatabase
                , assert
                );
        }

        // IObjectInSchema methods and properties


        public string NameWithSchema { get { return Delegee.NameWithSchema; } }
        public virtual IDatabaseObject ParentObject { get { return Delegee.GetParentObjectSafely(); } }
        public List<Type> ValidParentObjectTypes { get { return Delegee.GetValidParentObjectTypesSafely(); } }
        public void RejectIfInvalidParentObjectType(IDatabaseObject candidate)
        {
            Delegee.ObjectIsValidTypeForParentObject(candidate, true);
        }
        public bool ObjectIsValidTypeForParentObject(IDatabaseObject candidate, bool throwException = false)
        {
            return Delegee.ObjectIsValidTypeForParentObject(candidate, throwException = true);
        }
        public bool ParentObjectMustBeOwner { get { return Delegee.ParentObjectMustBeOwner; } }
        public List<Type> ValidOwnerTypesWhenNotParentObject { get { return Delegee.GetValidOwnerTypesWhenNotParentObjectSafely(); } }
        public IDatabaseObject GetParentObjectSafely() { return Delegee.GetParentObjectSafely(); }
        public List<Type> GetValidParentObjectTypesSafely() { return Delegee.GetValidParentObjectTypesSafely(); }
        public List<Type> GetValidOwnerTypesWhenNotParentObjectSafely() { return Delegee.GetValidOwnerTypesWhenNotParentObjectSafely(); }
        public abstract IObjectInSchema GetReferenceForSchema(ISchema schema, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true);
        public abstract IObjectInSchema GetCopyForSchema(ISchema schema, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true);
        public IObjectInSchema GetSafeReferenceForSchema(ISchema schema, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return GetCopyForSchema(schema, retrieveIDsFromDatabase, assert);
        }
        public abstract IObjectInSchema GetReferenceForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true);
        public abstract IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true);
        public IObjectInSchema GetSafeReferenceForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return GetCopyForParentObject(parentObject, retrieveIDsFromDatabase, assert);
        }

    }

    public class BadParentObjectTypeException : Exception
    {
        public IObjectInSchema BadObject { get; }
        public string BadParentObjectTypeName { get; }
        public string AcceptableParentObjectTypeName { get; }
        public BadParentObjectTypeException(
            IObjectInSchema badObject
            , string badObjectTypeName
            , string badParentObjectTypeName
            , string acceptableParentObjectTypeName
            , string message = null
            ) : base(
                (
                message != null
                ? message
                : "Cannot set the ParentObject property of an object "
                + "of type "
                + (badObjectTypeName != null ? badObjectTypeName : badObject.GetType().FullName)
                + " to an "
                + "object of type " + badParentObjectTypeName + "; the "
                + "parent object must be an object of type "
                + acceptableParentObjectTypeName + "."
                )
                )
        {
            BadObject = badObject;
            BadParentObjectTypeName = badParentObjectTypeName;
            AcceptableParentObjectTypeName = acceptableParentObjectTypeName;
        }
    }

    public class ParentObjectDatabaseOverrideException : Exception
    {
        public IObjectInSchema BadObject { get; }
        public ParentObjectDatabaseOverrideException(
            IObjectInSchema badObject
            , string badObjectTypeName
            , string message = null
            ) : base(
                (message != null
                ? message
                : "For objects of type "
                + (badObjectTypeName != null ? badObjectTypeName : badObject.GetType().FullName)
                + ", once they have a parent object, the database cannot "
                + "be changed directly, but must be changed on the parent object, "
                + "as by definition the database of an object of this type is "
                + "the database of its parent object."
                )
                )
        {
            BadObject = badObject;
        }

    }

    public class NullParentObjectException : Exception
    {
        public IObjectInSchema BadObject { get; }
        public NullParentObjectException(
            IObjectInSchema badObject
            , string badObjectTypeName
            , string message = null
            ) : base(
                (message != null
                ? message
                : "Attempted an operation on an IObjectInSchema of type "
                + (badObjectTypeName != null ? badObjectTypeName : badObject.GetType().FullName)
                + "that could not be performed because the ParentObject "
                + "is null."
                )
                )
        {
            BadObject = badObject;
        }
    }

    public class ParentObjectSchemaOverrideException : Exception
    {
        public IObjectInSchema BadObject { get; }
        public ParentObjectSchemaOverrideException(
            IObjectInSchema badObject
            , string badObjectTypeName
            , string message = null
            ) : base(
                (message != null
                ? message
                : "For objects of type "
                + (badObjectTypeName != null ? badObjectTypeName : badObject.GetType().FullName)
                + ", once they have a parent object, the schema cannot "
                + "be changed directly, but must be changed on the parent object, "
                + "as by definition the database of an object of this type is "
                + "the database of its parent object."
                )
                )
        {
            BadObject = badObject;
        }
    }

    public class ParentObjectNotOwnerException : Exception
    {
        public IObjectInSchema BadObject { get; }
        public ParentObjectNotOwnerException(
            IObjectInSchema badObject
            , string badObjectTypeName
            , string message = null
            ) : base(
                (message != null
                ? message
                : "For objects of type "
                + (badObjectTypeName != null ? badObjectTypeName : badObject.GetType().FullName)
                + ", either both the parent object and the owner "
                + "must be null, or else they must equal each other."
                )
                )
        {
            BadObject = badObject;
        }
    }

    public class ConstructionRequiresParentObjectException : Exception
    {
        public IDataProcObject BadObject { get; }
        public ConstructionRequiresParentObjectException(
            IDataProcObject badObject
            , string badObjectTypeName
            , string message = null
            ) : base(
                (message != null
                ? message
                : "Cannot complete construction of objects "
                + "of type "
                + (badObjectTypeName != null ? badObjectTypeName : badObject.GetType().FullName)
                + " as long as the ParentObject property is null."
                )
                )
        {
            BadObject = badObject;
        }
    }

}
