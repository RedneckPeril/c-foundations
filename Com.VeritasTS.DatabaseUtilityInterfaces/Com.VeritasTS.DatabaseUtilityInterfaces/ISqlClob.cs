﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// This interface identifies the "smallint"
    /// SQL type.
    /// </summary>
    /// <remarks>
    /// We ordinarily just retrieve the static object
    /// SqlClob.Standard(). Objects are immutable
    /// and therefore threadsafe.
    /// </remarks>
    public interface ISqlClob : ISqlIntType { }

    /// <summary>
    /// A basic, immutable implementation of the ISqlClob interface.
    /// </summary>
    public sealed class SqlClob : SqlLobType, ISqlClob
    {

        /****************
         *  Private methods
         ***************/

        private string FullTypeNameForCodeErrors { get { return typeof(SqlClob).FullName; } }
        private static SqlClob _standard;

        /****************
         *  Constructor
         ***************/

        private SqlClob() { }

        /****************
         *  SqlDataType method overrides
         ***************/

        public override string DefaultSql { get { return "clob"; } }

        public override bool IsStringType { get { return true; } }

        /****************
         *  ISqlClob properties and methods
         ***************/

        public static SqlClob Standard()
        {
            if (_standard == null) _standard = new SqlClob();
            return _standard;
        }

    }

}
