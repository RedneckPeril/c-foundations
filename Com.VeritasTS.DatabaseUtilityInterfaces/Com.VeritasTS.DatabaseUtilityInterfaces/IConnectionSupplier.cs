﻿

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface IConnectionSupplier: ISelfAsserter
    {
        IConnection GetAConnection();
        void ResumeControlOfConnection(IConnection conn, ITransaction trans = null);
    }

    public interface IConnectionSupplierSafe : IConnectionSupplier, ISafeMemberOfImmutableOwner { }

    // There is no default class because all connection suppliers
    //   are vendor-specific.
}
