﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface ISingleColumnConstraint : IPotentiallySingleColumnConstraint, IChildOfTable { }

    public interface ISingleColumnConstraintMutable : ISingleColumnConstraint, IPotentiallySingleColumnConstraintMutable, IChildOfTableMutable { }

    /// <summary>
    /// Boilerplate implementation of ISingleColumnConstraintMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides.</item>
    ///   <list type="bullet">
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class SingleColumnConstraintMutable : PotentiallySingleColumnConstraintMutable, ISingleColumnConstraintMutable
    {

        private string FullTypeNameForCodeErrors { get { return typeof(SingleColumnConstraintMutable).FullName; } }

        // Construction

        public SingleColumnConstraintMutable() : base() { }

        public SingleColumnConstraintMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , ITableColumn constrainedColumn = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , sqlTranslator
                , table
                , objectType
                , physicalObjectType
                , constrainedColumn
                , nameWhenPrefixed
                , id
                )
        {
            CompleteConstructionIfType(typeof(SingleColumnConstraintMutable), retrieveIDsFromDatabase, assert);
        }

        public SingleColumnConstraintMutable(
            string name
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , ITableColumn constrainedColumn = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , objectType
                , physicalObjectType
                , constrainedColumn
                , nameWhenPrefixed
                , id
                )
        {
            CompleteConstructionIfType(typeof(SingleColumnConstraintMutable), retrieveIDsFromDatabase, assert);
        }

        // DataProcObjectMutable method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    ConstrainedColumn != null
                    , "In a single-column constraint, the "
                    + "ConstrainedColumn property cannot be null."
                    );
            }
            return true;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new SingleColumnConstraintMutable(
                Name
                , GetSqlTranslatorSafely()
                , GetTableSafely()
                , GetObjectTypeSafely()
                , GetPhysicalObjectTypeSafely()
                , GetConstrainedColumnSafely()
                , NameWhenPrefixed
                , ID
                , false
                , assert
                );
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , table: null
                , objectType: null
                , physicalObjectType: null
                , constrainedColumn: null
                , nameWhenPrefixed: null
                , id: default(int?)
                );
        }

        protected override void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , ITableColumn constrainedColumn = null
            , string nameWhenPrefixed = null
            , int? id = null
            )
        {
            base.ConstructAfterConstruction(
                name
                , sqlTranslator
                , table
                , objectType
                , physicalObjectType
                , constrainedColumn
                , nameWhenPrefixed: nameWhenPrefixed
                , id: id
                );
        }

        public override void SetName(string value, bool assert = true)
        {
            if (value != null)
            {
                if (value.Length == 0)
                    throw new EmptyStringPropertyException(
                        this
                        , "Name"
                        , FullTypeNameForCodeErrors
                        );
                if (!HasNonWhitespace(value))
                    throw new WhitespaceOnlyStringPropertyException(
                        this
                        , "Name"
                        , FullTypeNameForCodeErrors
                        );
                base.SetName(value, assert);
            }
            else
            {
                Debug.Assert(
                    SqlTranslator != null
                    , "SqlTranslator escaped construction with a null value "
                    + "for object of type " + GetType().FullName + "."
                    );
                Debug.Assert(
                    ConstraintTypeTag != null
                    , "ConstraintTypeTag escaped construction with a null value "
                    + "for object of type " + GetType().FullName + "."
                    );
                Debug.Assert(
                    ConstrainedColumn != null
                    , "ConstrainedColumn escaped construction with a null value "
                    + "for object of type " + GetType().FullName + "."
                    );
                Debug.Assert(
                    ConstrainedColumn.ConstraintTag != null
                    , "ConstrainedColumn.ConstraintTag escaped construction with a null value "
                    + "for object of type " + GetType().FullName + "."
                    );
                Debug.Assert(
                    Table != null
                    , "Table escaped construction with a null value "
                    + "for object of type " + GetType().FullName + "."
                    );
                Debug.Assert(
                    Table.Name != null
                    , "Table.Name escaped construction with a null value "
                    + "for object of type " + GetType().FullName + "."
                    );
                base.SetName(
                    SqlTranslator.TableConstraintName(
                        ConstraintTypeTag + ConstrainedColumn.ConstraintTag
                        , Table.Name
                        )
                    , assert
                    );
            }
        }

        // ObjectInSchema method overrides

        public override IObjectInSchema GetCopyForParentObject(
                IDatabaseObject parentObject
                , bool? retrieveIDsFromDatabase = default(bool?)
                , bool assert = true
                )
        {
            RejectIfInvalidParentObjectType(parentObject);
            return new SingleColumnConstraintMutable(
                Name
                , SqlTranslator
                , (ITable)parentObject
                , ObjectType
                , PhysicalObjectType
                , ConstrainedColumn
                , NameWhenPrefixed
                , ID
                , false
                , assert
                );
        }

    }

    /// <summary>
    /// Boilerplate implementation of IChildOfVable without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <item>SingleColumnConstraintImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetCopyForColumn, always unless abstract class.</item>
    ///   <item>GetCopyForTable, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public class SingleColumnConstraintImmutable : PotentiallySingleColumnConstraintImmutable, ISingleColumnConstraint
    {

        private ISingleColumnConstraint Delegee { get { return (ISingleColumnConstraint)_delegee; } }

        protected SingleColumnConstraintImmutable() : base() { }

        public SingleColumnConstraintImmutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , ITableColumn constrainedColumn = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
        {
            _delegee
                = new SingleColumnConstraintMutable(
                    name
                    , sqlTranslator == null ? null : (ISqlTranslator)sqlTranslator.GetSafeReference()
                    , table == null ? null : (ITable)table.GetSafeReference()
                    , objectType == null ? null : (IObjectType)objectType.GetSafeReference()
                    , physicalObjectType == null ? null : (IObjectType)physicalObjectType.GetSafeReference()
                    , constrainedColumn == null ? null : (ITableColumn)constrainedColumn.GetSafeReference()
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    );
            CompleteConstructionIfType(typeof(SingleColumnConstraintImmutable), false, assert);
        }

        public SingleColumnConstraintImmutable(
            string name
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , ITableColumn constrainedColumn = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
        {
            _delegee
                = new SingleColumnConstraintMutable(
                    name
                    , objectType == null ? null : (IObjectType)objectType.GetSafeReference()
                    , physicalObjectType == null ? null : (IObjectType)physicalObjectType.GetSafeReference()
                    , constrainedColumn == null ? null : (ITableColumn)constrainedColumn.GetSafeReference()
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    );
            CompleteConstructionIfType(typeof(SingleColumnConstraintImmutable), false, assert);
        }

        public SingleColumnConstraintImmutable(ISingleColumnConstraint delegee, bool assert = true)
        {
            _delegee = delegee;
            CompleteConstructionIfType(typeof(SingleColumnConstraintImmutable), false, assert);
        }

        // DataProcObjectImmutable method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(ISingleColumnConstraint); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new SingleColumnConstraintImmutable(
                (ISingleColumnConstraint)
                Delegee.GetReferenceForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new SingleColumnConstraintImmutable(
                (ISingleColumnConstraint)
                Delegee.GetCopyForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }

        // PotentiallySingleColumnConstraintImmutable method overrides

        public override IPotentiallySingleColumnConstraint GetCopyForColumn(ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return new SingleColumnConstraintImmutable(
                (ISingleColumnConstraint)
                Delegee.GetCopyForColumn(
                    constrainedColumn
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }
        public override IPotentiallySingleColumnConstraint GetCopyForTable(ITable table, ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return new SingleColumnConstraintImmutable(
                (ISingleColumnConstraint)
                Delegee.GetCopyForTable(
                    table
                    , constrainedColumn
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }

    }

}
