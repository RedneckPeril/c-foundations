﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;


namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface IVable : IChildOfSchema
    {
        bool AllowsUpdates { get; }
        bool AllowsDeletes { get; }
        Dictionary<string, Tuple<int, IColumn>> Columns { get; }
        Dictionary<string, Tuple<int, IIDColumn>> IDColumns { get; }
        Dictionary<string, Tuple<int, IColumn>> UpdateableColumns { get; }
        bool HasAtLeastOneUpdateableColumn { get; }
        Dictionary<string, Tuple<int, IColumn>> ImmutableColumns { get; }
        bool HasAtLeastOneImmutableColumn { get; }
        Dictionary<string, IIndex> Indexes { get; }
        int? GetPositionOfColumn(string columnName);
        Dictionary<string, Tuple<int, IColumn>> GetColumnsSafely();
        Dictionary<string, Tuple<int, IIDColumn>> GetIDColumnsSafely();
        Dictionary<string, Tuple<int, IColumn>> GetUpdateableColumnsSafely();
        Dictionary<string, Tuple<int, IColumn>> GetImmutableColumnsSafely();
        Dictionary<string, IIndex> GetIndexesSafely();
    }

    public interface IVableMutable : IVable, IChildOfSchemaMutable
    {
        void SetAllowsUpdates(bool value, bool assert = true);
        void SetAllowsDeletes(bool value, bool assert = true);
        void SetColumns(Dictionary<string, Tuple<int, IColumn>> columns, bool? retrieveColumnIDsFromDatabase = null, bool assert = true);
        void AddOrOverwriteColumn(IColumn column, int? position = null, bool? retrieveColumnIDFromDatabase = null, bool assert = true);
        void AddOrOverwriteColumnAfter(IColumn column, string nameOfColumnToFollow, bool? retrieveColumnIDFromDatabase = null, bool assert = true);
        void AddOrOverwriteColumnBefore(IColumn column, string nameOfColumnToPrecede, bool? retrieveColumnIDFromDatabase = null, bool assert = true);
        void NormalizeColumnIndexes(bool assert = true);
        bool RemoveColumn(string nameOfDoomed, bool assert = true);
        bool RemoveColumn(IColumn doomed, bool assert = true);
        void SetIndexes(Dictionary<string, IIndex> indexes, bool? retrieveIndexIDsFromDatabase = true, bool assert = true);
        void AddOrOverwriteIndex(IIndex index, bool? retrieveIndexIDFromDatabase = true, bool assert = true);
        bool RemoveIndex(string nameOfDoomed, bool assert = true);
        bool RemoveIndex(IIndex doomed, bool assert = true);
    }

    /// <summary>
    /// Boilerplate implementation of IVableMutable.
    /// </summary>
    /// <remarks>
    /// The null constructor is used by frameworks and does
    /// NOT mark the object has having been fully constructed.
    /// This means that assertions do not fire until the 
    /// creator explicitly completes the construction, allowing
    /// for objects to be constructed from the class name
    /// using the null constructor, then initialized with
    /// Set method calls, and finally marked as ready for use.
    /// <para>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>GetCopyForOwner, always.</item>
    ///   <item>ValidOwnerTypesWhenNotOwner, occasionally if not abstract type
    ///   and if Owner is not required to be ParentObject.</item>
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>SetOwner, rarely, e.g. when Owner should always be ParentObject 
    /// and vice versa.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>ParentObjectMustBeOwner, if true rather than false.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaMutable method overrides
    ///   <list type="bullet">
    ///   <item>SetParentObject, rarely, e.g. when Owner should always be ParentObject 
    /// and vice versa.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </para>
    /// </remarks>
    public abstract class VableMutable : ChildOfSchemaMutable, IVableMutable
    {

        // Private members

        private bool _allowsUpdates;
        private bool _allowsDeletes;
        private Dictionary<string, Tuple<int, IColumn>> _columns;
        private Dictionary<string, Tuple<int, IColumn>> _updateableColumns;
        private Dictionary<string, Tuple<int, IColumn>> _invariantColumns;
        private Dictionary<string, IIndex> _indexes;

        // Constructors

        /// <summary>
        /// Default constructor, which may not fully
        /// construct the object into a useful state,
        /// depending upon how a specific descent class
        /// is designed.
        /// </summary>
        /// <remarks>
        /// The default constructor is used by frameworks and does
        /// NOT mark the object has having been fully constructed.
        /// This means that assertions do not fire until the 
        /// creator explicitly completes the construction, allowing
        /// for objects to be constructed from the class name only
        /// using the null constructor, then initialized with
        /// Set method calls, and finally marked as ready for use.
        /// </remarks>
        protected VableMutable() : base() { }

        public VableMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ISchema schema = null
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , IDataProcObject owner = null
            , bool allowsUpdates = true
            , bool allowsDeletes = true
            , Dictionary<string, Tuple<int, IColumn>> columns = null
            , Dictionary<string, IIndex> indexes = null
            , string nameWhenPrefixed = null
            , int? id = default(int?)
            ) : base(
                name
                , sqlTranslator
                , schema
                , objectType
                , physicalObjectType
                , owner
                , id: id
                )

        {
            ConstructNewMembers(
                allowsUpdates
                , allowsDeletes
                , columns
                , indexes
                );
        }

        // Object method overrides

        public override int GetHashCode()
        {
            int p1 = NextPrimes[base.HighestPrimeUsedInHashCode];
            int p2 = NextPrimes[p1];
            int p3 = NextPrimes[p2];
            int p4 = NextPrimes[p3];
            return base.GetHashCode()
                + p1 * (Columns == null ? 0 : Columns.GetHashCode())
                + p2 * (Indexes == null ? 0 : Indexes.GetHashCode());
        }

        // Object method overrides: GetHashCode assistants

        public override int HighestPrimeUsedInHashCode
        {
            get
            {
                int p1 = NextPrimes[base.HighestPrimeUsedInHashCode];
                return NextPrimes[p1];
            }
        }

        // IDataProcObject method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    Columns != null
                    , "The Columns property of an IVable can be empty, but cannot be null."
                    );
                foreach (KeyValuePair<string, Tuple<int, IColumn>> pair in Columns)
                {
                    Debug.Assert(
                        pair.Value.Item2 != null
                        , "An IVable cannot contain, in its Columns dictionary, "
                        + "a pointer to a null IColumn."
                        );
                    Debug.Assert(
                        pair.Key.Equals(pair.Value.Item2.Name)
                        , "In the Columns dictionary of an IVable, every "
                        + "key must be the name of the column to which the key "
                        + "gives access."
                        );
                    Debug.Assert(pair.Value.Item2.PassesSelfAssertion());
                }
                Debug.Assert(
                    Indexes != null
                    , "The Indexes property of an IVable can be empty, but cannot be null."
                    );
                foreach (KeyValuePair<string, IIndex> pair in Indexes)
                {
                    Debug.Assert(
                        pair.Value != null
                        , "An IVable cannot contain, in its Indexes dictionary, "
                        + "a pointer to a null IIndex."
                        );
                    Debug.Assert(
                        pair.Key.Equals(pair.Value.Name)
                        , "In the Indexes dictionary of an IVable, every "
                        + "dictionary key must be the name of the IIndex to which the "
                        + "dictionary key gives access."
                        );
                    Debug.Assert(pair.Value.PassesSelfAssertion());
                }

            }
            return true;
        }

        public override bool MembersMatch(object obj)
        {
            if (!base.MembersMatch(obj)) return false;
            if (!typeof(IVable).IsAssignableFrom(obj.GetType())) return false;
            IVable other = (IVable)obj;
            if (
                this.AllowsUpdates != other.AllowsUpdates
                || this.AllowsDeletes != other.AllowsDeletes
                || !EqualityUtilities.EqualOrBothNull(this.Columns, other.Columns)
                || !EqualityUtilities.EqualOrBothNull(this.Indexes, other.Indexes)
                ) return false;
            return true;
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , schema: null
                , objectType: null
                , physicalObjectType: null
                , owner: null
                , allowsUpdates: true
                , allowsDeletes: true
                , columns: null
                , indexes: null
                , nameWhenPrefixed: null
                , id: default(int?)
                );
        }

        protected virtual void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator
            , ISchema schema
            , IObjectType objectType
            , IObjectType physicalObjectType
            , IDataProcObject owner
            , bool allowsUpdates
            , bool allowsDeletes
            , Dictionary<string, Tuple<int, IColumn>> columns
            , Dictionary<string, IIndex> indexes
            , string nameWhenPrefixed
            , int? id = default(int?)
            )
        {
            base.ConstructAfterConstruction(
                name
                , sqlTranslator
                , schema
                , objectType
                , physicalObjectType
                , owner
                , nameWhenPrefixed
                , id
                );
            ConstructNewMembers(
                allowsUpdates
                , allowsDeletes
                , columns
                , indexes
                , false
                );
        }

        private void ConstructNewMembers(
            bool allowsUpdates = true
            , bool allowsDeletes = true
            , Dictionary<string, Tuple<int, IColumn>> columns = null
            , Dictionary<string, IIndex> indexes = null
            , bool forceUpdates = true
            )
        {
            if (SqlTranslator == null)
                throw new ConstructionRequiresSqlTranslatorException(
                    this
                    , typeof(VableMutable).FullName
                    );
            // If something is no longer at the default, don't change it
            //   unless the forceUpdates flag is switched on
            if (forceUpdates || AllowsUpdates) SetAllowsUpdates(allowsUpdates);
            if (forceUpdates || AllowsDeletes) SetAllowsDeletes(allowsDeletes);
            if (forceUpdates || Columns == null) SetColumns(columns);
            if (forceUpdates || Indexes == null) SetIndexes(indexes);
        }

        // DatabaseObjectMutable method overrides

        public override void RetrieveIDsFromDatabase(bool assert = true)
        {
            if (Database != null)
                if (Database.IsConnected)
                {
                    foreach (KeyValuePair<string, Tuple<int, IColumn>> pair in Columns)
                    {
                        if (typeof(IColumnMutable).IsAssignableFrom(pair.Value.Item2.GetType()))
                            ((IColumnMutable)pair.Value.Item2).RetrieveIDsFromDatabase(false);
                        else
                            AddOrOverwriteColumn(
                                (IColumn)pair.Value.Item2.GetReferenceForParentObject(this, true, false)
                                );
                    }
                    foreach (KeyValuePair<string, IIndex> pair in Indexes)
                    {
                        if (typeof(IIndexMutable).IsAssignableFrom(pair.Value.GetType()))
                            ((IIndexMutable)pair.Value).RetrieveIDsFromDatabase(false);
                        else
                            AddOrOverwriteIndex(
                                (IIndex)pair.Value.GetReferenceForParentObject(this, true, false)
                                );
                    }
                }
            base.RetrieveIDsFromDatabase(assert);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        // ObjectInSchemaMutable method overrides

        public override void PropagateSelfAsParentObjectToAllChildren()
        {
            foreach (string key in _columns.Keys)
                AddOrOverwriteColumn(
                    (IColumn)_columns[key].Item2.GetReferenceForParentObject(
                        this
                        , false
                        , false
                        )
                    );
            foreach (string key in _indexes.Keys)
                _indexes[key]
                    = (IIndex)_indexes[key].GetReferenceForParentObject(
                        this
                        , false
                        , false
                        );
        }

        // IVable properties and methods

        public bool AllowsUpdates
        {
            get { return _allowsUpdates; }
            set { SetAllowsUpdates(value); }
        }

        public bool AllowsDeletes
        {
            get { return _allowsDeletes; }
            set { SetAllowsUpdates(value); }
        }

        public Dictionary<string, Tuple<int, IColumn>> Columns
        {
            get { return _columns; }
            set { SetColumns(value); }
        }

        public Dictionary<string, Tuple<int, IIDColumn>> IDColumns
        {
            get
            {
                Dictionary<string, Tuple<int, IIDColumn>> retval = new Dictionary<string, Tuple<int, IIDColumn>>();
                foreach (
                    KeyValuePair<string, Tuple<int, IColumn>> pair
                    in Columns.Where(p => typeof(IIDColumn).IsAssignableFrom(p.Value.Item2.GetType()))
                    )
                    retval[pair.Key] = new Tuple<int, IIDColumn>(pair.Value.Item1, (IIDColumn)pair.Value.Item2);
                return retval;
            }
        }

        public Dictionary<string, Tuple<int, IColumn>> UpdateableColumns
        {
            get { return _updateableColumns; }
        }

        public bool HasAtLeastOneUpdateableColumn
        {
            get
            {
                return UpdateableColumns.Take(1).Count() > 0;
            }
        }

        public Dictionary<string, Tuple<int, IColumn>> ImmutableColumns
        {
            get { return _invariantColumns; }
        }

        public bool HasAtLeastOneImmutableColumn
        {
            get
            {
                return ImmutableColumns.Take(1).Count() > 0;
            }
        }

        public Dictionary<string,IIndex> Indexes
        {
            get { return _indexes; }
            set { SetIndexes(value); }
        }
        public int? GetPositionOfColumn(string columnName)
        {
            if (Columns[columnName] == null) return null;
            else return Columns[columnName].Item1;
        }
        public Dictionary<string, Tuple<int, IColumn>> GetColumnsSafely()
        {
            Dictionary<string,Tuple<int, IColumn>> retval 
                = new Dictionary<string, Tuple<int, IColumn>>();
            foreach (KeyValuePair<string, Tuple<int, IColumn>> pair in _columns)
            {
                retval[pair.Key]
                    = new Tuple<int, IColumn>(
                        pair.Value.Item1
                        , (IColumn)pair.Value.Item2.GetSafeReference()
                        );
            }
            return retval;
        }

        public Dictionary<string, Tuple<int, IIDColumn>> GetIDColumnsSafely()
        {
            Dictionary<string, Tuple<int, IIDColumn>> retval = new Dictionary<string, Tuple<int, IIDColumn>>();
            foreach (
                KeyValuePair<string, Tuple<int, IColumn>> pair
                in Columns.Where(p => typeof(IIDColumn).IsAssignableFrom(p.Value.Item2.GetType()))
                )
                retval[pair.Key]
                    = new Tuple<int, IIDColumn>(
                        pair.Value.Item1
                        , (IIDColumn)pair.Value.Item2.GetSafeReference()
                        );
            return retval;
        }

        public Dictionary<string, Tuple<int, IColumn>> GetUpdateableColumnsSafely()
        {
            Dictionary<string, Tuple<int, IColumn>> retval
                = new Dictionary<string, Tuple<int, IColumn>>();
            foreach (KeyValuePair<string, Tuple<int, IColumn>> pair in _updateableColumns)
            {
                retval[pair.Key]
                    = new Tuple<int, IColumn>(
                        pair.Value.Item1
                        , (IColumn)pair.Value.Item2.GetSafeReference()
                        );
            }
            return retval;
        }
        
        public Dictionary<string, Tuple<int, IColumn>> GetImmutableColumnsSafely()
        {
            Dictionary<string, Tuple<int, IColumn>> retval
                = new Dictionary<string, Tuple<int, IColumn>>();
            foreach (KeyValuePair<string, Tuple<int, IColumn>> pair in _invariantColumns)
            {
                retval[pair.Key]
                    = new Tuple<int, IColumn>(
                        pair.Value.Item1
                        , (IColumn)pair.Value.Item2.GetSafeReference()
                        );
            }
            return retval;
        }

        public Dictionary<string, IIndex> GetIndexesSafely()
        {
            Dictionary<string, IIndex> retval = new Dictionary<string, IIndex>();
            foreach (KeyValuePair<string, IIndex> pair in _indexes)
                retval[pair.Key] = (IIndex)pair.Value.GetSafeReference();
            return retval;
        }

        // IVableMutable properties and methods
        
        public virtual void SetAllowsUpdates(bool value, bool assert = true )
        {
            _allowsUpdates = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void SetAllowsDeletes(bool value, bool assert = true )
        {
            _allowsDeletes = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void SetColumns(Dictionary<string, Tuple<int, IColumn>> value, bool? retrieveColumnIDsFromDatabase = null, bool assert = true )
        {
            if (_columns != value)
            {
                _columns = new Dictionary<string, Tuple<int, IColumn>>();
                foreach (KeyValuePair<string, Tuple<int, IColumn>> pair in value.Where(p => p.Value != null).Where(p=>p.Value.Item2 != null))
                    AddOrOverwriteColumn(pair.Value.Item2, null, retrieveColumnIDsFromDatabase, assert: false);
            }
            else
            {
                foreach (KeyValuePair<string, Tuple<int, IColumn>> pair in _columns.Where(p => p.Value.Item2.ParentObject == null))
                    AddOrOverwriteColumn(pair.Value.Item2, null, retrieveColumnIDsFromDatabase, assert: false);
            } 
            if (assert) Debug.Assert(PassesSelfAssertion());
        }
        
        public virtual void AddOrOverwriteColumn(IColumn column, int? position = null, bool? retrieveColumnIDFromDatabase = null, bool assert = true )
        {
            if (column != null)
            {
                Debug.Assert(
                    column.Name != null
                    , "Cannot add columns with null names to vables."
                    );
                string columnName = column.Name;
                if (!Equals(column.ParentObject))
                    column = 
                        (IColumn)
                        column.GetReferenceForParentObject(this, retrieveColumnIDFromDatabase, false);
                bool containsKey = _columns.ContainsKey(columnName);
                bool alreadyInPosition
                    = (
                    containsKey
                    ? position == null ||  _columns[columnName].Item1 == position.Value
                    : false
                    );
                bool appendToEnd = (position == null  && !alreadyInPosition);
                if (appendToEnd)
                {
                    position
                        = (
                        _columns.Count > 0
                        ? _columns.Values.OrderByDescending(t => t.Item1).First().Item1 + 1
                        : 1
                        );
                    _columns[columnName] = new Tuple<int, IColumn>(position.Value, column);
                }
                else if (alreadyInPosition)
                {
                    position = _columns[columnName].Item1;
                    _columns[columnName] = new Tuple<int, IColumn>(position.Value, column);
                }
                else
                {
                    if (containsKey) _columns.Remove(columnName);
                    foreach (
                        KeyValuePair<string, Tuple<int, IColumn>> pair
                        in _columns.Where(kvp => kvp.Value.Item1 >= position).OrderByDescending(kvp => kvp.Value.Item1)
                        )
                        _columns[pair.Key]
                            = new Tuple<int, IColumn>(
                                pair.Value.Item1 + 1
                                , pair.Value.Item2
                                );
                    _columns[columnName]
                        = new Tuple<int, IColumn>(position.Value, column);

                }
                if (column.IsImmutable)
                {
                    _invariantColumns[columnName] = _columns[columnName];
                    _updateableColumns.Remove(columnName);
                }
                else
                {
                    _invariantColumns.Remove(columnName);
                    _updateableColumns[columnName] = _columns[columnName];
                }
                if (column.ParentObject == null)
                    _columns[columnName]
                        = new Tuple<int, IColumn>(
                            _columns[columnName].Item1
                            , (IColumn)
                            _columns[columnName].Item2.GetReferenceForParentObject(this, retrieveColumnIDFromDatabase, assert: false)
                            );
            }
            NormalizeColumnIndexes(false);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }
        
        public virtual void AddOrOverwriteColumnAfter(
            IColumn column
            , string nameOfColumnToFollow
            , bool? retrieveColumnIDFromDatabase = true
            , bool assert = true
            )
        {
            if (column != null)
            {
                if (!_columns.ContainsKey(nameOfColumnToFollow))
                    throw new NonexistentColumnException(
                        nameOfColumnToFollow
                        , this
                        );
                int position = _columns[nameOfColumnToFollow].Item1 + 1;
                foreach (
                    KeyValuePair<string, Tuple<int, IColumn>> pair
                    in _columns.Where(p => p.Value.Item1 >= position).OrderBy(p => p.Value.Item1).Take(1)
                    )
                {
                    if (pair.Value.Item1 > position && pair.Key.Equals(column.Name))
                        position = pair.Value.Item1;
                }
                AddOrOverwriteColumn(column, position, retrieveColumnIDFromDatabase, assert);
            }
        }

        public virtual void AddOrOverwriteColumnBefore(
            IColumn column
            , string nameOfColumnToPrecede
            , bool? retrieveColumnIDFromDatabase = true
            , bool assert = true
            )
        {
            if (column != null)
            {
                if (!_columns.ContainsKey(nameOfColumnToPrecede))
                    throw new NonexistentColumnException(
                        nameOfColumnToPrecede
                        , this
                        );
                int position = _columns[nameOfColumnToPrecede].Item1;
                foreach (
                    KeyValuePair<string, Tuple<int, IColumn>> pair
                    in _columns.Where(p => p.Value.Item1 < position).OrderByDescending(p => p.Value.Item1).Take(1)
                    )
                {
                    if (pair.Key.Equals(column.Name))
                        position = pair.Value.Item1;
                    else if (pair.Value.Item1 < position - 1)
                        position = position - 1;
                }
                AddOrOverwriteColumn(column, position, retrieveColumnIDFromDatabase, assert);
            }
        }

        public void NormalizeColumnIndexes(bool assert = true)
        {
            int i = 1;
            foreach( 
                KeyValuePair<string,Tuple<int,IColumn>> pair
                in Columns.OrderBy(p=>p.Value.Item1)
                )
            {
                Columns[pair.Key] = new Tuple<int, IColumn>(i, Columns[pair.Key].Item2);
                i++;
            }
        }

        public virtual bool RemoveColumn(string nameOfDoomed, bool assert = true )
        {
            bool retval = false;
            if (nameOfDoomed != null)
            {
                _invariantColumns.Remove(nameOfDoomed);
                _updateableColumns.Remove(nameOfDoomed);
                retval = _columns.Remove(nameOfDoomed);
            }
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }
        public bool RemoveColumn(IColumn doomed, bool assert = true )
        {
            if (UtilitiesForINamed.NameOf(doomed) == null) return false;
            else
                return RemoveColumn(doomed.Name, assert: true);
        }

        public virtual void SetIndexes(Dictionary<string, IIndex> value, bool? retrieveIndexIDsFromDatabase = true, bool assert = true )
        {
            if (_indexes != value)
            {
                _indexes = new Dictionary<string, IIndex>();
                foreach (KeyValuePair<string, IIndex> pair in value.Where(p => p.Value != null))
                    AddOrOverwriteIndex(pair.Value, retrieveIndexIDsFromDatabase, assert: false);
            }
            else
            {
                foreach (KeyValuePair<string, IIndex> pair in _indexes.Where(p => p.Value.ParentObject == null))
                    AddOrOverwriteIndex(pair.Value, retrieveIndexIDsFromDatabase, assert: false);
            }
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void AddOrOverwriteIndex(IIndex index, bool? retrieveIndexIDFromDatabase = true, bool assert = true )
        {
            Debug.Assert(
                index.Name != null
                , "Cannot add indexes with null names to vables."
                );
            string indexName = index.Name;
            _indexes[indexName]
                = !Equals(index.ParentObject)
                ? (IIndex)index.GetReferenceForParentObject(this, retrieveIndexIDFromDatabase, false)
                : index;
        }

        public virtual bool RemoveIndex(string nameOfDoomed, bool assert = true )
        {
            bool retval = false;
            if (nameOfDoomed != null)
                retval = _indexes.Remove(nameOfDoomed);
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }
        public bool RemoveIndex(IIndex doomed, bool assert = true)
        {
            if (UtilitiesForINamed.NameOf(doomed) == null) return false;
            else return RemoveIndex(doomed.Name, assert: true);
        }

    }

    /// <summary>
    /// Boilerplate implementation of IVable without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetCopyForOwner, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public abstract class VableImmutable : ChildOfSchemaImmutable, IVable
    {
        
        private IVable Delegee { get { return (IVable)_delegee; } }

        protected VableImmutable() { }

        // IVable properties and methods

        public bool AllowsUpdates { get { return Delegee.AllowsUpdates; } }
        public bool AllowsDeletes { get { return Delegee.AllowsDeletes; } }
        public Dictionary<string, Tuple<int, IColumn>> Columns { get { return Delegee.GetColumnsSafely(); } }
        public Dictionary<string, Tuple<int, IIDColumn>> IDColumns { get { return Delegee.GetIDColumnsSafely(); } }
        public Dictionary<string, Tuple<int, IColumn>> UpdateableColumns { get { return Delegee.GetUpdateableColumnsSafely(); } }
        public bool HasAtLeastOneUpdateableColumn { get { return Delegee.HasAtLeastOneUpdateableColumn; } }
        public Dictionary<string, Tuple<int, IColumn>> ImmutableColumns { get { return Delegee.GetImmutableColumnsSafely(); } }
        public bool HasAtLeastOneImmutableColumn { get { return Delegee.HasAtLeastOneImmutableColumn; } }
        public Dictionary<string, IIndex> Indexes { get { return Delegee.GetIndexesSafely(); } }
        public int? GetPositionOfColumn(string columnName)
        {
            return Delegee.GetPositionOfColumn(columnName);
        }
        public Dictionary<string, Tuple<int, IColumn>> GetColumnsSafely()
        {
            return Delegee.GetColumnsSafely();
        }
        public Dictionary<string, Tuple<int, IIDColumn>> GetIDColumnsSafely()
        {
            return Delegee.GetIDColumnsSafely();
        }
        public Dictionary<string, Tuple<int, IColumn>> GetUpdateableColumnsSafely()
        {
            return Delegee.GetUpdateableColumnsSafely();
        }
        public Dictionary<string, Tuple<int, IColumn>> GetImmutableColumnsSafely()
        {
            return Delegee.GetImmutableColumnsSafely();
        }
        public Dictionary<string, IIndex> GetIndexesSafely()
        {
            return Delegee.GetIndexesSafely();
        }

    }

    public class NameAlreadyInUseException : Exception
    {
        private NameAlreadyInUseException() { }
        public NameAlreadyInUseException(string name) { Name = name; }
        public string Name { get; }
    }

    public class NonexistentColumnException : Exception
    {
        private NonexistentColumnException() { }
        public NonexistentColumnException(string missingColumnName, IVable vable)
        {
            MissingColumnName = missingColumnName;
            Vable = vable;
        }
        public string MissingColumnName { get; }
        public IVable Vable { get; }
    }

}
