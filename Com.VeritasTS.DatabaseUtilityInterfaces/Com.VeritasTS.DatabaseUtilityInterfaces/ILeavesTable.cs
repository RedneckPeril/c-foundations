﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{
    public interface ILeavesTable : ITable { }
    public interface ILeavesTableMutable : ILeavesTable, ITableMutable { }

    /// <summary>
    /// Boilerplate implementation of ILeavesTableMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidOwnerTypesWhenNotOwner, occasionally if not abstract type
    ///   and if Owner is not required to be ParentObject.</item>
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>ParentObjectMustBeOwner, if true rather than false.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class LeavesTableMutable: TableMutable, ILeavesTableMutable
    {

        public static string DefaultNameColumnName { get { return "leafNameWithSchema"; } }
        public static string DefaultNameColumnConstraintTag { get { return "LNWS"; } }

        public LeavesTableMutable() : base() { }

        public LeavesTableMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ISchema schema = null
            , ITable clientTable = null
            , bool allowsUpdates = true
            , bool allowsDeletes = true
            , bool maintainChildVablesTable = false
            , string childVablesTableName = null
            , string nameWhenPrefixed = null
            , int? id = default(int?)
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name == null ? "" : name
                , sqlTranslator
                , schema:
                schema != null
                ? schema
                : ( clientTable != null ? clientTable.Schema : null )
                , objectType: ObjectTypes.Table()
                , allowsUpdates: allowsUpdates
                , allowsDeletes: allowsDeletes
                , hasIDColumn: true
                , maintainChildVablesTable: false
                , childVablesTableName: null
                , nameWhenPrefixed: nameWhenPrefixed
                , id: id
                , retrieveIDsFromDatabase: false
                , assert: false
                )
        {
            if (Name.Trim().Length == 0)
                SetName(clientTable.Name + "Leaves", false);
            ConstructNewMembers(
                maintainChildVablesTable
                , childVablesTableName
                , false
                );
            CompleteConstructionIfType(typeof(LeavesTableMutable), retrieveIDsFromDatabase, assert);
        }

        // IDataProcObjectMutable method overrides

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                clientTable: null
                , allowsUpdates: true
                , allowsDeletes: true
                , maintainChildVablesTable: false
                , childVablesTableName: null
                , nameWhenPrefixed: null
                , id: default(int?)
                );
        }

        protected virtual void ConstructAfterConstruction(
            ITable clientTable
            , bool allowsUpdates
            , bool allowsDeletes
            , bool maintainChildVablesTable
            , string childVablesTableName
            , string nameWhenPrefixed
            , int? id = default(int?)
            )
        {
            if (Name.Trim().Length == 0)
                SetName(clientTable.Name + "Leaves", false);
            base.ConstructAfterConstruction(
                objectType: ObjectTypes.Table()
                , owner: clientTable
                , allowsUpdates: allowsUpdates
                , allowsDeletes: allowsDeletes
                , columns: null
                , indexes: null
                , primaryKey: null
                , primaryKeySequence: null
                , uniqueKeys: null
                , foreignKeys: null
                , triggers: null
                , checkConstraints: null
                , defaultConstraints: null
                , isAudited: false
                , auditTableName: null
                , publishesChanges: false
                , changeTrackingTableName: null
                , parentTable: null
                , parentPassThrough: null
                , canHaveChildren: false
                , leavesTableName: null
                , maintainChildVablesTable: false
                , childVablesTableName: null
                , hasIDColumn: true
                , idColumnName: null
                , hasInputterColumn: false
                , inputtersColumnName: null
                , validInputtersTableName: null
                , hasLastUpdateDtmColumn: false
                , hasLastUpdaterColumn: false
                , nameWhenPrefixed: null
                , id: default(int?)
                );
            ConstructNewMembers(
                maintainChildVablesTable
                , childVablesTableName
                , false
                );
        }

        private void ConstructNewMembers(
            bool maintainChildVablesTable
            , string childVablesTableName
            , bool forceUpdates = true
            )
        {
            ISqlTranslator sql = SqlTranslator;
            if (sql == null)
                throw new ConstructionRequiresSqlTranslatorException(
                    this
                    , typeof(TableMutable).FullName
                    );
            if (!HasIDColumn)
                SetHasIDColumn(
                    true
                    , retrieveIDColumnIDFromDatabase: false
                    , assert: false
                    );
            string nameColumnCheckConstraintName
                = sql.TableConstraintName("_ck" + DefaultNameColumnConstraintTag, Name);
            AddOrOverwriteColumn(
                new ColumnMutable(
                    DefaultNameColumnName
                    , "nvarchar(255)"
                    , sql
                    , this
                    , isNullable: false
                    , isToBeTrimmed: true
                    , isImmutable: true
                    , constraintTag: DefaultNameColumnConstraintTag
                    , retrieveIDsFromDatabase: false
                    , assert: false
                    , checkConstraints: new Dictionary<string, ICheckConstraint>
                    {
                        [nameColumnCheckConstraintName]
                        = new CheckConstraintMutable(
                            nameColumnCheckConstraintName
                            , SqlTranslator
                            , this
                            , sql.TrimmingConstraintText(DefaultNameColumnName)
                            , retrieveIDsFromDatabase: false
                            , assert: false
                            )
                    }
                    )
                );
            SetHasLastUpdateDtmColumn(true, false, false);
            SetHasLastUpdaterColumn(true, false, false);
            SetPrimaryKey(
                new PrimaryKeyMutable(
                    sql.TableConstraintName("_pk", Name)
                    , sql
                    , this
                    , new List<string> { IDColumnName }
                    , retrieveIDsFromDatabase: false
                    , assert: false
                    )
                );
            AddOrOverwriteUniqueKey(
                new UniqueKeyMutable(
                    sql.TableConstraintName("_lpk", Name)
                    , sql
                    , this
                    , new List<string> { DefaultNameColumnName }
                    , isClustered: false
                    , retrieveIDsFromDatabase: false
                    , assert: false
                    )
                );
            SetMaintainChildVablesTable(
                maintainChildVablesTable
                , childVablesTableName
                , false
                , false
                );
        }

        // ILeavesTableMutable properties and methods

        public override void SetMaintainChildVablesTable(
            bool value
            , string childVablesTableName = null
            , bool? retrieveChildVablesTableIDFromDatabase = null
            , bool assert = true
            )
        {
            bool createForeignKey
                = value
                && (
                ChildVablesTable == null
                || (childVablesTableName != null && !childVablesTableName.Equals(ChildVablesTable.Name))
                );
            base.SetMaintainChildVablesTable(
                value
                , childVablesTableName
                , retrieveChildVablesTableIDFromDatabase
                , false
                );
            if (createForeignKey)
                AddOrOverwriteForeignKey(
                    new ForeignKeyMutable(
                        Name + "_fkLNWS"
                        , SqlTranslator
                        , this
                        , new Dictionary<string, string>
                        {
                            [DefaultNameColumnName]
                            = ChildVablesTableMutable.DefaultNameColumnName
                        }
                        , ChildVablesTable
                        , cascadeOnDelete: false
                        , autoCascadeOnDeleteIfPossible: false
                        , cascadeOnUpdate: false
                        , autoCascadeOnUpdateIfPossible: false
                        , constrainedColumn: Columns[DefaultNameColumnName].Item2
                        )
                    );
            else if (!value)
                foreach (
                    KeyValuePair<string, IForeignKey> pair
                    in ForeignKeys
                    .Where(p => p.Value.ConstrainedColumn != null && p.Value.ConstrainedColumn.Name == "leafNameWithSchema")
                    )
                    RemoveForeignKey(pair.Value);
        }

    }

    /// <summary>
    /// Boilerplate implementation of ILeavesTable without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public class LeavesTableImmutable : TableImmutable, ILeavesTable
    {
        private ILeavesTable Delegee { get { return (ILeavesTable)_delegee; } }

        //Disable default constructor
        private LeavesTableImmutable() : base(delegee: null) { }

        public LeavesTableImmutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ISchema schema = null
            , ITable clientTable = null
            , bool allowsUpdates = true
            , bool allowsDeletes = true
            , bool maintainChildVablesTable = false
            , string childVablesTableName = null
            , string nameWhenPrefixed = null
            , int? id = default(int?)
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(delegee: null)
        {
            _delegee =
                new LeavesTableMutable(
                    name
                    , sqlTranslator
                    , schema
                    , clientTable
                    , allowsUpdates
                    , allowsDeletes
                    , maintainChildVablesTable
                    , childVablesTableName
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    );
            CompleteConstructionIfType(typeof(LeavesTableImmutable), false, true);
        }

        public LeavesTableImmutable(ILeavesTable delegee) : base(delegee: null)
        {
            _delegee = delegee;
            CompleteConstructionIfType(typeof(LeavesTableImmutable), false, true);
        }

        // DataProcObject method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(ILeavesTable); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            IObjectInSchema retval
                = new LeavesTableImmutable(
                (ILeavesTable)
                    ((IObjectInSchema)_delegee).GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, assert)
                    );
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }
        public override IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            IObjectInSchema retval
                = new LeavesTableImmutable(
                (ILeavesTable)
                    ((IObjectInSchema)_delegee).GetCopyForParentObject(parentObject, retrieveIDsFromDatabase, assert)
                    );
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }

    }

}