﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface ITransaction
    {
        object NativeTransaction { get; }
        IConnection Connection { get; }
        void Rollback();
        void Commit();
    }

}
