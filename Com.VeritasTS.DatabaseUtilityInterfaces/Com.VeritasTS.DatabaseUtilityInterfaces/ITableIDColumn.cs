﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface ITableIDColumn : ITableColumn, IIDColumn
    {
        // For true sequences this is a null-op
        //   because no rows ever exist with null
        //   IDs.
        void PopulateValuesInNullIDRows(
            string tempTableName
            , string tempIDBaseColumnName
            , IConnection conn
            );
    }
    public interface ITableIDColumnMutable : ITableIDColumn, ITableColumnMutable, IIDColumnMutable { }

    /// <summary>
    /// Boilerplate implementation of ITableIDColumnMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode(), if additional members are added.</item>
    ///   <item>HighestPrimeUsedInHashCode, if GetHashCode() is overridden.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>SetOwner, if (as is occasionally the case) Owner
    /// is constrained to be an instance of a particular object type.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>ObjectIsValidTypeForParentObject, rarely, and generally only for
    ///   highly special-purpose column classes dedicated to a particular type
    ///   of special-purpose table class.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class TableIDColumnMutable : TableColumnMutable, ITableIDColumnMutable
    {

        private string FullTypeNameForCodeErrors { get { return typeof(TableIDColumnMutable).FullName; } }

        // Constructors

        public TableIDColumnMutable() : base() { }

        public TableIDColumnMutable(
            string name = "id"
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , bool isIdentity = true
            , bool isNullable = false
            , string nameWhenPrefixed = null
            , string constraintTag = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
            : base(
                  name == null ? "id" : name
                  , "int"
                  , table: table
                  , isIdentity: isIdentity
                  , isNullable: isNullable
                  , isImmutable: true
                  , nameWhenPrefixed: nameWhenPrefixed
                  , constraintTag: constraintTag
                  , id: id
                  )
        {
            ConstructNewMembers(true);
            CompleteConstructionIfType(typeof(TableIDColumnMutable), retrieveIDsFromDatabase);
        }

        // DataProcObjectMutable method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    "int".Equals(DataType)
                    , "A TableIDColumn must be typed as int, not "
                    + (DataType == null ? "[null]" : DataType)
                    + "."
                    );
                Debug.Assert(
                    (!IsNullable || IsConstrainedNonnull)
                    , "An TableIDColumn must either be non-nullable "
                    + "or else be constrained is non-null."
                    );
                Debug.Assert(
                    !(IsNullable && IsIdentity)
                    , "An TableIDColumn must either be a nullable column "
                    + "that is not an identity column, or else an "
                    + "identity column that is not nullable."
                    );
                Debug.Assert(
                    CalcFormula == null
                    , "A TableIDColumn cannot be a computed column "
                    + "and therefore cannot have a computed definition."
                    );
                Debug.Assert(
                    DefaultConstraint == null
                    , "A TableIDColumn cannot have a default constraint."
                    );
            }
            return true;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new TableIDColumnMutable(
                Name
                , GetSqlTranslatorSafely()
                , GetTableSafely()
                , IsNullable
                , IsIdentity
                , NameWhenPrefixed
                , ConstraintTag
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: "id"
                , sqlTranslator: null
                , table: null
                , isIdentity: true
                , isNullable: false
                , nameWhenPrefixed: null
                , constraintTag: null
                , id: null
                );
        }

        protected virtual void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator
            , ITable table
            , bool isIdentity
            , bool isNullable
            , string nameWhenPrefixed
            , string constraintTag
            , int? id
            )
        {
            if (Name != null) name = Name;
            if (name == null) name = "id";
            base.ConstructAfterConstruction(
                name
                , "int"
                , sqlTranslator
                , table
                , isNullable
                , isIdentity
                , isToBeTrimmed: false
                , isImmutable: true
                , calcFormula: null
                , isPersisted: false
                , defaultConstraint: null
                , checkConstraints: null
                , foreignKey: null
                , uniqueKey: null
                , nameWhenPrefixed: null
                , constraintTag: null
                , id: null
                );
            ConstructNewMembers(false);
        }

        private void ConstructNewMembers(bool forceUpdates = true)
        {
            if (IsNullable & !IsConstrainedNonnull)
                AddOrOverwriteCheckConstraint(
                    new NonnullabilityConstraintMutable(
                        this
                        , retrieveIDsFromDatabase: false
                        , assert: false
                        )
                    , false
                    , false
                    );
        }

        // ObjectInSchemaMutable method overrides

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true)
        {
            RejectIfInvalidParentObjectType(parentObject);
            return new TableIDColumnMutable(
                Name
                , SqlTranslator
                , (ITable)parentObject
                , IsNullable
                , IsIdentity
                , NameWhenPrefixed
                , ConstraintTag
                , ID
                , retrieveIDsFromDatabase
                , assert
                );
        }

        // ITableIDColumn methods

        public virtual void PopulateValuesInNullIDRows(
            string tempTableName
            , string tempIDBaseColumnName
            , IConnection conn
            )
        { }

    }

    /// <summary>
    /// Boilerplate implementation of ITableIDColumn without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObject method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public class TableIDColumnImmutable : IDColumnImmutable, ITableIDColumn
    {

        private ITableIDColumn Delegee { get { return (ITableIDColumn)_delegee; } }

        public TableIDColumnImmutable(
            string name = "id"
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , bool IsNullable = false
            , bool IsIdentity = true
            , string nameWhenPrefixed = null
            , string constraintTag = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
            : base(delegee: null)
        {
            _delegee
                = new TableIDColumnMutable(
                    name
                    , sqlTranslator
                    , table
                    , IsNullable
                    , IsIdentity
                    , nameWhenPrefixed
                    , constraintTag
                    , id
                    , retrieveIDsFromDatabase
                    , assert: false
                    );
            CompleteConstructionIfType(
                typeof(TableIDColumnImmutable)
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        public TableIDColumnImmutable(ITableIDColumn delegee, bool assert = true)
            : base(delegee)
        {
            CompleteConstructionIfType(
                typeof(TableIDColumnImmutable)
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        // DataProcObject method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(ITableIDColumn); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new TableIDColumnImmutable(
                (ITableIDColumn)
                Delegee.GetReferenceForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , assert: false
                    )
                , assert: assert
                );
        }

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new TableIDColumnImmutable(
                (ITableIDColumn)
                Delegee.GetCopyForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , assert: false
                    )
                , assert: assert
                );
        }

        // ITableIDColumn methods

        public virtual void PopulateValuesInNullIDRows(
            string tempTableName
            , string tempIDBaseColumnName
            , IConnection conn
            )
        {
            Delegee.PopulateValuesInNullIDRows(
                tempTableName
                , tempIDBaseColumnName
                , conn
                );
        }

    }

}