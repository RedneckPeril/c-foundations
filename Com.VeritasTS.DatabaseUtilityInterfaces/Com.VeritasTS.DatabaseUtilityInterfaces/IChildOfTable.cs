﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// Base interface for database objects whose
    /// ParentObject is an ITable.
    /// </summary>
    /// <remarks>
    /// Extends IChildOfVable.
    /// </remarks>
    public interface IChildOfTable : IChildOfVable
    {
        ITable Table { get; }
        ITable GetTableSafely();
        IChildOfTable GetReferenceForTable(ITable vable, bool? retrieveIDsFromDatabase = null, bool assert = true);
        IChildOfTable GetCopyForTable(ITable vable, bool? retrieveIDsFromDatabase = null, bool assert = true);
        IChildOfTable GetSafeReferenceForTable(ITable vable, bool? retrieveIDsFromDatabase = null, bool assert = true);
    }

    /// <summary>
    /// Base interface for database objects whose
    /// ParentObject is an ITable and that can be altered
    /// after construction.
    /// </summary>
    /// <remarks>
    /// Merely adds variability to IChildOfTable.
    /// <para>
    /// Extends IChildOfVableMutable.</para>
    /// </remarks>
    public interface IChildOfTableMutable : IChildOfTable, IChildOfVableMutable
    {
        void SetTable(
            ITable value
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            );
    }

    /// <summary>
    /// Boilerplate implementation of IChildOfTableMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides.</item>
    ///   <list type="bullet">
    ///   <item>ValidOwnerTypesWhenNotOwner, occasionally if not abstract type
    ///   and if Owner is not required to be ParentObject.</item>
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>SetOwner, rarely, e.g. when Owner should always be ParentObject 
    /// and vice versa.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>ParentObjectMustBeOwner, if true rather than false.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaMutable method overrides
    ///   <list type="bullet">
    ///   <item>SetParentObject, rarely, e.g. when Owner should always be ParentObject 
    /// and vice versa.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    abstract public class ChildOfTableMutable : ChildOfVableMutable, IChildOfTableMutable
    {

        private IDatabaseObject _parentObject;

        protected ChildOfTableMutable() : base() { }

        public ChildOfTableMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , IObjectType objectType = null
            , IObjectType physicalObjectType = null
            , IDataProcObject owner = null
            , string nameWhenPrefixed = null
            , int? id = null
            ) : base(
                name
                , sqlTranslator
                , table
                , objectType
                , physicalObjectType
                , owner
                , nameWhenPrefixed
                , id
            )
        { }

        // DatabaseObjectMutable method overrides

        // ObjectInSchema method overrides

        public override List<Type> ValidParentObjectTypes
        {
            get { return new List<Type> { typeof(ITable) }; }
        }

        // DataProcObjectMutable method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (IsFullyConstructed)
            {
                Debug.Assert(base.PassesSelfAssertion());
                if (ParentObject != null)
                    Debug.Assert(
                        typeof(ITable).IsAssignableFrom(GetType())
                        , "The parent object of an IChildOfTable object "
                        + "must be an ITable."
                        );
            }
            return true;
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , vable: null
                , objectType: null
                , physicalObjectType: null
                , owner: null
                , nameWhenPrefixed: null
                , id: default(int?)
                );
        }

        protected override void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator
            , IVable vable
            , IObjectType objectType
            , IObjectType physicalObjectType
            , IDataProcObject owner
            , string nameWhenPrefixed
            , int? id
            )
        {
            base.ConstructAfterConstruction(
                name
                , sqlTranslator
                , vable
                , objectType
                , physicalObjectType
                , owner
                , nameWhenPrefixed
                , id
                );
        }

        // IChildOfTable methods

        public ITable Table { get { return (ITable)ParentObject; } }
        public ITable GetTableSafely() { return (ITable)GetParentObjectSafely(); }
        public IChildOfTable GetReferenceForTable(ITable vable, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return (IChildOfTable)GetReferenceForParentObject(vable, retrieveIDsFromDatabase, assert);
        }
        public IChildOfTable GetCopyForTable(ITable vable, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return (IChildOfTable)GetCopyForParentObject(vable, retrieveIDsFromDatabase, assert);
        }
        public IChildOfTable GetSafeReferenceForTable(ITable vable, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return (IChildOfTable)GetSafeReferenceForParentObject(vable, retrieveIDsFromDatabase, assert);
        }

        // IChildOfTableMutable methods

        public void SetTable(
            ITable value
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            SetParentObject(value, retrieveIDsFromDatabase, assert);
        }

    }

    /// <summary>
    /// Boilerplate implementation of IChildOfTable without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    abstract public class ChildOfTableImmutable : ChildOfVableImmutable, IChildOfTable
    {

        private IChildOfTable Delegee { get { return (IChildOfTable)_delegee; } }

        protected ChildOfTableImmutable() : base() { }

        public ChildOfTableImmutable(
            IConstraint _delegee
            , bool assert = true
            ) : base( _delegee, false)
        {
            CompleteConstructionIfType(typeof(ConstraintImmutable), false, assert);
        }

        // IChildOfTableMutable methods

        public ITable Table { get { return (ITable)ParentObject; } }
        public ITable GetTableSafely() { return (ITable)GetParentObjectSafely(); }
        public IChildOfTable GetReferenceForTable(ITable vable, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return (IChildOfTable)GetReferenceForParentObject(vable, retrieveIDsFromDatabase, assert);
        }
        public IChildOfTable GetCopyForTable(ITable vable, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return (IChildOfTable)GetCopyForParentObject(vable, retrieveIDsFromDatabase, assert);
        }
        public IChildOfTable GetSafeReferenceForTable(ITable vable, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return (IChildOfTable)GetSafeReferenceForParentObject(vable, retrieveIDsFromDatabase, assert);
        }

    }

}
