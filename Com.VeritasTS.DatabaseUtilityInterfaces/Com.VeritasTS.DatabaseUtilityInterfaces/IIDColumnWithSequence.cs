﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface IIDColumnWithSequence : ITableIDColumn
    {
        IDatabaseSequence Sequence { get; }
        string TempInsertedIDBaseColumnName { get; }
        IDatabaseSequence GetSequenceSafely();
    }
    public interface IIDColumnWithSequenceMutable : IIDColumnWithSequence, ITableIDColumnMutable
    {
        void SetSequence(
            IDatabaseSequence value
            , bool? retrieveSequenceIDFromDatabase = false
            , bool assert = true
            );
        void SetTempInsertedIDBaseColumnName(string value, bool assert = true);
    }

    /// <summary>
    /// Boilerplate implementation of IIDColumnWithSequenceMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode(), if additional members are added.</item>
    ///   <item>HighestPrimeUsedInHashCode, if GetHashCode() is overridden.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>SetOwner, if (as is occasionally the case) Owner
    /// is constrained to be an instance of a particular object type.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>ObjectIsValidTypeForParentObject, rarely, and generally only for
    ///   highly special-purpose column classes dedicated to a particular type
    ///   of special-purpose table class.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class IDColumnWithSequenceMutable : TableIDColumnMutable, IIDColumnWithSequenceMutable
    {

        private IDatabaseSequence _sequence;
        private string _tempInsertedIDBaseColumnName;
        private string FullTypeNameForCodeErrors { get { return typeof(IDColumnWithSequenceMutable).FullName; } }
        protected string DefaultTempInsertedIDBaseColumnName { get { return "IDBase"; } }

        public IDColumnWithSequenceMutable() : base()
        {
            SetIsNullable(true, false);
            SetIsIdentity(false, false);
            SetTempInsertedIDBaseColumnName(null, false);
        }

        public IDColumnWithSequenceMutable(
            string name = "id"
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , IDatabaseSequence sequence = null
            , string tempInsertedIDBaseColumnName = null
            , string nameWhenPrefixed = null
            , string constraintTag = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
            : base(
                  name == null ? "id" : name
                  , sqlTranslator: sqlTranslator
                  , table: table
                  , isNullable: true
                  , isIdentity: false
                  , nameWhenPrefixed: nameWhenPrefixed
                  , constraintTag: constraintTag
                  , id: id
                  )
        {
            ConstructNewMembers(sequence, tempInsertedIDBaseColumnName, true);
            CompleteConstructionIfType(typeof(IDColumnWithSequenceMutable), retrieveIDsFromDatabase);
        }

        // DataProcObjectMutable method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    IsNullable
                    , "An IIDColumnWithSequence must be nullable."
                    );
                Debug.Assert(
                    !IsIdentity
                    , "An IIDColumnWithSequence must not be an identity column."
                    );
                Debug.Assert(
                    TempInsertedIDBaseColumnName != null
                    , "An IIDColumnWithSequence object cannot have "
                    + "TempInsertedIDBaseColumnName property."
                    );
                Debug.Assert(
                    TempInsertedIDBaseColumnName.Length > 0
                    , "An IIDColumnWithSequence object's TempInsertedIDBaseColumnName property "
                    + "cannot be the empty string."
                    );
                Debug.Assert(
                    HasNonWhitespace(TempInsertedIDBaseColumnName)
                    , "An IIDColumnWithSequence object's TempInsertedIDBaseColumnName property "
                    + "must contain something other than whitespace."
                    );
            }
            return true;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new IDColumnWithSequenceMutable(
                Name
                , GetSqlTranslatorSafely()
                , GetTableSafely()
                , GetSequenceSafely()
                , TempInsertedIDBaseColumnName
                , NameWhenPrefixed
                , ConstraintTag
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , table: null
                , sequence: null
                , tempInsertedIDBaseColumnName: null
                , nameWhenPrefixed: null
                , constraintTag: null
                , id: null
                );
        }

        protected virtual void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator
            , ITable table
            , IDatabaseSequence sequence
            , string tempInsertedIDBaseColumnName
            , string nameWhenPrefixed
            , string constraintTag
            , int? id 
            )
        {
            if (Name != null) name = Name;
            base.ConstructAfterConstruction(
                name == null ? "id" : name
                , sqlTranslator
                , table
                , true
                , false
                , nameWhenPrefixed: null
                , constraintTag: null
                , id: id
                );
            ConstructNewMembers(sequence, tempInsertedIDBaseColumnName, false);
        }

        private void ConstructNewMembers(
            IDatabaseSequence sequence
            , string tempInsertedIDBaseColumnName
            , bool forceUpdates = true
            )
        {
            // If something is no longer at the default, don't change it
            //   unless the forceUpdates flag is switched on
            if (forceUpdates || Sequence == null) SetSequence(sequence);
            ITable referenceTable = Table != null ? Table.ParentTable : null;
            Debug.Assert(
                referenceTable == null || referenceTable.IDColumnName != null
                , "If a table has children it has to have a non-null IDColumnName property; "
                + "this is not the case with table '" + referenceTable.Name + "' designated "
                + "as the parent table for table '" + Table.Name + "'."
                );
            if (forceUpdates || ( ForeignKey == null && referenceTable != null) )
                SetForeignKey(
                    new SingleColumnForeignKeyMutable(
                        this
                        , new Dictionary<string, string> { [Name] = referenceTable.IDColumnName }
                        , referenceTable
                        , cascadeOnDelete: false
                        , cascadeOnUpdate: false
                        , retrieveIDsFromDatabase: false
                        , assert: false
                        )
                    );
            if (forceUpdates || _tempInsertedIDBaseColumnName == null)
                SetTempInsertedIDBaseColumnName(_tempInsertedIDBaseColumnName, false);
        }

        // ObjectInSchemaMutable method overrides

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true)
        {
            RejectIfInvalidParentObjectType(parentObject);
            return new IDColumnWithSequenceMutable(
                Name
                , parentObject.SqlTranslator
                , (ITable)parentObject
                , (IDatabaseSequence)Sequence.GetCopyForDatabase(parentObject.Database)
                , TempInsertedIDBaseColumnName
                , NameWhenPrefixed
                , ConstraintTag
                , ID
                , retrieveIDsFromDatabase
                , assert
                );
        }

        // IIDColumnMutable method overrides

        protected bool InsertedTempTableIsSortedByIDNullsFirst(
            string tempTableName
            , string tempIDBaseColumnName
            , IConnection conn
            )
        {
            IDatabase db = Database;
            Debug.Assert(
                db != null
                , "Cannot check for inserted table not sorted null-id-first "
                + "if not hooked up to a database, i.e., "
                + "if the Database property is null."
                );
            string sql
                = ""
                + "\n select top(1) 1 "
                + "\n   from " + tempTableName + " nullIDBase"
                + "\n          join " + tempTableName + "notNullIDBase"
                + "\n            on notNullIDBase." + Name + " is not null"
                + "\n           and notNullIDBase." + tempIDBaseColumnName + " < nullIDBase." + tempIDBaseColumnName
                + "\n  where nullIDBase." + Name + " is null"
                + "\n ;"
                ;
            List<string[]> output = null;
            output = db.SqlExecuteReader(sql);
            return output.Count > 0;
        }

        public override void PopulateValuesInNullIDRows(
            string tempTableName
            , string tempTableIDBaseColumnName
            , IConnection conn
            )
        {
            IDatabaseSequence sequence = Sequence;
            Debug.Assert(
                sequence != null
                , "Cannot populate IDs in an IDColumnWithSequence "
                + "if not hooked up to a database sequence, i.e., "
                + "if the Sequence property is null."
                );
            IDatabase db = Database;
            Debug.Assert(
                db != null
                , "Cannot check for badly sorted inserted table "
                + "if not hooked up to a database, i.e., "
                + "if the Database property is null."
                );
            if (
                db.SqlExecuteReader(
                    "select top(1) 1 "
                    + "from " + tempTableName
                    + " where " + Name + " is null"
                    , conn
                    ).Count > 0
                )
            {
                int firstKey
                    = sequence.GetFirstOfKeysForTableRows(
                        tempTableName
                        , Name + " is null"
                        , conn
                        );
                int firstIDBase
                    = (int)db.SqlExecuteSingleValue(
                        "select min( " + tempTableIDBaseColumnName + " ) "
                        + "  from " + tempTableName
                        + " where " + Name + " is null "
                        + ";"
                        );
                db.SqlExecuteReader(
                    ""
                    + "\n update " + tempTableName
                    + "\n    set " + Name + " = " + tempTableIDBaseColumnName + " + " + firstIDBase.ToString()
                    + "\n  where " + Name + " is null"
                    + ";"
                    , conn
                    );
            }

        }

        // IIDColumnWithSequence methods

        public IDatabaseSequence Sequence { get { return _sequence; } set { SetSequence(value); } }

        public string TempInsertedIDBaseColumnName
        {
            get { return _tempInsertedIDBaseColumnName; }
            set { SetTempInsertedIDBaseColumnName(value); }
        }

        public IDatabaseSequence GetSequenceSafely()
        {
            return
                _sequence == null ? null
                : (IDatabaseSequence)_sequence.GetSafeReference();
        }

        // IIDColumnWithSequenceMutable methods

        public void SetSequence(
            IDatabaseSequence value
            , bool? retrieveSequenceIDFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            if (value == null)
                throw new NullArgumentException(
                    "value"
                    , FullTypeNameForCodeErrors
                    + ".SetSequence( value, retrieveSequenceIDFromDatabase, assert)"
                    );
            IDatabaseSafe db = Database;
            IDatabaseSafe valueDB = value.Database;
            if (
                (db == null && valueDB != null)
                || ( db != null && !db.Equals(valueDB))
                )
                value = (IDatabaseSequence)value.GetCopyForDatabase(null);
            if (
                retrieveSequenceIDFromDatabase != null && retrieveSequenceIDFromDatabase.HasValue
                ? retrieveSequenceIDFromDatabase.Value
                : ID != null && !ID.Equals(value.ID)
                )
                value = (IDatabaseSequence)value.GetCopyWithIDFromDatabase();
            _sequence = value;
        }

        public void SetTempInsertedIDBaseColumnName(
            string value
            , bool assert = true
            )
        {
            if (value == null)
                _tempInsertedIDBaseColumnName = DefaultTempInsertedIDBaseColumnName;
            else
                _tempInsertedIDBaseColumnName = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

    }

    /// <summary>
    /// Boilerplate implementation of IIDColumnWithSequence without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObject method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public class IDColumnWithSequenceImmutable : TableIDColumnImmutable, IIDColumnWithSequence
    {

        private IIDColumnWithSequence Delegee { get { return (IIDColumnWithSequence)_delegee; } }

        public IDColumnWithSequenceImmutable(
            string name = "id"
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , IDatabaseSequence sequence = null
            , string tempInsertedIDBaseColumnName = null
            , string nameWhenPrefixed = null
            , string constraintTag = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
            : base(delegee: null)
        {
            _delegee
                = new IDColumnWithSequenceMutable(
                    name
                    , sqlTranslator == null ? null : (ISqlTranslator)sqlTranslator.GetSafeReference()
                    , table == null ? null : (ITable)table.GetSafeReference()
                    , sequence == null ? null : (IDatabaseSequence)sequence.GetSafeReference()
                    , tempInsertedIDBaseColumnName
                    , nameWhenPrefixed
                    , constraintTag
                    , id
                    , retrieveIDsFromDatabase
                    , assert: false
                    );
            CompleteConstructionIfType(
                typeof(IDColumnWithSequenceImmutable)
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        public IDColumnWithSequenceImmutable(IIDColumnWithSequence delegee, bool assert = true)
            : base(delegee)
        {
            CompleteConstructionIfType(
                typeof(IDColumnWithSequenceImmutable)
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        // DataProcObject method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(IIDColumnWithSequence); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new IDColumnWithSequenceImmutable(
                (IIDColumnWithSequence)
                Delegee.GetReferenceForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , assert: false
                    )
                , assert: assert
                );
        }

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new IDColumnWithSequenceImmutable(
                (IIDColumnWithSequence)
                Delegee.GetCopyForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , assert: false
                    )
                , assert: assert
                );
        }

        // IIDColumnWithSequence methods

        public IDatabaseSequence Sequence { get { return Delegee.Sequence; } }

        public string TempInsertedIDBaseColumnName
        {
            get { return Delegee.TempInsertedIDBaseColumnName; }
        }

        public IDatabaseSequence GetSequenceSafely() { return Delegee.GetSequenceSafely(); }

    }

}