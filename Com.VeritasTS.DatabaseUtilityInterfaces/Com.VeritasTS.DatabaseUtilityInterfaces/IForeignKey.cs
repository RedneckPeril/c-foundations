﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface IForeignKey : IPotentiallySingleColumnConstraint, IChildOfTable
    {
        Dictionary<string, string> KeyReferenceColumnMappings { get; }
        List<string> KeyColumnNames { get; }
        List<string> ReferenceColumnNames { get; }
        ITable ReferencedTable { get; }
        bool CascadeOnDelete { get; }
        bool AutoCascadeOnDeleteIfPossible { get; }
        bool CascadeOnUpdate { get; }
        bool AutoCascadeOnUpdateIfPossible { get; }
        Dictionary<string, string> GetKeyReferenceColumnMappingsSafely();
        List<string> GetKeyColumnNamesSafely();
        List<string> GetReferenceColumnNamesSafely();
        ITable GetReferencedTableSafely();
    }

    public interface IForeignKeyMutable : IForeignKey, IPotentiallySingleColumnConstraintMutable, IChildOfTableMutable
    {
        void SetKeyReferenceColumnMappings(Dictionary<string, string> value, bool assert = true);
        void AddOrOverwriteKeyReferenceMapping(string key, string value, bool assert = true);
        bool RemoveKeyReferenceMapping(string key, bool assert = true);
        void SetReferencedTable(ITable value, bool assert = true);
        void SetCascadeOnDelete(bool value, bool assert = true);
        void SetAutoCascadeOnDeleteIfPossible(bool value, bool assert = true);
        void SetCascadeOnUpdate(bool value, bool assert = true);
        void SetAutoCascadeOnUpdateIfPossible(bool value, bool assert = true);
    }

    /// <summary>
    /// Boilerplate implementation of IForeignKeyMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides.</item>
    ///   <list type="bullet">
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class ForeignKeyMutable : PotentiallySingleColumnConstraintMutable, IForeignKeyMutable
    {

        private Dictionary<string, string> _keyReferenceColumnMappings;
        private ITable _referencedTable;
        private bool _cascadeOnDelete;
        private bool _autoCascadeOnDeleteIfPossible;
        private bool _cascadeOnUpdate;
        private bool _autoCascadeOnUpdateIfPossible;
        private string FullTypeNameForCodeErrors { get { return typeof(ForeignKeyMutable).FullName; } }

        protected ForeignKeyMutable() : base() { }

        public ForeignKeyMutable(
            string name
            , ISqlTranslator sqlTranslator
            , ITable table
            , ITableColumn constrainedColumn = null
            , Dictionary<string, string> keyReferenceColumnMappings = null
            , ITable referencedTable = null
            , bool cascadeOnDelete = false
            , bool autoCascadeOnDeleteIfPossible = false
            , bool cascadeOnUpdate = false
            , bool autoCascadeOnUpdateIfPossible = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name == null ? "" : name
                , sqlTranslator
                , table
                , ObjectTypes.ForeignKey()
                , ObjectTypes.ForeignKey()
                , constrainedColumn: constrainedColumn
                , nameWhenPrefixed: nameWhenPrefixed
                , id: id
                )
        {
            ConstructNewMembers(
                keyReferenceColumnMappings
                , referencedTable
                , cascadeOnDelete
                , autoCascadeOnDeleteIfPossible
                , cascadeOnUpdate
                , autoCascadeOnUpdateIfPossible
                , true
                );
            CompleteConstructionIfType(typeof(ForeignKeyMutable), retrieveIDsFromDatabase, assert);
        }

        public ForeignKeyMutable(
            string name
            , ITableColumn constrainedColumn = null
            , Dictionary<string, string> keyReferenceColumnMappings = null
            , ITable referencedTable = null
            , bool cascadeOnDelete = false
            , bool autoCascadeOnDeleteIfPossible = false
            , bool cascadeOnUpdate = false
            , bool autoCascadeOnUpdateIfPossible = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name == null ? "" : name
                , ObjectTypes.ForeignKey()
                , ObjectTypes.ForeignKey()
                , constrainedColumn: constrainedColumn
                , nameWhenPrefixed: nameWhenPrefixed
                , id: id
                )
        {
            ConstructNewMembers(
                keyReferenceColumnMappings
                , referencedTable
                , cascadeOnDelete
                , autoCascadeOnDeleteIfPossible
                , cascadeOnUpdate
                , autoCascadeOnUpdateIfPossible
                , true
                );
            CompleteConstructionIfType(typeof(ForeignKeyMutable), retrieveIDsFromDatabase, assert);
        }

        public ForeignKeyMutable(
            ITableColumn constrainedColumn = null
            , Dictionary<string, string> keyReferenceColumnMappings = null
            , ITable referencedTable = null
            , bool cascadeOnDelete = false
            , bool autoCascadeOnDeleteIfPossible = false
            , bool cascadeOnUpdate = false
            , bool autoCascadeOnUpdateIfPossible = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                ""
                , ObjectTypes.ForeignKey()
                , ObjectTypes.ForeignKey()
                , constrainedColumn: constrainedColumn
                , nameWhenPrefixed: nameWhenPrefixed
                , id: id
                )
        {
            ConstructNewMembers(
                keyReferenceColumnMappings
                , referencedTable
                , cascadeOnDelete
                , autoCascadeOnDeleteIfPossible
                , cascadeOnUpdate
                , autoCascadeOnUpdateIfPossible
                , true
                );
            CompleteConstructionIfType(typeof(ForeignKeyMutable), retrieveIDsFromDatabase, assert);
        }

        // Object method overrides and overridden GetHasCode assistants

        public override int GetHashCode()
        {
            int p1 = NextPrimes[base.HighestPrimeUsedInHashCode];
            int p2 = NextPrimes[p1];
            return base.GetHashCode()
                + p1 * KeyReferenceColumnMappings.GetHashCode()
                + p2 * (ReferencedTable == null ? 0 : ReferencedTable.GetHashCode());
        }

        public override int HighestPrimeUsedInHashCode
        {
            get
            {
                int p1 = NextPrimes[base.HighestPrimeUsedInHashCode];
                return NextPrimes[p1];
            }
        }

        // IDataProcObject method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    Name != null
                    , "The name of an IForeignKey cannot be null."
                    );
                Debug.Assert(
                    KeyReferenceColumnMappings != null
                    , "KeyReferenceColumnMappings property of IForeignKey object "
                    + (Name == null ? "" : "'" + Name + "' ")
                    + "can be empty dictionary, but can never be null."
                    );
                Debug.Assert(
                    CascadeOnDelete || !AutoCascadeOnDeleteIfPossible
                    , "IForeignKey object "
                    + (Name == null ? "" : "'" + Name + "' ")
                    + "is set not to cascade on delete, yet is set to "
                    + "autocascade on delete if possible, which makes no sense."
                    );
                Debug.Assert(
                    CascadeOnUpdate || !AutoCascadeOnUpdateIfPossible
                    , "IForeignKey object "
                    + (Name == null ? "" : "'" + Name + "' ")
                    + "is set not to cascade on update, yet is set to "
                    + "autocascade on update if possible, which makes no sense."
                    );
                if (ParentObject != null)
                {
                    foreach (string name in KeyReferenceColumnMappings.Keys)
                        Debug.Assert(
                            ((ITable)ParentObject).Columns.ContainsKey(name)
                            , "IForeignKey object "
                            + (Name == null ? "" : "'" + Name + "' ")
                            + "keys on a column named '" + name + "', which "
                            + "does not exist on the parent table."
                            );
                }
                if (ReferencedTable != null)
                    foreach (string name in KeyReferenceColumnMappings.Keys)
                        Debug.Assert(
                            ReferencedTable.Columns.ContainsKey(name)
                            , "IForeignKey object "
                            + (Name == null ? "" : "'" + Name + "' ")
                            + "refers to a column named '" + name + "', which "
                            + "does not exist on the referenced table."
                            );
                if (ConstrainedColumn != null)
                {
                    Debug.Assert(
                        KeyReferenceColumnMappings.Count > 0
                        , "IForeignKey object "
                        + (Name == null ? "" : "'" + Name + "' ")
                        + "specifies a single constrained column, "
                        + "but has no key reference mappings."
                        );
                    Debug.Assert(
                        KeyReferenceColumnMappings.Count < 2
                        , "IForeignKey object "
                        + (Name == null ? "" : "'" + Name + "' ")
                        + "specifies a single constrained column, "
                        + "but has multiple key reference mappings."
                        );
                    Debug.Assert(
                        KeyReferenceColumnMappings.ContainsKey(ConstrainedColumn.Name)
                        , "IForeignKey object "
                        + (Name == null ? "" : "'" + Name + "' ")
                        + "specifies a single constrained column "
                        + (ConstrainedColumn.Name == null ? "" : "'" + ConstrainedColumn.Name + "' ")
                        + ", but that column does not appear in its "
                        + "key reference mappings."
                        );
                }
            }
            return true;
        }

        public override bool MembersMatch(object obj)
        {
            if (!base.MembersMatch(obj)) return false;
            if (!typeof(IForeignKey).IsAssignableFrom(obj.GetType())) return false;
            IForeignKey other = (IForeignKey)obj;
            if (!EqualityUtilities.EqualOrBothNull(this.KeyReferenceColumnMappings, other.KeyReferenceColumnMappings)) return false;
            if (!EqualityUtilities.EqualOrBothNull(this.ReferencedTable, other.ReferencedTable)) return false;
            return true;
        }

        // DataProcObjectMutable method overrides

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new ForeignKeyMutable(
                Name
                , GetSqlTranslatorSafely()
                , (ITable)GetParentObjectSafely()
                , GetConstrainedColumnSafely()
                , GetKeyReferenceColumnMappingsSafely()
                , GetReferencedTableSafely()
                , CascadeOnDelete
                , AutoCascadeOnDeleteIfPossible
                , CascadeOnUpdate
                , AutoCascadeOnUpdateIfPossible
                , NameWhenPrefixed
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , table: null
                , constrainedColumn: null
                , keyReferenceColumnMappings: null
                , referenceTable: null
                , cascadeOnDelete: false
                , autoCascadeOnDeleteIfPossible: false
                , cascadeOnUpdate: false
                , autoCascadeOnUpdateIfPossible: false
                , nameWhenPrefixed: null
                , id: default(int?)
                );
        }

        protected virtual void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator
            , ITable table
            , ITableColumn constrainedColumn
            , Dictionary<string, string> keyReferenceColumnMappings
            , ITable referenceTable
            , bool cascadeOnDelete
            , bool autoCascadeOnDeleteIfPossible
            , bool cascadeOnUpdate
            , bool autoCascadeOnUpdateIfPossible
            , string nameWhenPrefixed
            , int? id
            )
        {
            base.ConstructAfterConstruction(
                name
                , sqlTranslator
                , table
                , ObjectTypes.ForeignKey()
                , ObjectTypes.ForeignKey()
                , constrainedColumn
                , nameWhenPrefixed
                , id
                );
            ConstructNewMembers(
                keyReferenceColumnMappings
                , referenceTable
                , cascadeOnDelete
                , autoCascadeOnDeleteIfPossible
                , cascadeOnUpdate
                , autoCascadeOnUpdateIfPossible
                , false
                );
        }

        private void ConstructNewMembers(
            Dictionary<string, string> keyReferenceColumnMappings
            , ITable referenceTable
            , bool cascadeOnDelete
            , bool autoCascadeOnDeleteIfPossible
            , bool cascadeOnUpdate
            , bool autoCascadeOnUpdateIfPossible
            , bool forceUpdates
            )
        {
            if (forceUpdates || KeyReferenceColumnMappings == null )
                SetKeyReferenceColumnMappings(keyReferenceColumnMappings, assert: false);
            if (forceUpdates || ReferencedTable == null)
                SetReferencedTable(referenceTable, assert: false);
            if (forceUpdates || !CascadeOnDelete)
                SetCascadeOnDelete(cascadeOnDelete, assert: false);
            if (forceUpdates || !AutoCascadeOnDeleteIfPossible)
                SetAutoCascadeOnDeleteIfPossible(autoCascadeOnDeleteIfPossible, assert: false);
            if (forceUpdates || !CascadeOnUpdate)
                SetCascadeOnUpdate(cascadeOnUpdate, assert: false);
            if (forceUpdates || !AutoCascadeOnUpdateIfPossible)
                SetAutoCascadeOnUpdateIfPossible(autoCascadeOnUpdateIfPossible, assert: false);
        }

        // DatabaseObjectMutable method overrides

        public override void RetrieveIDsFromDatabase(bool assert = true)
        {
            if (Database != null)
                if (ReferencedTable != null)
                    if (typeof(ITableMutable).IsAssignableFrom(ReferencedTable.GetType()))
                        ((ITableMutable)ReferencedTable).RetrieveIDsFromDatabase(assert);
            base.RetrieveIDsFromDatabase(assert);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        // ObjectInSchemaMutable method overrides

        public override bool ParentObjectMustBeOwner { get { return true; } }

        public override IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            RejectIfInvalidParentObjectType(parentObject);
            bool retrieveIDs = WillNeedToRetrieveIDs(ParentObject, parentObject, retrieveIDsFromDatabase);
            IForeignKeyMutable retval
                = new ForeignKeyMutable(
                Name
                , parentObject.SqlTranslator
                , (ITable)parentObject
                , ConstrainedColumn
                , KeyReferenceColumnMappings
                , (ITable)ReferencedTable.GetReferenceForDatabase(parentObject.Database)
                , CascadeOnDelete
                , AutoCascadeOnDeleteIfPossible
                , CascadeOnUpdate
                , AutoCascadeOnUpdateIfPossible
                , NameWhenPrefixed
                , ID
                , retrieveIDsFromDatabase: false
                , assert: false
                );
            if (retrieveIDs) retval.RetrieveIDsFromDatabase(assert: false);
            if (assert) Debug.Assert(retval.PassesSelfAssertion());
            return retval;
        }

        // IForeignKey methods and properties

        public Dictionary<string, string> KeyReferenceColumnMappings
        {
            get { return _keyReferenceColumnMappings; }
            set { SetKeyReferenceColumnMappings(value); }
        }

        public List<string> KeyColumnNames { get { return GetKeyColumnNamesSafely(); } }

        public List<string> ReferenceColumnNames { get { return GetReferenceColumnNamesSafely(); } }

        public ITable ReferencedTable
        {
            get { return _referencedTable; }
            set { SetReferencedTable(value); }
        }

        public bool CascadeOnDelete
        {
            get { return _cascadeOnDelete; }
            set { SetCascadeOnDelete(value); }
        }

        public bool AutoCascadeOnDeleteIfPossible
        {
            get { return _autoCascadeOnDeleteIfPossible; }
            set { SetAutoCascadeOnDeleteIfPossible(value); }
        }

        public bool CascadeOnUpdate
        {
            get { return _cascadeOnUpdate; }
            set { SetCascadeOnUpdate(value, assert: true); }
        }

        public bool AutoCascadeOnUpdateIfPossible
        {
            get { return _autoCascadeOnUpdateIfPossible; }
            set { SetAutoCascadeOnUpdateIfPossible(value); }
        }

        public Dictionary<string, string> GetKeyReferenceColumnMappingsSafely()
        {
            Dictionary<string, string> retval = new Dictionary<string, string>();
            foreach (KeyValuePair<string, string> pair in KeyReferenceColumnMappings)
                retval[pair.Key] = pair.Value;
            return retval;
        }

        public List<string> GetKeyColumnNamesSafely()
        {
            List<string> retval = new List<string>();
            foreach (string key in KeyReferenceColumnMappings.Keys)
                retval.Add(key);
            return retval;
        }

        public List<string> GetReferenceColumnNamesSafely()
        {
            List<string> retval = new List<string>();
            foreach (string value in KeyReferenceColumnMappings.Values)
                retval.Add(value);
            return retval;
        }

        public ITable GetReferencedTableSafely()
        {
            if (_referencedTable == null) return null;
            else return (ITable)_referencedTable.GetSafeReference();
        }

        // IForeignKeyImmutable methods

        public void SetCascadeOnDelete(bool value, bool assert = true)
        {
            _cascadeOnDelete = value;
            if (!value) SetAutoCascadeOnDeleteIfPossible(false, assert: false);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public void SetAutoCascadeOnDeleteIfPossible(bool value, bool assert = true)
        {
            _autoCascadeOnDeleteIfPossible = value;
            if (!value)
                // Determine
                if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public void SetCascadeOnUpdate(bool value, bool assert = true)
        {
            _cascadeOnUpdate = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public void SetAutoCascadeOnUpdateIfPossible(bool value, bool assert = true)
        {
            _autoCascadeOnUpdateIfPossible = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public void SetKeyReferenceColumnMappings(Dictionary<string, string> value, bool assert = true)
        {
            if (value == null)
                _keyReferenceColumnMappings = new Dictionary<string, string>();
            else
                _keyReferenceColumnMappings = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public void AddOrOverwriteKeyReferenceMapping(string key, string value, bool assert = true)
        {
            if (key != null)
                _keyReferenceColumnMappings[key] = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public bool RemoveKeyReferenceMapping(string key, bool assert = true)
        {
            bool retval = false;
            if (key != null)
                retval = _keyReferenceColumnMappings.Remove(key);
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }

        public void SetReferencedTable(
            ITable value
            , bool assert = true
            )
        {
            if (value == null)
                throw new NullReferenceTableException(
                    IsFullyConstructed ? this : null
                    , this.GetType().FullName
                    );
            _referencedTable = value;
        }

    }

    /// <summary>
    /// Boilerplate implementation of IChildOfVable without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <item>ForeignKeyImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetCopyForColumn, always unless abstract class.</item>
    ///   <item>GetCopyForTable, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public class ForeignKeyImmutable : PotentiallySingleColumnConstraintImmutable, IForeignKey
    {

        private IForeignKey Delegee { get { return (IForeignKey)_delegee; } }

        public ForeignKeyImmutable(
            string name
            , ISqlTranslator sqlTranslator
            , ITable table
            , ITableColumn constrainedColumn = null
            , Dictionary<string, string> keyReferenceColumnMappings = null
            , ITable referenceTable = null
            , bool cascadeOnDelete = false
            , bool autoCascadeOnDeleteIfPossible = false
            , bool cascadeOnUpdate = false
            , bool autoCascadeOnUpdateIfPossible = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
        {
            _delegee
                = (IForeignKey)
                new ForeignKeyMutable(
                    name
                    , sqlTranslator
                    , table
                    , constrainedColumn
                    , keyReferenceColumnMappings
                    , referenceTable
                    , cascadeOnDelete
                    , autoCascadeOnDeleteIfPossible
                    , cascadeOnUpdate
                    , autoCascadeOnUpdateIfPossible
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    ).GetSafeReference();
            CompleteConstructionIfType(typeof(ForeignKeyImmutable), false, assert);
        }

        public ForeignKeyImmutable(
            string name
            , ITableColumn constrainedColumn
            , Dictionary<string, string> keyReferenceColumnMappings = null
            , ITable referenceTable = null
            , bool cascadeOnDelete = false
            , bool autoCascadeOnDeleteIfPossible = false
            , bool cascadeOnUpdate = false
            , bool autoCascadeOnUpdateIfPossible = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
        {
            _delegee
                = (IForeignKey)
                new ForeignKeyMutable(
                    name
                    , constrainedColumn
                    , keyReferenceColumnMappings
                    , referenceTable
                    , cascadeOnDelete
                    , autoCascadeOnDeleteIfPossible
                    , cascadeOnUpdate
                    , autoCascadeOnUpdateIfPossible
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    ).GetSafeReference();
            CompleteConstructionIfType(typeof(ForeignKeyImmutable), false, assert);
        }

        public ForeignKeyImmutable(
            ITableColumn constrainedColumn
            , Dictionary<string, string> keyReferenceColumnMappings = null
            , ITable referenceTable = null
            , bool cascadeOnDelete = false
            , bool autoCascadeOnDeleteIfPossible = false
            , bool cascadeOnUpdate = false
            , bool autoCascadeOnUpdateIfPossible = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
        {
            _delegee
                = (IForeignKey)
                new ForeignKeyMutable(
                    constrainedColumn
                    , keyReferenceColumnMappings
                    , referenceTable
                    , cascadeOnDelete
                    , autoCascadeOnDeleteIfPossible
                    , cascadeOnUpdate
                    , autoCascadeOnUpdateIfPossible
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , false
                    ).GetSafeReference();
            CompleteConstructionIfType(typeof(ForeignKeyImmutable), false, assert);
        }

        public ForeignKeyImmutable(IForeignKey delegee, bool assert = true)
        {
            _delegee = delegee;
            CompleteConstructionIfType(typeof(ForeignKeyImmutable), false, assert);
        }

        // DataProcObject method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(IForeignKey); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return
                new ForeignKeyImmutable(
                    (IForeignKey)
                    Delegee.GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, false)
                    );
        }

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return
                new ForeignKeyImmutable(
                    (IForeignKey)
                    Delegee.GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, false)
                    );
        }

        // PotentiallySingleColumnConstraintImmutable method overrides

        public override IPotentiallySingleColumnConstraint GetCopyForColumn(ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return new ForeignKeyImmutable(
                (IForeignKey)
                Delegee.GetCopyForColumn(
                    constrainedColumn
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }
        public override IPotentiallySingleColumnConstraint GetCopyForTable(ITable table, ITableColumn constrainedColumn, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return new ForeignKeyImmutable(
                (IForeignKey)
                Delegee.GetCopyForTable(
                    table
                    , constrainedColumn
                    , retrieveIDsFromDatabase
                    , false
                    )
                , assert
                );
        }

        // IForeignKey properties

        public Dictionary<string, string> KeyReferenceColumnMappings { get { return Delegee.GetKeyReferenceColumnMappingsSafely(); } }

        public List<string> KeyColumnNames { get { return Delegee.GetKeyColumnNamesSafely(); } }

        public List<string> ReferenceColumnNames { get { return Delegee.GetReferenceColumnNamesSafely(); } }

        public ITable ReferencedTable { get { return Delegee.GetReferencedTableSafely(); } }

        public bool CascadeOnDelete { get { return Delegee.CascadeOnDelete; } }

        public bool AutoCascadeOnDeleteIfPossible { get { return Delegee.AutoCascadeOnDeleteIfPossible; } }

        public bool CascadeOnUpdate { get { return Delegee.CascadeOnUpdate; } }

        public bool AutoCascadeOnUpdateIfPossible { get { return Delegee.AutoCascadeOnUpdateIfPossible; } }

        public Dictionary<string, string> GetKeyReferenceColumnMappingsSafely() { return Delegee.GetKeyReferenceColumnMappingsSafely(); }

        public List<string> GetKeyColumnNamesSafely() { return Delegee.GetKeyColumnNamesSafely(); }

        public List<string> GetReferenceColumnNamesSafely() { return Delegee.GetReferenceColumnNamesSafely(); }

        public ITable GetReferencedTableSafely() { return Delegee.GetReferencedTableSafely(); }

    }

    public class NullReferenceTableException : Exception
    {
        public IForeignKey BadForeignKey { get; }
        public NullReferenceTableException(
            IForeignKey badForeignKey
            , string badForeignKeyTypeName
            , string message = null
            ) : base(
                (message != null
                ? message
                : "Cannot set the ReferencedTable property of an object "
                + "of type "
                + (badForeignKeyTypeName != null ? badForeignKeyTypeName : badForeignKey.GetType().FullName)
                + " to null."
                )
                )
        {
            BadForeignKey = badForeignKey;
        }
    }

}
