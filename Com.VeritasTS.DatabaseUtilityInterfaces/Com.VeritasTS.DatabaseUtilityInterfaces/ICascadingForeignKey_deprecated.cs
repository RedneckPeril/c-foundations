﻿using System;
using System.Diagnostics;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    //public interface ICascadingForeignKey_deprecated: ISelfAsserter, IMemberMatcher, ISafeMemberOfImmutableOwner
    //{
    //    ITable DependentTable { get; }
    //    IColumn DependentTableColumn { get; }
    //    IColumn ReferenceColumn { get; }
    //}

    //public interface ICascadingForeignKey_deprecatedMutable: ICascadingForeignKey_deprecated
    //{
    //    void SetDependentTable(ITable value);
    //    void SetDependentTableColumn(IColumn value);
    //    void SetReferenceColumn(IColumn value);
    //}

    //public class CascadingForeignKey_deprecatedMutable : ICascadingForeignKey_deprecatedMutable
    //{
    //    public CascadingForeignKey_deprecatedMutable(
    //        ITable dependentTable = null
    //        , IColumn dependentTableColumn = null
    //        , IColumn referenceColumn = null
    //        )
    //    {
    //        DependentTable = dependentTable;
    //        DependentTableColumn = dependentTableColumn;
    //        ReferenceColumn = referenceColumn;
    //        Debug.Assert(PassesSelfAssertion(typeof(CascadingForeignKey_deprecatedMutable)));
    //    }

    //    // Object method overrides
    //    public override bool Equals(object obj)
    //    {
    //        return EqualityUtilities.MemberMatchwiseEqualOrBothNull(this, obj);
    //    }

    //    public override int GetHashCode()
    //    {
    //        return (DependentTable == null ? 0 : DependentTable.GetHashCode())
    //            + 7 * (DependentTableColumn == null ? 0 : DependentTableColumn.GetHashCode())
    //            + 11 * (ReferenceColumn == null ? 0 : ReferenceColumn.GetHashCode());
    //    }

    //    // ISelfAsserter methods
    //    public virtual bool PassesSelfAssertion(Type testableType = null)
    //    {
    //        if (!SkipSelfAssertionUnlessThisType(testableType))
    //        {
    //            bool retval = true;
    //            if (DependentTable != null) retval = retval && DependentTable.PassesSelfAssertion();
    //            if (DependentTableColumn != null) retval = retval && DependentTableColumn.PassesSelfAssertion();
    //            if (ReferenceColumn != null) retval = retval && ReferenceColumn.PassesSelfAssertion();
    //            return retval;
    //        }
    //        else return true;
    //    }

    //    public bool SkipSelfAssertionUnlessThisType(Type testableType = null)
    //    {
    //        if (testableType == null) return false;
    //        return (!testableType.Equals(this.GetType()));
    //    }

    //    // IMemberMatcher methods
    //    public virtual bool MembersMatch(Object obj)
    //    {
    //        if (obj == null) return false;
    //        if (!typeof(ICascadingForeignKey_deprecated).IsAssignableFrom(obj.GetType())) return false;
    //        ICascadingForeignKey_deprecated other = (ICascadingForeignKey_deprecated)obj;
    //        if (!EqualityUtilities.EqualOrBothNull(this.DependentTable, other.DependentTable)) return false;
    //        if (!EqualityUtilities.EqualOrBothNull(this.DependentTableColumn, other.DependentTableColumn)) return false;
    //        if (!EqualityUtilities.EqualOrBothNull(this.ReferenceColumn, other.ReferenceColumn)) return false;
    //        return true;
    //    }

    //    // ISafeMemberOfImmutableOwner methods
    //    public ISafeMemberOfImmutableOwner GetSafeReference()
    //    {
    //        return new CascadingForeignKey_deprecatedMutable(
    //            (ITable)(DependentTable == null ? null : DependentTable.GetSafeReference())
    //            , (IColumn)(DependentTableColumn == null ? null : DependentTableColumn.GetSafeReference())
    //            , (IColumn)(ReferenceColumn == null ? null : ReferenceColumn.GetSafeReference())
    //            );
    //    }

    //    // ICascadingForeignKey_deprecated properties
    //    public ITable DependentTable { get; set; }
    //    public IColumn DependentTableColumn { get; set; }
    //    public IColumn ReferenceColumn { get; set; }

    //    // ICascadingForeignKey_deprecatedMutable methods
    //    public void SetDependentTable(ITable value)
    //    {
    //        DependentTable = value;
    //        Debug.Assert(PassesSelfAssertion(typeof(CascadingForeignKey_deprecatedMutable)));
    //    }

    //    public void SetDependentTableColumn(IColumn value)
    //    {
    //        DependentTableColumn = value;
    //        Debug.Assert(PassesSelfAssertion(typeof(CascadingForeignKey_deprecatedMutable)));
    //    }
    //    public void SetReferenceColumn(IColumn value)
    //    {
    //        ReferenceColumn = value;
    //        Debug.Assert(PassesSelfAssertion(typeof(CascadingForeignKey_deprecatedMutable)));
    //    }

    //}

    //public class CascadingForeignKey_deprecatedImmutable : ICascadingForeignKey_deprecated
    //{
    //    // Private members

    //    private ICascadingForeignKey_deprecated _delegee;

    //    // Constructors
    //    protected CascadingForeignKey_deprecatedImmutable() { }

    //    public CascadingForeignKey_deprecatedImmutable(
    //        ITable dependentTable 
    //        , IColumn dependentTableColumn
    //        , IColumn referenceColumn
    //        )
    //    {
    //        DependentTable = dependentTable;
    //        DependentTableColumn = dependentTableColumn;
    //        ReferenceColumn = referenceColumn;
    //        Debug.Assert(PassesSelfAssertion(typeof(CascadingForeignKey_deprecatedImmutable)));
    //    }
    //    public CascadingForeignKey_deprecatedImmutable( ICascadingForeignKey_deprecated delegee )
    //    {
    //        _delegee = delegee;
    //        Debug.Assert(PassesSelfAssertion(typeof(CascadingForeignKey_deprecatedImmutable)));
    //    }

    //    // Object method overrides
    //    public override bool Equals(object obj)
    //    {
    //        return _delegee.Equals(obj);
    //    }

    //    public override int GetHashCode()
    //    {
    //        return _delegee.GetHashCode();
    //    }

    //    // IMemberMatcher methods
    //    public virtual bool MembersMatch(Object obj)
    //    {
    //        return _delegee.MembersMatch(obj);
    //    }

    //    // ISelfAsserter methods
    //    public virtual bool PassesSelfAssertion(Type testableType = null)
    //    {
    //        if (!SkipSelfAssertionUnlessThisType(testableType))
    //        {
    //            Debug.Assert(
    //                _delegee != null
    //                , "_delegee object cannot be null for "
    //                + "CascadingForeignKey_deprecatedImmutable objects."
    //                );
    //            Debug.Assert(
    //                DependentTable != null
    //                , "DependentTable property cannot be null for "
    //                + "CascadingForeignKey_deprecatedImmutable objects."
    //                );
    //            Debug.Assert(
    //                DependentTable != null
    //                , "DependentTableColumn property cannot be null for "
    //                + "CascadingForeignKey_deprecatedImmutable objects."
    //                );
    //            Debug.Assert(
    //                ReferenceColumn != null
    //                , "DependentTableColumn property cannot be null for "
    //                + "CascadingForeignKey_deprecatedImmutable objects."
    //                );
    //            return _delegee.DependentTable.PassesSelfAssertion()
    //            && _delegee.DependentTableColumn.PassesSelfAssertion()
    //            && _delegee.ReferenceColumn.PassesSelfAssertion();
    //        }
    //        else return true;
    //    }

    //    public bool SkipSelfAssertionUnlessThisType(Type testableType = null)
    //    {
    //        if (testableType == null) return false;
    //        return (!testableType.Equals(this.GetType()));
    //    }

    //    // ISelfAsserter methods
    //    public bool PassesSelfAssertion()
    //    {
    //        bool retval = (_delegee != null);
    //        retval
    //            = retval
    //            && _delegee.PassesSelfAssertion()
    //            && (_delegee.DependentTable != null)
    //            && (_delegee.DependentTableColumn != null)
    //            && (_delegee.ReferenceColumn != null)
    //            ;
    //        retval
    //            = retval
    //            && _delegee.DependentTable.PassesSelfAssertion()
    //            && _delegee.DependentTableColumn.PassesSelfAssertion()
    //            && _delegee.ReferenceColumn.PassesSelfAssertion()
    //            ;
    //        return retval;
    //    }

    //    // ISafeMemberOfImmutableOwner methods
    //    public ISafeMemberOfImmutableOwner GetSafeReference()
    //    {
    //        return new CascadingForeignKey_deprecatedImmutable(
    //            (ICascadingForeignKey_deprecated)_delegee.GetSafeReference()
    //            );
    //    }

    //    // ICascadingForeignKey_deprecated properties
    //    public ITable DependentTable { get;}
    //    public IColumn DependentTableColumn { get; }
    //    public IColumn ReferenceColumn { get; }

    //}

}
