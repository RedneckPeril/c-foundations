﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// This interface identifies the "smallint"
    /// SQL type.
    /// </summary>
    /// <remarks>
    /// We ordinarily just retrieve the static object
    /// SqlBlob.Standard(). Objects are immutable
    /// and therefore threadsafe.
    /// </remarks>
    public interface ISqlBlob : ISqlIntType { }

    /// <summary>
    /// A basic, immutable implementation of the ISqlBlob interface.
    /// </summary>
    public sealed class SqlBlob : SqlLobType, ISqlBlob
    {

        /****************
         *  Private methods
         ***************/

        private string FullTypeNameForCodeErrors { get { return typeof(SqlBlob).FullName; } }
        private static SqlBlob _standard;

        /****************
         *  Constructor
         ***************/

        private SqlBlob() { }

        /****************
         *  SqlDataType method overrides
         ***************/

        public override string DefaultSql { get { return "blob"; } }

        /****************
         *  ISqlBlob properties and methods
         ***************/

        public static SqlBlob Standard()
        {
            if (_standard == null) _standard = new SqlBlob();
            return _standard;
        }

    }

}
