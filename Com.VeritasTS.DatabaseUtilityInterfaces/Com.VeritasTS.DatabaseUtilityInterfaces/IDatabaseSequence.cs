﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{
    public interface IDatabaseSequence : IChildOfSchema, ISequence
    {
        int GetNextKey(IConnection conn);
        int GetFirstKeyInNextBlockOfNKeys(int n, IConnection conn);
        int GetFirstOfKeysForTableRows(string tableName, string whereClause, IConnection conn = null);
    }
    public interface IDatabaseSequenceMutable : IDatabaseSequence, IChildOfSchemaMutable { }

    /// <summary>
    /// Boilerplate implementation of IChildOfSchemaMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidOwnerTypesWhenNotOwner, occasionally if not abstract type
    ///   and if Owner is not required to be ParentObject.</item>
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>SetOwner, rarely, e.g. when Owner should always be ParentObject 
    /// and vice versa.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>ParentObjectMustBeOwner, if true rather than false.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaMutable method overrides
    ///   <list type="bullet">
    ///   <item>SetParentObject, rarely, e.g. when Owner should always be ParentObject 
    /// and vice versa.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class DatabaseSequenceMutable : ChildOfSchemaMutable, IDatabaseSequenceMutable
    {

        private IDatabaseObject _parentObject;

        public DatabaseSequenceMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ISchema schema = null
            , IObjectType physicalObjectType = null
            , IDataProcObject owner = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            ) : base(
                name
                , sqlTranslator
                , schema
                , ObjectTypes.Sequence()
                , physicalObjectType
                , owner
                , nameWhenPrefixed
                , id
            )
        { }

        // DataProcObjectMutable method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    ObjectType == ObjectTypes.Sequence()
                    , "The ObjectType property of an "
                    + "IDatabaseSequence object must be "
                    + "ObjectTypes.Sequence()."
                    );
                Debug.Assert(
                    PhysicalObjectType == ObjectTypes.Sequence()
                    || PhysicalObjectType == ObjectTypes.Table()
                    , "The PhysicalObjectType property of an "
                    + "IDatabaseSequence object must be "
                    + "either ObjectTypes.Sequence() or ObjectTypes.Table()."
                    );
            }
            return true;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new DatabaseSequenceMutable(
                Name
                , GetSqlTranslatorSafely()
                , GetSchemaSafely()
                , GetPhysicalObjectTypeSafely()
                , GetOwnerSafely()
                , NameWhenPrefixed
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        // IDatabaseObject method overrides

        public override IDatabaseObject GetCopyForOwner(
            IDataProcObject owner
            , bool? retrieveIDsFromDatabase = null
            , bool assert = true
            )
        {
            RejectIfInvalidOwnerType(owner);
            bool retrieveIDs
                = retrieveIDsFromDatabase != null && retrieveIDsFromDatabase.HasValue
                ? retrieveIDsFromDatabase.Value
                : owner != null 
                && typeof(IDatabaseObject).IsAssignableFrom(owner.GetType()) 
                && ((IDatabaseObject)owner).ID != null;
            IDatabaseSequenceMutable retval
                = new DatabaseSequenceMutable(
                    Name
                    , SqlTranslator
                    , Schema
                    , PhysicalObjectType
                    , owner
                    , NameWhenPrefixed
                    , ID
                    , retrieveIDsFromDatabase: false
                    , assert: assert
                    );
            if (retrieveIDs) retval.RetrieveIDsFromDatabase(assert: false);
            if (assert) Debug.Assert(retval.PassesSelfAssertion());
            return retval;
        }

        // IObjectInSchema method overrides

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = null
            , bool assert = true
            )
        {
            RejectIfInvalidParentObjectType(parentObject);
            bool retrieveIDs
                = retrieveIDsFromDatabase != null && retrieveIDsFromDatabase.HasValue
                ? retrieveIDsFromDatabase.Value
                : parentObject != null && parentObject.ID != null;
            IDatabaseSequenceMutable retval
                = new DatabaseSequenceMutable(
                    Name
                    , SqlTranslator
                    , (ISchema)parentObject
                    , PhysicalObjectType
                    , Owner
                    , NameWhenPrefixed
                    , ID
                    , retrieveIDsFromDatabase: false
                    , assert: assert
                    );
            if (retrieveIDs) retval.RetrieveIDsFromDatabase(assert: false);
            if (assert) Debug.Assert(retval.PassesSelfAssertion());
            return retval;
        }

        // ISequence methods
        public int GetNextKey() { return GetNextKey(null); }
        public int GetFirstKeyInNextBlockOfNKeys(int n)
        {
            return GetFirstKeyInNextBlockOfNKeys(n, null);
        }

        // IDatabaseSequence methods
        public int GetNextKey(IConnection conn)
        {
            int retval;
            IDatabaseSafe db = Database;
            // assert error here if database is null
            ISqlTranslator translator = SqlTranslator;
            // assert error here if SQL translator is null
            bool useOwnConn = (conn == null);
            if (useOwnConn) conn = db.GetAConnection();
            retval
                = (int)db.SqlExecuteSingleValue(
                    translator.GetNextKeySql(this)
                    );
            db.CommitFully(conn);
            if (useOwnConn) db.ResumeControlOfConnection(conn);
            return retval;
        }

        public int GetFirstKeyInNextBlockOfNKeys(int n, IConnection conn)
        {
            int retval;
            IDatabaseSafe db = Database;
            // assert error here if database is null
            ISqlTranslator translator = SqlTranslator;
            // assert error here if SQL translator is null
            bool useOwnConn = (conn == null);
            if (useOwnConn) conn = db.GetAConnection();
            retval
                = (int)db.SqlExecuteSingleValue(
                    translator.GetFirstKeyInNextBlockOfNKeysSql(this, n)
                    );
            db.CommitFully(conn);
            if (useOwnConn) db.ResumeControlOfConnection(conn);
            return retval;
        }

        public virtual int GetFirstOfKeysForTableRows(string tempTableName, string whereClause, IConnection conn = null)
        {
            int retval;
            if (whereClause != null)
            {
                whereClause = whereClause.Trim();
                if (
                    whereClause.Length > 0
                    && !new Regex("^where\\b").IsMatch(whereClause.ToLower())
                    )
                    whereClause = "where " + whereClause;
            }
            else whereClause = "";
            IDatabaseSafe db = Database;
            // assert error here if database is null
            ISqlTranslator translator = SqlTranslator;
            // assert error here if SQL translator is null
            bool useOwnConn = (conn == null);
            if (useOwnConn) conn = db.GetAConnection();
            List<string[]> output
                = db.SqlExecuteReader(
                    new List<string> { "select top(2) 1 from " + tempTableName + " " + whereClause }
                    , conn
                    );
            db.CommitFully(conn);
            if (output.Count > 1)
                retval
                    = (int)db.SqlExecuteSingleValue(
                        translator.GetKeysForTableRowsSql(
                            this
                            , tempTableName
                            , whereClause
                            )
                        );
            else
                retval
                    = (int)db.SqlExecuteSingleValue(
                        translator.GetNextKeySql(this)
                        );
            db.CommitFully(conn);
            if (useOwnConn) db.ResumeControlOfConnection(conn);
            return retval;
        }

    }

    /// <summary>
    /// Boilerplate implementation of IDatabaseSequence without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public class DatabaseSequenceImmutable : ChildOfSchemaImmutable, IDatabaseSequence
    {

        private IDatabaseSequence Delegee { get { return (IDatabaseSequence)_delegee; } }

        protected DatabaseSequenceImmutable() { }

        public DatabaseSequenceImmutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ISchema schema = null
            , IObjectType physicalObjectType = null
            , IDataProcObject owner = null
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            _delegee = new DatabaseSequenceMutable(
                name
                , sqlTranslator == null ? null : (ISqlTranslator)sqlTranslator.GetSafeReference()
                , schema == null ? null : (ISchema)schema.GetSafeReference()
                , physicalObjectType == null ? null : (IObjectType)physicalObjectType.GetSafeReference()
                , owner == null ? null : (IDataProcObject)owner.GetSafeReference()
                , nameWhenPrefixed: nameWhenPrefixed
                , id: id
                , retrieveIDsFromDatabase: retrieveIDsFromDatabase
                , assert: false
                );
            CompleteConstructionIfType(typeof(DatabaseSequenceMutable), retrieveIDsFromDatabase: false, assert: assert);
        }

        public DatabaseSequenceImmutable(IDatabaseSequence delegee, bool assert = true)
        {
            _delegee = delegee;
            CompleteConstructionIfType(typeof(DatabaseSequenceMutable), retrieveIDsFromDatabase: false, assert: assert);
        }

        // DataProcObject method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(IDatabaseSequence); } }

        // DatabaseObjectMutable method overrides

        public override IDatabaseObject GetReferenceForOwner(IDataProcObject owner, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            IDatabaseObject retval
                = new DatabaseSequenceImmutable(
                (IDatabaseSequence)
                    ((IObjectInSchema)_delegee).GetReferenceForOwner(owner, retrieveIDsFromDatabase, assert)
                    );
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }
        public override IDatabaseObject GetCopyForOwner(IDataProcObject owner, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            IDatabaseObject retval
                = new DatabaseSequenceImmutable(
                (IDatabaseSequence)
                    ((IObjectInSchema)_delegee).GetCopyForOwner(owner, retrieveIDsFromDatabase, assert)
                    );
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            IObjectInSchema retval
                = new DatabaseSequenceImmutable(
                (IDatabaseSequence)
                    ((IObjectInSchema)_delegee).GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, assert)
                    );
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }
        public override IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            IObjectInSchema retval
                = new DatabaseSequenceImmutable(
                (IDatabaseSequence)
                    ((IObjectInSchema)_delegee).GetCopyForParentObject(parentObject, retrieveIDsFromDatabase, assert)
                    );
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }

        // ISequence methods
        public int GetNextKey() { return Delegee.GetNextKey(); }
        public int GetFirstKeyInNextBlockOfNKeys(int n)
        {
            return Delegee.GetFirstKeyInNextBlockOfNKeys(n);
        }

        // IDatabaseSequence methods
        public int GetNextKey(IConnection conn) { return Delegee.GetNextKey(conn); }
        public int GetFirstKeyInNextBlockOfNKeys(int n, IConnection conn)
        {
            return Delegee.GetFirstKeyInNextBlockOfNKeys(n, conn);
        }
        public virtual int GetFirstOfKeysForTableRows(
            string tempTableName
            , string whereClause
            , IConnection conn = null)
        {
            return Delegee.GetFirstOfKeysForTableRows(tempTableName, whereClause, conn);
        }

    }

}
