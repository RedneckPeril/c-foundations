﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// This interface identifies the "smallint"
    /// SQL type.
    /// </summary>
    /// <remarks>
    /// We ordinarily just retrieve the static object
    /// SqlTime.Standard(). Objects are immutable
    /// and therefore threadsafe.
    /// </remarks>
    public interface ISqlTime : ISqlIntType { }

    /// <summary>
    /// A basic, immutable implementation of the ISqlTime interface.
    /// </summary>
    public sealed class SqlTime : SqlDateTimeType, ISqlTime
    {

        /****************
         *  Private methods
         ***************/

        private string FullTypeNameForCodeErrors { get { return typeof(SqlTime).FullName; } }
        private static SqlTime _standard;

        /****************
         *  Constructor
         ***************/

        private SqlTime() { }

        /****************
         *  SqlDataType method overrides
         ***************/

        public override string DefaultSql { get { return "time"; } }

        /****************
         *  ISqlTime properties and methods
         ***************/

        public static SqlTime Standard()
        {
            if (_standard == null) _standard = new SqlTime();
            return _standard;
        }

    }

}
