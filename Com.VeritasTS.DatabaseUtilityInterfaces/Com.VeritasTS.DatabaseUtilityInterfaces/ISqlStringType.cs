﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// This interface groups together the SQL data
    /// types for which IsStringType is true.
    /// </summary>
    public interface ISqlStringType : ISqlDataType
    {
        int MaxLength { get; }
    }

    /// <summary>
    /// A basic, immutable implementation of the ISqlInt interface.
    /// </summary>
    public abstract class SqlStringType : SqlDataType, ISqlStringType
    {

        /****************
         *  Private members
         ***************/

        private string FullTypeNameForCodeErrors { get { return typeof(SqlStringType).FullName; } }

        /****************
         *  Constructor
         ***************/

        public SqlStringType( int maxLength = 1 )
        {
            if (maxLength <= 0)
                throw new InvalidMaxLengthException(maxLength);
            MaxLength = maxLength;
        }

        /****************
         *  ISelfAsserter methods via ISelfUsabilityAsserter
         ***************/

        /// <summary>
        /// Asserts the class invariants for this class.
        /// </summary>
        /// <returns>True, unless an assertion failure exception is thrown.</returns>
        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertion(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    MaxLength > 0
                    , "The MaxLength of a string type "
                    + "must be positive, i.e., > 0."
                    );
            }
            return true;
        }

        /****************
         *  SqlDataType method overrides
         ***************/

        public override bool IsStringType { get { return true; } }

        /****************
         *  ISqlStringType properties and methods
         ***************/

        public int MaxLength { get; }

        public class InvalidMaxLengthException: Exception
        {
            private InvalidMaxLengthException() { }
            public InvalidMaxLengthException(
                int maxLength
                , string message = null
                ): base(
                    message != null ? message
                    : "The MaxLength property of any ISqlStringType object "
                    + "must be positive and therefore cannot be set to "
                    + maxLength.ToString() + "."
                    )
            {
                BadMaxLength = maxLength;
            }
            public int BadMaxLength { get; }
        }

    }

}
