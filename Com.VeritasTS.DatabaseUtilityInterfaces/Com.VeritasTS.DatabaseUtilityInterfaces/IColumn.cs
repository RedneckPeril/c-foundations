﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface IColumn : IChildOfOwningVable
    {
        string DataType { get; }
        bool IsNullable { get; }
        bool IsImmutable { get; }
        bool IsIDColumn { get; }
        bool IsLastUpdateDtm { get; }
        bool IsLastUpdater { get; }
    }

    public interface IColumnMutable : IColumn, IChildOfOwningVableMutable
    {
        void SetDataType(string value, bool assert = true);
        void SetIsNullable(bool value, bool assert = true);
        void SetIsImmutable(bool value, bool assert = true);
    }

    /// <summary>
    /// Boilerplate implementation of IColumnMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode(), if additional members are added.</item>
    ///   <item>HighestPrimeUsedInHashCode, if GetHashCode() is overridden.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>ObjectIsValidTypeForParentObject, rarely, when narrowing the
    ///   type of vable.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class ColumnMutable : ChildOfOwningVableMutable, IColumnMutable
    {

        // Member objects
        private string _dataType;
        private bool _isNullable;
        private bool _isImmutable;

        // Construction

        protected ColumnMutable() : base() { }

        public ColumnMutable(
            string name
            , string dataType
            , ISqlTranslator sqlTranslator = null
            , IVable vable = null
            , bool isNullable = true
            , bool isImmutable = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , sqlTranslator
                , vable
                , ObjectTypes.Column()
                , ObjectTypes.Column()
                , vable
                , nameWhenPrefixed
                , id
                )
        {
            ConstructNewMembers(dataType, IsNullable, isImmutable, false);
            CompleteConstructionIfType(typeof(ColumnMutable), retrieveIDsFromDatabase, assert);
        }

        // Object method overrides and overridden GetHasCode assistants

        public override int GetHashCode()
        {
            int p1 = NextPrimes[base.HighestPrimeUsedInHashCode];
            int p2 = NextPrimes[p1];
            int p3 = NextPrimes[p2];
            return base.GetHashCode()
                + p1 * (DataType == null ? 0 : DataType.GetHashCode())
                + p2 * IsNullable.GetHashCode()
                + p3 * IsImmutable.GetHashCode();
        }

        public override int HighestPrimeUsedInHashCode
        {
            get
            {
                int p1 = NextPrimes[base.HighestPrimeUsedInHashCode];
                int p2 = NextPrimes[p1];
                return NextPrimes[p2];
            }
        }

        // DataProcObjectMutable method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    !(IsLastUpdateDtm && IsLastUpdater)
                    , "An IColumn cannot be both a LastUpdateDtm column "
                     + "and an ILastUpdaterColumn."
                    );
            }
            return true;
        }

        public override bool MembersMatch(object obj)
        {
            if (!base.MembersMatch(obj)) return false;
            if (!typeof(IColumn).IsAssignableFrom(obj.GetType())) return false;
            IColumn other = (IColumn)obj;
            return
                EqualityUtilities.EqualOrBothNull(DataType, other.DataType)
                && IsNullable == other.IsNullable
                && IsImmutable == other.IsImmutable
                && IsLastUpdateDtm == other.IsLastUpdateDtm
                && IsLastUpdater == other.IsLastUpdater;
        }

        public override IDataProcObject GetSafeReference( bool assert)
        {
            return new ColumnMutable(
                Name
                , DataType
                , GetSqlTranslatorSafely()
                , GetVableSafely()
                , IsNullable
                , IsImmutable
                , NameWhenPrefixed
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , dataType: null
                , sqlTranslator: null
                , vable: null
                , isNullable: true
                , isImmutable: false
                , nameWhenPrefixed: null
                , id: null
                );
        }

        protected virtual void ConstructAfterConstruction(
            string name
            , string dataType
            , ISqlTranslator sqlTranslator
            , IVable vable
            , bool isNullable
            , bool isImmutable
            , string nameWhenPrefixed
            , int? id
            )
        {
            base.ConstructAfterConstruction(
                name
                , sqlTranslator
                , vable
                , ObjectTypes.Column()
                , ObjectTypes.Column()
                , owner: null
                , nameWhenPrefixed: null
                , id: id
                );
            ConstructNewMembers(
                dataType
                , isNullable
                , isImmutable
                , false
                );
        }

        private void ConstructNewMembers(
            string dataType
            , bool isNullable
            , bool isImmutable
            , bool forceUpdates
            )
        {
            // If something is no longer at the default, don't change it
            //   unless the forceUpdates flag is switched on
            if (forceUpdates || DataType == null ) SetDataType(dataType);
            if (forceUpdates || IsNullable == true) SetIsNullable(isNullable);
            if (forceUpdates || IsImmutable == false) SetIsImmutable(isImmutable);
        }

        // IObjectInSchema method overrides

        public override bool ParentObjectMustBeOwner { get { return true; } }

        public override List<Type> ValidParentObjectTypes
        {
            get { return new List<Type> { typeof(IVable) }; }
        }

        public override IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = null, bool assert = true)
        {
            RejectIfInvalidParentObjectType(parentObject);
            int? id = null;
            bool retrieveIDs = WillNeedToRetrieveIDs(ParentObject, parentObject, retrieveIDsFromDatabase);
            IColumnMutable retval
                = new ColumnMutable(
                Name
                , DataType
                , SqlTranslator
                , (IVable)parentObject
                , IsNullable
                , IsImmutable
                , NameWhenPrefixed
                , id
                , retrieveIDsFromDatabase: false
                , assert: false
                );
            if (retrieveIDs) retval.RetrieveIDsFromDatabase(assert: false);
            if (assert) Debug.Assert(retval.PassesSelfAssertion());
            return retval;
        }

        // IColumn properties and methods

        public string DataType
        {
            get { return _dataType; }
            set { SetDataType(value, IsFullyConstructed); }
        }
        public bool IsNullable
        {
            get { return _isNullable; }
            set { SetIsNullable(value, IsFullyConstructed); }
        }
        public bool IsImmutable
        {
            get { return _isImmutable; }
            set { SetIsImmutable(value, IsFullyConstructed); }
        }
        public bool IsIDColumn
        {
            get { return typeof(IIDColumn).IsAssignableFrom(this.GetType()); }
        }
        public bool IsLastUpdateDtm
        {
            get { return typeof(ILastUpdateDtmColumn).IsAssignableFrom(this.GetType()); }
        }
        public bool IsLastUpdater
        {
            get { return typeof(ILastUpdaterColumn).IsAssignableFrom(this.GetType()); }
        }

        // IColumnMutable methods

        public virtual void SetDataType(string value, bool assert = true)
        {
            _dataType = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void SetIsNullable(bool value, bool assert = true)
        {
            _isNullable = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void SetIsImmutable(bool value, bool assert = true)
        {
            _isImmutable = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

    }

    /// <summary>
    /// Boilerplate implementation of IColumn without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObject method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public class ColumnImmutable : ChildOfOwningVableImmutable, IColumn
    {

        private IColumn Delegee { get { return (IColumn)_delegee; } }

        protected ColumnImmutable() { }
        
        public ColumnImmutable(
            string name
            , string dataType
            , ISqlTranslator sqlTranslator = null
            , IVable vable = null
            , bool isNullable = true
            , bool isImmutable = false
            , string nameWhenPrefixed = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
        {
            _delegee = new ColumnMutable(
                name
                , dataType
                , sqlTranslator: sqlTranslator == null ? null : (ISqlTranslator)sqlTranslator.GetSafeReference()
                , vable: vable == null ? null : (IVable)vable.GetSafeReference()
                , isNullable: isNullable
                , isImmutable: isImmutable
                , nameWhenPrefixed: nameWhenPrefixed
                , id: id
                , retrieveIDsFromDatabase: retrieveIDsFromDatabase
                , assert: false
                );
            CompleteConstructionIfType(typeof(ColumnMutable), retrieveIDsFromDatabase: false, assert: assert);
        }

        public ColumnImmutable( IColumn delegee, bool assert = true )
        {
            _delegee = delegee;
            CompleteConstructionIfType(typeof(ColumnMutable), retrieveIDsFromDatabase: false, assert: assert);
        }

        // DataProcObject method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(IColumn); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            IObjectInSchema retval
                = new ColumnImmutable(
                (IColumn)
                    ((IObjectInSchema)_delegee).GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, assert)
                    );
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }
        public override IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            IObjectInSchema retval
                = new ColumnImmutable(
                (IColumn)
                    ((IObjectInSchema)_delegee).GetCopyForParentObject(parentObject, retrieveIDsFromDatabase, assert)
                    );
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }

        // IColumn properties and methods
        public string DataType { get { return Delegee.DataType; } }
        public bool IsNullable { get { return Delegee.IsNullable; } }
        public bool IsImmutable { get { return Delegee.IsImmutable; } }
        public bool IsIDColumn { get { return Delegee.IsIDColumn; } }
        public bool IsLastUpdateDtm { get { return Delegee.IsLastUpdateDtm; } }
        public bool IsLastUpdater { get { return Delegee.IsLastUpdater; } }

    }

}
