﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.DatabaseUtilities
{
    public interface ISchema : IDatabaseObject { }
    public class Schema : DatabaseObject, ISchema
    {
        override public IObjectType ObjectType { get; set; } = new ObjectType { Name = "Schema", IsVable = false };
        override public IObjectType PhysicalObjectType { get; set; } = new ObjectType { Name = "Schema", IsVable = false };
        override public object GetThreadsafeClone() { return this; } // need a deep copy
        override public string GetExistsSql()
        {
            return "select count(*) cnt from sys.schemas where name = '" + Name + "'";
        }
        override public void EnsureExistence()
        {
            if (!Exists())
            {
                string sql = "create schema " + Name;
                Database.SqlExecNonQuery(sql);
            }
        }
    }

}
