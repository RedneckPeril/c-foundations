﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// This interface identifies the "integer"
    /// SQL type.
    /// </summary>
    /// <remarks>
    /// We ordinarily just retrieve the static object
    /// SqlSmallint.Standard(). Objects are immutable
    /// and therefore threadsafe.
    /// </remarks>
    public interface ISqlInt : ISqlIntType { }

    /// <summary>
    /// A basic, immutable implementation of the ISqlInt interface.
    /// </summary>
    public sealed class SqlInt : SqlIntType, ISqlInt
    {

        /****************
         *  Private methods
         ***************/

        private static SqlInt _standard;

        /****************
         *  Constructor
         ***************/

        private SqlInt() { }

        /****************
         *  ISqlInt properties and methods
         ***************/

        public override string DefaultSql { get { return "int"; } }

        public static SqlInt Standard()
        {
            if (_standard == null) _standard = new SqlInt();
            return _standard;
        }

    }

}
