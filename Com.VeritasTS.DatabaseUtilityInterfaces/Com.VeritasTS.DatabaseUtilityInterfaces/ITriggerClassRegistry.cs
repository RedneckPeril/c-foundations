﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface ITriggerClassRegistry : ITable
    {
        ITableTrigger Trigger(
            string triggerNameWithSchema
            , IConnection conn
            );
    }

    public interface ITableClassRegistryMutable : ITriggerClassRegistry, ITableMutable
    {
        void RegisterMapping(
            string triggerNameWithSchema
            , ITableTrigger trigger
            , IConnection conn
            );
        void DeregisterMapping(
            string triggerNameWithSchema
            , IConnection conn
            );
    }

    /// <summary>
    /// Boilerplate implementation of ITableClassRegistryMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidOwnerTypesWhenNotOwner, occasionally if not abstract type
    ///   and if Owner is not required to be ParentObject.</item>
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>ParentObjectMustBeOwner, if true rather than false.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class TableClassRegistryMutable : TableMutable, ITableClassRegistryMutable
    {

        /***************************
         * Private properties and members
        ***************************/

        private string FullTypeNameForCodeErrors { get { return typeof(TableClassRegistryMutable).FullName; } }

        /***************************
         * Constructors
        ***************************/

        /// <summary>
        /// Default constructor, which may not fully
        /// construct the object into a useful state,
        /// depending upon how a specific descent class
        /// is designed.
        /// </summary>
        /// <remarks>
        /// The default constructor is used by frameworks and does
        /// NOT mark the object has having been fully constructed.
        /// This means that assertions do not fire until the 
        /// creator explicitly completes the construction, allowing
        /// for objects to be constructed from the class name only
        /// using the null constructor, then initialized with
        /// Set method calls, and finally marked as ready for use.
        /// </remarks>
        public TableClassRegistryMutable() : base() { }

        public TableClassRegistryMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ISchema schema = null
            , string nameWhenPrefixed = null
            , int? id = default(int?)
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , sqlTranslator
                , schema
                , ObjectTypes.Table()
                , retrieveIDsFromDatabase: false
                , assert: false
                )
        {
            ConstructNewMembers();
            if (!retrieveIDsFromDatabase.HasValue || retrieveIDsFromDatabase.Value)
                RetrieveIDsFromDatabase(false);
            CompleteConstructionIfType(typeof(TableMutable), retrieveIDsFromDatabase, assert);
        }

        /***************************
         * DataProcObjectMutable method overrides
        ***************************/

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertion(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
            }
            return true;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new TableClassRegistryMutable(
                Name
                , GetSqlTranslatorSafely()
                , GetSchemaSafely()
                , NameWhenPrefixed
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , schema: null
                , nameWhenPrefixed: null
                , id: null
                );
        }

        protected virtual void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator
            , ISchema schema
            , string nameWhenPrefixed
            , int? id
            )
        {
            base.ConstructAfterConstruction(
                name: name
                , sqlTranslator: sqlTranslator
                , schema: schema
                , objectType: ObjectTypes.Table()
                , owner: null
                , allowsUpdates: true
                , allowsDeletes: true
                , columns: null
                , indexes: null
                , hasIdentityColumn: true
                , identityColumn: null
                , primaryKey: null
                , uniqueKeys: null
                , foreignKeys: null
                , cascadingForeignKeyRegistry: null
                , triggers: null
                , checkConstraints: null
                , defaultConstraints: null
                , isAudited: false
                , auditTable: null
                , publishesChanges: false
                , publishedChangesTable: null
                , changeSubscriberRegistry: null
                , parentTable: null
                , parentInsertColumnListExpressions: null
                , canHaveChildren: false
                , leavesTable: null
                , maintainChildVablesTable: false
                , childVablesTable: null
                , hasIDColumn: false
                , idColumn: null
                , hasInputterColumn: false
                , inputterColumn: null
                , validInputtersTable: null
                , hasLastUpdateDtmColumn: false
                , hasLastUpdaterColumn: false
                , nameWhenPrefixed: null
                , id: default(int?)
                );
            ConstructNewMembers();
        }

        private void ConstructNewMembers(bool forceUpdates = true)
        {
            ISqlTranslator sqlT = SqlTranslator;
            if (sqlT == null)
                throw new ConstructionRequiresSqlTranslatorException(
                    this
                    , typeof(TableMutable).FullName
                    );
            // If something is no longer at the default, don't change it
            //   unless the forceUpdates flag is switched on
            if (forceUpdates || Name == null || Name.Length == 0)
                SetName(null, false);
            if (forceUpdates || Columns == null || Columns.Count() == 0)
            {
                SetColumns(new Dictionary<string, Tuple<int, IColumn>>());
                AddOrOverwriteColumn(
                    new IdentityColumnMutable(
                        "id"
                        , sqlT
                        , this
                        , retrieveIDsFromDatabase: false
                        , assert: false
                        )
                    );
                AddOrOverwriteColumn(
                    new TableColumnMutable(
                        "triggerNameWithSchema"
                        , sqlT.SysnameTypeSql
                        , sqlT
                        , this
                        , isNullable: false
                        , isToBeTrimmed: true
                        , isImmutable: true
                        , constraintTag: "TN"
                        , retrieveIDsFromDatabase: false
                        , assert: false
                        )
                    );
                AddOrOverwriteColumn(
                    new TableColumnMutable(
                        "triggerObject"
                        , sqlT.UnicodeStringTypeSql + "(max)"
                        , sqlT
                        , this
                        , isNullable: false
                        , isToBeTrimmed: true
                        , isImmutable: true
                        , constraintTag: "TO"
                        , retrieveIDsFromDatabase: false
                        , assert: false
                        )
                    );
                AddOrOverwriteColumn(
                    new TableColumnMutable(
                        "triggerTypeFullName"
                        , sqlT.UnicodeStringTypeSql + "(max)"
                        , sqlT
                        , this
                        , isNullable: false
                        , isToBeTrimmed: true
                        , isImmutable: true
                        , constraintTag: "TTFN"
                        , retrieveIDsFromDatabase: false
                        , assert: false
                        )
                    );
                SetHasLastUpdateDtmColumn(true, false, false);
                SetHasLastUpdaterColumn(true, false, false);
                Dictionary<string, Tuple<int, ITableColumn>> columns = TableColumns;
                SetPrimaryKey(
                    new NonnullablePrimaryKeyMutable(
                        constrainedColumn: columns["id"].Item2
                        , columnNames: new List<string> { "id" }
                        , retrieveIDsFromDatabase: false
                        , assert: false
                        )
                    );
                AddOrOverwriteUniqueKey(
                    new UniqueKeyMutable(
                        name: sqlT.TableConstraintName(
                            "_lpk", Name
                            )
                        , objectType: ObjectTypes.UniqueKey()
                        , physicalObjectType: ObjectTypes.UniqueKey()
                        , columnNames: new List<string> { "triggerNameWithSchema" }
                        , retrieveIDsFromDatabase: false
                        , assert: false
                        )
                    );
            }
        }

        /***************************
         * IDatabaseObject method overrides
        ***************************/

        public override IDatabaseObject GetCopyForOwner(
            IDataProcObject owner
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            RejectIfInvalidOwnerType(owner);
            return new TableClassRegistryMutable(
                Name
                , SqlTranslator
                , Schema
                , NameWhenPrefixed
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        /***************************
         * ObjectInSchemaMutable method overrides
        ***************************/

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            RejectIfInvalidParentObjectType(parentObject);
            return new TableClassRegistryMutable(
                Name
                , SqlTranslator
                , Schema
                , NameWhenPrefixed
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        /***************************
         * ITableClassRegistry properties and methods
        ***************************/

        public ITableTrigger Trigger(
            string triggerNameWithSchema
            ,  IConnection conn
            )
        {
            List<string[]> output
                = Database.SqlExecuteReader(
                    ""
                    + "select top(1) triggerObject \n"
                    + "     , triggerType \n"
                    + "  from " + FullName + " with (nolock) \n"
                    + " where triggerNameWithSchema = '" + triggerNameWithSchema.Replace("'", "''") + "'"
                    + ";"
                    , conn
                    );
            XmlSerializer xs = new XmlSerializer(Type.GetType(output[1][1]));
            return (ITableTrigger)xs.Deserialize(new StringReader(output[1][0]));
        }

        /***************************
         * ITableClassRegistryMutable methods
        ***************************/

        public void RegisterMapping(
            string triggerNameWithSchema
            , ITableTrigger trigger
            , IConnection conn
            )
        {
            using (StringWriter sw = new StringWriter())
            {
                XmlSerializer xmlS = new XmlSerializer(typeof(ITableTrigger));
                xmlS.Serialize(sw, trigger);
                string classAsXMLSql = sw.ToString().Replace("'", "''");
                Database.SqlExecuteNonQuery(
                    ""
                    + "merge into " + FullName + " target \n"
                    + "using      " + SqlTranslator.SingletonVableSql + " singleton \n"
                    + "             on triggerNameWithSchema = '" + triggerNameWithSchema.Replace("'", "''") + "' \n"
                    + "when matched then \n"
                    + "update \n"
                    + "   set triggerObject = '" + classAsXMLSql + "' \n"
                    + "     , triggerTypeFullName = '" + trigger.GetType().FullName.Replace("'", "''") + "' \n"
                    + "when not matched then \n"
                    + "insert ( \n"
                    + "       triggerNameWithSchema \n"
                    + "     , triggerObject \n"
                    + "     , triggerTypeFullName \n"
                    + "       ) \n"
                    + "values ( \n"
                    + "       '" + triggerNameWithSchema.Replace("'", "''") + "' \n"
                    + "     , '" + classAsXMLSql + " \n"
                    + "     , '" + trigger.GetType().FullName.Replace("'", "''") + "' \n"
                    + "       ) \n"
                    + ";"
                    , conn
                    );
            }
        }

        public void DeregisterMapping(
            string triggerNameWithSchema
            , IConnection conn
            )
        {
            Database.SqlExecuteNonQuery(
                ""
                + "delete " + FullName + " \n"
                + " where triggerNameWithSchema = '" + triggerNameWithSchema.Replace("'", "''") + "' \n"
                + ";"
                , conn
                );
        }

    }

    /// <summary>
    /// Boilerplate implementation of ITableClassRegistry without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public class TableClassRegistryImmutable : TableImmutable, ITriggerClassRegistry
    {
        private ITriggerClassRegistry Delegee { get { return (ITriggerClassRegistry)_delegee; } }

        //Disable default constructor
        protected TableClassRegistryImmutable() : base() { }

        public TableClassRegistryImmutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ISchema schema = null
            , string nameWhenPrefixed = null
            , int? id = default(int?)
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
        {
            _delegee
                = new TableClassRegistryMutable(
                    name
                    , sqlTranslator
                    , schema
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , assert: false
                    );
            CompleteConstructionIfType(typeof(TableClassRegistryImmutable), false, true);
        }

        public TableClassRegistryImmutable(ITriggerClassRegistry delegee, bool assert = true)
        {
            RejectIfInvalidDelegeeType(delegee);
            _delegee = delegee;
            CompleteConstructionIfType(typeof(TableClassRegistryImmutable), false, true);
        }

        // IDataProcObject method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(ITriggerClassRegistry); } }

        // IDatabaseObject method overrides

        public override IDatabaseObject GetReferenceForOwner(IDataProcObject owner, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            RejectIfInvalidOwnerType(owner);
            return new TableClassRegistryImmutable(
                (ITriggerClassRegistry)Delegee.GetReferenceForOwner(owner, retrieveIDsFromDatabase, false)
                , assert
                );
        }

        public override IDatabaseObject GetCopyForOwner(IDataProcObject owner, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            RejectIfInvalidOwnerType(owner);
            return new TableClassRegistryImmutable(
                (ITriggerClassRegistry)Delegee.GetCopyForOwner(owner, retrieveIDsFromDatabase, false)
                , assert
                );
        }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            RejectIfInvalidParentObjectType(parentObject);
            return new TableClassRegistryImmutable(
                (ITriggerClassRegistry)Delegee.GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, false)
                , assert
                );
        }

        public override IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            RejectIfInvalidParentObjectType(parentObject);
            return new TableClassRegistryImmutable(
                (ITriggerClassRegistry)Delegee.GetCopyForParentObject(parentObject, retrieveIDsFromDatabase, false)
                , assert
                );
        }

        // ITableClassRegistry properties and methods

        public ITriggerClassMapping Trigger(
            string triggerNameWithSchema
            , IConnection conn
            )
        {
            return Delegee.Trigger(
                triggerNameWithSchema
                , conn
                );
        }

    }

}
