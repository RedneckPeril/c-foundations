﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface ITableColumn : IColumn, IChildOfTable, IChildOfOwningTable
    {
        
        bool IsIdentity { get; }
        bool IsComputed { get; }
        string CalcFormula { get; }
        bool IsToBeTrimmed { get; }
        bool IsPersisted { get; }
        bool IsInsertable { get;}
        string ConstraintTag { get; }
        bool HasDefaultConstraint { get; }
        IDefaultConstraint DefaultConstraint { get; }
        bool HasSingleColumnCheckConstraints { get; }
        Dictionary<string, ISingleColumnCheckConstraint> CheckConstraints { get; }
        bool IsConstrainedNonnull { get; }
        INonnullabilityConstraint NonnullabilityConstraint { get; }
        bool HasForeignKey { get; }
        ISingleColumnForeignKey ForeignKey { get; }
        bool HasUniqueKey { get; }
        ISingleColumnUniqueKey UniqueKey { get; }
        IDefaultConstraint GetDefaultConstraintSafely();
        Dictionary<string, ISingleColumnCheckConstraint> GetCheckConstraintsSafely();
        ISingleColumnCheckConstraint GetCheckConstraint(string name);
        ISingleColumnCheckConstraint GetCheckConstraintSafely(string name);
        INonnullabilityConstraint GetNonnullabilityConstraintSafely();
        ISingleColumnForeignKey GetForeignKeySafely();
        ISingleColumnUniqueKey GetUniqueKeySafely();
    }

    public interface ITableColumnMutable : ITableColumn, IColumnMutable, IChildOfTableMutable, IChildOfOwningTableMutable
    {
        void SetDataType(string value, bool adjustTrimmability = true, bool assert = true);
        void SetIsIdentity(bool value, bool adjustInvariance = true, bool assert = true);
        void SetCalcFormula(string value, bool adjustPersistence = true, bool assert = true);
        void SetIsToBeTrimmed(bool value, bool assert = true);
        void SetIsToBeTrimmed(bool? value = null, bool assert = true);
        void SetIsImmutable(bool? value = null, bool assert = true);
        void SetIsPersisted(bool value, bool assert = true);
        void SetIsPersisted(bool? value = null, bool assert = true);
        void SetConstraintTag(string value, bool assert = true);
        void SetDefaultConstraint(
            IDefaultConstraint value
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            );
        void SetCheckConstraints(
            Dictionary<string, ISingleColumnCheckConstraint> value
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            );
        void AddOrOverwriteCheckConstraint(
            ISingleColumnCheckConstraint constraint
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            );
        bool RemoveCheckConstraint(
            ISingleColumnCheckConstraint constraint
            , bool assert = true
            );
        bool RemoveCheckConstraint(
            string name
            , bool assert = true
            );
        void SetIsConstrainedNonnull(
            bool value
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            );
        void SetForeignKey(
            ISingleColumnForeignKey value
            , bool? retrieveForeignKeyIDFromDatabase = default(bool?)
            , bool assert = true
            );
        void SetUniqueKey(
            ISingleColumnUniqueKey value
            , bool? retrieveUniqueKeyIDFromDatabase = default(bool?)
            , bool assert = true
            );
    }

    /// <summary>
    /// Boilerplate implementation of ITableColumnMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode(), if additional members are added.</item>
    ///   <item>HighestPrimeUsedInHashCode, if GetHashCode() is overridden.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>SetOwner, if (as is occasionally the case) Owner
    /// is constrained to be an instance of a particular object type.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>ObjectIsValidTypeForParentObject, rarely, and generally only for
    ///   highly special-purpose column classes dedicated to a particular type
    ///   of special-purpose table class.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class TableColumnMutable : ColumnMutable, ITableColumnMutable
    {
        private bool _isIdentity;
        private string _calcFormula;
        private bool _isToBeTrimmed;
        private bool _isPersisted;
        private IDefaultConstraint _defaultConstraint;
        private Dictionary<string, ISingleColumnCheckConstraint> _checkConstraints;
        private ISingleColumnForeignKey _foreignKey;
        private ISingleColumnUniqueKey _uniqueKey;
        private string _constraintTag;
        private string FullTypeNameForCodeErrors { get { return typeof(TableColumnMutable).FullName; } }

        // Construction

        public TableColumnMutable() : base() { }

        public TableColumnMutable(
            string name
            , string dataType
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , bool isNullable = true
            , bool isIdentity = false
            , bool? isToBeTrimmed = null
            , bool? isImmutable = false
            , string calcFormula = null
            , bool? isPersisted = null
            , IDefaultConstraint defaultConstraint = null
            , Dictionary<string, ISingleColumnCheckConstraint> checkConstraints = null
            , ISingleColumnForeignKey foreignKey = null
            , ISingleColumnUniqueKey uniqueKey = null
            , string nameWhenPrefixed = null
            , string constraintTag = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , dataType
                , sqlTranslator
                , table
                , isNullable
                , isImmutable.HasValue ? isImmutable.Value : false
                , nameWhenPrefixed
                , id
                )
        {
            ConstructNewMembers(
                isIdentity
                , isToBeTrimmed
                , isImmutable
                , calcFormula
                , isPersisted
                , defaultConstraint
                , checkConstraints
                , foreignKey
                , uniqueKey
                , nameWhenPrefixed
                , constraintTag
                , true
                );
            CompleteConstructionIfType(typeof(TableColumnMutable), retrieveIDsFromDatabase);
        }

        // Object method overrides and overridden GetHasCode assistants

        public override int GetHashCode()
        {
            int p1 = NextPrimes[base.HighestPrimeUsedInHashCode];
            int p2 = NextPrimes[p1];
            int p3 = NextPrimes[p2];
            int p4 = NextPrimes[p3];
            int p5 = NextPrimes[p4];
            return base.GetHashCode()
                + p1 * (CalcFormula == null ? 0 : CalcFormula.GetHashCode())
                + p2 * (DefaultConstraint == null ? 0 : DefaultConstraint.GetHashCode())
                + p3 * (CheckConstraints == null ? 0 : CheckConstraints.GetHashCode())
                + p4 * (ForeignKey == null ? 0 : ForeignKey.GetHashCode())
                + p5 * (UniqueKey == null ? 0 : UniqueKey.GetHashCode());
        }

        public override int HighestPrimeUsedInHashCode
        {
            get
            {
                int p1 = NextPrimes[base.HighestPrimeUsedInHashCode];
                int p2 = NextPrimes[p1];
                int p3 = NextPrimes[p2];
                int p4 = NextPrimes[p3];
                return NextPrimes[p4];
            }
        }

        // DataProcObjectMutable method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                if (ParentObject != null)
                    Debug.Assert(
                        typeof(ITable).IsAssignableFrom(ParentObject.GetType())
                        , "An ITableColumn's parent object must be an ITable."
                        );
                Debug.Assert(
                    !(IsNullable && IsIdentity)
                    , "An identity column cannot be nullable."
                    );
                Debug.Assert(
                    ConstraintTag != null
                    , "ConstraintTag property of IColumn object cannot be null."
                    );
                Debug.Assert(
                    new Regex("\\S").IsMatch(ConstraintTag)
                    , "ConstraintTag property of IColumn object must contain "
                    + "something other than whitespace."
                    );
                Debug.Assert(
                    !(IsIdentity && !IsImmutable)
                    , "If an IColumn is an identity column "
                     + "it must also be an invariant column."
                    );
                if (DefaultConstraint != null)
                {
                    IDefaultConstraint defaultConstraint = DefaultConstraint;
                    Debug.Assert(defaultConstraint.PassesSelfAssertion());
                    Debug.Assert(
                        DefaultConstraint.ConstrainedColumn.Equals(this)
                        , "If an IColumn object's DefaultConstraint property's "
                        + "ConstrainedColumn property is not null, it "
                        + "must be the same as (that is, equal to) the IColumn object itself."
                        );
                }
                Debug.Assert(
                    CheckConstraints != null
                    , "An IColumn object's CheckConstraints list may be an empty "
                    + "dictionary, but it cannot be null."
                    );
                if (CheckConstraints.Count > 0)
                {
                    foreach (KeyValuePair<string, ISingleColumnCheckConstraint> pair in CheckConstraints)
                    {
                        Debug.Assert(pair.Value.PassesSelfAssertion());
                        Debug.Assert(
                            pair.Key.Equals(pair.Value.Name)
                            , "For any entry in an IColumn object's "
                            + "CheckConstraints list, the key must be the check "
                            + "constraint's name."
                            );
                        if (pair.Value.ConstrainedColumn != null)
                            Debug.Assert(
                                pair.Value.ConstrainedColumn.Equals(this)
                                , "If one of the ISingleColumnCheckConstraints in an IColumn object's "
                                + "CheckConstraints list has a non-null ConstrainedColumn property, "
                                + "it must be the same as (that is, equal to) the IColumn object itself."
                                );
                    }
                    Debug.Assert(
                        CheckConstraints
                        .Values
                        .Where(
                            column => typeof(INonnullabilityConstraint).IsAssignableFrom(column.GetType()))
                            .Count() < 2
                        , "An IColumn cannot have more than one non-nullability constraint."
                        );
                }
                if (ForeignKey != null)
                {
                    IForeignKey defaultConstraint = ForeignKey;
                    Debug.Assert(defaultConstraint.PassesSelfAssertion());
                    Debug.Assert(
                        ForeignKey.ConstrainedColumn.Equals(this)
                        , "If an IColumn object's ForeignKey property's "
                        + "ConstrainedColumn property is not null, it "
                        + "must be the same as (that is, equal to) the IColumn object itself."
                        );
                }
                if (UniqueKey != null)
                {
                    IUniqueKey defaultConstraint = UniqueKey;
                    Debug.Assert(defaultConstraint.PassesSelfAssertion());
                    Debug.Assert(
                        UniqueKey.ConstrainedColumn.Equals(this)
                        , "If an IColumn object's UniqueKey property's "
                        + "ConstrainedColumn property is not null, it "
                        + "must be the same as (that is, equal to) the IColumn object itself."
                        );
                }
                Debug.Assert(
                    ConstraintTag != null
                    , "An IColumn's constraint tag cannot be null."
                    );
                Debug.Assert(
                    ConstraintTag.Length > 0
                    , "An IColumn's constraint tag cannot be the empty string."
                    );
                Debug.Assert(
                    new Regex("^\\p{L}").IsMatch(ConstraintTag)
                    , "An IColumn's constraint tag must begin with "
                    + "an alphabetic character."
                    );
                Debug.Assert(
                    new Regex("^\\p{L}[\\p{L}\\p{N}]*$").IsMatch(ConstraintTag)
                    , "An IColumn's constraint tag must consist entirely "
                    + "of alphanumeric characters."
                    );
            }
            return true;
        }

        public override bool MembersMatch(object obj)
        {
            if (!base.MembersMatch(obj)) return false;
            if (!typeof(ITableColumn).IsAssignableFrom(obj.GetType())) return false;
            ITableColumn other = (ITableColumn)obj;
            return
                IsIdentity == other.IsIdentity
                && EqualityUtilities.EqualOrBothNull(CalcFormula, other.CalcFormula)
                && IsToBeTrimmed == other.IsToBeTrimmed
                && IsPersisted == other.IsPersisted
                && EqualityUtilities.EqualOrBothNull(DefaultConstraint, other.DefaultConstraint)
                && EqualityUtilities.EqualOrBothNull(CheckConstraints, other.CheckConstraints)
                && EqualityUtilities.EqualOrBothNull(ForeignKey, other.ForeignKey)
                && EqualityUtilities.EqualOrBothNull(UniqueKey, other.UniqueKey);
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new TableColumnMutable(
                Name
                , DataType
                , GetSqlTranslatorSafely()
                , GetTableSafely()
                , IsNullable
                , IsIdentity
                , IsToBeTrimmed
                , IsImmutable
                , CalcFormula
                , IsPersisted
                , GetDefaultConstraintSafely()
                , GetCheckConstraintsSafely()
                , GetForeignKeySafely()
                , GetUniqueKeySafely()
                , NameWhenPrefixed
                , ConstraintTag
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , dataType: null
                , sqlTranslator: null
                , table: null
                , isNullable: true
                , isIdentity: false
                , isToBeTrimmed: null
                , isImmutable: null
                , calcFormula: null
                , isPersisted: null
                , defaultConstraint: null
                , checkConstraints: null
                , foreignKey: null
                , uniqueKey: null
                , nameWhenPrefixed: null
                , constraintTag: null
                , id: null
                );
        }

        protected virtual void ConstructAfterConstruction(
            string name
            , string dataType
            , ISqlTranslator sqlTranslator
            , ITable table
            , bool isNullable
            , bool isIdentity
            , bool? isToBeTrimmed
            , bool? isImmutable
            , string calcFormula
            , bool? isPersisted
            , IDefaultConstraint defaultConstraint
            , Dictionary<string, ISingleColumnCheckConstraint> checkConstraints
            , ISingleColumnForeignKey foreignKey
            , ISingleColumnUniqueKey uniqueKey
            , string nameWhenPrefixed
            , string constraintTag
            , int? id
            )
        {
            base.ConstructAfterConstruction(
                name
                , dataType
                , sqlTranslator
                , table
                , isNullable
                , isImmutable.HasValue ? isImmutable.Value : false
                , nameWhenPrefixed
                , id
                );
            ConstructNewMembers(
                isIdentity
                , isToBeTrimmed
                , isImmutable
                , calcFormula
                , isPersisted
                , defaultConstraint
                , checkConstraints
                , foreignKey
                , uniqueKey
                , nameWhenPrefixed
                , constraintTag
                , false
                );
        }

        private void ConstructNewMembers(
            bool isIdentity
            , bool? isToBeTrimmed
            , bool? isImmutable
            , string calcFormula
            , bool? isPersisted
            , IDefaultConstraint defaultConstraint
            , Dictionary<string, ISingleColumnCheckConstraint> checkConstraints
            , ISingleColumnForeignKey foreignKey
            , ISingleColumnUniqueKey uniqueKey 
            , string nameWhenPrefixed
            , string constraintTag 
            , bool forceUpdates
            )
        {
            // If something is no longer at the default, don't change it
            //   unless the forceUpdates flag is switched on
            if (forceUpdates || IsIdentity == false) SetIsIdentity(isIdentity);
            SetIsToBeTrimmed(isToBeTrimmed);
            SetIsImmutable(isImmutable);
            if (forceUpdates || CalcFormula == null ) SetCalcFormula(calcFormula);
            SetIsPersisted(isPersisted);
            if (forceUpdates || DefaultConstraint == null) SetDefaultConstraint(defaultConstraint);
            if (forceUpdates || CheckConstraints == null) SetCheckConstraints(checkConstraints);
            if (forceUpdates || ForeignKey == null) SetForeignKey(foreignKey);
            if (forceUpdates || UniqueKey == null) SetUniqueKey(uniqueKey);
            if (forceUpdates || UniqueKey == null) SetUniqueKey(uniqueKey);
            if (forceUpdates || ConstraintTag == null) SetConstraintTag(constraintTag);
        }

        // DatabaseObjectMutable method overrides

        public override void RetrieveIDsFromDatabase(bool assert = true)
        {
            IDatabaseSafe db = Database;
            if (db != null && db.IsConnected)
            {
                base.RetrieveIDsFromDatabase(assert);
                if (DefaultConstraint != null)
                    SetDefaultConstraint(
                        (IDefaultConstraint)
                        DefaultConstraint.GetReferenceWithIDFromDatabase(true, assert)
                        );
                foreach (
                    KeyValuePair<string, ISingleColumnCheckConstraint> pair
                    in CheckConstraints
                    )
                    AddOrOverwriteCheckConstraint(
                        (ISingleColumnCheckConstraint)
                        pair.Value.GetReferenceWithIDFromDatabase(true, assert)
                        );
                if (ForeignKey != null)
                    SetForeignKey(
                        (ISingleColumnForeignKey)
                        ForeignKey.GetReferenceWithIDFromDatabase(true, assert)
                        );
                if (UniqueKey != null)
                    SetUniqueKey(
                        (ISingleColumnUniqueKey)
                        UniqueKey.GetReferenceWithIDFromDatabase(true, assert)
                        );
            }
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        // ObjectInSchema method overrides

        public override IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            RejectIfInvalidParentObjectType(parentObject);
            return new TableColumnMutable(
                Name
                , DataType
                , SqlTranslator
                , (ITable)parentObject
                , IsNullable
                , IsIdentity
                , IsToBeTrimmed
                , IsImmutable
                , CalcFormula
                , IsPersisted
                , DefaultConstraint
                , CheckConstraints
                , ForeignKey
                , UniqueKey
                , NameWhenPrefixed
                , ConstraintTag
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        // IChildOfTable properties and methods

        public ITable Table { get { return (ITable)ParentObject; } }
        public ITable GetTableSafely() { return (ITable)GetParentObjectSafely(); }
        public IChildOfTable GetReferenceForTable(ITable vable, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return (IChildOfTable)GetReferenceForParentObject(vable, retrieveIDsFromDatabase, assert);
        }
        public IChildOfTable GetCopyForTable(ITable vable, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return (IChildOfTable)GetCopyForParentObject(vable, retrieveIDsFromDatabase, assert);
        }
        public IChildOfTable GetSafeReferenceForTable(ITable vable, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return (IChildOfTable)GetSafeReferenceForParentObject(vable, retrieveIDsFromDatabase, assert);
        }

        // IChildOfTableMutable methods

        public void SetTable(
            ITable value
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            SetParentObject(value, retrieveIDsFromDatabase, assert);
        }

        // IColumnMutable method overrides

        public override void SetDataType(string value, bool assert = true)
        {
            SetDataType(value, false, assert);
        }

        // ITableColumn properties and methods
        public bool IsIdentity
        {
            get { return _isIdentity; }
            set { SetIsIdentity(value, IsFullyConstructed); }
        }
        public bool IsComputed
        {
            get { return _calcFormula != null; }
        }
        public string CalcFormula
        {
            get { return _calcFormula; }
            set { SetCalcFormula(value, IsFullyConstructed); }
        }
        public bool IsToBeTrimmed
        {
            get { return _isToBeTrimmed; }
            set { SetIsToBeTrimmed(value, IsFullyConstructed); }
        }
        public bool IsPersisted
        {
            get { return _isPersisted; }
            set { SetIsPersisted(value, IsFullyConstructed); }
        }
        public bool IsInsertable
        {
            get
            {
                return
                    !IsIdentity
                    && !IsComputed
                    && !IsLastUpdateDtm
                    && !IsLastUpdater;
            }
        }
        public bool HasDefaultConstraint { get { return DefaultConstraint != null; } }
        public IDefaultConstraint DefaultConstraint
        {
            get { return _defaultConstraint; }
            set { SetDefaultConstraint(value, IsFullyConstructed); }
        }
        public bool HasSingleColumnCheckConstraints { get { return CheckConstraints.Count > 0; } }
        public Dictionary<string, ISingleColumnCheckConstraint> CheckConstraints
        {
            get { return _checkConstraints; }
            set { SetCheckConstraints(value, IsFullyConstructed); }
        }
        public bool IsConstrainedNonnull { get { return NonnullabilityConstraint != null; } }
        public INonnullabilityConstraint NonnullabilityConstraint
        {
            get
            {
                INonnullabilityConstraint retval = null;
                foreach (
                    ISingleColumnCheckConstraint constraint
                    in CheckConstraints.Values.Where(
                        c => typeof(INonnullabilityConstraint).IsAssignableFrom(c.GetType()))
                        .Take(1)
                    )
                    retval = (INonnullabilityConstraint)constraint;
                return retval;
            }
        }
        public bool HasForeignKey { get { return _foreignKey != null; } }
        public ISingleColumnForeignKey ForeignKey
        {
            get { return _foreignKey; }
            set { SetForeignKey(value); }
        }
        public bool HasUniqueKey { get { return _uniqueKey != null; } }
        public ISingleColumnUniqueKey UniqueKey
        {
            get { return _uniqueKey; }
            set { SetUniqueKey(value); }
        }
        public IDefaultConstraint GetDefaultConstraintSafely()
        {
            if (_defaultConstraint == null) return null;
            else return (IDefaultConstraint)_defaultConstraint.GetSafeReference();
        }
        public Dictionary<string, ISingleColumnCheckConstraint> GetCheckConstraintsSafely()
        {
            if (_checkConstraints == null) return null;
            else
            {
                Dictionary<string, ISingleColumnCheckConstraint> retval = new Dictionary<string, ISingleColumnCheckConstraint>();
                foreach (KeyValuePair<string, ISingleColumnCheckConstraint> p in _checkConstraints)
                    retval[p.Key] = (ISingleColumnCheckConstraint)p.Value.GetSafeReference();
                return retval;
            }
        }
        public ISingleColumnCheckConstraint GetCheckConstraint(string name)
        {
            if (_checkConstraints.ContainsKey(name))
                return _checkConstraints[name];
            else return null;
        }
        public ISingleColumnCheckConstraint GetCheckConstraintSafely(string name)
        {
            if (_checkConstraints.ContainsKey(name))
                return (ISingleColumnCheckConstraint)_checkConstraints[name].GetSafeReference();
            else return null;
        }
        public INonnullabilityConstraint GetNonnullabilityConstraintSafely()
        {
            if (NonnullabilityConstraint == null) return null;
            else return (INonnullabilityConstraint)NonnullabilityConstraint.GetSafeReference();
        }
        public ISingleColumnForeignKey GetForeignKeySafely()
        {
            if (_foreignKey == null) return null;
            else return (ISingleColumnForeignKey)_foreignKey.GetSafeReference();
        }
        public ISingleColumnUniqueKey GetUniqueKeySafely()
        {
            if (_uniqueKey == null) return null;
            else return (ISingleColumnUniqueKey)_uniqueKey.GetSafeReference();
        }
        public string ConstraintTag
        {
            get { return _constraintTag; }
            set { SetConstraintTag(value, IsFullyConstructed); }
        }

        // ITableColumnMutable methods

        public virtual void SetDataType(string value, bool adjustTrimmability, bool assert = true)
        {
            if (adjustTrimmability) SetIsToBeTrimmed(value.Contains("char"));
            base.SetDataType(value, assert);
        }

        public virtual void SetIsIdentity(bool value, bool adjustInvariance = true, bool assert = true)
        {
            _isIdentity = value;
            if (value && adjustInvariance) SetIsImmutable(true, false);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void SetCalcFormula(string value, bool adjustPersistence = true, bool assert = true)
        {
            _calcFormula = value;
            if (adjustPersistence) SetIsPersisted(value != null, false);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void SetIsToBeTrimmed(bool value, bool assert = true)
        {
            _isToBeTrimmed = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void SetIsToBeTrimmed(bool? value = null, bool assert = true)
        {
            if (value.HasValue) _isToBeTrimmed = value.Value;
            else _isToBeTrimmed = (DataType.Contains("char"));
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void SetIsImmutable(bool? value = null, bool assert = true)
        {
            if (value.HasValue) base.SetIsImmutable(value.Value, assert);
            else base.SetIsImmutable(IsIdentity);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void SetIsPersisted(bool value, bool assert = true)
        {
            _isPersisted = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void SetIsPersisted(bool? value = null, bool assert = true)
        {
            if (value.HasValue) _isPersisted = value.Value;
            else _isPersisted = (CalcFormula != null);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public void SetDefaultConstraint(
            IDefaultConstraint value
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            bool retrieveIDs = WillNeedToRetrieveIDs(DefaultConstraint, value, retrieveIDsFromDatabase);
            if (value == null)
            {
                if (_defaultConstraint != null)
                {
                    string nameOfDoomed = _defaultConstraint.Name;
                    _defaultConstraint = null;
                    ((ITableMutable)Owner).RemoveDefaultConstraint(nameOfDoomed, assert: false);
                }
            }
            else if (!value.Equals(DefaultConstraint))
            {
                if (!Equals(value.ConstrainedColumn))
                {
                    if (value.ConstrainedColumn == null) _defaultConstraint = value;
                    else
                        _defaultConstraint
                            =
                            (IDefaultConstraint)
                            value.GetReferenceForColumn(
                                this
                                , retrieveIDsFromDatabase: false
                                , assert: false
                                );
                    if (ParentObject != null)
                        ((ITableMutable)ParentObject).AddOrOverwriteDefaultConstraint(
                                _defaultConstraint
                                , retrieveConstraintIDFromDatabase: false
                                , assert: false
                                );
                }
            }
            if (retrieveIDs) RetrieveIDsFromDatabase(false);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public void SetCheckConstraints(
            Dictionary<string, ISingleColumnCheckConstraint> value
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            bool retrieveIDs = WillNeedToRetrieveIDs(CheckConstraints, value, retrieveIDsFromDatabase);
            _checkConstraints = new Dictionary<string, ISingleColumnCheckConstraint>();
            foreach (KeyValuePair<string, ISingleColumnCheckConstraint> pair in value)
                AddOrOverwriteCheckConstraint(
                    pair.Value
                    , retrieveIDsFromDatabase: false
                    , assert: false
                    );
            if (retrieveIDs) RetrieveIDsFromDatabase(false);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public void AddOrOverwriteCheckConstraint(
            ISingleColumnCheckConstraint constraint
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            bool retrieveIDs
                = WillNeedToRetrieveIDs(
                    CheckConstraints[constraint.Name]
                    , constraint
                    , retrieveIDsFromDatabase
                    );
            if (
                constraint != null
                && !constraint.Equals(CheckConstraints[constraint.Name])
                )
            {
                CheckConstraints[constraint.Name]
                    =
                    (ISingleColumnCheckConstraint)
                    constraint.GetReferenceForColumn(
                        this
                        , retrieveIDsFromDatabase: false
                        , assert: false
                        );
                if (ParentObject != null)
                    ((ITableMutable)ParentObject).AddOrOverwriteCheckConstraint(
                        CheckConstraints[constraint.Name]
                        , retrieveConstraintIDFromDatabase: false
                        , assert: false
                        );
            }
            if (retrieveIDs) RetrieveIDsFromDatabase(false);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public bool RemoveCheckConstraint(
            ISingleColumnCheckConstraint constraint
            , bool assert = true
            )
        {
            return RemoveCheckConstraint(
                UtilitiesForINamed.NameOf(constraint)
                , assert
                );
        }

        public virtual bool RemoveCheckConstraint(string name, bool assert = true)
        {
            bool retval = false;
            if (name != null)
            {
                retval = CheckConstraints.Remove(name);
                if (ParentObject != null)
                    ((ITableMutable)ParentObject).RemoveCheckConstraint(name, assert);
            }
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }

        public void SetIsConstrainedNonnull(
            bool value
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            bool retrieveIDs
                = retrieveIDsFromDatabase != null && retrieveIDsFromDatabase.HasValue
                ? retrieveIDsFromDatabase.Value
                : ID != null;
            if (value & !IsConstrainedNonnull)
            {
                AddOrOverwriteCheckConstraint(
                    new NonnullabilityConstraintMutable(
                        null
                        , SqlTranslator
                        , (ITable)Vable
                        , this
                        , retrieveIDsFromDatabase: retrieveIDs
                        , assert: false
                        )
                    );
            }
            else
            {
                foreach (
                    KeyValuePair<string, ISingleColumnCheckConstraint> pair
                    in CheckConstraints.Where(
                        p => typeof(INonnullabilityConstraint).IsAssignableFrom(p.Value.GetType()))
                    )
                    RemoveCheckConstraint(pair.Key, false);
            }
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public void SetForeignKey(
            ISingleColumnForeignKey value
            , bool? retrieveForeignKeyIDFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            bool retrieveIDs
                = retrieveForeignKeyIDFromDatabase != null && retrieveForeignKeyIDFromDatabase.HasValue
                ? retrieveForeignKeyIDFromDatabase.Value
                : ID != null;
            if (value == null)
            {
                if (_foreignKey != null)
                {
                    string nameOfDoomed = _foreignKey.Name;
                    _foreignKey = null;
                    ((ITableMutable)Owner).RemoveForeignKey(nameOfDoomed, assert: false);
                }
            }
            else if (!value.Equals(ForeignKey))
            {
                if (!Equals(value.ConstrainedColumn))
                {
                    if (value.ConstrainedColumn == null) _foreignKey = value;
                    else
                        _foreignKey
                            =
                            (ISingleColumnForeignKey)
                            value.GetReferenceForColumn(
                                this
                                , retrieveIDsFromDatabase: false
                                , assert: false
                                );
                    if (ParentObject != null)
                        ((ITableMutable)ParentObject).AddOrOverwriteForeignKey(
                                _foreignKey
                                , retrieveForeignKeyIDFromDatabase: false
                                , assert: false
                                );
                }
            }
            if (retrieveIDs) RetrieveIDsFromDatabase(false);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public void SetUniqueKey(
            ISingleColumnUniqueKey value
            , bool? retrieveUniqueKeyIDFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            bool retrieveIDs
                = retrieveUniqueKeyIDFromDatabase != null && retrieveUniqueKeyIDFromDatabase.HasValue
                ? retrieveUniqueKeyIDFromDatabase.Value
                : ID != null;
            if (value == null)
            {
                if (_uniqueKey != null)
                {
                    string nameOfDoomed = _uniqueKey.Name;
                    _uniqueKey = null;
                    ((ITableMutable)Owner).RemoveUniqueKey(nameOfDoomed, assert: false);
                }
            }
            else if (!value.Equals(UniqueKey))
            {
                if (!Equals(value.ConstrainedColumn))
                {
                    if (value.ConstrainedColumn == null) _uniqueKey = value;
                    else
                        _uniqueKey
                            =
                            (ISingleColumnUniqueKey)
                            value.GetReferenceForColumn(
                                this
                                , retrieveIDsFromDatabase: false
                                , assert: false
                                );
                    if (ParentObject != null)
                        ((ITableMutable)ParentObject).AddOrOverwriteUniqueKey(
                                _uniqueKey
                                , retrieveUniqueKeyIDFromDatabase: false
                                , assert: false
                                );
                }
            }
            if (retrieveIDs) RetrieveIDsFromDatabase(false);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        public virtual void SetConstraintTag(string value = null, bool assert = true)
        {
            if (value != null) _constraintTag = value;
            else if (NameWhenPrefixed.Equals("ID")) _constraintTag = "ID";
            {
                string newValue
                    = Regex.Replace(
                        Regex.Replace(NameWhenPrefixed, "ID\\$", "")
                        , "\\p{Ll}|[^\\p{L}\\p{N}]"
                        , ""
                        );
            }
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

    }

    /// <summary>
    /// Boilerplate implementation of ITableColumn without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObject method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public class TableColumnImmutable : ColumnImmutable, ITableColumn
    {

        private ITableColumn Delegee { get { return (ITableColumn)_delegee; } }

        protected TableColumnImmutable() { }

        public TableColumnImmutable(
            string name
            , string dataType
            , ISqlTranslator sqlTranslator = null
            , ITable table = null
            , bool isNullable = true
            , bool isIdentity = false
            , bool? isToBeTrimmed = null
            , bool? isImmutable = false
            , string calcFormula = null
            , bool? isPersisted = null
            , IDefaultConstraint defaultConstraint = null
            , Dictionary<string, ISingleColumnCheckConstraint> checkConstraints = null
            , ISingleColumnForeignKey foreignKey = null
            , ISingleColumnUniqueKey uniqueKey = null
            , string nameWhenPrefixed = null
            , string constraintTag = null
            , int? id = null
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
        {
            Dictionary<string, ISingleColumnCheckConstraint> safeCheckConstraints = null;
            if (checkConstraints != null)
            {
                safeCheckConstraints = new Dictionary<string, ISingleColumnCheckConstraint>();
                foreach (KeyValuePair<string, ISingleColumnCheckConstraint> pair in checkConstraints)
                    safeCheckConstraints[pair.Key] = (ISingleColumnCheckConstraint)pair.Value.GetSafeReference();
            }
            _delegee = new TableColumnMutable(
                name
                , dataType
                , sqlTranslator
                , table == null ? null : (ITable)table.GetSafeReference()
                , isNullable
                , isIdentity
                , isToBeTrimmed
                , isImmutable
                , calcFormula
                , isPersisted
                , defaultConstraint
                , checkConstraints
                , foreignKey
                , uniqueKey
                , nameWhenPrefixed
                , constraintTag
                , id
                , retrieveIDsFromDatabase: retrieveIDsFromDatabase
                , assert: false
                );
            CompleteConstructionIfType(typeof(TableColumnImmutable), retrieveIDsFromDatabase: false, assert: assert);
        }

        public TableColumnImmutable(IColumn delegee, bool assert = true)
        {
            _delegee = delegee;
            CompleteConstructionIfType(typeof(TableColumnImmutable), retrieveIDsFromDatabase: false, assert: assert);
        }

        // DataProcObject method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(ITableColumn); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            IObjectInSchema retval
                = new TableColumnImmutable(
                (ITableColumn)
                    ((IObjectInSchema)_delegee).GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, assert)
                    );
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }
        public override IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            IObjectInSchema retval
                = new TableColumnImmutable(
                (ITableColumn)
                    ((IObjectInSchema)_delegee).GetCopyForParentObject(parentObject, retrieveIDsFromDatabase, assert)
                    );
            if (assert) Debug.Assert(PassesSelfAssertion());
            return retval;
        }

        // ITableColumn properties and methods
        public ITable Table { get { return (ITable)ParentObject; } }
        public ITable GetTableSafely() { return (ITable)GetParentObjectSafely(); }
        public IChildOfTable GetReferenceForTable(ITable vable, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return (IChildOfTable)GetReferenceForParentObject(vable, retrieveIDsFromDatabase, assert);
        }
        public IChildOfTable GetCopyForTable(ITable vable, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return (IChildOfTable)GetCopyForParentObject(vable, retrieveIDsFromDatabase, assert);
        }
        public IChildOfTable GetSafeReferenceForTable(ITable vable, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return (IChildOfTable)GetSafeReferenceForParentObject(vable, retrieveIDsFromDatabase, assert);
        }

        // ITableColumn properties and methods
        public bool IsIdentity { get { return Delegee.IsIdentity; } }
        public bool IsComputed { get { return Delegee.IsComputed; } }
        public string CalcFormula { get { return Delegee.CalcFormula; } }
        public bool IsToBeTrimmed { get { return Delegee.IsToBeTrimmed; } }
        public bool IsPersisted { get { return Delegee.IsPersisted; } }
        public bool IsInsertable { get { return Delegee.IsInsertable; } }
        public bool HasDefaultConstraint { get { return Delegee.HasDefaultConstraint; } }
        public IDefaultConstraint DefaultConstraint { get { return Delegee.DefaultConstraint; } }
        public bool HasSingleColumnCheckConstraints { get { return Delegee.HasSingleColumnCheckConstraints; } }
        public Dictionary<string, ISingleColumnCheckConstraint> CheckConstraints { get { return Delegee.GetCheckConstraintsSafely(); } }
        public bool IsConstrainedNonnull { get { return Delegee.IsConstrainedNonnull; } }
        public INonnullabilityConstraint NonnullabilityConstraint { get { return Delegee.GetNonnullabilityConstraintSafely(); } }
        public bool HasForeignKey { get { return Delegee.HasForeignKey; } }
        public ISingleColumnForeignKey ForeignKey { get { return Delegee.GetForeignKeySafely(); } }
        public bool HasUniqueKey { get { return Delegee.HasUniqueKey; } }
        public ISingleColumnUniqueKey UniqueKey { get { return Delegee.GetUniqueKeySafely(); } }
        public IDefaultConstraint GetDefaultConstraintSafely() { return Delegee.GetDefaultConstraintSafely(); }
        public Dictionary<string, ISingleColumnCheckConstraint> GetCheckConstraintsSafely() { return Delegee.GetCheckConstraintsSafely(); }
        public ISingleColumnCheckConstraint GetCheckConstraint(string name) { return Delegee.GetCheckConstraint(name); }
        public ISingleColumnCheckConstraint GetCheckConstraintSafely(string name) { return Delegee.GetCheckConstraintSafely(name); }
        public INonnullabilityConstraint GetNonnullabilityConstraintSafely() { return Delegee.GetNonnullabilityConstraintSafely(); }
        public ISingleColumnForeignKey GetForeignKeySafely() { return Delegee.GetForeignKeySafely(); }
        public ISingleColumnUniqueKey GetUniqueKeySafely() { return Delegee.GetUniqueKeySafely(); }
        public string ConstraintTag { get { return Delegee.ConstraintTag; } }

    }

}