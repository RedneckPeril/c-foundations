﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// This interface identifies database vendors. It is simply
    /// a subtype IComparableByName and ISafeMemberOfImmutableOwner,
    /// and Name is its only property. 
    /// </summary>
    /// <remarks>
    /// We ordinarily use this type as a sort of enumeration: although
    /// you can define your own custom vendors, 99% of the time we
    /// will simply access static members of the sealed pseudoenumeration
    /// class Vendors. 
    /// </remarks>
    public interface IVendor : ISelfUsabilityAsserter, IComparableByName, ISafeMemberOfImmutableOwner { }

    /// <summary>
    /// A basic, invariant implementation of the IVendor interface.
    /// </summary>
    public class Vendor : ComparableByNameImmutable, IVendor
    {

        /****************
         *  No private members
         ***************/

        /****************
         *  Constructors
         ***************/

        /// <summary>
        /// Hides default constructor.
        /// </summary>
        private Vendor() : base("") { }

        /// <summary>
        /// Construct from name.
        /// </summary>
        public Vendor(string name) : base(name)
        {
            Debug.Assert(PassesSelfAssertion());
        }

        /****************
         *  ISelfAsserter methods via ISelfUsabilityAsserter
         ***************/

        /// <summary>
        /// Asserts the class invariants for this class.
        /// </summary>
        /// <remarks>
        /// As there are no invariants, always instantly returns true.
        /// Method is included in order to satisfy contract for
        /// ISelfAsserter.</remarks>
        /// <returns>True.</returns>
        public override bool PassesSelfAssertion() { return base.PassesSelfAssertion(); }

        /****************
         *  ISelfUsabilityAsserter methods
         ***************/

        /// <summary>
        /// Asserts the usability invariants for this class.
        /// </summary>
        /// <remarks>
        /// The only usability requirement is that the
        /// Name property be usable, according to the
        /// standard definition of "usable" for "Name" properties
        /// of INamed objects.</remarks>
        /// <returns>True.</returns>
        public override bool PassesUsabilityAssertion() { return base.PassesUsabilityAssertion(); }

        /****************
         *  IComparableByName properties and methods
         *  are all inherited from ComparableByNameImmutable
         ***************/

        /****************
         *  ISafeMemberOfImmutableOwner methods
         ***************/

        /// <summary>
        /// Returns itself, which is always safe since the
        /// Vendor class is invariant.
        /// </summary>
        /// <returns>The object itself.</returns>
        public ISafeMemberOfImmutableOwner GetSafeReference() { return this; }

    }

    /// <summary>
    /// A psuedoenumeration holding the standard set of database vendors,
    /// each implemented as an invariant Vendor. 
    /// </summary>
    public sealed class Vendors
    {
        Vendors() { }

        /// <summary>
        /// The Oracle Vendor. 
        /// </summary>
        public static IVendor Oracle()
        {
            return OracleGenerator.instance;
        }

        /// <summary>
        /// The SQLServer Vendor. 
        /// </summary>
        public static IVendor SQLServer()
        {
            return SQLServerGenerator.instance;
        }

        /// <summary>
        /// The MySQL Vendor. 
        /// </summary>
        public static IVendor MySQL()
        {
            return MySQLGenerator.instance;
        }

        // Private generator classes

        class OracleGenerator
        {
            static OracleGenerator() { }
            internal static readonly Vendor instance = new Vendor("Oracle");
        }
        class SQLServerGenerator
        {
            static SQLServerGenerator() { }
            internal static readonly Vendor instance = new Vendor("SQLServer");
        }
        class MySQLGenerator
        {
            static MySQLGenerator() { }
            internal static readonly Vendor instance = new Vendor("MySQL");
        }
    }

}
