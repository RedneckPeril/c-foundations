﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface IHomeVableNameColumn : ITableColumn
    {
    }
    public interface IHomeVableNameColumnMutable : IHomeVableNameColumn, ITableColumnMutable
    {
    }

    /// <summary>
    /// Boilerplate implementation of ITableColumnMutable.
    /// </summary>
    /// <remarks>
    /// Sealed class for now.
    /// <remarks>
    public sealed class HomeVableNameColumnMutable : TableColumnMutable, IHomeVableNameColumnMutable
    {

        public HomeVableNameColumnMutable(
            string name
            , ISqlTranslator sqlTranslator
            , ITable table
            , ITable referenceTable = null
            , string nameWhenPrefixed = null
            , string constraintTag = null
            , int? id = null
            , bool retrieveIDsFromDatabase = false
            , bool assert = true
            )
            : base(
                  (name == null ? "homeVableNameWithSchema" : name)
                  , sqlTranslator != null ? sqlTranslator.SysnameDatatypeAsString
                  : table != null && table.SqlTranslator != null ? table.SqlTranslator.SysnameDatatypeAsString
                  : "nvarchar(255)"
                  , sqlTranslator != null ? sqlTranslator
                  : table != null && table.SqlTranslator != null ? table.SqlTranslator
                  : null
                  , table
                  , isNullable: false
                  , isImmutable: true
                  , nameWhenPrefixed: nameWhenPrefixed
                  , constraintTag: constraintTag
                  , id: id
                  )
        {
            CompleteConstructionIfType(
                typeof(HomeVableNameColumnMutable)
                , retrieveIDsFromDatabase
                , assert
                );
        }

        // DataProcObjectMutable method overrides

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    SqlTranslator.SysnameDatatypeAsString.Equals(DataType)
                    , "A HomeVableNameColumn must be typed as "
                    + SqlTranslator.SysnameDatatypeAsString
                    + ", not "
                    + (DataType == null ? "[null]" : DataType)
                    + "."
                    );
                Debug.Assert(
                    !IsIdentity
                    , "A HomeVableNameColumn cannot be an identity column."
                    );
                Debug.Assert(
                    !IsNullable
                    , "A HomeVableNameColumn cannot be nullable."
                    );
                Debug.Assert(
                    IsImmutable
                    , "A HomeVableNameColumn must be invariant."
                    );
                Debug.Assert(
                    CalcFormula == null
                    , "A HomeVableNameColumn cannot be a computed column "
                    + "and therefore cannot have a computed definition."
                    );
            }
            return true;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new HomeVableNameColumnMutable(
                Name
                , GetSqlTranslatorSafely()
                , GetTableSafely()
                , NameWhenPrefixed
                , ConstraintTag
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        // ObjectInSchemaMutable method overrides

        public override IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            return base.GetCopyForParentObject(parentObject, retrieveIDsFromDatabase, assert);
        }

    }

    /// <summary>
    /// Boilerplate implementation of ITableColumn without variability.
    /// </summary>
    /// <remarks>
    /// Sealed class for now.
    /// <remarks>
    public sealed class HomeVableNameColumnImmutable : TableColumnImmutable, IHomeVableNameColumn
    {

        private IHomeVableNameColumn Delegee { get { return (IHomeVableNameColumn)_delegee; } }

        public HomeVableNameColumnImmutable(
            string name
            , ISqlTranslator sqlTranslator
            , ITable parentObject
            , string nameWhenPrefixed = null
            , string constraintTag = null
            , int? id = null
            , bool retrieveIDsFromDatabase = false
            , bool assert = true
            )
            : base(delegee: null)
        {
            _delegee
                = new HomeVableNameColumnMutable(
                    name
                    , sqlTranslator
                    , parentObject
                    , nameWhenPrefixed
                    , constraintTag
                    , id
                    , retrieveIDsFromDatabase
                    , assert: false
                    );
            CompleteConstructionIfType(typeof(HomeVableNameColumnImmutable), retrieveIDsFromDatabase: false, assert: assert);
        }

        public HomeVableNameColumnImmutable(IHomeVableNameColumn delegee, bool assert)
            : base()
        {
            _delegee = delegee;
            Debug.Assert(PassesSelfAssertion(typeof(HomeVableNameColumnImmutable)));
        }

        // DataProcObject method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(IHomeVableNameColumn); } }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new HomeVableNameColumnImmutable(
                (IHomeVableNameColumn)
                Delegee.GetReferenceForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , assert: false
                    )
                , assert: assert
                );
        }

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new HomeVableNameColumnImmutable(
                (IHomeVableNameColumn)
                Delegee.GetCopyForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , assert: false
                    )
                , assert: assert
                );
        }

    }

}