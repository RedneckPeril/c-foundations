﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface ISequence: ISelfAsserter, ISafeMemberOfImmutableOwner
    {
        int GetNextKey();
        int GetFirstKeyInNextBlockOfNKeys(int n);
    }

}
