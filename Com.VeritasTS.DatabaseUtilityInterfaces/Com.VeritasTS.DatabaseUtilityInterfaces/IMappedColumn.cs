﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface IMappedColumn : IColumn
    {
        IColumn SourceColumn { get; }
        IColumn GetSourceColumnSafely();
    }
    public interface IMappedColumnMutable : IMappedColumn, IColumnMutable
    {
        void SetSourceColumn(IColumn value, bool assert = true);
    }

    /// <summary>
    /// Boilerplate implementation of IMappedColumnMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode(), if additional members are added.</item>
    ///   <item>HighestPrimeUsedInHashCode, if GetHashCode() is overridden.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>SetOwner, if (as is occasionally the case) Owner
    /// is constrained to be an instance of a particular object type.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaMutable method overrides
    ///   <list type="bullet">
    ///   <item>SetParentObject, if (as is usually the case) ParentObject
    /// is constrained to be an instance of a particular object type.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class MappedColumnMutable : ColumnMutable, IMappedColumnMutable
    {

        IColumn _sourceColumn;

        // Construction
        public MappedColumnMutable(
            string name
            , string dataType
            , ISqlTranslator sqlTranslator = null
            , IVable parentObject = null
            , bool isIdentity = false
            , bool isNullable = true
            , string calcFormula = null
            , bool? isToBeTrimmed = null
            , bool isImmutable = false
            , bool? isPersisted = null
            , IDefaultConstraint defaultConstraint = null
            , Dictionary<string, ICheckConstraint> checkConstraints = null
            , string nameWhenPrefixed = null
            , string constraintTag = null
            , IColumn sourceColumn = null
            , int? id = default(int?)
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , dataType
                , sqlTranslator
                , parentObject
                , isIdentity
                , isNullable
                , calcFormula
                , isToBeTrimmed
                , isImmutable
                , isPersisted
                , defaultConstraint
                , checkConstraints
                , nameWhenPrefixed
                , constraintTag
                , id
                )
        {
            SetSourceColumn(sourceColumn, false);
            CompleteConstructionIfType(
                typeof(MappedColumnMutable)
                , retrieveIDsFromDatabase
                , assert
                );
        }

        // Object method overrides

        public override int GetHashCode()
        {
            int p1 = NextPrimes[base.HighestPrimeUsedInHashCode];
            return base.GetHashCode()
                + p1 * (SourceColumn == null ? 0 : SourceColumn.GetHashCode());
        }

        // Object method overrides: GetHashCode assistants

        public override int HighestPrimeUsedInHashCode
        {
            get { return NextPrimes[base.HighestPrimeUsedInHashCode]; }
        }

        // DataProcObjectMutable method overrides
        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
                Debug.Assert(
                    SourceColumn != null
                    , "An IMappedColumn must have a non-null source column."
                    );
                Debug.Assert(SourceColumn.PassesSelfAssertion());
            }
            return true;
        }

        public override bool MembersMatch(object obj)
        {
            if (!base.MembersMatch(obj)) return false;
            if (!typeof(IMappedColumn).IsAssignableFrom(obj.GetType())) return false;
            IMappedColumn other = (IMappedColumn)obj;
            return EqualityUtilities.EqualOrBothNull(SourceColumn, other.SourceColumn);
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new MappedColumnMutable(
                Name
                , DataType
                , GetSqlTranslatorSafely()
                , (IVable)GetOwnerSafely()
                , IsIdentity
                , IsNullable
                , CalcFormula
                , IsToBeTrimmed
                , IsImmutable
                , IsPersisted
                , GetDefaultConstraintSafely()
                , GetCheckConstraintsSafely()
                , NameWhenPrefixed
                , ConstraintTag
                , GetSourceColumnSafely()
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        // IDatabaseObjectMutable method overrides

        public override void RetrieveIDsFromDatabase(bool assert = true)
        {
            if (Database != null)
                if (SourceColumn != null)
                    if (typeof(IColumnMutable).IsAssignableFrom(SourceColumn.GetType()))
                        ((IColumnMutable)SourceColumn).RetrieveIDsFromDatabase(assert);
            base.RetrieveIDsFromDatabase(assert);
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

        // IColumn method overrides
        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            if (
                parentObject != null
                && !typeof(IVable).IsAssignableFrom(parentObject.GetType())
                )
                throw new BadParentObjectTypeException(
                    this
                    , parentObject.GetType().FullName
                    , typeof(IVable).FullName
                    );
            return new MappedColumnMutable(
                Name
                , DataType
                , SqlTranslator
                , (IVable)parentObject
                , IsIdentity
                , IsNullable
                , CalcFormula
                , IsToBeTrimmed
                , IsImmutable
                , IsPersisted
                , DefaultConstraint
                , CheckConstraints
                , NameWhenPrefixed
                , ConstraintTag
                , SourceColumn
                , ID
                , retrieveIDsFromDatabase
                , assert
                );
        }

        // IMappedColumn properties

        public IColumn SourceColumn
        {
            get { return _sourceColumn; }
            set { SetSourceColumn(value); }
        }

        public IColumn GetSourceColumnSafely()
        {
            if (_sourceColumn == null) return null;
            else return (IColumn)_sourceColumn.GetSafeReference();
        }

        // IMappedColumnMutable methods

        public virtual void SetSourceColumn(
            IColumn value
            , bool assert = true
            )
        {
            _sourceColumn = value;
            if (assert) Debug.Assert(PassesSelfAssertion());
        }

    }

    /// <summary>
    /// Boilerplate implementation of IMappedColumn without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>ObjectInSchemaImmutable method overrides
    /// <list type="bullet">
    /// <item>GetReferenceForParentObject, always unless abstract class.</item>
    /// <item>GetCopyForParentObject, always unless abstract class.</item>
    /// </list>
    /// </item>
    /// </list>
    /// <remarks>
    public class MappedColumnImmutable : ColumnImmutable, IMappedColumn
    {

        private IMappedColumn Delegee { get { return (IMappedColumn)_delegee; } }

        // Construction
        public MappedColumnImmutable(
            string name
            , string dataType
            , ISqlTranslator sqlTranslator = null
            , IVable parentObject = null
            , bool isIdentity = false
            , bool isNullable = true
            , string calcFormula = null
            , bool? isToBeTrimmed = null
            , bool isImmutable = false
            , bool? isPersisted = null
            , IDefaultConstraint defaultConstraint = null
            , Dictionary<string, ICheckConstraint> checkConstraints = null
            , string nameWhenPrefixed = null
            , string constraintTag = null
            , IColumn sourceColumn = null
            , int? id = default(int?)
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base()
        {
            _delegee = new MappedColumnMutable(
                name
                , dataType
                , SqlTranslator
                , parentObject
                , isIdentity
                , isNullable
                , calcFormula
                , isToBeTrimmed
                , isImmutable
                , isPersisted
                , defaultConstraint
                , checkConstraints
                , nameWhenPrefixed
                , constraintTag
                , sourceColumn
                , id
                , retrieveIDsFromDatabase
                , assert: false
                );
            CompleteConstructionIfType(
                typeof(MappedColumnImmutable)
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        public MappedColumnImmutable(IMappedColumn delegee, bool assert): base(delegee)
        {
            CompleteConstructionIfType(
                typeof(MappedColumnImmutable)
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        // DataProcObject method overrides

        protected override bool ObjectIsValidTypeForDelegee(IDataProcObject candidate, bool throwException = false)
        {
            bool retval
                = candidate != null && !typeof(IMappedColumn).IsAssignableFrom(candidate.GetType());
            if (!retval && throwException)
                throw new BadDelegeeTypeException(
                    (IsFullyConstructed ? this : null)
                    , this.GetType().FullName
                    , candidate.GetType().FullName
                    , typeof(IMappedColumn).FullName
                    );
            return retval;
        }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new MappedColumnImmutable(
                (IMappedColumn)
                Delegee.GetReferenceForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , assert: false
                    )
                , assert: assert
                );
        }

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            return new MappedColumnImmutable(
                (IMappedColumn)
                Delegee.GetCopyForParentObject(
                    parentObject
                    , retrieveIDsFromDatabase
                    , assert: false
                    )
                , assert: assert
                );
        }

        // IMappedColumn methods and properties

        public IColumn SourceColumn { get { return Delegee.GetSourceColumnSafely(); } }

        public IColumn GetSourceColumnSafely() { return Delegee.GetSourceColumnSafely(); }

    }
}
