﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    /// <summary>
    /// This interface identifies the "smallint"
    /// SQL type.
    /// </summary>
    /// <remarks>
    /// We ordinarily just retrieve the static object
    /// SqlSmallint.Standard(). Objects are immutable
    /// and therefore threadsafe.
    /// </remarks>
    public interface ISqlSmallint : ISqlIntType { }

    /// <summary>
    /// A basic, immutable implementation of the ISqlSmallint interface.
    /// </summary>
    public sealed class SqlSmallint : SqlIntType, ISqlSmallint
    {

        /****************
         *  Private members
         ***************/

        private string FullTypeNameForCodeErrors { get { return typeof(SqlSmallint).FullName; } }
        private static SqlSmallint _standard;

        /****************
         *  Constructor
         ***************/

        private SqlSmallint() { }

        /****************
         *  ISqlSmallint properties and methods
         ***************/

        public override string DefaultSql { get { return "int"; } }

        public static SqlSmallint Standard()
        {
            if (_standard == null) _standard = new SqlSmallint();
            return _standard;
        }

    }

}
