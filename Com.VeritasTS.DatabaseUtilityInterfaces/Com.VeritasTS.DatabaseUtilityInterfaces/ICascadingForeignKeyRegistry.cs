﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.DatabaseUtilityInterfaces
{

    public interface ICascadingForeignKeyRegistry : ITable
    {
        List<ICascadingForeignKeyRelationship> RelationshipsList(
            string tableNameWithSchema
            , bool actionIsDelete
            , IConnection conn
            );
        bool HasAtLeastOneRelationship(
            string tableNameWithSchema
            , bool actionIsDelete
            , IConnection conn
            );
    }

    public interface ICascadingForeignKeyRegistryMutable : ICascadingForeignKeyRegistry, ITableMutable
    {
        void RegisterRelationship(
            IForeignKey fk
            , int placeInDeleteSequence
            , IConnection conn
            );
        void DeregisterRelationship(
            string constrainedTableNameWithSchema
            , string constrainedColumnList
            , string masterDataTableNameWithSchema
            , IConnection conn
            );
    }

    /// <summary>
    /// Boilerplate implementation of ICascadingForeignKeyRegistryMutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode().</item>
    ///   <item>HighestPrimeUsedInHashCode.</item>
    ///   </list>
    /// </item>
    /// <item>DataProcObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>Default constructor: should be simply base() {}.</item>
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   <item>MembersMatch(), if the class adds new members that should
    /// affect determination of equality with other objects.</item>
    ///   <item>GetSafeReference(), always.</item>
    ///   <item>ConstructAfterConstruction(), if there are new members with
    ///   meaningful defaults when constructed from default constructor
    ///   by frameworks, or if defaults for inherited members
    ///   change in the new generation. This involves overriding the
    ///   parameterless version and adding a new overload with
    ///   the same arguments as the primary constructor, directing
    ///   the parameterless version to the parameterized version
    ///   with the default values.</item>
    ///   </list>
    /// </item>
    /// <item>DatabaseObjectMutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidOwnerTypesWhenNotOwner, occasionally if not abstract type
    ///   and if Owner is not required to be ParentObject.</item>
    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchema method overrides
    ///   <list type="bullet">
    ///   <item>ParentObjectMustBeOwner, if true rather than false.</item>
    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
    /// added as members.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public class CascadingForeignKeyRegistryMutable : TableMutable, ICascadingForeignKeyRegistryMutable
    {

        /***************************
         * Private properties and members
        ***************************/

        private string FullTypeNameForCodeErrors { get { return typeof(CascadingForeignKeyRegistryMutable).FullName; } }

        /***************************
         * Constructors
        ***************************/

        /// <summary>
        /// Default constructor, which may not fully
        /// construct the object into a useful state,
        /// depending upon how a specific descent class
        /// is designed.
        /// </summary>
        /// <remarks>
        /// The default constructor is used by frameworks and does
        /// NOT mark the object has having been fully constructed.
        /// This means that assertions do not fire until the 
        /// creator explicitly completes the construction, allowing
        /// for objects to be constructed from the class name only
        /// using the null constructor, then initialized with
        /// Set method calls, and finally marked as ready for use.
        /// </remarks>
        public CascadingForeignKeyRegistryMutable() : base() { }

        public CascadingForeignKeyRegistryMutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ISchema schema = null
            , string nameWhenPrefixed = null
            , int? id = default(int?)
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                name
                , sqlTranslator
                , schema
                , ObjectTypes.Table()
                , retrieveIDsFromDatabase: false
                , assert: false
                )
        {
            ConstructNewMembers();
            if (!retrieveIDsFromDatabase.HasValue || retrieveIDsFromDatabase.Value)
                RetrieveIDsFromDatabase(false);
            CompleteConstructionIfType(typeof(TableMutable), retrieveIDsFromDatabase, assert);
        }

        /***************************
         * DataProcObjectMutable method overrides
        ***************************/

        public override bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(base.PassesSelfAssertion());
            }
            return true;
        }

        public override IDataProcObject GetSafeReference(bool assert)
        {
            return new CascadingForeignKeyRegistryMutable(
                Name
                , GetSqlTranslatorSafely()
                , GetSchemaSafely()
                , NameWhenPrefixed
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        protected override void ConstructAfterConstruction()
        {
            ConstructAfterConstruction(
                name: null
                , sqlTranslator: null
                , schema: null
                , nameWhenPrefixed: null
                , id: null
                );
        }

        protected virtual void ConstructAfterConstruction(
            string name
            , ISqlTranslator sqlTranslator
            , ISchema schema
            , string nameWhenPrefixed
            , int? id
            )
        {
            base.ConstructAfterConstruction(
                name: name
                , sqlTranslator: sqlTranslator
                , schema: schema
                , objectType: ObjectTypes.Table()
                , owner: null
                , allowsUpdates: true
                , allowsDeletes: true
                , columns: null
                , indexes: null
                , hasIdentityColumn: true
                , identityColumn: null
                , primaryKey: null
                , uniqueKeys: null
                , foreignKeys: null
                , cascadingForeignKeyRegistry: null
                , triggers: null
                , checkConstraints: null
                , defaultConstraints: null
                , isAudited: false
                , auditTable: null
                , publishesChanges: false
                , publishedChangesTable: null
                , changeSubscriberRegistry: null
                , parentTable: null
                , parentInsertColumnListExpressions: null
                , canHaveChildren: false
                , leavesTable: null
                , maintainChildVablesTable: false
                , childVablesTable: null
                , hasIDColumn: false
                , idColumn: null
                , hasInputterColumn: false
                , inputterColumn: null
                , validInputtersTable: null
                , hasLastUpdateDtmColumn: false
                , hasLastUpdaterColumn: false
                , nameWhenPrefixed: null
                , id: default(int?)
                );
            ConstructNewMembers();
        }

        private void ConstructNewMembers( bool forceUpdates = true )
        {
            ISqlTranslator sqlT = SqlTranslator;
            if (sqlT == null)
                throw new ConstructionRequiresSqlTranslatorException(
                    this
                    , typeof(TableMutable).FullName
                    );
            // If something is no longer at the default, don't change it
            //   unless the forceUpdates flag is switched on
            if (forceUpdates || Name == null || Name.Length == 0)
                SetName(null, false);
            if (forceUpdates || Columns == null || Columns.Count() == 0)
            {
                SetColumns(new Dictionary<string, Tuple<int, IColumn>>());
                AddOrOverwriteColumn(
                    new IdentityColumnMutable(
                        "id"
                        , sqlT
                        , this
                        , retrieveIDsFromDatabase: false
                        , assert: false
                        )
                    );
                AddOrOverwriteColumn(
                    new TableColumnMutable(
                        "constrainedTableNameWithSchema"
                        , sqlT.SysnameTypeSql
                        , sqlT
                        , this
                        , isNullable: false
                        , isToBeTrimmed: true
                        , isImmutable: true
                        , constraintTag: "CTN"
                        , retrieveIDsFromDatabase: false
                        , assert: false
                        )
                    );
                AddOrOverwriteColumn(
                    new TableColumnMutable(
                        "constrainedTableColumnList"
                        , sqlT.UnicodeStringTypeSql + "(1000)"
                        , sqlT
                        , this
                        , isNullable: false
                        , isToBeTrimmed: true
                        , isImmutable: true
                        , constraintTag: "CTCL"
                        , retrieveIDsFromDatabase: false
                        , assert: false
                        )
                    );
                AddOrOverwriteColumn(
                    new TableColumnMutable(
                        "masterDataTableNameWithSchema"
                        , sqlT.SysnameTypeSql
                        , sqlT
                        , this
                        , isNullable: false
                        , isToBeTrimmed: true
                        , isImmutable: true
                        , constraintTag: "MDTN"
                        , retrieveIDsFromDatabase: false
                        , assert: false
                        )
                    );
                AddOrOverwriteColumn(
                    new TableColumnMutable(
                        "masterDataTableColumnList"
                        , sqlT.UnicodeStringTypeSql + "(1000)"
                        , sqlT
                        , this
                        , isNullable: false
                        , isToBeTrimmed: true
                        , isImmutable: true
                        , constraintTag: "MDTCL"
                        , retrieveIDsFromDatabase: false
                        , assert: false
                        )
                    );
                AddOrOverwriteColumn(
                    new TableColumnMutable(
                        "placeInDeleteSequence"
                        , "int"
                        , sqlT
                        , this
                        , isNullable: false
                        , isToBeTrimmed: false
                        , isImmutable: false
                        , constraintTag: "PIDS"
                        , retrieveIDsFromDatabase: false
                        , assert: false
                        )
                    );
                AddOrOverwriteColumn(
                    new TableColumnMutable(
                        "cascadeOnDelete"
                        , sqlT.BooleanTypeSql()
                        , sqlT
                        , this
                        , isNullable: false
                        , isToBeTrimmed: false
                        , isImmutable: false
                        , constraintTag: "COD"
                        , defaultConstraint:
                            new DefaultConstraintMutable(
                                name: null
                                , sqlTranslator: sqlT
                                , table: this
                                , constrainedColumn: null
                                , defaultValueText: sqlT.TrueSql()
                                , retrieveIDsFromDatabase: false
                                , assert: false
                                )
                        , retrieveIDsFromDatabase: false
                        , assert: false
                        )
                    );
                AddOrOverwriteColumn(
                    new TableColumnMutable(
                        "cascadeOnUpdate"
                        , sqlT.BooleanTypeSql()
                        , sqlT
                        , this
                        , isNullable: false
                        , isToBeTrimmed: false
                        , isImmutable: false
                        , constraintTag: "COU"
                        , defaultConstraint:
                            new DefaultConstraintMutable(
                                name: null
                                , sqlTranslator: sqlT
                                , table: this
                                , constrainedColumn: null
                                , defaultValueText: sqlT.FalseSql()
                                , retrieveIDsFromDatabase: false
                                , assert: false
                                )
                        , retrieveIDsFromDatabase: false
                        , assert: false
                        )
                    );
                SetHasLastUpdateDtmColumn(true, false, false);
                SetHasLastUpdaterColumn(true, false, false);
                Dictionary<string,Tuple<int,ITableColumn >> columns = TableColumns;
                SetPrimaryKey(
                    new NonnullablePrimaryKeyMutable(
                        constrainedColumn: columns["id"].Item2
                        , columnNames: new List<string> { "id" }
                        , retrieveIDsFromDatabase: false
                        , assert: false
                        )
                    );
                AddOrOverwriteUniqueKey(
                    new UniqueKeyMutable(
                        name: sqlT.TableConstraintName(
                            "_lpk", Name
                            )
                        , objectType: ObjectTypes.UniqueKey()
                        , physicalObjectType: ObjectTypes.UniqueKey()
                        , columnNames: new List<string> { "constrainedTableNameWithSchema", "constrainedTableColumnList", "masterDataTableNameWithSchema" }
                        , retrieveIDsFromDatabase: false
                        , assert: false
                        )
                    );
                AddOrOverwriteIndex(
                    new IndexMutable(
                        Name + "_iMDTNMDTCLPIDS"
                        , sqlT
                        , this
                        , ObjectTypes.Index()
                        , ObjectTypes.Index()
                        , columnNames: new List<string> { "masterDataTableNameWithSchema", "masterDataTableColumnList", "placeInDeleteSequence" }
                        , retrieveIDsFromDatabase: false
                        , assert: false
                        )
                    );
            }
        }

        /***************************
         * ObjectInSchemaMutable method overrides
        ***************************/

        public override IObjectInSchema GetCopyForParentObject(
            IDatabaseObject parentObject
            , bool? retrieveIDsFromDatabase = default(bool?)
            , bool assert = true
            )
        {
            RejectIfInvalidParentObjectType(parentObject);
            return new CascadingForeignKeyRegistryMutable(
                Name
                , SqlTranslator
                , Schema
                , NameWhenPrefixed
                , ID
                , retrieveIDsFromDatabase: false
                , assert: assert
                );
        }

        /***************************
         * ICascadingForeignKeyRegistry properties and methods
        ***************************/

        public List<ICascadingForeignKeyRelationship> RelationshipsList(
            string tableNameWithSchema
            , bool actionIsDelete
            , IConnection conn
            )
        {
            string trueSql = SqlTranslator.TrueSql();
            List<ICascadingForeignKeyRelationship> retval
                = new List<ICascadingForeignKeyRelationship>();
            List<string[]> output
                = Database.SqlExecuteReader(
                    ""
                    + "select distinct \n"
                    + "       placeInDeleteSequence \n"
                    + "     , masterDataTableColumnList \n"
                    + "     , constrainedTableNameWithSchema \n"
                    + "     , constrainedTableColumnList \n"
                    + "     , cascadeOnDelete \n"
                    + "     , cascadeOnUpdate \n"
                    + "  from " + FullName + " with (nolock) \n"
                    + " where masterDataTableNameWithSchema = '" + tableNameWithSchema.Replace("'", "''") + "'"
                    + "   and " + ( actionIsDelete ? "cascadeOnDelete" : "cascadeOnUpdate" ) + " = " + trueSql
                    + " order by placeInDeleteSequence"
                    );
            foreach (string[] record in output)
                retval.Add(
                    new CascadingForeignKeyRelationship(
                        tableNameWithSchema
                        , record[1]
                        , Int32.Parse(record[0])
                        , record[2]
                        , record[3]
                        , record[4] == trueSql
                        , record[5] == trueSql
                        )
                    );
            return retval;
        }

        public bool HasAtLeastOneRelationship(
            string tableNameWithSchema
            , bool actionIsDelete
            , IConnection conn
            )
        {
            string trueSql = SqlTranslator.TrueSql();
            return
                Database.SqlExecuteReader(
                    ""
                    + "select top(1) 1 \n"
                    + "  from " + FullName + " with (nolock) \n"
                    + " where masterDataTableNameWithSchema = '" + tableNameWithSchema.Replace("'", "''") + "'"
                    + "   and " + (actionIsDelete ? "cascadeOnDelete" : "cascadeOnUpdate") + " = " + trueSql
                    + " order by placeInDeleteSequence"
                    ).Count > 0;
        }

        public void RegisterRelationship(
            IForeignKey fk
            , int placeInDeleteSequence
            , IConnection conn
            )
        {
            string constrainedColumnList = "";
            string masterDataColumnList = "";
            foreach (KeyValuePair<string,string> pair in fk.KeyReferenceColumnMappings)
            {
                constrainedColumnList += pair.Key + ", ";
                masterDataColumnList += pair.Value + ", ";
            }
            constrainedColumnList
                = constrainedColumnList
                .Substring(0, constrainedColumnList.Length - 1)
                .Trim()
                .Replace("'", "''");
            masterDataColumnList
                = masterDataColumnList
                .Substring(0, masterDataColumnList.Length - 1)
                .Trim()
                .Replace("'", "''");
            Database.SqlExecuteNonQuery(
                ""
                + "merge into " + FullName + " target \n"
                + "using      " + SqlTranslator.SingletonVableSql + " singleton \n"
                + "             on constrainedTableNameWithSchema = '" + fk.Table.FullName.Replace("'", "''") + "' \n"
                + "            and constrainedTableColumnList = '" + constrainedColumnList + "' \n"
                + "            and masterDataTableNameWithSchema = '" + fk.ReferencedTable.FullName.Replace("'", "''") + "' \n"
                + "when matched then \n"
                + "update \n"
                + "   set placeInDeleteSequence = " + placeInDeleteSequence.ToString() + " \n"
                + "     , masterDataColumnList = '" + masterDataColumnList + "' \n"
                + "when not matched then \n"
                + "insert ( \n"
                + "       constrainedTableNameWithSchema \n"
                + "     , constrainedTableColumnList \n"
                + "     , masterDataTableNameWithSchema \n"
                + "     , masterDataColumnList \n"
                + "     , placeInDeleteSequence \n"
                + "       ) \n"
                + "values ( \n"
                + "       '" + fk.Table.FullName.Replace("'", "''") + "' \n"
                + "     , '" + constrainedColumnList + "' \n"
                + "     , '" + fk.ReferencedTable.FullName.Replace("'", "''") + "' \n"
                + "     , '" + masterDataColumnList + "' \n"
                + "     , " + placeInDeleteSequence.ToString() + " \n"
                + "       ) \n"
                + ";"
                , conn
                );
        }
        public void DeregisterRelationship(
            string constrainedTableNameWithSchema
            , string constrainedColumnList
            , string masterDataTableNameWithSchema
            , IConnection conn
            )
        {
            Database.SqlExecuteNonQuery(
                ""
                + "delete " + FullName + " \n"
                + " where constrainedTableNameWithSchema = '" + constrainedTableNameWithSchema.Replace("'", "''") + "' \n"
                + "   and constrainedTableColumnList = '" + constrainedColumnList.Replace("'", "''") + "' \n"
                + "   and masterDataTableNameWithSchema = '" + masterDataTableNameWithSchema.Replace("'", "''") + "' \n"
                + ";"
                , conn
                );
        }

    }

    /// <summary>
    /// Boilerplate implementation of ICascadingForeignKeyRegistry without variability.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>DataProcObjectImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// <item>ObjectInSchemaImmutable method overrides
    ///   <list type="bullet">
    ///   <item>GetReferenceForParentObject, always unless abstract class.</item>
    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// <remarks>
    public class CascadingForeignKeyRegistryImmutable : TableImmutable, ICascadingForeignKeyRegistry
    {
        private ICascadingForeignKeyRegistry Delegee { get { return (ICascadingForeignKeyRegistry)_delegee; } }

        //Disable default constructor
        protected CascadingForeignKeyRegistryImmutable() : base() { }

        public CascadingForeignKeyRegistryImmutable(
            string name
            , ISqlTranslator sqlTranslator = null
            , ISchema schema = null
            , string nameWhenPrefixed = null
            , int? id = default(int?)
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            )
        {
            _delegee
                = new CascadingForeignKeyRegistryMutable(
                    name
                    , sqlTranslator
                    , schema
                    , nameWhenPrefixed
                    , id
                    , retrieveIDsFromDatabase
                    , assert: false
                    );
            CompleteConstructionIfType(typeof(CascadingForeignKeyRegistryImmutable), false, true);
        }

        public CascadingForeignKeyRegistryImmutable(ICascadingForeignKeyRegistry delegee, bool assert = true)
        {
            RejectIfInvalidDelegeeType(delegee);
            _delegee = delegee;
            CompleteConstructionIfType(typeof(CascadingForeignKeyRegistryImmutable), false, true);
        }

        // IDataProcObject method overrides

        protected override Type ValidTypeForDelegee { get { return typeof(ICascadingForeignKeyRegistry); } }

        // IDatabaseObject method overrides

        public override IDatabaseObject GetReferenceForOwner(IDataProcObject owner, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            RejectIfInvalidOwnerType(owner);
            return new CascadingForeignKeyRegistryImmutable(
                (ICascadingForeignKeyRegistry)Delegee.GetReferenceForOwner(owner, retrieveIDsFromDatabase, false)
                , assert
                );
        }

        public override IDatabaseObject GetCopyForOwner(IDataProcObject owner, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            RejectIfInvalidOwnerType(owner);
            return new CascadingForeignKeyRegistryImmutable(
                (ICascadingForeignKeyRegistry)Delegee.GetCopyForOwner(owner, retrieveIDsFromDatabase, false)
                , assert
                );
        }

        // ObjectInSchemaImmutable method overrides

        public override IObjectInSchema GetReferenceForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            RejectIfInvalidParentObjectType(parentObject);
            return new CascadingForeignKeyRegistryImmutable(
                (ICascadingForeignKeyRegistry)Delegee.GetReferenceForParentObject(parentObject, retrieveIDsFromDatabase, false)
                , assert
                );
        }

        public override IObjectInSchema GetCopyForParentObject(IDatabaseObject parentObject, bool? retrieveIDsFromDatabase = default(bool?), bool assert = true)
        {
            RejectIfInvalidParentObjectType(parentObject);
            return new CascadingForeignKeyRegistryImmutable(
                (ICascadingForeignKeyRegistry)Delegee.GetCopyForParentObject(parentObject, retrieveIDsFromDatabase, false)
                , assert
                );
        }

        // ICascadingForeignKeyRegistry properties and methods

        public List<ICascadingForeignKeyRelationship> RelationshipsList(
            string tableNameWithSchema
            , bool actionIsDelete
            , IConnection conn
            )
        {
            return Delegee.RelationshipsList(
                tableNameWithSchema
                , actionIsDelete
                , conn
                );
        }
        public bool HasAtLeastOneRelationship(
            string tableNameWithSchema
            , bool actionIsDelete
            , IConnection conn
            )
        {
            return Delegee.HasAtLeastOneRelationship(
                tableNameWithSchema
                , actionIsDelete
                , conn
                );
        }

    }

}
