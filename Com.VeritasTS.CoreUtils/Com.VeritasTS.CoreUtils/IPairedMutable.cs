﻿using System;
using System.Diagnostics;
using System.Reflection;

namespace Com.VeritasTS.CoreUtils
{
    /// <summary>
    /// An IMutable class that is tightly associated
    /// with a single IImmutable class in such a way that 
    /// instances of the two classes can equal each other.
    /// </summary>
    /// <remarks>
    /// Contract requirements: members of the
    /// two classes can equal each other. This
    /// is tested with the 
    /// AtLeastOnePairedMutableObjectEqualsAtLeastOnePairedImmutableObject
    /// method of the BaseIPairedMutable tester.
    /// </remarks>
    public interface IPairedMutable : IMutable
    {

        /// <summary>
        /// Returns the IPairedImmutable class
        /// that is paired with this IPairedMutable.
        /// </summary>
        /// <remarks>
        /// <list type="table">
        /// <listheader><term>Specification</term><description>Test method in BaseIMutableTester class</description></listheader>
        /// <item>
        /// <term>PairedImmutableType is assignable to IPairedImmutable.</term>
        /// <description>PairedImmutableClassIsIPairedImmutable</description>
        /// </item>
        /// </list>
        /// </remarks>
        /// <returns>Paired immutable type of this particular
        /// IPairedMutable class.</returns>
        Type PairedImmutableType { get; }

    }

    /// <summary>
    /// Boilerplate implementation of IPairedMutable,
    /// in which PairedImmutableType is deduced by naming
    /// convention.
    /// </summary>
    /// <remarks>
    /// The class also implements ISelfAsserter, but
    /// not ISafeMemberOfInvariantOwner.
    /// <para>
    /// Descendant classes should rarely if ever
    /// need to override default methods, except perhaps
    /// to improve performance when very high performance
    /// is required.
    /// </para>
    /// </remarks>
    public abstract class PairedMutable : SelfAsserterMutable, IPairedMutable
    {

        /********************************
         * 
         * Private members and methods
         * 
         *******************************/

        /// <summary>
        /// This class's full name, for identification in 
        /// reporting code errors.
        /// </summary>
        /// <remarks> 
        /// Note that this is NOT the actual type of the
        /// instance, because the purpose of identifying
        /// the class in the error messages is to tell
        /// support personnel where to find the code.
        /// And the code generating the errors is 
        /// in the Immutable class definition, even if 
        /// the actual object instantiated is a fourth-generation
        /// descendant.</remarks>
        private string FullTypeNameForCodeErrors { get { return typeof(PairedMutable).FullName; } }

        /********************************
         * 
         * IPairedMutable methods
         * 
         *******************************/

        /// <summary>
        /// Implements
        /// <see cref="IPairedMutable.PairedImmutableType"/>.
        /// </summary>
        /// <remarks> 
        /// Specifications are unchanged from 
        /// <see cref="IPairedMutable.PairedImmutableType"/>.
        /// </remarks>
        public virtual Type PairedImmutableType
        {
            get
            {
                string nameOfThisType = GetType().FullName;
                string nameOfPairedType
                    = nameOfThisType.Substring(
                        0
                        , nameOfThisType.Length - 7
                        )
                        + "Immutable";
                return Type.GetType(nameOfPairedType);
            }
        }

    }

}
