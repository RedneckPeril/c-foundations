﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.CoreUtils
{
    /// <summary>
    /// A class whose state can be changed 
    /// even after it is fully constructed.
    /// </summary>
    /// <remarks>
    /// Contract requirements: class cannot
    /// also implement IImmutable.
    /// </remarks>
    public interface IMutable
    {
    }
}
