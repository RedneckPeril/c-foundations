﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.CoreUtils
{

    /// <summary>
    /// With this interface we make explicit the contract
    /// a Veritas developer enters into when
    /// implementing the ICloneable interface.
    /// </summary>
    /// <remarks>
    /// The IVTSCloneable contract is as follows:
    /// <p>
    /// <list type="number">
    /// <item>If the class is (on a per-thread basis at least) a singleton
    /// class, then x.clone() == x; otherwise, x.clone() != x.
    /// Singletonness is determined by the boolean property 
    /// ObjectIsSingletonPerThread (for which a getter is required
    /// by the interface, but not necessarily a setter).</item>
    /// <item>x.clone().equals( x ).</item>
    /// <item>x.clone().getClass() == x.getClass().</item>
    /// <item>If the boolean property CloneProtectsInvariance is true,
    /// then x.Clone() = x.GetCloneWhenInvariant(). In this case, either
    /// the class is not stateful, or else the members are all primitives,
    /// or else the object produced by Clone() points to copies of
    /// private members rather than providing handles to the original
    /// object's members. The interface requires a getter, but not a setter,
    /// for CloneProtectsInvariance. It also requires the method
    /// getCloneWhenInvariant(), so that the object can be used as
    /// a member of an invariant object even if the object itself is
    /// designed to allow variance.</item>
    /// </list>
    /// </remarks>
    interface IVTSCloneable
    {

        /// <summary>
        /// Creates and hands over to the caller an appropriately
        /// deep (or shallow) copy of this, in accordance
        /// with the general IVTSCloneable contract.
        /// </summary>
        /// <remarks>
        /// If the class implements ObjectIsSingletonPerThread
        /// as constant true, then the Clone() and 
        /// GetCloneWhenInvariant() methods should 
        /// ALWAYS be implemented as follows:
        /// <para>
        /// <code>
        /// public object Clone() { return this; }
        /// public object CloneWhenInvariant() { return this; }
        /// </code>
        /// </para>
        /// <para>Otherwise, the Clone() method should be implemented in a way
        /// that deals properly with non-constant ObjectIsSingletonPerThread
        /// and CloneProtectsInvariance properties; that includes a try-catch
        /// block around an initial call to super.Clone() (in order to
        /// eliminate the compiler error for calling Object.clone() within
        /// a method that does not throw CloneNotSupportedExceptions),
        /// and that is appropriately deep. Meanwhile the GetCloneWhenInvariant() 
        /// method should be a simple call to Clone() when the
        /// CloneProtectsInvariance flag is constant true. Otherwise it should
        /// begin by calling Clone() and then include an additional section
        /// to replace any handles to original-object private members with
        /// handles to copies thereof.</para>
        /// Return: object (castable to same class as this) =
        /// copy of this.
        /// </remarks>
        /// <returns>object (castable to same class as this) =
        /// copy of this.</returns>
        object Clone();

        /// <summary>
        /// Flag set to true if the object is stateless,
        /// otherwise false.
        /// </summary>
        bool IsStateless { get; }

        /// <summary>
        /// Flag set to true if the object is invariant once fully constructed,
        /// otherwise false.
        /// </summary>
        bool IsInvariant { get; }

        /// <summary>
        /// Flag set to true if the object is a singleton class
        /// (at least within any given thread), otherwise false.
        /// </summary>
        bool IsSingletonPerThread { get; }

        /// <summary>
        /// Flag set to true if clone() returns a clone
        /// "deep enough" to protect threadsafety
        /// when the object is used within an object that
        /// is meant to be invariant. In effect
        /// this means that Clone() returns the same
        /// thing as GetCloneWhenInvariant().
        /// </summary>
        /// <remarks>
        /// The default logic is that stateless or invariant
        /// objects provide safe clones. This can be implemented
        /// in an abstract root class as:
        /// <code>
        /// public virtual bool CloneProtectsInvariance => IsStateless | IsInvariant;
        /// </code>
        /// </remarks>
        bool CloneProtectsInvariance { get; }

        /// <summary>
        /// Returns a clone "deep enough" to protect threadsafety
        /// when the object is used within an object that
        /// is meant to be invariant.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="CannotProduceInvariantCloneException">
        /// This exception is thrown if for some reason the object
        /// cannot create a threadsafe clone.
        IVTSCloneable GetCloneWhenInvariant();

    }
}
