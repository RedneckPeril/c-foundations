﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.CoreUtils
{
    /// <summary>
    /// Simple exception class for when an IVTSCloneable
    /// is asked to make a threadsafe clone of itself
    /// and for some reason can't do it.
    /// </summary>
    class CannotProduceInvariantCloneException: Exception
    {
        /// <summary>
        /// Default constructor, passes a default message
        /// "Cannot produce invariant clone." to the base
        /// constructor.
        /// </summary>
        public CannotProduceInvariantCloneException() :
            base("Cannot produce invariant clone.")
        { }
        /// <summary>
        /// Standard Exception constructor with message argument, doing
        /// nothing but forwarding to the base constructor.
        /// </summary>
        /// <param name="message">The exception message to be used.</param>
        public CannotProduceInvariantCloneException(string message) :
            base(message)
        { }
        /// <summary>
        /// Standard two-argument Exception constructor, doing
        /// nothing but forwarding to the base constructor.
        /// </summary>
        /// <param name="message">The exception message to be used.</param>
        /// <param name="innerException">The Exception instance that caused the current exception.</param>
        public CannotProduceInvariantCloneException(string message, Exception innerException) :
            base(message, innerException)
        { }
    }
}
