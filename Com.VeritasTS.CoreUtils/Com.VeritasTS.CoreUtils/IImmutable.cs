﻿using System;
using System.Diagnostics;
using System.Reflection;

namespace Com.VeritasTS.CoreUtils
{
    /// <summary>
    /// A class whose state cannot be changed 
    /// once it is fully constructed.
    /// </summary>
    /// <remarks>
    /// Contract requirements:
    /// 1. No method can export
    /// any reference to a member object
    /// unless that member object itself
    /// is an IImmutable or a primitive type.
    /// 2. Once an object has been flagged as
    /// being fully constructed
    /// (<see cref="IsFullyConstructed"/>,
    /// no methods can change its state.
    /// 3. Class cannot also implement IMutable.
    /// </remarks>
    public interface IImmutable : ISafeMemberOfImmutableOwner
    {

        /// <summary>
        /// States whether the object has been
        /// fully constructed and therefore can
        /// be trusted to be immutable.
        /// </summary>
        /// <remarks>
        /// Note that this method shares a name with 
        /// <see cref="ISelfAsserter.IsFullyConstructed"/>;
        /// when a class implements both ISelfAsserter
        /// and IImmutable, then IsFullyConstructed is
        /// true if and only if the object is trusted to
        /// be immutable <em>and</em> it will pass all
        /// self-assertions. The single method is used for
        /// both since both amount to saying, "The instance
        /// can now be trusted to honor its class constract,"
        /// immutability being part of the IImmutable class
        /// contract.
        /// <para>
        /// This property is read-only, though its value
        /// in non-immutable: IImmutables can be moved from 
        /// false to true (though
        /// not the other direction) by 
        /// <see cref="MarkAsFullyConstructed(Type)"/>.
        /// </para>
        /// <para>There are no testable class invariants.</para>
        /// </remarks>
        /// <returns>True if fully constructed, else false.</returns>
        bool IsFullyConstructed { get; }

        /// <summary>
        /// Marks an object as having been fully constructed,
        /// and therefore immutable.
        /// </summary>
        /// <remarks>
        /// If testableType is not null, then MarkAsFullyConstructed
        /// has no effect unless the object is an instance of 
        /// precisely the specified type. 
        /// If we have a class A and a class B: A, then
        /// calling MarkAsFullyConstructed(typeof(A)) on an instance
        /// of class B will have no effect.
        /// <para>
        /// The method returns a boolean so that it can be housed
        /// within a 
        /// <see cref="System.Diagnostics.Debug.Assert(bool)"/> 
        /// call, thus disappearing from the code entirely 
        /// under Production compilation if so desired.
        /// </para>
        /// <list type="table">
        /// <listheader><term>Specification</term><description>Test method in BaseIImmutableTester class</description></listheader>
        /// <item>
        /// <term>Where IsFullyConstructed is false and no assertion failures
        /// are lurking, MarkAsFullyConstructed(null) 
        /// returns true and causes IsFullyConstructed to become true.</term>
        /// <description>MarkAsFullyConstructedWithNullParamCompletesConstruction</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true and no assertion failures
        /// are lurking, MarkAsFullyConstructed(null) 
        /// returns true and leaves IsFullyConstructed set to true.</term>
        /// <description>MarkAsFullyConstructedWithNullParamLeavesConstructionComplete</description>
        /// </item>
        /// <item>
        /// <term>Where object is of type X and IsFullyConstructed is false and no assertion failures
        /// are lurking, MarkAsFullyConstructed(X) returns true and causes IsFullyConstructed to become true.</term>
        /// <description>MarkXAsFullyConstructedWithTypeOfXParamCompletesConstruction</description>
        /// </item>
        /// <item>
        /// <term>Where object is of type X and IsFullyConstructed is true and no assertion failures
        /// are lurking, MarkAsFullyConstructed(X) returns true and leaves IsFullyConstructed set to true.</term>
        /// <description>MarkXAsFullyConstructedWithTypeOfXParamLeavesConstructionComplete</description>
        /// </item>
        /// <item>
        /// <term>Where object is of type Y descended from X and IsFullyConstructed is false and no assertion failures
        /// are lurking, MarkAsFullyConstructed(X) returns true and leaves IsFullyConstructed set to false.</term>
        /// <description>MarkYAsFullyConstructedWithTypeOfXParamLeavesConstructionIncomplete</description>
        /// </item>
        /// <item>
        /// <term>Where object is of type Y descended from X and IsFullyConstructed is true and no assertion failures
        /// are lurking, MarkAsFullyConstructed(X) returns true and leaves IsFullyConstructed set to true.</term>
        /// <description>MarkYAsFullyConstructedWithTypeOfXParamLeavesConstructionIncomplete</description>
        /// </item>
        /// </list>
        /// </remarks>
        /// <param name="testableType">If testableType is non-null,
        /// and not precisely the type of this instance, then
        /// method is a null-op. Otherwise 
        /// <see cref="IsFullyConstructed"/> 
        /// is set to true if it is not already true.</param>
        /// <returns>True if fully constructed, else false.</returns>
        bool MarkAsFullyConstructed(Type testableType = null);

    }

    /// <summary>
    /// Boilerplate implementation of IImmutable, in
    /// which a wrapper is placed around a private
    /// delegee object and the wrapper is made as 
    /// nearly invisible as possible.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Immutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public abstract class Immutable : IImmutable
    {

        /********************************
         * 
         * Private members and methods
         * 
         *******************************/

        /// <summary>
        /// This class's full name, for identification in 
        /// reporting code errors.
        /// </summary>
        /// <remarks> 
        /// Note that this is NOT the actual type of the
        /// instance, because the purpose of identifying
        /// the class in the error messages is to tell
        /// support personnel where to find the code.
        /// And the code generating the errors is 
        /// in the Immutable class definition, even if 
        /// the actual object instantiated is a fourth-generation
        /// descendant.</remarks>
        private string FullTypeNameForCodeErrors { get { return typeof(Immutable).FullName; } }

        /// <summary>
        /// This is the object that actually 
        /// contains the business logic, and 
        /// is wrapped within (and completely
        /// private to) the Immutable object.
        /// </summary>
        protected object _delegee;

        /// <summary>
        /// Construct from delegee.
        /// </summary>
        /// <remarks>
        /// It is of absolutely <em>paramount</em>
        /// importance that the code that calls the
        /// constructor, having built the delegee
        /// object, discards <em>all</em> references
        /// to the delegee object <em>immediately</em>
        /// after calling the constructor, in such a
        /// way as to allow absolutely no reference
        /// to the delegee to escape. It is a fundamental
        /// assumption of the implementation that the
        /// delegee object is 100% private to the 
        /// Immutable object -- it is, in fact, a
        /// class invariant, but one which the Immutable
        /// object itself can in no way enforce given
        /// this hole in the encapsulation. The hole
        /// is provided simply because the convenience
        /// is worth the hole.
        /// <list type="table">
        /// <listheader><term>Specification</term><description>Test method in BaseIImmutableTester class</description></listheader>
        /// <item>
        /// <term>Where delegee param is null, constructor throws NullArgumentException.</term>
        /// <description>ConstructionFromNullDelegeeCausesNullArgumentException</description>
        /// </item>
        /// <item>
        /// <term>Where delegee param is not a valid delegee type, constructor throws
        /// BadDelegeeOfImmutableTypeException.</term>
        /// <description>ConstructionFromUnacceptableDelegeeCausesBadDelegeeOfImmutableTypeException</description>
        /// </item>
        /// <item>
        /// <term>Where delegee param is of an acceptable type, constructor completes
        /// successfully with IsFullyConstructed = true.</term>
        /// <description>ConstructionFromValidDelegeeFullyCompletesConstruction</description>
        /// </item>
        /// </list>
        /// </remarks>
        /// <param name="delegee">The object that
        /// will become the Immutable's delegee.</param>
        public Immutable(object delegee)
        {
            RejectIfInvalidDelegeeType(delegee);
            _delegee = delegee;
        }

        // Object method overrides

        /// <summary>
        /// Overrides 
        /// <see cref="object.Equals(object)"/>
        /// so that the
        /// Immutable wrapper becomes invisible to
        /// the Equals method -- that is, an Immutable
        /// is equal to another object iff its delegee
        /// is equal to that object.
        /// </summary>
        /// <remarks>
        /// Note that for any Immutable <em>x</em>,
        /// x.Equals(x._delegee). Therefore, since equality
        /// has to go both ways, any class that is to be used
        /// as an Immutable's delegee, must be defined so that,
        /// for an object d of class MutableD meant to be
        /// capable of serving as a delegee for class ImmutableD,
        /// d.Equals( new ImmutableD(d) ). Usually this is accomplished 
        /// using member-matching equality. 
        /// <para>
        /// If the _delegee member is null, then the object is not
        /// in a constructed state and the method should not have been
        /// called. In development compilation mode the assumption
        /// that _delegee is not null is asserted.
        /// </para>
        /// <list type="table">
        /// <listheader>
        /// <term>Specification ("TC" = TestableClass, "DC" = delegee class)</term>
        /// <description>Test method in BaseIImmutableTester class</description>
        /// </listheader>
        /// <item><term>!x.Equals(null)</term><description>ImmutableNotEqualsNull</description></item>
        /// <item><term>x.Equals(x)</term><description>ImmutableEqualsSelf</description></item>
        /// <item><term>For DC x, new TC(x).Equals(x)</term><description>ImmutableEqualsDelegee</description></item>
        /// <item><term>For DC x, x.Equals(new TC(x))</term><description>DelegeeEqualsImmutable</description></item>
        /// <item><term>For DC x and y not mutually equal, !x.Equals(new TC(y))</term><description>UnequalDelegeesProduceUnequalImmutables</description></item>
        /// </list>
        /// </remarks>
        /// <param name="obj">The object to which this is to be compared.</param>
        /// <returns>True if the two objects are considered equal.</returns>
        public override bool Equals(object obj)
        {
            Debug.Assert(
                _delegee != null
                , "Immutable._delegee cannot be null once "
                + "construction is complete, and Immutable.Equals(obj) "
                + "should never be called before completion of construction."
                );
            return _delegee.Equals(obj);
        }

        /// <summary>
        /// Overrides 
        /// <see cref="object.GetHashCode"/>
        /// so that the
        /// Immutable wrapper becomes invisible to
        /// the GetHashCode method -- that is, an Immutable's
        /// hash code is always the same as its delegee's.
        /// </summary>
        /// <remarks>
        /// Note that this implies that an Immutable's hash code
        /// is always the same as its delegee's.
        /// <para>
        /// If the _delegee member is null, then the object is not
        /// in a constructed state and the method should not have been
        /// called. In development compilation mode the assumption
        /// that _delegee is not null is asserted.
        /// </para>
        /// <list type="table">
        /// <listheader>
        /// <term>Specification ("TC" = TestableClass, "DC" = delegee class)</term>
        /// <description>Test method in BaseIImmutableTester class</description>
        /// </listheader>
        /// <item>
        /// <term>For DC x, new TC(x).GetHashCode == x.GetHashCode()</term>
        /// <description>ImmutableHashCodeEqualsDelegeeHashCode</description>
        /// </item>
        /// </list>
        /// </remarks>
        /// <returns></returns>
        public override int GetHashCode()
        {
            Debug.Assert(
                _delegee != null
                , "Immutable._delegee cannot be null once "
                + "construction is complete, and Immutable.GetHashCode() "
                + "should never be called before completion of construction."
                );
            return _delegee.GetHashCode();
        }

        // IImmutable properties and methods

        /// <summary>
        /// Implements
        /// <see cref="IImmutable.IsFullyConstructed"/> .
        /// </summary>
        /// <remarks>There are no additions to the
        /// <see cref="IImmutable.IsFullyConstructed"/>  contract.</remarks>
        public abstract bool IsFullyConstructed { get; }

        /// <summary>
        /// Implements
        /// <see cref="IImmutable.MarkAsFullyConstructed(Type)"/> .
        /// </summary>
        /// <remarks>There are no additions to the
        /// <see cref="IImmutable.MarkAsFullyConstructed(Type)"/>  contract.</remarks>
        public abstract bool MarkAsFullyConstructed(Type testableType = null);

        // Additional validation method

        /// <summary>
        /// Throws an exception if someone attempts to construct an Immutable
        /// wrapped around a delegee of an ineligible class, or around
        /// a null delegee.
        /// </summary>
        /// <param name="candidate"></param>
        protected void RejectIfInvalidDelegeeType(object candidate)
        {
            if (candidate == null)
                throw new ArgumentNullException(
                    "candidate"
                    , "Caller attempted to call method "
                    + FullTypeNameForCodeErrors
                    + ".RejectIfInvalidDelegeeType(candidate) "
                    + "using a null value for the candidate "
                    + "parameter, which cannot be null."
                    );
            if (!ObjectIsValidTypeForDelegee(candidate, true))
                throw new BadDelegeeOfImmutableTypeException(
                    FullTypeNameForCodeErrors
                    , candidate.GetType().FullName
                    , ValidTypeForDelegee.FullName
                    );
        }

        /// <summary>
        /// Specifies a single type to which the delegee
        /// instance must be assignable.
        /// </summary>
        protected abstract Type ValidTypeForDelegee { get; }

        /// <summary>
        /// Specifies a single type to which the delegee
        /// instance must be assignable.
        /// </summary>
        protected bool ObjectIsValidTypeForDelegee(object candidate, bool throwException = false)
        {
            Type validType = ValidTypeForDelegee;
            bool retval = (candidate != null) && (validType.GetTypeInfo().IsAssignableFrom(candidate.GetType().GetTypeInfo()));
            return retval;
        }

        /********************************
         * 
         * ISafeMemberOfImmutableOwner methods
         * 
         *******************************/

        /// <summary>
        /// Implements
        /// <see cref="ISafeMemberOfImmutableOwner.GetSafeReference(bool)"/> 
        /// </summary>
        /// <remarks> 
        /// Where descendant class also implements
        /// <see cref="ISelfAsserter"/>, override so that
        /// before returning this, we self-assert if assert
        /// parameter so decrees.
        /// <list type="table">
        /// <listheader>
        /// <term>Specification ("TC" = TestableClass, "DC" = delegee class)</term>
        /// <description>Test method in BaseIImmutableTester class</description>
        /// </listheader>
        /// <item>
        /// <term>x == x.GetSafeReference().</term>
        /// <description>GetSafeReferenceReturnsSelf</description>
        /// </item>
        /// </list></remarks>
        public virtual ISafeMemberOfImmutableOwner GetSafeReference(bool assert = true) { return this; }

    }

    /// <summary>
    /// Exception thrown when a caller tries
    /// to construct an Immutable object wrapped
    /// around a delegee instance of an ineligible 
    /// type.
    /// </summary>
    public class BadDelegeeOfImmutableTypeException : Exception
    {

        private BadDelegeeOfImmutableTypeException() { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="immutableTypeName">The type
        /// of Immutable that was under construction.</param>
        /// <param name="badDelegeeTypeName">The type
        /// of delegee that was supplied inappropriately.</param>
        /// <param name="acceptableDelegeeTypeName">The type
        /// of delegee that is required.</param>
        /// <param name="message">The message
        /// to be embedded in the exception; null defaults
        /// to a standard default message.</param>
        public BadDelegeeOfImmutableTypeException(
            string immutableTypeName
            , string badDelegeeTypeName
            , string acceptableDelegeeTypeName
            , string message = null
            ) : base(
                (
                message != null
                ? message
                : "Cannot set the Delegee property of an object "
                + "of type "
                + immutableTypeName != null ? immutableTypeName : "[unknown]"
                + " to an "
                + "object of type "
                + badDelegeeTypeName != null ? badDelegeeTypeName : "[unknown]"
                + "; the parent object must be an object of type "
                + acceptableDelegeeTypeName + "."
                )
                )
        {
            ImmutableTypeName = immutableTypeName;
            BadDelegeeTypeName = badDelegeeTypeName;
            AcceptableDelegeeTypeName = acceptableDelegeeTypeName;
        }
        /// <summary>
        /// The specific type of Immutable
        /// that was under construction.
        /// </summary>
        public string ImmutableTypeName { get; }
        /// <summary>
        /// The specific type of the delegee
        /// that was supplied inappropriately.
        /// </summary>
        public string BadDelegeeTypeName { get; }
        /// <summary>
        /// The type of delegee
        /// that is required.
        /// </summary>
        public string AcceptableDelegeeTypeName { get; }
    }

    /// <summary>
    /// Exception thrown when a caller tries
    /// to construct an Immutable object wrapped
    /// around a null delegee.
    /// </summary>
    public class NullDelegeeException : Exception
    {

        private NullDelegeeException() { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="immutableTypeName">The type
        /// of Immutable that was under construction.</param>
        /// <param name="message">The message
        /// to be embedded in the exception; null defaults
        /// to a standard default message.</param>
        public NullDelegeeException(
            string immutableTypeName
            , string message = null
            ) : base(
                (message != null
                ? message
                : "Cannot set the Delegee property of an object "
                + "of type "
                + (immutableTypeName != null ? immutableTypeName : "[unknown]")
                + " to null."
                )
                )
        {
            ImmutableTypeName = immutableTypeName;
        }
        /// <summary>
        /// The specific type of Immutable
        /// that was under construction.
        /// </summary>
        public string ImmutableTypeName { get; }
    }
}
