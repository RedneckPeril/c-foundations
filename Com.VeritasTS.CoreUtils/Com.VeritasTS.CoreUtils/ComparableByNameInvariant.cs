﻿//using System;

//namespace Com.VeritasTS.CoreUtils
//{

//    /// <summary>
//    /// Simple abstract implementation of IComparableByName
//    /// interface, where class is not invariant (that is,
//    /// the value of the Name property may be changed
//    /// after construction.
//    /// </summary>
//    abstract public class ComparableByNameImmutable : ISelfAsserter, IComparableByName
//    {

//        /****************
//         *  No private members
//         ***************/

//        /****************
//         *  Constructors
//         ***************/

//        /// <summary>
//        /// Hides default constructor.
//        /// </summary>
//        protected ComparableByNameImmutable() { }

//        /// <summary>
//        /// Construct from name.
//        /// </summary>
//        public ComparableByNameImmutable( string name) { Name = name; }

//        /****************
//         *  Object methods overridden
//         ***************/

//        /// <summary>
//        /// Standard Equals method, overridden to be false
//        /// if the other object is not an object of the same
//        /// class as this and EqualsInName if it 
//        /// isn't.
//        /// </summary>
//        /// <returns>Truth of statement "other object is a ComparableByNameImmutable
//        /// equal to this in class and name."</returns>
//        public override bool Equals(Object obj)
//        {
//            if (obj == null) return false;
//            else if (!obj.GetType().Equals(this.GetType())) return false;
//            else return EqualsInName((ComparableByNameImmutable)obj);
//        }

//        /// <summary>
//        /// Standard GetHashCode method, overridden to be the Name's hash
//        /// code if Name is not null and 0 if it is.
//        /// </summary>
//        /// <returns>The hash code</returns>
//        public override int GetHashCode()
//        {
//            if (Name == null) return 0;
//            else return Name.GetHashCode();
//        }

//        /****************
//         *  ISelfAsserter methods
//         ***************/

//        /// <summary>
//        /// Asserts class invariants; as there are none at the base
//        /// class level, simply returns an instant true.
//        /// </summary>
//        /// <returns>
//        /// At base level, simply returns true. </returns>
//        public virtual bool PassesSelfAssertion(Type testableType = null) { return true; }

//        /****************
//         *  INamed properties via IComparableByName
//         ***************/

//        /// <summary>
//        /// Simple string property Name, trivially
//        /// gettable but not settable because the object is
//        /// invariant, at least with respect to the Name property.
//        /// </summary>
//        public string Name { get; }

//        /****************
//         *  IComparableByName methods
//         ***************/

//        /// <summary>
//        /// EqualsInName method required by the
//        /// IComparableByName interface.
//        /// </summary>
//        /// <remarks>Two null Names are considered equal,
//        /// and a null object is considered to have a null Name.</remarks>
//        /// <returns>Truth of statement "the two Names are equal."</returns>
//        public bool EqualsInName(IComparableByName other)
//        {
//            return CompareByNameTo(other) == 0;
//        }

//        /// <summary>
//        /// CompareByNameTo method required by the
//        /// IComparableByName interface.
//        /// </summary>
//        /// <remarks>Two null Names are considered equal,
//        /// and a null object is considered to have a null Name.</remarks>
//        /// <returns>0 if it is considered true that "the two Names are equal,"
//        /// -1 if this's Name is alphabetically prior to the other's Name,
//        /// and 1 if this's Name is alphabetically subsequent to the other's Name.</returns>
//        public int CompareByNameTo(IComparableByName other)
//        {
//            if (Name != null)
//            {
//                if (other == null) return -1;
//                else return Name.CompareTo(other.Name);
//            }
//            else if (other == null) return 0;
//            else if (other.Name == null) return 0;
//            else return 1;
//        }

//    }

//}
