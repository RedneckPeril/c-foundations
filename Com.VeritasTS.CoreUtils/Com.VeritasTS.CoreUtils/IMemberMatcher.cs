﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.CoreUtils
{

    /// <summary>
    /// Interface for a hierarchy of objects whose
    /// equality is determined primarily
    /// by whether a particular subset ofthe members match,
    /// even if the objects are of two different classes.
    /// </summary>
    /// <remarks>
    /// This is particularly useful when we come
    /// to having pairs of classes that can appear
    /// to switch back and forth between being mutable
    /// and immutable without thereby losing equality --
    /// that is to say, for example, a particular real-life
    /// widget could be represented most of the time
    /// as a WidgetMutable, but could be wrapped in
    /// a WidgetImmutable for use in a hash table
    /// and yet still be equal to exactly the same
    /// objects the WidgetMutable is equal to.
    /// <para>
    /// There are, roughly, two categories of IMemberMatchers.
    /// In the first category, instances of descendant classes
    /// are never equal to instances of ancestor classes.
    /// In this case, ordinarily, the only time instances of
    /// two different classes are considered equal is when
    /// one is an IPairedMutable and the other is of the corresponding
    /// IPairedImmutable class.
    /// </para>
    /// <para>
    /// In the second category, by contrast, instances of 
    /// descendant classes can be equal to instances of
    /// ancestor classes, so long as they do not additional
    /// members to be included in the determination of 
    /// inequality. There are a surprisingly large number
    /// of subtleties associated with this category, however --
    /// what if there are two descendant classes of a
    /// single ancestor class so that neither is the descendant
    /// of the other but neither has added new members, for example?
    /// Therefore we will not implement the second category until
    /// we find a true need for it.
    /// </para>
    /// <para>
    /// Note that 
    /// <see cref="object.Equals(object)"/> and 
    /// <see cref="object.GetHashCode"/> must always be overridden
    /// such that <see cref="object.Equals(object)"/> returns 
    /// TypeCanEqualThis(o.GetType) and MembersMatch(o). This will
    /// render mutable MemberMatchers unsuitable for use as keys
    /// in hashed collections, but then we expect that practically
    /// always any mutable MemberMatcher would come paired with
    /// a corresponding IImmutable MemberMatcher, an instance of which
    /// the mutable version could generate on demand for use as a hash
    /// key.
    /// </para>
    /// </remarks>
    public interface IMemberMatcher
    {

        /// <summary>
        /// Determines whether it is possible for an object of the specified
        /// type to be considered equal to this.
        /// </summary>
        /// <param name="type">
        /// The type of object to be compared if possible.
        /// </param>
        /// <returns>True if equality is possible else false.</returns>
        bool TypeCanEqualThis(Type type);

        /// <summary>
        /// Determines whether this and object <em>obj</em> match on all
        /// members that are relevant for determination of equality.
        /// </summary>
        /// <remarks>
        /// Note that stateless objects can be implemented as
        /// MemberMatchers; in this case it simply means that
        /// all instances of the class are considered equal to each
        /// other.
        /// </remarks>
        /// <param name="obj">The object to which this is to be compared.</param>
        /// <returns></returns>
        bool MembersMatch(object obj);
    }

}
