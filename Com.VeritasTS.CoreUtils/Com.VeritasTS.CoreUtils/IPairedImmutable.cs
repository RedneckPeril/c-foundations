﻿using System;
using System.Diagnostics;
using System.Reflection;

namespace Com.VeritasTS.CoreUtils
{
    /// <summary>
    /// An IImmutable class that is tightly associated
    /// with a single IMutable class in such a way that 
    /// instances of the two classes can equal each other.
    /// </summary>
    /// <remarks>
    /// Contract requirements: members of the
    /// two classes can equal each other. This
    /// is tested with the 
    /// AtLeastOnePairedImmutableObjectEqualsAtLeastOnePairedMutableObject
    /// method of the BaseIPairedImmutable tester.
    /// </remarks>
    public interface IPairedImmutable : IImmutable
    {

        /// <summary>
        /// Returns the IPairedMutable class
        /// that is paired with this IPairedImmutable.
        /// </summary>
        /// <remarks>
        /// <list type="table">
        /// <listheader><term>Specification</term><description>Test method in BaseIImmutableTester class</description></listheader>
        /// <item>
        /// <term>PairedMutableType is assignable to IPairedMutable.</term>
        /// <description>PairedMutableClassIsIPairedMutable</description>
        /// </item>
        /// </list>
        /// </remarks>
        /// <returns>Paired mutable type of this particular
        /// IPairedImmutable class.</returns>
        Type PairedMutableType { get; }

    }

    /// <summary>
    /// Boilerplate implementation of IPairedImmutable,
    /// in which PairedMutableType is deduced by naming
    /// convention and is presumed to be the valid
    /// delegee type.
    /// </summary>
    /// <remarks>
    /// Descendant classes should rarely if ever
    /// need to override default methods, except perhaps
    /// to improve performance when very high performance
    /// is required.
    /// </remarks>
    public abstract class PairedImmutable : Immutable, IPairedImmutable
    {

        /********************************
         * 
         * Private members and methods
         * 
         *******************************/

        /// <summary>
        /// This class's full name, for identification in 
        /// reporting code errors.
        /// </summary>
        /// <remarks> 
        /// Note that this is NOT the actual type of the
        /// instance, because the purpose of identifying
        /// the class in the error messages is to tell
        /// support personnel where to find the code.
        /// And the code generating the errors is 
        /// in the Immutable class definition, even if 
        /// the actual object instantiated is a fourth-generation
        /// descendant.</remarks>
        private string FullTypeNameForCodeErrors { get { return typeof(PairedImmutable).FullName; } }

        /// <summary>
        /// Construct from delegee.
        /// </summary>
        /// <remarks>
        /// It is of absolutely <em>paramount</em>
        /// importance that the code that calls the
        /// constructor, having built the delegee
        /// object, discards <em>all</em> references
        /// to the delegee object <em>immediately</em>
        /// after calling the constructor, in such a
        /// way as to allow absolutely no reference
        /// to the delegee to escape. It is a fundamental
        /// assumption of the implementation that the
        /// delegee object is 100% private to the 
        /// Immutable object -- it is, in fact, a
        /// class invariant, but one which the PairedImmutable
        /// object itself can in no way enforce given
        /// this hole in the encapsulation. The hole
        /// is provided simply because the convenience
        /// is worth the hole.
        /// <list type="table">
        /// <listheader><term>Specification</term><description>Test method in BaseIImmutableTester class</description></listheader>
        /// <item>
        /// <term>Where delegee param is null, constructor throws NullArgumentException.</term>
        /// <description>ConstructionFromNullDelegeeCausesNullArgumentException</description>
        /// </item>
        /// <item>
        /// <term>Where delegee param is not a valid delegee type, constructor throws
        /// BadDelegeeOfImmutableTypeException.</term>
        /// <description>ConstructionFromUnacceptableDelegeeCausesBadDelegeeOfImmutableTypeException</description>
        /// </item>
        /// <item>
        /// <term>Where delegee param is of an acceptable type, constructor completes
        /// successfully with IsFullyConstructed = true.</term>
        /// <description>ConstructionFromValidDelegeeFullyCompletesConstruction</description>
        /// </item>
        /// </list>
        /// </remarks>
        /// <param name="delegee">The object that
        /// will become the PairedImmutable's delegee.</param>
        public PairedImmutable(object delegee): base(delegee)
        {}

        /// <summary>
        /// Implements
        /// <see cref="Immutable.ValidTypeForDelegee"/> 
        /// </summary>
        /// <remarks>
        /// The assumption is that the delegee must be
        /// an instance of the PairedMutableType. As long
        /// as this is true, this method need not be
        /// overridden by descendants except when maximum
        /// performance is required.
        /// <para>
        /// Note that this is convenient assumption but
        /// is NOT part of the contract imposed on
        /// descendants.</para>
        /// </remarks>
        protected override Type ValidTypeForDelegee
        {
            get { return PairedMutableType; }
        }

        /********************************
         * 
         * IPairedImmutable methods
         * 
         *******************************/

        /// <summary>
        /// Implements
        /// <see cref="IPairedImmutable.PairedMutableType"/>.
        /// </summary>
        /// <remarks> 
        /// Specifications are unchanged from 
        /// <see cref="IPairedImmutable.PairedMutableType"/>.
        /// </remarks>
        public virtual Type PairedMutableType
        {
            get
            {
                string nameOfThisType = GetType().FullName;
                string nameOfPairedType
                    = nameOfThisType.Substring(
                        0
                        , nameOfThisType.Length - 9
                        ) 
                        + "Mutable";
                return Type.GetType(nameOfPairedType);
            }
        }

    }

}
