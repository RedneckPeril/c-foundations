﻿using System;
using System.Diagnostics;

namespace Com.VeritasTS.CoreUtils
{

    /// <summary>
    /// Object capable of asserting its own
    /// class invariants once fully constructed
    /// </summary>
    /// <remarks>
    /// Class invariants are things that should always be 
    /// true of any instance of a class once that instance 
    /// has been fully constructed – for an appropriate definition 
    /// of “fully constructed.” For example, we could define a 
    /// PositiveInteger class with a Value property; in that case 
    /// (unless we defined it perversely) it would be an error if 
    /// our code ever allowed the Value to fall below 1. However, 
    /// if we were to check the Value property the moment we 
    /// entered the constructor, we would find it = 0, and this 
    /// would not be an error – it would just mean the constructor 
    /// hadn’t had the chance to set the Value property.
    /// <para>Once the object is fully constructed, however, we can 
    /// and should, at least when not in production or doing
    /// performance-testing, test all the class invariants, to make sure the 
    /// object was left in a sane state by the constructor. Furthermore, 
    /// all methods that have any possibility of altering the state of 
    /// the object, should run all the self-assertions as the last 
    /// step before returning to the caller. This way, the moment 
    /// an object is wrongly manipulated, the code screams a warning.
    /// </para>
    /// <para>Of course, all this code-checking takes time, and if we 
    /// really have written all the accessor and constructor methods 
    /// properly, the error-checking will never have any errors to 
    /// catch anyway. So the error-checking does not, generally 
    /// speaking, throw custom exceptions. Instead, the tests are 
    /// all executed with the 
    /// <see cref="System.Diagnostics.Debug.Assert(bool)"/> or 
    /// <see cref="System.Diagnostics.Debug.Assert(bool, string)"/> methods, 
    /// each of which disappears from the compiled code entirely when 
    /// compiling for production environments. Thus for unit testing 
    /// and systems integration testing, you compile with assertions 
    /// on, and simply running your code causes a massive amount of 
    /// testing to be constantly taking place. When it is time for 
    /// performance tuning, or for compiling a production build – 
    /// which you only do once all tests run cleanly with assertions 
    /// on – then you turn assertions off and all that testing 
    /// disappears, leaving no performance impact. That is to say, 
    /// if any of the methods of this interface are ever called in 
    /// production code, some programmer has misused that method.
    /// </para>
    /// <para>This is why all three of the methods in this interface
    /// return booleans – so that any call to an ISelfAsserter method 
    /// can be wrapped within a call to Debug.Assert.
    /// </para>
    /// <para>
    /// Four additional complications come into play here: the calling of 
    /// base constructors in a polymorphic inheritance tree, the issue of 
    /// constructing owned/owning object pairs where each references the 
    /// other, the issue of null construction in deserialization or
    /// framework instantiation, and the issue of complex updates in 
    /// the middle of which one or more invariants may be temporarily 
    /// invalidated.
    /// </para>
    /// <para></para>
    /// <para>
    /// <strong><em>Base constructor calls</em></strong>
    /// </para>
    /// <para>
    /// First, consider a <code>Grandchild</code> class that is inherited 
    /// from a <code>Child</code> class that itself is inherited from a 
    /// <code>Person</code> class, and that none of these are abstract 
    /// classes. If we reach the end of the <code>Person</code> constructor, 
    /// this could mean that the object is fully constructed and that from 
    /// this point on until its destruction all its class invariants 
    /// should hold true. But it could also simply mean that we are at 
    /// the end of the first stage of constructing a <code>Grandchild</code> class, 
    /// in which case, if we were to try to assert the class invariants, 
    /// we could cause a bogus assertion failure.
    /// </para>
    /// <para>The solution to this is simple: at the end of each constructor, 
    /// we call the self-assertion code if and only if the type of object we 
    /// are constructing is that constructor’s type. Thus at the end of the 
    /// <code>Person</code> constructor we will assert the object’s invariants, 
    /// if we are building a <code>Person</code> object, but not if we are 
    /// building a <code>Child</code> or <code>Grandchild</code>. At the end of 
    /// the <code>Child</code> constructor we will assert the object’s invariants, 
    /// if we are building a <code>Child</code> object, but not if we are building 
    /// a <code>Grandchild</code>. At the end of the <code>Grandchild</code>
    /// constructor we will assert the object’s invariants, if we are building a 
    /// <code>Grandchild</code> object, but not if we are building…anything else. 
    /// This is true even if the class has no descendants when we write it (unless
    /// it is a sealed class); if somebody later inherits a <code>Greatgrandchild</code> 
    /// class from <code>Grandchild</code> we do not want to have to rewrite 
    /// <code>Grandchild</code> to remove an assumption that it has no descendants.
    /// </para>
    /// <para></para>
    /// <para>
    /// <strong><em>Mutually dependent objects</em></strong>
    /// </para>
    /// <para>
    /// Next, assume that we wish to define two descendants of <code>Person</code>: 
    /// <code>Parent</code>, and <code>Child</code>. It is a fact that every 
    /// <code>Parent</code> must, by definition, have a <code>Child</code> – having
    /// a child is what makes you a parent.It is also a fact that every 
    /// <code>Child</code> must have a <code>Parent</code>, else the child would 
    /// not exist. So it would be natural to have a <code>List&lt;Child&gt; Children</code>
    /// property in the <code>Parent</code> class, and a 
    /// <code>List&lt;Parent&gt; Parents</code>property in the <code>Child</code> class, 
    /// and to include as class invariants for the two classes that neither list can be empty.
    /// </para>
    /// <para>
    /// But now we can run into difficulties when we are creating the pairs.
    /// For example, if we are creating a Family, we might want to have a statement like this:
    /// </para>
    /// <para>
    /// <code>
    /// Parent father
    ///     = new Parent(
    ///         name: "Adam"
    ///         , children: new List&lt;Child&gt;
    ///             {
    ///             new Child(
    ///                 "Cain"
    ///                 , parents: ???????
    ///                 )
    ///             , new Child(
    ///                 "Abel"
    ///                 , parents: ???????
    ///                 )
    ///             , new Child(
    ///                 "Seth"
    ///                 , parents: ???????
    ///                 )
    ///         );
    /// </code>
    /// </para>
    /// <para>
    /// Here we can’t pass Adam as the parent to his children’s constructors; 
    /// so when Cain is “fully constructed,” he will still fail assertion.
    /// In fact, Adam and Eve and Cain and Abel and Seth are “fully constructed” 
    /// in the <em>true</em> sense only when they have all been created and properly linked up.
    /// </para>
    /// <para>
    /// In classes such as these, we can provide an optional Boolean construction 
    /// parameter <code>assert</code> which, if set to <code>false</code>, skips 
    /// the self-assertion. Furthermore, all of the objects that alter the object’s 
    /// state can take that same parameter, so that the self-assertion can be avoided 
    /// for objects that the caller knows are not truly fully constructed.  Once the 
    /// factory that is building out the “family” of mutually dependent objects 
    /// knows that the family is complete, all other calls to state-changing methods 
    /// can leave the assert parameter at its default setting of true.
    /// </para>
    /// <para>
    /// Assume that for our <code>Parent</code> and <code>Child</code> classes, one of 
    /// our invariants for the <code>Child</code> class is that a <code>Child</code>’s 
    /// <code>Parent</code> objects must list the <code>Child</code> as a <code>Parent</code>, 
    /// and <em>vice versa</em>. Assume also that the <code>Parent.AddChild</code> method 
    /// automatically adds the <code>Parent</code> to <code>Child.Parents</code> at the same 
    /// time that it adds the <code>Child</code> to its own <code>Children</code> property.
    /// Then the following code will work properly to build out our family:
    /// </para>
    /// <para>
    /// <code>
    /// Parent Adam
    ///     = new Parent(
    ///         name: "Adam"
    ///         , children: new List&lt;Child&gt;()
    ///         , assert: false
    ///         );
    /// Parent Eve
    ///     = new Parent(
    ///         name: "Eve"
    ///         , children: new List&lt;Child&gt;()
    ///         , assert: false
    ///         );
    /// Child Cain
    ///     = new Child(
    ///         name: "Cain"
    ///         , parents: new List&lt;Parent&gt;()
    ///         , assert: false
    ///         );
    /// Adam.AddChild( Cain  ); // Adam and Cain both get tested here
    /// Eve.AddChild( Cain );  // Eve and Cain both get tested here
    /// Child Abel
    ///     = new Child(
    ///         name: "Abel"
    ///         , parents: new List&lt;Parent&gt;()
    ///         , assert: false
    ///         );
    /// Adam.AddChild( Abel  ); // Adam and Abel both get tested here
    /// Eve.AddChild( Abel );  // Eve and Abel both get tested here
    /// Child Seth
    ///     = new Child(
    ///         name: "Seth"
    ///         , parents: new List&lt;Parent&gt;()
    ///         , assert: false
    ///         );
    /// Adam.AddChild( Seth  ); // Adam and Seth both get tested here
    /// Eve.AddChild( Seth );  // Eve and Seth both get tested here
    /// </code>
    /// </para>
    /// <para>
    /// From now on, our code should assume that all five objects are 
    /// sane – as opposed to the people whom the objects represent, of course.
    /// If Cain kills Abel:
    /// </para>
    /// <para>
    /// <code>
    /// Adam.RemoveChild( Abel ); // Adam and Abel both get tested here
    /// Eve.RemoveChild( Abel );  // Eve and Abel both get tested here
    /// Abel = null;
    /// </code>
    /// </para>
    /// <para></para>
    /// <para>
    /// <strong><em>Null construction</em></strong>
    /// </para>
    /// <para>
    /// It is very common to have objects with properties that hold objects, 
    /// and to have a class invariant that one or more of those properties 
    /// must not be null. In such a case, the null constructor almost always 
    /// has to complete without setting those properties, since no value for 
    /// said properties is passed in to the null constructor, by definition.
    /// The natural response is to disable the null constructor. Yet we 
    /// may find it necessary use a null constructor, for creation 
    /// in contexts where no construction parameters can be passed. 
    /// </para>
    /// <para>
    /// In order to address this problem, we have a Boolean property 
    /// <see cref="ISelfAsserter.IsFullyConstructed"/>, which begins life as 
    /// <code>false</code>, but which can be set to <code>true</code> with the 
    /// <see cref="MarkAsFullyConstructed(Type, bool)"/> 
    /// method. Once it has been set to true it cannot be taken back to false.
    /// </para>
    /// <para>
    /// Constructors other than the null constructor always end with a call to 
    /// <see cref="MarkAsFullyConstructed(Type, bool)"/>. 
    /// If the constructor takes an optional assert parameter, this parameter 
    /// is referenced in the call:
    /// </para>
    /// <para>
    /// <code>
    /// Debug.Assert( MarkAsFullyConstructed( [type of current constructor], assert ) );
    /// </code>
    /// </para>
    /// <para>
    /// This call should have the following effects:
    /// <list type="number">
    /// <item>If we are not in Debug.Assert mode the whole thing disappears entirely.</item>
    /// <item>Else if we are not constructing an object of the current type, this is a null-op.</item>
    /// <item>Else:
    ///   <list type="number">
    ///   <item><see cref="ISelfAsserter.IsFullyConstructed"/> is irrevocably set to true.</item>
    ///   <item>If the <code>assert</code> parameter is true, a call to 
    ///   <code>Debug.Assert( PassesSelfAssertion() );</code> is executed.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </para>
    /// <para>
    /// If the null constructor can construct the argument into a sane state, 
    /// then it too ends by marking the object as fully constructed. Otherwise 
    /// it leaves the object marked as not yet ready for assertions. At this 
    /// point the calling object can fill in the blanks with appropriate 
    /// <code>SetPropertyX</code> statements, and then call a 
    /// <code>CompleteConstruction()</code> method that attempts to finish 
    /// constructing the argument based on what has been done so far, marks 
    /// the object as fully constructed, and fires the self-assertions. This 
    /// CompleteConstruction() method is not part if the ISelfAsserter interface, 
    /// since it is not required for objects whose null constructor itself can 
    /// complete the object’s construction. This mechanism – or an equivalent – 
    /// is added downstream, to whatever classes require it.
    /// </para>
    /// <para></para>
    /// <para>
    /// <strong><em>State change</em></strong>
    /// </para>
    /// <para>
    /// Where states can change, the object must ensure that when the state change 
    /// is complete, the class invariants still hold. They do <em>not</em> have to hold 
    /// while the object is in the middle of changing; however, if there is an 
    /// intermediate step while the object is temporarily not compliant with its 
    /// contract, that intermediate state must not be accessible. In a 
    /// multithreaded world, this means that that intermediate state must not 
    /// be accessible even in the face of thread-switching by the thread controller. 
    /// Thus a <code>SetPropertyX</code> method that consists of the single statement 
    /// <code>PropertyX = value</code> is safe. But say that we have a class that 
    /// has both a <code>ParentObject</code> and an <code>Owner</code>, and that for 
    /// some classes these two should always point at the same item. Thus if we 
    /// call <code>SetParentObject</code> we must have at least the two statements 
    /// <code>ParentObject = value;</code> and <code>Owner = value;</code>. 
    /// Locking or some other appropriate approach must be taken to render the 
    /// <code>SetParentObject</code> method threadsafe; otherwise a thread could 
    /// access the object while <code>ParentObject = value</code> but 
    /// <code>Owner</code> still points at the old value, violating the class invariant.
    /// </para>
    /// <para>
    /// There are multiple strategies for ensuring this type of threadsafety, 
    /// and no one right answer; thus we do not prescribe one. Generally speaking 
    /// the simplest, and therefore least error-prone and most often preferred, 
    /// is to use locking on the object. But this is not required by the 
    /// <code>ISelfAsserter</code> interface.
    /// </para>
    /// </remarks>
    public interface ISelfAsserter
    {

        /// <summary>
        /// States whether the object has been
        /// fully constructed and therefore can
        /// be self-asserted.
        /// </summary>
        /// <remarks>
        /// Returns a boolean so that it can be housed
        /// within a 
        /// <see cref="System.Diagnostics.Debug.Assert(bool)"/> 
        /// call, thus disappearing from the code entirely 
        /// under Production compilation.
        /// <para>
        /// This property is read-only, though its value
        /// in non-immutable ISelfAsserters can be moved from 
        /// false to true (though
        /// not the other direction) by 
        /// <see cref="MarkAsFullyConstructed(Type, bool)"/>.
        /// </para>
        /// <para>
        /// IsFullyConstructed does nothing except affect the behavior
        /// of PassesSelfAssertion, and therefore its specifications
        /// are fully laid out under the specifications for
        /// PassesSelfAssertion.
        /// </para>
        /// </remarks>
        /// <returns>True if fully constructed, else false.</returns>
        bool IsFullyConstructed { get; }

        /// <summary>
        /// Asserts, using only 
        /// <see cref="System.Diagnostics.Debug.Assert(bool)"/>,
        /// all class invariants.
        /// </summary>
        /// <remarks>
        /// Where the <see cref="IsFullyConstructed"/> property
        /// is false, this method is a null-op. Furthermore,
        /// if testableType is specified, then the method is a
        /// null-op unless the object is an instance of precisely 
        /// that type. If we have a class A and a class B: A, then
        /// calling PassesSelfAssertion(typeof(A)) on an instance
        /// of class B will have no effect.
        /// <para>
        /// The method returns a boolean so that it can be housed
        /// within a 
        /// <see cref="System.Diagnostics.Debug.Assert(bool)"/> 
        /// call, thus disappearing from the code entirely 
        /// under Production compilation.
        /// </para>
        /// <list type="table">
        /// <listheader><term>Specification</term><description>Test method in BaseISelfAsserterTester class</description></listheader>
        /// <item>
        /// <term>Where IsFullyConstructed is false, PassesSelfAssertion() returns true.</term>
        /// <description>ParameterlessSelfAssertionPassesWhenGoodAndNotFullyConstructed</description>
        /// </item>
        /// <item>
        /// <term></term>
        /// <description>ParameterlessSelfAssertionPassesWhenBadAndNotFullyConstructed</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true, PassesSelfAssertion() returns true
        /// if there are no lurking assertion failures.</term>
        /// <description>ParameterlessSelfAssertionPassesWhenGoodAndFullyConstructed</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true, PassesSelfAssertion()
        /// throws exception if there are lurking assertion failures.</term>
        /// <description>NullParameterSelfAssertionFailsWhenBadAndFullyConstructed</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is false, PassesSelfAssertion(null) returns true
        /// if there are no lurking assertion failures.</term>
        /// <description>NullParameterSelfAssertionPassesWhenGoodAndNotFullyConstructed</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is false, PassesSelfAssertion(null) returns true
        /// even if there are lurking assertion failures.</term>
        /// <description>NullParameterSelfAssertionPassesWhenBadAndNotFullyConstructed</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true, PassesSelfAssertion(null) returns true
        /// if there are no lurking assertion failures.</term>
        /// <description>NullParameterSelfAssertionPassesWhenGoodAndFullyConstructed</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true, PassesSelfAssertion(null)
        /// throws exception if there are lurking assertion failures.</term>
        /// <description>ParameterlessSelfAssertionFailsWhenBadAndFullyConstructed</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is false, x.PassesSelfAssertion(x.GetType()) returns true
        /// if there are no lurking assertion failures.</term>
        /// <description>SelfAssertionForExactTypePassesWhenGoodAndNotFullyConstructed</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is false, x.PassesSelfAssertion(x.GetType()) returns true
        /// even if there are lurking assertion failures.</term>
        /// <description>SelfAssertionForExactTypePassesWhenBadAndNotFullyConstructed</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true, x.PassesSelfAssertion(x.GetType()) returns true
        /// if there are no lurking assertion failures.</term>
        /// <description>SelfAssertionForExactTypePassesWhenGoodAndFullyConstructed</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true, x.PassesSelfAssertion(x.GetType())
        /// throws exception if there are lurking assertion failures.</term>
        /// <description>SelfAssertionForExactTypeFailsWhenBadAndFullyConstructed</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is false, x.PassesSelfAssertion(some type other than x.GetType())
        /// returns true if there are no lurking assertion failures.</term>
        /// <description>SelfAssertionForDifferentTypePassesWhenGoodAndNotFullyConstructed</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is false, x.PassesSelfAssertion(some type other than x.GetType())
        /// returns true even if there are lurking assertion failures.</term>
        /// <description>SelfAssertionForDifferentTypePassesWhenBadAndNotFullyConstructed</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true, x.PassesSelfAssertion(some type other than x.GetType())
        /// returns true if there are no lurking assertion failures.</term>
        /// <description>SelfAssertionForDifferentTypePassesWhenGoodAndFullyConstructed</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true, x.PassesSelfAssertion(some type other than x.GetType())
        /// returns true even if there are lurking assertion failures.</term>
        /// <description>SelfAssertionForDifferentTypePassesWhenBadAndFullyConstructed</description>
        /// </item>
        /// </list>
        /// </remarks>
        /// <param name="testableType">If testableType is non-null,
        /// and not precisely the type of this instance, then
        /// method is a null-op. Otherwise assertions are fired.</param>
        /// <returns>True if passes, else false.</returns>
        bool PassesSelfAssertion(Type testableType = null);

        /// <summary>
        /// Marks an object as having been fully constructed.
        /// </summary>
        /// <remarks>
        /// If testableType is not null, then MarkAsFullyConstructed
        /// has no effect unless the object is an instance of 
        /// precisely the specified type. 
        /// If we have a class A and a class B: A, then
        /// calling MarkAsFullyConstructed(typeof(A)) on an instance
        /// of class B will have no effect.
        /// <para>
        /// If this method changes the object's state, it calls 
        /// <see cref="ISelfAsserter.PassesSelfAssertion"/> 
        /// just before returning, unless the assert parameter 
        /// is explicitly set to false.
        /// </para>
        /// <para>
        /// The method returns a boolean so that it can be housed
        /// within a 
        /// <see cref="System.Diagnostics.Debug.Assert(bool)"/> 
        /// call, thus disappearing from the code entirely 
        /// under Production compilation.
        /// </para>
        /// <list type="table">
        /// <listheader><term>Specification</term><description>Test method in BaseIImmutableTester class</description></listheader>
        /// <item>
        /// <term>Where IsFullyConstructed is false and no assertion failures
        /// are lurking, MarkAsFullyConstructed() 
        /// returns true and causes IsFullyConstructed to become true.</term>
        /// <description>ParameterlessMarkAsFullyConstructedCompletesConstructionWhenGood</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is false and assertion failures
        /// are lurking, MarkAsFullyConstructed() 
        /// causes IsFullyConstructed to become true and either (1) returns
        /// true in Production compilation or (2) throws exception in
        /// Test compilation.</term>
        /// <description>ParameterlessMarkAsFullyConstructedCompletesConstructionAndRespondsToCompilationModeWhenBad</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true and no assertion failures
        /// are lurking, MarkAsFullyConstructed() 
        /// returns true and leaves IsFullyConstructed set to true.</term>
        /// <description>ParameterlessMarkAsFullyConstructedLeavesConstructionCompleteWhenGood</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true and assertion failures
        /// are lurking, MarkAsFullyConstructed() 
        /// leaves IsFullyConstructed set to true and either (1) returns
        /// true in Production compilation or (2) throws exception in
        /// Test compilation.</term>
        /// <description>ParameterlessMarkAsFullyConstructedLeavesConstructionCompleteAndRespondsToCompilationModeWhenBad</description>
        /// </item>
        /// 
        /// <item>
        /// <term>Where IsFullyConstructed is false and no assertion failures
        /// are lurking, MarkAsFullyConstructed(null) 
        /// returns true and causes IsFullyConstructed to become true.</term>
        /// <description>NullTypeDefaultAssertMarkAsFullyConstructedCompletesConstructionWhenGood</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is false and assertion failures
        /// are lurking, MarkAsFullyConstructed(null) 
        /// causes IsFullyConstructed to become true and either (1) returns
        /// true in Production compilation or (2) throws exception in
        /// Test compilation.</term>
        /// <description>NullTypeDefaultAssertMarkAsFullyConstructedCompletesConstructionAndRespondsToCompilationModeWhenBad</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true and no assertion failures
        /// are lurking, MarkAsFullyConstructed(null) 
        /// returns true and leaves IsFullyConstructed set to true.</term>
        /// <description>NullTypeDefaultAssertMarkAsFullyConstructedLeavesConstructionCompleteWhenGood</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true and assertion failures
        /// are lurking, MarkAsFullyConstructed(null) 
        /// leaves IsFullyConstructed set to true and either (1) returns
        /// true in Production compilation or (2) throws exception in
        /// Test compilation.</term>
        /// <description>NullTypeDefaultAssertMarkAsFullyConstructedLeavesConstructionCompleteAndRespondsToCompilationModeWhenBad</description>
        /// </item>
        /// 
        /// <item>
        /// <term>Where IsFullyConstructed is false and no assertion failures
        /// are lurking, MarkAsFullyConstructed(null, true) 
        /// returns true and causes IsFullyConstructed to become true.</term>
        /// <description>NullTypeAssertingMarkAsFullyConstructedCompletesConstructionWhenGood</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is false and assertion failures
        /// are lurking, MarkAsFullyConstructed(null, true) 
        /// causes IsFullyConstructed to become true and either (1) returns
        /// true in Production compilation or (2) throws exception in
        /// Test compilation.</term>
        /// <description>NullTypeAssertingMarkAsFullyConstructedCompletesConstructionAndRespondsToCompilationModeWhenBad</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true and no assertion failures
        /// are lurking, MarkAsFullyConstructed(null, true) 
        /// returns true and leaves IsFullyConstructed set to true.</term>
        /// <description>NullTypeAssertingMarkAsFullyConstructedLeavesConstructionCompleteWhenGood</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true and assertion failures
        /// are lurking, MarkAsFullyConstructed(null, true) 
        /// leaves IsFullyConstructed set to true and either (1) returns
        /// true in Production compilation or (2) throws exception in
        /// Test compilation.</term>
        /// <description>NullTypeAssertingMarkAsFullyConstructedLeavesConstructionCompleteAndRespondsToCompilationModeWhenBad</description>
        /// </item>
        /// 
        /// <item>
        /// <term>Where IsFullyConstructed is false and no assertion failures
        /// are lurking, MarkAsFullyConstructed(null, false) 
        /// returns true and causes IsFullyConstructed to become true.</term>
        /// <description>NullTypeNonassertingMarkAsFullyConstructedCompletesConstructionWhenGood</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is false and assertion failures
        /// are lurking, MarkAsFullyConstructed(null, false) 
        /// causes IsFullyConstructed to become true and returns true.</term>
        /// <description>NullTypeNonassertingMarkAsFullyConstructedCompletesConstructionWhenBad</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true and no assertion failures
        /// are lurking, MarkAsFullyConstructed(null, false) 
        /// returns true and leaves IsFullyConstructed set to true.</term>
        /// <description>NullTypeNonassertingMarkAsFullyConstructedLeavesConstructionCompleteWhenGood</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true and assertion failures
        /// are lurking, MarkAsFullyConstructed(null, false) 
        /// leaves IsFullyConstructed set to true and returns true.</term>
        /// <description>NullTypeNonassertingMarkAsFullyConstructedLeavesConstructionCompleteWhenBad</description>
        /// </item>
        /// 
        /// <item>
        /// <term>Where IsFullyConstructed is false and no assertion failures
        /// are lurking, x.MarkAsFullyConstructed(x.GetType()) 
        /// returns true and causes IsFullyConstructed to become true.</term>
        /// <description>ExactTypeDefaultAssertMarkAsFullyConstructedCompletesConstructionWhenGood</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is false and assertion failures
        /// are lurking, x.MarkAsFullyConstructed(x.GetType()) 
        /// causes IsFullyConstructed to become true and either (1) returns
        /// true in Production compilation or (2) throws exception in
        /// Test compilation.</term>
        /// <description>ExactTypeDefaultAssertMarkAsFullyConstructedCompletesConstructionAndRespondsToCompilationModeWhenBad</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true and no assertion failures
        /// are lurking, x.MarkAsFullyConstructed(x.GetType()) 
        /// returns true and leaves IsFullyConstructed set to true.</term>
        /// <description>ExactTypeDefaultAssertMarkAsFullyConstructedLeavesConstructionCompleteWhenGood</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true and assertion failures
        /// are lurking, x.MarkAsFullyConstructed(x.GetType()) 
        /// leaves IsFullyConstructed set to true and either (1) returns
        /// true in Production compilation or (2) throws exception in
        /// Test compilation.</term>
        /// <description>ExactTypeDefaultAssertMarkAsFullyConstructedLeavesConstructionCompleteAndRespondsToCompilationModeWhenBad</description>
        /// </item>
        /// 
        /// <item>
        /// <term>Where IsFullyConstructed is false and no assertion failures
        /// are lurking, x.MarkAsFullyConstructed(x.GetType(), true) 
        /// returns true and causes IsFullyConstructed to become true.</term>
        /// <description>ExactTypeAssertingMarkAsFullyConstructedCompletesConstructionWhenGood</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is false and assertion failures
        /// are lurking, x.MarkAsFullyConstructed(x.GetType(), true) 
        /// causes IsFullyConstructed to become true and either (1) returns
        /// true in Production compilation or (2) throws exception in
        /// Test compilation.</term>
        /// <description>ExactTypeAssertingMarkAsFullyConstructedCompletesConstructionAndRespondsToCompilationModeWhenBad</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true and no assertion failures
        /// are lurking, x.MarkAsFullyConstructed(x.GetType(), true) 
        /// returns true and leaves IsFullyConstructed set to true.</term>
        /// <description>ExactTypeAssertingMarkAsFullyConstructedLeavesConstructionCompleteWhenGood</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true and assertion failures
        /// are lurking, x.MarkAsFullyConstructed(x.GetType(), true) 
        /// leaves IsFullyConstructed set to true and either (1) returns
        /// true in Production compilation or (2) throws exception in
        /// Test compilation.</term>
        /// <description>ExactTypeAssertingMarkAsFullyConstructedLeavesConstructionCompleteAndRespondsToCompilationModeWhenBad</description>
        /// </item>
        /// 
        /// <item>
        /// <term>Where IsFullyConstructed is false and no assertion failures
        /// are lurking, x.MarkAsFullyConstructed(x.GetType(), false) 
        /// returns true and causes IsFullyConstructed to become true.</term>
        /// <description>ExactTypeNonassertingMarkAsFullyConstructedCompletesConstructionWhenGood</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is false and assertion failures
        /// are lurking, x.MarkAsFullyConstructed(x.GetType(), false) 
        /// causes IsFullyConstructed to become true and returns true.</term>
        /// <description>ExactTypeNonassertingMarkAsFullyConstructedCompletesConstructionWhenBad</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true and no assertion failures
        /// are lurking, x.MarkAsFullyConstructed(x.GetType(), false) 
        /// returns true and leaves IsFullyConstructed set to true.</term>
        /// <description>ExactTypeNonassertingMarkAsFullyConstructedLeavesConstructionCompleteWhenGood</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true and assertion failures
        /// are lurking, x.MarkAsFullyConstructed(x.GetType(), false) 
        /// leaves IsFullyConstructed set to true and returns true.</term>
        /// <description>ExactTypeNonassertingMarkAsFullyConstructedLeavesConstructionCompleteWhenBad</description>
        /// </item>
        /// 
        /// <item>
        /// <term>Where IsFullyConstructed is false and no assertion failures
        /// are lurking, x.MarkAsFullyConstructed(type other than x.GetType()) 
        /// returns true and leaves IsFullyConstructed set to false.</term>
        /// <description>DifferentTypeDefaultAssertMarkAsFullyConstructedLeavesConstructionIncompleteWhenGood</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is false and assertion failures
        /// are lurking, x.MarkAsFullyConstructed(type other than x.GetType()) 
        /// leaves IsFullyConstructed set to false and returns true.</term>
        /// <description>DifferentTypeDefaultAssertMarkAsFullyConstructedLeavesConstructionIncompleteWhenBad</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true and no assertion failures
        /// are lurking, x.MarkAsFullyConstructed(type other than x.GetType()) 
        /// returns true and leaves IsFullyConstructed set to true.</term>
        /// <description>DifferentTypeDefaultAssertMarkAsFullyConstructedLeavesConstructionCompleteWhenGood</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true and assertion failures
        /// are lurking, x.MarkAsFullyConstructed(type other than x.GetType()) 
        /// leaves IsFullyConstructed set to true and returns true.</term>
        /// <description>DifferentTypeDefaultAssertMarkAsFullyConstructedLeavesConstructionCompleteWhenBad</description>
        /// </item>
        /// 
        /// <item>
        /// <term>Where IsFullyConstructed is false and no assertion failures
        /// are lurking, x.MarkAsFullyConstructed(type other than x.GetType(), true) 
        /// returns true and leaves IsFullyConstructed set to false.</term>
        /// <description>DifferentTypeAssertingMarkAsFullyConstructedLeavesConstructionIncompleteWhenGood</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is false and assertion failures
        /// are lurking, x.MarkAsFullyConstructed(type other than x.GetType(), true) 
        /// leaves IsFullyConstructed set to false and returns true.</term>
        /// <description>DifferentTypeAssertingMarkAsFullyConstructedLeavesConstructionIncompleteWhenBad</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true and no assertion failures
        /// are lurking, x.MarkAsFullyConstructed(type other than x.GetType(), true) 
        /// returns true and leaves IsFullyConstructed set to true.</term>
        /// <description>DifferentTypeAssertingMarkAsFullyConstructedLeavesConstructionCompleteWhenGood</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true and assertion failures
        /// are lurking, x.MarkAsFullyConstructed(type other than x.GetType(), true) 
        /// leaves IsFullyConstructed set to true and returns true.</term>
        /// <description>DifferentTypeAssertingMarkAsFullyConstructedLeavesConstructionCompleteWhenBad</description>
        /// </item>
        /// 
        /// <item>
        /// <term>Where IsFullyConstructed is false and no assertion failures
        /// are lurking, x.MarkAsFullyConstructed(type other than x.GetType(), false) 
        /// returns true and leaves IsFullyConstructed set to false.</term>
        /// <description>DifferentTypeNonassertingMarkAsFullyConstructedLeavesConstructionIncompleteWhenGood</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is false and assertion failures
        /// are lurking, x.MarkAsFullyConstructed(type other than x.GetType(), false) 
        /// leaves IsFullyConstructed set to false and returns true.</term>
        /// <description>DifferentTypeNonassertingMarkAsFullyConstructedLeavesConstructionIncompleteWhenBad</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true and no assertion failures
        /// are lurking, x.MarkAsFullyConstructed(type other than x.GetType(), false) 
        /// returns true and leaves IsFullyConstructed set to true.</term>
        /// <description>DifferentTypeNonassertingMarkAsFullyConstructedLeavesConstructionCompleteWhenGood</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true and assertion failures
        /// are lurking, x.MarkAsFullyConstructed(type other than x.GetType(), false) 
        /// leaves IsFullyConstructed set to true and returns true.</term>
        /// <description>DifferentTypeNonassertingMarkAsFullyConstructedLeavesConstructionCompleteWhenBad</description>
        /// </item>
        /// 
        /// <item>
        /// <term>Where IsFullyConstructed is false and no assertion failures
        /// are lurking, MarkAsFullyConstructed(assert: true) 
        /// returns true and causes IsFullyConstructed to become true.</term>
        /// <description>NoTypeAssertingMarkAsFullyConstructedCompletesConstructionWhenGood</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is false and assertion failures
        /// are lurking, MarkAsFullyConstructed(assert: true) 
        /// causes IsFullyConstructed to become true and either (1) returns
        /// true in Production compilation or (2) throws exception in
        /// Test compilation.</term>
        /// <description>NoTypeAssertingMarkAsFullyConstructedCompletesConstructionAndRespondsToCompilationModeWhenBad</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true and no assertion failures
        /// are lurking, MarkAsFullyConstructed(assert: true) 
        /// returns true and leaves IsFullyConstructed set to true.</term>
        /// <description>NoTypeAssertingMarkAsFullyConstructedLeavesConstructionCompleteWhenGood</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true and assertion failures
        /// are lurking, MarkAsFullyConstructed(assert: true) 
        /// leaves IsFullyConstructed set to true and either (1) returns
        /// true in Production compilation or (2) throws exception in
        /// Test compilation.</term>
        /// <description>NoTypeAssertingMarkAsFullyConstructedLeavesConstructionCompleteAndRespondsToCompilationModeWhenBad</description>
        /// </item>
        /// 
        /// <item>
        /// <term>Where IsFullyConstructed is false and no assertion failures
        /// are lurking, MarkAsFullyConstructed(assert: false) 
        /// returns true and causes IsFullyConstructed to become true.</term>
        /// <description>NoTypeNonassertingMarkAsFullyConstructedCompletesConstructionWhenGood</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is false and assertion failures
        /// are lurking, MarkAsFullyConstructed(assert: false) 
        /// causes IsFullyConstructed to become true and returns true.</term>
        /// <description>NoTypeNonassertingMarkAsFullyConstructedCompletesConstructionWhenBad</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true and no assertion failures
        /// are lurking, MarkAsFullyConstructed(assert: false) 
        /// returns true and leaves IsFullyConstructed set to true.</term>
        /// <description>NoTypeNonassertingMarkAsFullyConstructedLeavesConstructionCompleteWhenGood</description>
        /// </item>
        /// <item>
        /// <term>Where IsFullyConstructed is true and assertion failures
        /// are lurking, MarkAsFullyConstructed(assert: false) 
        /// leaves IsFullyConstructed set to true and returns true.</term>
        /// <description>NoTypeNonassertingMarkAsFullyConstructedLeavesConstructionCompleteWhenBad</description>
        /// </item>
        /// 
        /// 
        /// </list>
        /// </remarks>
        /// <param name="testableType">If testableType is non-null,
        /// and not precisely the type of this instance, then
        /// method is a null-op. Otherwise 
        /// <see cref="ISelfAsserter.IsFullyConstructed"/> 
        /// is set to true if it is not already true.</param>
        /// <param name="assert">If omitted or true, then
        /// <see cref="ISelfAsserter.PassesSelfAssertion(Type)"/>
        /// will be called if MarkAsFullyConstructed changes
        /// <see cref="ISelfAsserter.IsFullyConstructed"/> from
        /// false to true.
        /// </param>
        /// <returns>True if fully constructed, else false.</returns>
        bool MarkAsFullyConstructed(
            Type testableType = null
            , bool assert = true
            );

    }

    /// <summary>
    /// Boilerplate implementation of ISelfAsserter.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>Object method overrides and overridden GetHasCode assistants
    ///   <list type="bullet">
    ///   <item>GetHashCode(), if additional members are added.</item>
    ///   <item>HighestPrimeUsedInHashCode, if GetHashCode() is overridden.</item>
    ///   </list>
    /// </item>
    /// <item>SelfAsserterMutable method overrides
    ///   <list type="bullet">
    ///   <item>PassesSelfAssertion(), usually.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public abstract class SelfAsserterMutable : ISelfAsserter, IMutable
    {
        private bool _isFullyConstructed = false;

        /// <summary>
        /// Implements 
        /// <see cref="ISelfAsserter.IsFullyConstructed"/>.
        /// <remarks>
        /// Refers to a private bool flag set to
        /// false on construction and later to true
        /// by 
        /// <see cref="MarkAsFullyConstructed(Type, bool)"/>.
        /// </remarks>
        /// </summary>
        public bool IsFullyConstructed { get { return _isFullyConstructed; } }

        /// <summary>
        /// Implements 
        /// <see cref="ISelfAsserter.PassesSelfAssertion(Type)"/>.
        /// <remarks>
        /// Refers to a private bool flag set to
        /// false on construction and later to true
        /// by 
        /// <see cref="MarkAsFullyConstructed(Type, bool)"/>.
        /// <para>
        /// When implemented, in each generation the following
        /// boilerplate should be used:
        /// <code>
        /// public override bool PassesSelfAssertion( Type testableType = null )
        /// {
        ///     if (!SkipSelfAssertion(testableType))
        ///     {
        ///         base.PassesSelfAssertion();
        ///         // Then add new assertions appropriate for this
        ///         //   class in addition to the assertions required
        ///         //   by the base class
        ///     }
        ///     return true;
        /// }
        /// </code>
        /// </para>
        /// </remarks>
        /// <param name="testableType">If testableType is non-null,
        /// and not precisely the type of this instance, then
        /// method is a null-op. Otherwise assertions are fired.</param>
        /// </summary>
        public virtual bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertion(testableType))
            {
                // Descendants should first call:
                // base.PassesSelfAssertion();
                // Then add new assertions appropriate for this
                //   class in addition to the assertions required
                //   by the base class
            }
            return true;
        }

        /// <summary>
        /// Determines whether or not assertions are
        /// truly to be fired.
        /// <remarks>
        /// Called only by 
        /// <see cref="PassesSelfAssertion(Type)"/>,
        /// in the base class and in overriding descendants. If
        /// the <see cref="IsFullyConstructed"/> property is
        /// false, returns false; otherwise if testableType
        /// is non-null AND not a perfect match for GetType(),
        /// returns false; otherwise returns true.
        /// </remarks>
        /// <param name="testableType">If testableType is non-null,
        /// and not precisely the type of this instance, then
        /// return false.</param>
        /// </summary>
        protected bool SkipSelfAssertion(Type testableType = null)
        {
            if (!IsFullyConstructed) return true;
            if (testableType == null) return false;
            return !testableType.Equals(GetType());
        }

        /// <summary>
        /// Implements 
        /// <see cref="ISelfAsserter.MarkAsFullyConstructed(Type, bool)"/>.
        /// </summary>
        public bool MarkAsFullyConstructed(
            Type testableType = null
            , bool assert = true
            )
        {
            if (
                testableType == null 
                || GetType().Equals(testableType)
                )
            {
                _isFullyConstructed = true;
                if (assert)
                    Debug.Assert(PassesSelfAssertion());
            }
            return true;
        }

    }

    /// <summary>
    /// Boilerplate implementation of ISelfAsserterImmutable.
    /// </summary>
    /// <remarks>
    /// Descendant classes should override:
    /// <list type="bullet">
    /// <item>SelfAsserterImmutable method overrides
    ///   <list type="bullet">
    ///   <item>ValidTypeForDelegee, unless abstract class.</item>
    ///   </list>
    /// </item>
    /// </list>
    /// </remarks>
    public abstract class SelfAsserterImmutable : Immutable, ISelfAsserter
    {

        private ISelfAsserter Delegee { get { return (ISelfAsserter)_delegee; } }

        private SelfAsserterImmutable() : base(null) { }

        /// <summary>
        /// Construct from delegee.
        /// </summary>
        /// <remarks>
        /// All observations documented at
        /// <see cref="Immutable.Immutable(object)"/>
        /// apply.
        /// </remarks>
        /// <param name="delegee">The object that
        /// will become the Immutable's delegee.</param>
        public SelfAsserterImmutable(ISelfAsserter delegee)
            : base(delegee) { }

        // ISelfAsserter methods

        /// <summary>
        /// Implements 
        /// <see cref="ISelfAsserter.IsFullyConstructed"/>.
        /// </summary>
        /// <remarks>Because it is always an
        /// error to exit the base constructor without
        /// being fully constructed, and because this
        /// method is never called before the base constructor
        /// has executed, this method always returns true
        /// for SelfAssertImmutables.</remarks>
        public override bool IsFullyConstructed { get { return true; } }

        /// <summary>
        /// Implements 
        /// <see cref="ISelfAsserter.PassesSelfAssertion(Type)"/>.
        /// </summary>
        public virtual bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(
                    _delegee != null
                    , "Delegee for SelfAsserterImmutable object "
                    + "can never be null."
                    );
                Debug.Assert(ObjectIsValidTypeForDelegee(_delegee, false));
                Debug.Assert(Delegee.PassesSelfAssertion());
            }
            return true;
        }

        /// <summary>
        /// Implements 
        /// <see cref="IImmutable.MarkAsFullyConstructed(Type)"/>.
        /// </summary>
        /// <remarks>Simply forwards method to 
        /// <see cref="MarkAsFullyConstructed(Type, bool)"/> 
        /// with assert defaulted to true.</remarks>
        public override bool MarkAsFullyConstructed(
            Type testableType = null
            )
        {
            return MarkAsFullyConstructed(testableType, true);
        }

        /// <summary>
        /// Implements 
        /// <see cref="ISelfAsserter.MarkAsFullyConstructed(Type, bool)"/>.
        /// </summary>
        /// <remarks>Because it is always an
        /// error to exit the base constructor without
        /// being fully constructed, and because this
        /// method is never called before the base constructor
        /// has executed, and because 
        /// <see cref="IsFullyConstructed"/> always returns true
        /// anyway, this method never has anything to
        /// do except fire off self-assertion. So that is
        /// all it ever does. For this reason it will almost
        /// always have to be overridden.</remarks>
        public virtual bool MarkAsFullyConstructed(
            Type testableType
            , bool assert
            )
        {
            if (assert && (testableType == null || testableType.Equals(GetType())))
                Debug.Assert(PassesSelfAssertion());
            return true;
        }

        /// <summary>
        /// Determines whether assertions should be fired.
        /// </summary>
        /// <remarks>
        /// Should not require overriding.
        /// </remarks>
        /// <param name="testableType">If specified,
        /// then assertions will not fire unless object is
        /// of exactly the specified type.</param>
        /// <returns></returns>
        protected bool SkipSelfAssertionUnlessThisType(Type testableType = null)
        {
            if (!IsFullyConstructed) return true;
            if (testableType == null) return false;
            return (!testableType.Equals(this.GetType()));
        }

        // ISafeMemberOfImmutableOwner properties and methods

        /// <summary>
        /// Implements 
        /// <see cref="ISafeMemberOfImmutableOwner.GetSafeReference(bool)"/>.
        /// </summary>
        /// <remarks>
        /// Should never require overriding.
        /// </remarks>
        public override ISafeMemberOfImmutableOwner GetSafeReference(bool assert) { return this; }

    }

}
