﻿using System;
using System.Reflection;
using NUnit.Framework;

// This tester is ready for use.

namespace Com.VeritasTS.CoreUtils.Test
{

    /// <summary>
    /// Base class tester for anything implementing the 
    /// Com.VeritasTS.TextAndErrorUtils.IImmutable interface.
    /// </summary>
    /// <remarks>Anyone implementing the
    /// Com.VeritasTS.TextAndErrorUtils.IImmutable must
    /// ensure that their implementation both fully honors
    /// the interface contract, and is proven to do so by
    /// thorough unit testing. 
    /// <para>
    /// Descendants must override:
    /// <list type="bullet">
    /// <item><see cref="TestableObject"/></item>
    /// <item><see cref="DifferentObjectEqualToTestableObject"/>, if TestableObject()
    /// does not create a new item each time</item>
    /// <item><see cref="SecondDifferentObjectEqualToTestableObject"/>, if TestableObject()
    /// does not create a new item each time</item>
    /// <item><see cref="ObjectOfTestableClassWithHashCodeNotEqualToTestableObject"/></item>
    /// <item><see cref="IncompletelyConstructedInstance"/> </item>
    /// <item><see cref="CompletelyConstructedInstance"/> </item>
    /// </list>
    /// </para>
    /// </remarks>
    public class BaseIImmutableTesterCore: EqualsAndHashCodeTesterCore
    {

        protected class Delegee
        {
            public override bool Equals(object obj)
            {
                return
                    obj == null ? false
                    : obj == this ? true
                    : (
                    obj.GetType().Equals(typeof(Delegee))
                    || obj.GetType().Equals(typeof(DelegeeImmutable))
                    );
            }
            public override int GetHashCode()
            {
                return 6271;
            }
        }

        protected class DelegeeImmutable : Immutable
        {
            private bool _isFullyConstructed;
            public DelegeeImmutable(object delegee)
                : base(delegee)
            {
                _isFullyConstructed = true;
            }
            public DelegeeImmutable(Delegee delegee, bool fullyConstructed)
                : base(delegee)
            {
                _isFullyConstructed = fullyConstructed;
            }
            public override bool IsFullyConstructed { get { return _isFullyConstructed; } }
            public override bool MarkAsFullyConstructed(Type testableType = null)
            {
                if (testableType == null || testableType.Equals(GetType()))
                    _isFullyConstructed = true;
                return true;
            }
            protected override Type ValidTypeForDelegee { get { return typeof(Delegee); } }
            public override bool Equals(object obj)
            {
                return _delegee.Equals(obj);
            }
            public override int GetHashCode()
            {
                return _delegee.GetHashCode();
            }
        }

        private class InnerSafeMemberOfInvariantOwnerTesterCore : BaseISafeMemberOfImmutableOwnerTesterCore
        {

            private BaseIImmutableTesterCore _owner;

            public InnerSafeMemberOfInvariantOwnerTesterCore(BaseIImmutableTesterCore owner)
                : base()
            {
                _owner = owner;
            }

            /// <summary>
            /// When a class implements ISafeMemberOfImmutableOwner, its associated
            /// BaseISafeMemberOfImmutableOwnerTester class must override
            /// this method to provide the base class tests with a testable
            /// instance of the new ISafeMemberOfImmutableOwner class.
            /// </summary>
            /// <returns>A testable object from the ISafeMemberOfImmutableOwner class
            /// undergoing validation.</returns>
            public override ISafeMemberOfImmutableOwner TestableObject()
            {
                return _owner.CompletelyConstructedInstance();
            }

        }

        /// <summary>
        /// Overrides
        /// <see cref="EqualsAndHashCodeTesterCore.TestableObject"/> 
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an object of the class under testing.</remarks>
        /// <returns>Testable object suitable for general testing.</returns>
        public override object TestableObject()
        {
            return CompletelyConstructedInstance();
        }

        /// <summary>
        /// Overrides
        /// <see cref="EqualsAndHashCodeTesterCore.TestableObject"/> 
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an object of the class under testing.</remarks>
        /// <returns>Testable object suitable for general testing.</returns>
        public override object ObjectOfTestableClassWithHashCodeNotEqualToTestableObject()
        {
            return null;
        }

        /// <summary>
        /// Returns IImmutable that has no lurking assertion failures
        /// but has not been marked as fully constructed.
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an object of the class under testing.</remarks>
        /// <returns>Testable IImmutable suitable for general testing.</returns>
        public virtual IImmutable IncompletelyConstructedInstance()
        {
            return new DelegeeImmutable(new Delegee(), false);
        }

        /// <summary>
        /// Returns IImmutable that has no lurking assertion failures
        /// and has been marked as fully constructed.
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an object of the class under testing.</remarks>
        /// <returns>Testable IImmutable suitable for general testing.</returns>
        public virtual IImmutable CompletelyConstructedInstance()
        {
            return new DelegeeImmutable(new Delegee(), true);
        }

        /***************************************************
         *** 
         *** 
         *** VALIDATION METHODS
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Ensures that MarkAsFullyConstructed(null) returns
        /// true and ensures that IsFullyConstructed is set to true
        /// when no assertion failures are lurking.
        /// </summary>
        /// <param name="preferenceSet">The object to be tested</param>
        public void MarkAsFullyConstructedWithNullParamEnsuresIsFullyComplete(IImmutable i)
        {
            if (i != null)
                Assert.True(
                    i.MarkAsFullyConstructed(null) && i.IsFullyConstructed
                    );
        }

        /// <summary>
        /// Ensures that x.MarkAsFullyConstructed(x.GetType()) returns
        /// true and ensures that IsFullyConstructed is set to true
        /// when no assertion failures are lurking.
        /// </summary>
        /// <param name="preferenceSet">The object to be tested</param>
        public void MarkXAsFullyConstructedWithTypeOfXParamEnsuresIsFullyComplete(IImmutable i)
        {
            if (i != null)
                Assert.True(
                    i.MarkAsFullyConstructed(i.GetType()) && i.IsFullyConstructed
                    , i.GetType().FullName
                    );
        }

        /// <summary>
        /// Ensures that x.MarkAsFullyConstructed(ancestor type of x) returns
        /// true and leaves IsFullyConstructed untouched
        /// when no assertion failures are lurking.
        /// </summary>
        /// <param name="preferenceSet">The object to be tested</param>
        public void MarkYAsFullyConstructedWithTypeOfXParamLeavesIsFullyConstructedUnchanged(IImmutable i)
        {
            if (i != null)
            {
                bool originalIFCValue = i.IsFullyConstructed;
                Assert.True(
                    i.MarkAsFullyConstructed(typeof(IImmutable))
                    && (i.IsFullyConstructed == originalIFCValue)
                    );
            }
        }

        /// <summary>
        /// Ensures that x.GetSafeReference() == x.
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an object of the class under testing.</remarks>
        /// <returns>Testable IImmutable suitable for general testing.</returns>
        public void GetSafeReferenceReturnsSelf(IImmutable i)
        {
            if (i != null)
                Assert.True(i == i.GetSafeReference());
        }

        /// <summary>
        /// Ensures that the IImmutable does not
        /// also implement IMutable.
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an object of the class under testing.</remarks>
        /// <returns>Testable IImmutable suitable for general testing.</returns>
        public void IsNotIMutable(IImmutable i)
        {
            if (i != null)
            {
                Type t = i.GetType();
                Assert.False(
                    typeof(IMutable).IsAssignableFrom(t)
                    , t.FullName
                    );
            }
        }

        /***************************************************
         *** 
         *** 
         *** TEST CASES
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Ensures that where IsFullyConstructed is false and no assertion failures
        /// are lurking, MarkAsFullyConstructed(null) 
        /// returns true and causes IsFullyConstructed to become true.
        /// </summary>
        // [TestCase]
        public void MarkAsFullyConstructedWithNullParamEnsuresIsFullyComplete()
        {
            MarkAsFullyConstructedWithNullParamEnsuresIsFullyComplete(IncompletelyConstructedInstance());
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true and no assertion failures
        /// are lurking, MarkAsFullyConstructed(null) 
        /// returns true and leaves IsFullyConstructed set to true.
        /// </summary>
        // [TestCase]
        public void MarkAsFullyConstructedWithNullParamLeavesConstructionComplete()
        {
            MarkAsFullyConstructedWithNullParamEnsuresIsFullyComplete(CompletelyConstructedInstance());
        }

        /// <summary>
        /// Ensures that where object is of type X and IsFullyConstructed is false and no assertion failures
        /// are lurking, MarkAsFullyConstructed(X) returns true and causes IsFullyConstructed to become true.
        /// </summary>
        // [TestCase]
        public void MarkXAsFullyConstructedWithTypeOfXParamEnsuresIsFullyComplete()
        {
            MarkXAsFullyConstructedWithTypeOfXParamEnsuresIsFullyComplete(IncompletelyConstructedInstance());
        }

        /// <summary>
        /// Ensures that where object is of type X and IsFullyConstructed is true and no assertion failures
        /// are lurking, MarkAsFullyConstructed(X) returns true and leaves IsFullyConstructed set to true.
        /// </summary>
        // [TestCase]
        public void MarkXAsFullyConstructedWithTypeOfXParamLeavesConstructionComplete()
        {
            MarkXAsFullyConstructedWithTypeOfXParamEnsuresIsFullyComplete(CompletelyConstructedInstance());
        }

        /// <summary>
        /// Ensures that where object is of type Y descended from X and IsFullyConstructed is false and no assertion failures
        /// are lurking, MarkAsFullyConstructed(X) returns true and leaves IsFullyConstructed set to false.
        /// </summary>
        // [TestCase]
        public void MarkYAsFullyConstructedWithTypeOfXParamLeavesIsFullyConstructedUnchanged()
        {
            MarkYAsFullyConstructedWithTypeOfXParamLeavesIsFullyConstructedUnchanged(IncompletelyConstructedInstance());
        }

        /// <summary>
        /// Ensures that where object is of type Y descended from X and IsFullyConstructed is true and no assertion failures
        /// are lurking, MarkAsFullyConstructed(X) returns true and leaves IsFullyConstructed set to true.
        /// </summary>
        // [TestCase]
        public void MarkYAsFullyConstructedWithTypeOfXParamLeavesConstructionComplete()
        {
            MarkYAsFullyConstructedWithTypeOfXParamLeavesIsFullyConstructedUnchanged(CompletelyConstructedInstance());
        }

        /// <summary>
        /// Ensures that x.GetSafeReference() == x.
        /// </summary>
        // [TestCase]
        public void GetSafeReferenceReturnsSelf()
        {
            GetSafeReferenceReturnsSelf(CompletelyConstructedInstance());
        }

        /// <summary>
        /// Ensures that the IImmutable does not
        /// also implement IMutable.
        /// </summary>
        // [TestCase]
        public void IsNotIMutable()
        {
            IsNotIMutable(CompletelyConstructedInstance());
        }

        /// <summary>
        /// Ensures that class passes all tests for 
        /// ISafeMemberOfImmutableOwner.
        /// </summary>
        // [TestCase]
        public void ValidateAsSafeMemberOfImmutableOwner()
        {
            BaseISafeMemberOfImmutableOwnerTesterCore safe
                = new InnerSafeMemberOfInvariantOwnerTesterCore(this);
            (new InnerSafeMemberOfInvariantOwnerTesterCore(this))
                .Validate(CompletelyConstructedInstance());
        }

        /***************************************************
         *** 
         *** 
         *** MASTER TEST METHOD
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Applies all contract validation to the object passed in as the testableObject
        /// argument.
        /// </summary>
        /// <remarks>
        /// Used when IImmutable is one of several interfaces
        /// implemented and the implementing class's Tester
        /// class includes BaseIImmutableTester by composition
        /// rather than inheritance.
        /// </remarks>
        /// <param name="testableObject">The object to be tested</param>
        public virtual void Validate()
        {
            MarkAsFullyConstructedWithNullParamEnsuresIsFullyComplete();
            MarkAsFullyConstructedWithNullParamLeavesConstructionComplete();
            MarkXAsFullyConstructedWithTypeOfXParamEnsuresIsFullyComplete();
            MarkXAsFullyConstructedWithTypeOfXParamLeavesConstructionComplete();
            MarkYAsFullyConstructedWithTypeOfXParamLeavesIsFullyConstructedUnchanged();
            MarkYAsFullyConstructedWithTypeOfXParamLeavesConstructionComplete();
            GetSafeReferenceReturnsSelf();
            IsNotIMutable();
            ValidateAsSafeMemberOfImmutableOwner();
        }

    }

    /// <summary>
    /// Base class tester for anything implementing the 
    /// Com.VeritasTS.TextAndErrorUtils.IImmutable interface.
    /// </summary>
    /// <remarks>Anyone implementing the
    /// Com.VeritasTS.TextAndErrorUtils.IImmutable must
    /// ensure that their implementation both fully honors
    /// the interface contract, and is proven to do so by
    /// thorough unit testing. 
    /// </remarks>
    [TestFixture]
    public class BaseIImmutableTester : EqualsAndHashCodeTester
    {

        /***************************************************
         *** 
         *** 
         *** TEST CASES
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Ensures that where IsFullyConstructed is false and no assertion failures
        /// are lurking, MarkAsFullyConstructed(null) 
        /// returns true and causes IsFullyConstructed to become true.
        /// </summary>
        [TestCase]
        public void MarkAsFullyConstructedWithNullParamEnsuresIsFullyComplete()
        {
            (new BaseIImmutableTesterCore())
            .MarkAsFullyConstructedWithNullParamEnsuresIsFullyComplete();
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true and no assertion failures
        /// are lurking, MarkAsFullyConstructed(null) 
        /// returns true and leaves IsFullyConstructed set to true.
        /// </summary>
        [TestCase]
        public void MarkAsFullyConstructedWithNullParamLeavesConstructionComplete()
        {
            (new BaseIImmutableTesterCore())
            .MarkAsFullyConstructedWithNullParamLeavesConstructionComplete();
        }

        /// <summary>
        /// Ensures that where object is of type X and IsFullyConstructed is false and no assertion failures
        /// are lurking, MarkAsFullyConstructed(X) returns true and causes IsFullyConstructed to become true.
        /// </summary>
        [TestCase]
        public void MarkXAsFullyConstructedWithTypeOfXParamEnsuresIsFullyComplete()
        {
            (new BaseIImmutableTesterCore())
            .MarkXAsFullyConstructedWithTypeOfXParamEnsuresIsFullyComplete();
        }

        /// <summary>
        /// Ensures that where object is of type X and IsFullyConstructed is true and no assertion failures
        /// are lurking, MarkAsFullyConstructed(X) returns true and leaves IsFullyConstructed set to true.
        /// </summary>
        [TestCase]
        public void MarkXAsFullyConstructedWithTypeOfXParamLeavesConstructionComplete()
        {
            (new BaseIImmutableTesterCore())
            .MarkXAsFullyConstructedWithTypeOfXParamLeavesConstructionComplete();
        }

        /// <summary>
        /// Ensures that where object is of type Y descended from X and IsFullyConstructed is false and no assertion failures
        /// are lurking, MarkAsFullyConstructed(X) returns true and leaves IsFullyConstructed set to false.
        /// </summary>
        [TestCase]
        public void MarkYAsFullyConstructedWithTypeOfXParamLeavesIsFullyConstructedUnchanged()
        {
            (new BaseIImmutableTesterCore())
            .MarkYAsFullyConstructedWithTypeOfXParamLeavesIsFullyConstructedUnchanged();
        }

        /// <summary>
        /// Ensures that where object is of type Y descended from X and IsFullyConstructed is true and no assertion failures
        /// are lurking, MarkAsFullyConstructed(X) returns true and leaves IsFullyConstructed set to true.
        /// </summary>
        [TestCase]
        public void MarkYAsFullyConstructedWithTypeOfXParamLeavesConstructionComplete()
        {
            (new BaseIImmutableTesterCore())
            .MarkYAsFullyConstructedWithTypeOfXParamLeavesConstructionComplete();
        }

        /// <summary>
        /// Ensures that x.GetSafeReference() == x.
        /// </summary>
        [TestCase]
        public void GetSafeReferenceReturnsSelf()
        {
            (new BaseIImmutableTesterCore())
            .GetSafeReferenceReturnsSelf();
        }

        /// <summary>
        /// Ensures that the IImmutable does not
        /// also implement IMutable.
        /// </summary>
        [TestCase]
        public void IsNotIMutable()
        {
            (new BaseIImmutableTesterCore())
            .IsNotIMutable();
        }

        /// <summary>
        /// Ensures that class passes all tests for 
        /// ISafeMemberOfImmutableOwner.
        /// </summary>
        [TestCase]
        public void ValidateAsSafeMemberOfImmutableOwner()
        {
            (new BaseIImmutableTesterCore())
            .ValidateAsSafeMemberOfImmutableOwner();
        }

        /***************************************************
         *** 
         *** 
         *** MASTER TEST METHOD
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Applies all contract validation to the object passed in as the testableObject
        /// argument.
        /// </summary>
        /// <remarks>
        /// Used when IImmutable is one of several interfaces
        /// implemented and the implementing class's Tester
        /// class includes BaseIImmutableTester by composition
        /// rather than inheritance.
        /// </remarks>
        /// <param name="testableObject">The object to be tested</param>
        public virtual void Validate()
        {
            MarkAsFullyConstructedWithNullParamEnsuresIsFullyComplete();
            MarkAsFullyConstructedWithNullParamLeavesConstructionComplete();
            MarkXAsFullyConstructedWithTypeOfXParamEnsuresIsFullyComplete();
            MarkXAsFullyConstructedWithTypeOfXParamLeavesConstructionComplete();
            MarkYAsFullyConstructedWithTypeOfXParamLeavesIsFullyConstructedUnchanged();
            MarkYAsFullyConstructedWithTypeOfXParamLeavesConstructionComplete();
            GetSafeReferenceReturnsSelf();
            IsNotIMutable();
            ValidateAsSafeMemberOfImmutableOwner();
        }

    }

    /// <summary>
    /// Base class tester for 
    /// Com.VeritasTS.TextAndErrorUtils.Immutable interface.
    /// </summary>
    /// <remarks>
    /// Descendent classes must override:
    /// <list type="bullet">
    /// <item><see cref="TypeUnderTesting"/> </item>
    /// <item><see cref="DelegeeForTypeUnderTesting"/> </item>
    /// <item><see cref="DelegeeTypeForTypeUnderTesting"/> </item>
    /// </list>
    /// </remarks>
    public class ImmutableTesterCore : BaseIImmutableTesterCore
    {

        /// <summary>
        /// Returns Type object for class whose construct-from-delegee
        /// constructor is to be tested.
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an object of the class under testing.</remarks>
        /// <returns>Testable type that implements IImmutable.</returns>
        public virtual Type TypeUnderTesting()
        {
            return typeof(DelegeeImmutable);
        }

        /// <summary>
        /// Returns a fully-constructed object that can serve as
        /// delegee for class whose construct-from-delegee
        /// constructor is to be tested.
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an object of the class under testing.</remarks>
        /// <returns>Object that can be used as
        /// delegee for type under testing.</returns>
        public virtual object DelegeeForTypeUnderTesting()
        {
            return new Delegee();
        }

        /// <summary>
        /// Returns Type object for class that can serve as
        /// delegee for class whose construct-from-delegee
        /// constructor is to be tested.
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an object of the class under testing.</remarks>
        /// <returns>Type that can be used as
        /// delegee for type under testing.</returns>
        public virtual Type DelegeeTypeForTypeUnderTesting()
        {
            return DelegeeForTypeUnderTesting().GetType();
        }

        /// <summary>
        /// Class that allows construction from delegee
        /// not of acceptable type or from null
        /// </summary>
        /// <remarks>
        /// Tests involving this class do NOT need to be
        /// run for descendant classes other than base classes,
        /// as they are simply
        /// to test the base-class constructor error-trapping
        /// for Immutable.</remarks>
        protected class BadDelegeeImmutable : DelegeeImmutable
        {
            public BadDelegeeImmutable(ImmutableTesterCore outerOwner, object delegee) : base(delegee)
            {
                OuterOwner = outerOwner;
            }
            protected override Type ValidTypeForDelegee
            {
                get { return (new ImmutableTesterCore()).DelegeeTypeForTypeUnderTesting(); }
            }
            public ImmutableTesterCore OuterOwner { get; }
        }

        /***************************************************
         *** 
         *** 
         *** VALIDATION METHODS
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Ensures that where delegee param is a valid delegee type, constructor completes
        /// successfully and fully constructs object.
        /// </summary>
        /// <remarks>
        /// If the class under testing has no constructor that takes only a delegee as
        /// the sole parameter, override this method to return null.
        /// </remarks>
        /// <param name="immutableType">The type object for the immutable class under testing.</param>
        /// <param name="delegeeType">The type object for the valid delegee class for the immutable class under testing.</param>
        public void ConstructionFromValidDelegeeFullyCompletesConstruction(object delegee, Type immutableType)
        {
            if (immutableType != null)
            {
                ConstructorInfo constructor = immutableType.GetConstructor(new Type[] { DelegeeTypeForTypeUnderTesting() });
                IImmutable i = (IImmutable)constructor.Invoke(new object[] { delegee });
                Assert.True(
                    delegee != null
                    && i != null
                    && i.IsFullyConstructed
                    );
            }
        }

        /***************************************************
         *** 
         *** 
         *** TEST CASES
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Ensures that where base constructor delegee param is null, 
        /// base constructor throws ArgumentNullException.
        /// </summary>
        /// <remarks>
        /// This test need be run only in the base class tester
        /// as it validates the behavior of the base class constructor
        /// but is not binding on the behavior of descendant class
        /// constructors.
        /// </remarks>
        public void ConstructionFromNullDelegeeCausesArgumentNullException()
        {
            if (GetType().Equals(typeof(ImmutableTesterCore)))
            {
                bool argumentNullExceptionThrown = false;
                try
                {
                    BadDelegeeImmutable x = new BadDelegeeImmutable(this, null);
                }
                catch (ArgumentNullException)
                {
                    argumentNullExceptionThrown = true;
                }
                Assert.True(argumentNullExceptionThrown);
            }
        }

        /// <summary>
        /// Ensures that where base constructor delegee param is not a valid 
        /// delegee type, constructor throws BadDelegeeOfImmutableTypeException.
        /// </summary>
        /// <remarks>
        /// This test need be run only in the base class tester
        /// as it validates the behavior of the base class constructor
        /// but is not binding on the behavior of descendant class
        /// constructors.
        /// </remarks>
        public void ConstructionFromUnacceptableDelegeeCausesBadDelegeeOfImmutableTypeException()
        {
            if (GetType().Equals(typeof(ImmutableTesterCore)))
            {
                bool badDelegeeOfImmutableTypeExceptionThrown = false;
                try
                {
                    BadDelegeeImmutable x = new BadDelegeeImmutable(this, this);
                }
                catch (BadDelegeeOfImmutableTypeException)
                {
                    badDelegeeOfImmutableTypeExceptionThrown = true;
                }
                Assert.True(badDelegeeOfImmutableTypeExceptionThrown);
            }
        }

        /// <summary>
        /// Ensures that where delegee param is a valid delegee type, constructor completes
        /// successfully and fully constructs object.
        /// </summary>
        public void ConstructionFromValidDelegeeFullyCompletesConstruction()
        {
            ConstructionFromValidDelegeeFullyCompletesConstruction(
                DelegeeForTypeUnderTesting()
                , TypeUnderTesting()
                );
        }

        /***************************************************
         *** 
         *** 
         *** MASTER TEST METHOD
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Applies all contract validation to the object passed in as the testableObject
        /// argument.
        /// </summary>
        /// <remarks>
        /// Used when Immutable functionality is added to
        /// a class by composition rather than by inheritance.
        /// </remarks>
        public override void Validate()
        {
            base.Validate();
            ConstructionFromNullDelegeeCausesArgumentNullException();
            ConstructionFromUnacceptableDelegeeCausesBadDelegeeOfImmutableTypeException();
            ConstructionFromValidDelegeeFullyCompletesConstruction();
        }

    }

    /// <summary>
    /// Base class tester for 
    /// Com.VeritasTS.TextAndErrorUtils.Immutable interface.
    /// </summary>
    /// <remarks>
    /// Descendent classes must override:
    /// <list type="bullet">
    /// <item><see cref="TypeUnderTesting"/> </item>
    /// <item><see cref="DelegeeForTypeUnderTesting"/> </item>
    /// <item><see cref="DelegeeTypeForTypeUnderTesting"/> </item>
    /// </list>
    /// </remarks>
    [TestFixture]
    public class ImmutableTester : BaseIImmutableTester
    {

        /***************************************************
         *** 
         *** 
         *** TEST CASES
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Ensures that where base constructor delegee param is null, 
        /// base constructor throws ArgumentNullException.
        /// </summary>
        /// <remarks>
        /// This test need be run only in the base class tester
        /// as it validates the behavior of the base class constructor
        /// but is not binding on the behavior of descendant class
        /// constructors.
        /// </remarks>
        [TestCase]
        public void ConstructionFromNullDelegeeCausesArgumentNullException()
        {
            (new ImmutableTesterCore())
                .ConstructionFromNullDelegeeCausesArgumentNullException();
        }

        /// <summary>
        /// Ensures that where base constructor delegee param is not a valid 
        /// delegee type, constructor throws BadDelegeeOfImmutableTypeException.
        /// </summary>
        /// <remarks>
        /// This test need be run only in the base class tester
        /// as it validates the behavior of the base class constructor
        /// but is not binding on the behavior of descendant class
        /// constructors.
        /// </remarks>
        [TestCase]
        public void ConstructionFromUnacceptableDelegeeCausesBadDelegeeOfImmutableTypeException()
        {
            if (GetType().Equals(typeof(ImmutableTester)))
                (new ImmutableTesterCore())
                    .ConstructionFromUnacceptableDelegeeCausesBadDelegeeOfImmutableTypeException();
        }

        /// <summary>
        /// Ensures that where delegee param is a valid delegee type, constructor completes
        /// successfully and fully constructs object.
        /// </summary>
        [TestCase]
        public void ConstructionFromValidDelegeeFullyCompletesConstruction()
        {
            (new ImmutableTesterCore())
                .ConstructionFromValidDelegeeFullyCompletesConstruction();
        }

        /***************************************************
         *** 
         *** 
         *** MASTER TEST METHOD
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Applies all contract validation to the object passed in as the testableObject
        /// argument.
        /// </summary>
        /// <remarks>
        /// Used when Immutable functionality is added to
        /// a class by composition rather than by inheritance.
        /// </remarks>
        public override void Validate()
        {
            (new ImmutableTesterCore())
                .Validate();
        }

    }

}
