﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.VeritasTS.CoreUtils;
using NUnit.Framework;

// This tester is ready for use.

namespace Com.VeritasTS.CoreUtils.Test
{

    /// <summary>
    /// Base class tester core for anything overriding
    /// object.Equals and object.GetHashCode.
    /// </summary>
    /// <remarks>
    /// Descendants must override:
    /// <list type="bullet">
    /// <item><see cref="TestableObject"/></item>
    /// <item><see cref="DifferentObjectEqualToTestableObject"/>, if TestableObject()
    /// does not create a new item each time.</item>
    /// <item><see cref="SecondDifferentObjectEqualToTestableObject"/>, if TestableObject()
    /// does not create a new item each time.</item>
    /// <item><see cref="ObjectOfTestableClassWithHashCodeNotEqualToTestableObject"/></item>
    /// </list>
    /// </remarks>
    public class EqualsAndHashCodeTesterCore
    {

        private class TestClass
        {
            public bool B { get; }
            public TestClass( bool b = true ) { B = b; }
            public override bool Equals(object obj)
            {
                if (obj == null) return false;
                if (obj == this) return true;
                if (!obj.GetType().Equals(GetType())) return false;
                return (B == ((TestClass)obj).B);
            }
            public override int GetHashCode()
            {
                return (B ? 1 : 0);
            }
        }

        /// <summary>
        /// An ordinary object of the class to be tested.
        /// </summary>
        /// <returns>A testable object from the class
        /// undergoing validation.</returns>
        public virtual object TestableObject()
        {
            return new TestClass();
        }

        /// <summary>
        /// An object from the class to be tested that is
        /// equal to TestableObject() but is not the same
        /// instance as TestableObject().
        /// </summary>
        /// <returns>A testable object from the ISafeMemberOfImmutableOwner class
        /// undergoing validation.</returns>
        public virtual object DifferentObjectEqualToTestableObject()
        {
            return TestableObject();
        }

        /// <summary>
        /// An object from the class to be tested that is
        /// equal to TestableObject() but is not the same
        /// instance as either TestableObject() or
        /// DifferentObjectEqualToTestableObject().
        /// </summary>
        /// <returns>A testable object from the ISafeMemberOfImmutableOwner class
        /// undergoing validation.</returns>
        public virtual object SecondDifferentObjectEqualToTestableObject()
        {
            return TestableObject();
        }

        /// <summary>
        /// An object of the class to be tested, one whose hash code is not
        /// equal to the hash code of TestableObject().
        /// </summary>
        /// <returns>A testable object from the ISafeMemberOfImmutableOwner class
        /// undergoing validation.</returns>
        public virtual object ObjectOfTestableClassWithHashCodeNotEqualToTestableObject()
        {
            return new TestClass(false);
        }

        /***************************************************
         *** 
         *** 
         *** TEST CASES
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Enforces the contract provision that 
        /// no object is equal to null.
        /// </summary>
        [TestCase]
        public void XNotEqualToNull()
        {
            object x = TestableObject();
            if (x != null)
            {
                Assert.False(x.Equals(null));
            }
        }

        /// <summary>
        /// Enforces the contract provision that 
        /// x.Equals(x).
        /// </summary>
        [TestCase]
        public void XEqualsX()
        {
            object x = TestableObject();
            if (x != null)
                Assert.True(x.Equals(x));
        }

        /// <summary>
        /// Enforces the contract provision that 
        /// x.Equals(any object with the same reference as x).
        /// </summary>
        [TestCase]
        public void XEqualsObjectWithSameReference()
        {
            object x = TestableObject();
            if (x != null)
            {
                object y = x;
                Assert.True(x.Equals(y));
            }
        }

        /// <summary>
        /// Enforces symmetry: if x and y are not the 
        /// same object (x != y) but x.Equals(y), then y.Equals(x).
        /// </summary>
        /// <remarks>
        /// Note that in descendants, additional testing may be required
        /// to ensure that, for example, base class instances do not think
        /// they are equal to descendant class instances that have extra
        /// members and consider themselves therefore unequal.</remarks>
        [TestCase]
        public void WhenXEqualsYYEqualsX()
        {
            object x = TestableObject();
            if (x != null)
            {
                object y = DifferentObjectEqualToTestableObject();
                Assert.True(x.Equals(y) && y.Equals(x));
            }
        }

        /// <summary>
        /// Enforces transitivity: if x.Equals(y) and y.Equals(z),
        /// then x.Equals(z).
        /// </summary>
        [TestCase]
        public void WhenXEqualsYAndYEqualsZThenXEqualsZ()
        {
            object x = TestableObject();
            if (x != null)
            {
                object y = DifferentObjectEqualToTestableObject();
                if (y != null)
                {
                    object z = SecondDifferentObjectEqualToTestableObject();
                    Assert.True(
                        x.Equals(y) && y.Equals(z) && x.Equals(z)
                        , x.GetType().FullName
                        );
                }
            }
        }

        /// <summary>
        /// Enforces condition that two objects whose hash codes
        /// are not equal cannot themselves be equal.
        /// </summary>
        [TestCase]
        public void WhenHashCodesAreUnequalObjectsAreUnequal()
        {
            object x = TestableObject();
            if (x != null)
            {
                object y = ObjectOfTestableClassWithHashCodeNotEqualToTestableObject();
                if (y != null )
                {
                    Assert.True(
                        x.GetHashCode() != y.GetHashCode()
                        && !x.Equals(y)
                        , x.GetType().FullName
                        );
                }
            }
        }

        /***************************************************
         *** 
         *** 
         *** MASTER TEST METHOD
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Enforces condition that two objects whose hash codes
        /// are not equal cannot themselves be equal.
        /// </summary>
        public virtual void ExecuteAllTests()
        {
            XNotEqualToNull();
            XEqualsX();
            XEqualsObjectWithSameReference();
            WhenXEqualsYYEqualsX();
            WhenXEqualsYAndYEqualsZThenXEqualsZ();
            WhenHashCodesAreUnequalObjectsAreUnequal();
        }

    }

    /// <summary>
    /// Base class tester for anything overriding
    /// object.Equals and object.GetHashCode.
    /// </summary>
    /// <remarks>
    /// Descendants must override the Core() method
    /// to provide a suitably overridden
    /// descendant of EqualsAndHashCodeTesterCore.
    /// </remarks>
    [TestFixture]
    public class EqualsAndHashCodeTester
    {

        protected virtual EqualsAndHashCodeTesterCore Core()
        {
            return new EqualsAndHashCodeTesterCore();
        }

        /***************************************************
         *** 
         *** 
         *** TEST CASES
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Enforces the contract provision that 
        /// no object is equal to null.
        /// </summary>
        [TestCase]
        public void XNotEqualToNull()
        {
            Core().XNotEqualToNull();
        }

        /// <summary>
        /// Enforces the contract provision that 
        /// x.Equals(x).
        /// </summary>
        [TestCase]
        public void XEqualsX()
        {
            Core().XEqualsX();
        }

        /// <summary>
        /// Enforces the contract provision that 
        /// x.Equals(any object with the same reference as x).
        /// </summary>
        [TestCase]
        public void XEqualsObjectWithSameReference()
        {
            Core().XEqualsObjectWithSameReference();
        }

        /// <summary>
        /// Enforces symmetry: if x and y are not the 
        /// same object (x != y) but x.Equals(y), then y.Equals(x).
        /// </summary>
        /// <remarks>
        /// Note that in descendants, additional testing may be required
        /// to ensure that, for example, base class instances do not think
        /// they are equal to descendant class instances that have extra
        /// members and consider themselves therefore unequal.</remarks>
        [TestCase]
        public void WhenXEqualsYYEqualsX()
        {
            Core().WhenXEqualsYYEqualsX();
        }

        /// <summary>
        /// Enforces transitivity: if x.Equals(y) and y.Equals(z),
        /// then x.Equals(z).
        /// </summary>
        [TestCase]
        public void WhenXEqualsYAndYEqualsZThenXEqualsZ()
        {
            Core().WhenXEqualsYAndYEqualsZThenXEqualsZ();
        }

        /// <summary>
        /// Enforces condition that two objects whose hash codes
        /// are not equal cannot themselves be equal.
        /// </summary>
        [TestCase]
        public void WhenHashCodesAreUnequalObjectsAreUnequal()
        {
            Core().WhenHashCodesAreUnequalObjectsAreUnequal();
        }

        /***************************************************
         *** 
         *** 
         *** MASTER TEST METHOD
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Executes all test cases.
        /// </summary>
        [TestCase]
        public virtual void ExecuteAllTests()
        {
            Core().ExecuteAllTests();
        }

    }

}
