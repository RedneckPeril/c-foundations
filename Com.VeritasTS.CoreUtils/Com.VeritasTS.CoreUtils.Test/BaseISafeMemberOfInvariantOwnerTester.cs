﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.VeritasTS.CoreUtils;
using NUnit.Framework;

// This tester is ready for use.

namespace Com.VeritasTS.CoreUtils.Test
{

    /// <summary>
    /// Base class tester for anything implementing the 
    /// Com.VeritasTS.CoreUtils.ISafeMemberOfImmutableOwner interface.
    /// </summary>
    /// <remarks>Anyone implementing the
    /// Com.VeritasTS.CoreUtils.ISafeMemberOfImmutableOwner must
    /// ensure that their implementation both fully honors
    /// the interface contract, and is proven to do so by
    /// thorough unit testing. Any code path that is followed within
    /// GetSafeReference must result in an object that honors this
    /// contract.
    /// <para>As the contract is a very simple one the base class 
    /// tests require the descendant to provide only a single 
    /// testable object, retrieved by the base class through
    /// the TestableObject() virtual method.</para></remarks>
    public class BaseISafeMemberOfImmutableOwnerTesterCore
    {

        private class Testable : ISafeMemberOfImmutableOwner
        {
            public Testable() { }
            public override bool Equals(object obj)
            {
                return
                    obj == null ? false
                    : obj == this ? true
                    : obj.GetType() == GetType();
            }
            public override int GetHashCode()
            {
                return 1;
            }
            public ISafeMemberOfImmutableOwner GetSafeReference(bool assert = true)
            {
                return this;
            }
        }

        /// <summary>
        /// When a class implements ISafeMemberOfImmutableOwner, its associated
        /// BaseISafeMemberOfImmutableOwnerTester class must override
        /// this method to provide the base class tests with a testable
        /// instance of the new ISafeMemberOfImmutableOwner class.
        /// </summary>
        /// <returns>A testable object from the ISafeMemberOfImmutableOwner class
        /// undergoing validation.</returns>
        public virtual ISafeMemberOfImmutableOwner TestableObject()
        {
            return new Testable();
        }

        /// <summary>
        /// Ensures that GetSafeReference does not return a null reference
        /// when called on the testableObject argument.
        /// </summary>
        /// <param name="testableObject">The object to be tested</param>
        public void SafeReferenceIsNotNull(ISafeMemberOfImmutableOwner testableObject)
        {
            Assert.True(
                testableObject.GetSafeReference() != null
                , testableObject.GetType().FullName
                );
        }

        /// <summary>
        /// Enforces the contract provision that 
        /// GetSafeReference() returns an object that Equals
        /// the object passed in as the testableObject
        /// argument.
        /// </summary>
        /// <param name="testableObject">The object to be tested</param>
        public void SelfEqualsSafeReference(ISafeMemberOfImmutableOwner testableObject)
        {
            Assert.True(
                testableObject.Equals(testableObject.GetSafeReference())
                , testableObject.GetType().FullName
                );
        }

        /// <summary>
        /// Enforces the contract provision that 
        /// GetSafeReference() returns an object whose type Equals
        /// the type of the object passed in as the testableObject
        /// argument.
        /// </summary>
        /// <param name="testableObject">The object to be tested</param>
        public void TypeOfSelfEqualsTypeOfSafeReference(ISafeMemberOfImmutableOwner testableObject)
        {
            ISafeMemberOfImmutableOwner safeReference = testableObject.GetSafeReference();
            Assert.True(
                testableObject.GetType().Equals(safeReference.GetType())
                , testableObject.GetType().FullName
                );
        }

        /// <summary>
        /// Ensures that GetSafeReference does not return a null reference
        /// when called on the object returned by TestableObject().
        /// </summary>
        public void SafeReferenceIsNotNull()
        {
            SafeReferenceIsNotNull(TestableObject());
        }

        /// <summary>
        /// Enforces the contract provision that 
        /// GetSafeReference() returns an object that Equals
        /// the object returned by TestableObject().
        /// </summary>
        public void SelfEqualsSafeReference()
        {
            SelfEqualsSafeReference(TestableObject());
        }

        /// <summary>
        /// Enforces the contract provision that 
        /// GetSafeReference() returns an object whose type Equals
        /// the type of the object returned by TestableObject().
        /// </summary>
        /// <param name="testableObject">The object to be tested</param>
        public void TypeOfSelfEqualsTypeOfSafeReference()
        {
            TypeOfSelfEqualsTypeOfSafeReference(TestableObject());
        }

        /***************************************************
         *** 
         *** 
         *** MASTER TEST METHOD
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Applies all contract validation to the object passed in as the testableObject
        /// argument.
        /// </summary>
        /// <remarks>
        /// Used when ISafeMemberOfImmutableOwner is one of several interfaces
        /// implemented and the implementing class's Tester
        /// class includes BaseISafeMemberOfImmutableOwnerTester by composition
        /// rather than inheritance.
        /// </remarks>
        /// <param name="testableObject">The object to be tested</param>
        public void Validate(ISafeMemberOfImmutableOwner testableObject)
        {
            SafeReferenceIsNotNull(testableObject);
            SelfEqualsSafeReference(testableObject);
            TypeOfSelfEqualsTypeOfSafeReference(testableObject);
        }

    }

    /// <summary>
    /// Base class tester for anything implementing the 
    /// Com.VeritasTS.CoreUtils.ISafeMemberOfImmutableOwner interface.
    /// </summary>
    /// <remarks>Anyone implementing the
    /// Com.VeritasTS.CoreUtils.ISafeMemberOfImmutableOwner must
    /// ensure that their implementation both fully honors
    /// the interface contract, and is proven to do so by
    /// thorough unit testing. Any code path that is followed within
    /// GetSafeReference must result in an object that honors this
    /// contract.
    /// <para>As the contract is a very simple one the base class 
    /// tests require the descendant to provide only a single 
    /// testable object, retrieved by the base class through
    /// the TestableObject() virtual method.</para></remarks>
    [TestFixture]
    public class BaseISafeMemberOfImmutableOwnerTester
    {

        /// <summary>
        /// Ensures that GetSafeReference does not return a null reference
        /// when called on the object returned by TestableObject().
        /// </summary>
        [TestCase]
        public void SafeReferenceIsNotNull()
        {
            (new BaseISafeMemberOfImmutableOwnerTesterCore())
                .SafeReferenceIsNotNull();
        }

        /// <summary>
        /// Enforces the contract provision that 
        /// GetSafeReference() returns an object that Equals
        /// the object returned by TestableObject().
        /// </summary>
        [TestCase]
        public void SelfEqualsSafeReference()
        {
            (new BaseISafeMemberOfImmutableOwnerTesterCore())
                .SelfEqualsSafeReference();
        }

        /// <summary>
        /// Enforces the contract provision that 
        /// GetSafeReference() returns an object whose type Equals
        /// the type of the object returned by TestableObject().
        /// </summary>
        /// <param name="testableObject">The object to be tested</param>
        [TestCase]
        public void TypeOfSelfEqualsTypeOfSafeReference()
        {
            (new BaseISafeMemberOfImmutableOwnerTesterCore())
                .TypeOfSelfEqualsTypeOfSafeReference();
        }

    }

}
