﻿using System;
using System.Reflection;
using NUnit.Framework;

// This tester is ready for use.

namespace Com.VeritasTS.CoreUtils.Test
{

    /// <summary>
    /// Base class tester for anything implementing the 
    /// Com.VeritasTS.TextAndErrorUtils.IMutable interface.
    /// </summary>
    /// <remarks>Anyone implementing the
    /// Com.VeritasTS.TextAndErrorUtils.IMutable must
    /// ensure that their implementation both fully honors
    /// the interface contract, and is proven to do so by
    /// thorough unit testing. 
    /// <para>Only the normal virtual TestableObject()
    /// method is required; there are no special cases to be tested
    /// at this level.</para>
    /// </remarks>
    public class BaseIMutableTesterCore
    {

        protected class TestMutable : IMutable //, IImmutable
        {
            //public bool IsFullyConstructed { get { return true; } }
            //public bool MarkAsFullyConstructed(Type testableType = null) { return true; }
            //public ISafeMemberOfImmutableOwner GetSafeReference(bool assert = true) { return null; }
        }

        /// <summary>
        /// Returns instance of class under testing.
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an object of the class under testing.</remarks>
        /// <returns>Testable IMutable suitable for general testing.</returns>
        public virtual IMutable TestableObject()
        {
            return new TestMutable();
        }

        /***************************************************
         *** 
         *** 
         *** VALIDATION METHODS
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Ensures that TestableObject does not
        /// implement IImmutable.
        /// </summary>
        /// <param name="preferenceSet">The object to be tested</param>
        public void NotAnIImmutable(IMutable i)
        {
            if (i != null)
            {
                Type t = i.GetType();
                Assert.False(
                    typeof(IImmutable).IsAssignableFrom(t)
                    , t.FullName
                    );
            }
        }

        /***************************************************
         *** 
         *** 
         *** MASTER TEST METHOD
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Applies all contract validation to the object passed in as the testableObject
        /// argument.
        /// </summary>
        /// <remarks>
        /// Used when IMutable is one of several interfaces
        /// implemented and the implementing class's Tester
        /// class includes BaseIMutableTester by composition
        /// rather than inheritance.
        /// </remarks>
        /// <param name="testableObject">The object to be tested</param>
        public virtual void Validate(IMutable i)
        {
            NotAnIImmutable(i);
        }

    }

    /// <summary>
    /// Base class tester for anything implementing the 
    /// Com.VeritasTS.TextAndErrorUtils.IMutable interface.
    /// </summary>
    /// <remarks>Anyone implementing the
    /// Com.VeritasTS.TextAndErrorUtils.IMutable must
    /// ensure that their implementation both fully honors
    /// the interface contract, and is proven to do so by
    /// thorough unit testing. 
    /// <para>Only the normal virtual TestableObject()
    /// method is required; there are no special cases to be tested
    /// at this level.</para>
    /// </remarks>
    [TestFixture]
    public class BaseIMutableTester
    {

        /***************************************************
         *** 
         *** 
         *** TEST CASES
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Ensures that TestableObject does not
        /// implement IImmutable.
        /// </summary>
        [TestCase]
        public void NotAnIImmutable()
        {
            BaseIMutableTesterCore core = new BaseIMutableTesterCore();
            core.NotAnIImmutable(core.TestableObject());
        }

        /***************************************************
         *** 
         *** 
         *** MASTER TEST METHOD
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Applies all contract validation to the object passed in as the testableObject
        /// argument.
        /// </summary>
        /// <remarks>
        /// Used when IMutable is one of several interfaces
        /// implemented and the implementing class's Tester
        /// class includes BaseIMutableTester by composition
        /// rather than inheritance.
        /// </remarks>
        /// <param name="testableObject">The object to be tested</param>
        public virtual void Validate()
        {
            NotAnIImmutable();
        }

    }

}
