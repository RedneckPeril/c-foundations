﻿using System;
using System.Diagnostics;
using NUnit.Framework;
using Com.VeritasTS.CoreUtils;

// This tester is ready for use.

namespace Com.VeritasTS.CoreUtils.Test
{

    /// <summary>
    /// Base class tester for anything implementing the 
    /// Com.VeritasTS.TextAndErrorUtils.ISelfAsserter interface.
    /// </summary>
    /// <remarks>Anyone implementing the
    /// Com.VeritasTS.TextAndErrorUtils.ISelfAsserter must
    /// ensure that their implementation both fully honors
    /// the interface contract, and is proven to do so by
    /// thorough unit testing. 
    /// <para>
    /// Descendent classes must override:
    /// <list type="bullet">
    /// <item><see cref="TypeUnderTesting"/> </item>
    /// <item><see cref="IncompletelyConstructedBadInstance"/> </item>
    /// <item><see cref="IncompletelyConstructedGoodInstance"/> </item>
    /// <item><see cref="CompletelyConstructedBadInstance"/> </item>
    /// <item><see cref="CompletelyConstructedGoodInstance"/> </item>
    /// </list>
    /// </para>
    /// </remarks>
    [TestFixture]
    public class BaseISelfAsserterTester
    {

        protected class TestSelfAsserter : SelfAsserterMutable
        {
            public bool IsBad { get; }
            public TestSelfAsserter(bool isBad, bool constructFully, bool assert = true)
            {
                IsBad = isBad;
                if (constructFully && assert)
                    Debug.Assert(MarkAsFullyConstructed(typeof(TestSelfAsserter)));
            }
            public override bool PassesSelfAssertion(Type testableType = null)
            {
                if (!SkipSelfAssertion(testableType))
                {
                    Debug.Assert(base.PassesSelfAssertion());
                    Debug.Assert(!IsBad);
                }
                return true;
            }
        }

        /// <summary>
        /// Returns Type object for class under testing.
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an object of the class under testing.</remarks>
        /// <returns>Testable type that implements ISelfAsserter.</returns>
        public virtual Type TypeUnderTesting()
        {
            return typeof(SelfAsserterMutable);
        }

        /// <summary>
        /// Returns ISelfAsserter that has no lurking assertion failures
        /// but has not been marked as fully constructed.
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an object of the class under testing.</remarks>
        /// <returns>Testable ISelfAsserter suitable for general testing.</returns>
        public virtual ISelfAsserter IncompletelyConstructedGoodInstance()
        {
            return new TestSelfAsserter(false, false);
        }

        /// <summary>
        /// Returns ISelfAsserter that has no lurking assertion failures
        /// and has been marked as fully constructed.
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an object of the class under testing.</remarks>
        /// <returns>Testable ISelfAsserter suitable for general testing.</returns>
        public virtual ISelfAsserter CompletelyConstructedGoodInstance()
        {
            return new TestSelfAsserter(false, true);
        }

        /// <summary>
        /// Returns ISelfAsserter that has lurking assertion failures
        /// but has not been marked as fully constructed.
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an object of the class under testing.</remarks>
        /// <returns>Testable ISelfAsserter suitable for general testing.</returns>
        public virtual ISelfAsserter IncompletelyConstructedBadInstance()
        {
            return new TestSelfAsserter(true, false);
        }

        /// <summary>
        /// Returns ISelfAsserter that has lurking assertion failures
        /// and has been marked as fully constructed.
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an object of the class under testing.</remarks>
        /// <returns>Testable ISelfAsserter suitable for general testing.</returns>
        public virtual ISelfAsserter CompletelyConstructedBadInstance()
        {
            return new TestSelfAsserter(true, true, false);
        }

        /***************************************************
         *** 
         *** 
         *** VALIDATION METHODS
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Ensures that if IsFullyConstructed is false, any variation on
        /// PassesSelfAssertion(...) returns true
        /// whether or not there are lurking assertion failures.
        /// </summary>
        /// <param name="preferenceSet">The object to be tested</param>
        public void SelfAssertionPassesWhenNotFullyConstructed(
            ISelfAsserter incomplete
            , Type testableType
            , bool parameterless
            )
        {
            if (incomplete != null)
                Assert.True(
                    !incomplete.IsFullyConstructed
                    &&
                    (
                    parameterless ? incomplete.PassesSelfAssertion()
                    : incomplete.PassesSelfAssertion(testableType)
                    )
                    );
        }

        /// <summary>
        /// Ensures that if there are no lurking assertion failures, 
        /// any variations on PassesSelfAssertion(...) return true.
        /// </summary>
        /// <param name="preferenceSet">The object to be tested</param>
        public void SelfAssertionPassesWhenGood(
            ISelfAsserter good
            , Type testableType
            , bool parameterless
            )
        {
            if (good != null)
                Assert.True(
                    parameterless ? good.PassesSelfAssertion()
                    : good.PassesSelfAssertion(testableType)
                    );
        }

        /// <summary>
        /// Ensures that if there are lurking assertion failures, 
        /// any variation on PassesSelfAssertion(...) returns false
        /// except when !testableType.Equals(x.GetType()), which
        /// returns true.
        /// </summary>
        /// <param name="preferenceSet">The object to be tested</param>
        public void SelfAssertionBehavesCorrectlyWhenBad(
            ISelfAsserter bad
            , Type testableType
            , bool parameterless
            )
        {
            if (bad != null)
            {
                if (testableType != null && !testableType.Equals(bad.GetType()))
                    Assert.True(
                        parameterless ? bad.PassesSelfAssertion()
                        : bad.PassesSelfAssertion(testableType)
                        );
                else
                {
                    bool assertionFailed = false;
                    try
                    {
                        if (parameterless)
                            bad.PassesSelfAssertion();
                        else
                            bad.PassesSelfAssertion(testableType);
                    }
                    catch (Exception)
                    {
                        assertionFailed = true;
                    }
                    Debug.Assert(assertionFailed);
                }
            }
        }

        /// <summary>
        /// Ensures correct behavior of all variations of MarkAsFullyConstructed(null)
        /// when no assertion failures are lurking.
        /// </summary>
        /// <param name="preferenceSet">The object to be tested</param>
        protected bool MarkAsFullyConstructedAppropriatelyCalled(
            ISelfAsserter selfAsserter
            , Type testableType
            , bool? assert
            , bool isTypeless
            )
        {
            bool assertIsDefaulted = assert == null || !assert.HasValue;
            if (isTypeless && assertIsDefaulted)
                return selfAsserter.MarkAsFullyConstructed();
            if (isTypeless)
                return selfAsserter.MarkAsFullyConstructed(assert: assert.Value);
            if (assertIsDefaulted)
                return selfAsserter.MarkAsFullyConstructed(testableType);
            return selfAsserter.MarkAsFullyConstructed(testableType, assert.Value);
        }

        /// <summary>
        /// Ensures correct behavior of all variations of MarkAsFullyConstructed(null)
        /// when no assertion failures are lurking.
        /// </summary>
        /// <param name="preferenceSet">The object to be tested</param>
        public void MarkAsFullyConstructedBehavesCorrectlyWhenGood(
            ISelfAsserter good
            , Type testableType
            , bool? assert
            , bool isTypeless
            )
        {
            if (good != null)
            {
                bool originalIFCValue = good.IsFullyConstructed;
                if (testableType != null && !testableType.Equals(good.GetType()))
                    Assert.True(
                        MarkAsFullyConstructedAppropriatelyCalled(
                            good
                            , testableType
                            , assert
                            , isTypeless
                            )
                        && (good.IsFullyConstructed == originalIFCValue)
                        , good.GetType().FullName
                        );
                else
                    Assert.True(
                        MarkAsFullyConstructedAppropriatelyCalled(
                            good
                            , testableType
                            , assert
                            , isTypeless
                            )
                        && good.IsFullyConstructed
                        );
            }
        }

        /// <summary>
        /// Ensures correct behavior of all variations of MarkAsFullyConstructed(null)
        /// when assertion failures are lurking.
        /// </summary>
        /// <param name="preferenceSet">The object to be tested</param>
        public void MarkAsFullyConstructedBehavesCorrectlyWhenBad(
            ISelfAsserter bad
            , Type testableType
            , bool? assert
            , bool isTypeless
            )
        {
            if (bad != null)
            {
                bool originalIFCValue = bad.IsFullyConstructed;
                if (testableType != null && !testableType.Equals(bad.GetType()))
                {
                    if (!bad.IsFullyConstructed)
                        Assert.True(
                            MarkAsFullyConstructedAppropriatelyCalled(
                                bad
                                , testableType
                                , assert
                                , isTypeless
                                )
                            && !bad.IsFullyConstructed
                            );
                    else if (
                        assert != null && assert.HasValue && !assert.Value
                        )
                        Assert.True(
                            MarkAsFullyConstructedAppropriatelyCalled(
                                bad
                                , testableType
                                , assert
                                , isTypeless
                                )
                            && bad.IsFullyConstructed
                            );
                }
                else
                {
                    bool assertionFailed = false;
                    try
                    {
                        Assert.True(
                            MarkAsFullyConstructedAppropriatelyCalled(
                                bad
                                , testableType
                                , assert
                                , isTypeless
                                )
                            && bad.IsFullyConstructed
                            );
                    }
                    catch (Exception)
                    {
                        assertionFailed = true;
                    }
                    Debug.Assert(assertionFailed);
                }
            }
        }

        /***************************************************
         *** 
         *** 
         *** TEST CASES
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Ensures that where IsFullyConstructed is false, PassesSelfAssertion() returns true
        /// if there are no lurking assertion failures.
        /// </summary>
        [TestCase]
        public void ParameterlessSelfAssertionPassesWhenGoodAndNotFullyConstructed()
        {
            SelfAssertionPassesWhenNotFullyConstructed(
                IncompletelyConstructedGoodInstance()
                , null
                , true
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is false, PassesSelfAssertion() returns true
        /// even if there are lurking assertion failures.
        /// </summary>
        [TestCase]
        public void ParameterlessSelfAssertionPassesWhenBadAndNotFullyConstructed()
        {
            SelfAssertionPassesWhenNotFullyConstructed(
                IncompletelyConstructedBadInstance()
                , null
                , true
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true, PassesSelfAssertion() returns true
        /// if there are no lurking assertion failures.
        /// </summary>
        [TestCase]
        public void ParameterlessSelfAssertionPassesWhenGoodAndFullyConstructed()
        {
            SelfAssertionPassesWhenGood(
                CompletelyConstructedGoodInstance()
                , null
                , true
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true, PassesSelfAssertion()
        /// throws exception if there are lurking assertion failures.
        /// </summary>
        //[TestCase]
        //HERE HERE HERE
        //public void ParameterlessSelfAssertionFailsWhenBadAndFullyConstructed()
        //{
        //    SelfAssertionBehavesCorrectlyWhenBad(
        //        CompletelyConstructedBadInstance()
        //        , null
        //        , true
        //        );
        //}

        /// <summary>
        /// Ensures that where IsFullyConstructed is false, PassesSelfAssertion(null) returns true
        /// if there are no lurking assertion failures.
        /// </summary>
        [TestCase]
        public void NullParameterSelfAssertionPassesWhenGoodAndNotFullyConstructed()
        {
            SelfAssertionPassesWhenNotFullyConstructed(
                IncompletelyConstructedGoodInstance()
                , null
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is false, PassesSelfAssertion(null) returns true
        /// even if there are lurking assertion failures.
        /// </summary>
        [TestCase]
        public void NullParameterSelfAssertionPassesWhenBadAndNotFullyConstructed()
        {
            SelfAssertionPassesWhenNotFullyConstructed(
                IncompletelyConstructedBadInstance()
                , null
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true, PassesSelfAssertion(null) returns true
        /// if there are no lurking assertion failures.
        /// </summary>
        [TestCase]
        public void NullParameterSelfAssertionPassesWhenGoodAndFullyConstructed()
        {
            SelfAssertionPassesWhenGood(
                CompletelyConstructedGoodInstance()
                , null
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true, PassesSelfAssertion(null)
        /// throws exception if there are lurking assertion failures.
        /// </summary>
        //[TestCase]
        //HERE HERE HERE
        //public void NullParameterSelfAssertionFailsWhenBadAndFullyConstructed()
        //{
        //    SelfAssertionBehavesCorrectlyWhenBad(
        //        CompletelyConstructedBadInstance()
        //        , null
        //        , false
        //        );
        //}

        /// <summary>
        /// Ensures that where IsFullyConstructed is false, x.PassesSelfAssertion(x.GetType()) returns true
        /// if there are no lurking assertion failures.
        /// </summary>
        [TestCase]
        public void NSelfAssertionForExactTypePassesWhenGoodAndNotFullyConstructed()
        {
            ISelfAsserter selfAsserter = IncompletelyConstructedGoodInstance();
            SelfAssertionPassesWhenNotFullyConstructed(
                selfAsserter
                , selfAsserter.GetType()
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is false, x.PassesSelfAssertion(x.GetType()) returns true
        /// even if there are lurking assertion failures.
        /// </summary>
        [TestCase]
        public void SelfAssertionForExactTypePassesWhenBadAndNotFullyConstructed()
        {
            ISelfAsserter selfAsserter = IncompletelyConstructedBadInstance();
            SelfAssertionPassesWhenNotFullyConstructed(
                selfAsserter
                , selfAsserter.GetType()
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true, x.PassesSelfAssertion(x.GetType()) returns true
        /// if there are no lurking assertion failures.
        /// </summary>
        [TestCase]
        public void SelfAssertionForExactTypePassesWhenGoodAndFullyConstructed()
        {
            ISelfAsserter selfAsserter = CompletelyConstructedGoodInstance();
            SelfAssertionPassesWhenGood(
                selfAsserter
                , selfAsserter.GetType()
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true, x.PassesSelfAssertion(x.GetType())
        /// throws exception if there are lurking assertion failures.
        /// </summary>
        //[TestCase]
        //HERE HERE HERE
        //public void SelfAssertionForExactTypeFailsWhenBadAndFullyConstructed()
        //{
        //    ISelfAsserter selfAsserter = CompletelyConstructedBadInstance();
        //    SelfAssertionBehavesCorrectlyWhenBad(
        //        selfAsserter
        //        , selfAsserter.GetType()
        //        , false
        //        );
        //}

        /// <summary>
        /// Ensures that where IsFullyConstructed is false, x.PassesSelfAssertion(some type other than x.GetType())
        /// returns true if there are no lurking assertion failures.
        /// </summary>
        [TestCase]
        public void SelfAssertionForDifferentTypePassesWhenGoodAndNotFullyConstructed()
        {
            ISelfAsserter selfAsserter = IncompletelyConstructedGoodInstance();
            SelfAssertionPassesWhenNotFullyConstructed(
                selfAsserter
                , typeof(ISelfAsserter)
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is false, x.PassesSelfAssertion(some type other than x.GetType())
        /// returns true even if there are lurking assertion failures.
        /// </summary>
        [TestCase]
        public void SelfAssertionForDifferentTypePassesWhenBadAndNotFullyConstructed()
        {
            ISelfAsserter selfAsserter = IncompletelyConstructedBadInstance();
            SelfAssertionPassesWhenNotFullyConstructed(
                selfAsserter
                , typeof(ISelfAsserter)
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true, x.PassesSelfAssertion(some type other than x.GetType())
        /// returns true if there are no lurking assertion failures.
        /// </summary>
        [TestCase]
        public void SelfAssertionForDifferentTypePassesWhenGoodAndFullyConstructed()
        {
            ISelfAsserter selfAsserter = CompletelyConstructedGoodInstance();
            SelfAssertionPassesWhenGood(
                selfAsserter
                , typeof(ISelfAsserter)
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true, x.PassesSelfAssertion(some type other than x.GetType())
        /// returns true even if there are lurking assertion failures.
        /// </summary>
        [TestCase]
        public void SelfAssertionForDifferentTypePassesWhenBadAndFullyConstructed()
        {
            ISelfAsserter selfAsserter = CompletelyConstructedBadInstance();
            SelfAssertionBehavesCorrectlyWhenBad(
                selfAsserter
                , typeof(ISelfAsserter)
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is false and no assertion failures
        /// are lurking, MarkAsFullyConstructed() 
        /// returns true and causes IsFullyConstructed to become true.
        /// </summary>
        [TestCase]
        public void ParameterlessMarkAsFullyConstructedCompletesConstructionWhenGood()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                IncompletelyConstructedGoodInstance()
                , null
                , null
                , true
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is false and assertion failures
        /// are lurking, MarkAsFullyConstructed() 
        /// causes IsFullyConstructed to become true and either (1) returns
        /// true in Production compilation or (2) throws exception in
        /// Test compilation.
        /// </summary>
        //[TestCase]
        //HERE HERE HERE
        //public void ParameterlessMarkAsFullyConstructedCompletesConstructionAndRespondsToCompilationModeWhenBad()
        //{
        //    SelfAssertionBehavesCorrectlyWhenBad(
        //        IncompletelyConstructedBadInstance()
        //        , null
        //        , true
        //        );
        //}

        /// <summary>
        /// Ensures that where IsFullyConstructed is true and no assertion failures
        /// are lurking, MarkAsFullyConstructed() 
        /// returns true and leaves IsFullyConstructed set to true.
        /// </summary>
        [TestCase]
        public void ParameterlessMarkAsFullyConstructedLeavesConstructionCompleteWhenGood()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                CompletelyConstructedGoodInstance()
                , null
                , null
                , true
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true and assertion failures
        /// are lurking, MarkAsFullyConstructed() 
        /// leaves IsFullyConstructed set to true and either (1) returns
        /// true in Production compilation or (2) throws exception in
        /// Test compilation.
        /// </summary>
        //[TestCase]
        //HERE HERE HERE
        //public void ParameterlessMarkAsFullyConstructedLeavesConstructionCompleteAndRespondsToCompilationModeWhenBad()
        //{
        //    MarkAsFullyConstructedBehavesCorrectlyWhenBad(
        //        CompletelyConstructedBadInstance()
        //        , null
        //        , null
        //        , true
        //        );
        //}

        /// <summary>
        /// Ensures that where IsFullyConstructed is false and no assertion failures
        /// are lurking, MarkAsFullyConstructed(null) 
        /// returns true and causes IsFullyConstructed to become true.
        /// </summary>
        [TestCase]
        public void NullTypeDefaultAssertMarkAsFullyConstructedCompletesConstructionWhenGood()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                IncompletelyConstructedGoodInstance()
                , null
                , null
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is false and assertion failures
        /// are lurking, MarkAsFullyConstructed(null) 
        /// causes IsFullyConstructed to become true and either (1) returns
        /// true in Production compilation or (2) throws exception in
        /// Test compilation.
        /// </summary>
        //[TestCase]
        //HERE HERE HERE
        //public void NullTypeDefaultAssertMarkAsFullyConstructedCompletesConstructionAndRespondsToCompilationModeWhenBad()
        //{
        //    MarkAsFullyConstructedBehavesCorrectlyWhenGood(
        //        IncompletelyConstructedBadInstance()
        //        , null
        //        , null
        //        , false
        //        );
        //}

        /// <summary>
        /// Ensures that where IsFullyConstructed is true and no assertion failures
        /// are lurking, MarkAsFullyConstructed(null) 
        /// returns true and leaves IsFullyConstructed set to true.
        /// </summary>
        [TestCase]
        public void NullTypeDefaultAssertMarkAsFullyConstructedLeavesConstructionCompleteWhenGood()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                CompletelyConstructedGoodInstance()
                , null
                , null
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true and assertion failures
        /// are lurking, MarkAsFullyConstructed(null) 
        /// leaves IsFullyConstructed set to true and either (1) returns
        /// true in Production compilation or (2) throws exception in
        /// Test compilation.
        /// </summary>
        //[TestCase]
        //HERE HERE HERE
        //public void NullTypeDefaultAssertMarkAsFullyConstructedLeavesConstructionCompleteAndRespondsToCompilationModeWhenBad()
        //{
        //    MarkAsFullyConstructedBehavesCorrectlyWhenGood(
        //        CompletelyConstructedBadInstance()
        //        , null
        //        , null
        //        , false
        //        );
        //}

        /// <summary>
        /// Ensures that where IsFullyConstructed is false and no assertion failures
        /// are lurking, MarkAsFullyConstructed(null, true) 
        /// returns true and causes IsFullyConstructed to become true.
        /// </summary>
        [TestCase]
        public void NullTypeAssertingMarkAsFullyConstructedCompletesConstructionWhenGood()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                IncompletelyConstructedGoodInstance()
                , null
                , true
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is false and assertion failures
        /// are lurking, MarkAsFullyConstructed(null, true) 
        /// causes IsFullyConstructed to become true and either (1) returns
        /// true in Production compilation or (2) throws exception in
        /// Test compilation.
        /// </summary>
        //[TestCase]
        //HERE HERE HERE
        //public void NullTypeAssertingMarkAsFullyConstructedCompletesConstructionAndRespondsToCompilationModeWhenBad()
        //{
        //    MarkAsFullyConstructedBehavesCorrectlyWhenGood(
        //        IncompletelyConstructedBadInstance()
        //        , null
        //        , true
        //        , false
        //        );
        //}

        /// <summary>
        /// Ensures that where IsFullyConstructed is true and no assertion failures
        /// are lurking, MarkAsFullyConstructed(null, true) 
        /// returns true and leaves IsFullyConstructed set to true.
        /// </summary>
        [TestCase]
        public void NullTypeAssertingMarkAsFullyConstructedLeavesConstructionCompleteWhenGood()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                CompletelyConstructedGoodInstance()
                , null
                , true
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true and assertion failures
        /// are lurking, MarkAsFullyConstructed(null, true) 
        /// leaves IsFullyConstructed set to true and either (1) returns
        /// true in Production compilation or (2) throws exception in
        /// Test compilation.
        /// </summary>
        //[TestCase]
        //HERE HERE HERE
        //public void NullTypeAssertingMarkAsFullyConstructedLeavesConstructionCompleteAndRespondsToCompilationModeWhenBad()
        //{
        //    MarkAsFullyConstructedBehavesCorrectlyWhenGood(
        //        CompletelyConstructedBadInstance()
        //        , null
        //        , true
        //        , false
        //        );
        //}

        /// <summary>
        /// Ensures that where IsFullyConstructed is false and no assertion failures
        /// are lurking, MarkAsFullyConstructed(null, false) 
        /// returns true and causes IsFullyConstructed to become true.
        /// </summary>
        [TestCase]
        public void NullTypeNonassertingMarkAsFullyConstructedCompletesConstructionWhenGood()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                IncompletelyConstructedGoodInstance()
                , null
                , false
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is false and assertion failures
        /// are lurking, MarkAsFullyConstructed(null, false) 
        /// causes IsFullyConstructed to become true and returns true.
        /// </summary>
        [TestCase]
        public void NullTypeNonassertingMarkAsFullyConstructedCompletesConstructionWhenBad()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                IncompletelyConstructedBadInstance()
                , null
                , false
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true and no assertion failures
        /// are lurking, MarkAsFullyConstructed(null, false) 
        /// returns true and leaves IsFullyConstructed set to true.
        /// </summary>
        [TestCase]
        public void NullTypeNonassertingMarkAsFullyConstructedLeavesConstructionCompleteWhenGood()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                CompletelyConstructedGoodInstance()
                , null
                , false
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true and assertion failures
        /// are lurking, MarkAsFullyConstructed(null, false) 
        /// leaves IsFullyConstructed set to true and returns true.
        /// </summary>
        [TestCase]
        public void NullTypeNonassertingMarkAsFullyConstructedLeavesConstructionCompleteWhenBad()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                CompletelyConstructedBadInstance()
                , null
                , false
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is false and no assertion failures
        /// are lurking, x.MarkAsFullyConstructed(x.GetType()) 
        /// returns true and causes IsFullyConstructed to become true.
        /// </summary>
        [TestCase]
        public void ExactTypeDefaultAssertMarkAsFullyConstructedCompletesConstructionWhenGood()
        {
            ISelfAsserter selfAsserter = IncompletelyConstructedGoodInstance();
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                selfAsserter
                , selfAsserter.GetType()
                , null
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is false and assertion failures
        /// are lurking, x.MarkAsFullyConstructed(x.GetType()) 
        /// causes IsFullyConstructed to become true and either (1) returns
        /// true in Production compilation or (2) throws exception in
        /// Test compilation.
        /// </summary>
        //[TestCase]
        //HERE HERE HERE
        //public void ExactTypeDefaultAssertMarkAsFullyConstructedCompletesConstructionAndRespondsToCompilationModeWhenBad()
        //{
        //    ISelfAsserter selfAsserter = IncompletelyConstructedBadInstance();
        //    MarkAsFullyConstructedBehavesCorrectlyWhenGood(
        //        selfAsserter
        //        , selfAsserter.GetType()
        //        , null
        //        , false
        //        );
        //}

        /// <summary>
        /// Ensures that where IsFullyConstructed is true and no assertion failures
        /// are lurking, x.MarkAsFullyConstructed(x.GetType()) 
        /// returns true and leaves IsFullyConstructed set to true.
        /// </summary>
        [TestCase]
        public void ExactTypeDefaultAssertMarkAsFullyConstructedLeavesConstructionCompleteWhenGood()
        {
            ISelfAsserter selfAsserter = CompletelyConstructedGoodInstance();
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                selfAsserter
                , selfAsserter.GetType()
                , null
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true and assertion failures
        /// are lurking, x.MarkAsFullyConstructed(x.GetType()) 
        /// leaves IsFullyConstructed set to true and either (1) returns
        /// true in Production compilation or (2) throws exception in
        /// Test compilation.
        /// </summary>
        //[TestCase]
        //HERE HERE HERE
        //public void ExactTypeDefaultAssertMarkAsFullyConstructedLeavesConstructionCompleteAndRespondsToCompilationModeWhenBad()
        //{
        //    ISelfAsserter selfAsserter = CompletelyConstructedBadInstance();
        //    MarkAsFullyConstructedBehavesCorrectlyWhenGood(
        //        selfAsserter
        //        , selfAsserter.GetType()
        //        , null
        //        , false
        //        );
        //}

        /// <summary>
        /// Ensures that where IsFullyConstructed is false and no assertion failures
        /// are lurking, x.MarkAsFullyConstructed(x.GetType(), true) 
        /// returns true and causes IsFullyConstructed to become true.
        /// </summary>
        [TestCase]
        public void ExactTypeAssertingMarkAsFullyConstructedCompletesConstructionWhenGood()
        {
            ISelfAsserter selfAsserter = IncompletelyConstructedGoodInstance();
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                selfAsserter
                , selfAsserter.GetType()
                , true
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is false and assertion failures
        /// are lurking, x.MarkAsFullyConstructed(x.GetType(), true) 
        /// causes IsFullyConstructed to become true and either (1) returns
        /// true in Production compilation or (2) throws exception in
        /// Test compilation.
        /// </summary>
        //[TestCase]
        //HERE HERE HERE
        //public void ExactTypeAssertingMarkAsFullyConstructedCompletesConstructionAndRespondsToCompilationModeWhenBad()
        //{
        //    ISelfAsserter selfAsserter = IncompletelyConstructedBadInstance();
        //    MarkAsFullyConstructedBehavesCorrectlyWhenBad(
        //        selfAsserter
        //        , selfAsserter.GetType()
        //        , true
        //        , false
        //        );
        //}

        /// <summary>
        /// Ensures that where IsFullyConstructed is true and no assertion failures
        /// are lurking, x.MarkAsFullyConstructed(x.GetType(), true) 
        /// returns true and leaves IsFullyConstructed set to true.
        /// </summary>
        [TestCase]
        public void ExactTypeAssertingMarkAsFullyConstructedLeavesConstructionCompleteWhenGood()
        {
            ISelfAsserter selfAsserter = CompletelyConstructedGoodInstance();
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                selfAsserter
                , selfAsserter.GetType()
                , true
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true and assertion failures
        /// are lurking, x.MarkAsFullyConstructed(x.GetType(), true) 
        /// leaves IsFullyConstructed set to true and either (1) returns
        /// true in Production compilation or (2) throws exception in
        /// Test compilation.
        /// </summary>
        //[TestCase]
        //HERE HERE HERE
        //public void ExactTypeAssertingMarkAsFullyConstructedLeavesConstructionCompleteAndRespondsToCompilationModeWhenBad()
        //{
        //    ISelfAsserter selfAsserter = CompletelyConstructedBadInstance();
        //    MarkAsFullyConstructedBehavesCorrectlyWhenGood(
        //        selfAsserter
        //        , selfAsserter.GetType()
        //        , true
        //        , false
        //        );
        //}

        /// <summary>
        /// Ensures that where IsFullyConstructed is false and no assertion failures
        /// are lurking, x.MarkAsFullyConstructed(x.GetType(), false) 
        /// returns true and causes IsFullyConstructed to become true.
        /// </summary>
        [TestCase]
        public void ExactTypeNonassertingMarkAsFullyConstructedCompletesConstructionWhenGood()
        {
            ISelfAsserter selfAsserter = IncompletelyConstructedGoodInstance();
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                selfAsserter
                , selfAsserter.GetType()
                , false
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is false and assertion failures
        /// are lurking, x.MarkAsFullyConstructed(x.GetType(), false) 
        /// causes IsFullyConstructed to become true and returns true.
        /// </summary>
        [TestCase]
        public void ExactTypeNonassertingMarkAsFullyConstructedCompletesConstructionWhenBad()
        {
            ISelfAsserter selfAsserter = IncompletelyConstructedBadInstance();
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                selfAsserter
                , selfAsserter.GetType()
                , false
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true and no assertion failures
        /// are lurking, x.MarkAsFullyConstructed(x.GetType(), false) 
        /// returns true and leaves IsFullyConstructed set to true.
        /// </summary>
        [TestCase]
        public void ExactTypeNonassertingMarkAsFullyConstructedLeavesConstructionCompleteWhenGood()
        {
            ISelfAsserter selfAsserter = CompletelyConstructedGoodInstance();
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                selfAsserter
                , selfAsserter.GetType()
                , false
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true and assertion failures
        /// are lurking, x.MarkAsFullyConstructed(x.GetType(), false) 
        /// leaves IsFullyConstructed set to true and returns true.
        /// </summary>
        [TestCase]
        public void ExactTypeNonassertingMarkAsFullyConstructedLeavesConstructionCompleteWhenBad()
        {
            ISelfAsserter selfAsserter = CompletelyConstructedBadInstance();
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                selfAsserter
                , selfAsserter.GetType()
                , false
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is false and no assertion failures
        /// are lurking, x.MarkAsFullyConstructed(type other than x.GetType()) 
        /// returns true and leaves IsFullyConstructed set to false.
        /// </summary>
        [TestCase]
        public void DifferentTypeDefaultAssertMarkAsFullyConstructedLeavesConstructionIncompleteWhenGood()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                IncompletelyConstructedGoodInstance()
                , typeof(ISelfAsserter)
                , null
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is false and assertion failures
        /// are lurking, x.MarkAsFullyConstructed(type other than x.GetType()) 
        /// leaves IsFullyConstructed set to false and returns true.
        /// </summary>
        [TestCase]
        public void DifferentTypeDefaultAssertMarkAsFullyConstructedLeavesConstructionIncompleteWhenBad()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                IncompletelyConstructedBadInstance()
                , typeof(ISelfAsserter)
                , null
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true and no assertion failures
        /// are lurking, x.MarkAsFullyConstructed(type other than x.GetType()) 
        /// returns true and leaves IsFullyConstructed set to true.
        /// </summary>
        [TestCase]
        public void DifferentTypeDefaultAssertMarkAsFullyConstructedLeavesConstructionCompleteWhenGood()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                CompletelyConstructedGoodInstance()
                , typeof(ISelfAsserter)
                , null
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true and assertion failures
        /// are lurking, x.MarkAsFullyConstructed(type other than x.GetType()) 
        /// leaves IsFullyConstructed set to true and returns true.
        /// </summary>
        [TestCase]
        public void DifferentTypeDefaultAssertMarkAsFullyConstructedLeavesConstructionCompleteWhenBad()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                CompletelyConstructedBadInstance()
                , typeof(ISelfAsserter)
                , null
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is false and no assertion failures
        /// are lurking, x.MarkAsFullyConstructed(type other than x.GetType(), true) 
        /// returns true and leaves IsFullyConstructed set to false.
        /// </summary>
        [TestCase]
        public void DifferentTypeAssertingMarkAsFullyConstructedLeavesConstructionIncompleteWhenGood()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                IncompletelyConstructedGoodInstance()
                , typeof(ISelfAsserter)
                , true
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is false and assertion failures
        /// are lurking, x.MarkAsFullyConstructed(type other than x.GetType(), true) 
        /// leaves IsFullyConstructed set to false and returns true.
        /// </summary>
        [TestCase]
        public void DifferentTypeAssertingMarkAsFullyConstructedLeavesConstructionIncompleteWhenBad()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                IncompletelyConstructedBadInstance()
                , typeof(ISelfAsserter)
                , true
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true and no assertion failures
        /// are lurking, x.MarkAsFullyConstructed(type other than x.GetType(), true) 
        /// returns true and leaves IsFullyConstructed set to true.
        /// </summary>
        [TestCase]
        public void DifferentTypeAssertingMarkAsFullyConstructedLeavesConstructionCompleteWhenGood()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                CompletelyConstructedGoodInstance()
                , typeof(ISelfAsserter)
                , true
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true and assertion failures
        /// are lurking, x.MarkAsFullyConstructed(type other than x.GetType(), true) 
        /// leaves IsFullyConstructed set to true and returns true.
        /// </summary>
        [TestCase]
        public void DifferentTypeAssertingMarkAsFullyConstructedLeavesConstructionCompleteWhenBad()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                CompletelyConstructedBadInstance()
                , typeof(ISelfAsserter)
                , true
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is false and no assertion failures
        /// are lurking, x.MarkAsFullyConstructed(type other than x.GetType(), false) 
        /// returns true and leaves IsFullyConstructed set to false.
        /// </summary>
        [TestCase]
        public void DifferentTypeNonassertingMarkAsFullyConstructedLeavesConstructionIncompleteWhenGood()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                IncompletelyConstructedGoodInstance()
                , typeof(ISelfAsserter)
                , false
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is false and assertion failures
        /// are lurking, x.MarkAsFullyConstructed(type other than x.GetType(), false) 
        /// leaves IsFullyConstructed set to false and returns true.
        /// </summary>
        [TestCase]
        public void DifferentTypeNonassertingMarkAsFullyConstructedLeavesConstructionIncompleteWhenBad()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                IncompletelyConstructedBadInstance()
                , typeof(ISelfAsserter)
                , false
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true and no assertion failures
        /// are lurking, x.MarkAsFullyConstructed(type other than x.GetType(), false) 
        /// returns true and leaves IsFullyConstructed set to true.
        /// </summary>
        [TestCase]
        public void DifferentTypeNonassertingMarkAsFullyConstructedLeavesConstructionCompleteWhenGood()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                CompletelyConstructedGoodInstance()
                , typeof(ISelfAsserter)
                , false
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true and assertion failures
        /// are lurking, x.MarkAsFullyConstructed(type other than x.GetType(), false) 
        /// leaves IsFullyConstructed set to true and returns true.
        /// </summary>
        [TestCase]
        public void DifferentTypeNonassertingMarkAsFullyConstructedLeavesConstructionCompleteWhenBad()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                CompletelyConstructedBadInstance()
                , typeof(ISelfAsserter)
                , false
                , false
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is false and no assertion failures
        /// are lurking, MarkAsFullyConstructed(assert: true) 
        /// returns true and causes IsFullyConstructed to become true.
        /// </summary>
        [TestCase]
        public void NoTypeAssertingMarkAsFullyConstructedCompletesConstructionWhenGood()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                IncompletelyConstructedGoodInstance()
                , null
                , true
                , true
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is false and assertion failures
        /// are lurking, MarkAsFullyConstructed(assert: true) 
        /// causes IsFullyConstructed to become true and either (1) returns
        /// true in Production compilation or (2) throws exception in
        /// Test compilation.
        /// </summary>
        //[TestCase]
        //HERE HERE HERE
        //public void NoTypeAssertingMarkAsFullyConstructedCompletesConstructionAndRespondsToCompilationModeWhenBad()
        //{
        //    MarkAsFullyConstructedBehavesCorrectlyWhenGood(
        //        IncompletelyConstructedBadInstance()
        //        , null
        //        , true
        //        , true
        //        );
        //}

        /// <summary>
        /// Ensures that where IsFullyConstructed is true and no assertion failures
        /// are lurking, MarkAsFullyConstructed(assert: true) 
        /// returns true and leaves IsFullyConstructed set to true.
        /// </summary>
        [TestCase]
        public void NoTypeAssertingMarkAsFullyConstructedLeavesConstructionCompleteWhenGood()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                CompletelyConstructedGoodInstance()
                , null
                , true
                , true
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true and assertion failures
        /// are lurking, MarkAsFullyConstructed(assert: true) 
        /// leaves IsFullyConstructed set to true and either (1) returns
        /// true in Production compilation or (2) throws exception in
        /// Test compilation.
        /// </summary>
        //[TestCase]
        //HERE HERE HERE
        //public void NoTypeAssertingMarkAsFullyConstructedLeavesConstructionCompleteAndRespondsToCompilationModeWhenBad()
        //{
        //    MarkAsFullyConstructedBehavesCorrectlyWhenGood(
        //        CompletelyConstructedBadInstance()
        //        , null
        //        , true
        //        , true
        //        );
        //}

        /// <summary>
        /// Ensures that where IsFullyConstructed is false and no assertion failures
        /// are lurking, MarkAsFullyConstructed(assert: false) 
        /// returns true and causes IsFullyConstructed to become true.
        /// </summary>
        [TestCase]
        public void NoTypeNonassertingMarkAsFullyConstructedCompletesConstructionWhenGood()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                IncompletelyConstructedGoodInstance()
                , null
                , false
                , true
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is false and assertion failures
        /// are lurking, MarkAsFullyConstructed(assert: false) 
        /// causes IsFullyConstructed to become true and returns true.
        /// </summary>
        [TestCase]
        public void NoTypeNonassertingMarkAsFullyConstructedCompletesConstructionWhenBad()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                IncompletelyConstructedBadInstance()
                , null
                , false
                , true
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true and no assertion failures
        /// are lurking, MarkAsFullyConstructed(assert: false) 
        /// returns true and leaves IsFullyConstructed set to true.
        /// </summary>
        [TestCase]
        public void NoTypeNonassertingMarkAsFullyConstructedLeavesConstructionCompleteWhenGood()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                CompletelyConstructedGoodInstance()
                , null
                , false
                , true
                );
        }

        /// <summary>
        /// Ensures that where IsFullyConstructed is true and assertion failures
        /// are lurking, MarkAsFullyConstructed(assert: false) 
        /// leaves IsFullyConstructed set to true and returns true.
        /// </summary>
        [TestCase]
        public void NoTypeNonassertingMarkAsFullyConstructedLeavesConstructionCompleteWhenBad()
        {
            MarkAsFullyConstructedBehavesCorrectlyWhenGood(
                CompletelyConstructedBadInstance()
                , null
                , false
                , true
                );
        }

    }

    ///// <summary>
    ///// Class tester for SelfAsserterMutable.
    ///// </summary>
    ///// <remarks>
    ///// Descendent classes must override:
    ///// <list type="bullet">
    ///// <item><see cref="TypeUnderTesting"/> </item>
    ///// <item><see cref="IncompletelyConstructedBadInstance"/> </item>
    ///// <item><see cref="IncompletelyConstructedGoodInstance"/> </item>
    ///// <item><see cref="CompletelyConstructedBadInstance"/> </item>
    ///// <item><see cref="CompletelyConstructedGoodInstance"/> </item>
    ///// </list>
    ///// </remarks>
    //[TestFixture]
    //public class SelfAsserterMutableTester : BaseISelfAsserterTester { }


















    ///// <summary>
    ///// Class tester for SelfAsserterImmutable.
    ///// </summary>
    ///// <remarks>
    ///// Descendent classes must override:
    ///// <list type="bullet">
    ///// <item><see cref="TypeUnderTesting"/> </item>
    ///// <item><see cref="DelegeeForTypeUnderTesting"/> </item>
    ///// <item><see cref="DelegeeTypeForTypeUnderTesting"/> </item>
    ///// </list>
    ///// </remarks>
    //[TestFixture]
    //public class SelfAsserterImmutableTester : BaseISelfAsserterTester
    //{

    //    /// <summary>
    //    /// IImmutable tester "inherited" through composition
    //    /// rather than inheritance.
    //    /// </summary>
    //    /// <remarks>
    //    /// Should rarely if ever require overriding.
    //    /// </remarks>
    //    /// <returns>Testable ISelfAsserter suitable for general testing.</returns>
    //    protected class InnerImmutableTester : ImmutableTester
    //    {

    //        private SelfAsserterImmutableTester _owner;
    //        /// <summary>
    //        /// Constructs with link to owning outer-class
    //        /// object.
    //        /// </summary>
    //        /// <remarks>
    //        /// Should never require overriding.</remarks>
    //        public InnerImmutableTester(SelfAsserterImmutableTester owner)
    //        {
    //            _owner = owner;
    //        }

    //        /// <summary>
    //        /// Overrides
    //        /// <see cref="ImmutableTester.TypeUnderTesting"/>.
    //        /// </summary>
    //        /// <remarks>
    //        /// Should never require overriding.</remarks>
    //        /// <returns>Testable type that implements IImmutable.</returns>
    //        public override Type TypeUnderTesting()
    //        {
    //            return _owner.TypeUnderTesting();
    //        }

    //        /// <summary>
    //        /// Overrides
    //        /// <see cref="ImmutableTester.TypeUnderTesting"/>.
    //        /// </summary>
    //        /// <remarks>
    //        /// Should never require overriding.</remarks>
    //        /// <returns>Type that can be used as
    //        /// delegee for type under testing.</returns>
    //        public override object DelegeeForTypeUnderTesting()
    //        {
    //            return _owner.DelegeeForTypeUnderTesting();
    //        }

    //        /// <summary>
    //        /// Returns Type object for class that can serve as
    //        /// delegee for class whose construct-from-delegee
    //        /// constructor is to be tested.
    //        /// </summary>
    //        /// <remarks>
    //        /// Must be overridden to return an object of the class under testing.</remarks>
    //        /// <returns>Object that can be used as
    //        /// delegee for type under testing.</returns>
    //        public override Type DelegeeTypeForTypeUnderTesting()
    //        {
    //            return _owner.DelegeeTypeForTypeUnderTesting();
    //        }
    //    }

    //    protected class TestSelfAsserterImmutable : SelfAsserterImmutable
    //    {
    //        private ISelfAsserter Delegee { get { return (ISelfAsserter)_delegee; } }
    //        protected TestSelfAsserterImmutable() : base(null) { }
    //        public TestSelfAsserterImmutable(TestSelfAsserter delegee) : base(delegee) { }
    //        protected override Type ValidTypeForDelegee { get { return typeof(TestSelfAsserter); } }
    //    }

    //    /// <summary>
    //    /// Returns Type object for class under testing.
    //    /// </summary>
    //    /// <remarks>
    //    /// Must be overridden to return an object of the class under testing.</remarks>
    //    /// <returns>Testable type that implements ISelfAsserter.</returns>
    //    public override Type TypeUnderTesting()
    //    {
    //        return typeof(TestSelfAsserterImmutable);
    //    }

    //    /// <summary>
    //    /// Returns ISelfAsserter that has no lurking assertion failures
    //    /// but has not been marked as fully constructed.
    //    /// </summary>
    //    /// <remarks>
    //    /// It is not possible to return an Immutable object
    //    /// that is not fully constructed.</remarks>
    //    /// <returns>Null.</returns>
    //    public override ISelfAsserter IncompletelyConstructedGoodInstance()
    //    {
    //        return null;
    //    }

    //    /// <summary>
    //    /// Returns ISelfAsserter that has no lurking assertion failures
    //    /// and has been marked as fully constructed.
    //    /// </summary>
    //    /// <remarks>
    //    /// Must be overridden to return an object of the class under testing.</remarks>
    //    /// <returns>Testable ISelfAsserter suitable for general testing.</returns>
    //    public override ISelfAsserter CompletelyConstructedGoodInstance()
    //    {
    //        return
    //            new TestSelfAsserterImmutable(
    //                new TestSelfAsserter(false, true)
    //                );
    //    }

    //    /// <summary>
    //    /// Returns ISelfAsserter that has lurking assertion failures
    //    /// but has not been marked as fully constructed.
    //    /// </summary>
    //    /// <remarks>
    //    /// It is not possible to return an Immutable object
    //    /// that is not fully constructed.</remarks>
    //    /// <returns>Null.</returns>
    //    public override ISelfAsserter IncompletelyConstructedBadInstance()
    //    {
    //        return null;
    //    }

    //    /// <summary>
    //    /// Returns ISelfAsserter that has lurking assertion failures
    //    /// and has been marked as fully constructed.
    //    /// </summary>
    //    /// <remarks>
    //    /// Must be overridden to return an object of the class under testing.</remarks>
    //    /// <returns>Testable ISelfAsserter suitable for general testing.</returns>
    //    public override ISelfAsserter CompletelyConstructedBadInstance()
    //    {
    //        return
    //            new TestSelfAsserterImmutable(
    //                new TestSelfAsserter(true, true)
    //                );
    //    }
    //    /// <item><see cref="TypeUnderTesting"/> </item>
    //    /// <item><see cref="DelegeeForTypeUnderTesting"/> </item>
    //    /// <item><see cref="DelegeeTypeForTypeUnderTesting"/> </item>

    //    /// <summary>
    //    /// Returns ISelfAsserter that has lurking assertion failures
    //    /// and has been marked as fully constructed.
    //    /// </summary>
    //    /// <remarks>
    //    /// Must be overridden to return an object of the class under testing.</remarks>
    //    /// <returns>Testable ISelfAsserter suitable for general testing.</returns>
    //    public virtual ISelfAsserter DelegeeForTypeUnderTesting()
    //    {
    //        return new TestSelfAsserter(false, true);
    //    }

    //    /// <summary>
    //    /// Returns ISelfAsserter that has lurking assertion failures
    //    /// and has been marked as fully constructed.
    //    /// </summary>
    //    /// <remarks>
    //    /// Must be overridden to return an object of the class under testing.</remarks>
    //    /// <returns>Testable ISelfAsserter suitable for general testing.</returns>
    //    public virtual Type DelegeeTypeForTypeUnderTesting()
    //    {
    //        return typeof(TestSelfAsserter);
    //    }

    //    /***************************************************
    //     *** 
    //     *** 
    //     *** VALIDATION METHODS
    //     *** 
    //     *** No new validation methods are required; we
    //     *** need only ensure that the inherited ISelfAsserter
    //     *** and IImutable tests pass.
    //     ************************************************ */

    //    /***************************************************
    //     *** 
    //     *** 
    //     *** TEST CASES
    //     *** 
    //     *** 
    //     ************************************************ */

    //    /// <summary>
    //    /// Ensures that class passes inherited IImmutable
    //    /// contract tests.
    //    /// </summary>
    //    [TestCase]
    //    public void PassesAllIImmutableContractTests()
    //    {
    //        (new InnerImmutableTester(this)).ExecuteAllTests();
    //    }

    //}






}

