﻿using System;
using System.Reflection;
using NUnit.Framework;
using Com.VeritasTS.CoreUtils;

// This tester is ready for use.

namespace Com.VeritasTS.CoreUtils.Test
{

    /// <summary>
    /// Base class tester for anything implementing the 
    /// Com.VeritasTS.TextAndErrorUtils.IMemberMatcher interface.
    /// </summary>
    /// <remarks>Anyone implementing the
    /// Com.VeritasTS.TextAndErrorUtils.IMemberMatcher must
    /// ensure that their implementation both fully honors
    /// the interface contract, and is proven to do so by
    /// thorough unit testing. 
    /// <para>Only the normal virtual TestableObject()
    /// method is required; there are no special cases to be tested
    /// at this level.</para>
    /// </remarks>
    [TestFixture]
    public class BaseIMemberMatcherTesterCore : EqualsAndHashCodeTesterCore
    {

        protected class TestMemberMatcher : IMemberMatcher
        {
            public TestMemberMatcher(int i)
            {
                I = i;
            }
            public int I { get; }
            public override bool Equals(object obj)
            {
                if (obj == null) return false;
                if (this == obj) return true;
                if (!TypeCanEqualThis(obj.GetType())) return false;
                return MembersMatch(obj);
            }
            public override int GetHashCode() { return I; }
            public bool TypeCanEqualThis(Type type)
            {
                return typeof(TestMemberMatcher).IsAssignableFrom(type);
            }
            public bool MembersMatch(object obj)
            {
                if (obj == null) return false;
                if (!typeof(TestMemberMatcher).IsAssignableFrom(obj.GetType())) return false;
                return (I == ((TestMemberMatcher)obj).I);
            }
        }

        protected class TestMemberMatcherDC : TestMemberMatcher
        {
            public TestMemberMatcherDC(int i) : base(i) { }
        }

        protected class Unacceptable
        {
            public Unacceptable(int i)
            {
                I = i;
            }
            public int I { get; }
        }

        /// <summary>
        /// Overrides
        /// <see cref="EqualsAndHashCodeTester.TestableObject"/>.
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an object of the class under testing.</remarks>
        /// <returns>Testable IMemberMatcher suitable for general testing.</returns>
        public override object TestableObject()
        {
            return new TestMemberMatcher(1);
        }

        /// <summary>
        /// Overrides
        /// <see cref="EqualsAndHashCodeTester.DifferentObjectEqualToTestableObject"/>.
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an object of the class under testing.</remarks>
        /// <returns>Testable IMemberMatcher suitable for general testing.</returns>
        public override object DifferentObjectEqualToTestableObject()
        {
            return new TestMemberMatcher(1);
        }

        /// <summary>
        /// Overrides
        /// <see cref="EqualsAndHashCodeTester.SecondDifferentObjectEqualToTestableObject"/>.
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an object of the class under testing.</remarks>
        /// <returns>Testable IMemberMatcher suitable for general testing.</returns>
        public override object SecondDifferentObjectEqualToTestableObject()
        {
            return new TestMemberMatcher(1);
        }

        /// <summary>
        /// Overrides
        /// <see cref="EqualsAndHashCodeTester.ObjectOfTestableClassWithHashCodeNotEqualToTestableObject()"/>.
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an object of the class under testing.</remarks>
        /// <returns>Testable IMemberMatcher suitable for general testing.</returns>
        public override object ObjectOfTestableClassWithHashCodeNotEqualToTestableObject()
        {
            return new TestMemberMatcher(0);
        }

        /// <summary>
        /// Returns IMemberMatcher that is not a member 
        /// of the exact class under testing, but that is still
        /// equal to TestableObject().
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an IMemberMatcher object 
        /// suitable for testing against TestableObject().</remarks>
        /// <returns>Appropriate IMemberMatcher object. If there is no such
        /// class then we should not have bothered implementing IMemberMatcher;
        /// so should never return null.</returns>
        public virtual object ObjectOfDifferentTypeYetEqualToTestableObject()
        {
            return new TestMemberMatcherDC(1);
        }

        /// <summary>
        /// Returns IMemberMatcher that is not a member 
        /// of the exact class under testing, and is not
        /// equal to TestableObject(), but that is of a class
        /// whose instances could be equal to TestableObject().
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an IMemberMatcher object 
        /// suitable for testing against TestableObject().</remarks>
        /// <returns>Appropriate IMemberMatcher object. If there is no such
        /// class then we should not have bothered implementing IMemberMatcher;
        /// so should never return null.</returns>
        public virtual object ObjectOfAcceptableDifferentTypeNotEqualToTestableObject()
        {
            return new TestMemberMatcherDC(0);
        }

        /// <summary>
        /// Returns IMemberMatcher that is not a member 
        /// of the exact class under testing, and is not
        /// equal to TestableObject(), but that is of a class
        /// whose instances could be equal to TestableObject().
        /// </summary>
        /// <remarks>
        /// Any object would do other than objects of the class
        /// under testing and its descendants/partners; the
        /// default of new {} should rarely if ever require overriding.</remarks>
        /// <returns>Appropriate object. </returns>
        public virtual object ObjectOfTypeUnacceptablyDifferentFromTestableObject()
        {
            return new { };
        }

        /***************************************************
         *** 
         *** 
         *** TEST CASES
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Ensures that Equals is false if not
        /// TypeCanBeEqual(obj.GetType()).
        /// </summary>
        [TestCase]
        public void IfObjIsUnacceptableTypeThenIsNotEqual()
        {
            object testable = TestableObject();
            if (testable != null)
            {
                object different = ObjectOfTypeUnacceptablyDifferentFromTestableObject();
                if (different != null)
                    Assert.True(
                        !testable.GetType().Equals(different.GetType())
                        && !testable.Equals(different)
                        , testable.GetType().FullName
                        );
            }
        }

        /// <summary>
        /// Ensures that Equals is false if 
        /// TypeCanBeEqual(obj.GetType()) though this
        /// and obj are not of the same type, but
        /// members don't match.
        /// </summary>
        [TestCase]
        public void IfTypeIsAcceptableButNotMembersMatchThenIsNotEqual()
        {
            object testable = TestableObject();
            if (testable != null)
            {
                object different = ObjectOfAcceptableDifferentTypeNotEqualToTestableObject();
                if (different != null)
                    Assert.True(
                        !testable.GetType().Equals(different.GetType())
                        && !testable.Equals(different)
                        , testable.GetType().FullName
                        );
            }
        }

        /// <summary>
        /// Ensures that Equals is true if 
        /// TypeCanBeEqual(obj.GetType()) though this
        /// and obj are not of the same type, and
        /// members match.
        /// </summary>
        [TestCase]
        public void IfTypeIsAcceptableAndMembersMatchThenIsEqual()
        {
            object testable = TestableObject();
            if (testable != null)
            {
                object different = ObjectOfDifferentTypeYetEqualToTestableObject();
                if (different != null)
                    Assert.True(
                        !testable.GetType().Equals(different.GetType())
                        && testable.Equals(different)
                        , testable.GetType().FullName
                        );
            }
        }

        /// <summary>
        /// Ensures that Equals is false if 
        /// GetType.Equals(obj.GetType()), but
        /// members don't match.
        /// </summary>
        [TestCase]
        public void IfTypeIsIdenticalButNotMembersMatchThenIsNotEqual()
        {
            object testable = TestableObject();
            if (testable != null)
            {
                object different = ObjectOfTestableClassWithHashCodeNotEqualToTestableObject();
                if (different != null)
                    Assert.True(
                        testable.GetType().Equals(different.GetType())
                        && !testable.Equals(different)
                        , testable.GetType().FullName
                        );
            }
        }

        /// <summary>
        /// Ensures that Equals is true if 
        /// GetType.Equals(obj.GetType()), and
        /// members match.
        /// </summary>
        [TestCase]
        public void IfTypeIsIdenticalAndMembersMatchThenIsEqual()
        {
            object testable = TestableObject();
            if (testable != null)
            {
                object different = DifferentObjectEqualToTestableObject();
                if (different != null)
                    Assert.True(
                        testable.GetType().Equals(different.GetType())
                        && testable.Equals(different)
                        , testable.GetType().FullName
                        );
            }
        }

        /// <summary>
        /// Ensures that GetHashCode == obj.GetHashCode if 
        /// TypeCanBeEqual(obj.GetType()) though this
        /// and obj are not of the same type, and
        /// members match.
        /// </summary>
        [TestCase]
        public void IfTypeIsAcceptableAndMembersMatchThenHashCodesAreEqual()
        {
            object testable = TestableObject();
            if (testable != null)
            {
                object different = ObjectOfDifferentTypeYetEqualToTestableObject();
                if (different != null)
                    Assert.True(
                        !testable.GetType().Equals(different.GetType())
                        && testable.GetHashCode() == different.GetHashCode()
                        , testable.GetType().FullName
                        );
            }
        }

        /// <summary>
        /// Ensures that GetHashCode == obj.GetHashCode if 
        /// GetType.Equals(obj.GetType()), and
        /// members match.
        /// </summary>
        [TestCase]
        public void IfTypeIsIdenticalAndMembersMatchThenHashCodesAreEqual()
        {
            object testable = TestableObject();
            if (testable != null)
            {
                object different = DifferentObjectEqualToTestableObject();
                if (different != null)
                    Assert.True(
                        testable.GetType().Equals(different.GetType())
                        && testable.GetHashCode() == different.GetHashCode()
                        , testable.GetType().FullName
                        );
            }
        }

    }

    /// <summary>
    /// Base class tester for anything implementing the 
    /// Com.VeritasTS.TextAndErrorUtils.IMemberMatcher interface.
    /// </summary>
    /// <remarks>Anyone implementing the
    /// Com.VeritasTS.TextAndErrorUtils.IMemberMatcher must
    /// ensure that their implementation both fully honors
    /// the interface contract, and is proven to do so by
    /// thorough unit testing. 
    /// <para>Only the normal virtual TestableObject()
    /// method is required; there are no special cases to be tested
    /// at this level.</para>
    /// </remarks>
    [TestFixture]
    public class BaseIMemberMatcherTester : EqualsAndHashCodeTester
    {

        /***************************************************
         *** 
         *** 
         *** TEST CASES
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Ensures that Equals is false if not
        /// TypeCanBeEqual(obj.GetType()).
        /// </summary>
        [TestCase]
        public void IfObjIsUnacceptableTypeThenIsNotEqual()
        {
            (new BaseIMemberMatcherTesterCore())
                .IfObjIsUnacceptableTypeThenIsNotEqual();
        }

        /// <summary>
        /// Ensures that Equals is false if 
        /// TypeCanBeEqual(obj.GetType()) though this
        /// and obj are not of the same type, but
        /// members don't match.
        /// </summary>
        [TestCase]
        public void IfTypeIsAcceptableButNotMembersMatchThenIsNotEqual()
        {
            (new BaseIMemberMatcherTesterCore())
                .IfTypeIsAcceptableButNotMembersMatchThenIsNotEqual();
        }

        /// <summary>
        /// Ensures that Equals is true if 
        /// TypeCanBeEqual(obj.GetType()) though this
        /// and obj are not of the same type, and
        /// members match.
        /// </summary>
        [TestCase]
        public void IfTypeIsAcceptableAndMembersMatchThenIsEqual()
        {
            (new BaseIMemberMatcherTesterCore())
                .IfTypeIsAcceptableAndMembersMatchThenIsEqual();
        }

        /// <summary>
        /// Ensures that Equals is false if 
        /// GetType.Equals(obj.GetType()), but
        /// members don't match.
        /// </summary>
        [TestCase]
        public void IfTypeIsIdenticalButNotMembersMatchThenIsNotEqual()
        {
            (new BaseIMemberMatcherTesterCore())
                .IfTypeIsIdenticalButNotMembersMatchThenIsNotEqual();
        }

        /// <summary>
        /// Ensures that Equals is true if 
        /// GetType.Equals(obj.GetType()), and
        /// members match.
        /// </summary>
        [TestCase]
        public void IfTypeIsIdenticalAndMembersMatchThenIsEqual()
        {
            (new BaseIMemberMatcherTesterCore())
                .IfTypeIsIdenticalAndMembersMatchThenIsEqual();
        }

        /// <summary>
        /// Ensures that GetHashCode == obj.GetHashCode if 
        /// TypeCanBeEqual(obj.GetType()) though this
        /// and obj are not of the same type, and
        /// members match.
        /// </summary>
        [TestCase]
        public void IfTypeIsAcceptableAndMembersMatchThenHashCodesAreEqual()
        {
            (new BaseIMemberMatcherTesterCore())
                .IfTypeIsIdenticalAndMembersMatchThenIsEqual();
        }

        /// <summary>
        /// Ensures that GetHashCode == obj.GetHashCode if 
        /// GetType.Equals(obj.GetType()), and
        /// members match.
        /// </summary>
        [TestCase]
        public void IfTypeIsIdenticalAndMembersMatchThenHashCodesAreEqual()
        {
            (new BaseIMemberMatcherTesterCore())
                .IfTypeIsIdenticalAndMembersMatchThenIsEqual();
        }

    }

}
