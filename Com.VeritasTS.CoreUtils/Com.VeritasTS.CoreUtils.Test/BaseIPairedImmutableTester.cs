﻿using System;
using System.Reflection;
using NUnit.Framework;

// This tester is ready for use.

namespace Com.VeritasTS.CoreUtils.Test
{

    /// <summary>
    /// Base class tester for anything implementing the 
    /// Com.VeritasTS.TextAndErrorUtils.IPairedImmutable interface.
    /// </summary>
    /// <remarks>Anyone implementing the
    /// Com.VeritasTS.TextAndErrorUtils.IPairedImmutable must
    /// ensure that their implementation both fully honors
    /// the interface contract, and is proven to do so by
    /// thorough unit testing. 
    /// </remarks>
    public class BaseIPairedImmutableTesterCore : BaseIImmutableTesterCore
    {

        protected class PairedDelegee : Delegee, IPairedMutable
        {
            public override bool Equals(object obj)
            {
                return
                    obj == null ? false
                    : (
                    obj.GetType().Equals(typeof(PairedDelegee))
                    || obj.GetType().Equals(typeof(PairedDelegeeImmutable))
                    );
            }
            public override int GetHashCode()
            {
                return 7103;
            }
            public virtual Type PairedImmutableType { get { return typeof(PairedDelegeeImmutable); } }
        }

        protected class PairedDelegeeImmutable : DelegeeImmutable, IPairedImmutable
        {
            public PairedDelegeeImmutable(PairedDelegee delegee)
                : base(delegee)
            { }

            public PairedDelegeeImmutable(PairedDelegee delegee, bool fullyConstructed)
                : base(delegee, fullyConstructed)
            { }

            protected override Type ValidTypeForDelegee { get { return PairedMutableType; } }

            public virtual Type PairedMutableType { get { return typeof(PairedDelegee); } }
        }

        /// <summary>
        /// Overrides
        /// <see cref="BaseIImmutableTesterCore.IncompletelyConstructedInstance"/>.
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an object of the class under testing.</remarks>
        /// <returns>Testable IPairedImmutable suitable for general testing.</returns>
        public override IImmutable IncompletelyConstructedInstance()
        {
            return new PairedDelegeeImmutable(new PairedDelegee(), false);
        }

        /// <summary>
        /// Overrides
        /// <see cref="BaseIImmutableTesterCore.CompletelyConstructedInstance"/>.
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an object of the class under testing.</remarks>
        /// <returns>Testable IPairedImmutable suitable for general testing.</returns>
        public override IImmutable CompletelyConstructedInstance()
        {
            return ImmutableWithEqualMutable();
        }

        /// <summary>
        /// Instance of IPairedImmutable class under testing
        /// that is equal to 
        /// <see cref="MutableEqualToImmutableWithEqualMutable"/>. 
        /// </summary>
        /// <returns>Suitable instance of class under testing.</returns>
        public virtual IPairedImmutable ImmutableWithEqualMutable()
        {
            return new PairedDelegeeImmutable(
                (PairedDelegee)MutableEqualToImmutableWithEqualMutable()
                , true
                );
        }

        /// <summary>
        /// Instance of IPairedMutable class under testing
        /// that is equal to 
        /// <see cref="ImmutableWithEqualMutable"/>. 
        /// </summary>
        /// <returns>Suitable instance of IPairedMutable 
        /// class paired to class under testing.</returns>
        public virtual IPairedMutable MutableEqualToImmutableWithEqualMutable()
        {
            return new PairedDelegee();
        }

        /***************************************************
         *** 
         *** 
         *** VALIDATION METHODS
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Ensures that instances of the paired classes can
        /// be equal to each other.
        /// </summary>
        /// <param name="i">The IPairedImmutable object</param>
        /// <param name="mEqualToI">The IPairedMutable object</param>
        public void AtLeastOnePairedImmutableObjectEqualsAtLeastOnePairedMutableObject(
            IPairedImmutable i
            , IPairedMutable mEqualToI
            )
        {
            Assert.True(
                i != null && i.Equals(mEqualToI)
                , i.GetType().FullName
                );
        }

        /// <summary>
        /// Ensures that PairedMutableType is assignable to IPairedMutable.
        /// </summary>
        /// <param name="testableObject">A testable instance of the class
        /// under testing</param>
        public void PairedMutableClassIsIPairedMutable(
            IPairedImmutable testableObject
            )
        {
            if (testableObject != null)
            {
                Type t = testableObject.GetType();
                Assert.True(
                    typeof(IPairedMutable)
                    .IsAssignableFrom(testableObject.PairedMutableType)
                    , testableObject.GetType().FullName
                    );
            }
        }

        /***************************************************
         *** 
         *** 
         *** TEST CASES
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Ensures that instances of the paired classes can
        /// be equal to each other.
        /// </summary>
        public void AtLeastOnePairedImmutableObjectEqualsAtLeastOnePairedMutableObject()
        {
            AtLeastOnePairedImmutableObjectEqualsAtLeastOnePairedMutableObject(
                ImmutableWithEqualMutable()
                , MutableEqualToImmutableWithEqualMutable()
                );
        }

        /// <summary>
        /// Ensures that PairedMutableClass is
        /// assignable to IPairedMutable.
        /// </summary>
        public void PairedMutableClassIsIPairedMutable()
        {
            PairedMutableClassIsIPairedMutable(
                (IPairedImmutable)TestableObject()
                );
        }


        /***************************************************
         *** 
         *** 
         *** MASTER TEST METHOD
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Applies all contract validation to the class currently under testing.
        /// </summary>
        /// <remarks>
        /// Used when IPairedImmutable is one of several interfaces
        /// implemented and the implementing class's Tester
        /// class includes BaseIPairedImmutableTester by composition
        /// rather than inheritance.
        /// </remarks>
        public override void Validate()
        {
            base.Validate();
            AtLeastOnePairedImmutableObjectEqualsAtLeastOnePairedMutableObject();
            PairedMutableClassIsIPairedMutable();
        }

    }

    /// <summary>
    /// Base class tester for anything implementing the 
    /// Com.VeritasTS.TextAndErrorUtils.IPairedImmutable interface.
    /// </summary>
    /// <remarks>Anyone implementing the
    /// Com.VeritasTS.TextAndErrorUtils.IPairedImmutable must
    /// ensure that their implementation both fully honors
    /// the interface contract, and is proven to do so by
    /// thorough unit testing. 
    /// </remarks>
    [TestFixture]
    public class BaseIPairedImmutableTester : BaseIImmutableTester
    {

        /***************************************************
         *** 
         *** 
         *** TEST CASES
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Ensures that instances of the paired classes can
        /// be equal to each other.
        /// </summary>
        [TestCase]
        public void AtLeastOnePairedImmutableObjectEqualsAtLeastOnePairedMutableObject()
        {
            (new BaseIPairedImmutableTesterCore())
                .AtLeastOnePairedImmutableObjectEqualsAtLeastOnePairedMutableObject();
        }

        /// <summary>
        /// Ensures that PairedMutableClass is
        /// assignable to IPairedMutable.
        /// </summary>
        [TestCase]
        public void PairedMutableClassIsIPairedMutable()
        {
            (new BaseIPairedImmutableTesterCore())
                .PairedMutableClassIsIPairedMutable();
        }


        /***************************************************
         *** 
         *** 
         *** MASTER TEST METHOD
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Applies all contract validation to the class currently under testing.
        /// </summary>
        /// <remarks>
        /// Used when IPairedImmutable is one of several interfaces
        /// implemented and the implementing class's Tester
        /// class includes BaseIPairedImmutableTester by composition
        /// rather than inheritance.
        /// </remarks>
        public override void Validate()
        {
            (new BaseIPairedImmutableTesterCore())
                .Validate();
        }

    }

}
