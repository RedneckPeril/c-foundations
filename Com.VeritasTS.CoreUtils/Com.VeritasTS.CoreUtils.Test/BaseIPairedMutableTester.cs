﻿using System;
using System.Reflection;
using NUnit.Framework;

// This tester is ready for use.

namespace Com.VeritasTS.CoreUtils.Test
{

    /// <summary>
    /// Base class tester for anything implementing the 
    /// Com.VeritasTS.TextAndErrorUtils.IPairedImmutable interface.
    /// </summary>
    /// <remarks>Anyone implementing the
    /// Com.VeritasTS.TextAndErrorUtils.IPairedImmutable must
    /// ensure that their implementation both fully honors
    /// the interface contract, and is proven to do so by
    /// thorough unit testing. 
    /// </remarks>
    public class BaseIPairedMutableTesterCore : BaseIMutableTesterCore
    {

        protected class PairedDelegee : PairedMutable
        {
            public override bool Equals(object obj)
            {
                return
                    obj == null ? false
                    : (
                    obj.GetType().Equals(typeof(PairedDelegee))
                    || obj.GetType().Equals(typeof(PairedDelegeeImmutable))
                    );
            }
            public override int GetHashCode()
            {
                return 5693;
            }
        }

        protected class PairedDelegeeImmutable : Immutable, IPairedImmutable
        {
            public PairedDelegeeImmutable(PairedDelegee delegee)
                : base(delegee)
            { }

            public override bool IsFullyConstructed { get { return true; } }

            public override bool MarkAsFullyConstructed(Type testableType = null)
            {
                return true;
            }

            protected override Type ValidTypeForDelegee { get { return PairedMutableType; } }

            public virtual Type PairedMutableType { get { return typeof(PairedDelegee); } }
        }

        /// <summary>
        /// Overrides
        /// <see cref="BaseIImmutableTesterCore.IncompletelyConstructedInstance"/>.
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an object of the class under testing.</remarks>
        /// <returns>Testable IMutable suitable for general testing.</returns>
        public override IMutable TestableObject()
        {
            return new PairedDelegee();
        }

        /// <summary>
        /// Instance of IPairedMutable class under testing
        /// that is equal to 
        /// <see cref="ImmutableEqualToMutableWithEqualImmutable"/>. 
        /// </summary>
        /// <returns>Suitable instance of class under testing.</returns>
        public virtual IPairedMutable MutableWithEqualImmutable()
        {
            return new PairedDelegee();
        }

        /// <summary>
        /// Instance of IPairedImmutable class under testing
        /// that is equal to 
        /// <see cref="MutableWithEqualImmutable"/>. 
        /// </summary>
        /// <returns>Suitable instance of IPairedImmutable 
        /// class paired to class under testing.</returns>
        public virtual IPairedImmutable ImmutableEqualToMutableWithEqualImmutable()
        {
            return new PairedDelegeeImmutable(
                (PairedDelegee)MutableWithEqualImmutable()
                );
        }

        /***************************************************
         *** 
         *** 
         *** VALIDATION METHODS
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Ensures that instances of the paired classes can
        /// be equal to each other.
        /// </summary>
        /// <param name="m">The IPairedMutable object</param>
        /// <param name="iEqualToM">The IPairedImmutable object</param>
        public void AtLeastOnePairedMutableObjectEqualsAtLeastOnePairedImmutableObject(
            IPairedMutable m
            , IPairedImmutable iEqualToM
            )
        {
            Assert.True(
                m != null && m.Equals(iEqualToM)
                , m.GetType().FullName
                );
        }

        /// <summary>
        /// Ensures that PairedImmutableType is assignable to IPairedImmutable.
        /// </summary>
        /// <param name="testableObject">A testable instance of the class
        /// under testing</param>
        public void PairedImmutableClassIsIPairedImmutable(
            IPairedMutable testableObject
            )
        {
            if (testableObject != null)
            {
                Type t = testableObject.GetType();
                Assert.False(
                    typeof(IPairedImmutable)
                    .IsAssignableFrom(testableObject.PairedImmutableType)
                    , testableObject.GetType().FullName
                    );
            }
        }

        /***************************************************
         *** 
         *** 
         *** TEST CASES
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Ensures that instances of the paired classes can
        /// be equal to each other.
        /// </summary>
        public void AtLeastOnePairedMutableObjectEqualsAtLeastOnePairedImmutableObject()
        {
            AtLeastOnePairedMutableObjectEqualsAtLeastOnePairedImmutableObject(
                MutableWithEqualImmutable()
                , ImmutableEqualToMutableWithEqualImmutable()
                );
        }

        /// <summary>
        /// Ensures that PairedImmutableClass is
        /// assignable to IPairedImmutable.
        /// </summary>
        public void PairedImmutableClassIsIPairedImmutable()
        {
            PairedImmutableClassIsIPairedImmutable(
                (IPairedMutable)TestableObject()
                );
        }


        /***************************************************
         *** 
         *** 
         *** MASTER TEST METHOD
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Applies all contract validation to the class currently under testing.
        /// </summary>
        /// <remarks>
        /// Used when IPairedMutable is one of several interfaces
        /// implemented and the implementing class's Tester
        /// class includes BaseIPairedMutableTester by composition
        /// rather than inheritance.
        /// </remarks>
        public virtual void Validate()
        {
            AtLeastOnePairedMutableObjectEqualsAtLeastOnePairedImmutableObject();
            PairedImmutableClassIsIPairedImmutable();
        }

    }

    /// <summary>
    /// Base class tester for anything implementing the 
    /// Com.VeritasTS.CoreUtils.IPairedMutable interface.
    /// </summary>
    /// <remarks>Anyone implementing the
    /// Com.VeritasTS.CoreUtils.IPairedMutable must
    /// ensure that their implementation both fully honors
    /// the interface contract, and is proven to do so by
    /// thorough unit testing. 
    /// </remarks>
    [TestFixture]
    public class BaseIPairedMutableTester : BaseIMutableTester
    {

        /***************************************************
         *** 
         *** 
         *** TEST CASES
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Ensures that instances of the paired classes can
        /// be equal to each other.
        /// </summary>
        [TestCase]
        public void AtLeastOnePairedMutableObjectEqualsAtLeastOnePairedImmutableObject()
        {
            (new BaseIPairedMutableTesterCore())
                .AtLeastOnePairedMutableObjectEqualsAtLeastOnePairedImmutableObject();
        }

        /// <summary>
        /// Ensures that instances of the paired classes can
        /// be equal to each other.
        /// </summary>
        [TestCase]
        public void PairedImmutableClassIsIPairedImmutable()
        {
            (new BaseIPairedMutableTesterCore())
                .PairedImmutableClassIsIPairedImmutable();
        }


        /***************************************************
         *** 
         *** 
         *** MASTER TEST METHOD
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Applies all contract validation to the class currently under testing.
        /// </summary>
        /// <remarks>
        /// Used when IPairedImmutable is one of several interfaces
        /// implemented and the implementing class's Tester
        /// class includes BaseIPairedImmutableTester by composition
        /// rather than inheritance.
        /// </remarks>
        public override void Validate()
        {
            (new BaseIPairedMutableTesterCore())
                .Validate();
        }

    }

}
