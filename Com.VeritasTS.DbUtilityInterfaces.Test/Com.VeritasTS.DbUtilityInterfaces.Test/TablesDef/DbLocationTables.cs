﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.DatabaseUtilities
{
    public class LocationVables : Table
    {
        public LocationVables() : base()
        {
            Schema = new Schema { Name = "dbo" };
            Name = "LocationVables";
            LoadColumn(new Column { Name = "Id", DataType = "int", IsIdentity = true, IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "Name", DataType = "nvarchar(100)", IsNullable = false, Owner = this });
            PrimaryKey = new PrimaryKey { Name = "PK_LocationVables", ColumnNames = new List<string> { "Id" }, Owner = this };
            LoadIndex(new Index { Name = "idx_LocationVables", ColumnNames = new List<string> { "Name" }, Owner = this, IsUnique = true, IsClustered = false });
        }
    }

    public class Locations : Table
    {
        public Locations() : base()
        {
            Schema = new Schema { Name = "dbo" };
            Name = "Locations";
            LoadColumn(new Column { Name = "Id", DataType = "int", IsNullable = false, Owner = this, IsInvariant = true });
            LoadColumn(new Column { Name = "LocationVableID", DataType = "int", IsNullable = false, Owner = this });
            PrimaryKey = new PrimaryKey { Name = "PK_Locations", ColumnNames = new List<string> { "id" }, Owner = this };
            LoadForeignKey(new ForeignKey { Name = "FK_Locations", Owner = this, KeyColumn = new Column { Name = "LocationVableID" }, ReferenceTable = new LocationVables(), ReferenceColumn = new Column { Name = "Id" } });
            LoadTrigger(new Trigger { Owner = this, BeforeInsert = true, Procedure = new StoredProcedure { Name = "SPCalledByInsertTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
        }
    }

    public class PhysicalLocations : Table
    {
        public PhysicalLocations() : base()
        {
            Schema = new Schema { Name = "dbo" };
            Name = "PhysicalLocations";
            LoadColumn(new Column { Name = "Id", DataType = "int", IsNullable = false, Owner = this, IsInvariant = true });
            LoadColumn(new Column { Name = "LocationVableID", DataType = "int", IsNullable = false, Owner = this });
            PrimaryKey = new PrimaryKey { Name = "PK_PhysicalLocations", ColumnNames = new List<string> { "id" }, Owner = this };
            LoadForeignKey(new ForeignKey { Name = "FK_PhysicalLocations_LocationVables", Owner = this, KeyColumn = new Column { Name = "LocationVableID" }, ReferenceTable = new LocationVables(), ReferenceColumn = new Column { Name = "Id" } });
            LoadForeignKey(new ForeignKey { Name = "FK_PhysicalLocations_Locations", Owner = this, KeyColumn = new Column { Name = "Id" }, ReferenceTable = new Locations(), ReferenceColumn = new Column { Name = "Id" } });
            ParentTable = new Locations();
            LoadTrigger(new Trigger { Owner = this, BeforeInsert = true, Procedure = new StoredProcedure { Name = "SPCalledByInsertTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
        }
    }

    public class FixedLocations : Table
    {
        public FixedLocations() : base()
        {
            Schema = new Schema { Name = "dbo" };
            Name = "FixedLocations";
            LoadColumn(new Column { Name = "Id", DataType = "int", IsNullable = false, Owner = this, IsInvariant = true });
            LoadColumn(new Column { Name = "LocationVableID", DataType = "int", IsNullable = false, Owner = this });
            PrimaryKey = new PrimaryKey { Name = "PK_FixedLocations", ColumnNames = new List<string> { "id" }, Owner = this };
            LoadForeignKey(new ForeignKey { Name = "FK_FixedLocations_LocationVables", Owner = this, KeyColumn = new Column { Name = "LocationVableID" }, ReferenceTable = new LocationVables(), ReferenceColumn = new Column { Name = "Id" } });
            LoadForeignKey(new ForeignKey { Name = "FK_FixedLocations_PhysicalLocations", Owner = this, KeyColumn = new Column { Name = "Id" }, ReferenceTable = new PhysicalLocations(), ReferenceColumn = new Column { Name = "Id" } });
            ParentTable = new PhysicalLocations();
            LoadTrigger(new Trigger { Owner = this, BeforeInsert = true, Procedure = new StoredProcedure { Name = "SPCalledByInsertTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
        }
    }


    public class PoliticalLocations : Table
    {
        public PoliticalLocations() : base()
        {
            Schema = new Schema { Name = "dbo" };
            Name = "PoliticalLocations";
            LoadColumn(new Column { Name = "Id", DataType = "int", IsNullable = false, Owner = this, IsInvariant = true });
            LoadColumn(new Column { Name = "LocationVableID", DataType = "int", IsNullable = false, Owner = this });
            PrimaryKey = new PrimaryKey { Name = "PK_PoliticalLocations", ColumnNames = new List<string> { "id" }, Owner = this };
            LoadForeignKey(new ForeignKey { Name = "FK_PoliticalLocations_LocationVables", Owner = this, KeyColumn = new Column { Name = "LocationVableID" }, ReferenceTable = new LocationVables(), ReferenceColumn = new Column { Name = "Id" } });
            LoadForeignKey(new ForeignKey { Name = "FK_PoliticalLocations_FixedLocations", Owner = this, KeyColumn = new Column { Name = "Id" }, ReferenceTable = new FixedLocations(), ReferenceColumn = new Column { Name = "Id" } });
            ParentTable = new FixedLocations();
            LoadTrigger(new Trigger { Owner = this, BeforeInsert = true, Procedure = new StoredProcedure { Name = "SPCalledByInsertTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
        }
    }

    public class Location_SequenceTable : SequenceTable
    {
        public Location_SequenceTable() : base()
        {
            Schema = new Schema { Name = "dbo" };
            Name = "Location_SequenceTable";
            SequenceTableIdentityColumn = new Column { Name = "Id", DataType = "int", IsIdentity = true, IsNullable = false, Owner = this };
            SequenceTableInsertColumn = new Column { Name = "x", DataType = "int", Owner = this };
            LoadColumn(SequenceTableIdentityColumn);
            LoadColumn(SequenceTableInsertColumn);
        }
    }

    public class Nations : Table
    {
        public Nations() : base()
        {
            Name = "Nations";
            Schema = new Schema { Name = "dbo" };
            LoadColumn(new Column { Name = "Id", DataType = "int", IsNullable = false, Owner = this, IsInvariant = true });
            LoadColumn(new Column { Name = "Abbrev", DataType = "nvarchar(50)", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "Name", DataType = "nvarchar(100)", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "LastUpdateDtm", DataType = "datetime", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "LastUpdater", DataType = "nvarchar(100)", IsNullable = false, Owner = this });

            PrimaryKey = new PrimaryKey { Name = "PK_Nations", ColumnNames = new List<string> { "id" }, Owner = this };
            ParentTable = new PoliticalLocations();
            PrimaryKeySequence = new PrimaryKeySequence
            {
                PrimaryKeyColumn = new Column { Name = "Id" },
                SequenceTable = new Location_SequenceTable(),
            };
            ParentPassthrough = new ParentPassthroughInfo
            {
                ParentPassthroughColumn = new Column { Name = "LocationVableID", DataType = "int" },
                ParentPassthroughSourceTable = new LocationVables(),
                ParentPassthroughSourceColumn = new Column { Name = "Id", DataType = "int" },
                ParentPassthroughMatchingSourceColumn = new Column { Name = "Name", DataType = "nvarchar(100)" },
                ParentPassthroughLookup = "Nations"
            };
            LoadForeignKey(new ForeignKey { Name = "FK_Nations_PoliticalLocations", Owner = this, KeyColumn = new Column { Name = "Id" }, ReferenceTable = new PoliticalLocations(), ReferenceColumn = new Column { Name = "Id" } });
            LoadIndex(new Index { Name = "idx_Nations_Abbrev", ColumnNames = new List<string> { "Abbrev" }, Owner = this, IsUnique = true, IsClustered = false });
            LoadTrigger(new Trigger { Owner = this, BeforeInsert = true, Procedure = new StoredProcedure { Name = "SPCalledByInsertTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
            LoadTrigger(new Trigger { Owner = this, BeforeDelete = true, Procedure = new StoredProcedure { Name = "SPCalledByDeleteTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
        }
    }

    public class NationalSubdivisions : Table
    {
        public NationalSubdivisions () : base()
        {
            Name = "NationalSubdivisions";
            Schema = new Schema { Name = "dbo" };
            LoadColumn(new Column { Name = "Id", DataType = "int", IsNullable = false, Owner = this, IsInvariant = true });
            LoadColumn(new Column { Name = "Abbrev", DataType = "nvarchar(50)", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "Name", DataType = "nvarchar(100)", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "NationId", DataType = "int", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "LastUpdateDtm", DataType = "datetime", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "LastUpdater", DataType = "nvarchar(100)", IsNullable = false, Owner = this });

            PrimaryKey = new PrimaryKey { Name = "PK_NationalSubdivisions", ColumnNames = new List<string> { "id" }, Owner = this };
            ParentTable = new PoliticalLocations();
            PrimaryKeySequence = new PrimaryKeySequence
            {
                PrimaryKeyColumn = new Column { Name = "Id" },
                SequenceTable = new Location_SequenceTable(),
            };
            ParentPassthrough = new ParentPassthroughInfo
            {
                ParentPassthroughColumn = new Column { Name = "LocationVableID", DataType = "int" },
                ParentPassthroughSourceTable = new LocationVables(),
                ParentPassthroughSourceColumn = new Column { Name = "Id", DataType = "int" },
                ParentPassthroughMatchingSourceColumn = new Column { Name = "Name", DataType = "nvarchar(100)" },
                ParentPassthroughLookup = "NationalSubdivisions"
            };

            LoadForeignKey(new ForeignKey { Name = "FK_NationalSubdivisions_PoliticalLocations", Owner = this, KeyColumn = new Column { Name = "Id" }, ReferenceTable = new PoliticalLocations(), ReferenceColumn = new Column { Name = "Id" } });
            LoadForeignKey(new ForeignKey { Name = "FK_NationalSubdivisions_Nations", Owner = this, KeyColumn = new Column { Name = "NationId" }, ReferenceTable = new Nations(), ReferenceColumn = new Column { Name = "Id" } });
            LoadIndex(new Index { Name = "idx_NationalSubdivisions", ColumnNames = new List<string> { "Abbrev", "NationId" }, Owner = this, IsUnique = true, IsClustered = false });
            LoadTrigger(new Trigger { Owner = this, BeforeInsert = true, Procedure = new StoredProcedure { Name = "SPCalledByInsertTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
            LoadTrigger(new Trigger { Owner = this, BeforeDelete = true, Procedure = new StoredProcedure { Name = "SPCalledByDeleteTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
        }
    }

    public class NatGasPipelines : Table
    {
        public NatGasPipelines()
        {
            Name = "NatGasPipelines";
            Schema = new Schema { Name = "dbo" };
            LoadColumn(new Column { Name = "Id", DataType = "int", IsNullable = false, Owner = this, IsInvariant = true });
            LoadColumn(new Column { Name = "Abbrev", DataType = "nvarchar(50)", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "Name", DataType = "nvarchar(100)", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "LastUpdateDtm", DataType = "datetime", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "LastUpdater", DataType = "nvarchar(100)", IsNullable = false, Owner = this });

            PrimaryKey = new PrimaryKey { Name = "PK_NatGasPipelines", ColumnNames = new List<string> { "id" }, Owner = this };
            ParentTable = new FixedLocations();
            PrimaryKeySequence = new PrimaryKeySequence
            {
                PrimaryKeyColumn = new Column { Name = "Id" },
                SequenceTable = new Location_SequenceTable(),
            };
            ParentPassthrough = new ParentPassthroughInfo
            {
                ParentPassthroughColumn = new Column { Name = "LocationVableID", DataType = "int" },
                ParentPassthroughSourceTable = new LocationVables(),
                ParentPassthroughSourceColumn = new Column { Name = "Id", DataType = "int" },
                ParentPassthroughMatchingSourceColumn = new Column { Name = "Name", DataType = "nvarchar(100)" },
                ParentPassthroughLookup = "NatGasPipelines"
            };

            LoadForeignKey(new ForeignKey { Name = "FK_NatGasPipelines_FixedLocations", Owner = this, KeyColumn = new Column { Name = "Id" }, ReferenceTable = new FixedLocations(), ReferenceColumn = new Column { Name = "Id" } });
            LoadIndex(new Index { Name = "idx_NatGasPipelines", ColumnNames = new List<string> { "Abbrev"}, Owner = this, IsClustered = false });
            LoadTrigger(new Trigger { Owner = this, BeforeInsert = true, Procedure = new StoredProcedure { Name = "SPCalledByInsertTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
            LoadTrigger(new Trigger { Owner = this, BeforeDelete = true, Procedure = new StoredProcedure { Name = "SPCalledByDeleteTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
        }
    }
    public class NatGasFacilityPoints : Table
    {
        public NatGasFacilityPoints()
        {
            Name = "NatGasFacilityPoints";
            Schema = new Schema { Name = "dbo" };
            LoadColumn(new Column { Name = "Id", DataType = "int", IsNullable = false, Owner = this, IsInvariant = true });
            LoadColumn(new Column { Name = "Abbrev", DataType = "nvarchar(50)", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "Name", DataType = "nvarchar(100)", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "NatGasPipelineID", DataType = "int", Owner = this });
            LoadColumn(new Column { Name = "LastUpdateDtm", DataType = "datetime", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "LastUpdater", DataType = "nvarchar(100)", IsNullable = false, Owner = this });

            PrimaryKey = new PrimaryKey { Name = "PK_NatGasFacilityPoints", ColumnNames = new List<string> { "id" }, Owner = this };
            ParentTable = new FixedLocations();
            PrimaryKeySequence = new PrimaryKeySequence
            {
                PrimaryKeyColumn = new Column { Name = "Id" },
                SequenceTable = new Location_SequenceTable(),
            };
            ParentPassthrough = new ParentPassthroughInfo
            {
                ParentPassthroughColumn = new Column { Name = "LocationVableID", DataType = "int" },
                ParentPassthroughSourceTable = new LocationVables(),
                ParentPassthroughSourceColumn = new Column { Name = "Id", DataType = "int" },
                ParentPassthroughMatchingSourceColumn = new Column { Name = "Name", DataType = "nvarchar(100)" },
                ParentPassthroughLookup = "NatGasFacilityPoints"
            };

            LoadForeignKey(new ForeignKey { Name = "FK_NatGasFacilityPoints_FixedLocations", Owner = this, KeyColumn = new Column { Name = "Id" }, ReferenceTable = new FixedLocations(), ReferenceColumn = new Column { Name = "Id" } });
            LoadForeignKey(new ForeignKey { Name = "FK_NatGasFacilityPoints_NatGasPipelines", Owner = this, KeyColumn = new Column { Name = "NatGasPipelineID" }, ReferenceTable = new NatGasPipelines(), ReferenceColumn = new Column { Name = "Id" } });
            LoadIndex(new Index { Name = "idx_NatGasFacilityPoints", ColumnNames = new List<string> { "Abbrev", "NatGasPipelineID" }, Owner = this, IsUnique = true, IsClustered = false });
            LoadTrigger(new Trigger { Owner = this, BeforeInsert = true, Procedure = new StoredProcedure { Name = "SPCalledByInsertTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
            LoadTrigger(new Trigger { Owner = this, BeforeDelete = true, Procedure = new StoredProcedure { Name = "SPCalledByDeleteTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
        }
    }

}
