﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.DatabaseUtilities
{
    public class testTable2 : Table
    {
        public testTable2() : base()
        {
            Name = "testTable2";
            Schema = new Schema { Name = "dbo" };
            LoadColumn(new Column { Name = "Id", DataType = "int", IsNullable = false, Owner = this, IsInvariant = true });
            LoadColumn(new Column { Name = "Abbrev", DataType = "nvarchar(50)", IsNullable = false, Owner = this, IsInvariant = true });
            LoadColumn(new Column { Name = "Name", DataType = "nvarchar(100)", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "NationId", DataType = "int", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "LastUpdateDtm", DataType = "datetime", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "LastUpdater", DataType = "nvarchar(100)", IsNullable = false, Owner = this });

            PrimaryKey = new PrimaryKey { Name = "PK_testTable2", ColumnNames = new List<string> { "id" }, Owner = this };
            ParentTable = new PoliticalLocations();
            PrimaryKeySequence = new PrimaryKeySequence
            {
                PrimaryKeyColumn = new Column { Name = "Id" },
                SequenceTable = new Location_SequenceTable(),
            };
            ParentPassthrough = new ParentPassthroughInfo
            {
                ParentPassthroughColumn = new Column { Name = "LocationVableID" },
                ParentPassthroughSourceTable = new LocationVables(),
                ParentPassthroughSourceColumn = new Column { Name = "Id" },
                ParentPassthroughMatchingSourceColumn = new Column { Name = "Name" },
                ParentPassthroughLookup = "testTable2"
            };
            LoadForeignKey(new ForeignKey { Name = "FK_testTable2_PoliticalLocations", Owner = this, KeyColumn = new Column { Name = "Id" }, ReferenceTable = new PoliticalLocations(), ReferenceColumn = new Column { Name = "Id" } });
            LoadForeignKey(new ForeignKey { Name = "FK_testTable2_Nations", Owner = this, KeyColumn = new Column { Name = "NationId" }, ReferenceTable = new Nations(), ReferenceColumn = new Column { Name = "Id" }, IsCascadeDelete = true });
            LoadIndex(new Index { Name = "idx_testTable2", ColumnNames = new List<string> { "Abbrev", "NationId" }, Owner = this, IsUnique = true, IsClustered = false });
            LoadCheckConstraint(new CheckConstraint { Name = "ck_testTable2", Owner = this, ConstraintText = "len(Abbrev) > 0 and len(Name) > 0" });
            LoadTrigger(new Trigger { Owner = this, BeforeInsert = true, Procedure = new StoredProcedure { Name = "SPCalledByInsertTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
            LoadTrigger(new Trigger { Owner = this, BeforeDelete = true, Procedure = new StoredProcedure { Name = "SPCalledByDeleteTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
            LoadTrigger(new Trigger { Owner = this, BeforeUpdate = true, Procedure = new StoredProcedure { Name = "SPCalledByUpdateTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
        }
    }

    public class testTable3 : Table
    {
        public testTable3() : base()
        {
            Schema = new Schema { Name = "dbo" };
            Name = "testTable3";
            LoadColumn(new Column { Name = "Id", DataType = "int", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "testTable2Id", DataType = "int", IsNullable = false, Owner = this });
            PrimaryKey = new PrimaryKey { Name = "PK_testTable3", ColumnNames = new List<string> { "id" }, Owner = this };
            LoadForeignKey(new ForeignKey { Name = "FK_testTable3_testTable2", Owner = this, KeyColumn = new Column { Name = "testTable2Id" }, ReferenceTable = new testTable2(), ReferenceColumn = new Column { Name = "Id" }, IsCascadeDelete = true });
            LoadTrigger(new Trigger { Owner = this, BeforeDelete = true, Procedure = new StoredProcedure { Name = "SPCalledByDeleteTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
        }
    }
    public class testTable : Table
    {
        public testTable() : base()
        {
            Name = "testTable";
            Schema = new Schema { Name = "dbo" };
            LoadColumn(new Column { Name = "Id", DataType = "int", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "Abbrev", DataType = "nvarchar(50)", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "Name", DataType = "nvarchar(100)", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "NationId", DataType = "int", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "LastUpdateDtm", DataType = "datetime", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "LastUpdater", DataType = "nvarchar(100)", IsNullable = false, Owner = this });

            PrimaryKey = new PrimaryKey { Name = "PK_testTable", ColumnNames = new List<string> { "id" }, Owner = this };
            LoadForeignKey(new ForeignKey { Name = "FK_testTable", Owner = this, KeyColumn = new Column { Name = "NationId" }, ReferenceTable = new Nations(), ReferenceColumn = new Column { Name = "Id" } });
            IsAudited = true;
            LoadTrigger(new Trigger { Owner = this, AfterIUD = true, Procedure = new StoredProcedure { Name = "SPCalledByAfterIudTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
        }
    }
    public class testQueueTable : Table
    {
        public testQueueTable() : base()
        {
            Name = "testQueueTable";
            Schema = new Schema { Name = "dbo" };
            LoadColumn(new Column { Name = "Id", DataType = "int", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "Abbrev", DataType = "nvarchar(50)", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "Name", DataType = "nvarchar(100)", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "NationId", DataType = "int", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "LastUpdateDtm", DataType = "datetime", IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "LastUpdater", DataType = "nvarchar(100)", IsNullable = false, Owner = this });

            PrimaryKey = new PrimaryKey { Name = "PK_testQueueTable", ColumnNames = new List<string> { "Id" }, Owner = this };
            IsAudited = true;
            LoadTrigger(new Trigger { Owner = this, AfterIUD = true, Procedure = new StoredProcedure { Name = "SPCalledByAfterIudTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
            IsQueued = true;
            Queue = new TestQueue(this);
            QueueKeyColumns = new List<IMappedColumn> { new MappedColumn { Name = "testQueueId", DataType = "int", IsNullable = false, SourceTable = this, SourceColumn = new Column { Name = "Id" } } };
            QueueJobsTable = new QueueJobsTable(this, QueueKeyColumns);
            BlackListTable = new BlackListTable(this, QueueKeyColumns);
            QueueLogTable = new QueueLogTable(this);
            QIsTable = new QIsTable(this);
        }
    }

    public class Table_1 : SequenceTable
    {
        public Table_1() : base()
        {
            Schema = new Schema { Name = "dbo" };
            Name = "Table_1";
            SequenceTableIdentityColumn = new Column { Name = "Id", DataType = "int", IsIdentity = true, IsNullable = false, Owner = this };
            SequenceTableInsertColumn = new Column { Name = "x", DataType = "int", Owner = this };
            LoadColumn(SequenceTableIdentityColumn);
            LoadColumn(SequenceTableInsertColumn);
        }
    }

}
