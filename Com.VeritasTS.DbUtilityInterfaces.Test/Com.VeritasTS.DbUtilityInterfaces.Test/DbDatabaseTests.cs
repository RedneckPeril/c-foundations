﻿using System.Linq;
using NUnit.Framework;
using System;
using Com.VeritasTS.DatabaseComm;


namespace Com.VeritasTS.DatabaseUtilities.Test
{
    [TestFixture]
    public class DbDatabaseTests
    {
        [Test]
        public void GetDatabaseTest()
        {
            Server server = new Server(new Vendor { Name = "SQLServer" });
            Database db = new Database(server) { Name = "Mazi_test", ConnectionString = @"Data Source=SERVER02\SQLEXPRESS;Initial Catalog=Mazi_test;Integrated Security=True" };
            bool b = db.OpenDbConnection();
            Assert.IsTrue(b);
            db.DisposeDbConnection();
        }

        [Test]
        public void SqlExecuteReaderTest()
        {
            Server server = new Server(new Vendor { Name = "SQLServer" });
            Database db = new Database(server) { Name = "Mazi_test", ConnectionString = @"Data Source=SERVER02\SQLEXPRESS;Initial Catalog=Mazi_test;Integrated Security=True" };
            string sql = "select * from nations";
            var ret = db.SqlExecuteReader(sql);
            System.Diagnostics.Debug.Print(ret.First()[0]);
            Assert.IsTrue(ret.Count() > 0);
            db.DisposeDbConnection();
        }

        [Test]
        public void SqlExecuteNonQueryTest()
        {
            Server server = new Server(new Vendor { Name = "SQLServer" });
            Database db = new Database(server) { Name = "Mazi_test", ConnectionString = @"Data Source=SERVER02\SQLEXPRESS;Initial Catalog=Mazi_test;Integrated Security=True" };
            string sql = "select LastUpdateDtm from nations where abbrev = 'USA'";
            var ret = db.SqlExecuteReader(sql);
            string cnt0 = ret.First()[0].ToString();
            sql = "update nations set LastUpdateDtm = getdate() where abbrev = 'USA'";
            db.SqlExecuteNonQuery(sql);
            sql = "select LastUpdateDtm from nations where abbrev = 'USA'";
            ret = db.SqlExecuteReader(sql);
            string cnt = ret.First()[0].ToString();
            db.DisposeDbConnection();
            Assert.IsTrue(!cnt.Equals(cnt0));
        }
        [Test]
        public void SqlExecuteSingleValueTest()
        {
            Server server = new Server(new Vendor { Name = "SQLServer" });
            Database db = new Database(server) { Name = "Mazi_test", ConnectionString = @"Data Source=SERVER02\SQLEXPRESS;Initial Catalog=Mazi_test;Integrated Security=True" };
            string sql = "select count(*) cnt from vtsqs.queues";
            int n = Convert.ToInt32(db.SqlExecuteSingleValue(sql));
            Assert.IsTrue(n > 0);
            sql = "select id from vtsqs.queues where name = '12345'";
            Assert.IsTrue(db.SqlExecuteSingleValue(sql) == null);
        }
    }
}
