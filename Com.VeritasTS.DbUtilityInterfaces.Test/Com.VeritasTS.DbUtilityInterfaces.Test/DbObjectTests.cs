﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Com.VeritasTS.DataTableDef;
using Com.VeritasTS.DatabaseComm;


namespace Com.VeritasTS.DatabaseUtilities.Test
{
    [TestFixture]
    public class DbObjectTests
    {
        Database _db = new Database(new Server(new Vendor { Name = "SQLServer" }))
        { Name = "Mazi_test", ConnectionString = @"Data Source=SERVER02\SQLEXPRESS;Initial Catalog=Mazi_test;Integrated Security=True" };
        [Test]
        public void SchemaExistsTest()
        {
            Schema schema = new Schema { Name = "dbo", Database = _db };
            bool b = schema.Exists();
            Assert.IsTrue(b);
            _db.DisposeDbConnection();
        }
        [Test]
        public void SchemaCreatingTest()
        {
            Schema schema = new Schema { Name = "vtsQs", Database = _db };
            schema.EnsureExistence();
            Assert.IsTrue(schema.Exists());
            _db.DisposeDbConnection();
        }
        [Test]
        public void DefaultTest()
        {
            IDefault aDefault = new Default { Name = "testDefault", Owner = new testTable { Database = _db }, ColumnWithDefault = new Column( "Name"), DefaultValue = "'test name'" };
            aDefault.EnsureExistence();
            Assert.IsTrue(aDefault.Exists());

            aDefault.Drop(SqlTranslatorSqlServer2014.GetDropDefaultExistsSql(aDefault));
            Assert.IsFalse(aDefault.Exists());
        }
    }
}
