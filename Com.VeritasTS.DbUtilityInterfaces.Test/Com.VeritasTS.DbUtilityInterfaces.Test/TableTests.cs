﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Com.VeritasTS.DataTableDef;
using Com.VeritasTS.DatabaseComm;

namespace Com.VeritasTS.DatabaseUtilities.Test
{
    [TestFixture]
    public class TableTests
    {
        Database _db = new Database(new Server(new Vendor { Name = "SQLServer" }))
        { Name = "Mazi_test", ConnectionString = @"Data Source=SERVER02\SQLEXPRESS;Initial Catalog=Mazi_test;Integrated Security=True" };

        [Test]
        public void TableCreationTest()
        {
            TableMutable table = new TableMutable { Name = "testTable", Database = _db, Schema = new Schema {Name = "dbo" } };
            table.EnsureExistence();
           
            table.LoadColumn(new Column("Id", "int", (ITable)table, isNullable: false, isIdentity: true));
            table.LoadColumn(new Column("Abbrev", "nvarchar(16)", (ITable)table, isNullable: false, isImmutable: true) );
            table.LoadColumn(new Column("Name", "varchar(64)", (ITable)table, isNullable: false) );
            table.LoadColumn(new Column("NationId", "int", (ITable)table, isNullable: false));
            table.LoadColumn(new Column("LastUpdateDtm", "datetime", (ITable)table, isNullable: false, isLastUpdateDtm: true));
            table.LoadColumn(new Column("LastUpdater", "nvarchar(100)", (ITable)table, isNullable: false, isLastUpdater: true) );
            table.LoadForeignKey(new ForeignKey("FK_testTable", new Column("NationId") , new Nations() , new Column("Id"), (ITable)table));
            table.LoadIndex(new Index( "idx_testTable1", new List<string> { "Abbrev", "NationId" }, (ITable)table));
            table.LoadIndex(new Index("idx_testTable2", new List<string> { "Name", "NationId" }, (ITable)table));

            table.MakeTable();
            Assert.IsTrue(table.Exists());

            //_primaryKey = new PrimaryKey("PK_testTable", new List<string> { "id" }, table);
            //table.PrimaryKey.EnsureExistenceInDatabase();

            Assert.IsTrue(table.PrimaryKey.Exists());
            Assert.IsTrue(table.ForeignKeys.First(i => i.Name == "FK_testTable").Exists());
            Assert.IsTrue(table.Indexes.First(i => i.Name == "idx_testTable1").Exists());
            Assert.IsTrue(table.Indexes.First(i => i.Name == "idx_testTable2").Exists());

            //table.DropTable();
            //Assert.IsFalse(table.Exists());
            _db.DisposeDbConnection();
        }

        [Test]
        public void TableCreation2Test()
        {
            testTable3 table = new testTable3() { Database = _db };
            table.EnsureExistence();
            table.MakeTable();
            Assert.IsTrue(table.Exists());

           // table.DropFromDatabaseIfExists("drop table " + table.Schema.Name + "." + table.Name);
           // Assert.IsFalse(table.Exists());
            _db.DisposeDbConnection();
        }
        [Test]
        public void GetCascadingForeignKeysTest()
        {
            Nations table = new Nations() { Database = _db };
            table.GetCascadingForeignKeys();
            Assert.IsTrue(table.CascadingForeignKeys.Count > 0);
        }

        [Test]
        public void RefreshColumnsFromDbTest()
        {
            Table table = new Table { Name = "Table_1", Database = _db, Schema = new Schema { Name = "dbo" } };
            table.RefreshColumnsFromDb();
            var columns = table.Columns;
            Assert.IsTrue(columns.First(c => c.Name == "id" && c.DataType == "int").IsIdentity == true);
            Assert.IsTrue(columns.First(c => c.Name == "x" && c.DataType == "int").IsNullable == true);
            _db.DisposeDbConnection();
        }
        [Test]
        public void AuditTableTest()
        {
            Table table = new testTable { Database = _db } ;
            table.EnsureExistence();
            table.MakeTable();
            Assert.IsTrue(table.AuditTable.Exists());
            _db.DisposeDbConnection();
        }
        [Test]
        public void QueueTablesTest()
        {
            Table table = new testQueueTable { Database = _db };
            table.EnsureExistence();
            table.MakeTable();
            Assert.IsTrue(table.Exists());
            //Assert.IsTrue(table.QIsTable.Exists());
            //Assert.IsTrue(table.BlackListTable.Exists());
            _db.DisposeDbConnection();
        }

        [Test]
        public void CreateLocationTables()
        {
            //CreateTable("Location_SequenceTable");
            //CreateTable("LocationVables");
            //CreateTable("Locations");
            //CreateTable("PhysicalLocations");
            //CreateTable("FixedLocations");
            //CreateTable("PoliticalLocations");
            CreateTable("Nations");
            //CreateTable("NationalSubdivisions");
            //CreateTable("NatGasPipelines");
            //CreateTable("NatGasFacilityPoints");
            //CreateTable("testTable2");
            //CreateTable("testTable3");
        }

        void CreateTable(string tableName)
        {
            ITable table = (ITable)CommUtils.GetObjectByObjectName(tableName);
            if (table == null)
            {
                System.Diagnostics.Debug.Print("!!!!!!!!!! can't create table " + tableName + " !!!!!!!!!!");
                return;
            }
            table.Database = _db;
            table.EnsureExistence();
            table.MakeTable();
        }
    }

    //class testTable2 : Table
    //{
    //    public testTable2() : base()
    //    {
    //        Name = "testTable2";
    //        Schema = new Schema { Name = "dbo" };
    //        AddColumnIfNotExists(new Column { Name = "Id", DataType = "int", IsNullable = false, Owner = this });
    //        AddColumnIfNotExists(new Column { Name = "Abbrev", DataType = "nvarchar(50)", IsNullable = false, Owner = this });
    //        AddColumnIfNotExists(new Column { Name = "Name", DataType = "nvarchar(100)", IsNullable = false, Owner = this });
    //        AddColumnIfNotExists(new Column { Name = "NationId", DataType = "int", IsNullable = false, Owner = this });
    //        AddColumnIfNotExists(new Column { Name = "LastUpdateDtm", DataType = "datetime", IsNullable = false, Owner = this });
    //        AddColumnIfNotExists(new Column { Name = "LastUpdater", DataType = "nvarchar(100)", IsNullable = false, Owner = this });

    //        PrimaryKey = new PrimaryKey { Name = "PK_testTable2", ColumnNames = new List<string> { "id" }, Owner = this };
    //        Table = new PoliticalLocations();
    //        PrimaryKeySequenceMap = new PrimaryKeySequenceMap
    //        {
    //            PrimaryKeyColumn = new Column { Name = "Id" },
    //            TableAsSequence = new Location_SequenceTable(),
    //        };
    //        ParentPassThrough = new ParentPassthroughInfo
    //        {
    //            ParentPassthroughColumn = new Column { Name = "LocationVableID" },
    //            ParentPassthroughSourceTable = new LocationVables(),
    //            ParentPassthroughSourceColumn = new Column { Name = "Id" },
    //            ParentPassthroughMatchingSourceColumn = new Column { Name = "Name" },
    //            ParentPassthroughLookup = "testTable2"
    //        };
    //        LoadForeignKey(new ForeignKey { Name = "FK_testTable2_PoliticalLocations", Owner = this, KeyColumn = new Column { Name = "Id" }, ReferencedTable = new PoliticalLocations(), ReferenceColumn = new Column { Name = "Id" } });
    //        LoadForeignKey(new ForeignKey { Name = "FK_testTable2_Nations", Owner = this, KeyColumn = new Column { Name = "NationId" }, ReferencedTable = new Nations(), ReferenceColumn = new Column { Name = "Id" }, CascadeOnDelete = true });
    //        LoadIndex(new Index { Name = "idx_testTable2", ColumnNames = new List<string> { "Abbrev", "NationId" }, Owner = this, IsUnique = true, IsClustered = false });
    //        LoadCheckConstraint(new CheckConstraint { Name = "ck_testTable2", Owner = this, ConstrainedColumn = "len(Abbrev) > 0 and len(Name) > 0" });
    //        //LoadTrigger(new Trigger { Owner = this, BeforeInsert = true, Procedure = new StoredProcedure { Name = "InsertLocations", Schema = new Schema { Name = "dbo" }, Owner = this } });
    //        LoadTrigger(new Trigger { Owner = this, BeforeDelete = true, Procedure = new StoredProcedure { Name = "SPCalledByDeleteTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
    //    }
    //}

    //class testTable3 : Table
    //{
    //    public testTable3() : base()
    //    {
    //        Schema = new Schema { Name = "dbo" };
    //        Name = "testTable3";
    //        AddColumnIfNotExists(new Column { Name = "Id", DataType = "int", IsNullable = false, Owner = this });
    //        AddColumnIfNotExists(new Column { Name = "testTable2Id", DataType = "int", IsNullable = false, Owner = this });
    //        PrimaryKey = new PrimaryKey { Name = "PK_testTable3", ColumnNames = new List<string> { "id" }, Owner = this };
    //        LoadForeignKey(new ForeignKey { Name = "FK_testTable3_testTable2", Owner = this, KeyColumn = new Column { Name = "testTable2Id" }, ReferencedTable = new testTable2(), ReferenceColumn = new Column { Name = "Id" }, CascadeOnDelete = true });
    //        LoadTrigger(new Trigger { Owner = this, BeforeDelete = true, Procedure = new StoredProcedure { Name = "SPCalledByDeleteTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
    //    }
    //}

}
