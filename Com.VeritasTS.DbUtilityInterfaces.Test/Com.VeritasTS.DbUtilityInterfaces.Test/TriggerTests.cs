﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Com.VeritasTS.DataTableDef;
using Com.VeritasTS.DatabaseComm;


namespace Com.VeritasTS.DatabaseUtilities.Test
{
    [TestFixture]
    public class TriggerTests
    {
        Database _db = new Database(new Server(new Vendor { Name = "SQLServer" }))
        { Name = "Mazi_test", ConnectionString = @"Data Source=SERVER02\SQLEXPRESS;Initial Catalog=Mazi_test;Integrated Security=True" };

        [Test]
        public void InsertTriggerSqlTest()
        {
            testTable2 table = new testTable2() { Database = _db };
            foreach (var trigger in table.Triggers)
            {
                string sql = trigger.InsertTriggerSql();
                System.Diagnostics.Debug.Print(sql);
                Assert.IsTrue(sql.Length > 0);
            }

            Locations loc = new Locations() { Database = _db };
            foreach (var trigger in loc.Triggers)
            {
                string sql = trigger.InsertTriggerSql();
                System.Diagnostics.Debug.Print(sql);
                Assert.IsTrue(sql.Length > 0);
            }

            //table.DropFromDatabaseIfExists("drop table " + table.Schema.Name + "." + table.Name);
            _db.DisposeDbConnection();
        }

        [Test]
        public void DeleteTriggerSqlTest()
        {
            testTable2 table = new testTable2() { Database = _db };
            var trigger = table.Triggers.FirstOrDefault(t => t.BeforeDelete);
            string sql = trigger.DeleteTriggerSql();
            System.Diagnostics.Debug.Print(sql);
            Assert.IsTrue(sql.Length > 0);
        }

        [Test]
        public void UpdateTriggerSqlTest()
        {
            testTable2 table = new testTable2() { Database = _db };
            var trigger = table.Triggers.FirstOrDefault(t => t.BeforeUpdate);
            string sql = trigger.UpdateTriggerSql();
            System.Diagnostics.Debug.Print(sql);
            Assert.IsTrue(sql.Length > 0);
        }
        [Test]
        public void AfterAnythingTriggerSqlTest()
        {
            testTable table = new testTable { Database = _db };
            var trigger = table.Triggers.FirstOrDefault(t => t.AfterAnything);
            string sql = trigger.AfterAnythingTriggerSql();
            System.Diagnostics.Debug.Print(sql);
            Assert.IsTrue(sql.Length > 0);
        }


        //[Test]
        //public void InsertTriggerSqlWithParentTest()
        //{
        //    ParentPassthroughInfo passthrough = new ParentPassthroughInfo { ParentPassthroughColumn = new Column { Name = "Id" },
        //        ParentPassthroughSourceTable = new Table { Name = "LocationVables", Schema = new Schema { Name = "dbo" } },
        //        ParentPassthroughSourceColumn = new Column { Name = "Id" },
        //        ParentPassthroughMatchingSourceColumn = new Column { Name = "Name" },
        //        ParentPassthroughLookup = "testTable" };
        //    Table table = new Table { Name = "testTable", DatabaseMutable = _db, Schema = new Schema { Name = "dbo" } , ParentPassThrough = passthrough };
        //    Trigger trigger = new Trigger { Owner = table, BeforeInsert = true, Procedure = new StoredProcedure { Name = "InsertLocations", Schema = new Schema { Name = "dbo" } } };
        //    string sql = trigger.InsertTriggerSql();
        //    System.Diagnostics.Debug.Print(sql);
        //    Assert.IsTrue(sql.Length > 0);

        //    _db.DisposeDbConnection();
        //}

        //[Test]
        //public void InsertTriggerSqlWithPrimarySequenceTest()
        //{
        //    PrimaryKeySequenceMap seq = new PrimaryKeySequenceMap
        //    {
        //        PrimaryKeyColumn = new Column { Name = "Id" },
        //        TableAsSequence = new Table { Name = "Table_1", Schema = new Schema { Name = "dbo" } },
        //        IdentityColumn = new Column { Name = "Id" },
        //        DummyInsertColumn = new Column { Name = "x" }
        //    };
        //    Table table = new Table { Name = "testTable", DatabaseMutable = _db, Schema = new Schema { Name = "dbo" }, PrimaryKeySequenceMap = seq };
        //    Trigger trigger = new Trigger { Owner = table, BeforeInsert = true, Procedure = new StoredProcedure { Name = "InsertLocations", Schema = new Schema { Name = "dbo" } } };
        //    string sql = trigger.InsertTriggerSql();
        //    System.Diagnostics.Debug.Print(sql);
        //    Assert.IsTrue(sql.Length > 0);

        //    _db.DisposeDbConnection();
        //}


        [Test]
        public void InsertTriggerEnsureExistenceTest()
        {
            testTable2 table = new testTable2 { Database = _db };
            if (!table.Exists())
            {
                table.EnsureExistence();
                table.MakeTable();
            }
            foreach (var trigger in table.Triggers)
            {
                trigger.EnsureExistence();
                Assert.IsTrue(trigger.Exists());
            }
            _db.DisposeDbConnection();
        }
        [Test]
        public void DeleteTriggerEnsureExistenceTest()
        {
            testTable2 table = new testTable2() { Database = _db };
            var trigger = table.Triggers.FirstOrDefault(t => t.BeforeDelete);
            trigger.EnsureExistence();
            Assert.IsTrue(trigger.Exists());
            _db.DisposeDbConnection();
        }
        [Test]
        public void UpdateTriggerEnsureExistenceTest()
        {
            testTable2 table = new testTable2() { Database = _db };
            var trigger = table.Triggers.FirstOrDefault(t => t.BeforeUpdate);
            trigger.EnsureExistence();
            Assert.IsTrue(trigger.Exists());
            _db.DisposeDbConnection();
        }
        [Test]
        public void AfterAnythingTriggerEnsureExistenceTest()
        {
            testTable table = new testTable() { Database = _db };
            var trigger = table.Triggers.FirstOrDefault(t => t.AfterAnything);
            trigger.EnsureExistence();
            Assert.IsTrue(trigger.Exists());
            _db.DisposeDbConnection();
        }
    }
}
