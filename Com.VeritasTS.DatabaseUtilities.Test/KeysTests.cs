﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Com.VeritasTS.DataTableDef;
using Com.VeritasTS.DatabaseComm;


namespace Com.VeritasTS.DatabaseUtilities.Test
{
    [TestFixture]
    public class KeysTests
    {
        Database _db = new Database(new Server(new Vendor { Name = "SQLServer" }))
        { Name = "Mazi_test", ConnectionString = @"Data Source=SERVER02\SQLEXPRESS;Initial Catalog=Mazi_test;Integrated Security=True" };
        Table _table = new Table { Name = "locations_0", Schema = new Schema { Name = "dbo" }};

        [Test]
        public void ForeignKeyTest()
        {
            //Table rtable = new Table { Name = "LocationVables", Schema = (ISchema)new Schema { Name = "dbo" } };
            ForeignKey key = new ForeignKey("fx_LocationVableIDTest", new Column("LocationVableID"), new LocationVables(), new Column("Id"), _table) { Database = _db };
            key.EnsureExistence();
            Assert.IsTrue( key.Exists());

            key.DropObject(SqlTranslatorSqlServer2014.GetDropKeySql((ITable)key.Owner, key.Name));
            Assert.IsFalse(key.Exists());
        }
        [Test]
        public void ForeignKeyCascadingTest()
        {
            testTable2 table = new testTable2();
            LocationVables vables = new LocationVables();
            Assert.IsTrue(vables.CascadingForeignKeys != null);
        }


        [Test]
        public void IndexTest()
        {
            Index index = new Index ("idx_locations_0Test", new List<string> { "Id", "LocationVableID" }, _table, true, false ) { Database = _db };
            index.EnsureExistence();
            Assert.IsTrue(index.Exists());

            index.DropObject(SqlTranslatorSqlServer2014.GetDropIndexSql(index));
            Assert.IsFalse(index.Exists());
        }

        [Test]
        public void UniqueKeyTest()
        {
            UniqueKey key = new UniqueKey( "uk_locations_0Test",  new List<string> { "Id", "LocationVableID" }, _table) { Database = _db };
            key.EnsureExistence();
            Assert.IsTrue(key.Exists());

            key.DropObject(SqlTranslatorSqlServer2014.GetDropKeySql((ITable)key.Owner, key.Name));
            Assert.IsFalse(key.Exists());
        }

        [Test]
        public void PrimaryKeyTest()
        {
            PrimaryKey key = new PrimaryKey("pk_locations_0Test", new List<string> { "Id" },  _table ) { Database = _db };
            key.EnsureExistence();
            Assert.IsTrue(key.Exists());

            key.DropObject(SqlTranslatorSqlServer2014.GetDropKeySql((ITable)key.Owner, key.Name));
            Assert.IsFalse(key.Exists());
        }

 
    }
}
