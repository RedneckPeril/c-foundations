﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Com.VeritasTS.DataTableDef;
using Com.VeritasTS.DatabaseComm;

namespace Com.VeritasTS.DatabaseUtilities.Test
{
    [TestFixture]
    public class ColumnTests
    {
        string _dbConnString = @"Data Source=SERVER02\SQLEXPRESS;Initial Catalog=Mazi_test;Integrated Security=True";
        [Test]
        public void ColumnExistsTest()
        {
            Database db = new Database(new Server(new Vendor { Name = "SQLServer" }))
            { Name = "Mazi_test", ConnectionString = _dbConnString };
            aTable table = new aTable { Name = "locations_0",  Schema = (ISchema)new Schema { Name = "dbo"} };
            Column column = new Column("LocationVableID", owner: table) { Database = db };
            bool b = column.Exists();
            db.DisposeDbConnection();
            Assert.IsTrue(b);
        }
        [Test]
        public void ColumnAddDropTest()
        {
            Database db = new Database(new Server(new Vendor { Name = "SQLServer" }))
            { Name = "Mazi_test", ConnectionString = _dbConnString };
            aTable table = new aTable { Name = "locations_0", Schema = (ISchema)new Schema { Name = "dbo" } };
            Column column = new Column("testColumn", "varchar(8)", table) { Database = db };
            bool b = column.Exists();
            Assert.IsFalse(b);

            column.EnsureExistence();
            b = column.Exists();
            Assert.IsTrue(b);

            column.DropObject(SqlTranslatorSqlServer2014.GetDropTableColumnSql(column));
            b = column.Exists();
            Assert.IsFalse(b);
            db.DisposeDbConnection();
        }
        [Test]
        public void CalcColumnTest()
        {
            Database db = new Database(new Server(new Vendor { Name = "SQLServer" }))
            { Name = "Mazi_test", ConnectionString = _dbConnString };
            aTable table = new aTable { Name = "locations_0", Schema = (ISchema)new Schema { Name = "dbo" } };
            Column column = new Column("testColumn", owner: table, isCalc: true, calcFormula: "Id + LocationVableID") { Database = db };
            bool b = column.Exists();
            Assert.IsFalse(b);

            column.EnsureExistence();
            b = column.Exists();
            Assert.IsTrue(b);

            var ret = db.SqlExecuteReader("select * from locations_0");
            Assert.AreEqual(Convert.ToInt32(ret.First()[0]) + Convert.ToInt32(ret.First()[1]), Convert.ToInt32(ret.First()[2]));

            column.DropObject(SqlTranslatorSqlServer2014.GetDropTableColumnSql(column));
            b = column.Exists();
            Assert.IsFalse(b);
            db.DisposeDbConnection();
        }

        [Test]
        public void CloneTest()
        {
            //IColumn column = new MappedColumn { Name = "testColumn", Owner = new testTable(), DataType = "varchar(8)", SourceColumn = new Column { Name = "testName"}, SourceTable = new testTable2() };
            IColumn column = new MappedColumn("testColumn", new Column("testName"), dataType: "varchar(8)", owner: new testTable());
            IMappedColumn aCopy = (IMappedColumn)column.GetThreadsafeClone();
            aCopy.Name = "changedName";
            aCopy.Owner = new testTable3();
            Assert.IsTrue(!aCopy.Owner.Name.Equals(column.Owner.Name) && !aCopy.Name.Equals(column.Name) );
        }

        class aTable : Table
        {
            //override public IEnumerable<IColumn> Columns
            //{
            //    get { return _columns; }
            //    set
            //    {
            //        _columns = new List<IColumn>();
            //        _columns.Add(new Column { Name = "id", DataType = "int", IsIdentity = true, IsNullable = false, OwnerTable = this });
            //        _columns.Add(new Column { Name = "LocationVableID", DataType = "int", IsIdentity = false, IsNullable = false, ColumnTable = this });
            //    }
            //}
        }
    }
}
