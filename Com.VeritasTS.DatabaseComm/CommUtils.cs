﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.DatabaseComm
{
    public static class CommUtils
    {
        public static Type GetTypeByName(string typeName)
        {
            Type type = Type.GetType(typeName);
            if (type == null)
            {
                foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
                {
                    string aName = a.FullName.Substring(0, a.FullName.IndexOf(","));
                    type = a.GetType(aName + "." + typeName);
                    if (type != null)
                        break;
                }
            }
            return type;
        }
        public static object GetObjectByObjectName(string typeName)
        {
            Type type = GetTypeByName(typeName);
            if (type != null)
                return Activator.CreateInstance(type);
            else
                return null;
        }

    }
}
