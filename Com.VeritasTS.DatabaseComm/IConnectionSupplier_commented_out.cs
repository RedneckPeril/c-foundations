﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Com.VeritasTS.CoreUtils;

namespace Com.VeritasTS.DatabaseComm
{
    /*
    public class ConnectionFactory : IConnectionFactory
    {
        public ConnectionFactory( string connectionString )
        {
            ConnectionString = connectionString;
        }

        public string ConnectionString { get; }

        public bool ConnectionStringMatches(ConnectionFactory other)
        {
            if (ConnectionString != null)
            {
                if (other == null) return false;
                else return (ConnectionString.Equals(other.ConnectionString));
            }
            else if (other == null) return true;
            else return (other.ConnectionString == null);
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            else if (!obj.GetType().Equals(this.GetType())) return false;
            else return ConnectionStringMatches((ConnectionFactory)obj);
        }

        public override int GetHashCode()
        {
            if (ConnectionString != null) return ConnectionString.GetHashCode();
            else return 0;
        }

        public IConnection GetAConnection()
        {
            IConnection retval = new SqlServerConnection(ConnectionString);
            try
            {
                retval.Open();
            }
            catch (Exception e)
            {
#if DEBUG
                Console.WriteLine("Database.OpenDbConnection:" + e.Message);
                System.Diagnostics.Debug.WriteLine("Database.OpenDbConnection:" + e.Message);
#endif
                throw;
            }
            return retval;
        }

        public ISafeMemberOfInvariantOwner GetSafeReference()
        {
            return new ConnectionFactory( ConnectionString);
        }
    }
    */
}
