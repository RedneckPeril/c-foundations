﻿using System;
using System.Collections.Generic;
using System.Data;
using Com.VeritasTS.CoreUtils;


using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.DatabaseComm
{

    /*
    public class Database : ComparableByNameVariant, IDatabaseVariant
    {

        protected IConnection connection;
        protected List<IStoredProcedureParameter> _parameters = new List<IStoredProcedureParameter>();

        public Database( string name = null, IServer server = null, IConnectionSupplier connectionSupplier = null ): base( name )
        {
            Server = server;
            ConnectionSupplier = connectionSupplier;
        }

        // IComparableByFullName methods and properties

        public string FullName
        {
            get
            {
                if (Server == null || Name == null) return null;
                else if (Server.Name == null) return null;
                else return Server.Name + "." + Name;
            }
        }
        public int CompareByFullNameTo(IComparableByFullName other)
        {
            return ComparableByFullName.CompareByFullName(this, other);
        }
        public bool EqualsInFullName(IComparableByFullName other)
        {
            return ComparableByFullName.AreEqualInFullName(this, other);
        }

        // IConnectionSupplier methods

        public IConnection GetAConnection()
        {
            if ( connection == null )
                connection = ConnectionSupplier.GetAConnection();
            return connection;
        }

        // IDatabase methods

        public IServer Server { get; set; }
        public IConnectionSupplier ConnectionSupplier { get; set; }
        public bool ServerMatches( IDatabase other )
        {
            if (Server != null)
            {
                if (other == null) return false;
                else return (Server.Equals(other.Server));
            }
            else if (other == null) return true;
            else return (other.Server == null);
        }
        public bool ConnectionSupplierMatches(IDatabase other)
        {
            if (ConnectionSupplier != null)
            {
                if (other == null) return false;
                else return (ConnectionSupplier.Equals(other.ConnectionSupplier));
            }
            else if (other == null) return true;
            else return (other.ConnectionSupplier == null);
        }
        public List<string[]> SqlExecuteReader(string sql, IConnection conn = null, ITransaction trans = null)
        {
            if ( conn == null ) conn = GetAConnection();
            if (!conn.IsOpen) conn.Open();
            using (ICommand cmd = conn.GetNewCommand(sql, trans) )
            {
                List<string[]> list = new List<string[]>();
                try
                {
                    int fieldCount = 0;
                    IDataReader r = cmd.ExecuteReader();
                    if (r.Read())
                    {
                        fieldCount = r.FieldCount;
                        do
                        {
                            string[] row = new string[fieldCount];
                            for (int i = 0; i < fieldCount; i++)
                            {
                                if (!DBNull.Value.Equals(r.GetValue(i)))
                                    row[i] = r.GetValue(i).ToString();
                                else
                                    row[i] = "";
                            }
                            list.Add(row);
                        } while (r.Read());
                    }
                    r.Close();
                    return list;
                }
                catch (Exception e)
                {
#if DEBUG
                    Console.WriteLine("Database.SqlExecuteReader:" + e.Message);
                    System.Diagnostics.Debug.WriteLine("Database.SqlExecuteReader:" + e.Message);
#endif
                    throw (e);
                }
            }
        }
        public object SqlGetSingleValue(string sql, IConnection conn = null, ITransaction trans = null)
        {
            object retval = null;
            if (conn == null) conn = GetAConnection();
            if (!conn.IsOpen) conn.Open();
            using (ICommand cmd = conn.GetNewCommand(sql, trans))
            {
                using (IDataReader r = cmd.ExecuteReader())
                {
                    if (r.Read()) retval = r.GetValue(0);
                }
            }
            return retval;
        }
        public bool SqlExecNonQuery(string sql, IConnection conn = null, ITransaction trans = null, bool byQueueRunner = false)
        {
            if (conn == null) conn = GetAConnection();
            if (!conn.IsOpen) conn.Open();
            using (ICommand cmd = conn.GetNewCommand(sql, trans))
            {
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
#if DEBUG
                    System.Diagnostics.Debug.WriteLine("Database.SqlExecNonQuery:" + e.Message);
                    Console.WriteLine("Database.SqlExecNonQuery:" + e.Message);
#endif
                    if (!byQueueRunner) throw (e);
                    else return false;
                }
                return true;
            }
        }

        public object SqlExecScalar(string sql, IConnection conn = null, ITransaction trans = null)
        {
            if (conn == null) conn = GetAConnection();
            if (!conn.IsOpen) conn.Open();
            using (ICommand cmd = conn.GetNewCommand(sql, trans))
            {
                object retval = null;
                try
                {
                    retval = cmd.ExecuteScalar();
                }
                catch (Exception e)
                {
#if DEBUG
                    Console.WriteLine("Database.SqlExecWithIdReturn:" + e.Message);
#endif
                    throw (e);
                }
                return retval;
            }
        }

        public List<GenNameValue> ExecStoredProcedure(string procedure, List<IStoredProcedureParameter> parameters = null, IConnection conn = null, ITransaction trans = null)
        {
            if (conn == null) conn = GetAConnection();
            if (!conn.IsOpen) conn.Open();
            if (parameters == null) parameters = StoredProcedureParameters;
            try
            {
                using (IStoredProcedure proc = conn.GetNewStoredProcedure(procedure, parameters, trans))
                {
                    return proc.Execute();
                }
            }
            catch (Exception e)
            {
#if DEBUG
                Console.WriteLine("Database.GetStoredProcedureCommand:" + e.Message);
#endif
                return null;
            }
        }

        // IDatabaseVariant methods and properties

        public IConnection Connection { get { return connection; } set { connection = value; } }

        public List<IStoredProcedureParameter> StoredProcedureParameters
        {
            get
            {
                List<IStoredProcedureParameter> retval = new List<IStoredProcedureParameter>();
                foreach (IStoredProcedureParameter parameter in _parameters)
                    retval.Add((IStoredProcedureParameter)parameter.GetSafeReference());
                return retval;
            }
        }

        public List<IStoredProcedureParameter> StoredProcedureOutputParameters
        {
            get
            {
                List<IStoredProcedureParameter> retval = new List<IStoredProcedureParameter>();
                foreach (IStoredProcedureParameter parameter in _parameters.Where( p => p.IsOutput ) )
                    retval.Add((IStoredProcedureParameter)parameter.GetSafeReference());
                return retval;
            }
        }

        public void ClearStoredProcedureParameters() { _parameters.Clear(); }

        public void AddStoredProcedureParameter( IStoredProcedureParameter parameter, bool refresh = false)
        {
            if (refresh) ClearStoredProcedureParameters();
            _parameters.Add(parameter);
        }

        public ITransaction BeginTransaction()
        {
            IConnection conn = GetAConnection();
            if (!conn.IsOpen) conn.Open();
            return conn.BeginTransaction();
        }
        
        public void DisposeDbConnection()
        {
            try
            {
                if (connection != null)
                {
                    connection.Dispose();
                    connection = null;
                }
            }
            catch (Exception e)
            {
#if DEBUG
                Console.WriteLine("Database.DisposeDbConnection:" + e.Message);
#endif
                throw (e);
            }
        }

        // Overriding of Equals and GetHashTag

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            else if (!obj.GetType().Equals(this.GetType())) return false;
            else 
            {
                Database other = (Database)obj;
                if (!EqualsInName(other)) return false;
                else if (!ServerMatches(other)) return false;
                else return ConnectionSupplierMatches(other);
            }
        }

        public override int GetHashCode()
        {
            if (Name != null) return Name.GetHashCode();
            else return 0;
        }

    }

    public class DatabaseSafe : ComparableByNameVariant, IDatabaseSafe
    {

        private Database db;

        public DatabaseSafe(string name = null, IServer server = null, IConnectionFactory connectionSupplier = null) : base( name )
        {
            db = new Database(name, (IServer)server.GetSafeReference(), (IConnectionSupplier)connectionSupplier.GetSafeReference() );
        }

        // IComparableByName methods and properties

        public string FullName { get { return db.FullName; } }

        public int CompareByFullNameTo(IComparableByFullName other)
        {
            return ComparableByFullName.CompareByFullName(this, other);
        }

        public bool EqualsInFullName(IComparableByFullName other)
        {
            return ComparableByFullName.AreEqualInFullName(this, other);
        }

        // IConnectionSupplier methods

        public IConnection GetAConnection() { return db.ConnectionSupplier.GetAConnection(); }

        // IDatabase methods

        public IServer Server { get { return db.Server; } }
        public IConnectionSupplier ConnectionSupplier { get { return db.ConnectionSupplier; } }
        public bool ServerMatches(IDatabase other) { return db.ServerMatches(other); }
        public bool ConnectionSupplierMatches(IDatabase other) { return db.ConnectionSupplierMatches(other); }
        public List<string[]> SqlExecuteReader(string sql, IConnection conn = null, ITransaction trans = null)
        {
            if (conn == null) conn = GetAConnection();
            return db.SqlExecuteReader(sql, conn, trans);
        }
        public object SqlGetSingleValue(string sql, IConnection conn = null, ITransaction trans = null)
        {
            if (conn == null) conn = GetAConnection();
            return db.SqlGetSingleValue(sql, conn, trans);
        }
        public bool SqlExecNonQuery(string sql, IConnection conn = null, ITransaction trans = null, bool byQueueRunner = false)
        {
            if (conn == null) conn = GetAConnection();
            return db.SqlExecNonQuery(sql, conn, trans, byQueueRunner);
        }

        public object SqlExecScalar(string sql, IConnection conn = null, ITransaction trans = null)
        {
            if (conn == null) conn = GetAConnection();
            return db.SqlExecScalar(sql, conn, trans);
        }

        public List<GenNameValue> ExecStoredProcedure(string procedure, List<IStoredProcedureParameter> parameters = null, IConnection conn = null, ITransaction trans = null)
        {
            if (conn == null) conn = GetAConnection();
            return db.ExecStoredProcedure(procedure, parameters, conn, trans);
        }

        // ISafeMemberOfInvariantOwner methods
        public ISafeMemberOfInvariantOwner GetSafeReference()
        {
            return new DatabaseSafe(Name, Server, (IConnectionFactory)ConnectionSupplier);
        }

        // Overriding of Equals and GetHashCode

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            else if (!obj.GetType().Equals(this.GetType())) return false;
            else
            {
                Database other = (Database)obj;
                if (!EqualsInName(other)) return false;
                else if (!ServerMatches(other)) return false;
                else return ConnectionSupplierMatches(other);
            }
        }

        public override int GetHashCode()
        {
            if (Name != null) return Name.GetHashCode();
            else return 0;
        }

    }
    */

}
