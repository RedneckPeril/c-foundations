﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.VeritasTS.CoreUtils;

namespace Com.VeritasTS.DatabaseComm
{

    /*
    public class SchemaVariant : DatabaseObjectVariant, ISchema
    {

        public SchemaVariant(
            string name
            , IDatabaseSafe database
            , int? id
            ) : base(
                name
                , database
                , owner: null
                , schema: null
                , objectType: ObjectTypes.Schema()
                , physicalObjectType: ObjectTypes.Schema()
                , id: id
                )
        {
            SetSchema(this);
        }

        public override string ExistsSql()
        {
            return "select top(1) 1 cnt from sys.schemas where name = '" + Name + "'";
        }

        public override void EnsureExistence()
        {
            if (!Exists())
            {
                string sql = "create schema " + Name;
                Database.SqlExecNonQuery(sql);
            }
        }

        public override void EnsureExistenceAsRequired()
        {
            EnsureExistence();
        }

        override public string DropSql()
        {
            return "drop schema " + Name;
        }

        public override ISafeMemberOfInvariantOwner GetSafeReference()
        {
            return new SchemaVariant(Name, (IDatabaseSafe)Database.GetSafeReference(), ID);
        }

    }

    public class SchemaInvariant : ISchema
    {

        private SchemaVariant _schema;

        public SchemaInvariant(
            string name
            , IDatabaseSafe database
            , int? id
            )
        {
            _schema = new SchemaVariant(name, database, id);
        }

        // IComparableByName methods and properties

        public string Name { get { return _schema.Name; } }

        public int CompareByNameTo(IComparableByName other)
        {
            return ComparableByNameVariant.CompareByName(this, other);
        }
        public bool EqualsInName(IComparableByName other)
        {
            return ComparableByNameVariant.AreEqualInName(this, other);
        }

        // IComparableByFullName methods and properties

        public string FullName { get { return _schema.FullName; } }

        public int CompareByFullNameTo(IComparableByFullName other)
        {
            return ComparableByFullName.CompareByFullName(this, other);
        }
        public bool EqualsInFullName(IComparableByFullName other)
        {
            return ComparableByFullName.AreEqualInFullName(this, other);
        }

        // ISafeMemberOfInvariantOwner methods
        public ISafeMemberOfInvariantOwner GetSafeReference()
        {
            return new SchemaInvariant(Name, Database, ID);
        }

        // IDataProcObject methods and properties
        public IDatabaseSafe Database
        {
            get { return _schema.Database; }
        }
        public ISchema Schema
        {
            get { return this; }
        }
        public void EnsureExistence() { _schema.EnsureExistence(); }
        public void EnsureExistenceAsRequired() { _schema.EnsureExistenceAsRequired(); }
        public bool Exists() { return _schema.Exists(); }

        // IDatabaseObject methods and properties
        public IDataProcObject Owner {  get { return null; } }
        public IObjectType ObjectType { get { return _schema.ObjectType; } }
        public IObjectType PhysicalObjectType { get { return _schema.PhysicalObjectType; } }
        public int? ID { get { return _schema.ID; } }
        public string ExistsSql() { return _schema.ExistsSql(); }
        public void Drop() { _schema.Drop(); }
        public string DropSql() { return _schema.DropSql(); }

    }
    */
}
