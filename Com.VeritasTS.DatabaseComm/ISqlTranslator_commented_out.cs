﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.DatabaseComm
{
    /*
    interface ISqlTranslator
    {
        string GetTableExistsSql(ITable table);
        string GetColumnExistsSql(IColumn column);
        string GetTriggerExistsSql(ITrigger trigger);
        string GetKeyExistsSql(ITable table, string keyName, bool isUnique);
        string GetIndexExistsSql(IIndex index);
        string GetProcedureExistsSql(IStoredProcedure proc);
        string GetTableColumnsSql(ITable table);
        string GetTableIdentityColumnSql(ITable table);
        string GetTableComputedColumnsSql(ITable table);
        string GetAddTableColumnSql(IColumn column);
        string GetDropTableColumnSql(IColumn column);
        string GetAddUniqueKeySql(IUniqueKey key);
        string GetDropKeySql(ITable table, string keyName);
        string GetTableForeignKeyDependentsSql(ITable table);
        string GetAddForeignKeySql(IForeignKey key);
        string GetAddPrimaryKeySql(IPrimaryKey key);
        string GetAddIndexSql(IIndex index);
        string GetDropIndexSql(IIndex index);
        string GetCheckConstraintExistsSql(ICheckConstraint constraint);
        string GetAddCheckConstraintSql(ICheckConstraint constraint);
        string GetDropCheckConstraintSql(ICheckConstraint constraint);
        string GetDefaultExistsSql(IDefault theDefault);
        string GetAddDefaultExistsSql(IDefault theDefault);
        string GetDropDefaultExistsSql(IDefault theDefault);
        string GetAddIntoQueuesSql(string columnList, string inputList, string schema);
        string GetFalseSQL();
        string GetTrueSQL();
        string GetBooleanType();
    }
    */
}
