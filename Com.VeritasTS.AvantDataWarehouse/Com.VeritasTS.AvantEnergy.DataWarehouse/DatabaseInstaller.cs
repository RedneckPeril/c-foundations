﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.VeritasTS.DataTableDef;
using Com.VeritasTS.DatabaseUtilityInterfaces;
using Com.VeritasTS.DbUtilitiesSqlServer;

namespace Com.VeritasTS.AvantEnergy.DataWarehouse
{
    public class DatabaseInstaller
    {
        private IDatabaseSafe db;
        private ISqlTranslator sqlTranslator;
        private SchemaMutable dbo;
        const char LF = (char)10;

        public DatabaseInstaller()

        {
            sqlTranslator = new SqlTranslatorSqlServer2014();
            setDB();
            dbo = new SchemaMutable("dbo", sqlTranslator, db, null, true, true);
        }

        public void Install()
        {
            String sql;
            String name;
            
            MeasurablePropertiesTable measurableProperties 
                = new MeasurablePropertiesTable(
                    sqlTranslator
                    , dbo
                    , null
                    , true
                    , true
                    );
            measurableProperties.EnsureExistenceInDatabaseAsDesigned();
            string measureablePropertiesName = measurableProperties.Name;
            name = measureablePropertiesName;
            sql =
                  "insert into " + name + "( abbrev, name )" + LF
                + "select abbrev, name " + LF
                + "  from ( " + LF
                + "             select 'length'      abbrev, 'length'                    name " + LF
                + "       union select 'mass'        abbrev, 'mass'                      name " + LF
                + "       union select 'time'        abbrev, 'time'                      name " + LF
                + "       union select 'current'     abbrev, 'electric current'          name " + LF
                + "       union select 'temperature' abbrev, 'thermodynamic temperature' name " + LF
                + "       union select 'substance'   abbrev, 'amount of substance'       name " + LF
                + "       union select 'luminosity'  abbrev, 'luminous intensity'        name " + LF
                + "       ) required " + LF
                + " where not exists ( " + LF
                + "                  select top(1) 1 " + LF
                + "                    from " + name + " alreadyThere " + LF
                + "                   where alreadyThere.abbrev = required.abbrev " + LF
                + "                  ) " + LF
                + ";";
            db.SqlExecuteNonQuery(sql);
            sql =
                  "insert into " + name + "( abbrev, name, numeratorID, denominatorID )" + LF
                + "select abbrev, name, numeratorID, denominatorID " + LF
                + "  from ( " + LF
                + "       select 'speed'        abbrev " + LF
                + "            , 'speed'        name " + LF
                + "            , numerator.id   numeratorID " + LF
                + "            , denominator.id denominatorID " + LF
                + "         from " + name + " numerator " + LF
                + "                join " + name + " denominator " + LF
                + "                  on denominator.abbrev = 'time' " + LF
                + "        where numerator.abbrev = 'length' " + LF
                + "       ) required " + LF
                + " where not exists ( " + LF
                + "                  select top(1) 1 " + LF
                + "                    from " + name + " alreadyThere " + LF
                + "                   where alreadyThere.abbrev = required.abbrev " + LF
                + "                  ) " + LF
                + ";";
            db.SqlExecuteNonQuery(sql);

            UOMsTable uoms
                = new UOMsTable(
                    sqlTranslator
                    , dbo
                    , measurableProperties
                    , null
                    , true
                    , true
                    );
            uoms.EnsureExistenceInDatabaseAsDesigned();
            name = uoms.Name;
            sql =
                  "insert into " + name + "( abbrev, name, propertyID )" + LF
                + "select abbrev, name, propertyID " + LF
                + "  from ( " + LF
                + "       select 'nmi'           abbrev " + LF
                + "            , 'nautical mile' name " + LF
                + "            , property.id     propertyID " + LF
                + "         from " + measureablePropertiesName + " property " + LF
                + "        where property.abbrev = 'length' " + LF
                + "       union " + LF 
                + "       select 'h'             abbrev " + LF
                + "            , 'hour'          name " + LF
                + "            , property.id     propertyID " + LF
                + "         from " + measureablePropertiesName + " property " + LF
                + "        where property.abbrev = 'time' " + LF
                + "       ) required " + LF
                + " where not exists ( " + LF
                + "                  select top(1) 1 " + LF
                + "                    from " + name + " alreadyThere " + LF
                + "                   where alreadyThere.abbrev = required.abbrev " + LF
                + "                  ) " + LF
                + ";";
            db.SqlExecuteNonQuery(sql);
            sql =
                  "insert into " + name + "( abbrev, name, propertyID, numeratorID, denominatorID )" + LF
                + "select abbrev, name, propertyID, numeratorID, denominatorID " + LF
                + "  from ( " + LF
                + "       select 'kn'         abbrev " + LF
                + "            , 'knot'       name " + LF
                + "            , property.id  propertyID " + LF
                + "            , numerator.id numeratorID " + LF
                + "            , denominator.id denominatorID " + LF
                + "         from " + name + " numerator " + LF
                + "                join " + name + " denominator " + LF
                + "                  on denominator.abbrev = 'time' " + LF
                + "                join " + measureablePropertiesName + " property " + LF
                + "                  on property.abbrev = 'speed' " + LF
                + "        where numerator.abbrev = 'length' " + LF
                + "       ) required " + LF
                + " where not exists ( " + LF
                + "                  select top(1) 1 " + LF
                + "                    from " + name + " alreadyThere " + LF
                + "                   where alreadyThere.abbrev = required.abbrev " + LF
                + "                  ) " + LF
                + ";";
            db.SqlExecuteNonQuery(sql);
        }

        private void setDB()
        {
            if (db == null)
                db = new DatabaseMutable(
                        "Mazi_test"
                        , new ServerImmutable(
                            "SERVER02"
                            , Vendors.SQLServer()
                            )
                        , new ConnectionFactorSqlServer(@"Data Source=SERVER02\SQLEXPRESS;Initial Catalog=Mazi_test;Integrated Security=True")
                        , sqlTranslator
                        );
        }

    }

}