﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.VeritasTS.DatabaseUtilityInterfaces;

namespace Com.VeritasTS.AvantEnergy.DataWarehouse
{
    public class UOMsTable: TableMutable
    {
        public UOMsTable(
            ISqlTranslator sqlTranslator = null
            , ISchema schema = null
            , MeasurablePropertiesTable measurablePropertiesTable = null
            , int? id = default(int?)
            , bool? retrieveIDsFromDatabase = false
            , bool assert = true
            ) : base(
                "UOMs"
                , sqlTranslator
                , schema
                , ObjectTypes.Table()
                , owner: null
                , isAudited: true
                , publishesChanges: true
                , id: id
                , retrieveIDsFromDatabase: false
                , assert: false
                )
        {
            ISqlTranslator sql = SqlTranslator;
            if (measurablePropertiesTable == null)
                measurablePropertiesTable
                    = new MeasurablePropertiesTable(
                        sqlTranslator
                        , schema
                        , null
                        , retrieveIDsFromDatabase
                        , assert: true
                        );
            SetHasIDColumn(
                true
                , retrieveIDColumnIDFromDatabase: false
                , assert: false
                );
            string abbrevCkName = sql.TableConstraintName("_ckA", Name);
            AddOrOverwriteColumn(
                new ColumnMutable(
                    "abbrev"
                    , "nvarchar(50)"
                    , SqlTranslator
                    , this
                    , isIdentity: true
                    , isNullable: false
                    , isToBeTrimmed: true
                    , constraintTag: "A"
                    , checkConstraints: new Dictionary<string, ICheckConstraint>
                    {
                        [abbrevCkName]
                        = new CheckConstraintMutable(
                            abbrevCkName
                            , SqlTranslator
                            , this
                            , sql.TrimmingConstraintText("abbrev")
                            , retrieveIDsFromDatabase: false
                            , assert: false
                            )
                    }
                    )
                , retrieveColumnIDFromDatabase: false
                , assert: false
                );
            string nameCkName = sql.TableConstraintName("_ckN", Name);
            AddOrOverwriteColumn(
                new ColumnMutable(
                    "name"
                    , "nvarchar(100)"
                    , SqlTranslator
                    , this
                    , isNullable: false
                    , isToBeTrimmed: true
                    , constraintTag: "N"
                    , checkConstraints: new Dictionary<string, ICheckConstraint>
                    {
                        [nameCkName]
                        = new CheckConstraintMutable(
                            nameCkName
                            , SqlTranslator
                            , this
                            , sql.TrimmingConstraintText("name")
                            , retrieveIDsFromDatabase: false
                            , assert: false
                            )
                    }
                    )
                , retrieveColumnIDFromDatabase: false
                , assert: false
                );
            AddOrOverwriteColumn(
                new ColumnMutable(
                    "propertyID"
                    , "int"
                    , SqlTranslator
                    , this
                    , isNullable: false
                    , constraintTag: "P"
                    )
                , retrieveColumnIDFromDatabase: false
                , assert: false
                );
            AddOrOverwriteColumn(
                new ColumnMutable(
                    "numeratorID"
                    , "int"
                    , SqlTranslator
                    , this
                    , constraintTag: "Num"
                    )
                , retrieveColumnIDFromDatabase: false
                , assert: false
                );
            AddOrOverwriteColumn(
                new ColumnMutable(
                    "denominatorID"
                    , "int"
                    , SqlTranslator
                    , this
                    , constraintTag: "D"
                    )
                , retrieveColumnIDFromDatabase: false
                , assert: false
                );
            SetHasLastUpdateDtmColumn(true, false, false);
            SetHasLastUpdaterColumn(true, false, false);
            SetPrimaryKey(
                new PrimaryKeyMutable(
                    "UOMs_pk"
                    , sql
                    , this
                    , new List<string> { IDColumnName }
                    , retrieveIDsFromDatabase: false
                    , assert: false
                    )
                , retrievePrimaryKeyID: false
                , assert: false
                );
            AddOrOverwriteUniqueKey(
                new UniqueKeyMutable(
                    "UOMs_lpk"
                    , sql
                    , this
                    , new List<string> { "abbrev" }
                    , retrieveIDsFromDatabase: false
                    , assert: false
                    )
                , retrieveUniqueKeyIDFromDatabase: false
                , assert: false
                );
            AddOrOverwriteUniqueKey(
                new UniqueKeyMutable(
                    "UOMs_lpkN"
                    , sql
                    , this
                    , new List<string> { "name" }
                    , retrieveIDsFromDatabase: false
                    , assert: false
                    )
                , retrieveUniqueKeyIDFromDatabase: false
                , assert: false
                );
            AddOrOverwriteForeignKey(
                new ForeignKeyMutable(
                    "UOMs_fkP"
                    , sql
                    , this
                    , new Dictionary<string, string>
                    {
                        ["propertyID"] = IDColumnName
                    }
                    , referenceTable: measurablePropertiesTable
                    , cascadeOnDelete: false
                    , cascadeOnUpdate: false
                    , constrainedColumn: Columns["propertyID"].Item2
                    , retrieveIDsFromDatabase: false
                    , assert: false
                    )
                , retrieveForeignKeyIDFromDatabase: false
                , assert: false
                );
            AddOrOverwriteForeignKey(
                new ForeignKeyMutable(
                    "UOMs_fkNum"
                    , sql
                    , this
                    , new Dictionary<string, string>
                    {
                        ["numeratorID"] = IDColumnName
                    }
                    , referenceTable: this
                    , cascadeOnDelete: false
                    , cascadeOnUpdate: false
                    , constrainedColumn: Columns["numeratorID"].Item2
                    , retrieveIDsFromDatabase: false
                    , assert: false
                    )
                , retrieveForeignKeyIDFromDatabase: false
                , assert: false
                );
            AddOrOverwriteForeignKey(
                new ForeignKeyMutable(
                    "UOMs_fkD"
                    , sql
                    , this
                    , new Dictionary<string, string>
                    {
                        ["denominatorID"] = IDColumnName
                    }
                    , referenceTable: this
                    , cascadeOnDelete: false
                    , cascadeOnUpdate: false
                    , constrainedColumn: Columns["denominatorID"].Item2
                    , retrieveIDsFromDatabase: false
                    , assert: false
                    )
                , retrieveForeignKeyIDFromDatabase: false
                , assert: false
                );
            AddOrOverwriteTrigger(
                new TriggerMutable(
                    null
                    , sql
                    , this
                    , TriggerTimings.BeforeInsert
                    , retrieveIDsFromDatabase: false
                    , assert: false
                    )
                , retrieveTriggerIDFromDatabase: false
                , assert: false
                );
            AddOrOverwriteTrigger(
                new TriggerMutable(
                    null
                    , sql
                    , this
                    , TriggerTimings.BeforeDelete
                    , retrieveIDsFromDatabase: false
                    , assert: false
                    )
                , retrieveTriggerIDFromDatabase: false
                , assert: false
                );
            AddOrOverwriteTrigger(
                new TriggerMutable(
                    null
                    , sql
                    , this
                    , TriggerTimings.BeforeUpdate
                    , retrieveIDsFromDatabase: false
                    , assert: false
                    )
                , retrieveTriggerIDFromDatabase: false
                , assert: false
                );
            AddOrOverwriteTrigger(
                new TriggerMutable(
                    null
                    , sql
                    , this
                    , TriggerTimings.AfterAnything
                    , retrieveIDsFromDatabase: false
                    , assert: false
                    )
                , retrieveTriggerIDFromDatabase: false
                , assert: false
                );
        }
    }
}
