﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Com.VeritasTS.DatabaseUtilities;

namespace Com.VeritasTS.AvantDataWarehouse
{
    public class MeasureableCharateristicsTable : Table
    {
        public MeasureableCharateristicsTable() : base()
        {
            Schema = new Schema { Name = "dbo" };
            Name = "LocationVables";
            LoadColumn(new Column { Name = "Id", DataType = "int", IsIdentity = true, IsNullable = false, Owner = this });
            LoadColumn(new Column { Name = "Name", DataType = "nvarchar(100)", IsNullable = false, Owner = this });
            PrimaryKey = new PrimaryKey { Name = "PK_LocationVables", ColumnNames = new List<string> { "Id" }, Owner = this };
            LoadIndex(new Index { Name = "idx_LocationVables", ColumnNames = new List<string> { "Name" }, Owner = this, IsUnique = true, IsClustered = false });
        }
    }

}
