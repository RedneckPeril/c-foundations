﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.VeritasTS.DatabaseUtilities;
using Com.VeritasTS.DatabaseComm;
//using Com.VeritasTS.Queues;

namespace Com.VeritasTS.DataTableDef
{
    public class testTable2 : Table
    {
        public testTable2() : base()
        {
            Name = "testTable2";
            Schema = new Schema { Name = "dbo" };
            _tableMutable.LoadColumn(new Column("Id", "int", this, isNullable: false, isImmutable: true) );
            _tableMutable.LoadColumn(new Column("Abbrev", "nvarchar(50)", this, isNullable: false, isImmutable: true) );
            _tableMutable.LoadColumn(new Column("Name", "nvarchar(100)", this, isNullable: false) );
            _tableMutable.LoadColumn(new Column("NationId", "int", this, isNullable: false));
            _tableMutable.LoadColumn(new Column("LastUpdateDtm", "datetime", this, isNullable: false, isLastUpdateDtm: true) );
            _tableMutable.LoadColumn(new Column("LastUpdater", "nvarchar(100)", this, isNullable: false, isLastUpdater: true));

            _tableMutable.PrimaryKey = new PrimaryKey("PK_testTable2", new List<string> { "id" }, this);
            _tableMutable.CheckConstraintsIU = true;
            _tableMutable.ParentTable = new PoliticalLocations();
            _tableMutable.PrimaryKeySequence = new PrimaryKeySequence
            {
                PrimaryKeyColumn = new Column("Id"),
                SequenceTable = new Location_SequenceTable(),
            };
            _tableMutable.ParentPassthrough = new ParentPassthroughInfo
            {
                ParentPassthroughColumn = new Column("LocationVableID", "int") ,
                ParentPassthroughSourceTable = new LocationVables(),
                ParentPassthroughSourceColumn = new Column("Id", "int"),
                ParentPassthroughMatchingSourceColumn = new Column("Name", "nvarchar(100)") ,
                ParentPassthroughLookup = "testTable2"
            };
            _tableMutable.LoadForeignKey(new ForeignKey("FK_testTable2_PoliticalLocations", new Column("Id") , new PoliticalLocations(), new Column("Id"), this));
            _tableMutable.LoadForeignKey(new ForeignKey("FK_testTable2_Nations", new Column("NationId"), new Nations(), new Column("Id") , this, true ));
            _tableMutable.LoadIndex(new Index("idx_testTable2", new List<string> { "Abbrev", "NationId" }, this, true, false ));
            _tableMutable.LoadCheckConstraint(new CheckConstraint { Name = "ck_testTable2", Owner = this, ConstraintText = "len(Abbrev) > 0 and len(Name) > 0" });
            _tableMutable.LoadTrigger(new Trigger { Owner = this, BeforeInsert = true, Procedure = new StoredProcedureForTriggers { Name = "SPCalledByInsertTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
            _tableMutable.LoadTrigger(new Trigger { Owner = this, BeforeDelete = true, Procedure = new StoredProcedureForTriggers { Name = "SPCalledByDeleteTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
            _tableMutable.LoadTrigger(new Trigger { Owner = this, BeforeUpdate = true, Procedure = new StoredProcedureForTriggers { Name = "SPCalledByUpdateTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
        }
    }

    public class testTable3 : Table
    {
        public testTable3() : base()
        {
            Schema = new Schema { Name = "dbo" };
            Name = "testTable3";
            _tableMutable.LoadColumn(new Column("Id", "int", isNullable: false, owner: this));
            _tableMutable.LoadColumn(new Column("testTable2Id", "int", isNullable: false, owner: this));
            _tableMutable.PrimaryKey = new PrimaryKey("PK_testTable3", new List<string> { "id" }, this );
            _tableMutable.LoadForeignKey(new ForeignKey("FK_testTable3_testTable2", new Column("testTable2Id") , new testTable2(), new Column("Id") , this , true ));
            _tableMutable.LoadTrigger(new Trigger { Owner = this, BeforeDelete = true, Procedure = new StoredProcedureForTriggers { Name = "SPCalledByDeleteTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
        }
    }
    public class testTable : Table
    {
        public testTable() : base()
        {
            Name = "testTable";
            Schema = new Schema { Name = "dbo" };
            _tableMutable.LoadColumn(new Column("Id", "int", isNullable: false, owner: this));
            _tableMutable.LoadColumn(new Column("Abbrev", "nvarchar(50)", isNullable: false, owner: this) );
            _tableMutable.LoadColumn(new Column("Name", "nvarchar(100)", isNullable: false, owner: this));
            _tableMutable.LoadColumn(new Column("NationId", "int", isNullable: false, owner: this));
            _tableMutable.LoadColumn(new Column("LastUpdateDtm", "datetime", isNullable: false, owner: this, isLastUpdateDtm: true));
            _tableMutable.LoadColumn(new Column("LastUpdater", "nvarchar(100)", isNullable: false, owner: this, isLastUpdater: true));

            _tableMutable.PrimaryKey = new PrimaryKey("PK_testTable",  new List<string> { "id" }, this );
            _tableMutable.LoadForeignKey(new ForeignKey("FK_testTable", new Column("NationId") , new Nations(), new Column("Id"), this));
            _tableMutable.IsAudited = true;
            _tableMutable.LoadTrigger(new Trigger { Owner = this, AfterAnything = true, Procedure = new StoredProcedureForTriggers { Name = "SPCalledByAfterIudTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
        }
    }
    public class testQueueTable : Table
    {
        public testQueueTable() : base()
        {
            Name = "testQueueTable";
            Schema = new Schema { Name = "dbo" };
            _tableMutable.LoadColumn(new Column("Id", "int", isNullable: false, owner: this));
            _tableMutable.LoadColumn(new Column("Abbrev", "nvarchar(50)", isNullable: false, owner: this) );
            _tableMutable.LoadColumn(new Column("Name", "nvarchar(100)", isNullable: false, owner: this));
            _tableMutable.LoadColumn(new Column("NationId", "int", isNullable: false, owner: this) );
            _tableMutable.LoadColumn(new Column("LastUpdateDtm", "datetime", isNullable: false, owner: this, isLastUpdateDtm: true) );
            _tableMutable.LoadColumn(new Column("LastUpdater", "nvarchar(100)", isNullable: false, owner: this, isLastUpdater: true) );

            _tableMutable.PrimaryKey = new PrimaryKey("PK_testQueueTable", new List<string> { "Id" }, this);
            _tableMutable.IsAudited = true;
            _tableMutable.LoadTrigger(new Trigger { Owner = this, AfterAnything = true, Procedure = new StoredProcedureForTriggers { Name = "SPCalledByAfterIudTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
            _tableMutable.IsQueued = true;
            _tableMutable.QueueKeyColumns = new List<IMappedColumn>() { new MappedColumn("testQueueId", new Column("Id"), "int", isNullable: false )  };
            //Queue = new testQueueTableQ { TriggerTable = this};
        }
    }

    public class Table_1 : SequenceTable
    {
        public Table_1() : base()
        {
            Schema = new Schema { Name = "dbo" };
            Name = "Table_1";
            SequenceTableIdentityColumn = new Column("Id", "int", isNullable: false, owner: this, isIdentity: true) ;
            SequenceTableInsertColumn = new Column("x", "int", owner: this) ;
            _tableMutable.LoadColumn(SequenceTableIdentityColumn);
            _tableMutable.LoadColumn(SequenceTableInsertColumn);
        }
    }

}
