﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.VeritasTS.DatabaseUtilityInterfaces;

namespace Com.VeritasTS.DataTableDef
{
    public class LocationVables : TableImmutable
    {
        public LocationVables( IDatabase db ) : base()
        {
            Schema = new Schema { Name = "dbo" };
            Name = "LocationVables";
            _tableMutable.LoadColumn(new Column("Id", "int", this, isNullable: false, isIdentity: true) );
            _tableMutable.LoadColumn(new Column("Name", "nvarchar(100)", this, isNullable: false));
            _tableMutable.PrimaryKey = new PrimaryKey( "PK_LocationVables", new List<string> { "Id" },  this );
            _tableMutable.LoadIndex( new Index( "idx_LocationVables", new List<string> { "Name" }, this,  true, false ));
        }
    }

    public class Locations : Table
    {
        public Locations() : base()
        {
            Schema = new Schema { Name = "dbo" };
            Name = "Locations";
            _tableMutable.LoadColumn(new Column("Id", "int", this, isNullable: false, isImmutable: true));
            _tableMutable.LoadColumn(new Column("LocationVableID", "int", this, isNullable: false));
            _tableMutable.PrimaryKey = new PrimaryKey("PK_Locations", new List<string> { "id" }, this);
            _tableMutable.LoadForeignKey(new ForeignKey( "FK_Locations", new Column("LocationVableID"), new LocationVables(), new Column("Id"), this));
            _tableMutable.LoadTrigger(new Trigger { Owner = this, BeforeInsert = true, Procedure = new StoredProcedureForTriggers { Name = "SPCalledByInsertTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
        }
    }

    public class PhysicalLocations : Table
    {
        public PhysicalLocations() : base()
        {
            Schema = new Schema { Name = "dbo" };
            Name = "PhysicalLocations";
            _tableMutable.LoadColumn(new Column("Id", "int", this, isNullable: false, isImmutable: true) );
            _tableMutable.LoadColumn(new Column("LocationVableID", "int", this, isNullable: false));
            _tableMutable.PrimaryKey = new PrimaryKey("PK_PhysicalLocations",  new List<string> { "id" }, this );
            _tableMutable.LoadForeignKey(new ForeignKey("FK_PhysicalLocations_LocationVables",  new Column("LocationVableID") , new LocationVables(),  new Column("Id"), this ));
            _tableMutable.LoadForeignKey(new ForeignKey("FK_PhysicalLocations_Locations", new Column("Id") , new Locations(),  new Column("Id"), this));
            _tableMutable.ParentTable = new Locations();
            _tableMutable.LoadTrigger(new Trigger { Owner = this, BeforeInsert = true, Procedure = new StoredProcedureForTriggers { Name = "SPCalledByInsertTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
        }
    }

    public class FixedLocations : Table
    {
        public FixedLocations() : base()
        {
            Schema = new Schema { Name = "dbo" };
            Name = "FixedLocations";
            _tableMutable.LoadColumn(new Column("Id", "int", this, isNullable: false, isImmutable: true) );
            _tableMutable.LoadColumn(new Column("LocationVableID", "int", this, isNullable: false) );
            _tableMutable.PrimaryKey = new PrimaryKey("PK_FixedLocations",  new List<string> { "id" },  this );
            _tableMutable.LoadForeignKey(new ForeignKey("FK_FixedLocations_LocationVables", new Column("LocationVableID") , new LocationVables(), new Column("Id"), this));
            _tableMutable.LoadForeignKey(new ForeignKey("FK_FixedLocations_PhysicalLocations", new Column("Id") , new PhysicalLocations(), new Column("Id"), this ));
            _tableMutable.ParentTable = new PhysicalLocations();
            _tableMutable.LoadTrigger(new Trigger { Owner = this, BeforeInsert = true, Procedure = new StoredProcedureForTriggers { Name = "SPCalledByInsertTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
        }
    }


    public class PoliticalLocations : Table
    {
        public PoliticalLocations() : base()
        {
            Schema = new Schema { Name = "dbo" };
            Name = "PoliticalLocations";
            _tableMutable.LoadColumn(new Column("Id", "int", this, isNullable: false, isImmutable: true) );
            _tableMutable.LoadColumn(new Column("LocationVableID", "int", this, isNullable: false) );
            _tableMutable.PrimaryKey = new PrimaryKey("PK_PoliticalLocations", new List<string> { "id" }, this);
            _tableMutable.LoadForeignKey(new ForeignKey("FK_PoliticalLocations_LocationVables", new Column("LocationVableID") , new LocationVables(),  new Column("Id"), this));
            _tableMutable.LoadForeignKey(new ForeignKey("FK_PoliticalLocations_FixedLocations", new Column("Id") , new FixedLocations(), new Column("Id"), this));
            _tableMutable.ParentTable = new FixedLocations();
            _tableMutable.LoadTrigger(new Trigger { Owner = this, BeforeInsert = true, Procedure = new StoredProcedureForTriggers { Name = "SPCalledByInsertTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
        }
    }

    public class Location_SequenceTable : TableAsSequence
    {
        public Location_SequenceTable() : base()
        {
            Schema = new Schema { Name = "dbo" };
            Name = "Location_SequenceTable";
            IdentityColumn = new Column("Id", "int", this, isNullable: false, isIdentity: true) ;
            DummyInsertColumn = new Column("x", "int", this) ;
            _tableMutable.LoadColumn(IdentityColumn);
            _tableMutable.LoadColumn(DummyInsertColumn);
        }
    }

    public class Nations : Table
    {
        public Nations() : base()
        {
            Name = "Nations";
            Schema = new Schema { Name = "dbo" };
            _tableMutable.LoadColumn(new Column("Id", "int", this, isNullable: false, isImmutable: true) );
            _tableMutable.LoadColumn(new Column("Abbrev", "nvarchar(50)", this, isNullable: false));
            _tableMutable.LoadColumn(new Column("Name", "nvarchar(100)", this, isNullable: false));
            _tableMutable.LoadColumn(new Column("LastUpdateDtm", "datetime", this, isNullable: false, isLastUpdateDtm: true));
            _tableMutable.LoadColumn(new Column("LastUpdater", "nvarchar(100)", this, isNullable: false, isLastUpdater: true));

            _tableMutable.PrimaryKey = new PrimaryKey("PK_Nations", new List<string> { "id" }, this);
            _tableMutable.ParentTable = new PoliticalLocations();
            _tableMutable.PrimaryKeySequence =  new PrimaryKeySequenceMap
            {
                PrimaryKeyColumn = new Column("Id") ,
                Sequence = new Location_SequenceTable(),
            };
            _tableMutable.ParentPassthrough =  new ParentPassthroughInfo
            {
                ParentPassthroughColumn = new Column("LocationVableID", "int"),
                ParentPassthroughSourceTable = new LocationVables(),
                ParentPassthroughSourceColumn = new Column("Id", "int") ,
                ParentPassthroughMatchingSourceColumn = new Column("Name", "nvarchar(100)") ,
                ParentPassthroughLookup = "Nations"
            };
            _tableMutable.LoadForeignKey(new ForeignKey("FK_Nations_PoliticalLocations", new Column("Id") , new PoliticalLocations(), new Column("Id"), this));
            _tableMutable.LoadIndex( new Index("idx_Nations_Abbrev", new List<string> { "Abbrev" }, this, isUnique: true, isClustered: false ));
            _tableMutable.LoadTrigger(new Trigger { Owner = this, BeforeInsert = true, Procedure = new StoredProcedureForTriggers { Name = "SPCalledByInsertTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
            _tableMutable.LoadTrigger(new Trigger { Owner = this, BeforeDelete = true, Procedure = new StoredProcedureForTriggers { Name = "SPCalledByDeleteTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
        }
    }

    public class NationalSubdivisions : Table
    {
        public NationalSubdivisions () : base()
        {
            Name = "NationalSubdivisions";
            Schema = new Schema { Name = "dbo" };
            _tableMutable.LoadColumn(new Column("Id", "int", this, isNullable: false, isImmutable: true));
            _tableMutable.LoadColumn(new Column("Abbrev", "nvarchar(50)", this, isNullable: false, isImmutable: true) );
            _tableMutable.LoadColumn(new Column("Name", "nvarchar(100)", this, isNullable: false));
            _tableMutable.LoadColumn(new Column("NationId", "int", this, isNullable: false) );
            _tableMutable.LoadColumn(new Column("LastUpdateDtm", "datetime", this, isNullable: false, isLastUpdateDtm: true) );
            _tableMutable.LoadColumn(new Column("LastUpdater", "nvarchar(100)", this, isNullable: false, isLastUpdater: true));

            _tableMutable.PrimaryKey = new PrimaryKey("PK_NationalSubdivisions", new List<string> { "id" }, this);
            _tableMutable.ParentTable = new PoliticalLocations();
            _tableMutable.PrimaryKeySequence =  new PrimaryKeySequenceMap
            {
                PrimaryKeyColumn = new Column("Id") ,
                Sequence = new Location_SequenceTable(),
            };
            _tableMutable.ParentPassthrough =  new ParentPassthroughInfo
            {
                ParentPassthroughColumn = new Column("LocationVableID", "int") ,
                ParentPassthroughSourceTable = new LocationVables(),
                ParentPassthroughSourceColumn = new Column("Id", "int"),
                ParentPassthroughMatchingSourceColumn = new Column("Name", "nvarchar(100)") ,
                ParentPassthroughLookup = "NationalSubdivisions"
            };

            _tableMutable.LoadForeignKey(new ForeignKey("FK_NationalSubdivisions_PoliticalLocations", new Column("Id"), new PoliticalLocations(),  new Column("Id"), this));
            _tableMutable.LoadForeignKey(new ForeignKey("FK_NationalSubdivisions_Nations", new Column("NationId") , new Nations(), new Column("Id"), this));
            _tableMutable.LoadIndex( new Index("idx_NationalSubdivisions", new List<string> { "Abbrev", "NationId" }, this,  true,  false ));
            _tableMutable.LoadTrigger(new Trigger { Owner = this, BeforeInsert = true, Procedure = new StoredProcedureForTriggers { Name = "SPCalledByInsertTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
            _tableMutable.LoadTrigger(new Trigger { Owner = this, BeforeDelete = true, Procedure = new StoredProcedureForTriggers { Name = "SPCalledByDeleteTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
        }
    }

    public class NatGasPipelines : Table
    {
        public NatGasPipelines()
        {
            Name = "NatGasPipelines";
            Schema = new Schema { Name = "dbo" };
            _tableMutable.LoadColumn(new Column("Id", "int", this, isNullable: false, isImmutable: true) );
            _tableMutable.LoadColumn(new Column("Abbrev", "nvarchar(50)", this, isNullable: false, isImmutable: true));
            _tableMutable.LoadColumn(new Column("Name", "nvarchar(100)", this, isNullable: false) );
            _tableMutable.LoadColumn(new Column("LastUpdateDtm", "datetime", this, isNullable: false, isLastUpdateDtm: true) );
            _tableMutable.LoadColumn(new Column("LastUpdater", "nvarchar(100)", this, isNullable: false, isLastUpdater: true));

            _tableMutable.PrimaryKey = new PrimaryKey("PK_NatGasPipelines", new List<string> { "id" }, this );
            _tableMutable.ParentTable = new FixedLocations();
            _tableMutable.PrimaryKeySequence =  new PrimaryKeySequenceMap
            {
                PrimaryKeyColumn = new Column("Id"),
                Sequence = new Location_SequenceTable(),
            };
            _tableMutable.ParentPassthrough =  new ParentPassthroughInfo
            {
                ParentPassthroughColumn = new Column("LocationVableID", "int"),
                ParentPassthroughSourceTable = new LocationVables(),
                ParentPassthroughSourceColumn = new Column("Id", "int") ,
                ParentPassthroughMatchingSourceColumn = new Column("Name", "nvarchar(100)") ,
                ParentPassthroughLookup = "NatGasPipelines"
            };

            _tableMutable.LoadForeignKey(new ForeignKey("FK_NatGasPipelines_FixedLocations", new Column("Id") , new FixedLocations(), new Column("Id"), this));
            _tableMutable.LoadIndex( new Index("idx_NatGasPipelines", new List<string> { "Abbrev"}, this, isClustered: false));
            _tableMutable.LoadTrigger(new Trigger { Owner = this, BeforeInsert = true, Procedure = new StoredProcedureForTriggers { Name = "SPCalledByInsertTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
            _tableMutable.LoadTrigger(new Trigger { Owner = this, BeforeDelete = true, Procedure = new StoredProcedureForTriggers { Name = "SPCalledByDeleteTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
        }
    }
    public class NatGasFacilityPoints : Table
    {
        public NatGasFacilityPoints()
        {
            Name = "NatGasFacilityPoints";
            Schema = new Schema { Name = "dbo" };
            _tableMutable.LoadColumn(new Column("Id", "int", this, isNullable: false, isImmutable: true));
            _tableMutable.LoadColumn(new Column("Abbrev", "nvarchar(50)", this, isNullable: false, isImmutable: true));
            _tableMutable.LoadColumn(new Column("Name", "nvarchar(100)", this, isNullable: false) );
            _tableMutable.LoadColumn(new Column("NatGasPipelineID", "int", this) );
            _tableMutable.LoadColumn(new Column("LastUpdateDtm", "datetime", this, isNullable: false, isLastUpdateDtm: true));
            _tableMutable.LoadColumn(new Column("LastUpdater", "nvarchar(100)", this, isNullable: false, isLastUpdater: true));

            _tableMutable.PrimaryKey = new PrimaryKey("PK_NatGasFacilityPoints", new List<string> { "id" }, this);
            _tableMutable.ParentTable = new FixedLocations();
            _tableMutable.PrimaryKeySequence =  new PrimaryKeySequenceMap
            {
                PrimaryKeyColumn = new Column("Id"),
                Sequence = new Location_SequenceTable(),
            };
            _tableMutable.ParentPassthrough =  new ParentPassthroughInfo
            {
                ParentPassthroughColumn = new Column("LocationVableID", "int") ,
                ParentPassthroughSourceTable = new LocationVables(),
                ParentPassthroughSourceColumn = new Column("Id", "int") ,
                ParentPassthroughMatchingSourceColumn = new Column("Name", "nvarchar(100)") ,
                ParentPassthroughLookup = "NatGasFacilityPoints"
            };

            _tableMutable.LoadForeignKey(new ForeignKey("FK_NatGasFacilityPoints_FixedLocations", new Column("Id"), new FixedLocations(), new Column("Id"), this));
            _tableMutable.LoadForeignKey(new ForeignKey("FK_NatGasFacilityPoints_NatGasPipelines", new Column("NatGasPipelineID") , new NatGasPipelines(), new Column("Id"), this));
            _tableMutable.LoadIndex( new Index("idx_NatGasFacilityPoints", new List<string> { "Abbrev", "NatGasPipelineID" }, this, true, false ));
            _tableMutable.LoadTrigger(new Trigger { Owner = this, BeforeInsert = true, Procedure = new StoredProcedureForTriggers { Name = "SPCalledByInsertTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
            _tableMutable.LoadTrigger(new Trigger { Owner = this, BeforeDelete = true, Procedure = new StoredProcedureForTriggers { Name = "SPCalledByDeleteTrigger", Schema = new Schema { Name = "dbo" }, Owner = this } });
        }
    }
    public class aTable : Table
    {
        public aTable() { Name = "aTable"; }
    }

}
