﻿//using System;
//using System.Diagnostics;

//namespace Com.VeritasTS.CoreUtils
//{

//    /// <summary>
//    /// Simple abstract implementation of IComparableByName
//    /// interface, where class is not invariant (that is,
//    /// the value of the Name property may be changed
//    /// after construction.
//    /// </summary>
//    abstract public class ComparableByNameMutable : ISelfAsserter, INamedMutable, IComparableByName
//    {

//        /****************
//         *  Private members
//         ***************/

//        private string _name;

//        /****************
//         *  Constructors
//         ***************/

//        /// <summary>
//        /// Hides default constructor.
//        /// </summary>
//        private ComparableByNameMutable() { }

//        /// <summary>
//        /// Construct from name.
//        /// </summary>
//        public ComparableByNameMutable(string name) { Name = name; }

//        /****************
//         *  Object methods overridden
//         ***************/

//        /// <summary>
//        /// Standard Equals method, overridden to be false
//        /// if the other object is not an object of the same
//        /// class as this and EqualsInName if it 
//        /// isn't.
//        /// </summary>
//        /// <returns>Truth of statement "other object is a ComparableByNameMutable
//        /// equal to this in class and name."</returns>
//        public override bool Equals(Object obj)
//        {
//            if (obj == null) return false;
//            else if (!obj.GetType().Equals(GetType())) return false;
//            else return EqualsInName((ComparableByNameMutable)obj);
//        }

//        /// <summary>
//        /// Standard GetHashCode method, overridden to be the Name's hash
//        /// code if Name is not null and 0 if it is.
//        /// </summary>
//        /// <returns>The hash code</returns>
//        public override int GetHashCode()
//        {
//            return (Name == null ? 0 : Name.GetHashCode());
//        }

//        /****************
//         *  ISelfAsserter methods
//         ***************/

//        /// <summary>
//        /// Asserts class invariants; as there are none at the base
//        /// class level, simply returns an instant true.
//        /// </summary>
//        /// <returns>
//        /// At base level, simply returns true. </returns>
//        public virtual bool PassesSelfAssertion(Type testableType = null) { return true; }

//        /****************
//         *  INamed properties via INamedMutable
//         ***************/

//        /// <summary>
//        /// Simple string property Name, trivially both
//        /// gettable and settable.
//        /// </summary>
//        public string Name { get; set; }

//        /****************
//         *  INamedMutable methods
//         ***************/

//        /// <summary>
//        /// Resets the object's name, optionally to null.
//        /// </summary>
//        /// <remarks>
//        /// There is an optional parameter to support
//        /// self-assertion in complex objects whose
//        /// SetName method can be called as part of a
//        /// construction process, in which case
//        /// self-assertion should be disabled pending
//        /// completion of construction.</remarks>
//        /// <param name="value">The new value for the name.</param>
//        /// <param name="assert">
//        /// Where the object self-asserts after changes
//        /// in value that occur after construction, the default
//        /// value of true allows that self-assertion to occur,
//        /// while false disables self-assertion. Where the
//        /// object is not a self-asserter, this parameter has
//        /// no effects.</param>
//        public void SetName(string value, bool assert = true)
//        {
//            _name = value;
//            if (assert) Debug.Assert(PassesSelfAssertion());
//        }

//        /****************
//         *  IComparableByName methods
//         ***************/

//        /// <summary>
//        /// EqualsInName method required by the
//        /// IComparableByName interface.
//        /// </summary>
//        /// <remarks>Two null Names are considered equal,
//        /// and a null object is considered to have a null Name.</remarks>
//        /// <returns>Truth of statement "the two Names are equal."</returns>
//        public bool EqualsInName(IComparableByName other)
//        {
//            return CompareByNameTo(other) == 0;
//        }

//        /// <summary>
//        /// CompareByNameTo method required by the
//        /// IComparableByName interface.
//        /// </summary>
//        /// <remarks>Two null Names are considered equal,
//        /// and a null object is considered to have a null Name.</remarks>
//        /// <returns>0 if it is considered true that "the two Names are equal,"
//        /// -1 if this's Name is alphabetically prior to the other's Name,
//        /// and 1 if this's Name is alphabetically subsequent to the other's Name.</returns>
//        public int CompareByNameTo(IComparableByName other)
//        {
//            if (Name != null)
//            {
//                if (other == null) return -1;
//                else return Name.CompareTo(other.Name);
//            }
//            else if (other == null) return 0;
//            else if (other.Name == null) return 0;
//            else return 1;
//        }

//        /****************
//         *  Static versions of IComparableByName methods
//         ***************/

//        /// <summary>
//        /// Static dual-argument CompareByName, analogous to
//        /// the CompareByNameTo method required by the
//        /// IComparableByName interface.
//        /// </summary>
//        /// <returns>Truth of statement "the two Names are equal," where two
//        /// nulls are considered equal, and a null object is considered to
//        /// have a null Name.</returns>
//        public static int CompareByName(IComparableByName theOne, IComparableByName theOther)
//        {
//            if (theOne != null) return theOne.CompareByNameTo(theOther);
//            else if (theOther != null) return theOther.CompareByNameTo(null);
//            else return 1;
//        }

//        /// <summary>
//        /// Static dual-argument EqualsInName, analogous to
//        /// the EqualsInName method required by the
//        /// IComparableByName interface.
//        /// </summary>
//        /// <returns>Truth of statement "the two Names are equal," where two
//        /// nulls are considered equal, and a null object is considered to
//        /// have a null Name.</returns>
//        public static bool AreEqualInName(IComparableByName theOne, IComparableByName theOther)
//        {
//            return (CompareByName( theOne, theOther ) == 0);
//        }

//    }

//}
