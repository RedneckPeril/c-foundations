﻿
//namespace Com.VeritasTS.CoreUtils
//{

//    /// <summary>
//    /// This interface simply specifies that the class implementing
//    /// the interface has a readable string property FullName.
//    /// </summary>
//    /// <remarks>
//    /// There is no test code since the entire contract is
//    /// fulfilled if the code will compile.
//    /// </remarks>
//    public interface IFullNamed
//    {
//        /// <summary>
//        /// Returns the object's full name, which could be null.
//        /// </summary>
//        /// <returns>The object's full name.</returns>
//        string FullName { get; }
//    }
//}
