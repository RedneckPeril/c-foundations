﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Com.VeritasTS.CoreUtils
//{

//    /// <summary>
//    /// This interface labels objects that can safely be used
//    /// as private members of an object that is meant to be
//    /// invariant, either because they are themselves invariant,
//    /// or because they can make a threadsafe copy of themselves
//    /// deep enough to ensure that no references are left to 
//    /// any variant object still contained within the holding
//    /// object.
//    /// </summary>
//    /// <remarks>
//    /// The interface provides the simple method GetSafeReference.
//    /// If the object is invariant itself then it can just return
//    /// itself. Otherwise, it does a deep-enough clone and return
//    /// the reference to that.
//    /// <para>
//    /// In essence, this is a variation on cloning, but for the
//    /// specific purpose of allowing an invariant object safely
//    /// to own references to objects that are themselves not
//    /// innately invariant. Generally speaking you will have to cast
//    /// the reference back to the class you're looking for
//    /// before using it.</para>
//    /// </remarks>
//    public interface ISafeMemberOfImmutableOwner
//    {
//        /// <summary>
//        /// Returns either a safe reference to itself, or
//        /// else a safe reference to a valid copy of itself.
//        /// </summary>
//        /// <remarks>
//        /// The reference returned must be to an object that 
//        /// always honors the following contract:
//        /// <list type="table">
//        /// <listheader><term>Specification</term><description>Test method in BaseISafeMemberOfImmutableOwnerTester class</description></listheader>
//        /// <item><term>x.Equals( x.GetSafeReference() )</term><description>SelfEqualsSafeReference</description></item></list>
//        /// <item><term>x.GetType().Equals( x.GetSafeReference().GetType() )</term><description>TypeOfSelfEqualsTypeOfSafeReference</description></item>
//        /// </remarks>
//        /// <returns></returns>
//        ISafeMemberOfImmutableOwner GetSafeReference();
//    }
//}
