﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Com.VeritasTS.CoreUtils
//{
//    public static class EqualityUtilities
//    {
//        public static bool EqualOrBothNull(Object theOne, Object theOther)
//        {
//            if (theOne == null) return theOther == null;
//            else return theOne.Equals(theOther);
//        }
//        public static bool EqualAndNotNull(Object theOne, Object theOther)
//        {
//            if (theOne == null) return false;
//            else if (theOther == null) return false;
//            else return theOne.Equals(theOther);
//        }

//        public static bool MemberMatchwiseEqualOrBothNull(IMemberMatcher matcher, Object theOther)
//        {
//            if (matcher == null) return theOther == null;
//            else if (theOther == null) return false;
//            else if (!matcher.MembersMatch(theOther)) return false;
//            else if (matcher.GetType().Equals(theOther.GetType())) return true;
//            else return ((IMemberMatcher)theOther).MembersMatch(matcher);
//        }

//        public static bool MemberMatchwiseEqualAndNotNull(IMemberMatcher matcher, Object theOther)
//        {
//            if (matcher == null) return false;
//            else if (theOther == null) return false;
//            else if (!matcher.MembersMatch(theOther)) return false;
//            else if (matcher.GetType().Equals(theOther.GetType())) return true;
//            else return ((IMemberMatcher)theOther).MembersMatch(matcher);
//        }
//    }
//}
