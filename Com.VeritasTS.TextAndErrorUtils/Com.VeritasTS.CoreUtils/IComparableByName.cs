﻿
//namespace Com.VeritasTS.CoreUtils
//{

//    /// <summary>
//    /// This interface specifies that the class implementing
//    /// the interface has a CompareByNameTo method and
//    /// an EqualsByName method, each of which implements
//    /// standard CompareTo logic against the objects' Name
//    /// property.
//    /// </summary>
//    public interface IComparableByName: INamed
//    {

//        /// <summary>
//        /// Implements standard CompareTo logic against the objects' Name
//        /// property.
//        /// </summary>
//        /// <remarks>
//        /// By contract, the result of calling x.CompareByNameTo( y ),
//        /// for x and y both IComparableByName's, should reflect the
//        /// standard for behavior of CompareTo in the IComparable&lt;T&gt;
//        /// interface. Specifically, null is less than non-null and two
//        /// nulls are equal. So we should get x.Name.CompareTo( y.Name )
//        /// where no nulls are involved, x.Name.CompareTo( null ) where
//        /// either y or y.Name is null, -y.Name.CompareTo( null ) where
//        /// x.Name is null and y.Name is not, and 0 where x.Name is null
//        /// and so is either y or y.Name.
//        /// <list type="table">
//        /// <listheader><term>Specification</term><description>Test method in BaseIComparableByNameTester class</description></listheader>
//        /// <item><term>Where non-null x.Name compares less than non-null y.Name, x.CompareToByName(y) == -1</term><description>XComparedByNameToYWithAlphabeticallyLaterNameIsMinus1</description></item>
//        /// <item><term>Where non-null x.Name compares equal to non-null y.Name, x.CompareToByName(y) == 0</term><description>XComparedByNameToYWithAlphabeticallyEqualNameIs0</description></item>
//        /// <item><term>Where non-null x.Name compares greater than non-null y.Name, x.CompareToByName(y) == 1</term><description>XComparedByNameToYWithAlphabeticallyEarlierNameIs1</description></item>
//        /// <item><term>Where non-null x.Name is compared to null y.Name, x.CompareToByName(y) == -1</term><description>XWithNonnullNameComparedByNameToYWithNullNameIsMinus1</description></item>
//        /// <item><term>Where non-null x.Name is compared to null y, x.CompareToByName(y) == -1</term><description>XWithNonnullNameComparedByNameToNullIsMinus1</description></item>
//        /// <item><term>Where x.Name is null and y.Name is not, x.CompareToByName(y) == -1</term><description>XWithNullNameComparedByNameToYWithNonnullNameIs1</description></item>
//        /// <item><term>Where x.Name is null and y.Name is null, x.CompareToByName(y) == 0</term><description>XWithNullNameComparedByNameToYWithNullNameIs0</description></item>
//        /// <item><term>Where x.Name is null and y is null, x.CompareToByName(y) == 0</term><description>XWithNullNameComparedByNameToNullIs0</description></item>
//        /// </list>
//        /// </remarks>
//        int CompareByNameTo(IComparableByName other);

//        /// <summary>
//        /// Implements standard Equals logic against the objects' Name
//        /// property, in accordance with the CompareByNameTo logic.
//        /// </summary>
//        /// <remarks>
//        /// By contract, x.EqualsInName(y) should return true
//        /// when x.CompareByNameTo(y) == 0 and false otherwise.
//        /// <list type="table">
//        /// <listheader><term>Specification</term><description>Test method in BaseIComparableByNameTester class</description></listheader>
//        /// <item><term>Where non-null x.Name compares less than non-null y.Name, x.EqualsInName(y) == false</term><description>XComparedByNameToYWithAlphabeticallyLaterNameIsFalse</description></item>
//        /// <item><term>Where non-null x.Name compares equal to non-null y.Name, x.EqualsInName(y) == true</term><description>XComparedByNameToYWithAlphabeticallyEqualNameIsTrue</description></item>
//        /// <item><term>Where non-null x.Name compares greater than non-null y.Name, x.EqualsInName(y) == false</term><description>XComparedByNameToYWithAlphabeticallyEarlierNameIsFalse</description></item>
//        /// <item><term>Where non-null x.Name is compared to null y.Name, x.EqualsInName(y) == false</term><description>XWithNonnullNameComparedByNameToYWithNullNameIsFalse</description></item>
//        /// <item><term>Where non-null x.Name is compared to null y, x.EqualsInName(y) == false</term><description>XWithNonnullNameComparedByNameToNullIsFalse</description></item>
//        /// <item><term>Where x.Name is null and y.Name is not, x.EqualsInName(y) == false</term><description>XWithNullNameComparedByNameToYWithNonnullNameIsFalse</description></item>
//        /// <item><term>Where x.Name is null and y.Name is null, x.EqualsInName(y) == true</term><description>XWithNullNameComparedByNameToYWithNullNameIsTrue</description></item>
//        /// <item><term>Where x.Name is null and y is null, x.EqualsInName(y) == true</term><description>XWithNullNameComparedByNameToNullIsTrue</description></item>
//        /// </list>
//        /// </remarks>
//        bool EqualsInName(IComparableByName other);
//    }
//}
