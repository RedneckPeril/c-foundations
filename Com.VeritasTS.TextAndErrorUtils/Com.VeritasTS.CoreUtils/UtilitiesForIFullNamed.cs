﻿//using System;
//using System.Text.RegularExpressions;

//namespace Com.VeritasTS.CoreUtils
//{
//    public class UtilitiesForIFullNamed
//    {

//        public static bool NonnullFullNamedParameterIsValid(IFullNamed element, string objectTypeFullName, string paramName, string callingMethod)
//        {
//            if (element == null)
//                throw new ArgumentNullException(
//                    paramName: paramName
//                    , message: ""
//                    + "Cannot provide null as '" + paramName + "' argument to "
//                    + callingMethod + "."
//                    );
//            if (element.FullName == null)
//                throw new ArgumentNullException(
//                    paramName: paramName + ".FullName"
//                    , message: ""
//                    + "Cannot provide " + objectTypeFullName + " with null FullName as '" + paramName + "' argument to "
//                    + callingMethod + "."
//                    );
//            return true;
//        }

//        public static bool ObjectWithNonnullFullNamedPropertyIsValidParameter(
//            IFullNamed element
//            , string objectTypeName
//            , string paramName
//            , string fullNamedPropertyName
//            , string fullNamedPropertyType
//            , bool fullNamedIsProperty
//            , string callingMethod
//            )
//        {
//            string propertyDescriptor = fullNamedPropertyName + " " + (fullNamedIsProperty ? "property" : "method");
//            string fullNamedPropertyFullDescription
//                = paramName + "." + fullNamedPropertyName;
//            if (element == null)
//                throw new ArgumentNullException(
//                    paramName: fullNamedPropertyFullDescription
//                    , message: ""
//                    + "Cannot provide " + objectTypeName
//                    + " with null " + propertyDescriptor
//                    + " as '" + paramName + "' argument to "
//                    + callingMethod + "."
//                    );
//            if (element.FullName == null)
//                throw new ArgumentNullException(
//                    paramName: fullNamedPropertyFullDescription + ".FullName"
//                    , message: ""
//                    + "Cannot provide " + objectTypeName
//                    + " whose " + propertyDescriptor
//                    + " has null FullName as '" + paramName + "' argument to "
//                    + callingMethod + "."
//                    );
//            return true;
//        }

//        public static string FullNameOf(IFullNamed obj, string defaultValue = null)
//        {
//            if (obj == null) return defaultValue;
//            else if (obj.FullName != null) return obj.FullName;
//            else return defaultValue;
//        }

//    }

//}
