﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.VeritasTS.CoreUtils;
using NUnit.Framework;

// This tester is ready for use.

namespace Com.VeritasTS.CoreUtils.Test
{
    /// <summary>
    /// Base class tester for anything implementing the 
    /// Com.VeritasTS.CoreUtils.ISafeMemberOfImmutableOwner interface.
    /// </summary>
    /// <remarks>Anyone implementing the
    /// Com.VeritasTS.CoreUtils.ISafeMemberOfImmutableOwner must
    /// ensure that their implementation both fully honors
    /// the interface contract, and is proven to do so by
    /// thorough unit testing. Any code path that is followed within
    /// GetSafeReference must result in an object that honors this
    /// contract.
    /// <para>As the contract is a very simple one the base class 
    /// tests require the descendant to provide only a single 
    /// testable object, retrieved by the base class through
    /// the TestableObject() virtual method.</para></remarks>
    [TestFixture]
    public class EqualsAndHashCodeTester
    {

        private class TestClass
        {
            public bool B { get; }
            public TestClass( bool b = true ) { B = b; }
            public override bool Equals(object obj)
            {
                if (obj == null) return false;
                if (obj == this) return true;
                if (!obj.GetType().Equals(GetType())) return false;
                return (B == ((TestClass)obj).B);
            }
            public override int GetHashCode()
            {
                return (B ? 1 : 0);
            }
        }

        /// <summary>
        /// When a class implements ISafeMemberOfImmutableOwner, its associated
        /// BaseISafeMemberOfImmutableOwnerTester class must override
        /// this method to provide the base class tests with a testable
        /// instance of the new ISafeMemberOfImmutableOwner class.
        /// </summary>
        /// <returns>A testable object from the ISafeMemberOfImmutableOwner class
        /// undergoing validation.</returns>
        public virtual object TestableObject()
        {
            return new TestClass();
        }

        /// <summary>
        /// When a class implements ISafeMemberOfImmutableOwner, its associated
        /// BaseISafeMemberOfImmutableOwnerTester class must override
        /// this method to provide the base class tests with a testable
        /// instance of the new ISafeMemberOfImmutableOwner class.
        /// </summary>
        /// <returns>A testable object from the ISafeMemberOfImmutableOwner class
        /// undergoing validation.</returns>
        public virtual object DifferentObjectEqualToTestableObject()
        {
            return new TestClass();
        }

        /// <summary>
        /// When a class implements ISafeMemberOfImmutableOwner, its associated
        /// BaseISafeMemberOfImmutableOwnerTester class must override
        /// this method to provide the base class tests with a testable
        /// instance of the new ISafeMemberOfImmutableOwner class.
        /// </summary>
        /// <returns>A testable object from the ISafeMemberOfImmutableOwner class
        /// undergoing validation.</returns>
        public virtual object SecondDifferentObjectEqualToTestableObject()
        {
            return new TestClass();
        }

        /// <summary>
        /// When a class implements ISafeMemberOfImmutableOwner, its associated
        /// BaseISafeMemberOfImmutableOwnerTester class must override
        /// this method to provide the base class tests with a testable
        /// instance of the new ISafeMemberOfImmutableOwner class.
        /// </summary>
        /// <returns>A testable object from the ISafeMemberOfImmutableOwner class
        /// undergoing validation.</returns>
        public virtual object ObjectWithHashCodeNotEqualToTestableObject()
        {
            return new TestClass(false);
        }

        /***************************************************
         *** 
         *** 
         *** TEST CASES
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Enforces the contract provision that 
        /// no object is equal to null.
        /// </summary>
        [TestCase]
        public void XNotEqualToNull()
        {
            object x = TestableObject();
            if (x != null)
            {
                Assert.False(x.Equals(null));
            }
        }

        /// <summary>
        /// Enforces the contract provision that 
        /// x.Equals(x).
        /// </summary>
        [TestCase]
        public void XEqualsX()
        {
            object x = TestableObject();
            if (x != null)
                Assert.True(x.Equals(x));
        }

        /// <summary>
        /// Enforces the contract provision that 
        /// x.Equals(any object with the same reference as x).
        /// </summary>
        [TestCase]
        public void XEqualsObjectWithSameReference()
        {
            object x = TestableObject();
            if (x != null)
            {
                object y = x;
                Assert.True(x.Equals(y));
            }
        }

        /// <summary>
        /// Enforces symmetry: if x and y are not the 
        /// same object (x != y) but x.Equals(y), then y.Equals(x).
        /// </summary>
        /// <remarks>
        /// Note that in descendants, additional testing may be required
        /// to ensure that, for example, base class instances do not think
        /// they are equal to descendant class instances that have extra
        /// members and consider themselves therefore unequal.</remarks>
        [TestCase]
        public void WhenXEqualsYYEqualsX()
        {
            object x = TestableObject();
            if (x != null)
            {
                object y = DifferentObjectEqualToTestableObject();
                Assert.True(x.Equals(y) && y.Equals(x));
            }
        }

        /// <summary>
        /// Enforces transitivity: if x.Equals(y) and y.Equals(z),
        /// then x.Equals(z).
        /// </summary>
        [TestCase]
        public void WhenXEqualsYAndYEqualsZThenXEqualsZ()
        {
            object x = TestableObject();
            if (x != null)
            {
                object y = DifferentObjectEqualToTestableObject();
                if (y != null)
                {
                    object z = SecondDifferentObjectEqualToTestableObject();
                    Assert.True(x.Equals(y) && y.Equals(z) && x.Equals(z));
                }
            }
        }

        /// <summary>
        /// Enforces condition that two objects whose hash codes
        /// are not equal cannot themselves be equal.
        /// </summary>
        [TestCase]
        public void WhenHashCodesAreUnequalObjectsAreUnequal()
        {
            object x = TestableObject();
            if (x != null)
            {
                object y = ObjectWithHashCodeNotEqualToTestableObject();
                if (y != null )
                {
                    Assert.True(
                        x.GetHashCode() != y.GetHashCode()
                        && !x.Equals(y)
                        );
                }
            }
        }

        /***************************************************
         *** 
         *** 
         *** MASTER TEST METHOD
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Enforces condition that two objects whose hash codes
        /// are not equal cannot themselves be equal.
        /// </summary>
        [TestCase]
        public virtual void ExecuteAllQueries()
        {
            XNotEqualToNull();
            XEqualsX();
            XEqualsObjectWithSameReference();
            WhenXEqualsYYEqualsX();
            WhenXEqualsYAndYEqualsZThenXEqualsZ();
            WhenHashCodesAreUnequalObjectsAreUnequal();
        }

    }

}
