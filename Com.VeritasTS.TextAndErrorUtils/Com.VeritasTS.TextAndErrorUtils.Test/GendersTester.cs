﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Com.VeritasTS.TextAndErrorUtils;
using Com.VeritasTS.TextAndErrorUtils.Inflection;

// This tester is ready for use.

namespace Com.VeritasTS.TextAndErrorUtils.Inflection.Test
{

    [TestFixture]
    public class GendersTester
    {

        private const int COUNT_OF_GENDERS = 4;

        /// <summary>Validates that CountOfGenders has the correct number of genders,
        /// at present (2017 February 28) 4.</summary>
        [TestCase]
        public void CountOfGendersIsCorrect()
        {
            Assert.True(Genders.CountOfGenders == COUNT_OF_GENDERS);
        }

        /// <summary>Validates that AllGenders has the correct number of genders.</summary>
        [TestCase]
        public void AllGendersHasCorrectNumberOfGenders()
        {
            Assert.True(Genders.AllGenders.Count == COUNT_OF_GENDERS);
        }

        /// <summary>Validates that AllGenders generates a new list on each call 
        /// (thus ensuring that it is not exposing the private list member.</summary>
        [TestCase]
        public void SecondCallToAllGendersDoesNotReturnSameListInstance()
        {
            List<IGender> firstCall = Genders.AllGenders;
            List<IGender> secondCall = Genders.AllGenders;
            Assert.True(firstCall != secondCall);
        }

        /// <summary>Validates that AllGenders begins with earliest class name,
        /// which belongs to Feminine.</summary>
        [TestCase]
        public void AllGendersBeginsWithEarliestClassName()
        {
            Assert.True(Genders.AllGenders.ElementAt(0) == Genders.Feminine);
        }

        /// <summary>Validates that AllGenders ends with last Class name,
        /// which belongs to NotApplicable.</summary>
        [TestCase]
        public void AllGendersEndsWithLastClassName()
        {
            Assert.True(Genders.AllGenders.Last() == Genders.NotApplicable);
        }

        /// <summary>Validates that ObservableAllGenders has the correct number of numbers.</summary>
        [TestCase]
        public void ObservableAllGendersHasCorrectGenderOfGenders()
        {
            Assert.True(Genders.ObservableAllGenders.Count == COUNT_OF_GENDERS);
        }

        /// <summary>Validates that ObservableAllGenders begins with earliest Class name,
        /// which belongs to Feminine.</summary>
        [TestCase]
        public void ObservableAllGendersBeginsWithEarliestClassName()
        {
            Assert.True(Genders.ObservableAllGenders.ElementAt(0) == Genders.Feminine);
        }

        /// <summary>Validates that ObservableAllGenders ends with last Class name,
        /// which belongs to NotApplicable.</summary>
        [TestCase]
        public void ObservableAllGendersEndsWithLastClassName()
        {
            Assert.True(Genders.ObservableAllGenders.Last() == Genders.NotApplicable);
        }



    }
}
