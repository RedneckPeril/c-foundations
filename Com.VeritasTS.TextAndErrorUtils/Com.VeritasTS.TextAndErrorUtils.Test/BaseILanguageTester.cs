﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using NUnit.Framework;

// This tester is ready for use.

namespace Com.VeritasTS.TextAndErrorUtils
{

    /// <summary>
    /// Base class tester for anything implementing the 
    /// Com.VeritasTS.TextAndErrorUtils.ILanguage interface.
    /// </summary>
    /// <remarks>Anyone implementing the
    /// Com.VeritasTS.TextAndErrorUtils.ILanguage must
    /// ensure that their implementation both fully honors
    /// the interface contract, and is proven to do so by
    /// thorough unit testing. 
    /// <para>Along with the normal virtual TestableObject()
    /// method, a number of special-case testable objects may
    /// be supplied by the descendant class. None of them MUST
    /// be supplied by the descendant class, as any descendant
    /// class could be designed not to allow such cases. As always,
    /// a descendant class that does not produce a given
    /// special case simply overrides the special-case method
    /// to return null.</para>
    /// <list type="bullet">
    /// <item>LanguageWithTwoLetterISOLanguageName</item>
    /// <item>LanguageWithAlternateThreeLetterISOLanguageName</item>
    /// <item>LanguageWithPrivateUseCharacterName</item>
    /// <item>LanguageWithoutPrivateUseCharacterName</item>
    /// <item>LanguageWithPrivateUseCharacterNameFontName</item>
    /// <item>LanguageWithoutPrivateUseCharacterNameFontName</item>
    /// </list> </remarks>
    [TestFixture]
    public class BaseILanguageTester
    {
        
        private class BaseTestLanguage : ILanguage
        {
            private string name;
            private string transliteratedName;
            private string englishName;
            private string twoLetterISOLanguageName;
            private string threeLetterISOLanguageName;
            private string alternateThreeLetterISOLanguageName;
            private string privateUseCharacterName;
            private string privateUseCharacterNameFontName;
            public BaseTestLanguage(
                string name
                , string transliteratedName
                , string englishName
                , string twoLetterISOLanguageName
                , string threeLetterISOLanguageName
                , string alternateThreeLetterISOLanguageName
                , string privateUseCharacterName
                , string privateUseCharacterNameFontName
                )
            {
                this.name = name;
                this.transliteratedName = transliteratedName;
                this.englishName = englishName;
                this.twoLetterISOLanguageName = twoLetterISOLanguageName;
                this.threeLetterISOLanguageName = threeLetterISOLanguageName;
                this.threeLetterISOLanguageName = threeLetterISOLanguageName;
                this.alternateThreeLetterISOLanguageName = alternateThreeLetterISOLanguageName;
                this.privateUseCharacterName = privateUseCharacterName;
                this.privateUseCharacterNameFontName = privateUseCharacterNameFontName;
            }
            public string Name => name;
            public string TransliteratedName => transliteratedName; 
            public string EnglishName => englishName; 
            public string TwoLetterISOCode => twoLetterISOLanguageName; 
            public string ThreeLetterISOCode => threeLetterISOLanguageName; 
            public string AlternateThreeLetterISOCode => alternateThreeLetterISOLanguageName; 
            public string PrivateUseCharacterName => privateUseCharacterName; 
            public string PrivateUseCharacterNameFontName => privateUseCharacterNameFontName; 
        }

        const string leadingWhitespacePattern = "\\A\\s";
        const string trailingWhitespacePattern = "\\s\\z";
        const string nonspaceWhitespacePattern = "[\\a\\b\\t\\r\\v\\f\\n\\e]";
        const string leadingCapitalPattern = "\\A[A-Z]";
        const string privateUseCharacterPattern = "\\p{IsPrivateUse}";

        ILanguage English()
        {
            return new BaseTestLanguage(
                name: "English"
                , transliteratedName: "English"
                , englishName: "English"
                , twoLetterISOLanguageName: "en"
                , threeLetterISOLanguageName: "eng"
                , alternateThreeLetterISOLanguageName: null
                , privateUseCharacterName: null
                , privateUseCharacterNameFontName: null
                );
        }
        ILanguage Armenian()
        {
            return new BaseTestLanguage(
                name: "հայերէն"
                , transliteratedName: "hayeren"
                , englishName: "Armenian"
                , twoLetterISOLanguageName: "hy"
                , threeLetterISOLanguageName: "arm"
                , alternateThreeLetterISOLanguageName: "arm"
                , privateUseCharacterName: null
                , privateUseCharacterNameFontName: null
                );
        }
        ILanguage Klingon()
        {
            return new BaseTestLanguage(
                name: "tlhIngan-Hol"
                , transliteratedName: "tlhIngan-Hol"
                , englishName: "Klingon"
                , twoLetterISOLanguageName: null
                , threeLetterISOLanguageName: "tlh"
                , alternateThreeLetterISOLanguageName: null
                , privateUseCharacterName: " "
                , privateUseCharacterNameFontName: "pIqaD"
                );
        }

        /// <summary>
        /// Testable instance of class under testing,
        /// such that the testable instance possesses
        /// a non-null two-letter ISO language name.
        /// </summary>
        /// <returns>Testable ILanguage with non-null two-letter ISO language name.</returns>
        public virtual ILanguage LanguageWithTwoLetterISOLanguageName()
        {
            return English();
        }

        /// <summary>
        /// Testable instance of class under testing,
        /// such that the testable instance possesses
        /// a non-null alternate three-letter ISO language name.
        /// </summary>
        /// <returns>Testable ILanguage with non-null
        /// alternate three-letter ISO language name.</returns>
        public virtual ILanguage LanguageWithAlternateThreeLetterISOLanguageName()
        {
            return Armenian();
        }

        /// <summary>
        /// Testable instance of class under testing,
        /// such that the testable instance possesses
        /// a non-null private-use character name.
        /// </summary>
        /// <returns>Testable ILanguage with non-null
        /// private-use character name.</returns>
        public virtual ILanguage LanguageWithPrivateUseCharacterName()
        {
            return Klingon();
        }

        /// <summary>
        /// Testable instance of class under testing,
        /// such that the testable instance does not possess
        /// a non-null private-use character name.
        /// </summary>
        /// <returns>Testable ILanguage with no non-null
        /// private-use character name.</returns>
        public virtual ILanguage LanguageWithoutPrivateUseCharacterName()
        {
            return English();
        }

        /// <summary>
        /// Testable instance of class under testing,
        /// such that the testable instance possesses
        /// a non-null private-use character name font name.
        /// </summary>
        /// <returns>Testable ILanguage with non-null
        /// private-use character name font name.</returns>
        public virtual ILanguage LanguageWithPrivateUseCharacterNameFontName()
        {
            return Klingon();
        }

        /// <summary>
        /// Testable instance of class under testing,
        /// such that the testable instance does not possess
        /// a non-null private-use character name font name.
        /// </summary>
        /// <returns>Testable ILanguage with no non-null
        /// private-use character name font name.</returns>
        public virtual ILanguage LanguageWithoutPrivateUseCharacterNameFontName()
        {
            return English();
        }

        /// <summary>
        /// Standard general-purpose testable object for classes that implement ILanguage.
        /// </summary>
        /// <remarks>
        /// The base class implementation will take any of the special-case 
        /// testing objects as long as at least one of them is non-null; so if
        /// the class under testing has a "special case" (from the base tester's
        /// perspective) that covers the entire range of the class, it need
        /// only provide that special case. If multiple special cases are available
        /// then the base class implementation takes the first non-null it finds
        /// when working in the following order:
        /// <list type="number">
        /// <item>LanguageWithTwoLetterISOLanguageName</item>
        /// <item>LanguageWithAlternateThreeLetterISOLanguageName</item>
        /// <item>LanguageWithPrivateUseCharacterName</item>
        /// <item>LanguageWithoutPrivateUseCharacterName</item>
        /// <item>LanguageWithPrivateUseCharacterNameFontName</item>
        /// <item>LanguageWithoutPrivateUseCharacterNameFontName</item>
        /// </list> </remarks>
        /// <returns>Testable ILanguage suitable for general testing.</returns>
        public virtual ILanguage TestableObject()
        {
            ILanguage retval = LanguageWithTwoLetterISOLanguageName();
            if (retval == null)
                retval = LanguageWithAlternateThreeLetterISOLanguageName();
            if (retval == null)
                retval = LanguageWithPrivateUseCharacterName();
            if (retval == null)
                retval = LanguageWithoutPrivateUseCharacterName();
            if (retval == null)
                retval = LanguageWithPrivateUseCharacterNameFontName();
            if (retval == null)
                retval = LanguageWithoutPrivateUseCharacterNameFontName();
            return retval;
        }

        /***************************************************
         *** 
         *** 
         *** VALIDATION METHODS
         *** 
         *** 
         ************************************************ */

        /* 
         * 
         * 
         * Name validation methods
         * 
         * 
         * 
         * */

        /// <summary>
        /// Ensures that the Name property of the language argument
        /// is not null.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void NameIsNotNull(ILanguage language)
        {
            Assert.False(language.Name == null);
        }

        /// <summary>
        /// Ensures that the Name property of the language argument
        /// is not the empty string.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void NameIsNotEmptyString(ILanguage language)
        {
            Assert.True(language.Name.Length > 0);
        }

        /// <summary>
        /// Ensures that the Name property of the language argument
        /// does not begin with whitespace.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void NameDoesNotBeginWithWhitespace(ILanguage language)
        {
            Assert.False(Regex.IsMatch(language.Name, leadingWhitespacePattern));
        }

        /// <summary>
        /// Ensures that the Name property of the language argument
        /// does not end with whitespace.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void NameDoesNotEndWithWhitespace(ILanguage language)
        {
            Assert.False(Regex.IsMatch(language.Name, trailingWhitespacePattern));
        }

        /// <summary>
        /// Ensures that the Name property of the language argument
        /// contains no private-use Unicode characters.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void NameContainsNoPrivateUseUnicodeCharacters(ILanguage language)
        {
            Assert.False(Regex.IsMatch(language.Name, privateUseCharacterPattern));
        }

        /// <summary>
        /// Applies to the language argument all validations of the Name property.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void ValidateName(ILanguage language)
        {
            NameIsNotNull(language);
            NameIsNotEmptyString(language);
            NameDoesNotBeginWithWhitespace(language);
            NameDoesNotEndWithWhitespace(language);
            NameContainsNoPrivateUseUnicodeCharacters(language);
        }

        /* 
         * 
         * 
         * TransliteratedName validation methods -- same as Name validation methods
         * 
         * 
         * 
         * */

        /// <summary>
        /// Ensures that the TransliteratedName property of the language argument
        /// is not null.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void TransliteratedNameIsNotNull(ILanguage language)
        {
            Assert.False(language.TransliteratedName == null);
        }

        /// <summary>
        /// Ensures that the TransliteratedName property of the language argument
        /// is not the empty string.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void TransliteratedNameIsNotEmptyString(ILanguage language)
        {
            Assert.True(language.TransliteratedName.Length > 0);
        }

        /// <summary>
        /// Ensures that the TransliteratedName property of the language argument
        /// does not begin with whitespace.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void TransliteratedNameDoesNotBeginWithWhitespace(ILanguage language)
        {
            Assert.False(Regex.IsMatch(language.TransliteratedName, leadingWhitespacePattern));
        }

        /// <summary>
        /// Ensures that the TransliteratedName property of the language argument
        /// does not end with whitespace.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void TransliteratedNameDoesNotEndWithWhitespace(ILanguage language)
        {
            Assert.False(Regex.IsMatch(language.TransliteratedName, trailingWhitespacePattern));
        }

        /// <summary>
        /// Ensures that the TransliteratedName property of the testableObject argument
        /// contains no private-use Unicode characters.
        /// </summary>
        /// <param name="testableObject">The object to be tested</param>
        public void TransliteratedNameContainsNoPrivateUseUnicodeCharacters(ILanguage language)
        {
            Assert.False(Regex.IsMatch(language.TransliteratedName, privateUseCharacterPattern));
        }

        /// <summary>
        /// Applies to the language argument
        /// all validations of the TransliteratedName property.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void ValidateTransliteratedName(ILanguage language)
        {
            TransliteratedNameIsNotNull(language);
            TransliteratedNameIsNotEmptyString(language);
            TransliteratedNameDoesNotBeginWithWhitespace(language);
            TransliteratedNameDoesNotEndWithWhitespace(language);
            TransliteratedNameContainsNoPrivateUseUnicodeCharacters(language);
        }

        /* 
         * 
         * 
         * EnglishName validation methods -- same as Name validation methods
         * except includes required first capital except in case of tlhIngan-Hol (Klingon)
         * 
         * 
         * */

        /// <summary>
        /// Ensures that the EnglishName property of the language argument
        /// is not null.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void EnglishNameIsNotNull(ILanguage language)
        {
            Assert.False(language.EnglishName == null);
        }

        /// <summary>
        /// Ensures that the EnglishName property of the language argument
        /// is not the empty string.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void EnglishNameIsNotEmptyString(ILanguage language)
        {
            Assert.True(language.EnglishName.Length > 0);
        }

        /// <summary>
        /// Ensures that the EnglishName property of the language argument
        /// does not begin with whitespace.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void EnglishNameDoesNotBeginWithWhitespace(ILanguage language)
        {
            Assert.False(Regex.IsMatch(language.EnglishName, leadingWhitespacePattern));
        }

        /// <summary>
        /// Ensures that the EnglishName property of the language argument
        /// does not end with whitespace.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void EnglishNameDoesNotEndWithWhitespace(ILanguage language)
        {
            Assert.False(Regex.IsMatch(language.EnglishName, trailingWhitespacePattern));
        }

        /// <summary>
        /// Ensures that the EnglishName property of the language argument
        /// contains no private-use Unicode characters.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void EnglishNameContainsNoPrivateUseUnicodeCharacters(ILanguage language)
        {
            Assert.False(Regex.IsMatch(language.EnglishName, privateUseCharacterPattern));
        }

        /// <summary>
        /// Ensures that the EnglishName property of the language argument
        /// is capitalized unless it is tlhIngan-Hol (Klingon).
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void EnglishNameBeginsWithCapitalLetterUnlessTlhInganHol(ILanguage language)
        {
            Assert.True(
                Regex.IsMatch(language.EnglishName, leadingCapitalPattern)
                || (language.EnglishName == "tlhIngan-Hol")
                );
        }

        /// <summary>
        /// Applies to the language argument
        /// all validations of the EnglishName property.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void ValidateEnglishName(ILanguage language)
        {
            EnglishNameIsNotNull(language);
            EnglishNameIsNotEmptyString(language);
            EnglishNameDoesNotBeginWithWhitespace(language);
            EnglishNameDoesNotEndWithWhitespace(language);
            EnglishNameContainsNoPrivateUseUnicodeCharacters(language);
            EnglishNameBeginsWithCapitalLetterUnlessTlhInganHol(language);
        }

        /* 
         * 
         * 
         * TwoLetterISOCode validation methods
         * 
         * 
         * */

        /// <summary>
        /// Ensures that the TwoLetterISOCode property of the language argument
        /// either is null, or else consists of exactly two lower-case standard English letters.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void TwoLetterISOCodeIsNullOrHasTwoLowerCaseStandardEnglishLetters(ILanguage language)
        {
            if (language.TwoLetterISOCode != null)
                Assert.True(Regex.IsMatch(language.TwoLetterISOCode, "\\A[a-z]{2}\\z"));
        }

        /// <summary>
        /// Applies to the language argument
        /// all validations of the TwoLetterISOLanguageName property.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void ValidateTwoLetterISOLanguageName(ILanguage language)
        {
            TwoLetterISOCodeIsNullOrHasTwoLowerCaseStandardEnglishLetters(language);
        }

        /* 
         * 
         * 
         * ThreeLetterISOCode validation methods
         * 
         * 
         * */

        /// <summary>
        /// Ensures that the ThreeLetterISOCode property of the language argument
        /// consists of exactly three lower-case standard English letters.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void ThreeLetterISOCodeHasThreeLowerCaseStandardEnglishLetters(ILanguage language)
        {
            Assert.True(Regex.IsMatch(language.ThreeLetterISOCode, "\\A[a-z]{3}\\z"));
        }

        /// <summary>
        /// Applies to the language argument
        /// all validations of the ThreeLetterISOLanguageName property.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void ValidateThreeLetterISOCode(ILanguage language)
        {
            ThreeLetterISOCodeHasThreeLowerCaseStandardEnglishLetters(language);
        }

        /* 
         * 
         * 
         * AlternateThreeLetterISOCode validation methods
         * 
         * 
         * */

        /// <summary>
        /// Ensures that the AlternateThreeLetterISOCode property of the language argument
        /// either is null, or else consists of exactly three lower-case standard English letters.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void AlternateThreeLetterISOCodeIsNullOrHasThreeLowerCaseStandardEnglishLetters(ILanguage language)
        {
            if (language.AlternateThreeLetterISOCode != null)
                Assert.True(
                    Regex.IsMatch(language.AlternateThreeLetterISOCode
                        , "\\A[a-zA-Z]{3}\\z")
                    );
        }

        /// <summary>
        /// Applies to the language argument
        /// all validations of the AlternateThreeLetterISOLanguageName property.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void ValidateAlternateThreeLetterISOLanguageName(ILanguage language)
        {
            AlternateThreeLetterISOCodeIsNullOrHasThreeLowerCaseStandardEnglishLetters(language);
        }

        /* 
         * 
         * 
         * PrivateUseCharacterName validation methods
         * 
         * 
         * */

        /// <summary>
        /// Ensures that the PrivateUseCharacterName property of the language argument
        /// either is null, or else contains at least one private-use Unicode character.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void PrivateUseCharacterNameIsNullOrContainsPrivateUseUnicodeCharacter(ILanguage language)
        {
            if (language.PrivateUseCharacterName != null)
                Assert.True( Regex.IsMatch(language.PrivateUseCharacterName, privateUseCharacterPattern ) );
        }

        /// <summary>
        /// Ensures that the PrivateUseCharacterName property of the language argument
        /// does not begin with whitespace.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void PrivateUseCharacterNameDoesNotBeginWithWhitespace(ILanguage language)
        {
            if (language.PrivateUseCharacterName != null)
                Assert.False(Regex.IsMatch(language.PrivateUseCharacterName, leadingWhitespacePattern));
        }

        /// <summary>
        /// Ensures that the PrivateUseCharacterName property of the language argument
        /// does not end with whitespace.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void PrivateUseCharacterNameDoesNotEndWithWhitespace(ILanguage language)
        {
            if (language.PrivateUseCharacterName != null)
                Assert.False(Regex.IsMatch(language.PrivateUseCharacterName, trailingWhitespacePattern));
        }

        /// <summary>
        /// Ensures that if the language argument's PrivateUseCharacterName property is null,
        /// then so is its PrivateUseCharacterNameFontName property.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void LanguageWithoutPrivateUseCharacterNameHasNoNonstandardFontName(ILanguage language)
        {
            if (language.PrivateUseCharacterName == null)
                Assert.True(language.PrivateUseCharacterNameFontName == null);
        }

        /// <summary>
        /// Ensures that if the language argument's PrivateUseCharacterName property is not null,
        /// then neither is its PrivateUseCharacterNameFontName property.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void LanguageWithPrivateUseCharacterNameHasNonstandardFontName(ILanguage language)
        {
            if (language.PrivateUseCharacterName != null)
                Assert.True(language.PrivateUseCharacterNameFontName != null);
        }

        /// <summary>
        /// Applies to the language argument
        /// all validations of the PrivateUseCharacterName property.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void ValidatePrivateUseCharacterName(ILanguage language)
        {
            PrivateUseCharacterNameIsNullOrContainsPrivateUseUnicodeCharacter(language);
            PrivateUseCharacterNameDoesNotBeginWithWhitespace(language);
            PrivateUseCharacterNameDoesNotEndWithWhitespace(language);
            LanguageWithoutPrivateUseCharacterNameHasNoNonstandardFontName(language);
            LanguageWithPrivateUseCharacterNameHasNonstandardFontName(language);
        }

        /* 
         * 
         * 
         * PrivateUseCharacterNameFontName validation methods
         * 
         * 
         * */

        /// <summary>
        /// Ensures that the PrivateUseCharacterNameFontName property of the language argument
        /// is not the empty string.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void PrivateUseCharacterNameFontNameIsNotEmptyString(ILanguage language)
        {
            if (language.PrivateUseCharacterNameFontName != null)
                Assert.True(language.PrivateUseCharacterNameFontName.Length > 0);
        }

        /// <summary>
        /// Ensures that the PrivateUseCharacterNameFontName property of the language argument
        /// does not begin with whitespace.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void PrivateUseCharacterNameFontNameDoesNotBeginWithWhitespace(ILanguage language)
        {
            if (language.PrivateUseCharacterNameFontName != null)
                Assert.False(Regex.IsMatch(language.PrivateUseCharacterNameFontName, leadingWhitespacePattern));
        }

        /// <summary>
        /// Ensures that the PrivateUseCharacterNameFontName property of the language argument
        /// does not end with whitespace.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void PrivateUseCharacterNameFontNameDoesNotEndWithWhitespace(ILanguage language)
        {
            if (language.PrivateUseCharacterNameFontName != null)
                Assert.False(Regex.IsMatch(language.PrivateUseCharacterNameFontName, trailingWhitespacePattern));
        }

        /// <summary>
        /// Ensures that the PrivateUseCharacterNameFontName property of the testableObject argument
        /// contains no private-use Unicode characters.
        /// </summary>
        /// <param name="testableObject">The object to be tested</param>
        public void PrivateUseCharacterNameFontNameContainsNoPrivateUseUnicodeCharacters(ILanguage language)
        {
            if (language.PrivateUseCharacterNameFontName != null)
                Assert.False(Regex.IsMatch(language.PrivateUseCharacterNameFontName, privateUseCharacterPattern));
        }

        /// <summary>
        /// Ensures that if the language argument's PrivateUseCharacterNameFontName property is null,
        /// then so is its PrivateUseCharacterName property.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void LanguageWithoutPrivateUseCharacterNameFontNameHasNoPrivateUseCharacterName(ILanguage language)
        {
            if (language.PrivateUseCharacterNameFontName == null)
                Assert.True(language.PrivateUseCharacterName == null);
        }

        /// <summary>
        /// Ensures that if the language argument's PrivateUseCharacterNameFontName property is not null,
        /// then neither is its PrivateUseCharacterName property.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void LanguageWithPrivateUseCharacterNameFontNameHasPrivateUseCharacterName(ILanguage language)
        {
            if (language.PrivateUseCharacterNameFontName != null)
                Assert.True(language.PrivateUseCharacterName != null);
        }

        /// <summary>
        /// Applies to the language argument
        /// all validations of the PrivateUseCharacterNameFontName property.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void ValidatePrivateUseCharacterNameFontName(ILanguage language)
        {
            PrivateUseCharacterNameFontNameIsNotEmptyString(language);
            PrivateUseCharacterNameFontNameDoesNotBeginWithWhitespace(language);
            PrivateUseCharacterNameFontNameDoesNotEndWithWhitespace(language);
            LanguageWithoutPrivateUseCharacterNameFontNameHasNoPrivateUseCharacterName(language);
            LanguageWithPrivateUseCharacterNameFontNameHasPrivateUseCharacterName(language);
            PrivateUseCharacterNameFontNameContainsNoPrivateUseUnicodeCharacters(language);
        }

        /* 
         * 
         * 
         * All-inclusive validation method
         * 
         * 
         * */

        /// <summary>
        /// Applies to the language argument all validations.
        /// </summary>
        /// <param name="language">The object to be tested</param>
        public void ValidateLanguage(ILanguage language)
        {
            ValidateName(language);
            ValidateEnglishName(language);
            ValidateTwoLetterISOLanguageName(language);
            ValidateThreeLetterISOCode(language);
            ValidateAlternateThreeLetterISOLanguageName(language);
            ValidatePrivateUseCharacterName(language);
            ValidatePrivateUseCharacterNameFontName(language);
        }

        /***************************************************
         *** 
         *** 
         *** TEST CASES
         *** 
         *** 
         ************************************************ */

        /* 
         * 
         * 
         * Name test cases
         * 
         * 
         * */

        /// <summary>
        /// Ensures that the Name property of TestableObject()
        /// is not null.
        /// </summary>
        [TestCase]
        public void NameIsNotNull()
        {
            NameIsNotNull(TestableObject());
        }

        /// <summary>
        /// Ensures that the Name property of TestableObject()
        /// is not the empty string.
        /// </summary>
        [TestCase]
        public void NameIsNotEmptyString()
        {
            NameIsNotEmptyString(TestableObject());
        }

        /// <summary>
        /// Ensures that the Name property of TestableObject()
        /// does not begin with whitespace.
        /// </summary>
        [TestCase]
        public void NameDoesNotBeginWithWhitespace()
        {
            NameDoesNotBeginWithWhitespace(TestableObject());
        }

        /// <summary>
        /// Ensures that the Name property of TestableObject()
        /// does not end with whitespace.
        /// </summary>
        [TestCase]
        public void NameDoesNotEndWithWhitespace()
        {
            NameDoesNotEndWithWhitespace(TestableObject());
        }

        /// <summary>
        /// Ensures that the Name property of TestableObject()
        /// contains no private-use Unicode characters.
        /// </summary>
        [TestCase]
        public void NameContainsNoPrivateUseUnicodeCharacters()
        {
            NameContainsNoPrivateUseUnicodeCharacters(TestableObject());
        }

        /* 
         * 
         * 
         * TransliteratedName test cases
         * 
         * 
         * */

        /// <summary>
        /// Ensures that the TransliteratedName property of TestableObject()
        /// is not null.
        /// </summary>
        [TestCase]
        public void TransliteratedNameIsNotNull()
        {
            TransliteratedNameIsNotNull(TestableObject());
        }

        /// <summary>
        /// Ensures that the TransliteratedName property of TestableObject()
        /// is not the empty string.
        /// </summary>
        [TestCase]
        public void TransliteratedNameIsNotEmptyString()
        {
            TransliteratedNameIsNotEmptyString(TestableObject());
        }

        /// <summary>
        /// Ensures that the TransliteratedName property of TestableObject()
        /// does not begin with whitespace.
        /// </summary>
        [TestCase]
        public void TransliteratedNameDoesNotBeginWithWhitespace()
        {
            TransliteratedNameDoesNotBeginWithWhitespace(TestableObject());
        }

        /// <summary>
        /// Ensures that the TransliteratedName property of TestableObject()
        /// does not end with whitespace.
        /// </summary>
        [TestCase]
        public void TransliteratedNameDoesNotEndWithWhitespace()
        {
            TransliteratedNameDoesNotEndWithWhitespace(TestableObject());
        }

        /// <summary>
        /// Ensures that the TransliteratedName property of TestableObject()
        /// contains no private-use Unicode characters.
        /// </summary>
        [TestCase]
        public void TransliteratedNameContainsNoPrivateUseUnicodeCharacters()
        {
            TransliteratedNameContainsNoPrivateUseUnicodeCharacters(TestableObject());
        }

        /* 
         * 
         * 
         * EnglishName test cases
         * 
         * 
         * */

        /// <summary>
        /// Ensures that the EnglishName property of TestableObject()
        /// is not null.
        /// </summary>
        [TestCase]
        public void EnglishNameIsNotNull()
        {
            EnglishNameIsNotNull(TestableObject());
        }

        /// <summary>
        /// Ensures that the EnglishName property of TestableObject()
        /// is not the empty string.
        /// </summary>
        [TestCase]
        public void EnglishNameIsNotEmptyString()
        {
            EnglishNameIsNotEmptyString(TestableObject());
        }

        /// <summary>
        /// Ensures that the EnglishName property of TestableObject()
        /// does not begin with whitespace.
        /// </summary>
        [TestCase]
        public void EnglishNameDoesNotBeginWithWhitespace()
        {
            EnglishNameDoesNotBeginWithWhitespace(TestableObject());
        }

        /// <summary>
        /// Ensures that the EnglishName property of TestableObject()
        /// does not end with whitespace.
        /// </summary>
        [TestCase]
        public void EnglishNameDoesNotEndWithWhitespace()
        {
            EnglishNameDoesNotEndWithWhitespace(TestableObject());
        }

        /// <summary>
        /// Ensures that the EnglishName property of TestableObject()
        /// contains no private-use Unicode characters.
        /// </summary>
        [TestCase]
        public void EnglishNameContainsNoPrivateUseUnicodeCharacters()
        {
            EnglishNameContainsNoPrivateUseUnicodeCharacters(TestableObject());
        }

        /// <summary>
        /// Ensures that the EnglishName property of TestableObject()
        /// begins with a capital letter unless it is tlhIngan-Hol (Klingon).
        /// </summary>
        [TestCase]
        public void EnglishNameBeginsWithCapitalLetterUnlessTlhInganHol()
        {
            EnglishNameBeginsWithCapitalLetterUnlessTlhInganHol(TestableObject());
        }

        /* 
         * 
         * 
         * TwoLetterISOCode test cases
         * 
         * 
         * */

        /// <summary>
        /// Ensures that the TwoLetterISOCode property of LanguageWithTwoLetterISOLanguageName()
        /// either is null, or else consists of exactly two lower-case standard English letters.
        /// </summary>
        [TestCase]
        public void TwoLetterISOCodeIsNullOrHasTwoLowerCaseStandardEnglishLetters()
        {
            TwoLetterISOCodeIsNullOrHasTwoLowerCaseStandardEnglishLetters(LanguageWithTwoLetterISOLanguageName());
        }

        /* 
         * 
         * 
         * ThreeLetterISOCode test cases
         * 
         * 
         * */

        /// <summary>
        /// Ensures that the ThreeLetterISOCode property of TestableObject()
        /// consists of exactly three lower-case standard English letters.
        /// </summary>
        [TestCase]
        public void ThreeLetterISOCodeHasThreeLowerCaseStandardEnglishLetters()
        {
            ThreeLetterISOCodeHasThreeLowerCaseStandardEnglishLetters(TestableObject());
        }

        /* 
         * 
         * 
         * AlternateThreeLetterISOCode test cases
         * 
         * 
         * */

        /// <summary>
        /// Ensures that the AlternateThreeLetterISOCode property of LanguageWithAlternateThreeLetterISOLanguageName()
        /// either is null, or else consists of exactly three lower-case standard English letters.
        /// </summary>
        [TestCase]
        public void AlternateThreeLetterISOCodeIsNullOrHasThreeLowerCaseStandardEnglishLetters()
        {
            AlternateThreeLetterISOCodeIsNullOrHasThreeLowerCaseStandardEnglishLetters(
                LanguageWithAlternateThreeLetterISOLanguageName()
                );
        }

        /* 
         * 
         * 
         * PrivateUseCharacterName test cases
         * 
         * 
         * */

        /// <summary>
        /// Ensures that the PrivateUseCharacterName property of LanguageWithPrivateUseCharacterName()
        /// either is null, or else contains at least one private-use Unicode character.
        /// </summary>
        [TestCase]
        public void PrivateUseCharacterNameIsNullOrContainsPrivateUseUnicodeCharacter()
        {
            PrivateUseCharacterNameIsNullOrContainsPrivateUseUnicodeCharacter(
                LanguageWithPrivateUseCharacterName()
                );
        }

        /// <summary>
        /// Ensures that the PrivateUseCharacterName property of LanguageWithPrivateUseCharacterName()
        /// does not begin with whitespace.
        /// </summary>
        [TestCase]
        public void PrivateUseCharacterNameDoesNotBeginWithWhitespace()
        {
            PrivateUseCharacterNameDoesNotBeginWithWhitespace(
                LanguageWithPrivateUseCharacterName()
                );
        }

        /// <summary>
        /// Ensures that the PrivateUseCharacterName property of LanguageWithPrivateUseCharacterName()
        /// does not end with whitespace.
        /// </summary>
        [TestCase]
        public void PrivateUseCharacterNameDoesNotEndWithWhitespace()
        {
            PrivateUseCharacterNameDoesNotEndWithWhitespace(
                LanguageWithPrivateUseCharacterName()
                );
        }

        /// <summary>
        /// Ensures that the PrivateUseCharacterNameFontName 
        /// of LanguageWithoutPrivateUseCharacterName() is null.
        /// </summary>
        [TestCase]
        public void LanguageWithoutPrivateUseCharacterNameHasNoNonstandardFontName()
        {
            LanguageWithoutPrivateUseCharacterNameHasNoNonstandardFontName(
                LanguageWithoutPrivateUseCharacterName()
                );
        }

        /// <summary>
        /// Ensures that the PrivateUseCharacterNameFontName 
        /// of LanguageWithPrivateUseCharacterName() is not null.
        /// </summary>
        [TestCase]
        public void LanguageWithPrivateUseCharacterNameHasNonstandardFontName()
        {
            LanguageWithPrivateUseCharacterNameHasNonstandardFontName(
                LanguageWithPrivateUseCharacterName()
                );
        }

        /* 
         * 
         * 
         * PrivateUseCharacterNameFontName test cases
         * 
         * 
         * */

        /// <summary>
        /// Ensures that the PrivateUseCharacterNameFontName property of LanguageWithPrivateUseCharacterNameFontName()
        /// is not the empty string.
        /// </summary>
        [TestCase]
        public void PrivateUseCharacterNameFontNameIsNotEmptyString()
        {
            PrivateUseCharacterNameFontNameIsNotEmptyString(
                LanguageWithPrivateUseCharacterNameFontName()
                );
        }

        /// <summary>
        /// Ensures that the PrivateUseCharacterNameFontName property of LanguageWithPrivateUseCharacterNameFontName()
        /// does not begin with whitespace.
        /// </summary>
        [TestCase]
        public void PrivateUseCharacterNameFontNameDoesNotBeginWithWhitespace()
        {
            PrivateUseCharacterNameFontNameDoesNotBeginWithWhitespace(
                LanguageWithPrivateUseCharacterNameFontName()
                );
        }

        /// <summary>
        /// Ensures that the PrivateUseCharacterNameFontName property of LanguageWithPrivateUseCharacterNameFontName()
        /// does not end with whitespace.
        /// </summary>
        [TestCase]
        public void PrivateUseCharacterNameFontNameDoesNotEndWithWhitespace()
        {
            PrivateUseCharacterNameFontNameDoesNotEndWithWhitespace(
                LanguageWithPrivateUseCharacterNameFontName()
                );
        }

        /// <summary>
        /// Ensures that the PrivateUseCharacterNameFontName property of LanguageWithPrivateUseCharacterNameFontName()
        /// contains no private-use Unicode characters.
        /// </summary>
        [TestCase]
        public void PrivateUseCharacterNameFontNameContainsNoPrivateUseUnicodeCharacters()
        {
            PrivateUseCharacterNameFontNameContainsNoPrivateUseUnicodeCharacters(
                LanguageWithPrivateUseCharacterNameFontName()
                );
        }

        /// <summary>
        /// Ensures that the PrivateUseCharacterName
        /// of LanguageWithoutPrivateUseCharacterNameFontName() is null.
        /// </summary>
        [TestCase]
        public void LanguageWithoutPrivateUseCharacterNameFontNameHasNoPrivateUseCharacterName()
        {
            LanguageWithoutPrivateUseCharacterNameFontNameHasNoPrivateUseCharacterName(
                LanguageWithoutPrivateUseCharacterNameFontName()
                );
        }

        /// <summary>
        /// Ensures that the PrivateUseCharacterName
        /// of LanguageWithPrivateUseCharacterNameFontName() is not null.
        /// </summary>
        [TestCase]
        public void LanguageWithPrivateUseCharacterNameFontNameHasPrivateUseCharacterName()
        {
            LanguageWithPrivateUseCharacterNameFontNameHasPrivateUseCharacterName(
                LanguageWithPrivateUseCharacterNameFontName()
                );
        }

    }

}

