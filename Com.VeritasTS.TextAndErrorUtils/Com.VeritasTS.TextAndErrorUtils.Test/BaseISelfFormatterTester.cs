﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Com.VeritasTS.TextAndErrorUtils;

// This tester is ready for use.

namespace Com.VeritasTS.TextAndErrorUtils.Test
{

    /// <summary>
    /// Base class tester for anything implementing the 
    /// Com.VeritasTS.TextAndErrorUtils.ISelfFormatter interface.
    /// </summary>
    /// <remarks>Anyone implementing the
    /// Com.VeritasTS.TextAndErrorUtils.ISelfFormatter must
    /// ensure that their implementation both fully honors
    /// the interface contract, and is proven to do so by
    /// thorough unit testing. 
    /// <para>Only the normal virtual TestableObject()
    /// method is required; there are no special cases to be tested
    /// at this level.</para>
    /// </remarks>
    public class BaseISelfFormatterTester
    {

        class SelfFormatter : ISelfFormatter
        {
            public string ToString(
                IAudiencePreferenceSet preferenceSet
                , IEnumerable<KeyValuePair<string, ISelfFormatter>> variables
                ) => ToString();
        }

        /// <summary>
        /// Standard general-purpose testable object for classes that implement IAudiencePreferenceSet.
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an object of the class under testing.</remarks>
        /// <returns>Testable IAudiencePreferenceSet suitable for general testing.</returns>
        public virtual ISelfFormatter TestableObject()
        {
            return new SelfFormatter();
        }

        /***************************************************
         *** 
         *** 
         *** VALIDATION METHODS
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Ensures that the ToString method of the selfFormatter argument
        /// does not return null.
        /// </summary>
        /// <param name="preferenceSet">The object to be tested</param>
        public void ToStringIsNotNull(ISelfFormatter selfFormatter)
        {
            Assert.True(selfFormatter.ToString( new InvariantAudiencePreferenceSet(), null ) != null);
        }

        /// <summary>
        /// Applies to the preferenceSet argument all validations.
        /// </summary>
        /// <param name="preferenceSet">The object to be tested</param>
        public void ValidateISelfFormatter(ISelfFormatter selfFormatter)
        {
            ToStringIsNotNull(selfFormatter);
        }

        /***************************************************
         *** 
         *** 
         *** TEST CASES
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Ensures that the ToString method of TestableObject()
        /// does not return null.
        /// </summary>
        [TestCase]
        public void ToStringIsNotNull()
        {
            ToStringIsNotNull(TestableObject());
        }

    }
}
