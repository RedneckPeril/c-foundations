﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Com.VeritasTS.TextAndErrorUtils;
using Com.VeritasTS.TextAndErrorUtils.Inflection;

// This tester is ready for use.

namespace Com.VeritasTS.TextAndErrorUtils.Inflection.Test
{
    [TestFixture]
    public class CasesTester
    {

        private const int COUNT_OF_CASES = 26;

        /// <summary>Validates that CountOfCases has the correct number of cases,
        /// at present (2017 February 28) 26.</summary>
        [TestCase]
        public void CountOfCasesIsCorrect()
        {
            Assert.True(Cases.CountOfCases == COUNT_OF_CASES);
        }

        /// <summary>Validates that AllCases has the correct number of cases.</summary>
        [TestCase]
        public void AllCasesHasCorrectNumberOfCases()
        {
            Assert.True(Cases.AllCases.Count == COUNT_OF_CASES);
        }

        /// <summary>Validates that AllCases generates a new list on each call 
        /// (thus ensuring that it is not exposing the private list member.</summary>
        [TestCase]
        public void SecondCallToAllCasesDoesNotReturnSameListInstance()
        {
            List<ICase> firstCall = Cases.AllCases;
            List<ICase> secondCall = Cases.AllCases;
            Assert.True(firstCall != secondCall);
        }

        /// <summary>Validates that AllCases begins with earliest class name,
        /// which belongs to Ablative.</summary>
        [TestCase]
        public void AllCasesBeginsWithEarliestClassName()
        {
            Assert.True(Cases.AllCases.ElementAt(0) == Cases.Ablative);
        }

        /// <summary>Validates that AllCases ends with last Class name,
        /// which belongs to Vocative.</summary>
        [TestCase]
        public void AllCasesEndsWithLastClassName()
        {
            Assert.True(Cases.AllCases.Last() == Cases.Vocative);
        }

        /// <summary>Validates that ObservableAllCases has the correct number of numbers.</summary>
        [TestCase]
        public void ObservableAllCasesHasCorrectCaseOfCases()
        {
            Assert.True(Cases.ObservableAllCases.Count == COUNT_OF_CASES);
        }

        /// <summary>Validates that ObservableAllCases begins with earliest Class name,
        /// which belongs to Ablative.</summary>
        [TestCase]
        public void ObservableAllCasesBeginsWithEarliestClassName()
        {
            Assert.True(Cases.ObservableAllCases.ElementAt(0) == Cases.Ablative);
        }

        /// <summary>Validates that ObservableAllCases ends with last Class name,
        /// which belongs to Vocative.</summary>
        [TestCase]
        public void ObservableAllCasesEndsWithLastClassName()
        {
            Assert.True(Cases.ObservableAllCases.Last() == Cases.Vocative);
        }



    }
}
