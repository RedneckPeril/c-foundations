﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.VeritasTS.TextAndErrorUtils;
using NUnit.Framework;
using System.Globalization;

// This tester is ready for use.

namespace Com.VeritasTS.TextAndErrorUtils.Test
{



    /// <summary>
    /// Class tester for Com.VeritasTS.TextAndErrorUtils.InvariantAudiencePreferenceSet.
    /// </summary>
    /// <remarks>Anyone implementing the
    /// Com.VeritasTS.TextAndErrorUtils.IAudiencePreferenceSet must
    /// ensure that their implementation both fully honors
    /// the interface contract, and is proven to do so by
    /// thorough unit testing. 
    /// <para>Only the normal virtual TestableObject()
    /// method is required; there are no special cases to be tested
    /// at this level.</para>
    /// </remarks>
    public class InvariantAudiencePreferenceSetTester: BaseIAudiencePreferenceSetTester
    {

        class CultureInfoWithBadLanguageCode : CultureInfo
        {
            public CultureInfoWithBadLanguageCode() : base("en-US") { }
            public override string TwoLetterISOLanguageName { get { return "--"; } }
        }

        /// <summary>
        /// Culture info with bad language code.
        /// </summary>
        /// <remarks>
        /// Used to ensure that attempt to construct with such a CultureInfo, fails.</remarks>
        /// <returns>CultureInfoWithBadLanguageCode object, suitable for error-path testing.</returns>
        public virtual CultureInfo GetCultureInfoWithBadLanguageCode()
        {
            return new CultureInfoWithBadLanguageCode();
        }

        /// <summary>
        /// Culture info with two-letter TwoLetterISOLanguageName.
        /// </summary>
        /// <returns>CultureCulture info with two-letter TwoLetterISOLanguageName.</returns>
        public virtual CultureInfo GetCultureInfoWithTwoLetterLanguageCode()
        {
            // Pick any culture with a two-letter code...say, Iraqi Kurdish.
            return new CultureInfo("ku-Arab-IQ");
        }

        /// <summary>
        /// Culture info with three-letter TwoLetterISOLanguageName.
        /// </summary>
        /// <returns>CultureCulture info with two-letter TwoLetterISOLanguageName.</returns>
        public virtual CultureInfo GetCultureInfoWithThreeLetterLanguageCode()
        {
            // Pick any culture with a three-letter code...say, French Alsatian.
            return new CultureInfo("gsw-FR");
        }

        /// <summary>
        /// IndecipherableLanguageException constructed from null cultureInfo argument.
        /// </summary>
        /// <returns>IndecipherableLanguageException constructed from null cultureInfo argument.</returns>
        public virtual InvariantAudiencePreferenceSet.IndecipherableLanguageException IndecipherableLanguageExceptionConstructedFromNullCultureInfo()
        {
            // Pick any culture with a three-letter code...say, Central Atlas Tamazight.
            return new InvariantAudiencePreferenceSet.IndecipherableLanguageException(null);
        }

        /// <summary>
        /// IndecipherableLanguageException constructed from nonnull cultureInfo argument.
        /// </summary>
        /// <returns>IndecipherableLanguageException constructed from null cultureInfo argument.</returns>
        public virtual 
            Tuple<CultureInfo,InvariantAudiencePreferenceSet.IndecipherableLanguageException> IndecipherableLanguageExceptionConstructedFromNonNullCultureInfo()
        {
            // Pick any culture with a three-letter code...say, Central Atlas Tamazight.
            CultureInfo cultureInfo = GetCultureInfoWithBadLanguageCode();
            InvariantAudiencePreferenceSet.IndecipherableLanguageException e 
                = new InvariantAudiencePreferenceSet.IndecipherableLanguageException( cultureInfo );
            return new Tuple<CultureInfo, InvariantAudiencePreferenceSet.IndecipherableLanguageException>(cultureInfo, e);
        }

        /// <summary>
        /// Testable object generated with default constructor.
        /// </summary>
        /// <returns>Testable InvariantAudiencePreferenceSet generated with default constructor.</returns>
        public virtual IAudiencePreferenceSet DefaultConstructedObject()
        {
            return new InvariantAudiencePreferenceSet();
        }

        /// <summary>
        /// Testable object generated with null constructor cultureInfo argument.
        /// </summary>
        /// <returns>Testable InvariantAudiencePreferenceSet generated with null constructor cultureInfo argument.</returns>
        public virtual IAudiencePreferenceSet ObjectConstructedFromNullCultureInfo()
        {
            return new InvariantAudiencePreferenceSet(null);
        }

        /// <summary>
        /// Testable object generated with constructor cultureInfo argument having a two-letter TwoLetterISOLanguageName.
        /// </summary>
        /// <returns>Testable InvariantAudiencePreferenceSet generated with 
        /// constructor cultureInfo argument having a two-letter TwoLetterISOLanguageName.</returns>
        public virtual Tuple<IAudiencePreferenceSet, CultureInfo> CultureInfoWithTwoLetterLanguageCodeAndResultingObject()
        {
            CultureInfo cultureInfo = GetCultureInfoWithTwoLetterLanguageCode();
            IAudiencePreferenceSet aps = new InvariantAudiencePreferenceSet(cultureInfo);
            return Tuple.Create( aps, cultureInfo );
        }

        /// <summary>
        /// Testable object generated with constructor cultureInfo argument having a two-letter TwoLetterISOLanguageName.
        /// </summary>
        /// <returns>Testable InvariantAudiencePreferenceSet generated with 
        /// constructor cultureInfo argument having a three-letter TwoLetterISOLanguageName.</returns>
        public virtual Tuple<IAudiencePreferenceSet, CultureInfo> CultureInfoWithThreeLetterLanguageCodeAndResultingObject()
        {
            CultureInfo cultureInfo = GetCultureInfoWithThreeLetterLanguageCode();
            IAudiencePreferenceSet aps = new InvariantAudiencePreferenceSet(cultureInfo);
            return Tuple.Create( aps, cultureInfo );
        }


        /// <summary>
        /// Standard general-purpose testable object for classes that implement IAudiencePreferenceSet.
        /// </summary>
        /// <remarks>
        /// Overridden to return an object of the class under testing,
        /// i.e., InvariantAudiencePreferenceSet.</remarks>
        /// <returns>Testable IAudiencePreferenceSet suitable for general testing.</returns>
        public virtual Tuple<IAudiencePreferenceSet, CultureInfo> TestableObjectWithConstructionArgument()
        {
            Tuple<IAudiencePreferenceSet, CultureInfo> retval = CultureInfoWithTwoLetterLanguageCodeAndResultingObject();
            if (retval == null) retval = CultureInfoWithThreeLetterLanguageCodeAndResultingObject();
            if (retval == null) retval = new Tuple<IAudiencePreferenceSet, CultureInfo>( DefaultConstructedObject(), null );
            if (retval == null) retval = new Tuple<IAudiencePreferenceSet, CultureInfo>( ObjectConstructedFromNullCultureInfo(), null); 
            return retval;
        }


        /// <summary>
        /// Standard general-purpose testable object for classes that implement IAudiencePreferenceSet.
        /// </summary>
        /// <remarks>
        /// Overridden to return an object of the class under testing,
        /// i.e., InvariantAudiencePreferenceSet.</remarks>
        /// <returns>Testable IAudiencePreferenceSet suitable for general testing.</returns>
        public override IAudiencePreferenceSet TestableObject()
        {
            return TestableObjectWithConstructionArgument().Item1;
        }

        /***************************************************
         *** 
         *** 
         *** TEST CASES
         *** 
         *** 
         ************************************************ */

        /*
         * 
         * 
         * IndecipherableLanguageException class test cases
         * 
         * 
         */

        /*
         * 
         * 
         * IndecipherableLanguageException test cases
         * 
         * 
         */

        /// <summary>
        /// Ensures that IndecipherableLanguageException constructed
        /// from null cultureInfo argument, has null CultureInfo property.
        /// </summary>
        [TestCase]
        public void IndecipherableLanguageExceptionConstructedFromNullHasNullCultureInfo()
        {
            Assert.True(
                IndecipherableLanguageExceptionConstructedFromNullCultureInfo().CultureInfo == null
                );
        }

        /// <summary>
        /// Ensures that IndecipherableLanguageException constructed
        /// from null cultureInfo argument, has CultureInfo equal to
        /// the cultureInfo argument.
        /// </summary>
        [TestCase]
        public void CultureInfoOfIndecipherableLanguageExceptionEqualsConstructionArgument()
        {
            Tuple<CultureInfo, InvariantAudiencePreferenceSet.IndecipherableLanguageException> tuple
                = IndecipherableLanguageExceptionConstructedFromNonNullCultureInfo();
            Assert.True(
                tuple.Item2.CultureInfo.Equals( tuple.Item1 )
                );
        }

        /// <summary>
        /// Ensures that IndecipherableLanguageException constructed
        /// from null cultureInfo argument, has CultureInfo that does
        /// not point to the original cultureInfo argument.
        /// </summary>
        [TestCase]
        public void CultureInfoOfIndecipherableLanguageExceptionIsNotSameInstanceAsConstructionArgument()
        {
            Tuple<CultureInfo, InvariantAudiencePreferenceSet.IndecipherableLanguageException> tuple
                = IndecipherableLanguageExceptionConstructedFromNonNullCultureInfo();
            Assert.True(
                tuple.Item2.CultureInfo != tuple.Item1
                );
        }

        /// <summary>
        /// Ensures that attempt to construct an InvariantAudiencePreferenceSet from
        /// a CultureInfo whose language code does not match any language in the ISO spec,
        /// fails by throwing an IndeciperableLanguageException.
        /// </summary>
        [TestCase]
        public void AttemptToConstructFromCultureInfoWithBadLanguageCausesIndecipherableLanguageException()
        {
            try
            {
                InvariantAudiencePreferenceSet shouldFail 
                    = new InvariantAudiencePreferenceSet(GetCultureInfoWithBadLanguageCode());
                Assert.True(false);
            } catch (InvariantAudiencePreferenceSet.IndecipherableLanguageException e )
            {
                string discard = e.Message;
            }
        }

        /*
         * 
         * 
         * CultureInfo property test cases
         * 
         * 
         */

        /// <summary>
        /// Ensures that preference set constructed by default constructor
        /// has CultureInfo equal to current culture.
        /// </summary>
        [TestCase]
        public void CultureInfoEqualsCurrentCultureWhenDefaultConstructed()
        {
            Assert.True(
                DefaultConstructedObject().CultureInfo.Equals(CultureInfo.CurrentCulture)
                );
        }

        /// <summary>
        /// Ensures that preference set constructed with caller-specified cultureInfo of null
        /// has CultureInfo equal to current culture.
        /// </summary>
        [TestCase]
        public void CultureInfoEqualsCurrentCultureWhenNullConstructorArgumentSupplied()
        {
            Assert.True(
                ObjectConstructedFromNullCultureInfo().CultureInfo.Equals(CultureInfo.CurrentCulture)
                );
        }

        /// <summary>
        /// Ensures that preference set constructed with caller-specified cultureInfo
        /// has CultureInfo equal to construction argument.
        /// </summary>
        [TestCase]
        public void CultureInfoEqualsConstructionArgumentWhenNonnullArgumentSupplied()
        {
            Tuple<IAudiencePreferenceSet, CultureInfo> tuple = CultureInfoWithTwoLetterLanguageCodeAndResultingObject();
            if (tuple == null) tuple = CultureInfoWithThreeLetterLanguageCodeAndResultingObject();
            if (tuple != null)
                Assert.True(
                    tuple.Item1.CultureInfo.Equals( tuple.Item2 )
                    );
        }

        /// <summary>
        /// Ensures that preference set constructed with caller-specified cultureInfo
        /// has a language that matches the cultureInfo's language when the cultureInfo's 
        /// "two-letter" ISO code is really two letters.
        /// </summary>
        [TestCase]
        public void CultureInfoIsNotConstructionArgumentWhenNonnullArgumentSupplied()
        {
            Tuple<IAudiencePreferenceSet, CultureInfo> tuple = CultureInfoWithTwoLetterLanguageCodeAndResultingObject();
            if (tuple == null) tuple = CultureInfoWithThreeLetterLanguageCodeAndResultingObject();
            if (tuple != null)
                Assert.True(
                    tuple.Item1.CultureInfo != tuple.Item2
                    );
        }

        /*
         * 
         * 
         * Language property test cases
         * 
         * 
         */

        /// <summary>
        /// Ensures that preference set constructed with caller-specified cultureInfo
        /// has a language that matches the cultureInfo's language when the cultureInfo's 
        /// "two-letter" ISO code is actually three letters.
        /// </summary>
        [TestCase]
        public void LanguageMatchesCultureInfoWithTwoLetterCode()
        {
            Tuple<IAudiencePreferenceSet, CultureInfo> tuple = CultureInfoWithTwoLetterLanguageCodeAndResultingObject();
            if (tuple != null)
                Assert.True(
                    tuple.Item1.Language.TwoLetterISOCode.Equals(tuple.Item2.TwoLetterISOLanguageName)
                    );
        }

        /// <summary>
        /// Ensures that preference set constructed with caller-specified cultureInfo
        /// has a language that matches the cultureInfo's language when the cultureInfo's 
        /// "two-letter" ISO code is actually three letters.
        /// </summary>
        [TestCase]
        public void LanguageMatchesCultureInfoWithThreeLetterCode()
        {
            Tuple<IAudiencePreferenceSet, CultureInfo> tuple = CultureInfoWithThreeLetterLanguageCodeAndResultingObject();
            string fromObj = tuple.Item1.Language.ThreeLetterISOCode;
            string fromCI = tuple.Item2.TwoLetterISOLanguageName;
            if (tuple != null)
                Assert.True(
                    tuple.Item1.Language.ThreeLetterISOCode.Equals(tuple.Item2.TwoLetterISOLanguageName)
                    );
        }

    }
}
