﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Com.VeritasTS.TextAndErrorUtils.Inflection;
using Com.VeritasTS.TextAndErrorUtils.Test;

// This tester is ready for use.

namespace Com.VeritasTS.TextAndErrorUtils.Inflection.Test
{

    /// <summary>
    /// Class tester for  
    /// Com.VeritasTS.TextAndErrorUtils.InvariantString.
    /// </summary>
    /// <remarks>
    /// Only the normal virtual TestableObject()
    /// method is required; there are no special cases to be tested
    /// at this level.
    /// </remarks>
    public class NoSuchFormExistsExceptionTester : BaseISelfFormatterTester
    {

        /// <summary>
        /// Standard general-purpose testable object for classes that implement IAudiencePreferenceSet.
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an object of the class under testing.</remarks>
        /// <returns>Testable IAudiencePreferenceSet suitable for general testing.</returns>
        public virtual Tuple< 
            NoSuchFormExistsException, IInflectableLanguageElement, IEnumerable< KeyValuePair< Type,IInflectionCategory>> 
            > TestableObjectWithParams()
        {
            IInflectableLanguageElement currentForm = new IndeclinableNoun("x");
            Dictionary<Type, IInflectionCategory> requestedSettings = new Dictionary<Type, IInflectionCategory>();
            requestedSettings.Add(Cases.Ablative.GetType(), Cases.Ablative);
            NoSuchFormExistsException exc = new NoSuchFormExistsException( currentForm, requestedSettings);
            return new Tuple<NoSuchFormExistsException, IInflectableLanguageElement, IEnumerable<KeyValuePair<Type, IInflectionCategory>>
                (
                exc, currentForm, requestedSettings.AsEnumerable()
                );
        }

        /// <summary>
        /// Standard general-purpose testable object for classes that implement IAudiencePreferenceSet.
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an object of the class under testing.</remarks>
        /// <returns>Testable IAudiencePreferenceSet suitable for general testing.</returns>
        public override ISelfFormatter TestableObject() => TestableObjectWithParams().Item1;

        /***************************************************
         *** 
         *** 
         *** TEST CASES
         *** 
         *** 
         ************************************************ */

        /*
         * 
         * 
         * Constructor test cases
         * 
         * 
         * */
HERE HERE HERE
        /// <summary>
        /// Ensures that an attempt to construct an InvariantString
        /// with a null string results in thrown NullConstructorArgumentException.
        /// </summary>
        [TestCase]
        public void ExceptionConstructedWithBothArgumentsNullHasNullCurrentForm()
        {
            Assert.False(
                (new NoSuchFormExistsExceptionTester( null, null )).CurrentForm == null
                );
        }

        /*
         * 
         * 
         * ToString test cases
         * 
         * 
         * */

        /// <summary>
        /// Ensures that the value of an InvariantString,
        /// as retrieved by the simple ToString method, equals the
        /// value of the original constructor argument.
        /// </summary>
        [TestCase]
        public void SimpleToStringEqualsOriginalConstructorArgument()
        {
            Assert.True(
                (new InvariantString("x")).ToString().Equals("x")
                );
        }

        /// <summary>
        /// Ensures that the ISelfFormatter version of ToString
        /// returns the internal value.
        /// </summary>
        [TestCase]
        public void SelfFormatterToStringReturnsValue()
        {
            Assert.True(
                (new InvariantString("x")).ToString(
                    new InvariantAudiencePreferenceSet(), null
                    ).Equals("x")
                );
        }

        /// <summary>
        /// Ensures that the hash code of an InvariantString is the hash
        /// code of the internal value.
        /// </summary>
        [TestCase]
        public void GetHashCodeEqualsValuesGetHashCode()
        {
            Assert.True(
                (new InvariantString("x")).GetHashCode().Equals("x".GetHashCode())
                );
        }

        /// <summary>
        /// Ensures that CompareTo sorts the InvariantString "a"
        /// before the InvariantString "z".
        /// </summary>
        [TestCase]
        public void AComparesToLessThanZ()
        {
            Assert.True(
                (new InvariantString("a")).CompareTo(new InvariantString("z")) < 0
                );
        }

        /// <summary>
        /// Ensures that CompareTo sorts the InvariantString "a"
        /// before the InvariantString "z".
        /// </summary>
        [TestCase]
        public void ZComparesToGreaterThanA()
        {
            Assert.True(
                (new InvariantString("z")).CompareTo(new InvariantString("a")) > 0
                );
        }

        /// <summary>
        /// Ensures that CompareTo sorts the InvariantString "a"
        /// before the InvariantString "z".
        /// </summary>
        [TestCase]
        public void AComparesToEqualToA()
        {
            Assert.True(
                (new InvariantString("a")).CompareTo(new InvariantString("a")) == 0
                );
        }

    }
}
