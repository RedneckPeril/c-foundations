﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Com.VeritasTS.TextAndErrorUtils;
using Com.VeritasTS.TextAndErrorUtils.Inflection;

// This tester is ready for use.

namespace Com.VeritasTS.TextAndErrorUtils.Inflection.Test
{
    [TestFixture]
    public class NumbersTester
    {

        private const int COUNT_OF_NUMBERS = 4;

        /// <summary>Validates that CountOfNumbers has the correct number of numbers,
        /// at present (2017 February 26) 4.</summary>
        [TestCase]
        public void CountOfNumbersIsCorrect()
        {
            Assert.True(Numbers.CountOfNumbers == COUNT_OF_NUMBERS);
        }

        /// <summary>Validates that AllNumbers has the correct number of numbers.</summary>
        [TestCase]
        public void AllNumbersHasCorrectNumberOfNumbers()
        {
            Assert.True(Numbers.AllNumbers.Count == COUNT_OF_NUMBERS);
        }

        /// <summary>Validates that AllNumbers generates a new list on each call 
        /// (thus ensuring that it is not exposing the private list member.</summary>
        [TestCase]
        public void SecondCallToAllNumbersDoesNotReturnSameListInstance()
        {
            List<INumber> firstCall = Numbers.AllNumbers;
            List<INumber> secondCall = Numbers.AllNumbers;
            Assert.True(firstCall != secondCall);
        }

        /// <summary>Validates that AllNumbers begins with earliest class name,
        /// which belongs to Dual.</summary>
        [TestCase]
        public void AllNumbersBeginsWithEarliestClassName()
        {
            Assert.True(Numbers.AllNumbers.ElementAt(0) == Numbers.Dual);
        }

        /// <summary>Validates that AllNumbers ends with last Class name,
        /// which belongs to Singular.</summary>
        [TestCase]
        public void AllNumbersEndsWithLastClassName()
        {
            Assert.True(Numbers.AllNumbers.Last() == Numbers.Singular);
        }

        /// <summary>Validates that ObservableAllNumbers has the correct number of numbers.</summary>
        [TestCase]
        public void ObservableAllNumbersHasCorrectNumberOfNumbers()
        {
            Assert.True(Numbers.ObservableAllNumbers.Count == COUNT_OF_NUMBERS);
        }

        /// <summary>Validates that ObservableAllNumbers begins with earliest Class name,
        /// which belongs to Dual.</summary>
        [TestCase]
        public void ObservableAllNumbersBeginsWithEarliestClassName()
        {
            Assert.True(Numbers.ObservableAllNumbers.ElementAt(0) == Numbers.Dual);
        }

        /// <summary>Validates that ObservableAllNumbers ends with last Class name,
        /// which belongs to Singular.</summary>
        [TestCase]
        public void ObservableAllNumbersEndsWithLastClassName()
        {
            Assert.True(Numbers.ObservableAllNumbers.Last() == Numbers.Singular);
        }



    }
}
