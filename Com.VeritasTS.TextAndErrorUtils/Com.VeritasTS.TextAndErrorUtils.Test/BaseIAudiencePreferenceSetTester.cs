﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Com.VeritasTS.TextAndErrorUtils;
using System.Globalization;

// This tester is ready for use.

namespace Com.VeritasTS.TextAndErrorUtils.Test
{

    /// <summary>
    /// Base class tester for anything implementing the 
    /// Com.VeritasTS.TextAndErrorUtils.IAudiencePreferenceSet interface.
    /// </summary>
    /// <remarks>Anyone implementing the
    /// Com.VeritasTS.TextAndErrorUtils.IAudiencePreferenceSet must
    /// ensure that their implementation both fully honors
    /// the interface contract, and is proven to do so by
    /// thorough unit testing. 
    /// <para>Only the normal virtual TestableObject()
    /// method is required; there are no special cases to be tested
    /// at this level.</para>
    /// </remarks>
    [TestFixture]
    public class BaseIAudiencePreferenceSetTester
    {

        class AudiencePreferenceSet : IAudiencePreferenceSet
        {
            public ILanguage Language { get { return ISOLanguages.English; } }
            public CultureInfo CultureInfo { get { return CultureInfo.CurrentCulture; } }
            public object GetFormat(Type formatType) { return CultureInfo.GetFormat(formatType); }
        }

        /// <summary>
        /// Standard general-purpose testable object for classes that implement IAudiencePreferenceSet.
        /// </summary>
        /// <remarks>
        /// Must be overridden to return an object of the class under testing.</remarks>
        /// <returns>Testable IAudiencePreferenceSet suitable for general testing.</returns>
        public virtual IAudiencePreferenceSet TestableObject()
        {
            return new AudiencePreferenceSet();
        }

        /***************************************************
         *** 
         *** 
         *** VALIDATION METHODS
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Ensures that the CultureInfo property of the preferenceSet argument
        /// is not null.
        /// </summary>
        /// <param name="preferenceSet">The object to be tested</param>
        public void CultureInfoIsNotNull(IAudiencePreferenceSet preferenceSet)
        {
            Assert.True(preferenceSet.CultureInfo != null);
        }

        /// <summary>
        /// Ensures that the Language property of the preferenceSet argument
        /// is not null.
        /// </summary>
        /// <param name="preferenceSet">The object to be tested</param>
        public void LanguageIsNotNull(IAudiencePreferenceSet preferenceSet)
        {
            Assert.True(preferenceSet.Language != null);
        }

        /// <summary>
        /// Ensures that the GetFormat method of the preferenceSet argument
        /// returns a valid DateTimeFormatInfo when one is requested.
        /// </summary>
        /// <param name="preferenceSet">The object to be tested</param>
        public void GetFormatWorksForDate(IAudiencePreferenceSet preferenceSet)
        {
            DateTimeFormatInfo format = (DateTimeFormatInfo)(preferenceSet.GetFormat(typeof(DateTimeFormatInfo)));
            Assert.True(format != null);
        }

        /// <summary>
        /// Ensures that the GetFormat method of the preferenceSet argument
        /// returns a valid NumberFormatInfo when one is requested.
        /// </summary>
        /// <param name="preferenceSet">The object to be tested</param>
        public void GetFormatWorksForInt(IAudiencePreferenceSet preferenceSet)
        {
            NumberFormatInfo format = (NumberFormatInfo)(preferenceSet.GetFormat(typeof(NumberFormatInfo)));
            Assert.True(format != null);
        }

        /// <summary>
        /// Applies to the preferenceSet argument all validations.
        /// </summary>
        /// <param name="preferenceSet">The object to be tested</param>
        public void ValidateAudiencePreferenceSet(IAudiencePreferenceSet preferenceSet)
        {
            CultureInfoIsNotNull(preferenceSet);
            LanguageIsNotNull(preferenceSet);
        }

        /***************************************************
         *** 
         *** 
         *** TEST CASES
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Ensures that the CultureInfo property of TestableObject()
        /// is not null.
        /// </summary>
        [TestCase]
        public void CultureInfoIsNotNull()
        {
            CultureInfoIsNotNull(TestableObject());
        }

        /// <summary>
        /// Ensures that the Language property of TestableObject()
        /// is not null.
        /// </summary>
        [TestCase]
        public void LanguageIsNotNull()
        {
            LanguageIsNotNull(TestableObject());
        }

        /// <summary>
        /// Ensures that the GetFormat method of TestableObject()
        /// returns a valid DateFormat when called with a date.
        /// </summary>
        [TestCase]
        public void GetFormatWorksForDate()
        {
            GetFormatWorksForDate(TestableObject());
        }

        /// <summary>
        /// Ensures that the GetFormat method of TestableObject()
        /// returns a valid DateFormat when called with an int.
        /// </summary>
        [TestCase]
        public void GetFormatWorksForInt()
        {
            GetFormatWorksForInt(TestableObject());
        }

    }
}
