﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.VeritasTS.TextAndErrorUtils.Inflection;
using Com.VeritasTS.TextAndErrorUtils.Test;
using Com.VeritasTS.CoreUtils;
using NUnit.Framework;

// This tester is NOT ready for use as it requires test code
//   for currentForm, which is now part of the IInflectableLanguageElement
//   specification.

namespace Com.VeritasTS.TextAndErrorUtils.Inflection.Test
{

    [TestFixture]
    public class BaseIInflectableLanguageElementTester
    {

        private class Testable : IInflectableLanguageElement
        {
            public override bool Equals(object obj)
            {
                return true;
            }
            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
            public ISafeMemberOfInvariantOwner GetSafeReference()
            {
                return this;
            }
            public string ToString(
                IAudiencePreferenceSet aps
                , IEnumerable<KeyValuePair<string, ISelfFormatter>> variables
                ) => this.ToString();
            public string CurrentForm { get { return null; } }
        }

        //private BaseISafeMemberOfInvariantOwnerTester safeMemberTester;
        //public BaseISafeMemberOfInvariantOwnerTester GetSafeMemberTester() { return safeMemberTester; }

        //private BaseISelfFormatterTester selfFormatterTester;
        //public BaseISelfFormatterTester SelfFormatterTester { get { return selfFormatterTester; } }

        class SafeMemberTester : BaseISafeMemberOfInvariantOwnerTester
        {
            public override ISafeMemberOfInvariantOwner TestableObject() => new Testable();
        }

        class SelfFormatterTester : BaseISelfFormatterTester
        {
            public override ISelfFormatter TestableObject() => new Testable();
        }

        public virtual ISafeMemberOfInvariantOwner TestableObject() => new Testable();



        /***************************************************
         *** 
         *** 
         *** VALIDATION METHODS
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Ensures that the CultureInfo property of the preferenceSet argument
        /// is not null.
        /// </summary>
        /// <param name="element">The object to be tested</param>
        public void CurrentFormIsNotNull(IInflectableLanguageElement element)
        {
            Assert.True(element.CurrentForm != null);
        }

        /// <summary>
        /// Applies to the preferenceSet argument all validations.
        /// </summary>
        /// <param name="element">The object to be tested</param>
        public void ValidateInflectableLanguageElement(IInflectableLanguageElement element)
        {
            CurrentFormIsNotNull(element);
        }

        /***************************************************
         *** 
         *** 
         *** TEST CASES
         *** 
         *** 
         ************************************************ */

        /// <summary>
        /// Ensures that the CurrentForm property of TestableObject()
        /// is not null.
        /// </summary>
        [TestCase]
        public void CurrentFormIsNotNull()
        {
            CurrentFormIsNotNull((IInflectableLanguageElement)(TestableObject()));
        }

        /// <summary>
        /// Ensures that the Language property of TestableObject()
        /// is not null.
        /// </summary>
        [TestCase]
        public void LanguageIsNotNull()
        {
            LanguageIsNotNull(TestableObject());
        }

        /// <summary>
        /// Ensures that the GetFormat method of TestableObject()
        /// returns a valid DateFormat when called with a date.
        /// </summary>
        [TestCase]
        public void GetFormatWorksForDate()
        {
            GetFormatWorksForDate(TestableObject());
        }

        /// <summary>
        /// Ensures that the GetFormat method of TestableObject()
        /// returns a valid DateFormat when called with an int.
        /// </summary>
        [TestCase]
        public void GetFormatWorksForInt()
        {
            GetFormatWorksForInt(TestableObject());
        }

    }

}
