﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Com.VeritasTS.TextAndErrorUtils;

// This tester is ready for use.

namespace Com.VeritasTS.TextAndErrorUtils.Test
{
    [TestFixture]
    public class NullConstructionArgumentExceptionTester
    {

        /// <summary>Validates that attempt to construct exception with
        /// null paramName parameter results in null paramName.</summary>
        [TestCase]
        public void NullParamNameIsRetainedAsNull()
        {
            Assert.True(new NullConstructorArgumentException(null, "x").ParamName == null); 
        }

        /// <summary>Validates that ParamName returns string equal to original
        /// paramName construction argument.</summary>
        [TestCase]
        public void ParamNameReturnsOriginalParamNameArgument()
        {
            Assert.True( "x".Equals( (new NullConstructorArgumentException("x","y") ).ParamName ) ); 
        }

        /// <summary>Validates that attempt to construct exception with
        /// null className parameter results in empty-string className.</summary>
        [TestCase]
        public void NullClassNameBecomesEmptyString()
        {
            Assert.True( "".Equals( (new NullConstructorArgumentException("x", null)).ClassName ) );
        }

        /// <summary>Validates that ParamName returns string equal to original
        /// paramName construction argument.</summary>
        [TestCase]
        public void ClassNameReturnsOriginalClassNameArgument()
        {
            Assert.True("y".Equals((new NullConstructorArgumentException("x", "y")).ClassName)); 
        }



    }
}
