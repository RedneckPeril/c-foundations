﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

// This tester is ready for use.

namespace Com.VeritasTS.TextAndErrorUtils.Test
{
    [TestFixture]
    class ISOLanguagesTester
    {

        private const int COUNT_OF_LANGUAGES = 565;

        class ISOLanguageTester: BaseILanguageTester
        {
            public override ILanguage LanguageWithTwoLetterISOLanguageName()
            {
                return ISOLanguages.English;
            }
            public override ILanguage LanguageWithAlternateThreeLetterISOLanguageName()
            {
                return ISOLanguages.Armenian;
            }
            public override ILanguage LanguageWithPrivateUseCharacterName()
            {
                return ISOLanguages.Klingon;
            }
            public override ILanguage LanguageWithoutPrivateUseCharacterName()
            {
                return ISOLanguages.English;
            }
            public override ILanguage LanguageWithPrivateUseCharacterNameFontName()
            {
                return ISOLanguages.Klingon;
            }
            public override ILanguage LanguageWithoutPrivateUseCharacterNameFontName()
            {
                return ISOLanguages.English;
            }
        }

        public void ValidateLanguage(ILanguage language)
        {
            new ISOLanguageTester().ValidateLanguage(language);
        }

        /// <summary>Validates that CountOfISOLanguages has the correct number of languages,
        /// at present (2017 February 26) 565.</summary>
        [TestCase]
        public void CountOfLanguagesIsCorrect()
        {
            Assert.True(ISOLanguages.CountOfISOLanguages == COUNT_OF_LANGUAGES);
        }

        /// <summary>Validates that AllISOLanguages has the correct number of languages.</summary>
        [TestCase]
        public void AllLanguagesHasCorrectNumberOfLanguages()
        {
            Assert.True(ISOLanguages.AllISOLanguages.Count == COUNT_OF_LANGUAGES);
        }

        /// <summary>Validates that AllISOLanguages generates a new list on each call 
        /// (thus ensuring that it is not exposing the private list member.</summary>
        [TestCase]
        public void SecondCallToAllLanguagesDoesNotReturnSameListInstance()
        {
            List<ILanguage> firstCall = ISOLanguages.AllISOLanguages;
            List<ILanguage> secondCall = ISOLanguages.AllISOLanguages;
            Assert.True(firstCall != secondCall);
        }

        /// <summary>Validates that AllISOLanguages begins with earliest English name,
        /// which belongs to Abkhazian.</summary>
        [TestCase]
        public void AllLanguagesBeginsWithEarliestEnglishName()
        {
            Assert.True(ISOLanguages.AllISOLanguages.ElementAt(0) == ISOLanguages.Abkhazian);
        }

        /// <summary>Validates that AllISOLanguages ends with last English name,
        /// which belongs to Zuni.</summary>
        [TestCase]
        public void AllLanguagesEndsWithLastEnglishName()
        {
            Assert.True(ISOLanguages.AllISOLanguages.Last() == ISOLanguages.Zuni);
        }

        /// <summary>Validates that ObservableAllISOLanguages has the correct number of languages.</summary>
        [TestCase]
        public void ObservableAllLanguagesHasCorrectNumberOfLanguages()
        {
            Assert.True(ISOLanguages.ObservableAllISOLanguages.Count == COUNT_OF_LANGUAGES);
        }

        /// <summary>Validates that ObservableAllISOLanguages begins with earliest English name,
        /// which belongs to Abkhazian.</summary>
        [TestCase]
        public void ObservableAllLanguagesBeginsWithEarliestEnglishName()
        {
            Assert.True(ISOLanguages.ObservableAllISOLanguages.ElementAt(0) == ISOLanguages.Abkhazian);
        }

        /// <summary>Validates that ObservableAllISOLanguages ends with last English name,
        /// which belongs to Zuni.</summary>
        [TestCase]
        public void ObservableAllLanguagesEndsWithLastEnglishName()
        {
            Assert.True(ISOLanguages.ObservableAllISOLanguages.Last() == ISOLanguages.Zuni);
        }

        /// <summary>Validates that GetLanguageWithTwoLetterISOCode returns null when given null ISO code.</summary>
        [TestCase]
        public void GetLanguageWithTwoLetterISOCodeReturnsNullForNullISOCode()
        {
            Assert.True(ISOLanguages.GetLanguageWithTwoLetterISOCode(null) == null);
        }

        /// <summary>Validates that GetLanguageWithTwoLetterISOCode returns null when given unused ISO code.</summary>
        [TestCase]
        public void GetLanguageWithTwoLetterISOCodeReturnsNullForUnusedISOCode()
        {
            Assert.True(ISOLanguages.GetLanguageWithTwoLetterISOCode("") == null);
        }

        /// <summary>Validates that GetLanguageWithTwoLetterISOCode finds English.</summary>
        [TestCase]
        public void GetLanguageWithTwoLetterISOCodeFindsEnglish()
        {
            Assert.True(
                ISOLanguages.GetLanguageWithTwoLetterISOCode(ISOLanguages.English.TwoLetterISOCode)
                == ISOLanguages.English
                );
        }

        /// <summary>Validates that GetLanguageWithThreeLetterISOCode returns null when given null ISO code.</summary>
        [TestCase]
        public void GetLanguageWithThreeLetterISOCodeReturnsNullForNullISOCode()
        {
            Assert.True(ISOLanguages.GetLanguageWithThreeLetterISOCode(null) == null);
        }

        /// <summary>Validates that GetLanguageWithThreeLetterISOCode returns null when given unused ISO code.</summary>
        [TestCase]
        public void GetLanguageWithThreeLetterISOCodeReturnsNullForUnusedISOCode()
        {
            Assert.True(ISOLanguages.GetLanguageWithThreeLetterISOCode("") == null);
        }

        /// <summary>Validates that GetLanguageWithThreeLetterISOCode finds English correctly.</summary>
        [TestCase]
        public void GetLanguageWithThreeLetterISOCodeFindsEnglish()
        {
            Assert.True(
                ISOLanguages.GetLanguageWithThreeLetterISOCode(ISOLanguages.English.ThreeLetterISOCode)
                == ISOLanguages.English
                );
        }

        /// <summary>Validates that GetLanguageWithThreeLetterISOCode finds Klingon rather than TlhInganHol,
        /// thus validating that when two language have the same code it takes the one that comes
        /// first in alphabetical order by English name.</summary>
        [TestCase]
        public void GetLanguageWithThreeLetterISOCodeFindsKlingonNotTlhInganHol()
        {
            Assert.True(
                ISOLanguages.GetLanguageWithThreeLetterISOCode(ISOLanguages.TlhInganHol.ThreeLetterISOCode)
                == ISOLanguages.Klingon
                );
        }

        /// <summary>Validates that GetLanguageWithThreeLetterISOCode finds Icelandic from its alternate three-letter ISO code.</summary>
        [TestCase]
        public void GetLanguageWithThreeLetterISOCodeFindsIcelandicFromAlternateThreeLetterISOCode()
        {
            Assert.True(
                ISOLanguages.GetLanguageWithThreeLetterISOCode(ISOLanguages.Icelandic.AlternateThreeLetterISOCode)
                == ISOLanguages.Icelandic
                );
        }

        /// <summary>Validates that GetLanguageWithTwoOrThreeLetterISOCode returns null when given null ISO code.</summary>
        [TestCase]
        public void GetLanguageWithTwoOrThreeLetterISOCodeReturnsNullForNullISOCode()
        {
            Assert.True(ISOLanguages.GetLanguageWithTwoOrThreeLetterISOCode(null) == null); 
        }

        /// <summary>Validates that GetLanguageWithTwoOrThreeLetterISOCode returns two-letter match when given two-letter ISO code.</summary>
        [TestCase]
        public void GetLanguageWithTwoOrThreeLetterISOCodeFindsEnglishFromTwoLetterCode()
        {
            Assert.True(
                ISOLanguages.GetLanguageWithTwoOrThreeLetterISOCode(ISOLanguages.English.TwoLetterISOCode) 
                == ISOLanguages.English
                );
        }

        /// <summary>Validates that GetLanguageWithTwoOrThreeLetterISOCode returns three-letter match when given three-letter ISO code.</summary>
        [TestCase]
        public void GetLanguageWithTwoOrThreeLetterISOCodeFindsEnglishFromThreeLetterCode()
        {
            string isoCode = ISOLanguages.English.ThreeLetterISOCode;
            ILanguage retval = ISOLanguages.GetLanguageWithTwoOrThreeLetterISOCode(isoCode);
            Assert.True(
                ISOLanguages.GetLanguageWithTwoOrThreeLetterISOCode(ISOLanguages.English.ThreeLetterISOCode)
                == ISOLanguages.English
                );
        }


        /// <summary>Validates each of the individual languages separately.</summary>
        [TestCase]
        public void ValidateEachLanguage()
        {
            foreach (ILanguage language in ISOLanguages.AllISOLanguages)
                ValidateLanguage(language);
        }



    }
}
