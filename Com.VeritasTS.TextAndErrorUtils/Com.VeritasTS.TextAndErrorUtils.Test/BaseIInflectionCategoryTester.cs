﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Com.VeritasTS.TextAndErrorUtils.Inflection;
using Com.VeritasTS.TextAndErrorUtils.Test;

// This tester is ready for use.

namespace Com.VeritasTS.TextAndErrorUtils.Inflection.Test
{

    /// <summary>
    /// 
    /// Interface contract tester for
    /// Com.VeritasTS.TextAndErrorUtils.IInflectionCategory.
    /// </summary>
    /// <remarks>
    /// Only the normal virtual TestableObject()
    /// method is required; there are no special cases to be tested
    /// at this level. It applies only the standard ISelfFormatter
    /// tests, and has no additional contract provisions.
    /// </remarks>
    [TestFixture]
    public class BaseIInflectionCategoryTester : BaseISelfFormatterTester { }
}
