﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.TextAndErrorUtils
{

    /// <summary>
    /// An ImmutableString is essentially a string wrapped
    /// up so that it can comply with the ISelfFormatter
    /// interface and be used anywhere an ISelfFormatter is
    /// expected. Its value never changes with context; it serves
    /// as a string literal.
    /// </summary>
    public sealed class ImmutableString :ISelfFormatter, IComparable<ImmutableString>
    {
        private string value;

        /// <summary>
        /// Instantiates an ImmutableString from a non-null
        /// string, which string from the point of instantiation
        /// on is the value of the ImmutableString.
        /// </summary>
        /// <list type="table">
        /// <listheader><term>Specification</term><description>Test method in ImmutableStringTester class</description></listheader>
        /// <item>
        ///   <term>Attempt to initialize with null value argument causes NullConstructorArgumentException to be thrown.</term>
        ///   <description>AttemptToConstructFromNullValueCausesNullConstructorArgumentException</description>
        /// </item>
        /// </list>
        /// <param name="value">The string that becomes the ImmutableString's value;
        /// cannot be null. If it is null, a NullConstructorArgumentException is thrown.</param>
        public ImmutableString( string value )
        {
            if (value == null)
                throw new NullConstructorArgumentException( 
                    "value"
                    , "Com.VeritasTS.TextAndErrorUtils.ImmutableString"
                    );
            this.value = value;
        }

        /// <summary>
        /// Standard ToString function: the value of the internally
        /// held string is the value of the object.
        /// </summary>
        /// <remarks>
        /// Test method SimpleToStringEqualsOriginalConstructorArgument</remarks>
        /// <returns></returns>
        public override string ToString() => value;

        /// <summary>
        /// ISelfFormatter version of ToString function, which in this
        /// class simply returns the internal string value no matter
        /// what the preference set says.
        /// </summary>
        /// <remarks>
        /// Test method is SelfFormatterToStringReturnsValue.</remarks>
        /// <returns></returns>
        public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
            => value;

        /// <summary>
        /// Basic Equals method, defined such that the ImmutableString.Equals 
        /// the other object if and only if the internal value Equals the
        /// other object.
        /// </summary>
        /// <remarks>
        /// Test method: ToStringEqualsOriginalConstructorArgument.
        /// </remarks>
        /// <returns></returns>
        public override bool Equals(object other) => value.Equals(other);

        /// <summary>
        /// GetHasCode function, simply forwarded to internal string member.
        /// </summary>
        /// <remarks>
        /// Test method: GetHashCodeEqualsValuesGetHashCode.
        /// </remarks>
        /// <returns></returns>
        public override int GetHashCode() => value.GetHashCode();

        /// <summary>
        /// Class comparator, defined so that ImmutableStrings
        /// sort in the same order that their internal string members do.
        /// </summary>
        /// <remarks>
        /// Test methods: 
        /// <list type="bullet">
        /// <item>AComparesToLessThanZ</item>
        /// <item>ZComparesToGreaterThanA</item>
        /// <item>AComparesToEqualToA</item>
        /// </list>
        /// </remarks>
        /// <returns></returns>
        public int CompareTo( ImmutableString other ) => value.CompareTo( other.value );

    }
}
