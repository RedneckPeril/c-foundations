﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace Com.VeritasTS.TextAndErrorUtils
{

    /// <summary>
    /// Wrapper around CultureInfo class to allow for
    /// Veritas-specific enhancements.
    /// </summary>
    /// <remarks>
    /// The primary intent of this class is to support the 
    /// ISelfFormatter and ITranslatable hierarchies that allow
    /// complex error messages with run-time specific information
    /// to be presented to an individual user in the language
    /// and style he or she prefers.
    /// </remarks>
    public interface IAudiencePreferenceSet : IFormatProvider
    {

        /// <summary>
        /// The CultureInfo object used for most formatting
        /// and translation.
        /// </summary>
        /// <remarks>
        /// This property cannot be null. The test method that validates this is
        /// CultureInfoIsNotNull.
        /// </remarks>
        /// <returns>CultureInfo: The CultureInfo object used for most formatting
        /// and translation.</returns>
        CultureInfo CultureInfo { get; }

        /// <summary>
        /// The language into which ITranslatables are to be translated.
        /// </summary>
        /// <remarks>
        /// This property cannot be null. The test method that validates this is
        /// LanguageIsNotNull.
        /// </remarks>
        /// <returns>ILanguage: The language into which ITranslatables are to be translated.</returns>
        ILanguage Language { get; }

    }

}
