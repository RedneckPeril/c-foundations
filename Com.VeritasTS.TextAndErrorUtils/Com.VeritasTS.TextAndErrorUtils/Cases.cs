﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Globalization;

namespace Com.VeritasTS.TextAndErrorUtils.Inflection
{

    /// <summary>
    /// This class generates a set of singleton,
    /// invariant ICases covering each of the grammatical cases
    /// encountered in most of the world's languages.
    /// </summary>
    /// <remarks>
    /// Along with the set of enumerated values (each of which
    /// is an invariant ICase), there is also a method
    /// Cases producing a List of ICases, as
    /// well as ObservableAllCases that returns an
    /// ObservableCollection rather than a List.
    /// <para>
    /// As this is expected to be a highly stable class, 
    /// we have optimized it for maximal laziness and performance
    /// at run-time. All properties are inlined constants,
    /// each grammatical case is a singleton, and no grammatical case is loaded
    /// if not used.
    /// </para><para>
    /// We do not include all possible cases used in all possible 
    /// languages, since we do not expect to need to support the sixty-four
    /// grammatical cases used by the Tsez language.
    /// </para>
    /// </remarks>
    public sealed class Cases
    {
        Cases() { }

        private static List<ICase> sortedByClassName;

        /// <summary>The "n/a (not applicable)" case, for uninflected languages.</summary>
        public static ICase NotApplicable { get { return NotApplicableGenerator.instance; } }
        /// <summary>The nominative case.</summary>
        public static ICase Nominative { get { return NominativeGenerator.instance; } }
        /// <summary>The accusative case.</summary>
        public static ICase Accusative { get { return AccusativeGenerator.instance; } }
        /// <summary>The instrumental grammatical case.</summary>
        public static ICase Instrumental { get { return InstrumentalGenerator.instance; } }
        /// <summary>The dative grammatical case.</summary>
        public static ICase Dative { get { return DativeGenerator.instance; } }
        /// <summary>The ablative grammatical case.</summary>
        public static ICase Ablative { get { return AblativeGenerator.instance; } }
        /// <summary>The genitive grammatical case.</summary>
        public static ICase Genitive { get { return GenitiveGenerator.instance; } }
        /// <summary>The locative grammatical case.</summary>
        public static ICase Locative { get { return LocativeGenerator.instance; } }
        /// <summary>The vocative grammatical case.</summary>
        public static ICase Vocative { get { return VocativeGenerator.instance; } }
        /// <summary>The lative grammatical case.</summary>
        public static ICase Lative { get { return LativeGenerator.instance; } }
        /// <summary>The absolutive grammatical case.</summary>
        public static ICase Absolutive { get { return AbsolutiveGenerator.instance; } }
        /// <summary>The ergative grammatical case.</summary>
        public static ICase Ergative { get { return ErgativeGenerator.instance; } }
        /// <summary>The instrumental-comitative grammatical case.</summary>
        public static ICase InstrumentalComitative { get { return InstrumentalComitativeGenerator.instance; } }
        /// <summary>The causal-final grammatical case.</summary>
        public static ICase CausalFinal { get { return CausalFinalGenerator.instance; } }
        /// <summary>The translative grammatical case.</summary>
        public static ICase Translative { get { return TranslativeGenerator.instance; } }
        /// <summary>The terminative grammatical case.</summary>
        public static ICase Terminative { get { return TerminativeGenerator.instance; } }
        /// <summary>The essive-formal grammatical case.</summary>
        public static ICase EssiveFormal { get { return EssiveFormalGenerator.instance; } }
        /// <summary>The essive-modal grammatical case.</summary>
        public static ICase EssiveModal { get { return EssiveModalGenerator.instance; } }
        /// <summary>The inessive grammatical case.</summary>
        public static ICase Inessive { get { return InessiveGenerator.instance; } }
        /// <summary>The superessive grammatical case.</summary>
        public static ICase Superessive { get { return SuperessiveGenerator.instance; } }
        /// <summary>The adessive grammatical case.</summary>
        public static ICase Adessive { get { return AdessiveGenerator.instance; } }
        /// <summary>The illative grammatical case.</summary>
        public static ICase Illative { get { return IllativeGenerator.instance; } }
        /// <summary>The sublative grammatical case.</summary>
        public static ICase Sublative { get { return SublativeGenerator.instance; } }
        /// <summary>The allative grammatical case.</summary>
        public static ICase Allative { get { return AllativeGenerator.instance; } }
        /// <summary>The elative grammatical case.</summary>
        public static ICase Elative { get { return ElativeGenerator.instance; } }
        /// <summary>The delative grammatical case.</summary>
        public static ICase Delative { get { return DelativeGenerator.instance; } }

        // Encapsulates reflection logic to access enumeration of static public properties,
        //   which is to say, the list of grammatical cases. 
        static void InitializeSortedByClassName()
        {
            sortedByClassName = null;

            SortedDictionary<string, ICase> sorter = new SortedDictionary<string, ICase>();
            foreach (
                PropertyInfo property in (new Cases()).GetType().GetRuntimeProperties().TakeWhile(
                    property
                    => property.GetMethod.IsStatic
                        & (property.PropertyType == typeof(ICase))
                    )
                )
            {
                ICase theCase = (ICase)(property.GetValue(null, null));
                sorter.Add(theCase.GetType().Name, theCase);
            }

            sortedByClassName = new List<ICase>();
            foreach (KeyValuePair<string, ICase> keyValuePair in sorter)
                sortedByClassName.Add(keyValuePair.Value);

        }

        /// <summary>
        /// A list of all defined grammatical cases, in alphabetical order by type name.
        /// </summary>
        /// <returns>List of all defined grammatical cases,
        /// in alphabetical order by class name.</returns>
        public static List<ICase> AllCases
        {
            get
            {
                if (sortedByClassName == null) InitializeSortedByClassName();
                return new List<ICase>(sortedByClassName);
            }
        }

        /// <summary>
        /// An ObservableCollection of all defined grammatical cases, in alphabetical order by type name.
        /// </summary>
        /// <returns>List of all defined grammatical cases,
        /// in alphabetical order by class name.</returns>
        public static ObservableCollection<ICase> ObservableAllCases
        {
            get
            {
                if (sortedByClassName == null) InitializeSortedByClassName();
                return new ObservableCollection<ICase>(sortedByClassName);
            }
        }

        /// <summary>
        /// The number of defined grammatical cases.
        /// </summary>
        /// <returns>Count of defined grammatical cases.</returns>
        public static int CountOfCases
        {
            get
            {
                if (sortedByClassName == null) InitializeSortedByClassName();
                return sortedByClassName.Count;
            }
        }

        class NotApplicableGenerator
        {
            class NotApplicable : ICase
            {
                public string ToString( IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string,ISelfFormatter>> variables )
                {
                    return "not-applicable case";
                }
            }
            static NotApplicableGenerator() { }
            internal static readonly ICase instance = new NotApplicable();
        }
        class NominativeGenerator
        {
            class Nominative : ICase
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "nominative case";
                }
            }
            static NominativeGenerator() { }
            internal static readonly ICase instance = new Nominative();
        }
        class AccusativeGenerator
        {
            class Accusative : ICase
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "accusative case";
                }
            }
            static AccusativeGenerator() { }
            internal static readonly ICase instance = new Accusative();
        }
        class InstrumentalGenerator
        {
            class Instrumental : ICase
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "instrumental case";
                }
            }
            static InstrumentalGenerator() { }
            internal static readonly ICase instance = new Instrumental();
        }
        class DativeGenerator
        {
            class Dative : ICase
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "dative case";
                }
            }
            static DativeGenerator() { }
            internal static readonly ICase instance = new Dative();
        }
        class AblativeGenerator
        {
            class Ablative : ICase
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "ablative case";
                }
            }
            static AblativeGenerator() { }
            internal static readonly ICase instance = new Ablative();
        }
        class GenitiveGenerator
        {
            class Genitive : ICase
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "genitive case";
                }
            }
            static GenitiveGenerator() { }
            internal static readonly ICase instance = new Genitive();
        }
        class LocativeGenerator
        {
            class Locative : ICase
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "locative case";
                }
            }
            static LocativeGenerator() { }
            internal static readonly ICase instance = new Locative();
        }
        class VocativeGenerator
        {
            class Vocative : ICase
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "vocative case";
                }
            }
            static VocativeGenerator() { }
            internal static readonly ICase instance = new Vocative();
        }
        class LativeGenerator
        {
            class Lative : ICase
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "lative case";
                }
            }
            static LativeGenerator() { }
            internal static readonly ICase instance = new Lative();
        }
        class AbsolutiveGenerator
        {
            class Absolutive : ICase
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "absolutive case";
                }
            }
            static AbsolutiveGenerator() { }
            internal static readonly ICase instance = new Absolutive();
        }
        class ErgativeGenerator
        {
            class Ergative : ICase
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "ergative case";
                }
            }
            static ErgativeGenerator() { }
            internal static readonly ICase instance = new Ergative();
        }
        class InstrumentalComitativeGenerator
        {
            class InstrumentalComitative : ICase
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "instrumental-comitative case";
                }
            }
            static InstrumentalComitativeGenerator() { }
            internal static readonly ICase instance = new InstrumentalComitative();
        }
        class CausalFinalGenerator
        {
            class CausalFinal : ICase
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "causal-final case";
                }
            }
            static CausalFinalGenerator() { }
            internal static readonly ICase instance = new CausalFinal();
        }
        class TranslativeGenerator
        {
            class Translative : ICase
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "translative case";
                }
            }
            static TranslativeGenerator() { }
            internal static readonly ICase instance = new Translative();
        }
        class TerminativeGenerator
        {
            class Terminative : ICase
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "terminative case";
                }
            }
            static TerminativeGenerator() { }
            internal static readonly ICase instance = new Terminative();
        }
        class EssiveFormalGenerator
        {
            class EssiveFormal : ICase
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "essive-formal case";
                }
            }
            static EssiveFormalGenerator() { }
            internal static readonly ICase instance = new EssiveFormal();
        }
        class EssiveModalGenerator
        {
            class EssiveModal : ICase
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "essive-modal case";
                }
            }
            static EssiveModalGenerator() { }
            internal static readonly ICase instance = new EssiveModal();
        }
        class InessiveGenerator
        {
            class Inessive : ICase
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "inessive case";
                }
            }
            static InessiveGenerator() { }
            internal static readonly ICase instance = new Inessive();
        }
        class SuperessiveGenerator
        {
            class Superessive : ICase
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "superessive case";
                }
            }
            static SuperessiveGenerator() { }
            internal static readonly ICase instance = new Superessive();
        }
        class AdessiveGenerator
        {
            class Adessive : ICase
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "adessive case";
                }
            }
            static AdessiveGenerator() { }
            internal static readonly ICase instance = new Adessive();
        }
        class IllativeGenerator
        {
            class Illative : ICase
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "illative case";
                }
            }
            static IllativeGenerator() { }
            internal static readonly ICase instance = new Illative();
        }
        class SublativeGenerator
        {
            class Sublative : ICase
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "sublative case";
                }
            }
            static SublativeGenerator() { }
            internal static readonly ICase instance = new Sublative();
        }
        class AllativeGenerator
        {
            class Allative : ICase
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "allative case";
                }
            }
            static AllativeGenerator() { }
            internal static readonly ICase instance = new Allative();
        }
        class ElativeGenerator
        {
            class Elative : ICase
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "elative case";
                }
            }
            static ElativeGenerator() { }
            internal static readonly ICase instance = new Elative();
        }
        class DelativeGenerator
        {
            class Delative : ICase
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "delative case";
                }
            }
            static DelativeGenerator() { }
            internal static readonly ICase instance = new Delative();
        }

    }
}
