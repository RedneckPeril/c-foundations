﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.TextAndErrorUtils.Inflection
{

    /// <summary>
    /// A noun or noun phrase, for purposes of inflection
    /// of itself and of other dependent words in sentences
    /// </summary>
    public interface INoun: IInflectableLanguageElement
    {

        /// <summary>
        /// The grammatical number of the current form of the noun.
        /// For languages that do not inflect on number,
        /// this is Numbers.NotApplicable.
        /// </summary>
        /// <para>
        /// This method never returns null.
        /// </para>
        INumber Number { get; }

        /// <summary>
        /// The grammatical case of the current form of the noun.
        /// For languages that do not inflect on case,
        /// this is Cases.NotApplicable.
        /// </summary>
        /// <para>
        /// This method never returns null.
        /// </para>
        ICase Case { get; }

        /// <summary>
        /// The grammatical gender of the current form of the noun.
        /// For languages that do not characterize nouns by gender,
        /// this is Gender.NotApplicable.
        /// </summary>
        /// <para>
        /// This method never returns null.
        /// </para>
        IGender Gender { get; }

        /// <summary>
        /// Produces a new form of the noun, in the number, case
        /// and gender requested, if possible.
        /// </summary>
        /// <remarks>
        /// Any parameters that are null have the meaning,
        /// "In this respect, leave yourself whatever you are."
        /// <para>
        /// If no such form exists -- if you ask, for example,
        /// for the Spanish noun fiesta to transform itself
        /// into a plural masculine noun -- the noun simply gets as
        /// close as it can.
        /// </para>
        /// <para>
        /// This method never returns null.
        /// </para>
        /// </remarks>
        INoun TransformedInto(INumber number = null, ICase aCase = null, IGender gender = null);

        /// <summary>
        /// Produces a new form of the noun, in the number, case
        /// and gender requested, if possible.
        /// </summary>
        /// <remarks>
        /// Any parameters that are null have the meaning,
        /// "In this respect, leave yourself whatever you are."
        /// <para>
        /// If no such form exists -- if you ask, for example,
        /// for the Spanish noun fiesta to transform itself
        /// into a plural masculine noun -- a NoSuchFormExists
        /// exception is thrown.
        /// </para>
        /// <para>
        /// This method never returns null, but it may throw 
        /// an exception rather than returning any value at all.
        /// </para>
        /// <exception cref="NoSuchFormExistsException">Thrown when the noun is
        /// unable to make a form of itself that complies with
        /// the caller's instructions.</exception>
        /// </remarks>
        INoun TransformedIntoExactly(INumber number = null, ICase aCase = null, IGender gender = null);

    }
    
}
