﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
// 
using Com.VeritasTS.TextAndErrorUtils;

namespace Com.VeritasTS.TextAndErrorUtils.Inflection
{
    /// <summary>
    /// Exception class for when an IInflectableLanguageElement
    /// is asked to transform itself into a new inflected form
    /// and for some reason can't do it.
    /// </summary>
    /// <remarks>
    /// The class preserves the original form and the description
    /// of the new form into which it could not be transformed.
    /// </remarks>
    public class NoSuchFormExistsException : Exception, ISelfFormatter
    {
        private NoSuchFormExistsException() { }
        private IInflectableLanguageElement currentForm;
        private Dictionary<Type, IInflectionCategory> requestedSettings;

        private static string ConstructorMessage(
            IInflectableLanguageElement currentForm
            , IEnumerable<KeyValuePair<Type, IInflectionCategory>> requestedSettings
            )
        {
            string retval = null;
            if (currentForm == null)
                retval = "A null currentForm was passed into the NoSuchFormExistsException constructor, "
                    + " which should never happen; this is a core programming bug.";
            else if (requestedSettings == null)
                retval = "A null requestedSettings stream was passed into the NoSuchFormExistsException constructor, "
                    + " which should never happen; this is a core programming bug.";
            else
            {
                bool atLeastOneSettingRequested = false;
                retval
                    = "The word "
                    + currentForm
                    + " could not be transformed into ";
                foreach (KeyValuePair<Type, IInflectionCategory> pair in requestedSettings)
                {
                    if (atLeastOneSettingRequested) retval += ", ";
                    else atLeastOneSettingRequested = true;
                    retval += pair.Value.ToString(new ImmutableAudiencePreferenceSet(), null);
                }
                if (atLeastOneSettingRequested) retval += ".";
                else retval = "An empty requestedSettings stream was passed into the NoSuchFormExistsException constructor, "
                    + " which should never happen; this is a core programming bug.";
            }
            return retval;
        }

        /// <summary>
        /// Construct from current form and stream of requested settings.
        /// </summary>
        /// <remarks>
        /// The constructor is robust, allowing either or both parameters to be
        /// null without causing additional exceptions. 
        /// <list type="table">
        /// <listheader><term>Specification</term><description>Test method in NoSuchFormExistsExceptionTester class</description></listheader>
        /// <item>
        ///   <term>Exception is successfully created with two null parameters.</term>
        ///   <description>ExceptionConstructedWithBothArgumentsNullHasNullCurrentForm</description>
        /// </item>
        /// <item>
        ///   <term>Exception is successfully created with null current form and non-null settings.</term>
        ///   <description>ExceptionConstructedWithNullCurrentFormAndNonnullSettingsHasNullCurrentForm</description>
        /// </item>
        /// <item>
        ///   <term>Exception is successfully created with non-null current form null settings.</term>
        ///   <description>ExceptionConstructedWithNonNullCurrentFormAndNullSettingsHasNonNullCurrentForm</description>
        /// </item>
        /// </list>
        /// </remarks>
        /// <param name="currentForm">The current form of the term as it was when
        /// asked to transform itself.</param>
        /// <param name="requestedSettings">The set of settings defining
        /// into what form the term was asked to transform itself.</param>
        public NoSuchFormExistsException(
            IInflectableLanguageElement currentForm
            , IEnumerable<KeyValuePair<Type, IInflectionCategory>> requestedSettings
            ) :
            base(ConstructorMessage(currentForm, requestedSettings))
        {
            this.currentForm = currentForm;
            this.requestedSettings = new Dictionary<Type, IInflectionCategory>();
            foreach (KeyValuePair<Type, IInflectionCategory> pair in requestedSettings)
                this.requestedSettings.Add(pair.Key, pair.Value);
        }


        /// <summary>
        /// The current form of the term as it was when
        /// asked to transform itself.
        /// </summary>
        /// <remarks>
        /// <list type="table">
        /// <listheader><term>Specification</term><description>Test method in NoSuchFormExistsExceptionTester class</description></listheader>
        /// <item>
        ///   <term>CurrentForm property is null if currentForm constructor argument was null.</term>
        ///   <description>ExceptionConstructedWithNullCurrentFormHasNullCurrentForm</description>
        /// </item>
        /// <item>
        ///   <term>CurrentForm property Equals currentForm constructor argument when non-null.</term>
        ///   <description>CurrentFormEqualsCurrentFormConstructorArgument</description>
        /// </item>
        /// </list>
        /// </remarks>
        public IInflectableLanguageElement CurrentForm
        {
            get
            {
                return (IInflectableLanguageElement)(this.currentForm.GetSafeReference());
            }
        }


        /// <summary>
        /// The set of settings defining
        /// into what form the term was asked to transform itself,
        /// as an enumerable.
        /// </summary>
        /// <remarks>
        /// <list type="table">
        /// <listheader><term>Specification</term><description>Test method in NoSuchFormExistsExceptionTester class</description></listheader>
        /// <item>
        ///   <term>RequestedSettings property is null if requestedSettings constructor argument was null.</term>
        ///   <description>ExceptionConstructedWithNullRequestedSettingsHasNullRequestedSettings</description>
        /// </item>
        /// <item>
        ///   <term>RequestedSettings property returns same key-value pairs, in 
        ///   same order, as requestedSettings constructor argument when non-null.</term>
        ///   <description>RequestedSettingsMatchesRequestedSettingsConstructorArgument</description>
        /// </item>
        /// <item>
        ///   <term>RequestedSettings property does not point to same instance
        ///   as requestedSettings constructor argument when non-null.</term>
        ///   <description>RequestedSettingsIsNotRequestedSettingsConstructorArgument</description>
        /// </item>
        /// </list>
        /// </remarks>
        public IEnumerable<KeyValuePair<Type, IInflectionCategory>> RequestedSettings
            => requestedSettings.AsEnumerable();

        /// <summary>
        /// SelfFormatter version of ToString (simply returns ToString() ).
        /// </summary>
        /// <remarks>
        /// Test method: SelfFormatterToStringEqualsToString.
        /// </remarks>
        /// <param name="aps">The audience preference set to which the
        /// object will conform itself.</param>
        /// <param name="variables">The set of variables using which
        /// the object will conform itself.</param>
        public string ToString(
            IAudiencePreferenceSet aps
            , IEnumerable<KeyValuePair<string, ISelfFormatter>> variables
            ) => ToString();

    }
}
