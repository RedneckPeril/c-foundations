﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace Com.VeritasTS.TextAndErrorUtils
{

    /// <summary>
    /// This class generates a set of singleton,
    /// invariant ILangagues covering each of the "languages"
    /// in the ISO 639-2 standard, as a pseudo-enumerated
    /// type.
    /// </summary>
    /// <remarks>
    /// Along with the set of enumerated values (each of which
    /// is an invariant ILanguage), there is also a method
    /// AllISOLanguages producing a List of ILanguages, as
    /// well as ObservableAllISOLanguages that returns an
    /// ObservableCollection rather than a List.
    /// <para>
    /// If desired at a later date we may extend this to cover
    /// everything in ISO 639-3.
    /// </para><para>
    /// As this is expected to be a highly stable class, 
    /// we have optimized it for maximal laziness and performance
    /// at run-time. All properties are inlined constants,
    /// each language is a singleton, and no language is loaded
    /// if not used.
    /// </para><para>
    /// Note, by the way, that the people who designed
    /// Klingon are rather annoying in not really being linguists,
    /// and the people who let them into the ISO standard are
    /// even more annoying for letting them in -- it is the
    /// only language whose English name is not capitalized,
    /// and also the only one whose native name is written
    /// with characters in the private-meaning section of the
    /// Unicode and therefore doesn't display properly without
    /// special code. But we cover it all the same because it 
    /// is in the standard.
    /// </para>
    /// </remarks>
    public sealed class ISOLanguages
    {
        ISOLanguages() { }

        private static List<ILanguage> sortedByEnglishName;

        abstract class ISOLanguage: ILanguage
        {
            abstract public string Name { get; }
            public virtual string TransliteratedName => Name; 
            public virtual string EnglishName => Name; 
            public virtual string TwoLetterISOCode => null;
            abstract public string ThreeLetterISOCode { get; }
            public virtual string AlternateThreeLetterISOCode => null; 
            public virtual string PrivateUseCharacterName => null; 
            public virtual string PrivateUseCharacterNameFontName => null;
        }



        /// <summary>The Abkhazian language.</summary>
        public static ILanguage Abkhazian { get { return AbkhazianLanguage.instance; } }
        /// <summary>The Achinese language.</summary>
        public static ILanguage Achinese { get { return AchineseLanguage.instance; } }
        /// <summary>The Acoli language.</summary>
        public static ILanguage Acoli { get { return AcoliLanguage.instance; } }
        /// <summary>The Adangme language.</summary>
        public static ILanguage Adangme { get { return AdangmeLanguage.instance; } }
        /// <summary>The Adygei language.</summary>
        public static ILanguage Adygei { get { return AdygeiLanguage.instance; } }
        /// <summary>The Adyghe language.</summary>
        public static ILanguage Adyghe { get { return AdygheLanguage.instance; } }
        /// <summary>The Afar language.</summary>
        public static ILanguage Afar { get { return AfarLanguage.instance; } }
        /// <summary>The Afrihili language.</summary>
        public static ILanguage Afrihili { get { return AfrihiliLanguage.instance; } }
        /// <summary>The Afrikaans language.</summary>
        public static ILanguage Afrikaans { get { return AfrikaansLanguage.instance; } }
        /// <summary>The Afro-Asiatic languages language.</summary>
        public static ILanguage AfroAsiaticLanguages { get { return AfroAsiaticLanguagesLanguage.instance; } }
        /// <summary>The Ainu language.</summary>
        public static ILanguage Ainu { get { return AinuLanguage.instance; } }
        /// <summary>The Akan language.</summary>
        public static ILanguage Akan { get { return AkanLanguage.instance; } }
        /// <summary>The Akkadian language.</summary>
        public static ILanguage Akkadian { get { return AkkadianLanguage.instance; } }
        /// <summary>The Albanian language.</summary>
        public static ILanguage Albanian { get { return AlbanianLanguage.instance; } }
        /// <summary>The Alemannic language.</summary>
        public static ILanguage Alemannic { get { return AlemannicLanguage.instance; } }
        /// <summary>The Aleut language.</summary>
        public static ILanguage Aleut { get { return AleutLanguage.instance; } }
        /// <summary>The Algonquian languages language.</summary>
        public static ILanguage AlgonquianLanguages { get { return AlgonquianLanguagesLanguage.instance; } }
        /// <summary>The Alsatian language.</summary>
        public static ILanguage Alsatian { get { return AlsatianLanguage.instance; } }
        /// <summary>The Altaic languages language.</summary>
        public static ILanguage AltaicLanguages { get { return AltaicLanguagesLanguage.instance; } }
        /// <summary>The Amharic language.</summary>
        public static ILanguage Amharic { get { return AmharicLanguage.instance; } }
        /// <summary>The Angika language.</summary>
        public static ILanguage Angika { get { return AngikaLanguage.instance; } }
        /// <summary>The Apache languages language.</summary>
        public static ILanguage ApacheLanguages { get { return ApacheLanguagesLanguage.instance; } }
        /// <summary>The Arabic language.</summary>
        public static ILanguage Arabic { get { return ArabicLanguage.instance; } }
        /// <summary>The Aragonese language.</summary>
        public static ILanguage Aragonese { get { return AragoneseLanguage.instance; } }
        /// <summary>The Arapaho language.</summary>
        public static ILanguage Arapaho { get { return ArapahoLanguage.instance; } }
        /// <summary>The Arawak language.</summary>
        public static ILanguage Arawak { get { return ArawakLanguage.instance; } }
        /// <summary>The Armenian language.</summary>
        public static ILanguage Armenian { get { return ArmenianLanguage.instance; } }
        /// <summary>The Aromanian language.</summary>
        public static ILanguage Aromanian { get { return AromanianLanguage.instance; } }
        /// <summary>The Artificial languages language.</summary>
        public static ILanguage ArtificialLanguages { get { return ArtificialLanguagesLanguage.instance; } }
        /// <summary>The Arumanian language.</summary>
        public static ILanguage Arumanian { get { return ArumanianLanguage.instance; } }
        /// <summary>The Assamese language.</summary>
        public static ILanguage Assamese { get { return AssameseLanguage.instance; } }
        /// <summary>The Asturian language.</summary>
        public static ILanguage Asturian { get { return AsturianLanguage.instance; } }
        /// <summary>The Asturleonese language.</summary>
        public static ILanguage Asturleonese { get { return AsturleoneseLanguage.instance; } }
        /// <summary>The Athapascan languages language.</summary>
        public static ILanguage AthapascanLanguages { get { return AthapascanLanguagesLanguage.instance; } }
        /// <summary>The Australian languages language.</summary>
        public static ILanguage AustralianLanguages { get { return AustralianLanguagesLanguage.instance; } }
        /// <summary>The Austronesian languages language.</summary>
        public static ILanguage AustronesianLanguages { get { return AustronesianLanguagesLanguage.instance; } }
        /// <summary>The Avaric language.</summary>
        public static ILanguage Avaric { get { return AvaricLanguage.instance; } }
        /// <summary>The Avestan language.</summary>
        public static ILanguage Avestan { get { return AvestanLanguage.instance; } }
        /// <summary>The Awadhi language.</summary>
        public static ILanguage Awadhi { get { return AwadhiLanguage.instance; } }
        /// <summary>The Aymara language.</summary>
        public static ILanguage Aymara { get { return AymaraLanguage.instance; } }
        /// <summary>The Azerbaijani language.</summary>
        public static ILanguage Azerbaijani { get { return AzerbaijaniLanguage.instance; } }
        /// <summary>The Bable language.</summary>
        public static ILanguage Bable { get { return BableLanguage.instance; } }
        /// <summary>The Balinese language.</summary>
        public static ILanguage Balinese { get { return BalineseLanguage.instance; } }
        /// <summary>The Baltic languages language.</summary>
        public static ILanguage BalticLanguages { get { return BalticLanguagesLanguage.instance; } }
        /// <summary>The Baluchi language.</summary>
        public static ILanguage Baluchi { get { return BaluchiLanguage.instance; } }
        /// <summary>The Bambara language.</summary>
        public static ILanguage Bambara { get { return BambaraLanguage.instance; } }
        /// <summary>The Bamileke languages language.</summary>
        public static ILanguage BamilekeLanguages { get { return BamilekeLanguagesLanguage.instance; } }
        /// <summary>The Banda languages language.</summary>
        public static ILanguage BandaLanguages { get { return BandaLanguagesLanguage.instance; } }
        /// <summary>The Bantu languages language.</summary>
        public static ILanguage BantuLanguages { get { return BantuLanguagesLanguage.instance; } }
        /// <summary>The Basa language.</summary>
        public static ILanguage Basa { get { return BasaLanguage.instance; } }
        /// <summary>The Bashkir language.</summary>
        public static ILanguage Bashkir { get { return BashkirLanguage.instance; } }
        /// <summary>The Basque language.</summary>
        public static ILanguage Basque { get { return BasqueLanguage.instance; } }
        /// <summary>The Batak languages language.</summary>
        public static ILanguage BatakLanguages { get { return BatakLanguagesLanguage.instance; } }
        /// <summary>The Bedawiyet language.</summary>
        public static ILanguage Bedawiyet { get { return BedawiyetLanguage.instance; } }
        /// <summary>The Beja language.</summary>
        public static ILanguage Beja { get { return BejaLanguage.instance; } }
        /// <summary>The Belarusian language.</summary>
        public static ILanguage Belarusian { get { return BelarusianLanguage.instance; } }
        /// <summary>The Bemba language.</summary>
        public static ILanguage Bemba { get { return BembaLanguage.instance; } }
        /// <summary>The Bengali language.</summary>
        public static ILanguage Bengali { get { return BengaliLanguage.instance; } }
        /// <summary>The Berber languages language.</summary>
        public static ILanguage BerberLanguages { get { return BerberLanguagesLanguage.instance; } }
        /// <summary>The Bhojpuri language.</summary>
        public static ILanguage Bhojpuri { get { return BhojpuriLanguage.instance; } }
        /// <summary>The Bihari languages language.</summary>
        public static ILanguage BihariLanguages { get { return BihariLanguagesLanguage.instance; } }
        /// <summary>The Bikol language.</summary>
        public static ILanguage Bikol { get { return BikolLanguage.instance; } }
        /// <summary>The Bilin language.</summary>
        public static ILanguage Bilin { get { return BilinLanguage.instance; } }
        /// <summary>The Bini language.</summary>
        public static ILanguage Bini { get { return BiniLanguage.instance; } }
        /// <summary>The Bislama language.</summary>
        public static ILanguage Bislama { get { return BislamaLanguage.instance; } }
        /// <summary>The Blin language.</summary>
        public static ILanguage Blin { get { return BlinLanguage.instance; } }
        /// <summary>The Bliss language.</summary>
        public static ILanguage Bliss { get { return BlissLanguage.instance; } }
        /// <summary>The Blissymbolics language.</summary>
        public static ILanguage Blissymbolics { get { return BlissymbolicsLanguage.instance; } }
        /// <summary>The Blissymbols language.</summary>
        public static ILanguage Blissymbols { get { return BlissymbolsLanguage.instance; } }
        /// <summary>The Bokmål, Norwegian language.</summary>
        public static ILanguage BokmalNorwegian { get { return BokmalNorwegianLanguage.instance; } }
        /// <summary>The Bosnian language.</summary>
        public static ILanguage Bosnian { get { return BosnianLanguage.instance; } }
        /// <summary>The Braj language.</summary>
        public static ILanguage Braj { get { return BrajLanguage.instance; } }
        /// <summary>The Breton language.</summary>
        public static ILanguage Breton { get { return BretonLanguage.instance; } }
        /// <summary>The Buginese language.</summary>
        public static ILanguage Buginese { get { return BugineseLanguage.instance; } }
        /// <summary>The Bulgarian language.</summary>
        public static ILanguage Bulgarian { get { return BulgarianLanguage.instance; } }
        /// <summary>The Buriat language.</summary>
        public static ILanguage Buriat { get { return BuriatLanguage.instance; } }
        /// <summary>The Burmese language.</summary>
        public static ILanguage Burmese { get { return BurmeseLanguage.instance; } }
        /// <summary>The Caddo language.</summary>
        public static ILanguage Caddo { get { return CaddoLanguage.instance; } }
        /// <summary>The Castilian language.</summary>
        public static ILanguage Castilian { get { return CastilianLanguage.instance; } }
        /// <summary>The Catalan language.</summary>
        public static ILanguage Catalan { get { return CatalanLanguage.instance; } }
        /// <summary>The Caucasian languages language.</summary>
        public static ILanguage CaucasianLanguages { get { return CaucasianLanguagesLanguage.instance; } }
        /// <summary>The Cebuano language.</summary>
        public static ILanguage Cebuano { get { return CebuanoLanguage.instance; } }
        /// <summary>The Celtic languages language.</summary>
        public static ILanguage CelticLanguages { get { return CelticLanguagesLanguage.instance; } }
        /// <summary>The Central American Indian languages language.</summary>
        public static ILanguage CentralAmericanIndianLanguages { get { return CentralAmericanIndianLanguagesLanguage.instance; } }
        /// <summary>The Central Khmer language.</summary>
        public static ILanguage CentralKhmer { get { return CentralKhmerLanguage.instance; } }
        /// <summary>The Chagatai language.</summary>
        public static ILanguage Chagatai { get { return ChagataiLanguage.instance; } }
        /// <summary>The Chamic languages language.</summary>
        public static ILanguage ChamicLanguages { get { return ChamicLanguagesLanguage.instance; } }
        /// <summary>The Chamorro language.</summary>
        public static ILanguage Chamorro { get { return ChamorroLanguage.instance; } }
        /// <summary>The Chechen language.</summary>
        public static ILanguage Chechen { get { return ChechenLanguage.instance; } }
        /// <summary>The Cherokee language.</summary>
        public static ILanguage Cherokee { get { return CherokeeLanguage.instance; } }
        /// <summary>The Chewa language.</summary>
        public static ILanguage Chewa { get { return ChewaLanguage.instance; } }
        /// <summary>The Cheyenne language.</summary>
        public static ILanguage Cheyenne { get { return CheyenneLanguage.instance; } }
        /// <summary>The Chibcha language.</summary>
        public static ILanguage Chibcha { get { return ChibchaLanguage.instance; } }
        /// <summary>The Chichewa language.</summary>
        public static ILanguage Chichewa { get { return ChichewaLanguage.instance; } }
        /// <summary>The Chinese language.</summary>
        public static ILanguage Chinese { get { return ChineseLanguage.instance; } }
        /// <summary>The Chinook jargon language.</summary>
        public static ILanguage ChinookJargon { get { return ChinookJargonLanguage.instance; } }
        /// <summary>The Chipewyan language.</summary>
        public static ILanguage Chipewyan { get { return ChipewyanLanguage.instance; } }
        /// <summary>The Choctaw language.</summary>
        public static ILanguage Choctaw { get { return ChoctawLanguage.instance; } }
        /// <summary>The Chuang language.</summary>
        public static ILanguage Chuang { get { return ChuangLanguage.instance; } }
        /// <summary>The Church Slavic language.</summary>
        public static ILanguage ChurchSlavic { get { return ChurchSlavicLanguage.instance; } }
        /// <summary>The Church Slavonic language.</summary>
        public static ILanguage ChurchSlavonic { get { return ChurchSlavonicLanguage.instance; } }
        /// <summary>The Chuukese language.</summary>
        public static ILanguage Chuukese { get { return ChuukeseLanguage.instance; } }
        /// <summary>The Chuvash language.</summary>
        public static ILanguage Chuvash { get { return ChuvashLanguage.instance; } }
        /// <summary>The Classical Nepal Bhasa language.</summary>
        public static ILanguage ClassicalNepalBhasa { get { return ClassicalNepalBhasaLanguage.instance; } }
        /// <summary>The Classical Newari language.</summary>
        public static ILanguage ClassicalNewari { get { return ClassicalNewariLanguage.instance; } }
        /// <summary>The Classical Syriac language.</summary>
        public static ILanguage ClassicalSyriac { get { return ClassicalSyriacLanguage.instance; } }
        /// <summary>The Cook Islands Maori language.</summary>
        public static ILanguage CookIslandsMaori { get { return CookIslandsMaoriLanguage.instance; } }
        /// <summary>The Coptic language.</summary>
        public static ILanguage Coptic { get { return CopticLanguage.instance; } }
        /// <summary>The Cornish language.</summary>
        public static ILanguage Cornish { get { return CornishLanguage.instance; } }
        /// <summary>The Corsican language.</summary>
        public static ILanguage Corsican { get { return CorsicanLanguage.instance; } }
        /// <summary>The Cree language.</summary>
        public static ILanguage Cree { get { return CreeLanguage.instance; } }
        /// <summary>The Creek language.</summary>
        public static ILanguage Creek { get { return CreekLanguage.instance; } }
        /// <summary>The Creoles and pidgins language.</summary>
        public static ILanguage CreolesAndPidgins { get { return CreolesAndPidginsLanguage.instance; } }
        /// <summary>The Creoles and pidgins, English based language.</summary>
        public static ILanguage CreolesAndPidginsEnglishBased { get { return CreolesAndPidginsEnglishBasedLanguage.instance; } }
        /// <summary>The Creoles and pidgins, French-based language.</summary>
        public static ILanguage CreolesAndPidginsFrenchBased { get { return CreolesAndPidginsFrenchBasedLanguage.instance; } }
        /// <summary>The Creoles and pidgins, Portuguese-based language.</summary>
        public static ILanguage CreolesAndPidginsPortugueseBased { get { return CreolesAndPidginsPortugueseBasedLanguage.instance; } }
        /// <summary>The Crimean Tatar language.</summary>
        public static ILanguage CrimeanTatar { get { return CrimeanTatarLanguage.instance; } }
        /// <summary>The Crimean Turkish language.</summary>
        public static ILanguage CrimeanTurkish { get { return CrimeanTurkishLanguage.instance; } }
        /// <summary>The Croatian language.</summary>
        public static ILanguage Croatian { get { return CroatianLanguage.instance; } }
        /// <summary>The Cushitic languages language.</summary>
        public static ILanguage CushiticLanguages { get { return CushiticLanguagesLanguage.instance; } }
        /// <summary>The Czech language.</summary>
        public static ILanguage Czech { get { return CzechLanguage.instance; } }
        /// <summary>The Dakota language.</summary>
        public static ILanguage Dakota { get { return DakotaLanguage.instance; } }
        /// <summary>The Danish language.</summary>
        public static ILanguage Danish { get { return DanishLanguage.instance; } }
        /// <summary>The Dargwa language.</summary>
        public static ILanguage Dargwa { get { return DargwaLanguage.instance; } }
        /// <summary>The Delaware language.</summary>
        public static ILanguage Delaware { get { return DelawareLanguage.instance; } }
        /// <summary>The Dene Suline language.</summary>
        public static ILanguage DeneSuline { get { return DeneSulineLanguage.instance; } }
        /// <summary>The Dhivehi language.</summary>
        public static ILanguage Dhivehi { get { return DhivehiLanguage.instance; } }
        /// <summary>The Dimili language.</summary>
        public static ILanguage Dimili { get { return DimiliLanguage.instance; } }
        /// <summary>The Dimli language.</summary>
        public static ILanguage Dimli { get { return DimliLanguage.instance; } }
        /// <summary>The Dinka language.</summary>
        public static ILanguage Dinka { get { return DinkaLanguage.instance; } }
        /// <summary>The Divehi language.</summary>
        public static ILanguage Divehi { get { return DivehiLanguage.instance; } }
        /// <summary>The Dogri language.</summary>
        public static ILanguage Dogri { get { return DogriLanguage.instance; } }
        /// <summary>The Dogrib language.</summary>
        public static ILanguage Dogrib { get { return DogribLanguage.instance; } }
        /// <summary>The Dravidian languages language.</summary>
        public static ILanguage DravidianLanguages { get { return DravidianLanguagesLanguage.instance; } }
        /// <summary>The Duala language.</summary>
        public static ILanguage Duala { get { return DualaLanguage.instance; } }
        /// <summary>The Dutch language.</summary>
        public static ILanguage Dutch { get { return DutchLanguage.instance; } }
        /// <summary>The Dutch, Middle (ca.1050-1350) language.</summary>
        public static ILanguage DutchMiddle { get { return DutchMiddleLanguage.instance; } }
        /// <summary>The Dyula language.</summary>
        public static ILanguage Dyula { get { return DyulaLanguage.instance; } }
        /// <summary>The Dzongkha language.</summary>
        public static ILanguage Dzongkha { get { return DzongkhaLanguage.instance; } }
        /// <summary>The Eastern Frisian language.</summary>
        public static ILanguage EasternFrisian { get { return EasternFrisianLanguage.instance; } }
        /// <summary>The Edo language.</summary>
        public static ILanguage Edo { get { return EdoLanguage.instance; } }
        /// <summary>The Efik language.</summary>
        public static ILanguage Efik { get { return EfikLanguage.instance; } }
        /// <summary>The Egyptian (Ancient) language.</summary>
        public static ILanguage Egyptian { get { return EgyptianLanguage.instance; } }
        /// <summary>The Ekajuk language.</summary>
        public static ILanguage Ekajuk { get { return EkajukLanguage.instance; } }
        /// <summary>The Elamite language.</summary>
        public static ILanguage Elamite { get { return ElamiteLanguage.instance; } }
        /// <summary>The English language.</summary>
        public static ILanguage English { get { return EnglishLanguage.instance; } }
        /// <summary>The English, Middle (1100-1500) language.</summary>
        public static ILanguage EnglishMiddle { get { return EnglishMiddleLanguage.instance; } }
        /// <summary>The English, Old (ca.450-1100) language.</summary>
        public static ILanguage EnglishOld { get { return EnglishOldLanguage.instance; } }
        /// <summary>The Erzya language.</summary>
        public static ILanguage Erzya { get { return ErzyaLanguage.instance; } }
        /// <summary>The Esperanto language.</summary>
        public static ILanguage Esperanto { get { return EsperantoLanguage.instance; } }
        /// <summary>The Estonian language.</summary>
        public static ILanguage Estonian { get { return EstonianLanguage.instance; } }
        /// <summary>The Ewe language.</summary>
        public static ILanguage Ewe { get { return EweLanguage.instance; } }
        /// <summary>The Ewondo language.</summary>
        public static ILanguage Ewondo { get { return EwondoLanguage.instance; } }
        /// <summary>The Fang language.</summary>
        public static ILanguage Fang { get { return FangLanguage.instance; } }
        /// <summary>The Fanti language.</summary>
        public static ILanguage Fanti { get { return FantiLanguage.instance; } }
        /// <summary>The Faroese language.</summary>
        public static ILanguage Faroese { get { return FaroeseLanguage.instance; } }
        /// <summary>The Fijian language.</summary>
        public static ILanguage Fijian { get { return FijianLanguage.instance; } }
        /// <summary>The Filipino language.</summary>
        public static ILanguage Filipino { get { return FilipinoLanguage.instance; } }
        /// <summary>The Finnish language.</summary>
        public static ILanguage Finnish { get { return FinnishLanguage.instance; } }
        /// <summary>The Finno-Ugrian languages language.</summary>
        public static ILanguage FinnoUgrianLanguages { get { return FinnoUgrianLanguagesLanguage.instance; } }
        /// <summary>The Flemish language.</summary>
        public static ILanguage Flemish { get { return FlemishLanguage.instance; } }
        /// <summary>The Fon language.</summary>
        public static ILanguage Fon { get { return FonLanguage.instance; } }
        /// <summary>The French language.</summary>
        public static ILanguage French { get { return FrenchLanguage.instance; } }
        /// <summary>The French, Middle (ca.1400-1600) language.</summary>
        public static ILanguage FrenchMiddle { get { return FrenchMiddleLanguage.instance; } }
        /// <summary>The French, Old (842-ca.1400) language.</summary>
        public static ILanguage FrenchOld { get { return FrenchOldLanguage.instance; } }
        /// <summary>The Friulian language.</summary>
        public static ILanguage Friulian { get { return FriulianLanguage.instance; } }
        /// <summary>The Fulah language.</summary>
        public static ILanguage Fulah { get { return FulahLanguage.instance; } }
        /// <summary>The Ga language.</summary>
        public static ILanguage Ga { get { return GaLanguage.instance; } }
        /// <summary>The Gaelic language.</summary>
        public static ILanguage Gaelic { get { return GaelicLanguage.instance; } }
        /// <summary>The Galibi Carib language.</summary>
        public static ILanguage GalibiCarib { get { return GalibiCaribLanguage.instance; } }
        /// <summary>The Galician language.</summary>
        public static ILanguage Galician { get { return GalicianLanguage.instance; } }
        /// <summary>The Ganda language.</summary>
        public static ILanguage Ganda { get { return GandaLanguage.instance; } }
        /// <summary>The Gayo language.</summary>
        public static ILanguage Gayo { get { return GayoLanguage.instance; } }
        /// <summary>The Gbaya language.</summary>
        public static ILanguage Gbaya { get { return GbayaLanguage.instance; } }
        /// <summary>The Geez language.</summary>
        public static ILanguage Geez { get { return GeezLanguage.instance; } }
        /// <summary>The Georgian language.</summary>
        public static ILanguage Georgian { get { return GeorgianLanguage.instance; } }
        /// <summary>The German language.</summary>
        public static ILanguage German { get { return GermanLanguage.instance; } }
        /// <summary>The German, Low language.</summary>
        public static ILanguage GermanLow { get { return GermanLowLanguage.instance; } }
        /// <summary>The German, Middle High (ca.1050-1500) language.</summary>
        public static ILanguage GermanMiddleHigh { get { return GermanMiddleHighLanguage.instance; } }
        /// <summary>The German, Old High (ca.750-1050) language.</summary>
        public static ILanguage GermanOldHigh { get { return GermanOldHighLanguage.instance; } }
        /// <summary>The Germanic languages language.</summary>
        public static ILanguage GermanicLanguages { get { return GermanicLanguagesLanguage.instance; } }
        /// <summary>The Gikuyu language.</summary>
        public static ILanguage Gikuyu { get { return GikuyuLanguage.instance; } }
        /// <summary>The Gilbertese language.</summary>
        public static ILanguage Gilbertese { get { return GilberteseLanguage.instance; } }
        /// <summary>The Gondi language.</summary>
        public static ILanguage Gondi { get { return GondiLanguage.instance; } }
        /// <summary>The Gorontalo language.</summary>
        public static ILanguage Gorontalo { get { return GorontaloLanguage.instance; } }
        /// <summary>The Gothic language.</summary>
        public static ILanguage Gothic { get { return GothicLanguage.instance; } }
        /// <summary>The Grebo language.</summary>
        public static ILanguage Grebo { get { return GreboLanguage.instance; } }
        /// <summary>The Greek, Ancient (to 1453) language.</summary>
        public static ILanguage GreekAncient { get { return GreekAncientLanguage.instance; } }
        /// <summary>The Greek, Modern (1453-) language.</summary>
        public static ILanguage GreekModern { get { return GreekModernLanguage.instance; } }
        /// <summary>The Greenlandic language.</summary>
        public static ILanguage Greenlandic { get { return GreenlandicLanguage.instance; } }
        /// <summary>The Guarani language.</summary>
        public static ILanguage Guarani { get { return GuaraniLanguage.instance; } }
        /// <summary>The Gujarati language.</summary>
        public static ILanguage Gujarati { get { return GujaratiLanguage.instance; } }
        /// <summary>The Gwich'in language.</summary>
        public static ILanguage Gwichin { get { return GwichinLanguage.instance; } }
        /// <summary>The Haida language.</summary>
        public static ILanguage Haida { get { return HaidaLanguage.instance; } }
        /// <summary>The Haitian language.</summary>
        public static ILanguage Haitian { get { return HaitianLanguage.instance; } }
        /// <summary>The Haitian Creole language.</summary>
        public static ILanguage HaitianCreole { get { return HaitianCreoleLanguage.instance; } }
        /// <summary>The Hausa language.</summary>
        public static ILanguage Hausa { get { return HausaLanguage.instance; } }
        /// <summary>The Hawaiian language.</summary>
        public static ILanguage Hawaiian { get { return HawaiianLanguage.instance; } }
        /// <summary>The Hebrew language.</summary>
        public static ILanguage Hebrew { get { return HebrewLanguage.instance; } }
        /// <summary>The Herero language.</summary>
        public static ILanguage Herero { get { return HereroLanguage.instance; } }
        /// <summary>The Hiligaynon language.</summary>
        public static ILanguage Hiligaynon { get { return HiligaynonLanguage.instance; } }
        /// <summary>The Himachali languages language.</summary>
        public static ILanguage HimachaliLanguages { get { return HimachaliLanguagesLanguage.instance; } }
        /// <summary>The Hindi language.</summary>
        public static ILanguage Hindi { get { return HindiLanguage.instance; } }
        /// <summary>The Hiri Motu language.</summary>
        public static ILanguage HiriMotu { get { return HiriMotuLanguage.instance; } }
        /// <summary>The Hittite language.</summary>
        public static ILanguage Hittite { get { return HittiteLanguage.instance; } }
        /// <summary>The Hmong language.</summary>
        public static ILanguage Hmong { get { return HmongLanguage.instance; } }
        /// <summary>The Hungarian language.</summary>
        public static ILanguage Hungarian { get { return HungarianLanguage.instance; } }
        /// <summary>The Hupa language.</summary>
        public static ILanguage Hupa { get { return HupaLanguage.instance; } }
        /// <summary>The Iban language.</summary>
        public static ILanguage Iban { get { return IbanLanguage.instance; } }
        /// <summary>The Icelandic language.</summary>
        public static ILanguage Icelandic { get { return IcelandicLanguage.instance; } }
        /// <summary>The Ido language.</summary>
        public static ILanguage Ido { get { return IdoLanguage.instance; } }
        /// <summary>The Igbo language.</summary>
        public static ILanguage Igbo { get { return IgboLanguage.instance; } }
        /// <summary>The Ijo languages language.</summary>
        public static ILanguage IjoLanguages { get { return IjoLanguagesLanguage.instance; } }
        /// <summary>The Iloko language.</summary>
        public static ILanguage Iloko { get { return IlokoLanguage.instance; } }
        /// <summary>The Imperial Aramaic (700-300 BCE) language.</summary>
        public static ILanguage ImperialAramaic { get { return ImperialAramaicLanguage.instance; } }
        /// <summary>The Inari Sami language.</summary>
        public static ILanguage InariSami { get { return InariSamiLanguage.instance; } }
        /// <summary>The Indic languages language.</summary>
        public static ILanguage IndicLanguages { get { return IndicLanguagesLanguage.instance; } }
        /// <summary>The Indo-European languages language.</summary>
        public static ILanguage IndoEuropeanLanguages { get { return IndoEuropeanLanguagesLanguage.instance; } }
        /// <summary>The Indonesian language.</summary>
        public static ILanguage Indonesian { get { return IndonesianLanguage.instance; } }
        /// <summary>The Ingush language.</summary>
        public static ILanguage Ingush { get { return IngushLanguage.instance; } }
        /// <summary>The Interlingua (International Auxiliary Language Association) language.</summary>
        public static ILanguage Interlingua { get { return InterlinguaLanguage.instance; } }
        /// <summary>The Interlingue language.</summary>
        public static ILanguage Interlingue { get { return InterlingueLanguage.instance; } }
        /// <summary>The Inuktitut language.</summary>
        public static ILanguage Inuktitut { get { return InuktitutLanguage.instance; } }
        /// <summary>The Inupiaq language.</summary>
        public static ILanguage Inupiaq { get { return InupiaqLanguage.instance; } }
        /// <summary>The Iranian languages language.</summary>
        public static ILanguage IranianLanguages { get { return IranianLanguagesLanguage.instance; } }
        /// <summary>The Irish language.</summary>
        public static ILanguage Irish { get { return IrishLanguage.instance; } }
        /// <summary>The Irish, Middle (900-1200) language.</summary>
        public static ILanguage IrishMiddle { get { return IrishMiddleLanguage.instance; } }
        /// <summary>The Irish, Old (to 900) language.</summary>
        public static ILanguage IrishOld { get { return IrishOldLanguage.instance; } }
        /// <summary>The Iroquoian languages language.</summary>
        public static ILanguage IroquoianLanguages { get { return IroquoianLanguagesLanguage.instance; } }
        /// <summary>The Italian language.</summary>
        public static ILanguage Italian { get { return ItalianLanguage.instance; } }
        /// <summary>The Japanese language.</summary>
        public static ILanguage Japanese { get { return JapaneseLanguage.instance; } }
        /// <summary>The Javanese language.</summary>
        public static ILanguage Javanese { get { return JavaneseLanguage.instance; } }
        /// <summary>The Jingpho language.</summary>
        public static ILanguage Jingpho { get { return JingphoLanguage.instance; } }
        /// <summary>The Judeo-Arabic language.</summary>
        public static ILanguage JudeoArabic { get { return JudeoArabicLanguage.instance; } }
        /// <summary>The Judeo-Persian language.</summary>
        public static ILanguage JudeoPersian { get { return JudeoPersianLanguage.instance; } }
        /// <summary>The Kabardian language.</summary>
        public static ILanguage Kabardian { get { return KabardianLanguage.instance; } }
        /// <summary>The Kabyle language.</summary>
        public static ILanguage Kabyle { get { return KabyleLanguage.instance; } }
        /// <summary>The Kachin language.</summary>
        public static ILanguage Kachin { get { return KachinLanguage.instance; } }
        /// <summary>The Kalaallisut language.</summary>
        public static ILanguage Kalaallisut { get { return KalaallisutLanguage.instance; } }
        /// <summary>The Kalmyk language.</summary>
        public static ILanguage Kalmyk { get { return KalmykLanguage.instance; } }
        /// <summary>The Kamba language.</summary>
        public static ILanguage Kamba { get { return KambaLanguage.instance; } }
        /// <summary>The Kannada language.</summary>
        public static ILanguage Kannada { get { return KannadaLanguage.instance; } }
        /// <summary>The Kanuri language.</summary>
        public static ILanguage Kanuri { get { return KanuriLanguage.instance; } }
        /// <summary>The Kapampangan language.</summary>
        public static ILanguage Kapampangan { get { return KapampanganLanguage.instance; } }
        /// <summary>The Kara-Kalpak language.</summary>
        public static ILanguage KaraKalpak { get { return KaraKalpakLanguage.instance; } }
        /// <summary>The Karachay-Balkar language.</summary>
        public static ILanguage KarachayBalkar { get { return KarachayBalkarLanguage.instance; } }
        /// <summary>The Karelian language.</summary>
        public static ILanguage Karelian { get { return KarelianLanguage.instance; } }
        /// <summary>The Karen languages language.</summary>
        public static ILanguage KarenLanguages { get { return KarenLanguagesLanguage.instance; } }
        /// <summary>The Kashmiri language.</summary>
        public static ILanguage Kashmiri { get { return KashmiriLanguage.instance; } }
        /// <summary>The Kashubian language.</summary>
        public static ILanguage Kashubian { get { return KashubianLanguage.instance; } }
        /// <summary>The Kawi language.</summary>
        public static ILanguage Kawi { get { return KawiLanguage.instance; } }
        /// <summary>The Kazakh language.</summary>
        public static ILanguage Kazakh { get { return KazakhLanguage.instance; } }
        /// <summary>The Khasi language.</summary>
        public static ILanguage Khasi { get { return KhasiLanguage.instance; } }
        /// <summary>The Khoisan languages language.</summary>
        public static ILanguage KhoisanLanguages { get { return KhoisanLanguagesLanguage.instance; } }
        /// <summary>The Khotanese language.</summary>
        public static ILanguage Khotanese { get { return KhotaneseLanguage.instance; } }
        /// <summary>The Kikuyu language.</summary>
        public static ILanguage Kikuyu { get { return KikuyuLanguage.instance; } }
        /// <summary>The Kimbundu language.</summary>
        public static ILanguage Kimbundu { get { return KimbunduLanguage.instance; } }
        /// <summary>The Kinyarwanda language.</summary>
        public static ILanguage Kinyarwanda { get { return KinyarwandaLanguage.instance; } }
        /// <summary>The Kirdki language.</summary>
        public static ILanguage Kirdki { get { return KirdkiLanguage.instance; } }
        /// <summary>The Kirghiz language.</summary>
        public static ILanguage Kirghiz { get { return KirghizLanguage.instance; } }
        /// <summary>The Kirmanjki language.</summary>
        public static ILanguage Kirmanjki { get { return KirmanjkiLanguage.instance; } }
        /// <summary>The Klingon language.</summary>
        public static ILanguage Klingon { get { return KlingonLanguage.instance; } }
        /// <summary>The Komi language.</summary>
        public static ILanguage Komi { get { return KomiLanguage.instance; } }
        /// <summary>The Kongo language.</summary>
        public static ILanguage Kongo { get { return KongoLanguage.instance; } }
        /// <summary>The Konkani language.</summary>
        public static ILanguage Konkani { get { return KonkaniLanguage.instance; } }
        /// <summary>The Korean language.</summary>
        public static ILanguage Korean { get { return KoreanLanguage.instance; } }
        /// <summary>The Kosraean language.</summary>
        public static ILanguage Kosraean { get { return KosraeanLanguage.instance; } }
        /// <summary>The Kpelle language.</summary>
        public static ILanguage Kpelle { get { return KpelleLanguage.instance; } }
        /// <summary>The Kru languages language.</summary>
        public static ILanguage KruLanguages { get { return KruLanguagesLanguage.instance; } }
        /// <summary>The Kuanyama language.</summary>
        public static ILanguage Kuanyama { get { return KuanyamaLanguage.instance; } }
        /// <summary>The Kumyk language.</summary>
        public static ILanguage Kumyk { get { return KumykLanguage.instance; } }
        /// <summary>The Kurdish language.</summary>
        public static ILanguage Kurdish { get { return KurdishLanguage.instance; } }
        /// <summary>The Kurukh language.</summary>
        public static ILanguage Kurukh { get { return KurukhLanguage.instance; } }
        /// <summary>The Kutenai language.</summary>
        public static ILanguage Kutenai { get { return KutenaiLanguage.instance; } }
        /// <summary>The Kwanyama language.</summary>
        public static ILanguage Kwanyama { get { return KwanyamaLanguage.instance; } }
        /// <summary>The Kyrgyz language.</summary>
        public static ILanguage Kyrgyz { get { return KyrgyzLanguage.instance; } }
        /// <summary>The Ladino language.</summary>
        public static ILanguage Ladino { get { return LadinoLanguage.instance; } }
        /// <summary>The Lahnda language.</summary>
        public static ILanguage Lahnda { get { return LahndaLanguage.instance; } }
        /// <summary>The Lamba language.</summary>
        public static ILanguage Lamba { get { return LambaLanguage.instance; } }
        /// <summary>The Land Dayak languages language.</summary>
        public static ILanguage LandDayakLanguages { get { return LandDayakLanguagesLanguage.instance; } }
        /// <summary>The Lao language.</summary>
        public static ILanguage Lao { get { return LaoLanguage.instance; } }
        /// <summary>The Latin language.</summary>
        public static ILanguage Latin { get { return LatinLanguage.instance; } }
        /// <summary>The Latvian language.</summary>
        public static ILanguage Latvian { get { return LatvianLanguage.instance; } }
        /// <summary>The Leonese language.</summary>
        public static ILanguage Leonese { get { return LeoneseLanguage.instance; } }
        /// <summary>The Letzeburgesch language.</summary>
        public static ILanguage Letzeburgesch { get { return LetzeburgeschLanguage.instance; } }
        /// <summary>The Lezghian language.</summary>
        public static ILanguage Lezghian { get { return LezghianLanguage.instance; } }
        /// <summary>The Limburgan language.</summary>
        public static ILanguage Limburgan { get { return LimburganLanguage.instance; } }
        /// <summary>The Limburger language.</summary>
        public static ILanguage Limburger { get { return LimburgerLanguage.instance; } }
        /// <summary>The Limburgish language.</summary>
        public static ILanguage Limburgish { get { return LimburgishLanguage.instance; } }
        /// <summary>The Lingala language.</summary>
        public static ILanguage Lingala { get { return LingalaLanguage.instance; } }
        /// <summary>The Lithuanian language.</summary>
        public static ILanguage Lithuanian { get { return LithuanianLanguage.instance; } }
        /// <summary>The Lojban language.</summary>
        public static ILanguage Lojban { get { return LojbanLanguage.instance; } }
        /// <summary>The Low German language.</summary>
        public static ILanguage LowGerman { get { return LowGermanLanguage.instance; } }
        /// <summary>The Low Saxon language.</summary>
        public static ILanguage LowSaxon { get { return LowSaxonLanguage.instance; } }
        /// <summary>The Lower Sorbian language.</summary>
        public static ILanguage LowerSorbian { get { return LowerSorbianLanguage.instance; } }
        /// <summary>The Lozi language.</summary>
        public static ILanguage Lozi { get { return LoziLanguage.instance; } }
        /// <summary>The Luba-Katanga language.</summary>
        public static ILanguage LubaKatanga { get { return LubaKatangaLanguage.instance; } }
        /// <summary>The Luba-Lulua language.</summary>
        public static ILanguage LubaLulua { get { return LubaLuluaLanguage.instance; } }
        /// <summary>The Luiseno language.</summary>
        public static ILanguage Luiseno { get { return LuisenoLanguage.instance; } }
        /// <summary>The Lule Sami language.</summary>
        public static ILanguage LuleSami { get { return LuleSamiLanguage.instance; } }
        /// <summary>The Lunda language.</summary>
        public static ILanguage Lunda { get { return LundaLanguage.instance; } }
        /// <summary>The Luo (Kenya and Tanzania) language.</summary>
        public static ILanguage Luo { get { return LuoLanguage.instance; } }
        /// <summary>The Lushai language.</summary>
        public static ILanguage Lushai { get { return LushaiLanguage.instance; } }
        /// <summary>The Luxembourgish language.</summary>
        public static ILanguage Luxembourgish { get { return LuxembourgishLanguage.instance; } }
        /// <summary>The Macedo-Romanian language.</summary>
        public static ILanguage MacedoRomanian { get { return MacedoRomanianLanguage.instance; } }
        /// <summary>The Macedonian language.</summary>
        public static ILanguage Macedonian { get { return MacedonianLanguage.instance; } }
        /// <summary>The Madurese language.</summary>
        public static ILanguage Madurese { get { return MadureseLanguage.instance; } }
        /// <summary>The Magahi language.</summary>
        public static ILanguage Magahi { get { return MagahiLanguage.instance; } }
        /// <summary>The Maithili language.</summary>
        public static ILanguage Maithili { get { return MaithiliLanguage.instance; } }
        /// <summary>The Makasar language.</summary>
        public static ILanguage Makasar { get { return MakasarLanguage.instance; } }
        /// <summary>The Malagasy language.</summary>
        public static ILanguage Malagasy { get { return MalagasyLanguage.instance; } }
        /// <summary>The Malay language.</summary>
        public static ILanguage Malay { get { return MalayLanguage.instance; } }
        /// <summary>The Malayalam language.</summary>
        public static ILanguage Malayalam { get { return MalayalamLanguage.instance; } }
        /// <summary>The Maldivian language.</summary>
        public static ILanguage Maldivian { get { return MaldivianLanguage.instance; } }
        /// <summary>The Maltese language.</summary>
        public static ILanguage Maltese { get { return MalteseLanguage.instance; } }
        /// <summary>The Manchu language.</summary>
        public static ILanguage Manchu { get { return ManchuLanguage.instance; } }
        /// <summary>The Mandar language.</summary>
        public static ILanguage Mandar { get { return MandarLanguage.instance; } }
        /// <summary>The Mandingo language.</summary>
        public static ILanguage Mandingo { get { return MandingoLanguage.instance; } }
        /// <summary>The Manipuri language.</summary>
        public static ILanguage Manipuri { get { return ManipuriLanguage.instance; } }
        /// <summary>The Manobo languages language.</summary>
        public static ILanguage ManoboLanguages { get { return ManoboLanguagesLanguage.instance; } }
        /// <summary>The Manx language.</summary>
        public static ILanguage Manx { get { return ManxLanguage.instance; } }
        /// <summary>The Maori language.</summary>
        public static ILanguage Maori { get { return MaoriLanguage.instance; } }
        /// <summary>The Mapuche language.</summary>
        public static ILanguage Mapuche { get { return MapucheLanguage.instance; } }
        /// <summary>The Mapudungun language.</summary>
        public static ILanguage Mapudungun { get { return MapudungunLanguage.instance; } }
        /// <summary>The Marathi language.</summary>
        public static ILanguage Marathi { get { return MarathiLanguage.instance; } }
        /// <summary>The Mari language.</summary>
        public static ILanguage Mari { get { return MariLanguage.instance; } }
        /// <summary>The Marshallese language.</summary>
        public static ILanguage Marshallese { get { return MarshalleseLanguage.instance; } }
        /// <summary>The Marwari language.</summary>
        public static ILanguage Marwari { get { return MarwariLanguage.instance; } }
        /// <summary>The Masai language.</summary>
        public static ILanguage Masai { get { return MasaiLanguage.instance; } }
        /// <summary>The Mayan languages language.</summary>
        public static ILanguage MayanLanguages { get { return MayanLanguagesLanguage.instance; } }
        /// <summary>The Mende language.</summary>
        public static ILanguage Mende { get { return MendeLanguage.instance; } }
        /// <summary>The Mi'kmaq language.</summary>
        public static ILanguage Mikmaq { get { return MikmaqLanguage.instance; } }
        /// <summary>The Micmac language.</summary>
        public static ILanguage Micmac { get { return MicmacLanguage.instance; } }
        /// <summary>The Minangkabau language.</summary>
        public static ILanguage Minangkabau { get { return MinangkabauLanguage.instance; } }
        /// <summary>The Mirandese language.</summary>
        public static ILanguage Mirandese { get { return MirandeseLanguage.instance; } }
        /// <summary>The Mohawk language.</summary>
        public static ILanguage Mohawk { get { return MohawkLanguage.instance; } }
        /// <summary>The Moksha language.</summary>
        public static ILanguage Moksha { get { return MokshaLanguage.instance; } }
        /// <summary>The Moldavian language.</summary>
        public static ILanguage Moldavian { get { return MoldavianLanguage.instance; } }
        /// <summary>The Moldovan language.</summary>
        public static ILanguage Moldovan { get { return MoldovanLanguage.instance; } }
        /// <summary>The Mon-Khmer languages language.</summary>
        public static ILanguage MonKhmerLanguages { get { return MonKhmerLanguagesLanguage.instance; } }
        /// <summary>The Mong language.</summary>
        public static ILanguage Mong { get { return MongLanguage.instance; } }
        /// <summary>The Mongo language.</summary>
        public static ILanguage Mongo { get { return MongoLanguage.instance; } }
        /// <summary>The Mongolian language.</summary>
        public static ILanguage Mongolian { get { return MongolianLanguage.instance; } }
        /// <summary>The Mossi language.</summary>
        public static ILanguage Mossi { get { return MossiLanguage.instance; } }
        /// <summary>The Multiple languages language.</summary>
        public static ILanguage MultipleLanguages { get { return MultipleLanguagesLanguage.instance; } }
        /// <summary>The Munda languages language.</summary>
        public static ILanguage MundaLanguages { get { return MundaLanguagesLanguage.instance; } }
        /// <summary>The N'Ko language.</summary>
        public static ILanguage NKo { get { return NKoLanguage.instance; } }
        /// <summary>The Nahuatl languages language.</summary>
        public static ILanguage NahuatlLanguages { get { return NahuatlLanguagesLanguage.instance; } }
        /// <summary>The Nauru language.</summary>
        public static ILanguage Nauru { get { return NauruLanguage.instance; } }
        /// <summary>The Navaho language.</summary>
        public static ILanguage Navaho { get { return NavahoLanguage.instance; } }
        /// <summary>The Navajo language.</summary>
        public static ILanguage Navajo { get { return NavajoLanguage.instance; } }
        /// <summary>The Ndebele, North language.</summary>
        public static ILanguage NdebeleNorth { get { return NdebeleNorthLanguage.instance; } }
        /// <summary>The Ndebele, South language.</summary>
        public static ILanguage NdebeleSouth { get { return NdebeleSouthLanguage.instance; } }
        /// <summary>The Ndonga language.</summary>
        public static ILanguage Ndonga { get { return NdongaLanguage.instance; } }
        /// <summary>The Neapolitan language.</summary>
        public static ILanguage Neapolitan { get { return NeapolitanLanguage.instance; } }
        /// <summary>The Nepal Bhasa language.</summary>
        public static ILanguage NepalBhasa { get { return NepalBhasaLanguage.instance; } }
        /// <summary>The Nepali language.</summary>
        public static ILanguage Nepali { get { return NepaliLanguage.instance; } }
        /// <summary>The Newari language.</summary>
        public static ILanguage Newari { get { return NewariLanguage.instance; } }
        /// <summary>The Nias language.</summary>
        public static ILanguage Nias { get { return NiasLanguage.instance; } }
        /// <summary>The Niger-Kordofanian languages language.</summary>
        public static ILanguage NigerKordofanianLanguages { get { return NigerKordofanianLanguagesLanguage.instance; } }
        /// <summary>The Nilo-Saharan languages language.</summary>
        public static ILanguage NiloSaharanLanguages { get { return NiloSaharanLanguagesLanguage.instance; } }
        /// <summary>The Niuean language.</summary>
        public static ILanguage Niuean { get { return NiueanLanguage.instance; } }
        /// <summary>The No linguistic content language.</summary>
        public static ILanguage NoLinguisticContent { get { return NoLinguisticContentLanguage.instance; } }
        /// <summary>The Nogai language.</summary>
        public static ILanguage Nogai { get { return NogaiLanguage.instance; } }
        /// <summary>The Norse, Old language.</summary>
        public static ILanguage NorseOld { get { return NorseOldLanguage.instance; } }
        /// <summary>The North American Indian languages language.</summary>
        public static ILanguage NorthAmericanIndianLanguages { get { return NorthAmericanIndianLanguagesLanguage.instance; } }
        /// <summary>The North Ndebele language.</summary>
        public static ILanguage NorthNdebele { get { return NorthNdebeleLanguage.instance; } }
        /// <summary>The Northern Frisian language.</summary>
        public static ILanguage NorthernFrisian { get { return NorthernFrisianLanguage.instance; } }
        /// <summary>The Northern Sami language.</summary>
        public static ILanguage NorthernSami { get { return NorthernSamiLanguage.instance; } }
        /// <summary>The Northern Sotho language.</summary>
        public static ILanguage NorthernSotho { get { return NorthernSothoLanguage.instance; } }
        /// <summary>The Norwegian language.</summary>
        public static ILanguage Norwegian { get { return NorwegianLanguage.instance; } }
        /// <summary>The Norwegian Bokmål language.</summary>
        public static ILanguage NorwegianBokmal { get { return NorwegianBokmalLanguage.instance; } }
        /// <summary>The Norwegian Nynorsk language.</summary>
        public static ILanguage NorwegianNynorsk { get { return NorwegianNynorskLanguage.instance; } }
        /// <summary>The Not applicable language.</summary>
        public static ILanguage NotApplicable { get { return NotApplicableLanguage.instance; } }
        /// <summary>The Nubian languages language.</summary>
        public static ILanguage NubianLanguages { get { return NubianLanguagesLanguage.instance; } }
        /// <summary>The Nuosu language.</summary>
        public static ILanguage Nuosu { get { return NuosuLanguage.instance; } }
        /// <summary>The Nyamwezi language.</summary>
        public static ILanguage Nyamwezi { get { return NyamweziLanguage.instance; } }
        /// <summary>The Nyanja language.</summary>
        public static ILanguage Nyanja { get { return NyanjaLanguage.instance; } }
        /// <summary>The Nyankole language.</summary>
        public static ILanguage Nyankole { get { return NyankoleLanguage.instance; } }
        /// <summary>The Nynorsk, Norwegian language.</summary>
        public static ILanguage NynorskNorwegian { get { return NynorskNorwegianLanguage.instance; } }
        /// <summary>The Nyoro language.</summary>
        public static ILanguage Nyoro { get { return NyoroLanguage.instance; } }
        /// <summary>The Nzima language.</summary>
        public static ILanguage Nzima { get { return NzimaLanguage.instance; } }
        /// <summary>The Occidental language.</summary>
        public static ILanguage Occidental { get { return OccidentalLanguage.instance; } }
        /// <summary>The Occitan (post 1500) language.</summary>
        public static ILanguage Occitan { get { return OccitanLanguage.instance; } }
        /// <summary>The Occitan, Old (to 1500) language.</summary>
        public static ILanguage OccitanOld { get { return OccitanOldLanguage.instance; } }
        /// <summary>The Official Aramaic (700-300 BCE) language.</summary>
        public static ILanguage OfficialAramaic { get { return OfficialAramaicLanguage.instance; } }
        /// <summary>The Oirat language.</summary>
        public static ILanguage Oirat { get { return OiratLanguage.instance; } }
        /// <summary>The Ojibwa language.</summary>
        public static ILanguage Ojibwa { get { return OjibwaLanguage.instance; } }
        /// <summary>The Old Bulgarian language.</summary>
        public static ILanguage OldBulgarian { get { return OldBulgarianLanguage.instance; } }
        /// <summary>The Old Church Slavonic language.</summary>
        public static ILanguage OldChurchSlavonic { get { return OldChurchSlavonicLanguage.instance; } }
        /// <summary>The Old Newari language.</summary>
        public static ILanguage OldNewari { get { return OldNewariLanguage.instance; } }
        /// <summary>The Old Slavonic language.</summary>
        public static ILanguage OldSlavonic { get { return OldSlavonicLanguage.instance; } }
        /// <summary>The Oriya language.</summary>
        public static ILanguage Oriya { get { return OriyaLanguage.instance; } }
        /// <summary>The Oromo language.</summary>
        public static ILanguage Oromo { get { return OromoLanguage.instance; } }
        /// <summary>The Osage language.</summary>
        public static ILanguage Osage { get { return OsageLanguage.instance; } }
        /// <summary>The Ossetian language.</summary>
        public static ILanguage Ossetian { get { return OssetianLanguage.instance; } }
        /// <summary>The Ossetic language.</summary>
        public static ILanguage Ossetic { get { return OsseticLanguage.instance; } }
        /// <summary>The Otomian languages language.</summary>
        public static ILanguage OtomianLanguages { get { return OtomianLanguagesLanguage.instance; } }
        /// <summary>The Pahlavi language.</summary>
        public static ILanguage Pahlavi { get { return PahlaviLanguage.instance; } }
        /// <summary>The Palauan language.</summary>
        public static ILanguage Palauan { get { return PalauanLanguage.instance; } }
        /// <summary>The Pali language.</summary>
        public static ILanguage Pali { get { return PaliLanguage.instance; } }
        /// <summary>The Pampanga language.</summary>
        public static ILanguage Pampanga { get { return PampangaLanguage.instance; } }
        /// <summary>The Pangasinan language.</summary>
        public static ILanguage Pangasinan { get { return PangasinanLanguage.instance; } }
        /// <summary>The Panjabi language.</summary>
        public static ILanguage Panjabi { get { return PanjabiLanguage.instance; } }
        /// <summary>The Papiamento language.</summary>
        public static ILanguage Papiamento { get { return PapiamentoLanguage.instance; } }
        /// <summary>The Papuan languages language.</summary>
        public static ILanguage PapuanLanguages { get { return PapuanLanguagesLanguage.instance; } }
        /// <summary>The Pashto language.</summary>
        public static ILanguage Pashto { get { return PashtoLanguage.instance; } }
        /// <summary>The Pedi language.</summary>
        public static ILanguage Pedi { get { return PediLanguage.instance; } }
        /// <summary>The Persian language.</summary>
        public static ILanguage Persian { get { return PersianLanguage.instance; } }
        /// <summary>The Persian, Old (ca.600-400 B.C.) language.</summary>
        public static ILanguage PersianOld { get { return PersianOldLanguage.instance; } }
        /// <summary>The Philippine languages language.</summary>
        public static ILanguage PhilippineLanguages { get { return PhilippineLanguagesLanguage.instance; } }
        /// <summary>The Phoenician language.</summary>
        public static ILanguage Phoenician { get { return PhoenicianLanguage.instance; } }
        /// <summary>The Pilipino language.</summary>
        public static ILanguage Pilipino { get { return PilipinoLanguage.instance; } }
        /// <summary>The Pohnpeian language.</summary>
        public static ILanguage Pohnpeian { get { return PohnpeianLanguage.instance; } }
        /// <summary>The Polish language.</summary>
        public static ILanguage Polish { get { return PolishLanguage.instance; } }
        /// <summary>The Portuguese language.</summary>
        public static ILanguage Portuguese { get { return PortugueseLanguage.instance; } }
        /// <summary>The Prakrit languages language.</summary>
        public static ILanguage PrakritLanguages { get { return PrakritLanguagesLanguage.instance; } }
        /// <summary>The Provençal, Old (to 1500) language.</summary>
        public static ILanguage ProvencalOld { get { return ProvencalOldLanguage.instance; } }
        /// <summary>The Punjabi language.</summary>
        public static ILanguage Punjabi { get { return PunjabiLanguage.instance; } }
        /// <summary>The Pushto language.</summary>
        public static ILanguage Pushto { get { return PushtoLanguage.instance; } }
        /// <summary>The Quechua language.</summary>
        public static ILanguage Quechua { get { return QuechuaLanguage.instance; } }
        /// <summary>The Rajasthani language.</summary>
        public static ILanguage Rajasthani { get { return RajasthaniLanguage.instance; } }
        /// <summary>The Rapanui language.</summary>
        public static ILanguage Rapanui { get { return RapanuiLanguage.instance; } }
        /// <summary>The Rarotongan language.</summary>
        public static ILanguage Rarotongan { get { return RarotonganLanguage.instance; } }
        /// <summary>The Reserved for local use language.</summary>
        public static ILanguage ReservedForLocalUse { get { return ReservedForLocalUseLanguage.instance; } }
        /// <summary>The Romance languages language.</summary>
        public static ILanguage RomanceLanguages { get { return RomanceLanguagesLanguage.instance; } }
        /// <summary>The Romanian language.</summary>
        public static ILanguage Romanian { get { return RomanianLanguage.instance; } }
        /// <summary>The Romansh language.</summary>
        public static ILanguage Romansh { get { return RomanshLanguage.instance; } }
        /// <summary>The Romany language.</summary>
        public static ILanguage Romany { get { return RomanyLanguage.instance; } }
        /// <summary>The Rundi language.</summary>
        public static ILanguage Rundi { get { return RundiLanguage.instance; } }
        /// <summary>The Russian language.</summary>
        public static ILanguage Russian { get { return RussianLanguage.instance; } }
        /// <summary>The Sakan language.</summary>
        public static ILanguage Sakan { get { return SakanLanguage.instance; } }
        /// <summary>The Salishan languages language.</summary>
        public static ILanguage SalishanLanguages { get { return SalishanLanguagesLanguage.instance; } }
        /// <summary>The Samaritan Aramaic language.</summary>
        public static ILanguage SamaritanAramaic { get { return SamaritanAramaicLanguage.instance; } }
        /// <summary>The Sami languages language.</summary>
        public static ILanguage SamiLanguages { get { return SamiLanguagesLanguage.instance; } }
        /// <summary>The Samoan language.</summary>
        public static ILanguage Samoan { get { return SamoanLanguage.instance; } }
        /// <summary>The Sandawe language.</summary>
        public static ILanguage Sandawe { get { return SandaweLanguage.instance; } }
        /// <summary>The Sango language.</summary>
        public static ILanguage Sango { get { return SangoLanguage.instance; } }
        /// <summary>The Sanskrit language.</summary>
        public static ILanguage Sanskrit { get { return SanskritLanguage.instance; } }
        /// <summary>The Santali language.</summary>
        public static ILanguage Santali { get { return SantaliLanguage.instance; } }
        /// <summary>The Sardinian language.</summary>
        public static ILanguage Sardinian { get { return SardinianLanguage.instance; } }
        /// <summary>The Sasak language.</summary>
        public static ILanguage Sasak { get { return SasakLanguage.instance; } }
        /// <summary>The Saxon, Low language.</summary>
        public static ILanguage SaxonLow { get { return SaxonLowLanguage.instance; } }
        /// <summary>The Scots language.</summary>
        public static ILanguage Scots { get { return ScotsLanguage.instance; } }
        /// <summary>The Scottish Gaelic language.</summary>
        public static ILanguage ScottishGaelic { get { return ScottishGaelicLanguage.instance; } }
        /// <summary>The Selkup language.</summary>
        public static ILanguage Selkup { get { return SelkupLanguage.instance; } }
        /// <summary>The Semitic languages language.</summary>
        public static ILanguage SemiticLanguages { get { return SemiticLanguagesLanguage.instance; } }
        /// <summary>The Sepedi language.</summary>
        public static ILanguage Sepedi { get { return SepediLanguage.instance; } }
        /// <summary>The Serbian language.</summary>
        public static ILanguage Serbian { get { return SerbianLanguage.instance; } }
        /// <summary>The Serer language.</summary>
        public static ILanguage Serer { get { return SererLanguage.instance; } }
        /// <summary>The Shan language.</summary>
        public static ILanguage Shan { get { return ShanLanguage.instance; } }
        /// <summary>The Shona language.</summary>
        public static ILanguage Shona { get { return ShonaLanguage.instance; } }
        /// <summary>The Sichuan Yi language.</summary>
        public static ILanguage SichuanYi { get { return SichuanYiLanguage.instance; } }
        /// <summary>The Sicilian language.</summary>
        public static ILanguage Sicilian { get { return SicilianLanguage.instance; } }
        /// <summary>The Sidamo language.</summary>
        public static ILanguage Sidamo { get { return SidamoLanguage.instance; } }
        /// <summary>The Sign Languages language.</summary>
        public static ILanguage SignLanguages { get { return SignLanguagesLanguage.instance; } }
        /// <summary>The Siksika language.</summary>
        public static ILanguage Siksika { get { return SiksikaLanguage.instance; } }
        /// <summary>The Sindhi language.</summary>
        public static ILanguage Sindhi { get { return SindhiLanguage.instance; } }
        /// <summary>The Sinhala language.</summary>
        public static ILanguage Sinhala { get { return SinhalaLanguage.instance; } }
        /// <summary>The Sinhalese language.</summary>
        public static ILanguage Sinhalese { get { return SinhaleseLanguage.instance; } }
        /// <summary>The Sino-Tibetan languages language.</summary>
        public static ILanguage SinoTibetanLanguages { get { return SinoTibetanLanguagesLanguage.instance; } }
        /// <summary>The Siouan languages language.</summary>
        public static ILanguage SiouanLanguages { get { return SiouanLanguagesLanguage.instance; } }
        /// <summary>The Skolt Sami language.</summary>
        public static ILanguage SkoltSami { get { return SkoltSamiLanguage.instance; } }
        /// <summary>The Slave (Athapascan) language.</summary>
        public static ILanguage Slave { get { return SlaveLanguage.instance; } }
        /// <summary>The Slavic languages language.</summary>
        public static ILanguage SlavicLanguages { get { return SlavicLanguagesLanguage.instance; } }
        /// <summary>The Slovak language.</summary>
        public static ILanguage Slovak { get { return SlovakLanguage.instance; } }
        /// <summary>The Slovenian language.</summary>
        public static ILanguage Slovenian { get { return SlovenianLanguage.instance; } }
        /// <summary>The Sogdian language.</summary>
        public static ILanguage Sogdian { get { return SogdianLanguage.instance; } }
        /// <summary>The Somali language.</summary>
        public static ILanguage Somali { get { return SomaliLanguage.instance; } }
        /// <summary>The Songhai languages language.</summary>
        public static ILanguage SonghaiLanguages { get { return SonghaiLanguagesLanguage.instance; } }
        /// <summary>The Soninke language.</summary>
        public static ILanguage Soninke { get { return SoninkeLanguage.instance; } }
        /// <summary>The Sorbian languages language.</summary>
        public static ILanguage SorbianLanguages { get { return SorbianLanguagesLanguage.instance; } }
        /// <summary>The Sotho, Northern language.</summary>
        public static ILanguage SothoNorthern { get { return SothoNorthernLanguage.instance; } }
        /// <summary>The Sotho, Southern language.</summary>
        public static ILanguage SothoSouthern { get { return SothoSouthernLanguage.instance; } }
        /// <summary>The South American Indian languages language.</summary>
        public static ILanguage SouthAmericanIndianLanguages { get { return SouthAmericanIndianLanguagesLanguage.instance; } }
        /// <summary>The South Ndebele language.</summary>
        public static ILanguage SouthNdebele { get { return SouthNdebeleLanguage.instance; } }
        /// <summary>The Southern Altai language.</summary>
        public static ILanguage SouthernAltai { get { return SouthernAltaiLanguage.instance; } }
        /// <summary>The Southern Sami language.</summary>
        public static ILanguage SouthernSami { get { return SouthernSamiLanguage.instance; } }
        /// <summary>The Spanish language.</summary>
        public static ILanguage Spanish { get { return SpanishLanguage.instance; } }
        /// <summary>The Sranan Tongo language.</summary>
        public static ILanguage SrananTongo { get { return SrananTongoLanguage.instance; } }
        /// <summary>The Standard Moroccan Tamazight language.</summary>
        public static ILanguage StandardMoroccanTamazight { get { return StandardMoroccanTamazightLanguage.instance; } }
        /// <summary>The Sukuma language.</summary>
        public static ILanguage Sukuma { get { return SukumaLanguage.instance; } }
        /// <summary>The Sumerian language.</summary>
        public static ILanguage Sumerian { get { return SumerianLanguage.instance; } }
        /// <summary>The Sundanese language.</summary>
        public static ILanguage Sundanese { get { return SundaneseLanguage.instance; } }
        /// <summary>The Susu language.</summary>
        public static ILanguage Susu { get { return SusuLanguage.instance; } }
        /// <summary>The Swahili language.</summary>
        public static ILanguage Swahili { get { return SwahiliLanguage.instance; } }
        /// <summary>The Swati language.</summary>
        public static ILanguage Swati { get { return SwatiLanguage.instance; } }
        /// <summary>The Swedish language.</summary>
        public static ILanguage Swedish { get { return SwedishLanguage.instance; } }
        /// <summary>The Swiss German language.</summary>
        public static ILanguage SwissGerman { get { return SwissGermanLanguage.instance; } }
        /// <summary>The Syriac language.</summary>
        public static ILanguage Syriac { get { return SyriacLanguage.instance; } }
        /// <summary>The Tagalog language.</summary>
        public static ILanguage Tagalog { get { return TagalogLanguage.instance; } }
        /// <summary>The Tahitian language.</summary>
        public static ILanguage Tahitian { get { return TahitianLanguage.instance; } }
        /// <summary>The Tai languages language.</summary>
        public static ILanguage TaiLanguages { get { return TaiLanguagesLanguage.instance; } }
        /// <summary>The Tajik language.</summary>
        public static ILanguage Tajik { get { return TajikLanguage.instance; } }
        /// <summary>The Tamashek language.</summary>
        public static ILanguage Tamashek { get { return TamashekLanguage.instance; } }
        /// <summary>The Tamil language.</summary>
        public static ILanguage Tamil { get { return TamilLanguage.instance; } }
        /// <summary>The Tatar language.</summary>
        public static ILanguage Tatar { get { return TatarLanguage.instance; } }
        /// <summary>The Telugu language.</summary>
        public static ILanguage Telugu { get { return TeluguLanguage.instance; } }
        /// <summary>The Tereno language.</summary>
        public static ILanguage Tereno { get { return TerenoLanguage.instance; } }
        /// <summary>The Tetum language.</summary>
        public static ILanguage Tetum { get { return TetumLanguage.instance; } }
        /// <summary>The Thai language.</summary>
        public static ILanguage Thai { get { return ThaiLanguage.instance; } }
        /// <summary>The Tibetan language.</summary>
        public static ILanguage Tibetan { get { return TibetanLanguage.instance; } }
        /// <summary>The Tigre language.</summary>
        public static ILanguage Tigre { get { return TigreLanguage.instance; } }
        /// <summary>The Tigrinya language.</summary>
        public static ILanguage Tigrinya { get { return TigrinyaLanguage.instance; } }
        /// <summary>The Timne language.</summary>
        public static ILanguage Timne { get { return TimneLanguage.instance; } }
        /// <summary>The Tiv language.</summary>
        public static ILanguage Tiv { get { return TivLanguage.instance; } }
        /// <summary>The tlhIngan-Hol language.</summary>
        public static ILanguage TlhInganHol { get { return TlhInganHolLanguage.instance; } }
        /// <summary>The Tlingit language.</summary>
        public static ILanguage Tlingit { get { return TlingitLanguage.instance; } }
        /// <summary>The Tok Pisin language.</summary>
        public static ILanguage TokPisin { get { return TokPisinLanguage.instance; } }
        /// <summary>The Tokelau language.</summary>
        public static ILanguage Tokelau { get { return TokelauLanguage.instance; } }
        /// <summary>The Tonga (Nyasa) language.</summary>
        public static ILanguage TongaNyasa { get { return TongaNyasaLanguage.instance; } }
        /// <summary>The Tonga (Tonga Islands) language.</summary>
        public static ILanguage TongaTongaIslands { get { return TongaTongaIslandsLanguage.instance; } }
        /// <summary>The Tsimshian language.</summary>
        public static ILanguage Tsimshian { get { return TsimshianLanguage.instance; } }
        /// <summary>The Tsonga language.</summary>
        public static ILanguage Tsonga { get { return TsongaLanguage.instance; } }
        /// <summary>The Tswana language.</summary>
        public static ILanguage Tswana { get { return TswanaLanguage.instance; } }
        /// <summary>The Tumbuka language.</summary>
        public static ILanguage Tumbuka { get { return TumbukaLanguage.instance; } }
        /// <summary>The Tupi languages language.</summary>
        public static ILanguage TupiLanguages { get { return TupiLanguagesLanguage.instance; } }
        /// <summary>The Turkish language.</summary>
        public static ILanguage Turkish { get { return TurkishLanguage.instance; } }
        /// <summary>The Turkish, Ottoman (1500-1928) language.</summary>
        public static ILanguage TurkishOttoman { get { return TurkishOttomanLanguage.instance; } }
        /// <summary>The Turkmen language.</summary>
        public static ILanguage Turkmen { get { return TurkmenLanguage.instance; } }
        /// <summary>The Tuvalu language.</summary>
        public static ILanguage Tuvalu { get { return TuvaluLanguage.instance; } }
        /// <summary>The Tuvinian language.</summary>
        public static ILanguage Tuvinian { get { return TuvinianLanguage.instance; } }
        /// <summary>The Twi language.</summary>
        public static ILanguage Twi { get { return TwiLanguage.instance; } }
        /// <summary>The Udmurt language.</summary>
        public static ILanguage Udmurt { get { return UdmurtLanguage.instance; } }
        /// <summary>The Ugaritic language.</summary>
        public static ILanguage Ugaritic { get { return UgariticLanguage.instance; } }
        /// <summary>The Uighur language.</summary>
        public static ILanguage Uighur { get { return UighurLanguage.instance; } }
        /// <summary>The Ukrainian language.</summary>
        public static ILanguage Ukrainian { get { return UkrainianLanguage.instance; } }
        /// <summary>The Umbundu language.</summary>
        public static ILanguage Umbundu { get { return UmbunduLanguage.instance; } }
        /// <summary>The Uncoded languages language.</summary>
        public static ILanguage UncodedLanguages { get { return UncodedLanguagesLanguage.instance; } }
        /// <summary>The Undetermined language.</summary>
        public static ILanguage Undetermined { get { return UndeterminedLanguage.instance; } }
        /// <summary>The Upper Sorbian language.</summary>
        public static ILanguage UpperSorbian { get { return UpperSorbianLanguage.instance; } }
        /// <summary>The Urdu language.</summary>
        public static ILanguage Urdu { get { return UrduLanguage.instance; } }
        /// <summary>The Uyghur language.</summary>
        public static ILanguage Uyghur { get { return UyghurLanguage.instance; } }
        /// <summary>The Uzbek language.</summary>
        public static ILanguage Uzbek { get { return UzbekLanguage.instance; } }
        /// <summary>The Vai language.</summary>
        public static ILanguage Vai { get { return VaiLanguage.instance; } }
        /// <summary>The Valencian language.</summary>
        public static ILanguage Valencian { get { return ValencianLanguage.instance; } }
        /// <summary>The Venda language.</summary>
        public static ILanguage Venda { get { return VendaLanguage.instance; } }
        /// <summary>The Vietnamese language.</summary>
        public static ILanguage Vietnamese { get { return VietnameseLanguage.instance; } }
        /// <summary>The Volapük language.</summary>
        public static ILanguage Volapuk { get { return VolapukLanguage.instance; } }
        /// <summary>The Votic language.</summary>
        public static ILanguage Votic { get { return VoticLanguage.instance; } }
        /// <summary>The Wakashan languages language.</summary>
        public static ILanguage WakashanLanguages { get { return WakashanLanguagesLanguage.instance; } }
        /// <summary>The Walloon language.</summary>
        public static ILanguage Walloon { get { return WalloonLanguage.instance; } }
        /// <summary>The Waray language.</summary>
        public static ILanguage Waray { get { return WarayLanguage.instance; } }
        /// <summary>The Washo language.</summary>
        public static ILanguage Washo { get { return WashoLanguage.instance; } }
        /// <summary>The Welsh language.</summary>
        public static ILanguage Welsh { get { return WelshLanguage.instance; } }
        /// <summary>The Western Frisian language.</summary>
        public static ILanguage WesternFrisian { get { return WesternFrisianLanguage.instance; } }
        /// <summary>The Western Pahari languages language.</summary>
        public static ILanguage WesternPahariLanguages { get { return WesternPahariLanguagesLanguage.instance; } }
        /// <summary>The Wolaitta language.</summary>
        public static ILanguage Wolaitta { get { return WolaittaLanguage.instance; } }
        /// <summary>The Wolaytta language.</summary>
        public static ILanguage Wolaytta { get { return WolayttaLanguage.instance; } }
        /// <summary>The Wolof language.</summary>
        public static ILanguage Wolof { get { return WolofLanguage.instance; } }
        /// <summary>The Xhosa language.</summary>
        public static ILanguage Xhosa { get { return XhosaLanguage.instance; } }
        /// <summary>The Yakut language.</summary>
        public static ILanguage Yakut { get { return YakutLanguage.instance; } }
        /// <summary>The Yao language.</summary>
        public static ILanguage Yao { get { return YaoLanguage.instance; } }
        /// <summary>The Yapese language.</summary>
        public static ILanguage Yapese { get { return YapeseLanguage.instance; } }
        /// <summary>The Yiddish language.</summary>
        public static ILanguage Yiddish { get { return YiddishLanguage.instance; } }
        /// <summary>The Yoruba language.</summary>
        public static ILanguage Yoruba { get { return YorubaLanguage.instance; } }
        /// <summary>The Yupik languages language.</summary>
        public static ILanguage YupikLanguages { get { return YupikLanguagesLanguage.instance; } }
        /// <summary>The Zande languages language.</summary>
        public static ILanguage ZandeLanguages { get { return ZandeLanguagesLanguage.instance; } }
        /// <summary>The Zapotec language.</summary>
        public static ILanguage Zapotec { get { return ZapotecLanguage.instance; } }
        /// <summary>The Zaza language.</summary>
        public static ILanguage Zaza { get { return ZazaLanguage.instance; } }
        /// <summary>The Zazaki language.</summary>
        public static ILanguage Zazaki { get { return ZazakiLanguage.instance; } }
        /// <summary>The Zenaga language.</summary>
        public static ILanguage Zenaga { get { return ZenagaLanguage.instance; } }
        /// <summary>The Zhuang language.</summary>
        public static ILanguage Zhuang { get { return ZhuangLanguage.instance; } }
        /// <summary>The Zulu language.</summary>
        public static ILanguage Zulu { get { return ZuluLanguage.instance; } }
        /// <summary>The Zuni language.</summary>
        public static ILanguage Zuni { get { return ZuniLanguage.instance; } }

        // Encapsulates reflection logic to access enumeration of static public properties,
        //   which is to say, the list of languages. 
        static void InitializeSortedByEnglishName()
        {
            sortedByEnglishName = null;

            SortedDictionary<string, ILanguage> sorter = new SortedDictionary<string, ILanguage>();
            foreach (
                PropertyInfo property in (new ISOLanguages()).GetType().GetRuntimeProperties().TakeWhile(
                    property
                    => property.GetMethod.IsStatic
                        & (property.PropertyType == typeof(ILanguage))
                    )
                )
            {
                ILanguage language = (ILanguage)(property.GetValue(null, null));
                sorter.Add(language.EnglishName, language);
            }

            sortedByEnglishName = new List<ILanguage>();
            foreach (KeyValuePair<string, ILanguage> keyValuePair in sorter)
                sortedByEnglishName.Add(keyValuePair.Value);

        }

        /// <summary>
        /// A list of all languages defined in the ISO 639-2 standard, 
        /// in alphabetical order by English name.
        /// </summary>
        /// <returns>list of all languages defined in the
        /// ISO 639-2 standard, sorted by English name.</returns>
        public static List<ILanguage> AllISOLanguages
        {
            get
            {
                if (sortedByEnglishName == null) InitializeSortedByEnglishName();
                return new List<ILanguage>(sortedByEnglishName);
            }
        }

        /// <summary>
        /// An ObservableCollection of all languages defined in the
        /// ISO 639-2 standard, in alphabetical order by English name.
        /// </summary>
        /// <returns>list of all languages defined in the
        /// ISO 639-2 standard, in alphabetical order by English name.</returns>
        public static ObservableCollection<ILanguage> ObservableAllISOLanguages
        {
            get
            {
                if (sortedByEnglishName == null) InitializeSortedByEnglishName();
                return new ObservableCollection<ILanguage>( sortedByEnglishName );
            }
        }

        /// <summary>
        /// The number of languages defined in the ISO 639-2 standard.
        /// </summary>
        /// <returns>Count of languages defined in the ISO 639-2 standard.</returns>
        public static int CountOfISOLanguages
        {
            get
            {
                if (sortedByEnglishName == null) InitializeSortedByEnglishName();
                return sortedByEnglishName.Count;
            }
        }

        /// <summary>
        /// That language which, of all the languages with the 
        /// two-letter ISO 639-2 code supplied by the caller,
        /// has the alphabetically earliest English name; null
        /// if the code is not a valid ISO 639-2 code.
        /// </summary>
        /// <returns>ILanguage: the language primarily associated
        /// with the specified two-letter code.</returns>
        public static ILanguage GetLanguageWithTwoLetterISOCode(string isoCode)
        {

            if (isoCode == null) return null;
            else
            {
                ILanguage retval = null;
                if (sortedByEnglishName == null) InitializeSortedByEnglishName();
                foreach (
                    ILanguage language in sortedByEnglishName.Where(
                        language => isoCode.Equals(language.TwoLetterISOCode)
                        ).Take(1)
                    )
                    retval = language;
                return retval;
            }

        }

        /// <summary>
        /// That language which, of all the languages with the 
        /// three-letter ISO 639-2 code supplied by the caller,
        /// has the alphabetically earliest English name; null
        /// if the code is not a valid ISO 639-2 code.
        /// </summary>
        /// <returns>ILanguage: the language primarily associated
        /// with the specified three-letter code.</returns>
        public static ILanguage GetLanguageWithThreeLetterISOCode(string isoCode)
        {

            if (isoCode == null) return null;
            else
            {
                ILanguage retval = null;
                if (sortedByEnglishName == null) InitializeSortedByEnglishName();
                foreach (
                    ILanguage language in sortedByEnglishName.Where(
                        language => isoCode.Equals(language.ThreeLetterISOCode) | isoCode.Equals(language.AlternateThreeLetterISOCode)
                        ).Take(1)
                    )
                    retval = language;
                return retval;
            }

        }

        /// <summary>
        /// That language which, of all the languages with the 
        /// two- or three-letter ISO 639-2 code supplied by the caller,
        /// has the alphabetically earliest English name; null
        /// if the code is not a valid ISO 639-2 code.
        /// </summary>
        /// <returns>ILanguage: the language primarily associated
        /// with the specified code.</returns>
        public static ILanguage GetLanguageWithTwoOrThreeLetterISOCode(string isoCode)
        {

            if (isoCode == null) return null;
            else if (isoCode.Length == 2) return GetLanguageWithTwoLetterISOCode(isoCode);
            else if (isoCode.Length == 3) return GetLanguageWithThreeLetterISOCode(isoCode);
            else return null;

        }


        class AbkhazianLanguage
        {
            class Abkhazian : ISOLanguage
            {
                public override string Name => "Abkhazian";
                public override string TwoLetterISOCode => "ab";
                public override string ThreeLetterISOCode => "abk";
            }
            static AbkhazianLanguage() { }
            internal static readonly ILanguage instance = new Abkhazian();
        }
        class AchineseLanguage
        {
            class Achinese : ISOLanguage
            {
                public override string Name => "Achinese";
                public override string ThreeLetterISOCode => "ace";
            }
            static AchineseLanguage() { }
            internal static readonly ILanguage instance = new Achinese();
        }
        class AcoliLanguage
        {
            class Acoli : ISOLanguage
            {
                public override string Name => "Acoli";
                public override string ThreeLetterISOCode => "ach";
            }
            static AcoliLanguage() { }
            internal static readonly ILanguage instance = new Acoli();
        }
        class AdangmeLanguage
        {
            class Adangme : ISOLanguage
            {
                public override string Name => "Adangme";
                public override string ThreeLetterISOCode => "ada";
            }
            static AdangmeLanguage() { }
            internal static readonly ILanguage instance = new Adangme();
        }
        class AdygeiLanguage
        {
            class Adygei : ISOLanguage
            {
                public override string Name => "Adygei";
                public override string ThreeLetterISOCode => "ady";
            }
            static AdygeiLanguage() { }
            internal static readonly ILanguage instance = new Adygei();
        }
        class AdygheLanguage
        {
            class Adyghe : ISOLanguage
            {
                public override string Name => "Adyghe";
                public override string ThreeLetterISOCode => "ady";
            }
            static AdygheLanguage() { }
            internal static readonly ILanguage instance = new Adyghe();
        }
        class AfarLanguage
        {
            class Afar : ISOLanguage
            {
                public override string Name => "Afar";
                public override string TwoLetterISOCode => "aa";
                public override string ThreeLetterISOCode => "aar";
            }
            static AfarLanguage() { }
            internal static readonly ILanguage instance = new Afar();
        }
        class AfrihiliLanguage
        {
            class Afrihili : ISOLanguage
            {
                public override string Name => "Afrihili";
                public override string ThreeLetterISOCode => "afh";
            }
            static AfrihiliLanguage() { }
            internal static readonly ILanguage instance = new Afrihili();
        }
        class AfrikaansLanguage
        {
            class Afrikaans : ISOLanguage
            {
                public override string Name => "Afrikaans";
                public override string TwoLetterISOCode => "af";
                public override string ThreeLetterISOCode => "afr";
            }
            static AfrikaansLanguage() { }
            internal static readonly ILanguage instance = new Afrikaans();
        }
        class AfroAsiaticLanguagesLanguage
        {
            class AfroAsiaticLanguages : ISOLanguage
            {
                public override string Name => "Afro-Asiatic languages";
                public override string ThreeLetterISOCode => "afa";
            }
            static AfroAsiaticLanguagesLanguage() { }
            internal static readonly ILanguage instance = new AfroAsiaticLanguages();
        }
        class AinuLanguage
        {
            class Ainu : ISOLanguage
            {
                public override string Name => "Ainu";
                public override string ThreeLetterISOCode => "ain";
            }
            static AinuLanguage() { }
            internal static readonly ILanguage instance = new Ainu();
        }
        class AkanLanguage
        {
            class Akan : ISOLanguage
            {
                public override string Name => "Akan";
                public override string TwoLetterISOCode => "ak";
                public override string ThreeLetterISOCode => "aka";
            }
            static AkanLanguage() { }
            internal static readonly ILanguage instance = new Akan();
        }
        class AkkadianLanguage
        {
            class Akkadian : ISOLanguage
            {
                public override string Name => "Akkadian";
                public override string ThreeLetterISOCode => "akk";
            }
            static AkkadianLanguage() { }
            internal static readonly ILanguage instance = new Akkadian();
        }
        class AlbanianLanguage
        {
            class Albanian : ISOLanguage
            {
                public override string Name => "Albanian";
                public override string TwoLetterISOCode => "sq";
                public override string ThreeLetterISOCode => "alb";
                public override string AlternateThreeLetterISOCode => "sqi";
            }
            static AlbanianLanguage() { }
            internal static readonly ILanguage instance = new Albanian();
        }
        class AlemannicLanguage
        {
            class Alemannic : ISOLanguage
            {
                public override string Name => "Alemannic";
                public override string ThreeLetterISOCode => "gsw";
            }
            static AlemannicLanguage() { }
            internal static readonly ILanguage instance = new Alemannic();
        }
        class AleutLanguage
        {
            class Aleut : ISOLanguage
            {
                public override string Name => "Aleut";
                public override string ThreeLetterISOCode => "ale";
            }
            static AleutLanguage() { }
            internal static readonly ILanguage instance = new Aleut();
        }
        class AlgonquianLanguagesLanguage
        {
            class AlgonquianLanguages : ISOLanguage
            {
                public override string Name => "Algonquian languages";
                public override string ThreeLetterISOCode => "alg";
            }
            static AlgonquianLanguagesLanguage() { }
            internal static readonly ILanguage instance = new AlgonquianLanguages();
        }
        class AlsatianLanguage
        {
            class Alsatian : ISOLanguage
            {
                public override string Name => "Alsatian";
                public override string ThreeLetterISOCode => "gsw";
            }
            static AlsatianLanguage() { }
            internal static readonly ILanguage instance = new Alsatian();
        }
        class AltaicLanguagesLanguage
        {
            class AltaicLanguages : ISOLanguage
            {
                public override string Name => "Altaic languages";
                public override string ThreeLetterISOCode => "tut";
            }
            static AltaicLanguagesLanguage() { }
            internal static readonly ILanguage instance = new AltaicLanguages();
        }
        class AmharicLanguage
        {
            class Amharic : ISOLanguage
            {
                public override string Name => "Amharic";
                public override string TwoLetterISOCode => "am";
                public override string ThreeLetterISOCode => "amh";
            }
            static AmharicLanguage() { }
            internal static readonly ILanguage instance = new Amharic();
        }
        class AngikaLanguage
        {
            class Angika : ISOLanguage
            {
                public override string Name => "Angika";
                public override string ThreeLetterISOCode => "anp";
            }
            static AngikaLanguage() { }
            internal static readonly ILanguage instance = new Angika();
        }
        class ApacheLanguagesLanguage
        {
            class ApacheLanguages : ISOLanguage
            {
                public override string Name => "Apache languages";
                public override string ThreeLetterISOCode => "apa";
            }
            static ApacheLanguagesLanguage() { }
            internal static readonly ILanguage instance = new ApacheLanguages();
        }
        class ArabicLanguage
        {
            class Arabic : ISOLanguage
            {
                public override string Name => "Arabic";
                public override string TwoLetterISOCode => "ar";
                public override string ThreeLetterISOCode => "ara";
            }
            static ArabicLanguage() { }
            internal static readonly ILanguage instance = new Arabic();
        }
        class AragoneseLanguage
        {
            class Aragonese : ISOLanguage
            {
                public override string Name => "Aragonese";
                public override string TwoLetterISOCode => "an";
                public override string ThreeLetterISOCode => "arg";
            }
            static AragoneseLanguage() { }
            internal static readonly ILanguage instance = new Aragonese();
        }
        class ArapahoLanguage
        {
            class Arapaho : ISOLanguage
            {
                public override string Name => "Arapaho";
                public override string ThreeLetterISOCode => "arp";
            }
            static ArapahoLanguage() { }
            internal static readonly ILanguage instance = new Arapaho();
        }
        class ArawakLanguage
        {
            class Arawak : ISOLanguage
            {
                public override string Name => "Arawak";
                public override string ThreeLetterISOCode => "arw";
            }
            static ArawakLanguage() { }
            internal static readonly ILanguage instance = new Arawak();
        }
        class ArmenianLanguage
        {
            class Armenian : ISOLanguage
            {
                public override string Name => "հայերէն";
                public override string TransliteratedName => "hayeren";
                public override string EnglishName => "Armenian";
                public override string TwoLetterISOCode => "hy";
                public override string ThreeLetterISOCode => "arm";
                public override string AlternateThreeLetterISOCode => "hye";
            }
            static ArmenianLanguage() { }
            internal static readonly ILanguage instance = new Armenian();
        }
        class AromanianLanguage
        {
            class Aromanian : ISOLanguage
            {
                public override string Name => "Aromanian";
                public override string ThreeLetterISOCode => "rup";
            }
            static AromanianLanguage() { }
            internal static readonly ILanguage instance = new Aromanian();
        }
        class ArtificialLanguagesLanguage
        {
            class ArtificialLanguages : ISOLanguage
            {
                public override string Name => "Artificial languages";
                public override string ThreeLetterISOCode => "art";
            }
            static ArtificialLanguagesLanguage() { }
            internal static readonly ILanguage instance = new ArtificialLanguages();
        }
        class ArumanianLanguage
        {
            class Arumanian : ISOLanguage
            {
                public override string Name => "Arumanian";
                public override string ThreeLetterISOCode => "rup";
            }
            static ArumanianLanguage() { }
            internal static readonly ILanguage instance = new Arumanian();
        }
        class AssameseLanguage
        {
            class Assamese : ISOLanguage
            {
                public override string Name => "Assamese";
                public override string TwoLetterISOCode => "as";
                public override string ThreeLetterISOCode => "asm";
            }
            static AssameseLanguage() { }
            internal static readonly ILanguage instance = new Assamese();
        }
        class AsturianLanguage
        {
            class Asturian : ISOLanguage
            {
                public override string Name => "Asturian";
                public override string ThreeLetterISOCode => "ast";
            }
            static AsturianLanguage() { }
            internal static readonly ILanguage instance = new Asturian();
        }
        class AsturleoneseLanguage
        {
            class Asturleonese : ISOLanguage
            {
                public override string Name => "Asturleonese";
                public override string ThreeLetterISOCode => "ast";
            }
            static AsturleoneseLanguage() { }
            internal static readonly ILanguage instance = new Asturleonese();
        }
        class AthapascanLanguagesLanguage
        {
            class AthapascanLanguages : ISOLanguage
            {
                public override string Name => "Athapascan languages";
                public override string ThreeLetterISOCode => "ath";
            }
            static AthapascanLanguagesLanguage() { }
            internal static readonly ILanguage instance = new AthapascanLanguages();
        }
        class AustralianLanguagesLanguage
        {
            class AustralianLanguages : ISOLanguage
            {
                public override string Name => "Australian languages";
                public override string ThreeLetterISOCode => "aus";
            }
            static AustralianLanguagesLanguage() { }
            internal static readonly ILanguage instance = new AustralianLanguages();
        }
        class AustronesianLanguagesLanguage
        {
            class AustronesianLanguages : ISOLanguage
            {
                public override string Name => "Austronesian languages";
                public override string ThreeLetterISOCode => "map";
            }
            static AustronesianLanguagesLanguage() { }
            internal static readonly ILanguage instance = new AustronesianLanguages();
        }
        class AvaricLanguage
        {
            class Avaric : ISOLanguage
            {
                public override string Name => "Avaric";
                public override string TwoLetterISOCode => "av";
                public override string ThreeLetterISOCode => "ava";
            }
            static AvaricLanguage() { }
            internal static readonly ILanguage instance = new Avaric();
        }
        class AvestanLanguage
        {
            class Avestan : ISOLanguage
            {
                public override string Name => "Avestan";
                public override string TwoLetterISOCode => "ae";
                public override string ThreeLetterISOCode => "ave";
            }
            static AvestanLanguage() { }
            internal static readonly ILanguage instance = new Avestan();
        }
        class AwadhiLanguage
        {
            class Awadhi : ISOLanguage
            {
                public override string Name => "Awadhi";
                public override string ThreeLetterISOCode => "awa";
            }
            static AwadhiLanguage() { }
            internal static readonly ILanguage instance = new Awadhi();
        }
        class AymaraLanguage
        {
            class Aymara : ISOLanguage
            {
                public override string Name => "Aymara";
                public override string TwoLetterISOCode => "ay";
                public override string ThreeLetterISOCode => "aym";
            }
            static AymaraLanguage() { }
            internal static readonly ILanguage instance = new Aymara();
        }
        class AzerbaijaniLanguage
        {
            class Azerbaijani : ISOLanguage
            {
                public override string Name => "Azerbaijani";
                public override string TwoLetterISOCode => "az";
                public override string ThreeLetterISOCode => "aze";
            }
            static AzerbaijaniLanguage() { }
            internal static readonly ILanguage instance = new Azerbaijani();
        }
        class BableLanguage
        {
            class Bable : ISOLanguage
            {
                public override string Name => "Bable";
                public override string ThreeLetterISOCode => "ast";
            }
            static BableLanguage() { }
            internal static readonly ILanguage instance = new Bable();
        }
        class BalineseLanguage
        {
            class Balinese : ISOLanguage
            {
                public override string Name => "Balinese";
                public override string ThreeLetterISOCode => "ban";
            }
            static BalineseLanguage() { }
            internal static readonly ILanguage instance = new Balinese();
        }
        class BalticLanguagesLanguage
        {
            class BalticLanguages : ISOLanguage
            {
                public override string Name => "Baltic languages";
                public override string ThreeLetterISOCode => "bat";
            }
            static BalticLanguagesLanguage() { }
            internal static readonly ILanguage instance = new BalticLanguages();
        }
        class BaluchiLanguage
        {
            class Baluchi : ISOLanguage
            {
                public override string Name => "Baluchi";
                public override string ThreeLetterISOCode => "bal";
            }
            static BaluchiLanguage() { }
            internal static readonly ILanguage instance = new Baluchi();
        }
        class BambaraLanguage
        {
            class Bambara : ISOLanguage
            {
                public override string Name => "Bambara";
                public override string TwoLetterISOCode => "bm";
                public override string ThreeLetterISOCode => "bam";
            }
            static BambaraLanguage() { }
            internal static readonly ILanguage instance = new Bambara();
        }
        class BamilekeLanguagesLanguage
        {
            class BamilekeLanguages : ISOLanguage
            {
                public override string Name => "Bamileke languages";
                public override string ThreeLetterISOCode => "bai";
            }
            static BamilekeLanguagesLanguage() { }
            internal static readonly ILanguage instance = new BamilekeLanguages();
        }
        class BandaLanguagesLanguage
        {
            class BandaLanguages : ISOLanguage
            {
                public override string Name => "Banda languages";
                public override string ThreeLetterISOCode => "bad";
            }
            static BandaLanguagesLanguage() { }
            internal static readonly ILanguage instance = new BandaLanguages();
        }
        class BantuLanguagesLanguage
        {
            class BantuLanguages : ISOLanguage
            {
                public override string Name => "Bantu languages";
                public override string ThreeLetterISOCode => "bnt";
            }
            static BantuLanguagesLanguage() { }
            internal static readonly ILanguage instance = new BantuLanguages();
        }
        class BasaLanguage
        {
            class Basa : ISOLanguage
            {
                public override string Name => "Basa";
                public override string ThreeLetterISOCode => "bas";
            }
            static BasaLanguage() { }
            internal static readonly ILanguage instance = new Basa();
        }
        class BashkirLanguage
        {
            class Bashkir : ISOLanguage
            {
                public override string Name => "Bashkir";
                public override string TwoLetterISOCode => "ba";
                public override string ThreeLetterISOCode => "bak";
            }
            static BashkirLanguage() { }
            internal static readonly ILanguage instance = new Bashkir();
        }
        class BasqueLanguage
        {
            class Basque : ISOLanguage
            {
                public override string Name => "Basque";
                public override string TwoLetterISOCode => "eu";
                public override string ThreeLetterISOCode => "baq";
                public override string AlternateThreeLetterISOCode => "eus";
            }
            static BasqueLanguage() { }
            internal static readonly ILanguage instance = new Basque();
        }
        class BatakLanguagesLanguage
        {
            class BatakLanguages : ISOLanguage
            {
                public override string Name => "Batak languages";
                public override string ThreeLetterISOCode => "btk";
            }
            static BatakLanguagesLanguage() { }
            internal static readonly ILanguage instance = new BatakLanguages();
        }
        class BedawiyetLanguage
        {
            class Bedawiyet : ISOLanguage
            {
                public override string Name => "Bedawiyet";
                public override string ThreeLetterISOCode => "bej";
            }
            static BedawiyetLanguage() { }
            internal static readonly ILanguage instance = new Bedawiyet();
        }
        class BejaLanguage
        {
            class Beja : ISOLanguage
            {
                public override string Name => "Beja";
                public override string ThreeLetterISOCode => "bej";
            }
            static BejaLanguage() { }
            internal static readonly ILanguage instance = new Beja();
        }
        class BelarusianLanguage
        {
            class Belarusian : ISOLanguage
            {
                public override string Name => "Belarusian";
                public override string TwoLetterISOCode => "be";
                public override string ThreeLetterISOCode => "bel";
            }
            static BelarusianLanguage() { }
            internal static readonly ILanguage instance = new Belarusian();
        }
        class BembaLanguage
        {
            class Bemba : ISOLanguage
            {
                public override string Name => "Bemba";
                public override string ThreeLetterISOCode => "bem";
            }
            static BembaLanguage() { }
            internal static readonly ILanguage instance = new Bemba();
        }
        class BengaliLanguage
        {
            class Bengali : ISOLanguage
            {
                public override string Name => "Bengali";
                public override string TwoLetterISOCode => "bn";
                public override string ThreeLetterISOCode => "ben";
            }
            static BengaliLanguage() { }
            internal static readonly ILanguage instance = new Bengali();
        }
        class BerberLanguagesLanguage
        {
            class BerberLanguages : ISOLanguage
            {
                public override string Name => "Berber languages";
                public override string ThreeLetterISOCode => "ber";
            }
            static BerberLanguagesLanguage() { }
            internal static readonly ILanguage instance = new BerberLanguages();
        }
        class BhojpuriLanguage
        {
            class Bhojpuri : ISOLanguage
            {
                public override string Name => "Bhojpuri";
                public override string ThreeLetterISOCode => "bho";
            }
            static BhojpuriLanguage() { }
            internal static readonly ILanguage instance = new Bhojpuri();
        }
        class BihariLanguagesLanguage
        {
            class BihariLanguages : ISOLanguage
            {
                public override string Name => "Bihari languages";
                public override string TwoLetterISOCode => "bh";
                public override string ThreeLetterISOCode => "bih";
            }
            static BihariLanguagesLanguage() { }
            internal static readonly ILanguage instance = new BihariLanguages();
        }
        class BikolLanguage
        {
            class Bikol : ISOLanguage
            {
                public override string Name => "Bikol";
                public override string ThreeLetterISOCode => "bik";
            }
            static BikolLanguage() { }
            internal static readonly ILanguage instance = new Bikol();
        }
        class BilinLanguage
        {
            class Bilin : ISOLanguage
            {
                public override string Name => "Bilin";
                public override string ThreeLetterISOCode => "byn";
            }
            static BilinLanguage() { }
            internal static readonly ILanguage instance = new Bilin();
        }
        class BiniLanguage
        {
            class Bini : ISOLanguage
            {
                public override string Name => "Bini";
                public override string ThreeLetterISOCode => "bin";
            }
            static BiniLanguage() { }
            internal static readonly ILanguage instance = new Bini();
        }
        class BislamaLanguage
        {
            class Bislama : ISOLanguage
            {
                public override string Name => "Bislama";
                public override string TwoLetterISOCode => "bi";
                public override string ThreeLetterISOCode => "bis";
            }
            static BislamaLanguage() { }
            internal static readonly ILanguage instance = new Bislama();
        }
        class BlinLanguage
        {
            class Blin : ISOLanguage
            {
                public override string Name => "Blin";
                public override string ThreeLetterISOCode => "byn";
            }
            static BlinLanguage() { }
            internal static readonly ILanguage instance = new Blin();
        }
        class BlissLanguage
        {
            class Bliss : ISOLanguage
            {
                public override string Name => "Bliss";
                public override string ThreeLetterISOCode => "zbl";
            }
            static BlissLanguage() { }
            internal static readonly ILanguage instance = new Bliss();
        }
        class BlissymbolicsLanguage
        {
            class Blissymbolics : ISOLanguage
            {
                public override string Name => "Blissymbolics";
                public override string ThreeLetterISOCode => "zbl";
            }
            static BlissymbolicsLanguage() { }
            internal static readonly ILanguage instance = new Blissymbolics();
        }
        class BlissymbolsLanguage
        {
            class Blissymbols : ISOLanguage
            {
                public override string Name => "Blissymbols";
                public override string ThreeLetterISOCode => "zbl";
            }
            static BlissymbolsLanguage() { }
            internal static readonly ILanguage instance = new Blissymbols();
        }
        class BokmalNorwegianLanguage
        {
            class BokmalNorwegian : ISOLanguage
            {
                public override string Name => "Bokmål, Norwegian";
                public override string TwoLetterISOCode => "nb";
                public override string ThreeLetterISOCode => "nob";
            }
            static BokmalNorwegianLanguage() { }
            internal static readonly ILanguage instance = new BokmalNorwegian();
        }
        class BosnianLanguage
        {
            class Bosnian : ISOLanguage
            {
                public override string Name => "Bosnian";
                public override string TwoLetterISOCode => "bs";
                public override string ThreeLetterISOCode => "bos";
            }
            static BosnianLanguage() { }
            internal static readonly ILanguage instance = new Bosnian();
        }
        class BrajLanguage
        {
            class Braj : ISOLanguage
            {
                public override string Name => "Braj";
                public override string ThreeLetterISOCode => "bra";
            }
            static BrajLanguage() { }
            internal static readonly ILanguage instance = new Braj();
        }
        class BretonLanguage
        {
            class Breton : ISOLanguage
            {
                public override string Name => "Breton";
                public override string TwoLetterISOCode => "br";
                public override string ThreeLetterISOCode => "bre";
            }
            static BretonLanguage() { }
            internal static readonly ILanguage instance = new Breton();
        }
        class BugineseLanguage
        {
            class Buginese : ISOLanguage
            {
                public override string Name => "Buginese";
                public override string ThreeLetterISOCode => "bug";
            }
            static BugineseLanguage() { }
            internal static readonly ILanguage instance = new Buginese();
        }
        class BulgarianLanguage
        {
            class Bulgarian : ISOLanguage
            {
                public override string Name => "Bulgarian";
                public override string TwoLetterISOCode => "bg";
                public override string ThreeLetterISOCode => "bul";
            }
            static BulgarianLanguage() { }
            internal static readonly ILanguage instance = new Bulgarian();
        }
        class BuriatLanguage
        {
            class Buriat : ISOLanguage
            {
                public override string Name => "Buriat";
                public override string ThreeLetterISOCode => "bua";
            }
            static BuriatLanguage() { }
            internal static readonly ILanguage instance = new Buriat();
        }
        class BurmeseLanguage
        {
            class Burmese : ISOLanguage
            {
                public override string Name => "Burmese";
                public override string TwoLetterISOCode => "my";
                public override string ThreeLetterISOCode => "bur";
                public override string AlternateThreeLetterISOCode => "mya";
            }
            static BurmeseLanguage() { }
            internal static readonly ILanguage instance = new Burmese();
        }
        class CaddoLanguage
        {
            class Caddo : ISOLanguage
            {
                public override string Name => "Caddo";
                public override string ThreeLetterISOCode => "cad";
            }
            static CaddoLanguage() { }
            internal static readonly ILanguage instance = new Caddo();
        }
        class CastilianLanguage
        {
            class Castilian : ISOLanguage
            {
                public override string Name => "Castilian";
                public override string TwoLetterISOCode => "es";
                public override string ThreeLetterISOCode => "spa";
            }
            static CastilianLanguage() { }
            internal static readonly ILanguage instance = new Castilian();
        }
        class CatalanLanguage
        {
            class Catalan : ISOLanguage
            {
                public override string Name => "Catalan";
                public override string TwoLetterISOCode => "ca";
                public override string ThreeLetterISOCode => "cat";
            }
            static CatalanLanguage() { }
            internal static readonly ILanguage instance = new Catalan();
        }
        class CaucasianLanguagesLanguage
        {
            class CaucasianLanguages : ISOLanguage
            {
                public override string Name => "Caucasian languages";
                public override string ThreeLetterISOCode => "cau";
            }
            static CaucasianLanguagesLanguage() { }
            internal static readonly ILanguage instance = new CaucasianLanguages();
        }
        class CebuanoLanguage
        {
            class Cebuano : ISOLanguage
            {
                public override string Name => "Cebuano";
                public override string ThreeLetterISOCode => "ceb";
            }
            static CebuanoLanguage() { }
            internal static readonly ILanguage instance = new Cebuano();
        }
        class CelticLanguagesLanguage
        {
            class CelticLanguages : ISOLanguage
            {
                public override string Name => "Celtic languages";
                public override string ThreeLetterISOCode => "cel";
            }
            static CelticLanguagesLanguage() { }
            internal static readonly ILanguage instance = new CelticLanguages();
        }
        class CentralAmericanIndianLanguagesLanguage
        {
            class CentralAmericanIndianLanguages : ISOLanguage
            {
                public override string Name => "Central American Indian languages";
                public override string ThreeLetterISOCode => "cai";
            }
            static CentralAmericanIndianLanguagesLanguage() { }
            internal static readonly ILanguage instance = new CentralAmericanIndianLanguages();
        }
        class CentralKhmerLanguage
        {
            class CentralKhmer : ISOLanguage
            {
                public override string Name => "Central Khmer";
                public override string TwoLetterISOCode => "km";
                public override string ThreeLetterISOCode => "khm";
            }
            static CentralKhmerLanguage() { }
            internal static readonly ILanguage instance = new CentralKhmer();
        }
        class ChagataiLanguage
        {
            class Chagatai : ISOLanguage
            {
                public override string Name => "Chagatai";
                public override string ThreeLetterISOCode => "chg";
            }
            static ChagataiLanguage() { }
            internal static readonly ILanguage instance = new Chagatai();
        }
        class ChamicLanguagesLanguage
        {
            class ChamicLanguages : ISOLanguage
            {
                public override string Name => "Chamic languages";
                public override string ThreeLetterISOCode => "cmc";
            }
            static ChamicLanguagesLanguage() { }
            internal static readonly ILanguage instance = new ChamicLanguages();
        }
        class ChamorroLanguage
        {
            class Chamorro : ISOLanguage
            {
                public override string Name => "Chamorro";
                public override string TwoLetterISOCode => "ch";
                public override string ThreeLetterISOCode => "cha";
            }
            static ChamorroLanguage() { }
            internal static readonly ILanguage instance = new Chamorro();
        }
        class ChechenLanguage
        {
            class Chechen : ISOLanguage
            {
                public override string Name => "Chechen";
                public override string TwoLetterISOCode => "ce";
                public override string ThreeLetterISOCode => "che";
            }
            static ChechenLanguage() { }
            internal static readonly ILanguage instance = new Chechen();
        }
        class CherokeeLanguage
        {
            class Cherokee : ISOLanguage
            {
                public override string Name => "ᏣᎳᎩ";
                public override string TransliteratedName => "Tsalagi";
                public override string EnglishName => "Cherokee";
                public override string ThreeLetterISOCode => "chr";
            }
            static CherokeeLanguage() { }
            internal static readonly ILanguage instance = new Cherokee();
        }
        class ChewaLanguage
        {
            class Chewa : ISOLanguage
            {
                public override string Name => "Chewa";
                public override string TwoLetterISOCode => "ny";
                public override string ThreeLetterISOCode => "nya";
            }
            static ChewaLanguage() { }
            internal static readonly ILanguage instance = new Chewa();
        }
        class CheyenneLanguage
        {
            class Cheyenne : ISOLanguage
            {
                public override string Name => "Cheyenne";
                public override string ThreeLetterISOCode => "chy";
            }
            static CheyenneLanguage() { }
            internal static readonly ILanguage instance = new Cheyenne();
        }
        class ChibchaLanguage
        {
            class Chibcha : ISOLanguage
            {
                public override string Name => "Chibcha";
                public override string ThreeLetterISOCode => "chb";
            }
            static ChibchaLanguage() { }
            internal static readonly ILanguage instance = new Chibcha();
        }
        class ChichewaLanguage
        {
            class Chichewa : ISOLanguage
            {
                public override string Name => "Chichewa";
                public override string TwoLetterISOCode => "ny";
                public override string ThreeLetterISOCode => "nya";
            }
            static ChichewaLanguage() { }
            internal static readonly ILanguage instance = new Chichewa();
        }
        class ChineseLanguage
        {
            class Chinese : ISOLanguage
            {
                public override string Name => "中国";
                public override string TransliteratedName => "zhōngguó";
                public override string EnglishName => "Chinese";
                public override string TwoLetterISOCode => "zh";
                public override string ThreeLetterISOCode => "chi";
                public override string AlternateThreeLetterISOCode => "zho";
            }
            static ChineseLanguage() { }
            internal static readonly ILanguage instance = new Chinese();
        }
        class ChinookJargonLanguage
        {
            class ChinookJargon : ISOLanguage
            {
                public override string Name => "Chinook jargon";
                public override string ThreeLetterISOCode => "chn";
            }
            static ChinookJargonLanguage() { }
            internal static readonly ILanguage instance = new ChinookJargon();
        }
        class ChipewyanLanguage
        {
            class Chipewyan : ISOLanguage
            {
                public override string Name => "Chipewyan";
                public override string ThreeLetterISOCode => "chp";
            }
            static ChipewyanLanguage() { }
            internal static readonly ILanguage instance = new Chipewyan();
        }
        class ChoctawLanguage
        {
            class Choctaw : ISOLanguage
            {
                public override string Name => "Choctaw";
                public override string ThreeLetterISOCode => "cho";
            }
            static ChoctawLanguage() { }
            internal static readonly ILanguage instance = new Choctaw();
        }
        class ChuangLanguage
        {
            class Chuang : ISOLanguage
            {
                public override string Name => "Chuang";
                public override string TwoLetterISOCode => "za";
                public override string ThreeLetterISOCode => "zha";
            }
            static ChuangLanguage() { }
            internal static readonly ILanguage instance = new Chuang();
        }
        class ChurchSlavicLanguage
        {
            class ChurchSlavic : ISOLanguage
            {
                public override string Name => "Church Slavic";
                public override string TwoLetterISOCode => "cu";
                public override string ThreeLetterISOCode => "chu";
            }
            static ChurchSlavicLanguage() { }
            internal static readonly ILanguage instance = new ChurchSlavic();
        }
        class ChurchSlavonicLanguage
        {
            class ChurchSlavonic : ISOLanguage
            {
                public override string Name => "Church Slavonic";
                public override string TwoLetterISOCode => "cu";
                public override string ThreeLetterISOCode => "chu";
            }
            static ChurchSlavonicLanguage() { }
            internal static readonly ILanguage instance = new ChurchSlavonic();
        }
        class ChuukeseLanguage
        {
            class Chuukese : ISOLanguage
            {
                public override string Name => "Chuukese";
                public override string ThreeLetterISOCode => "chk";
            }
            static ChuukeseLanguage() { }
            internal static readonly ILanguage instance = new Chuukese();
        }
        class ChuvashLanguage
        {
            class Chuvash : ISOLanguage
            {
                public override string Name => "Chuvash";
                public override string TwoLetterISOCode => "cv";
                public override string ThreeLetterISOCode => "chv";
            }
            static ChuvashLanguage() { }
            internal static readonly ILanguage instance = new Chuvash();
        }
        class ClassicalNepalBhasaLanguage
        {
            class ClassicalNepalBhasa : ISOLanguage
            {
                public override string Name => "Classical Nepal Bhasa";
                public override string ThreeLetterISOCode => "nwc";
            }
            static ClassicalNepalBhasaLanguage() { }
            internal static readonly ILanguage instance = new ClassicalNepalBhasa();
        }
        class ClassicalNewariLanguage
        {
            class ClassicalNewari : ISOLanguage
            {
                public override string Name => "Classical Newari";
                public override string ThreeLetterISOCode => "nwc";
            }
            static ClassicalNewariLanguage() { }
            internal static readonly ILanguage instance = new ClassicalNewari();
        }
        class ClassicalSyriacLanguage
        {
            class ClassicalSyriac : ISOLanguage
            {
                public override string Name => "Classical Syriac";
                public override string ThreeLetterISOCode => "syc";
            }
            static ClassicalSyriacLanguage() { }
            internal static readonly ILanguage instance = new ClassicalSyriac();
        }
        class CookIslandsMaoriLanguage
        {
            class CookIslandsMaori : ISOLanguage
            {
                public override string Name => "Cook Islands Maori";
                public override string ThreeLetterISOCode => "rar";
            }
            static CookIslandsMaoriLanguage() { }
            internal static readonly ILanguage instance = new CookIslandsMaori();
        }
        class CopticLanguage
        {
            class Coptic : ISOLanguage
            {
                public override string Name => "Coptic";
                public override string ThreeLetterISOCode => "cop";
            }
            static CopticLanguage() { }
            internal static readonly ILanguage instance = new Coptic();
        }
        class CornishLanguage
        {
            class Cornish : ISOLanguage
            {
                public override string Name => "Cornish";
                public override string TwoLetterISOCode => "kw";
                public override string ThreeLetterISOCode => "cor";
            }
            static CornishLanguage() { }
            internal static readonly ILanguage instance = new Cornish();
        }
        class CorsicanLanguage
        {
            class Corsican : ISOLanguage
            {
                public override string Name => "Corsican";
                public override string TwoLetterISOCode => "co";
                public override string ThreeLetterISOCode => "cos";
            }
            static CorsicanLanguage() { }
            internal static readonly ILanguage instance = new Corsican();
        }
        class CreeLanguage
        {
            class Cree : ISOLanguage
            {
                public override string Name => "Cree";
                public override string TwoLetterISOCode => "cr";
                public override string ThreeLetterISOCode => "cre";
            }
            static CreeLanguage() { }
            internal static readonly ILanguage instance = new Cree();
        }
        class CreekLanguage
        {
            class Creek : ISOLanguage
            {
                public override string Name => "Creek";
                public override string ThreeLetterISOCode => "mus";
            }
            static CreekLanguage() { }
            internal static readonly ILanguage instance = new Creek();
        }
        class CreolesAndPidginsLanguage
        {
            class CreolesAndPidgins : ISOLanguage
            {
                public override string Name => "Creoles and pidgins";
                public override string ThreeLetterISOCode => "crp";
            }
            static CreolesAndPidginsLanguage() { }
            internal static readonly ILanguage instance = new CreolesAndPidgins();
        }
        class CreolesAndPidginsEnglishBasedLanguage
        {
            class CreolesAndPidginsEnglishBased : ISOLanguage
            {
                public override string Name => "Creoles and pidgins, English based";
                public override string ThreeLetterISOCode => "cpe";
            }
            static CreolesAndPidginsEnglishBasedLanguage() { }
            internal static readonly ILanguage instance = new CreolesAndPidginsEnglishBased();
        }
        class CreolesAndPidginsFrenchBasedLanguage
        {
            class CreolesAndPidginsFrenchBased : ISOLanguage
            {
                public override string Name => "Creoles and pidgins, French-based";
                public override string ThreeLetterISOCode => "cpf";
            }
            static CreolesAndPidginsFrenchBasedLanguage() { }
            internal static readonly ILanguage instance = new CreolesAndPidginsFrenchBased();
        }
        class CreolesAndPidginsPortugueseBasedLanguage
        {
            class CreolesAndPidginsPortugueseBased : ISOLanguage
            {
                public override string Name => "Creoles and pidgins, Portuguese-based";
                public override string ThreeLetterISOCode => "cpp";
            }
            static CreolesAndPidginsPortugueseBasedLanguage() { }
            internal static readonly ILanguage instance = new CreolesAndPidginsPortugueseBased();
        }
        class CrimeanTatarLanguage
        {
            class CrimeanTatar : ISOLanguage
            {
                public override string Name => "Crimean Tatar";
                public override string ThreeLetterISOCode => "crh";
            }
            static CrimeanTatarLanguage() { }
            internal static readonly ILanguage instance = new CrimeanTatar();
        }
        class CrimeanTurkishLanguage
        {
            class CrimeanTurkish : ISOLanguage
            {
                public override string Name => "Crimean Turkish";
                public override string ThreeLetterISOCode => "crh";
            }
            static CrimeanTurkishLanguage() { }
            internal static readonly ILanguage instance = new CrimeanTurkish();
        }
        class CroatianLanguage
        {
            class Croatian : ISOLanguage
            {
                public override string Name => "Croatian";
                public override string TwoLetterISOCode => "hr";
                public override string ThreeLetterISOCode => "hrv";
            }
            static CroatianLanguage() { }
            internal static readonly ILanguage instance = new Croatian();
        }
        class CushiticLanguagesLanguage
        {
            class CushiticLanguages : ISOLanguage
            {
                public override string Name => "Cushitic languages";
                public override string ThreeLetterISOCode => "cus";
            }
            static CushiticLanguagesLanguage() { }
            internal static readonly ILanguage instance = new CushiticLanguages();
        }
        class CzechLanguage
        {
            class Czech : ISOLanguage
            {
                public override string Name => "Czech";
                public override string TwoLetterISOCode => "cs";
                public override string ThreeLetterISOCode => "cze";
                public override string AlternateThreeLetterISOCode => "ces";
            }
            static CzechLanguage() { }
            internal static readonly ILanguage instance = new Czech();
        }
        class DakotaLanguage
        {
            class Dakota : ISOLanguage
            {
                public override string Name => "Dakota";
                public override string ThreeLetterISOCode => "dak";
            }
            static DakotaLanguage() { }
            internal static readonly ILanguage instance = new Dakota();
        }
        class DanishLanguage
        {
            class Danish : ISOLanguage
            {
                public override string Name => "Danish";
                public override string TwoLetterISOCode => "da";
                public override string ThreeLetterISOCode => "dan";
            }
            static DanishLanguage() { }
            internal static readonly ILanguage instance = new Danish();
        }
        class DargwaLanguage
        {
            class Dargwa : ISOLanguage
            {
                public override string Name => "Dargwa";
                public override string ThreeLetterISOCode => "dar";
            }
            static DargwaLanguage() { }
            internal static readonly ILanguage instance = new Dargwa();
        }
        class DelawareLanguage
        {
            class Delaware : ISOLanguage
            {
                public override string Name => "Delaware";
                public override string ThreeLetterISOCode => "del";
            }
            static DelawareLanguage() { }
            internal static readonly ILanguage instance = new Delaware();
        }
        class DeneSulineLanguage
        {
            class DeneSuline : ISOLanguage
            {
                public override string Name => "Dene Suline";
                public override string ThreeLetterISOCode => "chp";
            }
            static DeneSulineLanguage() { }
            internal static readonly ILanguage instance = new DeneSuline();
        }
        class DhivehiLanguage
        {
            class Dhivehi : ISOLanguage
            {
                public override string Name => "Dhivehi";
                public override string TwoLetterISOCode => "dv";
                public override string ThreeLetterISOCode => "div";
            }
            static DhivehiLanguage() { }
            internal static readonly ILanguage instance = new Dhivehi();
        }
        class DimiliLanguage
        {
            class Dimili : ISOLanguage
            {
                public override string Name => "Dimili";
                public override string ThreeLetterISOCode => "zza";
            }
            static DimiliLanguage() { }
            internal static readonly ILanguage instance = new Dimili();
        }
        class DimliLanguage
        {
            class Dimli : ISOLanguage
            {
                public override string Name => "Dimli";
                public override string ThreeLetterISOCode => "zza";
            }
            static DimliLanguage() { }
            internal static readonly ILanguage instance = new Dimli();
        }
        class DinkaLanguage
        {
            class Dinka : ISOLanguage
            {
                public override string Name => "Dinka";
                public override string ThreeLetterISOCode => "din";
            }
            static DinkaLanguage() { }
            internal static readonly ILanguage instance = new Dinka();
        }
        class DivehiLanguage
        {
            class Divehi : ISOLanguage
            {
                public override string Name => "Divehi";
                public override string TwoLetterISOCode => "dv";
                public override string ThreeLetterISOCode => "div";
            }
            static DivehiLanguage() { }
            internal static readonly ILanguage instance = new Divehi();
        }
        class DogriLanguage
        {
            class Dogri : ISOLanguage
            {
                public override string Name => "Dogri";
                public override string ThreeLetterISOCode => "doi";
            }
            static DogriLanguage() { }
            internal static readonly ILanguage instance = new Dogri();
        }
        class DogribLanguage
        {
            class Dogrib : ISOLanguage
            {
                public override string Name => "Dogrib";
                public override string ThreeLetterISOCode => "dgr";
            }
            static DogribLanguage() { }
            internal static readonly ILanguage instance = new Dogrib();
        }
        class DravidianLanguagesLanguage
        {
            class DravidianLanguages : ISOLanguage
            {
                public override string Name => "Dravidian languages";
                public override string ThreeLetterISOCode => "dra";
            }
            static DravidianLanguagesLanguage() { }
            internal static readonly ILanguage instance = new DravidianLanguages();
        }
        class DualaLanguage
        {
            class Duala : ISOLanguage
            {
                public override string Name => "Duala";
                public override string ThreeLetterISOCode => "dua";
            }
            static DualaLanguage() { }
            internal static readonly ILanguage instance = new Duala();
        }
        class DutchLanguage
        {
            class Dutch : ISOLanguage
            {
                public override string Name => "Dutch";
                public override string TwoLetterISOCode => "nl";
                public override string ThreeLetterISOCode => "dut";
                public override string AlternateThreeLetterISOCode => "nld";
            }
            static DutchLanguage() { }
            internal static readonly ILanguage instance = new Dutch();
        }
        class DutchMiddleLanguage
        {
            class DutchMiddle : ISOLanguage
            {
                public override string Name => "Dutch, Middle (ca.1050-1350)";
                public override string ThreeLetterISOCode => "dum";
            }
            static DutchMiddleLanguage() { }
            internal static readonly ILanguage instance = new DutchMiddle();
        }
        class DyulaLanguage
        {
            class Dyula : ISOLanguage
            {
                public override string Name => "Dyula";
                public override string ThreeLetterISOCode => "dyu";
            }
            static DyulaLanguage() { }
            internal static readonly ILanguage instance = new Dyula();
        }
        class DzongkhaLanguage
        {
            class Dzongkha : ISOLanguage
            {
                public override string Name => "Dzongkha";
                public override string TwoLetterISOCode => "dz";
                public override string ThreeLetterISOCode => "dzo";
            }
            static DzongkhaLanguage() { }
            internal static readonly ILanguage instance = new Dzongkha();
        }
        class EasternFrisianLanguage
        {
            class EasternFrisian : ISOLanguage
            {
                public override string Name => "Eastern Frisian";
                public override string ThreeLetterISOCode => "frs";
            }
            static EasternFrisianLanguage() { }
            internal static readonly ILanguage instance = new EasternFrisian();
        }
        class EdoLanguage
        {
            class Edo : ISOLanguage
            {
                public override string Name => "Edo";
                public override string ThreeLetterISOCode => "bin";
            }
            static EdoLanguage() { }
            internal static readonly ILanguage instance = new Edo();
        }
        class EfikLanguage
        {
            class Efik : ISOLanguage
            {
                public override string Name => "Efik";
                public override string ThreeLetterISOCode => "efi";
            }
            static EfikLanguage() { }
            internal static readonly ILanguage instance = new Efik();
        }
        class EgyptianLanguage
        {
            class Egyptian : ISOLanguage
            {
                public override string Name => "Egyptian (Ancient)";
                public override string ThreeLetterISOCode => "egy";
            }
            static EgyptianLanguage() { }
            internal static readonly ILanguage instance = new Egyptian();
        }
        class EkajukLanguage
        {
            class Ekajuk : ISOLanguage
            {
                public override string Name => "Ekajuk";
                public override string ThreeLetterISOCode => "eka";
            }
            static EkajukLanguage() { }
            internal static readonly ILanguage instance = new Ekajuk();
        }
        class ElamiteLanguage
        {
            class Elamite : ISOLanguage
            {
                public override string Name => "Elamite";
                public override string ThreeLetterISOCode => "elx";
            }
            static ElamiteLanguage() { }
            internal static readonly ILanguage instance = new Elamite();
        }
        class EnglishLanguage
        {
            class English : ISOLanguage
            {
                public override string Name => "English";
                public override string TwoLetterISOCode => "en";
                public override string ThreeLetterISOCode => "eng";
            }
            static EnglishLanguage() { }
            internal static readonly ILanguage instance = new English();
        }
        class EnglishMiddleLanguage
        {
            class EnglishMiddle : ISOLanguage
            {
                public override string Name => "English, Middle (1100-1500)";
                public override string ThreeLetterISOCode => "enm";
            }
            static EnglishMiddleLanguage() { }
            internal static readonly ILanguage instance = new EnglishMiddle();
        }
        class EnglishOldLanguage
        {
            class EnglishOld : ISOLanguage
            {
                public override string Name => "English, Old (ca.450-1100)";
                public override string ThreeLetterISOCode => "ang";
            }
            static EnglishOldLanguage() { }
            internal static readonly ILanguage instance = new EnglishOld();
        }
        class ErzyaLanguage
        {
            class Erzya : ISOLanguage
            {
                public override string Name => "Erzya";
                public override string ThreeLetterISOCode => "myv";
            }
            static ErzyaLanguage() { }
            internal static readonly ILanguage instance = new Erzya();
        }
        class EsperantoLanguage
        {
            class Esperanto : ISOLanguage
            {
                public override string Name => "Esperanto";
                public override string TwoLetterISOCode => "eo";
                public override string ThreeLetterISOCode => "epo";
            }
            static EsperantoLanguage() { }
            internal static readonly ILanguage instance = new Esperanto();
        }
        class EstonianLanguage
        {
            class Estonian : ISOLanguage
            {
                public override string Name => "Estonian";
                public override string TwoLetterISOCode => "et";
                public override string ThreeLetterISOCode => "est";
            }
            static EstonianLanguage() { }
            internal static readonly ILanguage instance = new Estonian();
        }
        class EweLanguage
        {
            class Ewe : ISOLanguage
            {
                public override string Name => "Ewe";
                public override string TwoLetterISOCode => "ee";
                public override string ThreeLetterISOCode => "ewe";
            }
            static EweLanguage() { }
            internal static readonly ILanguage instance = new Ewe();
        }
        class EwondoLanguage
        {
            class Ewondo : ISOLanguage
            {
                public override string Name => "Ewondo";
                public override string ThreeLetterISOCode => "ewo";
            }
            static EwondoLanguage() { }
            internal static readonly ILanguage instance = new Ewondo();
        }
        class FangLanguage
        {
            class Fang : ISOLanguage
            {
                public override string Name => "Fang";
                public override string ThreeLetterISOCode => "fan";
            }
            static FangLanguage() { }
            internal static readonly ILanguage instance = new Fang();
        }
        class FantiLanguage
        {
            class Fanti : ISOLanguage
            {
                public override string Name => "Fanti";
                public override string ThreeLetterISOCode => "fat";
            }
            static FantiLanguage() { }
            internal static readonly ILanguage instance = new Fanti();
        }
        class FaroeseLanguage
        {
            class Faroese : ISOLanguage
            {
                public override string Name => "Faroese";
                public override string TwoLetterISOCode => "fo";
                public override string ThreeLetterISOCode => "fao";
            }
            static FaroeseLanguage() { }
            internal static readonly ILanguage instance = new Faroese();
        }
        class FijianLanguage
        {
            class Fijian : ISOLanguage
            {
                public override string Name => "Fijian";
                public override string TwoLetterISOCode => "fj";
                public override string ThreeLetterISOCode => "fij";
            }
            static FijianLanguage() { }
            internal static readonly ILanguage instance = new Fijian();
        }
        class FilipinoLanguage
        {
            class Filipino : ISOLanguage
            {
                public override string Name => "Filipino";
                public override string ThreeLetterISOCode => "fil";
            }
            static FilipinoLanguage() { }
            internal static readonly ILanguage instance = new Filipino();
        }
        class FinnishLanguage
        {
            class Finnish : ISOLanguage
            {
                public override string Name => "Finnish";
                public override string TwoLetterISOCode => "fi";
                public override string ThreeLetterISOCode => "fin";
            }
            static FinnishLanguage() { }
            internal static readonly ILanguage instance = new Finnish();
        }
        class FinnoUgrianLanguagesLanguage
        {
            class FinnoUgrianLanguages : ISOLanguage
            {
                public override string Name => "Finno-Ugrian languages";
                public override string ThreeLetterISOCode => "fiu";
            }
            static FinnoUgrianLanguagesLanguage() { }
            internal static readonly ILanguage instance = new FinnoUgrianLanguages();
        }
        class FlemishLanguage
        {
            class Flemish : ISOLanguage
            {
                public override string Name => "Flemish";
                public override string TwoLetterISOCode => "nl";
                public override string ThreeLetterISOCode => "dut";
                public override string AlternateThreeLetterISOCode => "nld";
            }
            static FlemishLanguage() { }
            internal static readonly ILanguage instance = new Flemish();
        }
        class FonLanguage
        {
            class Fon : ISOLanguage
            {
                public override string Name => "Fon";
                public override string ThreeLetterISOCode => "fon";
            }
            static FonLanguage() { }
            internal static readonly ILanguage instance = new Fon();
        }
        class FrenchLanguage
        {
            class French : ISOLanguage
            {
                public override string Name => "français";
                public override string EnglishName => "French";
                public override string TwoLetterISOCode => "fr";
                public override string ThreeLetterISOCode => "fre";
                public override string AlternateThreeLetterISOCode => "fra";
            }
            static FrenchLanguage() { }
            internal static readonly ILanguage instance = new French();
        }
        class FrenchMiddleLanguage
        {
            class FrenchMiddle : ISOLanguage
            {
                public override string Name => "French, Middle (ca.1400-1600)";
                public override string ThreeLetterISOCode => "frm";
            }
            static FrenchMiddleLanguage() { }
            internal static readonly ILanguage instance = new FrenchMiddle();
        }
        class FrenchOldLanguage
        {
            class FrenchOld : ISOLanguage
            {
                public override string Name => "French, Old (842-ca.1400)";
                public override string ThreeLetterISOCode => "fro";
            }
            static FrenchOldLanguage() { }
            internal static readonly ILanguage instance = new FrenchOld();
        }
        class FriulianLanguage
        {
            class Friulian : ISOLanguage
            {
                public override string Name => "Friulian";
                public override string ThreeLetterISOCode => "fur";
            }
            static FriulianLanguage() { }
            internal static readonly ILanguage instance = new Friulian();
        }
        class FulahLanguage
        {
            class Fulah : ISOLanguage
            {
                public override string Name => "Fulah";
                public override string TwoLetterISOCode => "ff";
                public override string ThreeLetterISOCode => "ful";
            }
            static FulahLanguage() { }
            internal static readonly ILanguage instance = new Fulah();
        }
        class GaLanguage
        {
            class Ga : ISOLanguage
            {
                public override string Name => "Ga";
                public override string ThreeLetterISOCode => "gaa";
            }
            static GaLanguage() { }
            internal static readonly ILanguage instance = new Ga();
        }
        class GaelicLanguage
        {
            class Gaelic : ISOLanguage
            {
                public override string Name => "Gaelic";
                public override string TwoLetterISOCode => "gd";
                public override string ThreeLetterISOCode => "gla";
            }
            static GaelicLanguage() { }
            internal static readonly ILanguage instance = new Gaelic();
        }
        class GalibiCaribLanguage
        {
            class GalibiCarib : ISOLanguage
            {
                public override string Name => "Galibi Carib";
                public override string ThreeLetterISOCode => "car";
            }
            static GalibiCaribLanguage() { }
            internal static readonly ILanguage instance = new GalibiCarib();
        }
        class GalicianLanguage
        {
            class Galician : ISOLanguage
            {
                public override string Name => "Galician";
                public override string TwoLetterISOCode => "gl";
                public override string ThreeLetterISOCode => "glg";
            }
            static GalicianLanguage() { }
            internal static readonly ILanguage instance = new Galician();
        }
        class GandaLanguage
        {
            class Ganda : ISOLanguage
            {
                public override string Name => "Ganda";
                public override string TwoLetterISOCode => "lg";
                public override string ThreeLetterISOCode => "lug";
            }
            static GandaLanguage() { }
            internal static readonly ILanguage instance = new Ganda();
        }
        class GayoLanguage
        {
            class Gayo : ISOLanguage
            {
                public override string Name => "Gayo";
                public override string ThreeLetterISOCode => "gay";
            }
            static GayoLanguage() { }
            internal static readonly ILanguage instance = new Gayo();
        }
        class GbayaLanguage
        {
            class Gbaya : ISOLanguage
            {
                public override string Name => "Gbaya";
                public override string ThreeLetterISOCode => "gba";
            }
            static GbayaLanguage() { }
            internal static readonly ILanguage instance = new Gbaya();
        }
        class GeezLanguage
        {
            class Geez : ISOLanguage
            {
                public override string Name => "Geez";
                public override string ThreeLetterISOCode => "gez";
            }
            static GeezLanguage() { }
            internal static readonly ILanguage instance = new Geez();
        }
        class GeorgianLanguage
        {
            class Georgian : ISOLanguage
            {
                public override string Name => "ქართული";
                public override string TransliteratedName => "kartuli";
                public override string EnglishName => "Georgian";
                public override string TwoLetterISOCode => "ka";
                public override string ThreeLetterISOCode => "geo";
                public override string AlternateThreeLetterISOCode => "kat";
            }
            static GeorgianLanguage() { }
            internal static readonly ILanguage instance = new Georgian();
        }
        class GermanLanguage
        {
            class German : ISOLanguage
            {
                public override string Name => "Deutsch";
                public override string EnglishName => "German";
                public override string TwoLetterISOCode => "de";
                public override string ThreeLetterISOCode => "ger";
                public override string AlternateThreeLetterISOCode => "deu";
            }
            static GermanLanguage() { }
            internal static readonly ILanguage instance = new German();
        }
        class GermanLowLanguage
        {
            class GermanLow : ISOLanguage
            {
                public override string Name => "German, Low";
                public override string ThreeLetterISOCode => "nds";
            }
            static GermanLowLanguage() { }
            internal static readonly ILanguage instance = new GermanLow();
        }
        class GermanMiddleHighLanguage
        {
            class GermanMiddleHigh : ISOLanguage
            {
                public override string Name => "German, Middle High (ca.1050-1500)";
                public override string ThreeLetterISOCode => "gmh";
            }
            static GermanMiddleHighLanguage() { }
            internal static readonly ILanguage instance = new GermanMiddleHigh();
        }
        class GermanOldHighLanguage
        {
            class GermanOldHigh : ISOLanguage
            {
                public override string Name => "German, Old High (ca.750-1050)";
                public override string ThreeLetterISOCode => "goh";
            }
            static GermanOldHighLanguage() { }
            internal static readonly ILanguage instance = new GermanOldHigh();
        }
        class GermanicLanguagesLanguage
        {
            class GermanicLanguages : ISOLanguage
            {
                public override string Name => "Germanic languages";
                public override string ThreeLetterISOCode => "gem";
            }
            static GermanicLanguagesLanguage() { }
            internal static readonly ILanguage instance = new GermanicLanguages();
        }
        class GikuyuLanguage
        {
            class Gikuyu : ISOLanguage
            {
                public override string Name => "Gikuyu";
                public override string TwoLetterISOCode => "ki";
                public override string ThreeLetterISOCode => "kik";
            }
            static GikuyuLanguage() { }
            internal static readonly ILanguage instance = new Gikuyu();
        }
        class GilberteseLanguage
        {
            class Gilbertese : ISOLanguage
            {
                public override string Name => "Gilbertese";
                public override string ThreeLetterISOCode => "gil";
            }
            static GilberteseLanguage() { }
            internal static readonly ILanguage instance = new Gilbertese();
        }
        class GondiLanguage
        {
            class Gondi : ISOLanguage
            {
                public override string Name => "Gondi";
                public override string ThreeLetterISOCode => "gon";
            }
            static GondiLanguage() { }
            internal static readonly ILanguage instance = new Gondi();
        }
        class GorontaloLanguage
        {
            class Gorontalo : ISOLanguage
            {
                public override string Name => "Gorontalo";
                public override string ThreeLetterISOCode => "gor";
            }
            static GorontaloLanguage() { }
            internal static readonly ILanguage instance = new Gorontalo();
        }
        class GothicLanguage
        {
            class Gothic : ISOLanguage
            {
                public override string Name => "Gothic";
                public override string ThreeLetterISOCode => "got";
            }
            static GothicLanguage() { }
            internal static readonly ILanguage instance = new Gothic();
        }
        class GreboLanguage
        {
            class Grebo : ISOLanguage
            {
                public override string Name => "Grebo";
                public override string ThreeLetterISOCode => "grb";
            }
            static GreboLanguage() { }
            internal static readonly ILanguage instance = new Grebo();
        }
        class GreekAncientLanguage
        {
            class GreekAncient : ISOLanguage
            {
                public override string Name => "Ἑλληνική";
                public override string TransliteratedName => "Helleniké";
                public override string EnglishName => "Greek, Ancient (to 1453)";
                public override string ThreeLetterISOCode => "grc";
            }
            static GreekAncientLanguage() { }
            internal static readonly ILanguage instance = new GreekAncient();
        }
        class GreekModernLanguage
        {
            class GreekModern : ISOLanguage
            {
                public override string Name => "ελληνικά";
                public override string TransliteratedName => "elleniká";
                public override string EnglishName => "Greek, Modern (1453-)";
                public override string TwoLetterISOCode => "el";
                public override string ThreeLetterISOCode => "gre";
                public override string AlternateThreeLetterISOCode => "ell";
            }
            static GreekModernLanguage() { }
            internal static readonly ILanguage instance = new GreekModern();
        }
        class GreenlandicLanguage
        {
            class Greenlandic : ISOLanguage
            {
                public override string Name => "Greenlandic";
                public override string TwoLetterISOCode => "kl";
                public override string ThreeLetterISOCode => "kal";
            }
            static GreenlandicLanguage() { }
            internal static readonly ILanguage instance = new Greenlandic();
        }
        class GuaraniLanguage
        {
            class Guarani : ISOLanguage
            {
                public override string Name => "Guarani";
                public override string TwoLetterISOCode => "gn";
                public override string ThreeLetterISOCode => "grn";
            }
            static GuaraniLanguage() { }
            internal static readonly ILanguage instance = new Guarani();
        }
        class GujaratiLanguage
        {
            class Gujarati : ISOLanguage
            {
                public override string Name => "Gujarati";
                public override string TwoLetterISOCode => "gu";
                public override string ThreeLetterISOCode => "guj";
            }
            static GujaratiLanguage() { }
            internal static readonly ILanguage instance = new Gujarati();
        }
        class GwichinLanguage
        {
            class Gwichin : ISOLanguage
            {
                public override string Name => "Gwich'in";
                public override string ThreeLetterISOCode => "gwi";
            }
            static GwichinLanguage() { }
            internal static readonly ILanguage instance = new Gwichin();
        }
        class HaidaLanguage
        {
            class Haida : ISOLanguage
            {
                public override string Name => "Haida";
                public override string ThreeLetterISOCode => "hai";
            }
            static HaidaLanguage() { }
            internal static readonly ILanguage instance = new Haida();
        }
        class HaitianLanguage
        {
            class Haitian : ISOLanguage
            {
                public override string Name => "Haitian";
                public override string TwoLetterISOCode => "ht";
                public override string ThreeLetterISOCode => "hat";
            }
            static HaitianLanguage() { }
            internal static readonly ILanguage instance = new Haitian();
        }
        class HaitianCreoleLanguage
        {
            class HaitianCreole : ISOLanguage
            {
                public override string Name => "Haitian Creole";
                public override string TwoLetterISOCode => "ht";
                public override string ThreeLetterISOCode => "hat";
            }
            static HaitianCreoleLanguage() { }
            internal static readonly ILanguage instance = new HaitianCreole();
        }
        class HausaLanguage
        {
            class Hausa : ISOLanguage
            {
                public override string Name => "Hausa";
                public override string TwoLetterISOCode => "ha";
                public override string ThreeLetterISOCode => "hau";
            }
            static HausaLanguage() { }
            internal static readonly ILanguage instance = new Hausa();
        }
        class HawaiianLanguage
        {
            class Hawaiian : ISOLanguage
            {
                public override string Name => "Hawaiian";
                public override string ThreeLetterISOCode => "haw";
            }
            static HawaiianLanguage() { }
            internal static readonly ILanguage instance = new Hawaiian();
        }
        class HebrewLanguage
        {
            class Hebrew : ISOLanguage
            {
                public override string Name => "Hebrew";
                public override string TwoLetterISOCode => "he";
                public override string ThreeLetterISOCode => "heb";
            }
            static HebrewLanguage() { }
            internal static readonly ILanguage instance = new Hebrew();
        }
        class HereroLanguage
        {
            class Herero : ISOLanguage
            {
                public override string Name => "Herero";
                public override string TwoLetterISOCode => "hz";
                public override string ThreeLetterISOCode => "her";
            }
            static HereroLanguage() { }
            internal static readonly ILanguage instance = new Herero();
        }
        class HiligaynonLanguage
        {
            class Hiligaynon : ISOLanguage
            {
                public override string Name => "Hiligaynon";
                public override string ThreeLetterISOCode => "hil";
            }
            static HiligaynonLanguage() { }
            internal static readonly ILanguage instance = new Hiligaynon();
        }
        class HimachaliLanguagesLanguage
        {
            class HimachaliLanguages : ISOLanguage
            {
                public override string Name => "Himachali languages";
                public override string ThreeLetterISOCode => "him";
            }
            static HimachaliLanguagesLanguage() { }
            internal static readonly ILanguage instance = new HimachaliLanguages();
        }
        class HindiLanguage
        {
            class Hindi : ISOLanguage
            {
                public override string Name => "Hindi";
                public override string TwoLetterISOCode => "hi";
                public override string ThreeLetterISOCode => "hin";
            }
            static HindiLanguage() { }
            internal static readonly ILanguage instance = new Hindi();
        }
        class HiriMotuLanguage
        {
            class HiriMotu : ISOLanguage
            {
                public override string Name => "Hiri Motu";
                public override string TwoLetterISOCode => "ho";
                public override string ThreeLetterISOCode => "hmo";
            }
            static HiriMotuLanguage() { }
            internal static readonly ILanguage instance = new HiriMotu();
        }
        class HittiteLanguage
        {
            class Hittite : ISOLanguage
            {
                public override string Name => "Hittite";
                public override string ThreeLetterISOCode => "hit";
            }
            static HittiteLanguage() { }
            internal static readonly ILanguage instance = new Hittite();
        }
        class HmongLanguage
        {
            class Hmong : ISOLanguage
            {
                public override string Name => "Hmong";
                public override string ThreeLetterISOCode => "hmn";
            }
            static HmongLanguage() { }
            internal static readonly ILanguage instance = new Hmong();
        }
        class HungarianLanguage
        {
            class Hungarian : ISOLanguage
            {
                public override string Name => "Hungarian";
                public override string TwoLetterISOCode => "hu";
                public override string ThreeLetterISOCode => "hun";
            }
            static HungarianLanguage() { }
            internal static readonly ILanguage instance = new Hungarian();
        }
        class HupaLanguage
        {
            class Hupa : ISOLanguage
            {
                public override string Name => "Hupa";
                public override string ThreeLetterISOCode => "hup";
            }
            static HupaLanguage() { }
            internal static readonly ILanguage instance = new Hupa();
        }
        class IbanLanguage
        {
            class Iban : ISOLanguage
            {
                public override string Name => "Iban";
                public override string ThreeLetterISOCode => "iba";
            }
            static IbanLanguage() { }
            internal static readonly ILanguage instance = new Iban();
        }
        class IcelandicLanguage
        {
            class Icelandic : ISOLanguage
            {
                public override string Name => "Icelandic";
                public override string TwoLetterISOCode => "is";
                public override string ThreeLetterISOCode => "ice";
                public override string AlternateThreeLetterISOCode => "isl";
            }
            static IcelandicLanguage() { }
            internal static readonly ILanguage instance = new Icelandic();
        }
        class IdoLanguage
        {
            class Ido : ISOLanguage
            {
                public override string Name => "Ido";
                public override string TwoLetterISOCode => "io";
                public override string ThreeLetterISOCode => "ido";
            }
            static IdoLanguage() { }
            internal static readonly ILanguage instance = new Ido();
        }
        class IgboLanguage
        {
            class Igbo : ISOLanguage
            {
                public override string Name => "Igbo";
                public override string TwoLetterISOCode => "ig";
                public override string ThreeLetterISOCode => "ibo";
            }
            static IgboLanguage() { }
            internal static readonly ILanguage instance = new Igbo();
        }
        class IjoLanguagesLanguage
        {
            class IjoLanguages : ISOLanguage
            {
                public override string Name => "Ijo languages";
                public override string ThreeLetterISOCode => "ijo";
            }
            static IjoLanguagesLanguage() { }
            internal static readonly ILanguage instance = new IjoLanguages();
        }
        class IlokoLanguage
        {
            class Iloko : ISOLanguage
            {
                public override string Name => "Iloko";
                public override string ThreeLetterISOCode => "ilo";
            }
            static IlokoLanguage() { }
            internal static readonly ILanguage instance = new Iloko();
        }
        class ImperialAramaicLanguage
        {
            class ImperialAramaic : ISOLanguage
            {
                public override string Name => "Imperial Aramaic (700-300 BCE)";
                public override string ThreeLetterISOCode => "arc";
            }
            static ImperialAramaicLanguage() { }
            internal static readonly ILanguage instance = new ImperialAramaic();
        }
        class InariSamiLanguage
        {
            class InariSami : ISOLanguage
            {
                public override string Name => "Inari Sami";
                public override string ThreeLetterISOCode => "smn";
            }
            static InariSamiLanguage() { }
            internal static readonly ILanguage instance = new InariSami();
        }
        class IndicLanguagesLanguage
        {
            class IndicLanguages : ISOLanguage
            {
                public override string Name => "Indic languages";
                public override string ThreeLetterISOCode => "inc";
            }
            static IndicLanguagesLanguage() { }
            internal static readonly ILanguage instance = new IndicLanguages();
        }
        class IndoEuropeanLanguagesLanguage
        {
            class IndoEuropeanLanguages : ISOLanguage
            {
                public override string Name => "Indo-European languages";
                public override string ThreeLetterISOCode => "ine";
            }
            static IndoEuropeanLanguagesLanguage() { }
            internal static readonly ILanguage instance = new IndoEuropeanLanguages();
        }
        class IndonesianLanguage
        {
            class Indonesian : ISOLanguage
            {
                public override string Name => "Indonesian";
                public override string TwoLetterISOCode => "id";
                public override string ThreeLetterISOCode => "ind";
            }
            static IndonesianLanguage() { }
            internal static readonly ILanguage instance = new Indonesian();
        }
        class IngushLanguage
        {
            class Ingush : ISOLanguage
            {
                public override string Name => "Ingush";
                public override string ThreeLetterISOCode => "inh";
            }
            static IngushLanguage() { }
            internal static readonly ILanguage instance = new Ingush();
        }
        class InterlinguaLanguage
        {
            class Interlingua : ISOLanguage
            {
                public override string Name => "Interlingua (International Auxiliary Language Association)";
                public override string TwoLetterISOCode => "ia";
                public override string ThreeLetterISOCode => "ina";
            }
            static InterlinguaLanguage() { }
            internal static readonly ILanguage instance = new Interlingua();
        }
        class InterlingueLanguage
        {
            class Interlingue : ISOLanguage
            {
                public override string Name => "Interlingue";
                public override string TwoLetterISOCode => "ie";
                public override string ThreeLetterISOCode => "ile";
            }
            static InterlingueLanguage() { }
            internal static readonly ILanguage instance = new Interlingue();
        }
        class InuktitutLanguage
        {
            class Inuktitut : ISOLanguage
            {
                public override string Name => "Inuktitut";
                public override string TwoLetterISOCode => "iu";
                public override string ThreeLetterISOCode => "iku";
            }
            static InuktitutLanguage() { }
            internal static readonly ILanguage instance = new Inuktitut();
        }
        class InupiaqLanguage
        {
            class Inupiaq : ISOLanguage
            {
                public override string Name => "Inupiaq";
                public override string TwoLetterISOCode => "ik";
                public override string ThreeLetterISOCode => "ipk";
            }
            static InupiaqLanguage() { }
            internal static readonly ILanguage instance = new Inupiaq();
        }
        class IranianLanguagesLanguage
        {
            class IranianLanguages : ISOLanguage
            {
                public override string Name => "Iranian languages";
                public override string ThreeLetterISOCode => "ira";
            }
            static IranianLanguagesLanguage() { }
            internal static readonly ILanguage instance = new IranianLanguages();
        }
        class IrishLanguage
        {
            class Irish : ISOLanguage
            {
                public override string Name => "Irish";
                public override string TwoLetterISOCode => "ga";
                public override string ThreeLetterISOCode => "gle";
            }
            static IrishLanguage() { }
            internal static readonly ILanguage instance = new Irish();
        }
        class IrishMiddleLanguage
        {
            class IrishMiddle : ISOLanguage
            {
                public override string Name => "Irish, Middle (900-1200)";
                public override string ThreeLetterISOCode => "mga";
            }
            static IrishMiddleLanguage() { }
            internal static readonly ILanguage instance = new IrishMiddle();
        }
        class IrishOldLanguage
        {
            class IrishOld : ISOLanguage
            {
                public override string Name => "Irish, Old (to 900)";
                public override string ThreeLetterISOCode => "sga";
            }
            static IrishOldLanguage() { }
            internal static readonly ILanguage instance = new IrishOld();
        }
        class IroquoianLanguagesLanguage
        {
            class IroquoianLanguages : ISOLanguage
            {
                public override string Name => "Iroquoian languages";
                public override string ThreeLetterISOCode => "iro";
            }
            static IroquoianLanguagesLanguage() { }
            internal static readonly ILanguage instance = new IroquoianLanguages();
        }
        class ItalianLanguage
        {
            class Italian : ISOLanguage
            {
                public override string Name => "italiano";
                public override string EnglishName => "Italian";
                public override string TwoLetterISOCode => "it";
                public override string ThreeLetterISOCode => "ita";
            }
            static ItalianLanguage() { }
            internal static readonly ILanguage instance = new Italian();
        }
        class JapaneseLanguage
        {
            class Japanese : ISOLanguage
            {
                public override string Name => "Japanese";
                public override string TwoLetterISOCode => "ja";
                public override string ThreeLetterISOCode => "jpn";
            }
            static JapaneseLanguage() { }
            internal static readonly ILanguage instance = new Japanese();
        }
        class JavaneseLanguage
        {
            class Javanese : ISOLanguage
            {
                public override string Name => "Javanese";
                public override string TwoLetterISOCode => "jv";
                public override string ThreeLetterISOCode => "jav";
            }
            static JavaneseLanguage() { }
            internal static readonly ILanguage instance = new Javanese();
        }
        class JingphoLanguage
        {
            class Jingpho : ISOLanguage
            {
                public override string Name => "Jingpho";
                public override string ThreeLetterISOCode => "kac";
            }
            static JingphoLanguage() { }
            internal static readonly ILanguage instance = new Jingpho();
        }
        class JudeoArabicLanguage
        {
            class JudeoArabic : ISOLanguage
            {
                public override string Name => "Judeo-Arabic";
                public override string ThreeLetterISOCode => "jrb";
            }
            static JudeoArabicLanguage() { }
            internal static readonly ILanguage instance = new JudeoArabic();
        }
        class JudeoPersianLanguage
        {
            class JudeoPersian : ISOLanguage
            {
                public override string Name => "Judeo-Persian";
                public override string ThreeLetterISOCode => "jpr";
            }
            static JudeoPersianLanguage() { }
            internal static readonly ILanguage instance = new JudeoPersian();
        }
        class KabardianLanguage
        {
            class Kabardian : ISOLanguage
            {
                public override string Name => "Kabardian";
                public override string ThreeLetterISOCode => "kbd";
            }
            static KabardianLanguage() { }
            internal static readonly ILanguage instance = new Kabardian();
        }
        class KabyleLanguage
        {
            class Kabyle : ISOLanguage
            {
                public override string Name => "Kabyle";
                public override string ThreeLetterISOCode => "kab";
            }
            static KabyleLanguage() { }
            internal static readonly ILanguage instance = new Kabyle();
        }
        class KachinLanguage
        {
            class Kachin : ISOLanguage
            {
                public override string Name => "Kachin";
                public override string ThreeLetterISOCode => "kac";
            }
            static KachinLanguage() { }
            internal static readonly ILanguage instance = new Kachin();
        }
        class KalaallisutLanguage
        {
            class Kalaallisut : ISOLanguage
            {
                public override string Name => "Kalaallisut";
                public override string TwoLetterISOCode => "kl";
                public override string ThreeLetterISOCode => "kal";
            }
            static KalaallisutLanguage() { }
            internal static readonly ILanguage instance = new Kalaallisut();
        }
        class KalmykLanguage
        {
            class Kalmyk : ISOLanguage
            {
                public override string Name => "Kalmyk";
                public override string ThreeLetterISOCode => "xal";
            }
            static KalmykLanguage() { }
            internal static readonly ILanguage instance = new Kalmyk();
        }
        class KambaLanguage
        {
            class Kamba : ISOLanguage
            {
                public override string Name => "Kamba";
                public override string ThreeLetterISOCode => "kam";
            }
            static KambaLanguage() { }
            internal static readonly ILanguage instance = new Kamba();
        }
        class KannadaLanguage
        {
            class Kannada : ISOLanguage
            {
                public override string Name => "Kannada";
                public override string TwoLetterISOCode => "kn";
                public override string ThreeLetterISOCode => "kan";
            }
            static KannadaLanguage() { }
            internal static readonly ILanguage instance = new Kannada();
        }
        class KanuriLanguage
        {
            class Kanuri : ISOLanguage
            {
                public override string Name => "Kanuri";
                public override string TwoLetterISOCode => "kr";
                public override string ThreeLetterISOCode => "kau";
            }
            static KanuriLanguage() { }
            internal static readonly ILanguage instance = new Kanuri();
        }
        class KapampanganLanguage
        {
            class Kapampangan : ISOLanguage
            {
                public override string Name => "Kapampangan";
                public override string ThreeLetterISOCode => "pam";
            }
            static KapampanganLanguage() { }
            internal static readonly ILanguage instance = new Kapampangan();
        }
        class KaraKalpakLanguage
        {
            class KaraKalpak : ISOLanguage
            {
                public override string Name => "Kara-Kalpak";
                public override string ThreeLetterISOCode => "kaa";
            }
            static KaraKalpakLanguage() { }
            internal static readonly ILanguage instance = new KaraKalpak();
        }
        class KarachayBalkarLanguage
        {
            class KarachayBalkar : ISOLanguage
            {
                public override string Name => "Karachay-Balkar";
                public override string ThreeLetterISOCode => "krc";
            }
            static KarachayBalkarLanguage() { }
            internal static readonly ILanguage instance = new KarachayBalkar();
        }
        class KarelianLanguage
        {
            class Karelian : ISOLanguage
            {
                public override string Name => "Karelian";
                public override string ThreeLetterISOCode => "krl";
            }
            static KarelianLanguage() { }
            internal static readonly ILanguage instance = new Karelian();
        }
        class KarenLanguagesLanguage
        {
            class KarenLanguages : ISOLanguage
            {
                public override string Name => "Karen languages";
                public override string ThreeLetterISOCode => "kar";
            }
            static KarenLanguagesLanguage() { }
            internal static readonly ILanguage instance = new KarenLanguages();
        }
        class KashmiriLanguage
        {
            class Kashmiri : ISOLanguage
            {
                public override string Name => "Kashmiri";
                public override string TwoLetterISOCode => "ks";
                public override string ThreeLetterISOCode => "kas";
            }
            static KashmiriLanguage() { }
            internal static readonly ILanguage instance = new Kashmiri();
        }
        class KashubianLanguage
        {
            class Kashubian : ISOLanguage
            {
                public override string Name => "Kashubian";
                public override string ThreeLetterISOCode => "csb";
            }
            static KashubianLanguage() { }
            internal static readonly ILanguage instance = new Kashubian();
        }
        class KawiLanguage
        {
            class Kawi : ISOLanguage
            {
                public override string Name => "Kawi";
                public override string ThreeLetterISOCode => "kaw";
            }
            static KawiLanguage() { }
            internal static readonly ILanguage instance = new Kawi();
        }
        class KazakhLanguage
        {
            class Kazakh : ISOLanguage
            {
                public override string Name => "Қазақ";
                public override string TransliteratedName => "Qazaq";
                public override string EnglishName => "Kazakh";
                public override string TwoLetterISOCode => "kk";
                public override string ThreeLetterISOCode => "kaz";
            }
            static KazakhLanguage() { }
            internal static readonly ILanguage instance = new Kazakh();
        }
        class KhasiLanguage
        {
            class Khasi : ISOLanguage
            {
                public override string Name => "Khasi";
                public override string ThreeLetterISOCode => "kha";
            }
            static KhasiLanguage() { }
            internal static readonly ILanguage instance = new Khasi();
        }
        class KhoisanLanguagesLanguage
        {
            class KhoisanLanguages : ISOLanguage
            {
                public override string Name => "Khoisan languages";
                public override string ThreeLetterISOCode => "khi";
            }
            static KhoisanLanguagesLanguage() { }
            internal static readonly ILanguage instance = new KhoisanLanguages();
        }
        class KhotaneseLanguage
        {
            class Khotanese : ISOLanguage
            {
                public override string Name => "Khotanese";
                public override string ThreeLetterISOCode => "kho";
            }
            static KhotaneseLanguage() { }
            internal static readonly ILanguage instance = new Khotanese();
        }
        class KikuyuLanguage
        {
            class Kikuyu : ISOLanguage
            {
                public override string Name => "Kikuyu";
                public override string TwoLetterISOCode => "ki";
                public override string ThreeLetterISOCode => "kik";
            }
            static KikuyuLanguage() { }
            internal static readonly ILanguage instance = new Kikuyu();
        }
        class KimbunduLanguage
        {
            class Kimbundu : ISOLanguage
            {
                public override string Name => "Kimbundu";
                public override string ThreeLetterISOCode => "kmb";
            }
            static KimbunduLanguage() { }
            internal static readonly ILanguage instance = new Kimbundu();
        }
        class KinyarwandaLanguage
        {
            class Kinyarwanda : ISOLanguage
            {
                public override string Name => "Kinyarwanda";
                public override string TwoLetterISOCode => "rw";
                public override string ThreeLetterISOCode => "kin";
            }
            static KinyarwandaLanguage() { }
            internal static readonly ILanguage instance = new Kinyarwanda();
        }
        class KirdkiLanguage
        {
            class Kirdki : ISOLanguage
            {
                public override string Name => "Kirdki";
                public override string ThreeLetterISOCode => "zza";
            }
            static KirdkiLanguage() { }
            internal static readonly ILanguage instance = new Kirdki();
        }
        class KirghizLanguage
        {
            class Kirghiz : ISOLanguage
            {
                public override string Name => "Kirghiz";
                public override string TwoLetterISOCode => "ky";
                public override string ThreeLetterISOCode => "kir";
            }
            static KirghizLanguage() { }
            internal static readonly ILanguage instance = new Kirghiz();
        }
        class KirmanjkiLanguage
        {
            class Kirmanjki : ISOLanguage
            {
                public override string Name => "Kirmanjki";
                public override string ThreeLetterISOCode => "zza";
            }
            static KirmanjkiLanguage() { }
            internal static readonly ILanguage instance = new Kirmanjki();
        }
        class KlingonLanguage
        {
            class Klingon : ISOLanguage
            {
                public override string Name => "tlhIngan-Hol";
                public override string EnglishName => "Klingon";
                public override string ThreeLetterISOCode => "tlh";
                public override string PrivateUseCharacterName => " ";
                public override string PrivateUseCharacterNameFontName => "pIqaD";
            }
            static KlingonLanguage() { }
            internal static readonly ILanguage instance = new Klingon();
        }
        class KomiLanguage
        {
            class Komi : ISOLanguage
            {
                public override string Name => "Komi";
                public override string TwoLetterISOCode => "kv";
                public override string ThreeLetterISOCode => "kom";
            }
            static KomiLanguage() { }
            internal static readonly ILanguage instance = new Komi();
        }
        class KongoLanguage
        {
            class Kongo : ISOLanguage
            {
                public override string Name => "Kongo";
                public override string TwoLetterISOCode => "kg";
                public override string ThreeLetterISOCode => "kon";
            }
            static KongoLanguage() { }
            internal static readonly ILanguage instance = new Kongo();
        }
        class KonkaniLanguage
        {
            class Konkani : ISOLanguage
            {
                public override string Name => "Konkani";
                public override string ThreeLetterISOCode => "kok";
            }
            static KonkaniLanguage() { }
            internal static readonly ILanguage instance = new Konkani();
        }
        class KoreanLanguage
        {
            class Korean : ISOLanguage
            {
                public override string Name => "Korean";
                public override string TwoLetterISOCode => "ko";
                public override string ThreeLetterISOCode => "kor";
            }
            static KoreanLanguage() { }
            internal static readonly ILanguage instance = new Korean();
        }
        class KosraeanLanguage
        {
            class Kosraean : ISOLanguage
            {
                public override string Name => "Kosraean";
                public override string ThreeLetterISOCode => "kos";
            }
            static KosraeanLanguage() { }
            internal static readonly ILanguage instance = new Kosraean();
        }
        class KpelleLanguage
        {
            class Kpelle : ISOLanguage
            {
                public override string Name => "Kpelle";
                public override string ThreeLetterISOCode => "kpe";
            }
            static KpelleLanguage() { }
            internal static readonly ILanguage instance = new Kpelle();
        }
        class KruLanguagesLanguage
        {
            class KruLanguages : ISOLanguage
            {
                public override string Name => "Kru languages";
                public override string ThreeLetterISOCode => "kro";
            }
            static KruLanguagesLanguage() { }
            internal static readonly ILanguage instance = new KruLanguages();
        }
        class KuanyamaLanguage
        {
            class Kuanyama : ISOLanguage
            {
                public override string Name => "Kuanyama";
                public override string TwoLetterISOCode => "kj";
                public override string ThreeLetterISOCode => "kua";
            }
            static KuanyamaLanguage() { }
            internal static readonly ILanguage instance = new Kuanyama();
        }
        class KumykLanguage
        {
            class Kumyk : ISOLanguage
            {
                public override string Name => "Kumyk";
                public override string ThreeLetterISOCode => "kum";
            }
            static KumykLanguage() { }
            internal static readonly ILanguage instance = new Kumyk();
        }
        class KurdishLanguage
        {
            class Kurdish : ISOLanguage
            {
                public override string Name => "Kurdish";
                public override string TwoLetterISOCode => "ku";
                public override string ThreeLetterISOCode => "kur";
            }
            static KurdishLanguage() { }
            internal static readonly ILanguage instance = new Kurdish();
        }
        class KurukhLanguage
        {
            class Kurukh : ISOLanguage
            {
                public override string Name => "Kurukh";
                public override string ThreeLetterISOCode => "kru";
            }
            static KurukhLanguage() { }
            internal static readonly ILanguage instance = new Kurukh();
        }
        class KutenaiLanguage
        {
            class Kutenai : ISOLanguage
            {
                public override string Name => "Kutenai";
                public override string ThreeLetterISOCode => "kut";
            }
            static KutenaiLanguage() { }
            internal static readonly ILanguage instance = new Kutenai();
        }
        class KwanyamaLanguage
        {
            class Kwanyama : ISOLanguage
            {
                public override string Name => "Kwanyama";
                public override string TwoLetterISOCode => "kj";
                public override string ThreeLetterISOCode => "kua";
            }
            static KwanyamaLanguage() { }
            internal static readonly ILanguage instance = new Kwanyama();
        }
        class KyrgyzLanguage
        {
            class Kyrgyz : ISOLanguage
            {
                public override string Name => "Kyrgyz";
                public override string TwoLetterISOCode => "ky";
                public override string ThreeLetterISOCode => "kir";
            }
            static KyrgyzLanguage() { }
            internal static readonly ILanguage instance = new Kyrgyz();
        }
        class LadinoLanguage
        {
            class Ladino : ISOLanguage
            {
                public override string Name => "Ladino";
                public override string ThreeLetterISOCode => "lad";
            }
            static LadinoLanguage() { }
            internal static readonly ILanguage instance = new Ladino();
        }
        class LahndaLanguage
        {
            class Lahnda : ISOLanguage
            {
                public override string Name => "Lahnda";
                public override string ThreeLetterISOCode => "lah";
            }
            static LahndaLanguage() { }
            internal static readonly ILanguage instance = new Lahnda();
        }
        class LambaLanguage
        {
            class Lamba : ISOLanguage
            {
                public override string Name => "Lamba";
                public override string ThreeLetterISOCode => "lam";
            }
            static LambaLanguage() { }
            internal static readonly ILanguage instance = new Lamba();
        }
        class LandDayakLanguagesLanguage
        {
            class LandDayakLanguages : ISOLanguage
            {
                public override string Name => "Land Dayak languages";
                public override string ThreeLetterISOCode => "day";
            }
            static LandDayakLanguagesLanguage() { }
            internal static readonly ILanguage instance = new LandDayakLanguages();
        }
        class LaoLanguage
        {
            class Lao : ISOLanguage
            {
                public override string Name => "Lao";
                public override string TwoLetterISOCode => "lo";
                public override string ThreeLetterISOCode => "lao";
            }
            static LaoLanguage() { }
            internal static readonly ILanguage instance = new Lao();
        }
        class LatinLanguage
        {
            class Latin : ISOLanguage
            {
                public override string Name => "latīna";
                public override string EnglishName => "Latin";
                public override string TwoLetterISOCode => "la";
                public override string ThreeLetterISOCode => "lat";
            }
            static LatinLanguage() { }
            internal static readonly ILanguage instance = new Latin();
        }
        class LatvianLanguage
        {
            class Latvian : ISOLanguage
            {
                public override string Name => "Latvian";
                public override string TwoLetterISOCode => "lv";
                public override string ThreeLetterISOCode => "lav";
            }
            static LatvianLanguage() { }
            internal static readonly ILanguage instance = new Latvian();
        }
        class LeoneseLanguage
        {
            class Leonese : ISOLanguage
            {
                public override string Name => "Leonese";
                public override string ThreeLetterISOCode => "ast";
            }
            static LeoneseLanguage() { }
            internal static readonly ILanguage instance = new Leonese();
        }
        class LetzeburgeschLanguage
        {
            class Letzeburgesch : ISOLanguage
            {
                public override string Name => "Letzeburgesch";
                public override string TwoLetterISOCode => "lb";
                public override string ThreeLetterISOCode => "ltz";
            }
            static LetzeburgeschLanguage() { }
            internal static readonly ILanguage instance = new Letzeburgesch();
        }
        class LezghianLanguage
        {
            class Lezghian : ISOLanguage
            {
                public override string Name => "Lezghian";
                public override string ThreeLetterISOCode => "lez";
            }
            static LezghianLanguage() { }
            internal static readonly ILanguage instance = new Lezghian();
        }
        class LimburganLanguage
        {
            class Limburgan : ISOLanguage
            {
                public override string Name => "Limburgan";
                public override string TwoLetterISOCode => "li";
                public override string ThreeLetterISOCode => "lim";
            }
            static LimburganLanguage() { }
            internal static readonly ILanguage instance = new Limburgan();
        }
        class LimburgerLanguage
        {
            class Limburger : ISOLanguage
            {
                public override string Name => "Limburger";
                public override string TwoLetterISOCode => "li";
                public override string ThreeLetterISOCode => "lim";
            }
            static LimburgerLanguage() { }
            internal static readonly ILanguage instance = new Limburger();
        }
        class LimburgishLanguage
        {
            class Limburgish : ISOLanguage
            {
                public override string Name => "Limburgish";
                public override string TwoLetterISOCode => "li";
                public override string ThreeLetterISOCode => "lim";
            }
            static LimburgishLanguage() { }
            internal static readonly ILanguage instance = new Limburgish();
        }
        class LingalaLanguage
        {
            class Lingala : ISOLanguage
            {
                public override string Name => "Lingala";
                public override string TwoLetterISOCode => "ln";
                public override string ThreeLetterISOCode => "lin";
            }
            static LingalaLanguage() { }
            internal static readonly ILanguage instance = new Lingala();
        }
        class LithuanianLanguage
        {
            class Lithuanian : ISOLanguage
            {
                public override string Name => "Lithuanian";
                public override string TwoLetterISOCode => "lt";
                public override string ThreeLetterISOCode => "lit";
            }
            static LithuanianLanguage() { }
            internal static readonly ILanguage instance = new Lithuanian();
        }
        class LojbanLanguage
        {
            class Lojban : ISOLanguage
            {
                public override string Name => "Lojban";
                public override string ThreeLetterISOCode => "jbo";
            }
            static LojbanLanguage() { }
            internal static readonly ILanguage instance = new Lojban();
        }
        class LowGermanLanguage
        {
            class LowGerman : ISOLanguage
            {
                public override string Name => "Low German";
                public override string ThreeLetterISOCode => "nds";
            }
            static LowGermanLanguage() { }
            internal static readonly ILanguage instance = new LowGerman();
        }
        class LowSaxonLanguage
        {
            class LowSaxon : ISOLanguage
            {
                public override string Name => "Low Saxon";
                public override string ThreeLetterISOCode => "nds";
            }
            static LowSaxonLanguage() { }
            internal static readonly ILanguage instance = new LowSaxon();
        }
        class LowerSorbianLanguage
        {
            class LowerSorbian : ISOLanguage
            {
                public override string Name => "Lower Sorbian";
                public override string ThreeLetterISOCode => "dsb";
            }
            static LowerSorbianLanguage() { }
            internal static readonly ILanguage instance = new LowerSorbian();
        }
        class LoziLanguage
        {
            class Lozi : ISOLanguage
            {
                public override string Name => "Lozi";
                public override string ThreeLetterISOCode => "loz";
            }
            static LoziLanguage() { }
            internal static readonly ILanguage instance = new Lozi();
        }
        class LubaKatangaLanguage
        {
            class LubaKatanga : ISOLanguage
            {
                public override string Name => "Luba-Katanga";
                public override string TwoLetterISOCode => "lu";
                public override string ThreeLetterISOCode => "lub";
            }
            static LubaKatangaLanguage() { }
            internal static readonly ILanguage instance = new LubaKatanga();
        }
        class LubaLuluaLanguage
        {
            class LubaLulua : ISOLanguage
            {
                public override string Name => "Luba-Lulua";
                public override string ThreeLetterISOCode => "lua";
            }
            static LubaLuluaLanguage() { }
            internal static readonly ILanguage instance = new LubaLulua();
        }
        class LuisenoLanguage
        {
            class Luiseno : ISOLanguage
            {
                public override string Name => "Luiseno";
                public override string ThreeLetterISOCode => "lui";
            }
            static LuisenoLanguage() { }
            internal static readonly ILanguage instance = new Luiseno();
        }
        class LuleSamiLanguage
        {
            class LuleSami : ISOLanguage
            {
                public override string Name => "Lule Sami";
                public override string ThreeLetterISOCode => "smj";
            }
            static LuleSamiLanguage() { }
            internal static readonly ILanguage instance = new LuleSami();
        }
        class LundaLanguage
        {
            class Lunda : ISOLanguage
            {
                public override string Name => "Lunda";
                public override string ThreeLetterISOCode => "lun";
            }
            static LundaLanguage() { }
            internal static readonly ILanguage instance = new Lunda();
        }
        class LuoLanguage
        {
            class Luo : ISOLanguage
            {
                public override string Name => "Luo (Kenya and Tanzania)";
                public override string ThreeLetterISOCode => "luo";
            }
            static LuoLanguage() { }
            internal static readonly ILanguage instance = new Luo();
        }
        class LushaiLanguage
        {
            class Lushai : ISOLanguage
            {
                public override string Name => "Lushai";
                public override string ThreeLetterISOCode => "lus";
            }
            static LushaiLanguage() { }
            internal static readonly ILanguage instance = new Lushai();
        }
        class LuxembourgishLanguage
        {
            class Luxembourgish : ISOLanguage
            {
                public override string Name => "Luxembourgish";
                public override string TwoLetterISOCode => "lb";
                public override string ThreeLetterISOCode => "ltz";
            }
            static LuxembourgishLanguage() { }
            internal static readonly ILanguage instance = new Luxembourgish();
        }
        class MacedoRomanianLanguage
        {
            class MacedoRomanian : ISOLanguage
            {
                public override string Name => "Macedo-Romanian";
                public override string ThreeLetterISOCode => "rup";
            }
            static MacedoRomanianLanguage() { }
            internal static readonly ILanguage instance = new MacedoRomanian();
        }
        class MacedonianLanguage
        {
            class Macedonian : ISOLanguage
            {
                public override string Name => "Macedonian";
                public override string TwoLetterISOCode => "mk";
                public override string ThreeLetterISOCode => "mac";
                public override string AlternateThreeLetterISOCode => "mkd";
            }
            static MacedonianLanguage() { }
            internal static readonly ILanguage instance = new Macedonian();
        }
        class MadureseLanguage
        {
            class Madurese : ISOLanguage
            {
                public override string Name => "Madurese";
                public override string ThreeLetterISOCode => "mad";
            }
            static MadureseLanguage() { }
            internal static readonly ILanguage instance = new Madurese();
        }
        class MagahiLanguage
        {
            class Magahi : ISOLanguage
            {
                public override string Name => "Magahi";
                public override string ThreeLetterISOCode => "mag";
            }
            static MagahiLanguage() { }
            internal static readonly ILanguage instance = new Magahi();
        }
        class MaithiliLanguage
        {
            class Maithili : ISOLanguage
            {
                public override string Name => "Maithili";
                public override string ThreeLetterISOCode => "mai";
            }
            static MaithiliLanguage() { }
            internal static readonly ILanguage instance = new Maithili();
        }
        class MakasarLanguage
        {
            class Makasar : ISOLanguage
            {
                public override string Name => "Makasar";
                public override string ThreeLetterISOCode => "mak";
            }
            static MakasarLanguage() { }
            internal static readonly ILanguage instance = new Makasar();
        }
        class MalagasyLanguage
        {
            class Malagasy : ISOLanguage
            {
                public override string Name => "Malagasy";
                public override string TwoLetterISOCode => "mg";
                public override string ThreeLetterISOCode => "mlg";
            }
            static MalagasyLanguage() { }
            internal static readonly ILanguage instance = new Malagasy();
        }
        class MalayLanguage
        {
            class Malay : ISOLanguage
            {
                public override string Name => "Malay";
                public override string TwoLetterISOCode => "ms";
                public override string ThreeLetterISOCode => "may";
                public override string AlternateThreeLetterISOCode => "msa";
            }
            static MalayLanguage() { }
            internal static readonly ILanguage instance = new Malay();
        }
        class MalayalamLanguage
        {
            class Malayalam : ISOLanguage
            {
                public override string Name => "Malayalam";
                public override string TwoLetterISOCode => "ml";
                public override string ThreeLetterISOCode => "mal";
            }
            static MalayalamLanguage() { }
            internal static readonly ILanguage instance = new Malayalam();
        }
        class MaldivianLanguage
        {
            class Maldivian : ISOLanguage
            {
                public override string Name => "Maldivian";
                public override string TwoLetterISOCode => "dv";
                public override string ThreeLetterISOCode => "div";
            }
            static MaldivianLanguage() { }
            internal static readonly ILanguage instance = new Maldivian();
        }
        class MalteseLanguage
        {
            class Maltese : ISOLanguage
            {
                public override string Name => "Maltese";
                public override string TwoLetterISOCode => "mt";
                public override string ThreeLetterISOCode => "mlt";
            }
            static MalteseLanguage() { }
            internal static readonly ILanguage instance = new Maltese();
        }
        class ManchuLanguage
        {
            class Manchu : ISOLanguage
            {
                public override string Name => "Manchu";
                public override string ThreeLetterISOCode => "mnc";
            }
            static ManchuLanguage() { }
            internal static readonly ILanguage instance = new Manchu();
        }
        class MandarLanguage
        {
            class Mandar : ISOLanguage
            {
                public override string Name => "Mandar";
                public override string ThreeLetterISOCode => "mdr";
            }
            static MandarLanguage() { }
            internal static readonly ILanguage instance = new Mandar();
        }
        class MandingoLanguage
        {
            class Mandingo : ISOLanguage
            {
                public override string Name => "Mandingo";
                public override string ThreeLetterISOCode => "man";
            }
            static MandingoLanguage() { }
            internal static readonly ILanguage instance = new Mandingo();
        }
        class ManipuriLanguage
        {
            class Manipuri : ISOLanguage
            {
                public override string Name => "Manipuri";
                public override string ThreeLetterISOCode => "mni";
            }
            static ManipuriLanguage() { }
            internal static readonly ILanguage instance = new Manipuri();
        }
        class ManoboLanguagesLanguage
        {
            class ManoboLanguages : ISOLanguage
            {
                public override string Name => "Manobo languages";
                public override string ThreeLetterISOCode => "mno";
            }
            static ManoboLanguagesLanguage() { }
            internal static readonly ILanguage instance = new ManoboLanguages();
        }
        class ManxLanguage
        {
            class Manx : ISOLanguage
            {
                public override string Name => "Manx";
                public override string TwoLetterISOCode => "gv";
                public override string ThreeLetterISOCode => "glv";
            }
            static ManxLanguage() { }
            internal static readonly ILanguage instance = new Manx();
        }
        class MaoriLanguage
        {
            class Maori : ISOLanguage
            {
                public override string Name => "Te Reo";
                public override string EnglishName => "Maori";
                public override string TwoLetterISOCode => "mi";
                public override string ThreeLetterISOCode => "mao";
                public override string AlternateThreeLetterISOCode => "mri";
            }
            static MaoriLanguage() { }
            internal static readonly ILanguage instance = new Maori();
        }
        class MapucheLanguage
        {
            class Mapuche : ISOLanguage
            {
                public override string Name => "Mapuche";
                public override string ThreeLetterISOCode => "arn";
            }
            static MapucheLanguage() { }
            internal static readonly ILanguage instance = new Mapuche();
        }
        class MapudungunLanguage
        {
            class Mapudungun : ISOLanguage
            {
                public override string Name => "Mapudungun";
                public override string ThreeLetterISOCode => "arn";
            }
            static MapudungunLanguage() { }
            internal static readonly ILanguage instance = new Mapudungun();
        }
        class MarathiLanguage
        {
            class Marathi : ISOLanguage
            {
                public override string Name => "Marathi";
                public override string TwoLetterISOCode => "mr";
                public override string ThreeLetterISOCode => "mar";
            }
            static MarathiLanguage() { }
            internal static readonly ILanguage instance = new Marathi();
        }
        class MariLanguage
        {
            class Mari : ISOLanguage
            {
                public override string Name => "Mari";
                public override string ThreeLetterISOCode => "chm";
            }
            static MariLanguage() { }
            internal static readonly ILanguage instance = new Mari();
        }
        class MarshalleseLanguage
        {
            class Marshallese : ISOLanguage
            {
                public override string Name => "Marshallese";
                public override string TwoLetterISOCode => "mh";
                public override string ThreeLetterISOCode => "mah";
            }
            static MarshalleseLanguage() { }
            internal static readonly ILanguage instance = new Marshallese();
        }
        class MarwariLanguage
        {
            class Marwari : ISOLanguage
            {
                public override string Name => "Marwari";
                public override string ThreeLetterISOCode => "mwr";
            }
            static MarwariLanguage() { }
            internal static readonly ILanguage instance = new Marwari();
        }
        class MasaiLanguage
        {
            class Masai : ISOLanguage
            {
                public override string Name => "Masai";
                public override string ThreeLetterISOCode => "mas";
            }
            static MasaiLanguage() { }
            internal static readonly ILanguage instance = new Masai();
        }
        class MayanLanguagesLanguage
        {
            class MayanLanguages : ISOLanguage
            {
                public override string Name => "Mayan languages";
                public override string ThreeLetterISOCode => "myn";
            }
            static MayanLanguagesLanguage() { }
            internal static readonly ILanguage instance = new MayanLanguages();
        }
        class MendeLanguage
        {
            class Mende : ISOLanguage
            {
                public override string Name => "Mende";
                public override string ThreeLetterISOCode => "men";
            }
            static MendeLanguage() { }
            internal static readonly ILanguage instance = new Mende();
        }
        class MikmaqLanguage
        {
            class Mikmaq : ISOLanguage
            {
                public override string Name => "Mi'kmaq";
                public override string ThreeLetterISOCode => "mic";
            }
            static MikmaqLanguage() { }
            internal static readonly ILanguage instance = new Mikmaq();
        }
        class MicmacLanguage
        {
            class Micmac : ISOLanguage
            {
                public override string Name => "Micmac";
                public override string ThreeLetterISOCode => "mic";
            }
            static MicmacLanguage() { }
            internal static readonly ILanguage instance = new Micmac();
        }
        class MinangkabauLanguage
        {
            class Minangkabau : ISOLanguage
            {
                public override string Name => "Minangkabau";
                public override string ThreeLetterISOCode => "min";
            }
            static MinangkabauLanguage() { }
            internal static readonly ILanguage instance = new Minangkabau();
        }
        class MirandeseLanguage
        {
            class Mirandese : ISOLanguage
            {
                public override string Name => "Mirandese";
                public override string ThreeLetterISOCode => "mwl";
            }
            static MirandeseLanguage() { }
            internal static readonly ILanguage instance = new Mirandese();
        }
        class MohawkLanguage
        {
            class Mohawk : ISOLanguage
            {
                public override string Name => "Mohawk";
                public override string ThreeLetterISOCode => "moh";
            }
            static MohawkLanguage() { }
            internal static readonly ILanguage instance = new Mohawk();
        }
        class MokshaLanguage
        {
            class Moksha : ISOLanguage
            {
                public override string Name => "Moksha";
                public override string ThreeLetterISOCode => "mdf";
            }
            static MokshaLanguage() { }
            internal static readonly ILanguage instance = new Moksha();
        }
        class MoldavianLanguage
        {
            class Moldavian : ISOLanguage
            {
                public override string Name => "Moldavian";
                public override string TwoLetterISOCode => "ro";
                public override string ThreeLetterISOCode => "rum";
                public override string AlternateThreeLetterISOCode => "ron";
            }
            static MoldavianLanguage() { }
            internal static readonly ILanguage instance = new Moldavian();
        }
        class MoldovanLanguage
        {
            class Moldovan : ISOLanguage
            {
                public override string Name => "Moldovan";
                public override string TwoLetterISOCode => "ro";
                public override string ThreeLetterISOCode => "rum";
                public override string AlternateThreeLetterISOCode => "ron";
            }
            static MoldovanLanguage() { }
            internal static readonly ILanguage instance = new Moldovan();
        }
        class MonKhmerLanguagesLanguage
        {
            class MonKhmerLanguages : ISOLanguage
            {
                public override string Name => "Mon-Khmer languages";
                public override string ThreeLetterISOCode => "mkh";
            }
            static MonKhmerLanguagesLanguage() { }
            internal static readonly ILanguage instance = new MonKhmerLanguages();
        }
        class MongLanguage
        {
            class Mong : ISOLanguage
            {
                public override string Name => "Mong";
                public override string ThreeLetterISOCode => "hmn";
            }
            static MongLanguage() { }
            internal static readonly ILanguage instance = new Mong();
        }
        class MongoLanguage
        {
            class Mongo : ISOLanguage
            {
                public override string Name => "Mongo";
                public override string ThreeLetterISOCode => "lol";
            }
            static MongoLanguage() { }
            internal static readonly ILanguage instance = new Mongo();
        }
        class MongolianLanguage
        {
            class Mongolian : ISOLanguage
            {
                public override string Name => "Mongolian";
                public override string TwoLetterISOCode => "mn";
                public override string ThreeLetterISOCode => "mon";
            }
            static MongolianLanguage() { }
            internal static readonly ILanguage instance = new Mongolian();
        }
        class MossiLanguage
        {
            class Mossi : ISOLanguage
            {
                public override string Name => "Mossi";
                public override string ThreeLetterISOCode => "mos";
            }
            static MossiLanguage() { }
            internal static readonly ILanguage instance = new Mossi();
        }
        class MultipleLanguagesLanguage
        {
            class MultipleLanguages : ISOLanguage
            {
                public override string Name => "Multiple languages";
                public override string ThreeLetterISOCode => "mul";
            }
            static MultipleLanguagesLanguage() { }
            internal static readonly ILanguage instance = new MultipleLanguages();
        }
        class MundaLanguagesLanguage
        {
            class MundaLanguages : ISOLanguage
            {
                public override string Name => "Munda languages";
                public override string ThreeLetterISOCode => "mun";
            }
            static MundaLanguagesLanguage() { }
            internal static readonly ILanguage instance = new MundaLanguages();
        }
        class NKoLanguage
        {
            class NKo : ISOLanguage
            {
                public override string Name => "N'Ko";
                public override string ThreeLetterISOCode => "nqo";
            }
            static NKoLanguage() { }
            internal static readonly ILanguage instance = new NKo();
        }
        class NahuatlLanguagesLanguage
        {
            class NahuatlLanguages : ISOLanguage
            {
                public override string Name => "Nahuatl languages";
                public override string ThreeLetterISOCode => "nah";
            }
            static NahuatlLanguagesLanguage() { }
            internal static readonly ILanguage instance = new NahuatlLanguages();
        }
        class NauruLanguage
        {
            class Nauru : ISOLanguage
            {
                public override string Name => "Nauru";
                public override string TwoLetterISOCode => "na";
                public override string ThreeLetterISOCode => "nau";
            }
            static NauruLanguage() { }
            internal static readonly ILanguage instance = new Nauru();
        }
        class NavahoLanguage
        {
            class Navaho : ISOLanguage
            {
                public override string Name => "Navaho";
                public override string TwoLetterISOCode => "nv";
                public override string ThreeLetterISOCode => "nav";
            }
            static NavahoLanguage() { }
            internal static readonly ILanguage instance = new Navaho();
        }
        class NavajoLanguage
        {
            class Navajo : ISOLanguage
            {
                public override string Name => "Navajo";
                public override string TwoLetterISOCode => "nv";
                public override string ThreeLetterISOCode => "nav";
            }
            static NavajoLanguage() { }
            internal static readonly ILanguage instance = new Navajo();
        }
        class NdebeleNorthLanguage
        {
            class NdebeleNorth : ISOLanguage
            {
                public override string Name => "Ndebele, North";
                public override string TwoLetterISOCode => "nd";
                public override string ThreeLetterISOCode => "nde";
            }
            static NdebeleNorthLanguage() { }
            internal static readonly ILanguage instance = new NdebeleNorth();
        }
        class NdebeleSouthLanguage
        {
            class NdebeleSouth : ISOLanguage
            {
                public override string Name => "Ndebele, South";
                public override string TwoLetterISOCode => "nr";
                public override string ThreeLetterISOCode => "nbl";
            }
            static NdebeleSouthLanguage() { }
            internal static readonly ILanguage instance = new NdebeleSouth();
        }
        class NdongaLanguage
        {
            class Ndonga : ISOLanguage
            {
                public override string Name => "Ndonga";
                public override string TwoLetterISOCode => "ng";
                public override string ThreeLetterISOCode => "ndo";
            }
            static NdongaLanguage() { }
            internal static readonly ILanguage instance = new Ndonga();
        }
        class NeapolitanLanguage
        {
            class Neapolitan : ISOLanguage
            {
                public override string Name => "Neapolitan";
                public override string ThreeLetterISOCode => "nap";
            }
            static NeapolitanLanguage() { }
            internal static readonly ILanguage instance = new Neapolitan();
        }
        class NepalBhasaLanguage
        {
            class NepalBhasa : ISOLanguage
            {
                public override string Name => "Nepal Bhasa";
                public override string ThreeLetterISOCode => "new";
            }
            static NepalBhasaLanguage() { }
            internal static readonly ILanguage instance = new NepalBhasa();
        }
        class NepaliLanguage
        {
            class Nepali : ISOLanguage
            {
                public override string Name => "Nepali";
                public override string TwoLetterISOCode => "ne";
                public override string ThreeLetterISOCode => "nep";
            }
            static NepaliLanguage() { }
            internal static readonly ILanguage instance = new Nepali();
        }
        class NewariLanguage
        {
            class Newari : ISOLanguage
            {
                public override string Name => "Newari";
                public override string ThreeLetterISOCode => "new";
            }
            static NewariLanguage() { }
            internal static readonly ILanguage instance = new Newari();
        }
        class NiasLanguage
        {
            class Nias : ISOLanguage
            {
                public override string Name => "Nias";
                public override string ThreeLetterISOCode => "nia";
            }
            static NiasLanguage() { }
            internal static readonly ILanguage instance = new Nias();
        }
        class NigerKordofanianLanguagesLanguage
        {
            class NigerKordofanianLanguages : ISOLanguage
            {
                public override string Name => "Niger-Kordofanian languages";
                public override string ThreeLetterISOCode => "nic";
            }
            static NigerKordofanianLanguagesLanguage() { }
            internal static readonly ILanguage instance = new NigerKordofanianLanguages();
        }
        class NiloSaharanLanguagesLanguage
        {
            class NiloSaharanLanguages : ISOLanguage
            {
                public override string Name => "Nilo-Saharan languages";
                public override string ThreeLetterISOCode => "ssa";
            }
            static NiloSaharanLanguagesLanguage() { }
            internal static readonly ILanguage instance = new NiloSaharanLanguages();
        }
        class NiueanLanguage
        {
            class Niuean : ISOLanguage
            {
                public override string Name => "Niuean";
                public override string ThreeLetterISOCode => "niu";
            }
            static NiueanLanguage() { }
            internal static readonly ILanguage instance = new Niuean();
        }
        class NoLinguisticContentLanguage
        {
            class NoLinguisticContent : ISOLanguage
            {
                public override string Name => "No linguistic content";
                public override string ThreeLetterISOCode => "zxx";
            }
            static NoLinguisticContentLanguage() { }
            internal static readonly ILanguage instance = new NoLinguisticContent();
        }
        class NogaiLanguage
        {
            class Nogai : ISOLanguage
            {
                public override string Name => "Nogai";
                public override string ThreeLetterISOCode => "nog";
            }
            static NogaiLanguage() { }
            internal static readonly ILanguage instance = new Nogai();
        }
        class NorseOldLanguage
        {
            class NorseOld : ISOLanguage
            {
                public override string Name => "Norse, Old";
                public override string ThreeLetterISOCode => "non";
            }
            static NorseOldLanguage() { }
            internal static readonly ILanguage instance = new NorseOld();
        }
        class NorthAmericanIndianLanguagesLanguage
        {
            class NorthAmericanIndianLanguages : ISOLanguage
            {
                public override string Name => "North American Indian languages";
                public override string ThreeLetterISOCode => "nai";
            }
            static NorthAmericanIndianLanguagesLanguage() { }
            internal static readonly ILanguage instance = new NorthAmericanIndianLanguages();
        }
        class NorthNdebeleLanguage
        {
            class NorthNdebele : ISOLanguage
            {
                public override string Name => "North Ndebele";
                public override string TwoLetterISOCode => "nd";
                public override string ThreeLetterISOCode => "nde";
            }
            static NorthNdebeleLanguage() { }
            internal static readonly ILanguage instance = new NorthNdebele();
        }
        class NorthernFrisianLanguage
        {
            class NorthernFrisian : ISOLanguage
            {
                public override string Name => "Northern Frisian";
                public override string ThreeLetterISOCode => "frr";
            }
            static NorthernFrisianLanguage() { }
            internal static readonly ILanguage instance = new NorthernFrisian();
        }
        class NorthernSamiLanguage
        {
            class NorthernSami : ISOLanguage
            {
                public override string Name => "Northern Sami";
                public override string TwoLetterISOCode => "se";
                public override string ThreeLetterISOCode => "sme";
            }
            static NorthernSamiLanguage() { }
            internal static readonly ILanguage instance = new NorthernSami();
        }
        class NorthernSothoLanguage
        {
            class NorthernSotho : ISOLanguage
            {
                public override string Name => "Northern Sotho";
                public override string ThreeLetterISOCode => "nso";
            }
            static NorthernSothoLanguage() { }
            internal static readonly ILanguage instance = new NorthernSotho();
        }
        class NorwegianLanguage
        {
            class Norwegian : ISOLanguage
            {
                public override string Name => "Norwegian";
                public override string TwoLetterISOCode => "no";
                public override string ThreeLetterISOCode => "nor";
            }
            static NorwegianLanguage() { }
            internal static readonly ILanguage instance = new Norwegian();
        }
        class NorwegianBokmalLanguage
        {
            class NorwegianBokmal : ISOLanguage
            {
                public override string Name => "Norwegian Bokmål";
                public override string TwoLetterISOCode => "nb";
                public override string ThreeLetterISOCode => "nob";
            }
            static NorwegianBokmalLanguage() { }
            internal static readonly ILanguage instance = new NorwegianBokmal();
        }
        class NorwegianNynorskLanguage
        {
            class NorwegianNynorsk : ISOLanguage
            {
                public override string Name => "Norwegian Nynorsk";
                public override string TwoLetterISOCode => "nn";
                public override string ThreeLetterISOCode => "nno";
            }
            static NorwegianNynorskLanguage() { }
            internal static readonly ILanguage instance = new NorwegianNynorsk();
        }
        class NotApplicableLanguage
        {
            class NotApplicable : ISOLanguage
            {
                public override string Name => "Not applicable";
                public override string ThreeLetterISOCode => "zxx";
            }
            static NotApplicableLanguage() { }
            internal static readonly ILanguage instance = new NotApplicable();
        }
        class NubianLanguagesLanguage
        {
            class NubianLanguages : ISOLanguage
            {
                public override string Name => "Nubian languages";
                public override string ThreeLetterISOCode => "nub";
            }
            static NubianLanguagesLanguage() { }
            internal static readonly ILanguage instance = new NubianLanguages();
        }
        class NuosuLanguage
        {
            class Nuosu : ISOLanguage
            {
                public override string Name => "Nuosu";
                public override string TwoLetterISOCode => "ii";
                public override string ThreeLetterISOCode => "iii";
            }
            static NuosuLanguage() { }
            internal static readonly ILanguage instance = new Nuosu();
        }
        class NyamweziLanguage
        {
            class Nyamwezi : ISOLanguage
            {
                public override string Name => "Nyamwezi";
                public override string ThreeLetterISOCode => "nym";
            }
            static NyamweziLanguage() { }
            internal static readonly ILanguage instance = new Nyamwezi();
        }
        class NyanjaLanguage
        {
            class Nyanja : ISOLanguage
            {
                public override string Name => "Nyanja";
                public override string TwoLetterISOCode => "ny";
                public override string ThreeLetterISOCode => "nya";
            }
            static NyanjaLanguage() { }
            internal static readonly ILanguage instance = new Nyanja();
        }
        class NyankoleLanguage
        {
            class Nyankole : ISOLanguage
            {
                public override string Name => "Nyankole";
                public override string ThreeLetterISOCode => "nyn";
            }
            static NyankoleLanguage() { }
            internal static readonly ILanguage instance = new Nyankole();
        }
        class NynorskNorwegianLanguage
        {
            class NynorskNorwegian : ISOLanguage
            {
                public override string Name => "Nynorsk, Norwegian";
                public override string TwoLetterISOCode => "nn";
                public override string ThreeLetterISOCode => "nno";
            }
            static NynorskNorwegianLanguage() { }
            internal static readonly ILanguage instance = new NynorskNorwegian();
        }
        class NyoroLanguage
        {
            class Nyoro : ISOLanguage
            {
                public override string Name => "Nyoro";
                public override string ThreeLetterISOCode => "nyo";
            }
            static NyoroLanguage() { }
            internal static readonly ILanguage instance = new Nyoro();
        }
        class NzimaLanguage
        {
            class Nzima : ISOLanguage
            {
                public override string Name => "Nzima";
                public override string ThreeLetterISOCode => "nzi";
            }
            static NzimaLanguage() { }
            internal static readonly ILanguage instance = new Nzima();
        }
        class OccidentalLanguage
        {
            class Occidental : ISOLanguage
            {
                public override string Name => "Occidental";
                public override string TwoLetterISOCode => "ie";
                public override string ThreeLetterISOCode => "ile";
            }
            static OccidentalLanguage() { }
            internal static readonly ILanguage instance = new Occidental();
        }
        class OccitanLanguage
        {
            class Occitan : ISOLanguage
            {
                public override string Name => "Occitan (post 1500)";
                public override string TwoLetterISOCode => "oc";
                public override string ThreeLetterISOCode => "oci";
            }
            static OccitanLanguage() { }
            internal static readonly ILanguage instance = new Occitan();
        }
        class OccitanOldLanguage
        {
            class OccitanOld : ISOLanguage
            {
                public override string Name => "Occitan, Old (to 1500)";
                public override string ThreeLetterISOCode => "pro";
            }
            static OccitanOldLanguage() { }
            internal static readonly ILanguage instance = new OccitanOld();
        }
        class OfficialAramaicLanguage
        {
            class OfficialAramaic : ISOLanguage
            {
                public override string Name => "Official Aramaic (700-300 BCE)";
                public override string ThreeLetterISOCode => "arc";
            }
            static OfficialAramaicLanguage() { }
            internal static readonly ILanguage instance = new OfficialAramaic();
        }
        class OiratLanguage
        {
            class Oirat : ISOLanguage
            {
                public override string Name => "Oirat";
                public override string ThreeLetterISOCode => "xal";
            }
            static OiratLanguage() { }
            internal static readonly ILanguage instance = new Oirat();
        }
        class OjibwaLanguage
        {
            class Ojibwa : ISOLanguage
            {
                public override string Name => "Ojibwa";
                public override string TwoLetterISOCode => "oj";
                public override string ThreeLetterISOCode => "oji";
            }
            static OjibwaLanguage() { }
            internal static readonly ILanguage instance = new Ojibwa();
        }
        class OldBulgarianLanguage
        {
            class OldBulgarian : ISOLanguage
            {
                public override string Name => "Old Bulgarian";
                public override string TwoLetterISOCode => "cu";
                public override string ThreeLetterISOCode => "chu";
            }
            static OldBulgarianLanguage() { }
            internal static readonly ILanguage instance = new OldBulgarian();
        }
        class OldChurchSlavonicLanguage
        {
            class OldChurchSlavonic : ISOLanguage
            {
                public override string Name => "Old Church Slavonic";
                public override string TwoLetterISOCode => "cu";
                public override string ThreeLetterISOCode => "chu";
            }
            static OldChurchSlavonicLanguage() { }
            internal static readonly ILanguage instance = new OldChurchSlavonic();
        }
        class OldNewariLanguage
        {
            class OldNewari : ISOLanguage
            {
                public override string Name => "Old Newari";
                public override string ThreeLetterISOCode => "nwc";
            }
            static OldNewariLanguage() { }
            internal static readonly ILanguage instance = new OldNewari();
        }
        class OldSlavonicLanguage
        {
            class OldSlavonic : ISOLanguage
            {
                public override string Name => "Old Slavonic";
                public override string TwoLetterISOCode => "cu";
                public override string ThreeLetterISOCode => "chu";
            }
            static OldSlavonicLanguage() { }
            internal static readonly ILanguage instance = new OldSlavonic();
        }
        class OriyaLanguage
        {
            class Oriya : ISOLanguage
            {
                public override string Name => "Oriya";
                public override string TwoLetterISOCode => "or";
                public override string ThreeLetterISOCode => "ori";
            }
            static OriyaLanguage() { }
            internal static readonly ILanguage instance = new Oriya();
        }
        class OromoLanguage
        {
            class Oromo : ISOLanguage
            {
                public override string Name => "Oromo";
                public override string TwoLetterISOCode => "om";
                public override string ThreeLetterISOCode => "orm";
            }
            static OromoLanguage() { }
            internal static readonly ILanguage instance = new Oromo();
        }
        class OsageLanguage
        {
            class Osage : ISOLanguage
            {
                public override string Name => "Osage";
                public override string ThreeLetterISOCode => "osa";
            }
            static OsageLanguage() { }
            internal static readonly ILanguage instance = new Osage();
        }
        class OssetianLanguage
        {
            class Ossetian : ISOLanguage
            {
                public override string Name => "Ossetian";
                public override string TwoLetterISOCode => "os";
                public override string ThreeLetterISOCode => "oss";
            }
            static OssetianLanguage() { }
            internal static readonly ILanguage instance = new Ossetian();
        }
        class OsseticLanguage
        {
            class Ossetic : ISOLanguage
            {
                public override string Name => "Ossetic";
                public override string TwoLetterISOCode => "os";
                public override string ThreeLetterISOCode => "oss";
            }
            static OsseticLanguage() { }
            internal static readonly ILanguage instance = new Ossetic();
        }
        class OtomianLanguagesLanguage
        {
            class OtomianLanguages : ISOLanguage
            {
                public override string Name => "Otomian languages";
                public override string ThreeLetterISOCode => "oto";
            }
            static OtomianLanguagesLanguage() { }
            internal static readonly ILanguage instance = new OtomianLanguages();
        }
        class PahlaviLanguage
        {
            class Pahlavi : ISOLanguage
            {
                public override string Name => "Pahlavi";
                public override string ThreeLetterISOCode => "pal";
            }
            static PahlaviLanguage() { }
            internal static readonly ILanguage instance = new Pahlavi();
        }
        class PalauanLanguage
        {
            class Palauan : ISOLanguage
            {
                public override string Name => "Palauan";
                public override string ThreeLetterISOCode => "pau";
            }
            static PalauanLanguage() { }
            internal static readonly ILanguage instance = new Palauan();
        }
        class PaliLanguage
        {
            class Pali : ISOLanguage
            {
                public override string Name => "Pali";
                public override string TwoLetterISOCode => "pi";
                public override string ThreeLetterISOCode => "pli";
            }
            static PaliLanguage() { }
            internal static readonly ILanguage instance = new Pali();
        }
        class PampangaLanguage
        {
            class Pampanga : ISOLanguage
            {
                public override string Name => "Pampanga";
                public override string ThreeLetterISOCode => "pam";
            }
            static PampangaLanguage() { }
            internal static readonly ILanguage instance = new Pampanga();
        }
        class PangasinanLanguage
        {
            class Pangasinan : ISOLanguage
            {
                public override string Name => "Pangasinan";
                public override string ThreeLetterISOCode => "pag";
            }
            static PangasinanLanguage() { }
            internal static readonly ILanguage instance = new Pangasinan();
        }
        class PanjabiLanguage
        {
            class Panjabi : ISOLanguage
            {
                public override string Name => "Panjabi";
                public override string TwoLetterISOCode => "pa";
                public override string ThreeLetterISOCode => "pan";
            }
            static PanjabiLanguage() { }
            internal static readonly ILanguage instance = new Panjabi();
        }
        class PapiamentoLanguage
        {
            class Papiamento : ISOLanguage
            {
                public override string Name => "Papiamento";
                public override string ThreeLetterISOCode => "pap";
            }
            static PapiamentoLanguage() { }
            internal static readonly ILanguage instance = new Papiamento();
        }
        class PapuanLanguagesLanguage
        {
            class PapuanLanguages : ISOLanguage
            {
                public override string Name => "Papuan languages";
                public override string ThreeLetterISOCode => "paa";
            }
            static PapuanLanguagesLanguage() { }
            internal static readonly ILanguage instance = new PapuanLanguages();
        }
        class PashtoLanguage
        {
            class Pashto : ISOLanguage
            {
                public override string Name => "Pashto";
                public override string TwoLetterISOCode => "ps";
                public override string ThreeLetterISOCode => "pus";
            }
            static PashtoLanguage() { }
            internal static readonly ILanguage instance = new Pashto();
        }
        class PediLanguage
        {
            class Pedi : ISOLanguage
            {
                public override string Name => "Pedi";
                public override string ThreeLetterISOCode => "nso";
            }
            static PediLanguage() { }
            internal static readonly ILanguage instance = new Pedi();
        }
        class PersianLanguage
        {
            class Persian : ISOLanguage
            {
                public override string Name => "Persian";
                public override string TwoLetterISOCode => "fa";
                public override string ThreeLetterISOCode => "per";
                public override string AlternateThreeLetterISOCode => "fas";
            }
            static PersianLanguage() { }
            internal static readonly ILanguage instance = new Persian();
        }
        class PersianOldLanguage
        {
            class PersianOld : ISOLanguage
            {
                public override string Name => "Persian, Old (ca.600-400 B.C.)";
                public override string ThreeLetterISOCode => "peo";
            }
            static PersianOldLanguage() { }
            internal static readonly ILanguage instance = new PersianOld();
        }
        class PhilippineLanguagesLanguage
        {
            class PhilippineLanguages : ISOLanguage
            {
                public override string Name => "Philippine languages";
                public override string ThreeLetterISOCode => "phi";
            }
            static PhilippineLanguagesLanguage() { }
            internal static readonly ILanguage instance = new PhilippineLanguages();
        }
        class PhoenicianLanguage
        {
            class Phoenician : ISOLanguage
            {
                public override string Name => "Phoenician";
                public override string ThreeLetterISOCode => "phn";
            }
            static PhoenicianLanguage() { }
            internal static readonly ILanguage instance = new Phoenician();
        }
        class PilipinoLanguage
        {
            class Pilipino : ISOLanguage
            {
                public override string Name => "Pilipino";
                public override string ThreeLetterISOCode => "fil";
            }
            static PilipinoLanguage() { }
            internal static readonly ILanguage instance = new Pilipino();
        }
        class PohnpeianLanguage
        {
            class Pohnpeian : ISOLanguage
            {
                public override string Name => "Pohnpeian";
                public override string ThreeLetterISOCode => "pon";
            }
            static PohnpeianLanguage() { }
            internal static readonly ILanguage instance = new Pohnpeian();
        }
        class PolishLanguage
        {
            class Polish : ISOLanguage
            {
                public override string Name => "Polish";
                public override string TwoLetterISOCode => "pl";
                public override string ThreeLetterISOCode => "pol";
            }
            static PolishLanguage() { }
            internal static readonly ILanguage instance = new Polish();
        }
        class PortugueseLanguage
        {
            class Portuguese : ISOLanguage
            {
                public override string Name => "Portuguese";
                public override string TwoLetterISOCode => "pt";
                public override string ThreeLetterISOCode => "por";
            }
            static PortugueseLanguage() { }
            internal static readonly ILanguage instance = new Portuguese();
        }
        class PrakritLanguagesLanguage
        {
            class PrakritLanguages : ISOLanguage
            {
                public override string Name => "Prakrit languages";
                public override string ThreeLetterISOCode => "pra";
            }
            static PrakritLanguagesLanguage() { }
            internal static readonly ILanguage instance = new PrakritLanguages();
        }
        class ProvencalOldLanguage
        {
            class ProvencalOld : ISOLanguage
            {
                public override string Name => "Provençal, Old (to 1500)";
                public override string ThreeLetterISOCode => "pro";
            }
            static ProvencalOldLanguage() { }
            internal static readonly ILanguage instance = new ProvencalOld();
        }
        class PunjabiLanguage
        {
            class Punjabi : ISOLanguage
            {
                public override string Name => "Punjabi";
                public override string TwoLetterISOCode => "pa";
                public override string ThreeLetterISOCode => "pan";
            }
            static PunjabiLanguage() { }
            internal static readonly ILanguage instance = new Punjabi();
        }
        class PushtoLanguage
        {
            class Pushto : ISOLanguage
            {
                public override string Name => "Pushto";
                public override string TwoLetterISOCode => "ps";
                public override string ThreeLetterISOCode => "pus";
            }
            static PushtoLanguage() { }
            internal static readonly ILanguage instance = new Pushto();
        }
        class QuechuaLanguage
        {
            class Quechua : ISOLanguage
            {
                public override string Name => "Quechua";
                public override string TwoLetterISOCode => "qu";
                public override string ThreeLetterISOCode => "que";
            }
            static QuechuaLanguage() { }
            internal static readonly ILanguage instance = new Quechua();
        }
        class RajasthaniLanguage
        {
            class Rajasthani : ISOLanguage
            {
                public override string Name => "Rajasthani";
                public override string ThreeLetterISOCode => "raj";
            }
            static RajasthaniLanguage() { }
            internal static readonly ILanguage instance = new Rajasthani();
        }
        class RapanuiLanguage
        {
            class Rapanui : ISOLanguage
            {
                public override string Name => "Rapanui";
                public override string ThreeLetterISOCode => "rap";
            }
            static RapanuiLanguage() { }
            internal static readonly ILanguage instance = new Rapanui();
        }
        class RarotonganLanguage
        {
            class Rarotongan : ISOLanguage
            {
                public override string Name => "Rarotongan";
                public override string ThreeLetterISOCode => "rar";
            }
            static RarotonganLanguage() { }
            internal static readonly ILanguage instance = new Rarotongan();
        }
        class ReservedForLocalUseLanguage
        {
            class ReservedForLocalUse : ISOLanguage
            {
                public override string Name => "Reserved for local use";
                public override string ThreeLetterISOCode => "qaa";
                public override string AlternateThreeLetterISOCode => "qtz";
            }
            static ReservedForLocalUseLanguage() { }
            internal static readonly ILanguage instance = new ReservedForLocalUse();
        }
        class RomanceLanguagesLanguage
        {
            class RomanceLanguages : ISOLanguage
            {
                public override string Name => "Romance languages";
                public override string ThreeLetterISOCode => "roa";
            }
            static RomanceLanguagesLanguage() { }
            internal static readonly ILanguage instance = new RomanceLanguages();
        }
        class RomanianLanguage
        {
            class Romanian : ISOLanguage
            {
                public override string Name => "Romanian";
                public override string TwoLetterISOCode => "ro";
                public override string ThreeLetterISOCode => "rum";
                public override string AlternateThreeLetterISOCode => "ron";
            }
            static RomanianLanguage() { }
            internal static readonly ILanguage instance = new Romanian();
        }
        class RomanshLanguage
        {
            class Romansh : ISOLanguage
            {
                public override string Name => "Romansh";
                public override string TwoLetterISOCode => "rm";
                public override string ThreeLetterISOCode => "roh";
            }
            static RomanshLanguage() { }
            internal static readonly ILanguage instance = new Romansh();
        }
        class RomanyLanguage
        {
            class Romany : ISOLanguage
            {
                public override string Name => "Romany";
                public override string ThreeLetterISOCode => "rom";
            }
            static RomanyLanguage() { }
            internal static readonly ILanguage instance = new Romany();
        }
        class RundiLanguage
        {
            class Rundi : ISOLanguage
            {
                public override string Name => "Rundi";
                public override string TwoLetterISOCode => "rn";
                public override string ThreeLetterISOCode => "run";
            }
            static RundiLanguage() { }
            internal static readonly ILanguage instance = new Rundi();
        }
        class RussianLanguage
        {
            class Russian : ISOLanguage
            {
                public override string Name => "русский";
                public override string TransliteratedName => "russkiy";
                public override string EnglishName => "Russian";
                public override string TwoLetterISOCode => "ru";
                public override string ThreeLetterISOCode => "rus";
            }
            static RussianLanguage() { }
            internal static readonly ILanguage instance = new Russian();
        }
        class SakanLanguage
        {
            class Sakan : ISOLanguage
            {
                public override string Name => "Sakan";
                public override string ThreeLetterISOCode => "kho";
            }
            static SakanLanguage() { }
            internal static readonly ILanguage instance = new Sakan();
        }
        class SalishanLanguagesLanguage
        {
            class SalishanLanguages : ISOLanguage
            {
                public override string Name => "Salishan languages";
                public override string ThreeLetterISOCode => "sal";
            }
            static SalishanLanguagesLanguage() { }
            internal static readonly ILanguage instance = new SalishanLanguages();
        }
        class SamaritanAramaicLanguage
        {
            class SamaritanAramaic : ISOLanguage
            {
                public override string Name => "Samaritan Aramaic";
                public override string ThreeLetterISOCode => "sam";
            }
            static SamaritanAramaicLanguage() { }
            internal static readonly ILanguage instance = new SamaritanAramaic();
        }
        class SamiLanguagesLanguage
        {
            class SamiLanguages : ISOLanguage
            {
                public override string Name => "Sami languages";
                public override string ThreeLetterISOCode => "smi";
            }
            static SamiLanguagesLanguage() { }
            internal static readonly ILanguage instance = new SamiLanguages();
        }
        class SamoanLanguage
        {
            class Samoan : ISOLanguage
            {
                public override string Name => "Samoan";
                public override string TwoLetterISOCode => "sm";
                public override string ThreeLetterISOCode => "smo";
            }
            static SamoanLanguage() { }
            internal static readonly ILanguage instance = new Samoan();
        }
        class SandaweLanguage
        {
            class Sandawe : ISOLanguage
            {
                public override string Name => "Sandawe";
                public override string ThreeLetterISOCode => "sad";
            }
            static SandaweLanguage() { }
            internal static readonly ILanguage instance = new Sandawe();
        }
        class SangoLanguage
        {
            class Sango : ISOLanguage
            {
                public override string Name => "Sango";
                public override string TwoLetterISOCode => "sg";
                public override string ThreeLetterISOCode => "sag";
            }
            static SangoLanguage() { }
            internal static readonly ILanguage instance = new Sango();
        }
        class SanskritLanguage
        {
            class Sanskrit : ISOLanguage
            {
                public override string Name => "Sanskrit";
                public override string TwoLetterISOCode => "sa";
                public override string ThreeLetterISOCode => "san";
            }
            static SanskritLanguage() { }
            internal static readonly ILanguage instance = new Sanskrit();
        }
        class SantaliLanguage
        {
            class Santali : ISOLanguage
            {
                public override string Name => "Santali";
                public override string ThreeLetterISOCode => "sat";
            }
            static SantaliLanguage() { }
            internal static readonly ILanguage instance = new Santali();
        }
        class SardinianLanguage
        {
            class Sardinian : ISOLanguage
            {
                public override string Name => "Sardinian";
                public override string TwoLetterISOCode => "sc";
                public override string ThreeLetterISOCode => "srd";
            }
            static SardinianLanguage() { }
            internal static readonly ILanguage instance = new Sardinian();
        }
        class SasakLanguage
        {
            class Sasak : ISOLanguage
            {
                public override string Name => "Sasak";
                public override string ThreeLetterISOCode => "sas";
            }
            static SasakLanguage() { }
            internal static readonly ILanguage instance = new Sasak();
        }
        class SaxonLowLanguage
        {
            class SaxonLow : ISOLanguage
            {
                public override string Name => "Saxon, Low";
                public override string ThreeLetterISOCode => "nds";
            }
            static SaxonLowLanguage() { }
            internal static readonly ILanguage instance = new SaxonLow();
        }
        class ScotsLanguage
        {
            class Scots : ISOLanguage
            {
                public override string Name => "Scots";
                public override string ThreeLetterISOCode => "sco";
            }
            static ScotsLanguage() { }
            internal static readonly ILanguage instance = new Scots();
        }
        class ScottishGaelicLanguage
        {
            class ScottishGaelic : ISOLanguage
            {
                public override string Name => "Scottish Gaelic";
                public override string TwoLetterISOCode => "gd";
                public override string ThreeLetterISOCode => "gla";
            }
            static ScottishGaelicLanguage() { }
            internal static readonly ILanguage instance = new ScottishGaelic();
        }
        class SelkupLanguage
        {
            class Selkup : ISOLanguage
            {
                public override string Name => "Selkup";
                public override string ThreeLetterISOCode => "sel";
            }
            static SelkupLanguage() { }
            internal static readonly ILanguage instance = new Selkup();
        }
        class SemiticLanguagesLanguage
        {
            class SemiticLanguages : ISOLanguage
            {
                public override string Name => "Semitic languages";
                public override string ThreeLetterISOCode => "sem";
            }
            static SemiticLanguagesLanguage() { }
            internal static readonly ILanguage instance = new SemiticLanguages();
        }
        class SepediLanguage
        {
            class Sepedi : ISOLanguage
            {
                public override string Name => "Sepedi";
                public override string ThreeLetterISOCode => "nso";
            }
            static SepediLanguage() { }
            internal static readonly ILanguage instance = new Sepedi();
        }
        class SerbianLanguage
        {
            class Serbian : ISOLanguage
            {
                public override string Name => "Serbian";
                public override string TwoLetterISOCode => "sr";
                public override string ThreeLetterISOCode => "srp";
            }
            static SerbianLanguage() { }
            internal static readonly ILanguage instance = new Serbian();
        }
        class SererLanguage
        {
            class Serer : ISOLanguage
            {
                public override string Name => "Serer";
                public override string ThreeLetterISOCode => "srr";
            }
            static SererLanguage() { }
            internal static readonly ILanguage instance = new Serer();
        }
        class ShanLanguage
        {
            class Shan : ISOLanguage
            {
                public override string Name => "Shan";
                public override string ThreeLetterISOCode => "shn";
            }
            static ShanLanguage() { }
            internal static readonly ILanguage instance = new Shan();
        }
        class ShonaLanguage
        {
            class Shona : ISOLanguage
            {
                public override string Name => "Shona";
                public override string TwoLetterISOCode => "sn";
                public override string ThreeLetterISOCode => "sna";
            }
            static ShonaLanguage() { }
            internal static readonly ILanguage instance = new Shona();
        }
        class SichuanYiLanguage
        {
            class SichuanYi : ISOLanguage
            {
                public override string Name => "Sichuan Yi";
                public override string TwoLetterISOCode => "ii";
                public override string ThreeLetterISOCode => "iii";
            }
            static SichuanYiLanguage() { }
            internal static readonly ILanguage instance = new SichuanYi();
        }
        class SicilianLanguage
        {
            class Sicilian : ISOLanguage
            {
                public override string Name => "Sicilian";
                public override string ThreeLetterISOCode => "scn";
            }
            static SicilianLanguage() { }
            internal static readonly ILanguage instance = new Sicilian();
        }
        class SidamoLanguage
        {
            class Sidamo : ISOLanguage
            {
                public override string Name => "Sidamo";
                public override string ThreeLetterISOCode => "sid";
            }
            static SidamoLanguage() { }
            internal static readonly ILanguage instance = new Sidamo();
        }
        class SignLanguagesLanguage
        {
            class SignLanguages : ISOLanguage
            {
                public override string Name => "Sign Languages";
                public override string ThreeLetterISOCode => "sgn";
            }
            static SignLanguagesLanguage() { }
            internal static readonly ILanguage instance = new SignLanguages();
        }
        class SiksikaLanguage
        {
            class Siksika : ISOLanguage
            {
                public override string Name => "Siksika";
                public override string ThreeLetterISOCode => "bla";
            }
            static SiksikaLanguage() { }
            internal static readonly ILanguage instance = new Siksika();
        }
        class SindhiLanguage
        {
            class Sindhi : ISOLanguage
            {
                public override string Name => "Sindhi";
                public override string TwoLetterISOCode => "sd";
                public override string ThreeLetterISOCode => "snd";
            }
            static SindhiLanguage() { }
            internal static readonly ILanguage instance = new Sindhi();
        }
        class SinhalaLanguage
        {
            class Sinhala : ISOLanguage
            {
                public override string Name => "Sinhala";
                public override string TwoLetterISOCode => "si";
                public override string ThreeLetterISOCode => "sin";
            }
            static SinhalaLanguage() { }
            internal static readonly ILanguage instance = new Sinhala();
        }
        class SinhaleseLanguage
        {
            class Sinhalese : ISOLanguage
            {
                public override string Name => "Sinhalese";
                public override string TwoLetterISOCode => "si";
                public override string ThreeLetterISOCode => "sin";
            }
            static SinhaleseLanguage() { }
            internal static readonly ILanguage instance = new Sinhalese();
        }
        class SinoTibetanLanguagesLanguage
        {
            class SinoTibetanLanguages : ISOLanguage
            {
                public override string Name => "Sino-Tibetan languages";
                public override string ThreeLetterISOCode => "sit";
            }
            static SinoTibetanLanguagesLanguage() { }
            internal static readonly ILanguage instance = new SinoTibetanLanguages();
        }
        class SiouanLanguagesLanguage
        {
            class SiouanLanguages : ISOLanguage
            {
                public override string Name => "Siouan languages";
                public override string ThreeLetterISOCode => "sio";
            }
            static SiouanLanguagesLanguage() { }
            internal static readonly ILanguage instance = new SiouanLanguages();
        }
        class SkoltSamiLanguage
        {
            class SkoltSami : ISOLanguage
            {
                public override string Name => "Skolt Sami";
                public override string ThreeLetterISOCode => "sms";
            }
            static SkoltSamiLanguage() { }
            internal static readonly ILanguage instance = new SkoltSami();
        }
        class SlaveLanguage
        {
            class Slave : ISOLanguage
            {
                public override string Name => "Slave (Athapascan)";
                public override string ThreeLetterISOCode => "den";
            }
            static SlaveLanguage() { }
            internal static readonly ILanguage instance = new Slave();
        }
        class SlavicLanguagesLanguage
        {
            class SlavicLanguages : ISOLanguage
            {
                public override string Name => "Slavic languages";
                public override string ThreeLetterISOCode => "sla";
            }
            static SlavicLanguagesLanguage() { }
            internal static readonly ILanguage instance = new SlavicLanguages();
        }
        class SlovakLanguage
        {
            class Slovak : ISOLanguage
            {
                public override string Name => "Slovak";
                public override string TwoLetterISOCode => "sk";
                public override string ThreeLetterISOCode => "slo";
                public override string AlternateThreeLetterISOCode => "slk";
            }
            static SlovakLanguage() { }
            internal static readonly ILanguage instance = new Slovak();
        }
        class SlovenianLanguage
        {
            class Slovenian : ISOLanguage
            {
                public override string Name => "Slovenian";
                public override string TwoLetterISOCode => "sl";
                public override string ThreeLetterISOCode => "slv";
            }
            static SlovenianLanguage() { }
            internal static readonly ILanguage instance = new Slovenian();
        }
        class SogdianLanguage
        {
            class Sogdian : ISOLanguage
            {
                public override string Name => "Sogdian";
                public override string ThreeLetterISOCode => "sog";
            }
            static SogdianLanguage() { }
            internal static readonly ILanguage instance = new Sogdian();
        }
        class SomaliLanguage
        {
            class Somali : ISOLanguage
            {
                public override string Name => "Somali";
                public override string TwoLetterISOCode => "so";
                public override string ThreeLetterISOCode => "som";
            }
            static SomaliLanguage() { }
            internal static readonly ILanguage instance = new Somali();
        }
        class SonghaiLanguagesLanguage
        {
            class SonghaiLanguages : ISOLanguage
            {
                public override string Name => "Songhai languages";
                public override string ThreeLetterISOCode => "son";
            }
            static SonghaiLanguagesLanguage() { }
            internal static readonly ILanguage instance = new SonghaiLanguages();
        }
        class SoninkeLanguage
        {
            class Soninke : ISOLanguage
            {
                public override string Name => "Soninke";
                public override string ThreeLetterISOCode => "snk";
            }
            static SoninkeLanguage() { }
            internal static readonly ILanguage instance = new Soninke();
        }
        class SorbianLanguagesLanguage
        {
            class SorbianLanguages : ISOLanguage
            {
                public override string Name => "Sorbian languages";
                public override string ThreeLetterISOCode => "wen";
            }
            static SorbianLanguagesLanguage() { }
            internal static readonly ILanguage instance = new SorbianLanguages();
        }
        class SothoNorthernLanguage
        {
            class SothoNorthern : ISOLanguage
            {
                public override string Name => "Sotho, Northern";
                public override string ThreeLetterISOCode => "nso";
            }
            static SothoNorthernLanguage() { }
            internal static readonly ILanguage instance = new SothoNorthern();
        }
        class SothoSouthernLanguage
        {
            class SothoSouthern : ISOLanguage
            {
                public override string Name => "Sotho, Southern";
                public override string TwoLetterISOCode => "st";
                public override string ThreeLetterISOCode => "sot";
            }
            static SothoSouthernLanguage() { }
            internal static readonly ILanguage instance = new SothoSouthern();
        }
        class SouthAmericanIndianLanguagesLanguage
        {
            class SouthAmericanIndianLanguages : ISOLanguage
            {
                public override string Name => "South American Indian languages";
                public override string ThreeLetterISOCode => "sai";
            }
            static SouthAmericanIndianLanguagesLanguage() { }
            internal static readonly ILanguage instance = new SouthAmericanIndianLanguages();
        }
        class SouthNdebeleLanguage
        {
            class SouthNdebele : ISOLanguage
            {
                public override string Name => "South Ndebele";
                public override string TwoLetterISOCode => "nr";
                public override string ThreeLetterISOCode => "nbl";
            }
            static SouthNdebeleLanguage() { }
            internal static readonly ILanguage instance = new SouthNdebele();
        }
        class SouthernAltaiLanguage
        {
            class SouthernAltai : ISOLanguage
            {
                public override string Name => "Southern Altai";
                public override string ThreeLetterISOCode => "alt";
            }
            static SouthernAltaiLanguage() { }
            internal static readonly ILanguage instance = new SouthernAltai();
        }
        class SouthernSamiLanguage
        {
            class SouthernSami : ISOLanguage
            {
                public override string Name => "Southern Sami";
                public override string ThreeLetterISOCode => "sma";
            }
            static SouthernSamiLanguage() { }
            internal static readonly ILanguage instance = new SouthernSami();
        }
        class SpanishLanguage
        {
            class Spanish : ISOLanguage
            {
                public override string Name => "español";
                public override string EnglishName => "Spanish";
                public override string TwoLetterISOCode => "es";
                public override string ThreeLetterISOCode => "spa";
            }
            static SpanishLanguage() { }
            internal static readonly ILanguage instance = new Spanish();
        }
        class SrananTongoLanguage
        {
            class SrananTongo : ISOLanguage
            {
                public override string Name => "Sranan Tongo";
                public override string ThreeLetterISOCode => "srn";
            }
            static SrananTongoLanguage() { }
            internal static readonly ILanguage instance = new SrananTongo();
        }
        class StandardMoroccanTamazightLanguage
        {
            class StandardMoroccanTamazight : ISOLanguage
            {
                public override string Name => "Standard Moroccan Tamazight";
                public override string ThreeLetterISOCode => "zgh";
            }
            static StandardMoroccanTamazightLanguage() { }
            internal static readonly ILanguage instance = new StandardMoroccanTamazight();
        }
        class SukumaLanguage
        {
            class Sukuma : ISOLanguage
            {
                public override string Name => "Sukuma";
                public override string ThreeLetterISOCode => "suk";
            }
            static SukumaLanguage() { }
            internal static readonly ILanguage instance = new Sukuma();
        }
        class SumerianLanguage
        {
            class Sumerian : ISOLanguage
            {
                public override string Name => "Sumerian";
                public override string ThreeLetterISOCode => "sux";
            }
            static SumerianLanguage() { }
            internal static readonly ILanguage instance = new Sumerian();
        }
        class SundaneseLanguage
        {
            class Sundanese : ISOLanguage
            {
                public override string Name => "Sundanese";
                public override string TwoLetterISOCode => "su";
                public override string ThreeLetterISOCode => "sun";
            }
            static SundaneseLanguage() { }
            internal static readonly ILanguage instance = new Sundanese();
        }
        class SusuLanguage
        {
            class Susu : ISOLanguage
            {
                public override string Name => "Susu";
                public override string ThreeLetterISOCode => "sus";
            }
            static SusuLanguage() { }
            internal static readonly ILanguage instance = new Susu();
        }
        class SwahiliLanguage
        {
            class Swahili : ISOLanguage
            {
                public override string Name => "Swahili";
                public override string TwoLetterISOCode => "sw";
                public override string ThreeLetterISOCode => "swa";
            }
            static SwahiliLanguage() { }
            internal static readonly ILanguage instance = new Swahili();
        }
        class SwatiLanguage
        {
            class Swati : ISOLanguage
            {
                public override string Name => "Swati";
                public override string TwoLetterISOCode => "ss";
                public override string ThreeLetterISOCode => "ssw";
            }
            static SwatiLanguage() { }
            internal static readonly ILanguage instance = new Swati();
        }
        class SwedishLanguage
        {
            class Swedish : ISOLanguage
            {
                public override string Name => "Swedish";
                public override string TwoLetterISOCode => "sv";
                public override string ThreeLetterISOCode => "swe";
            }
            static SwedishLanguage() { }
            internal static readonly ILanguage instance = new Swedish();
        }
        class SwissGermanLanguage
        {
            class SwissGerman : ISOLanguage
            {
                public override string Name => "Swiss German";
                public override string ThreeLetterISOCode => "gsw";
            }
            static SwissGermanLanguage() { }
            internal static readonly ILanguage instance = new SwissGerman();
        }
        class SyriacLanguage
        {
            class Syriac : ISOLanguage
            {
                public override string Name => "Syriac";
                public override string ThreeLetterISOCode => "syr";
            }
            static SyriacLanguage() { }
            internal static readonly ILanguage instance = new Syriac();
        }
        class TagalogLanguage
        {
            class Tagalog : ISOLanguage
            {
                public override string Name => "Tagalog";
                public override string TwoLetterISOCode => "tl";
                public override string ThreeLetterISOCode => "tgl";
            }
            static TagalogLanguage() { }
            internal static readonly ILanguage instance = new Tagalog();
        }
        class TahitianLanguage
        {
            class Tahitian : ISOLanguage
            {
                public override string Name => "Tahitian";
                public override string TwoLetterISOCode => "ty";
                public override string ThreeLetterISOCode => "tah";
            }
            static TahitianLanguage() { }
            internal static readonly ILanguage instance = new Tahitian();
        }
        class TaiLanguagesLanguage
        {
            class TaiLanguages : ISOLanguage
            {
                public override string Name => "Tai languages";
                public override string ThreeLetterISOCode => "tai";
            }
            static TaiLanguagesLanguage() { }
            internal static readonly ILanguage instance = new TaiLanguages();
        }
        class TajikLanguage
        {
            class Tajik : ISOLanguage
            {
                public override string Name => "Tajik";
                public override string TwoLetterISOCode => "tg";
                public override string ThreeLetterISOCode => "tgk";
            }
            static TajikLanguage() { }
            internal static readonly ILanguage instance = new Tajik();
        }
        class TamashekLanguage
        {
            class Tamashek : ISOLanguage
            {
                public override string Name => "Tamashek";
                public override string ThreeLetterISOCode => "tmh";
            }
            static TamashekLanguage() { }
            internal static readonly ILanguage instance = new Tamashek();
        }
        class TamilLanguage
        {
            class Tamil : ISOLanguage
            {
                public override string Name => "Tamil";
                public override string TwoLetterISOCode => "ta";
                public override string ThreeLetterISOCode => "tam";
            }
            static TamilLanguage() { }
            internal static readonly ILanguage instance = new Tamil();
        }
        class TatarLanguage
        {
            class Tatar : ISOLanguage
            {
                public override string Name => "Tatar";
                public override string TwoLetterISOCode => "tt";
                public override string ThreeLetterISOCode => "tat";
            }
            static TatarLanguage() { }
            internal static readonly ILanguage instance = new Tatar();
        }
        class TeluguLanguage
        {
            class Telugu : ISOLanguage
            {
                public override string Name => "Telugu";
                public override string TwoLetterISOCode => "te";
                public override string ThreeLetterISOCode => "tel";
            }
            static TeluguLanguage() { }
            internal static readonly ILanguage instance = new Telugu();
        }
        class TerenoLanguage
        {
            class Tereno : ISOLanguage
            {
                public override string Name => "Tereno";
                public override string ThreeLetterISOCode => "ter";
            }
            static TerenoLanguage() { }
            internal static readonly ILanguage instance = new Tereno();
        }
        class TetumLanguage
        {
            class Tetum : ISOLanguage
            {
                public override string Name => "Tetum";
                public override string ThreeLetterISOCode => "tet";
            }
            static TetumLanguage() { }
            internal static readonly ILanguage instance = new Tetum();
        }
        class ThaiLanguage
        {
            class Thai : ISOLanguage
            {
                public override string Name => "Thai";
                public override string TwoLetterISOCode => "th";
                public override string ThreeLetterISOCode => "tha";
            }
            static ThaiLanguage() { }
            internal static readonly ILanguage instance = new Thai();
        }
        class TibetanLanguage
        {
            class Tibetan : ISOLanguage
            {
                public override string Name => "Tibetan";
                public override string TwoLetterISOCode => "bo";
                public override string ThreeLetterISOCode => "tib";
                public override string AlternateThreeLetterISOCode => "bod";
            }
            static TibetanLanguage() { }
            internal static readonly ILanguage instance = new Tibetan();
        }
        class TigreLanguage
        {
            class Tigre : ISOLanguage
            {
                public override string Name => "Tigre";
                public override string ThreeLetterISOCode => "tig";
            }
            static TigreLanguage() { }
            internal static readonly ILanguage instance = new Tigre();
        }
        class TigrinyaLanguage
        {
            class Tigrinya : ISOLanguage
            {
                public override string Name => "Tigrinya";
                public override string TwoLetterISOCode => "ti";
                public override string ThreeLetterISOCode => "tir";
            }
            static TigrinyaLanguage() { }
            internal static readonly ILanguage instance = new Tigrinya();
        }
        class TimneLanguage
        {
            class Timne : ISOLanguage
            {
                public override string Name => "Timne";
                public override string ThreeLetterISOCode => "tem";
            }
            static TimneLanguage() { }
            internal static readonly ILanguage instance = new Timne();
        }
        class TivLanguage
        {
            class Tiv : ISOLanguage
            {
                public override string Name => "Tiv";
                public override string ThreeLetterISOCode => "tiv";
            }
            static TivLanguage() { }
            internal static readonly ILanguage instance = new Tiv();
        }
        class TlhInganHolLanguage
        {
            class TlhInganHol : ISOLanguage
            {
                public override string Name => "tlhIngan-Hol";
                public override string ThreeLetterISOCode => "tlh";
                public override string PrivateUseCharacterName => " ";
                public override string PrivateUseCharacterNameFontName => "pIqaD";
            }
            static TlhInganHolLanguage() { }
            internal static readonly ILanguage instance = new TlhInganHol();
        }
        class TlingitLanguage
        {
            class Tlingit : ISOLanguage
            {
                public override string Name => "Tlingit";
                public override string ThreeLetterISOCode => "tli";
            }
            static TlingitLanguage() { }
            internal static readonly ILanguage instance = new Tlingit();
        }
        class TokPisinLanguage
        {
            class TokPisin : ISOLanguage
            {
                public override string Name => "Tok Pisin";
                public override string ThreeLetterISOCode => "tpi";
            }
            static TokPisinLanguage() { }
            internal static readonly ILanguage instance = new TokPisin();
        }
        class TokelauLanguage
        {
            class Tokelau : ISOLanguage
            {
                public override string Name => "Tokelau";
                public override string ThreeLetterISOCode => "tkl";
            }
            static TokelauLanguage() { }
            internal static readonly ILanguage instance = new Tokelau();
        }
        class TongaNyasaLanguage
        {
            class TongaNyasa : ISOLanguage
            {
                public override string Name => "Tonga (Nyasa)";
                public override string ThreeLetterISOCode => "tog";
            }
            static TongaNyasaLanguage() { }
            internal static readonly ILanguage instance = new TongaNyasa();
        }
        class TongaTongaIslandsLanguage
        {
            class TongaTongaIslands : ISOLanguage
            {
                public override string Name => "Tonga (Tonga Islands)";
                public override string TwoLetterISOCode => "to";
                public override string ThreeLetterISOCode => "ton";
            }
            static TongaTongaIslandsLanguage() { }
            internal static readonly ILanguage instance = new TongaTongaIslands();
        }
        class TsimshianLanguage
        {
            class Tsimshian : ISOLanguage
            {
                public override string Name => "Tsimshian";
                public override string ThreeLetterISOCode => "tsi";
            }
            static TsimshianLanguage() { }
            internal static readonly ILanguage instance = new Tsimshian();
        }
        class TsongaLanguage
        {
            class Tsonga : ISOLanguage
            {
                public override string Name => "Tsonga";
                public override string TwoLetterISOCode => "ts";
                public override string ThreeLetterISOCode => "tso";
            }
            static TsongaLanguage() { }
            internal static readonly ILanguage instance = new Tsonga();
        }
        class TswanaLanguage
        {
            class Tswana : ISOLanguage
            {
                public override string Name => "Tswana";
                public override string TwoLetterISOCode => "tn";
                public override string ThreeLetterISOCode => "tsn";
            }
            static TswanaLanguage() { }
            internal static readonly ILanguage instance = new Tswana();
        }
        class TumbukaLanguage
        {
            class Tumbuka : ISOLanguage
            {
                public override string Name => "Tumbuka";
                public override string ThreeLetterISOCode => "tum";
            }
            static TumbukaLanguage() { }
            internal static readonly ILanguage instance = new Tumbuka();
        }
        class TupiLanguagesLanguage
        {
            class TupiLanguages : ISOLanguage
            {
                public override string Name => "Tupi languages";
                public override string ThreeLetterISOCode => "tup";
            }
            static TupiLanguagesLanguage() { }
            internal static readonly ILanguage instance = new TupiLanguages();
        }
        class TurkishLanguage
        {
            class Turkish : ISOLanguage
            {
                public override string Name => "Turkish";
                public override string TwoLetterISOCode => "tr";
                public override string ThreeLetterISOCode => "tur";
            }
            static TurkishLanguage() { }
            internal static readonly ILanguage instance = new Turkish();
        }
        class TurkishOttomanLanguage
        {
            class TurkishOttoman : ISOLanguage
            {
                public override string Name => "Turkish, Ottoman (1500-1928)";
                public override string ThreeLetterISOCode => "ota";
            }
            static TurkishOttomanLanguage() { }
            internal static readonly ILanguage instance = new TurkishOttoman();
        }
        class TurkmenLanguage
        {
            class Turkmen : ISOLanguage
            {
                public override string Name => "Turkmen";
                public override string TwoLetterISOCode => "tk";
                public override string ThreeLetterISOCode => "tuk";
            }
            static TurkmenLanguage() { }
            internal static readonly ILanguage instance = new Turkmen();
        }
        class TuvaluLanguage
        {
            class Tuvalu : ISOLanguage
            {
                public override string Name => "Tuvalu";
                public override string ThreeLetterISOCode => "tvl";
            }
            static TuvaluLanguage() { }
            internal static readonly ILanguage instance = new Tuvalu();
        }
        class TuvinianLanguage
        {
            class Tuvinian : ISOLanguage
            {
                public override string Name => "Tuvinian";
                public override string ThreeLetterISOCode => "tyv";
            }
            static TuvinianLanguage() { }
            internal static readonly ILanguage instance = new Tuvinian();
        }
        class TwiLanguage
        {
            class Twi : ISOLanguage
            {
                public override string Name => "Twi";
                public override string TwoLetterISOCode => "tw";
                public override string ThreeLetterISOCode => "twi";
            }
            static TwiLanguage() { }
            internal static readonly ILanguage instance = new Twi();
        }
        class UdmurtLanguage
        {
            class Udmurt : ISOLanguage
            {
                public override string Name => "Udmurt";
                public override string ThreeLetterISOCode => "udm";
            }
            static UdmurtLanguage() { }
            internal static readonly ILanguage instance = new Udmurt();
        }
        class UgariticLanguage
        {
            class Ugaritic : ISOLanguage
            {
                public override string Name => "Ugaritic";
                public override string ThreeLetterISOCode => "uga";
            }
            static UgariticLanguage() { }
            internal static readonly ILanguage instance = new Ugaritic();
        }
        class UighurLanguage
        {
            class Uighur : ISOLanguage
            {
                public override string Name => "Uighur";
                public override string TwoLetterISOCode => "ug";
                public override string ThreeLetterISOCode => "uig";
            }
            static UighurLanguage() { }
            internal static readonly ILanguage instance = new Uighur();
        }
        class UkrainianLanguage
        {
            class Ukrainian : ISOLanguage
            {
                public override string Name => "Ukrainian";
                public override string TwoLetterISOCode => "uk";
                public override string ThreeLetterISOCode => "ukr";
            }
            static UkrainianLanguage() { }
            internal static readonly ILanguage instance = new Ukrainian();
        }
        class UmbunduLanguage
        {
            class Umbundu : ISOLanguage
            {
                public override string Name => "Umbundu";
                public override string ThreeLetterISOCode => "umb";
            }
            static UmbunduLanguage() { }
            internal static readonly ILanguage instance = new Umbundu();
        }
        class UncodedLanguagesLanguage
        {
            class UncodedLanguages : ISOLanguage
            {
                public override string Name => "Uncoded languages";
                public override string ThreeLetterISOCode => "mis";
            }
            static UncodedLanguagesLanguage() { }
            internal static readonly ILanguage instance = new UncodedLanguages();
        }
        class UndeterminedLanguage
        {
            class Undetermined : ISOLanguage
            {
                public override string Name => "Undetermined";
                public override string ThreeLetterISOCode => "und";
            }
            static UndeterminedLanguage() { }
            internal static readonly ILanguage instance = new Undetermined();
        }
        class UpperSorbianLanguage
        {
            class UpperSorbian : ISOLanguage
            {
                public override string Name => "Upper Sorbian";
                public override string ThreeLetterISOCode => "hsb";
            }
            static UpperSorbianLanguage() { }
            internal static readonly ILanguage instance = new UpperSorbian();
        }
        class UrduLanguage
        {
            class Urdu : ISOLanguage
            {
                public override string Name => "Urdu";
                public override string TwoLetterISOCode => "ur";
                public override string ThreeLetterISOCode => "urd";
            }
            static UrduLanguage() { }
            internal static readonly ILanguage instance = new Urdu();
        }
        class UyghurLanguage
        {
            class Uyghur : ISOLanguage
            {
                public override string Name => "Uyghur";
                public override string TwoLetterISOCode => "ug";
                public override string ThreeLetterISOCode => "uig";
            }
            static UyghurLanguage() { }
            internal static readonly ILanguage instance = new Uyghur();
        }
        class UzbekLanguage
        {
            class Uzbek : ISOLanguage
            {
                public override string Name => "Uzbek";
                public override string TwoLetterISOCode => "uz";
                public override string ThreeLetterISOCode => "uzb";
            }
            static UzbekLanguage() { }
            internal static readonly ILanguage instance = new Uzbek();
        }
        class VaiLanguage
        {
            class Vai : ISOLanguage
            {
                public override string Name => "Vai";
                public override string ThreeLetterISOCode => "vai";
            }
            static VaiLanguage() { }
            internal static readonly ILanguage instance = new Vai();
        }
        class ValencianLanguage
        {
            class Valencian : ISOLanguage
            {
                public override string Name => "Valencian";
                public override string TwoLetterISOCode => "ca";
                public override string ThreeLetterISOCode => "cat";
            }
            static ValencianLanguage() { }
            internal static readonly ILanguage instance = new Valencian();
        }
        class VendaLanguage
        {
            class Venda : ISOLanguage
            {
                public override string Name => "Venda";
                public override string TwoLetterISOCode => "ve";
                public override string ThreeLetterISOCode => "ven";
            }
            static VendaLanguage() { }
            internal static readonly ILanguage instance = new Venda();
        }
        class VietnameseLanguage
        {
            class Vietnamese : ISOLanguage
            {
                public override string Name => "Vietnamese";
                public override string TwoLetterISOCode => "vi";
                public override string ThreeLetterISOCode => "vie";
            }
            static VietnameseLanguage() { }
            internal static readonly ILanguage instance = new Vietnamese();
        }
        class VolapukLanguage
        {
            class Volapuk : ISOLanguage
            {
                public override string Name => "Volapük";
                public override string TwoLetterISOCode => "vo";
                public override string ThreeLetterISOCode => "vol";
            }
            static VolapukLanguage() { }
            internal static readonly ILanguage instance = new Volapuk();
        }
        class VoticLanguage
        {
            class Votic : ISOLanguage
            {
                public override string Name => "Votic";
                public override string ThreeLetterISOCode => "vot";
            }
            static VoticLanguage() { }
            internal static readonly ILanguage instance = new Votic();
        }
        class WakashanLanguagesLanguage
        {
            class WakashanLanguages : ISOLanguage
            {
                public override string Name => "Wakashan languages";
                public override string ThreeLetterISOCode => "wak";
            }
            static WakashanLanguagesLanguage() { }
            internal static readonly ILanguage instance = new WakashanLanguages();
        }
        class WalloonLanguage
        {
            class Walloon : ISOLanguage
            {
                public override string Name => "Walloon";
                public override string TwoLetterISOCode => "wa";
                public override string ThreeLetterISOCode => "wln";
            }
            static WalloonLanguage() { }
            internal static readonly ILanguage instance = new Walloon();
        }
        class WarayLanguage
        {
            class Waray : ISOLanguage
            {
                public override string Name => "Waray";
                public override string ThreeLetterISOCode => "war";
            }
            static WarayLanguage() { }
            internal static readonly ILanguage instance = new Waray();
        }
        class WashoLanguage
        {
            class Washo : ISOLanguage
            {
                public override string Name => "Washo";
                public override string ThreeLetterISOCode => "was";
            }
            static WashoLanguage() { }
            internal static readonly ILanguage instance = new Washo();
        }
        class WelshLanguage
        {
            class Welsh : ISOLanguage
            {
                public override string Name => "Welsh";
                public override string TwoLetterISOCode => "cy";
                public override string ThreeLetterISOCode => "wel";
                public override string AlternateThreeLetterISOCode => "cym";
            }
            static WelshLanguage() { }
            internal static readonly ILanguage instance = new Welsh();
        }
        class WesternFrisianLanguage
        {
            class WesternFrisian : ISOLanguage
            {
                public override string Name => "Western Frisian";
                public override string TwoLetterISOCode => "fy";
                public override string ThreeLetterISOCode => "fry";
            }
            static WesternFrisianLanguage() { }
            internal static readonly ILanguage instance = new WesternFrisian();
        }
        class WesternPahariLanguagesLanguage
        {
            class WesternPahariLanguages : ISOLanguage
            {
                public override string Name => "Western Pahari languages";
                public override string ThreeLetterISOCode => "him";
            }
            static WesternPahariLanguagesLanguage() { }
            internal static readonly ILanguage instance = new WesternPahariLanguages();
        }
        class WolaittaLanguage
        {
            class Wolaitta : ISOLanguage
            {
                public override string Name => "Wolaitta";
                public override string ThreeLetterISOCode => "wal";
            }
            static WolaittaLanguage() { }
            internal static readonly ILanguage instance = new Wolaitta();
        }
        class WolayttaLanguage
        {
            class Wolaytta : ISOLanguage
            {
                public override string Name => "Wolaytta";
                public override string ThreeLetterISOCode => "wal";
            }
            static WolayttaLanguage() { }
            internal static readonly ILanguage instance = new Wolaytta();
        }
        class WolofLanguage
        {
            class Wolof : ISOLanguage
            {
                public override string Name => "Wolof";
                public override string TwoLetterISOCode => "wo";
                public override string ThreeLetterISOCode => "wol";
            }
            static WolofLanguage() { }
            internal static readonly ILanguage instance = new Wolof();
        }
        class XhosaLanguage
        {
            class Xhosa : ISOLanguage
            {
                public override string Name => "Xhosa";
                public override string TwoLetterISOCode => "xh";
                public override string ThreeLetterISOCode => "xho";
            }
            static XhosaLanguage() { }
            internal static readonly ILanguage instance = new Xhosa();
        }
        class YakutLanguage
        {
            class Yakut : ISOLanguage
            {
                public override string Name => "Yakut";
                public override string ThreeLetterISOCode => "sah";
            }
            static YakutLanguage() { }
            internal static readonly ILanguage instance = new Yakut();
        }
        class YaoLanguage
        {
            class Yao : ISOLanguage
            {
                public override string Name => "Yao";
                public override string ThreeLetterISOCode => "yao";
            }
            static YaoLanguage() { }
            internal static readonly ILanguage instance = new Yao();
        }
        class YapeseLanguage
        {
            class Yapese : ISOLanguage
            {
                public override string Name => "Yapese";
                public override string ThreeLetterISOCode => "yap";
            }
            static YapeseLanguage() { }
            internal static readonly ILanguage instance = new Yapese();
        }
        class YiddishLanguage
        {
            class Yiddish : ISOLanguage
            {
                public override string Name => "Yiddish";
                public override string TwoLetterISOCode => "yi";
                public override string ThreeLetterISOCode => "yid";
            }
            static YiddishLanguage() { }
            internal static readonly ILanguage instance = new Yiddish();
        }
        class YorubaLanguage
        {
            class Yoruba : ISOLanguage
            {
                public override string Name => "Yoruba";
                public override string TwoLetterISOCode => "yo";
                public override string ThreeLetterISOCode => "yor";
            }
            static YorubaLanguage() { }
            internal static readonly ILanguage instance = new Yoruba();
        }
        class YupikLanguagesLanguage
        {
            class YupikLanguages : ISOLanguage
            {
                public override string Name => "Yupik languages";
                public override string ThreeLetterISOCode => "ypk";
            }
            static YupikLanguagesLanguage() { }
            internal static readonly ILanguage instance = new YupikLanguages();
        }
        class ZandeLanguagesLanguage
        {
            class ZandeLanguages : ISOLanguage
            {
                public override string Name => "Zande languages";
                public override string ThreeLetterISOCode => "znd";
            }
            static ZandeLanguagesLanguage() { }
            internal static readonly ILanguage instance = new ZandeLanguages();
        }
        class ZapotecLanguage
        {
            class Zapotec : ISOLanguage
            {
                public override string Name => "Zapotec";
                public override string ThreeLetterISOCode => "zap";
            }
            static ZapotecLanguage() { }
            internal static readonly ILanguage instance = new Zapotec();
        }
        class ZazaLanguage
        {
            class Zaza : ISOLanguage
            {
                public override string Name => "Zaza";
                public override string ThreeLetterISOCode => "zza";
            }
            static ZazaLanguage() { }
            internal static readonly ILanguage instance = new Zaza();
        }
        class ZazakiLanguage
        {
            class Zazaki : ISOLanguage
            {
                public override string Name => "Zazaki";
                public override string ThreeLetterISOCode => "zza";
            }
            static ZazakiLanguage() { }
            internal static readonly ILanguage instance = new Zazaki();
        }
        class ZenagaLanguage
        {
            class Zenaga : ISOLanguage
            {
                public override string Name => "Zenaga";
                public override string ThreeLetterISOCode => "zen";
            }
            static ZenagaLanguage() { }
            internal static readonly ILanguage instance = new Zenaga();
        }
        class ZhuangLanguage
        {
            class Zhuang : ISOLanguage
            {
                public override string Name => "Zhuang";
                public override string TwoLetterISOCode => "za";
                public override string ThreeLetterISOCode => "zha";
            }
            static ZhuangLanguage() { }
            internal static readonly ILanguage instance = new Zhuang();
        }
        class ZuluLanguage
        {
            class Zulu : ISOLanguage
            {
                public override string Name => "Zulu";
                public override string TwoLetterISOCode => "zu";
                public override string ThreeLetterISOCode => "zul";
            }
            static ZuluLanguage() { }
            internal static readonly ILanguage instance = new Zulu();
        }
        class ZuniLanguage
        {
            class Zuni : ISOLanguage
            {
                public override string Name => "Zuni";
                public override string ThreeLetterISOCode => "zun";
            }
            static ZuniLanguage() { }
            internal static readonly ILanguage instance = new Zuni();
        }












    }
}
