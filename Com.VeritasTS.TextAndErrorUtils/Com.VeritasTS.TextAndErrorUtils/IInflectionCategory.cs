﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.TextAndErrorUtils.Inflection
{

    /// <summary>
    /// A category used by at least one language
    /// to govern inflection of at least one part of
    /// speech -- for example, number, gender, person, mood, etc.
    /// </summary>
    /// <remarks>
    /// This interface is used merely to provide type-checking
    /// in method argument lists; there is no contract other 
    /// than the pre-existing ISelfFormatter contract, as
    /// it promises to do nothing.</remarks>
    public interface IInflectionCategory: ISelfFormatter
    {
    }

}
