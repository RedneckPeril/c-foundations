﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.TextAndErrorUtils
{

    /// <summary>
    /// An object that can produce a text representation 
    /// of itself that is audience-preference-sensitive.
    /// </summary>
    /// <remarks>
    /// The primary intent of this class is to work with the 
    /// IAudiencePreferenceSet and ITranslatable hierarchies that allow
    /// complex error messages with run-time specific information
    /// to be presented to an individual user in the language
    /// and style he or she prefers.
    /// </remarks>
    public interface ISelfFormatter
    {

        /// <summary>
        /// The text representation of the object, formatted as
        /// specified by the preferenceSet argument, and taking
        /// into account as necessary (possibly by ignoring)
        /// the variables in the dictionary.
        /// </summary>
        /// <remarks>
        /// The returned string cannot be null. This is enforced
        /// by the test method PreferredTextIsNotNull.
        /// <para>
        /// For objects that always look the same no matter the context,
        /// this method can be implemented simply as:
        /// <code>
        /// public ToString(
        ///          IAudiencePreferenceSet preferenceSet
        ///          , IEnumerable≤ KeyValuePair≤ string, ISelfFormatter ≥ ≥ variables
        ///          ) => ToString();
        /// </code>
        /// </para>
        /// </remarks>
        /// <returns>Text representation of the object for the given preference
        /// set and variables set.</returns>
        string ToString(IAudiencePreferenceSet preferenceSet, IEnumerable<KeyValuePair< string, ISelfFormatter>> variables );

    }
}
