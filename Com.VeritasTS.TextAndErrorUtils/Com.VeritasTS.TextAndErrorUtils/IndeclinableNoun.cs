﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.TextAndErrorUtils.Inflection
{

    /// <summary>
    /// A noun that always has exactly the
    /// same form in all circumstances.
    /// </summary>
    public class IndeclinableNoun: INoun
    {

        private string _baseForm;
        private INumber _number;

        public IndeclinableNoun(
            string baseForm
            , INumber number = null
            )
        {
            _baseForm = baseForm;
            _number = number != null ? number : Numbers.NotApplicable;
        }

        /// <summary>
        /// Implements
        /// <see cref="INoun.Number"/>. 
        /// </summary>
        /// <returns>The grammatical number specified
        /// at the time of construction.</returns>
        INumber Number { get { return _number; } }

        /// <summary>
        /// Implements
        /// <see cref="INoun.Case"/>. 
        /// </summary>
        /// <returns>Cases.NotApplicable.</returns>
        ICase Case { get { return Cases.NotApplicable; } }

        /// <summary>
        /// Implements
        /// <see cref="INoun.Gender"/>. 
        /// </summary>
        /// <returns>Genders.NotApplicable.</returns>
        IGender Gender { get { return Genders.NotApplicable; } }

        /// <summary>
        /// Implements
        /// <see cref="INoun.Gender"/>. 
        /// </summary>
        /// <returns>Itself, unchanged.</returns>
        INoun TransformedInto(INumber number = null, ICase aCase = null, IGender gender = null)
        {
            return this;
        }

        /// <summary>
        /// Implements
        /// <see cref="INoun.TransformedInto(INumber, ICase, IGender)"/>. 
        /// </summary>
        /// <returns>Itself, unchanged, unless the caller tries to force it
        /// into a form different from its current form.</returns>
        INoun TransformedIntoExactly(INumber number = null, ICase aCase = null, IGender gender = null)
        {
            if (
                ( number != null && number != _number)
                || ( aCase != null && aCase != Cases.NotApplicable )
                || (aGender != null && aGender != Genders.NotApplicable)
                )
                throw new NoSuchFormExistsException(
                    this
                    , new Dictionary<Type, IInflectionCategory>
                    {
                        [typeof(INumber)] = number
                        ,
                        [typeof(ICase)] = aCase
                        ,
                        [typeof(IGender)] = aGender
                    }
                    );
            return this;
        }
    }


}
