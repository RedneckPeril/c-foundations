﻿
namespace Com.VeritasTS.TextAndErrorUtils
{

    /// <summary>
    /// This interface simply specifies that the class implementing
    /// the interface has a readable string property Name.
    /// </summary>
    /// <remarks>
    /// There is no test code since the entire contract is
    /// fulfilled if the code will compile.
    /// </remarks>
    public interface INamed
    {
        /// <summary>
        /// Returns the object's name, which could be null.
        /// </summary>
        /// <returns>The object's name.</returns>
        string Name { get; }
    }

    /// <summary>
    /// This interface specifies that the class implementing
    /// the interface is an INamed object whose
    /// Name property can be altered after completion of 
    /// construction.
    /// </summary>
    /// <remarks>
    /// There is no test code since the entire contract is
    /// fulfilled if the code will compile.
    /// </remarks>
    public interface INamedMutable
    {
        /// <summary>
        /// Resets the object's name, which could be null.
        /// </summary>
        /// <remarks>
        /// There is an optional parameter to support
        /// self-assertion in complex objects whose
        /// SetName method can be called as part of a
        /// construction process, in which case
        /// self-assertion should be disabled pending
        /// completion of construction.</remarks>
        /// <param name="value">The new value for the name.</param>
        /// <param name="assert">
        /// Where the object self-asserts after changes
        /// in value that occur after construction, the default
        /// value of true allows that self-assertion to occur,
        /// while false disables self-assertion. Where the
        /// object is not a self-asserter, this parameter has
        /// no effects.</param>
        void SetName(string value, bool assert = true);
    }

}
