﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.TextAndErrorUtils.Inflection
{

    /// <summary>
    /// The grammatical category "gender," as used in inflected
    /// languages.
    /// </summary>
    /// <remarks>
    /// This interface is used merely to provide type-checking
    /// in method argument lists; there is no contract as
    /// it promises to do nothing. It is essentially an
    /// enumeration, but one embedded within an inheritance hierarchy.
    /// </remarks>
    public interface IGender : IInflectionCategory { }
}
