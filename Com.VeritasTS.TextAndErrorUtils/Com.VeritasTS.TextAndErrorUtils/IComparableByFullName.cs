﻿
namespace Com.VeritasTS.TextAndErrorUtils
{

    /// <summary>
    /// This interface specifies that the class implementing
    /// the interface has a CompareByFullNameTo method and
    /// an EqualsByFullName method, each of which implements
    /// standard CompareTo logic against the objects' FullName
    /// property.
    /// </summary>
    public interface IComparableByFullName : IFullNamed
    {

        /// <summary>
        /// Implements standard CompareTo logic against the objects' FullName
        /// property.
        /// </summary>
        /// <remarks>
        /// By contract, the result of calling x.CompareByFullNameTo( y ),
        /// for x and y both IComparableByFullName's, should reflect the
        /// standard for behavior of CompareTo in the IComparable&lt;T&gt;
        /// interface. Specifically, null is less than non-null and two
        /// nulls are equal. So we should get x.FullName.CompareTo( y.FullName )
        /// where no nulls are involved, x.FullName.CompareTo( null ) where
        /// either y or y.FullName is null, -y.FullName.CompareTo( null ) where
        /// x.FullName is null and y.FullName is not, and 0 where x.FullName is null
        /// and so is either y or y.FullName.
        /// <list type="table">
        /// <listheader><term>Specification</term><description>Test method in BaseIComparableByFullNameTester class</description></listheader>
        /// <item><term>Where non-null x.FullName compares less than non-null y.FullName, x.CompareToByFullName(y) == -1</term><description>XComparedByFullNameToYWithAlphabeticallyLaterFullNameIsMinus1</description></item>
        /// <item><term>Where non-null x.FullName compares equal to non-null y.FullName, x.CompareToByFullName(y) == 0</term><description>XComparedByFullNameToYWithAlphabeticallyEqualFullNameIs0</description></item>
        /// <item><term>Where non-null x.FullName compares greater than non-null y.FullName, x.CompareToByFullName(y) == 1</term><description>XComparedByFullNameToYWithAlphabeticallyEarlierFullNameIs1</description></item>
        /// <item><term>Where non-null x.FullName is compared to null y.FullName, x.CompareToByFullName(y) == -1</term><description>XWithNonnullFullNameComparedByFullNameToYWithNullFullNameIsMinus1</description></item>
        /// <item><term>Where non-null x.FullName is compared to null y, x.CompareToByFullName(y) == -1</term><description>XWithNonnullFullNameComparedByFullNameToNullIsMinus1</description></item>
        /// <item><term>Where x.FullName is null and y.FullName is not, x.CompareToByFullName(y) == -1</term><description>XWithNullFullNameComparedByFullNameToYWithNonnullFullNameIs1</description></item>
        /// <item><term>Where x.FullName is null and y.FullName is null, x.CompareToByFullName(y) == 0</term><description>XWithNullFullNameComparedByFullNameToYWithNullFullNameIs0</description></item>
        /// <item><term>Where x.FullName is null and y is null, x.CompareToByFullName(y) == 0</term><description>XWithNullFullNameComparedByFullNameToNullIs0</description></item>
        /// </list>
        /// </remarks>
        int CompareByFullNameTo(IComparableByFullName other);

        /// <summary>
        /// Implements standard Equals logic against the objects' FullName
        /// property, in accordance with the CompareByFullNameTo logic.
        /// </summary>
        /// <remarks>
        /// By contract, x.EqualsInFullName(y) should return true
        /// when x.CompareByFullNameTo(y) == 0 and false otherwise.
        /// <list type="table">
        /// <listheader><term>Specification</term><description>Test method in BaseIComparableByFullNameTester class</description></listheader>
        /// <item><term>Where non-null x.FullName compares less than non-null y.FullName, x.EqualsInFullName(y) == false</term><description>XComparedByFullNameToYWithAlphabeticallyLaterFullNameIsFalse</description></item>
        /// <item><term>Where non-null x.FullName compares equal to non-null y.FullName, x.EqualsInFullName(y) == true</term><description>XComparedByFullNameToYWithAlphabeticallyEqualFullNameIsTrue</description></item>
        /// <item><term>Where non-null x.FullName compares greater than non-null y.FullName, x.EqualsInFullName(y) == false</term><description>XComparedByFullNameToYWithAlphabeticallyEarlierFullNameIsFalse</description></item>
        /// <item><term>Where non-null x.FullName is compared to null y.FullName, x.EqualsInFullName(y) == false</term><description>XWithNonnullFullNameComparedByFullNameToYWithNullFullNameIsFalse</description></item>
        /// <item><term>Where non-null x.FullName is compared to null y, x.EqualsInFullName(y) == false</term><description>XWithNonnullFullNameComparedByFullNameToNullIsFalse</description></item>
        /// <item><term>Where x.FullName is null and y.FullName is not, x.EqualsInFullName(y) == false</term><description>XWithNullFullNameComparedByFullNameToYWithNonnullFullNameIsFalse</description></item>
        /// <item><term>Where x.FullName is null and y.FullName is null, x.EqualsInFullName(y) == true</term><description>XWithNullFullNameComparedByFullNameToYWithNullFullNameIsTrue</description></item>
        /// <item><term>Where x.FullName is null and y is null, x.EqualsInFullName(y) == true</term><description>XWithNullFullNameComparedByFullNameToNullIsTrue</description></item>
        /// </list>
        /// </remarks>
        bool EqualsInFullName(IComparableByFullName other);
    }
}
