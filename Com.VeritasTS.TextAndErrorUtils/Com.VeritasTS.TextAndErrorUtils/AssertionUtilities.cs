﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.TextAndErrorUtils
{

    /// <summary>
    /// Provides utility methods for common assertions
    /// </summary>
    public class AssertionUtilities
    {

        /// <summary>
        /// Throws an exception if the provided parameter is null.
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="paramName"></param>
        /// <param name="callingMethod"></param>
        /// <returns></returns>
        public static bool NonnullParameterPassesAssertion(
            Object parameter
            , string paramName
            , string callingMethod
            )
        {
            bool retval = true;
            if (parameter == null)
                throw new ArgumentNullException(
                    paramName: paramName
                    , message: ""
                    + "Cannot provide null as '" + paramName + "' argument to "
                    + callingMethod + "."
                    );
            //if (typeof(ISelfAsserter).IsAssignableFrom(parameter.GetType()))
            //    retval = ((ISelfAsserter)parameter).PassesSelfAssertion();
            return retval;
        } 
    }
}
