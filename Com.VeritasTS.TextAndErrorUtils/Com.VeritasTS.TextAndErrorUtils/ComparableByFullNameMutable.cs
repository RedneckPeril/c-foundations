﻿
namespace Com.VeritasTS.TextAndErrorUtils
{

    /// <summary>
    /// Simple abstract implementation of IComparableByFullName
    /// interface, where class is not invariant (that is,
    /// the value of the FullName property may be changed
    /// after construction.
    /// </summary>
    abstract public class ComparableByFullNameMutable : IComparableByFullName
    {

        /// <summary>
        /// Hides default constructor.
        /// </summary>
        private ComparableByFullNameMutable() { }

        /// <summary>
        /// Construct from name.
        /// </summary>
        public ComparableByFullNameMutable(string name) { FullName = name; }

        /// <summary>
        /// Simple string property FullName, trivially both
        /// gettable and settable.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Static dual-argument CompareByFullName, analogous to
        /// the CompareByFullNameTo method required by the
        /// IComparableByFullName interface.
        /// </summary>
        /// <returns>Truth of statement "the two FullNames are equal," where two
        /// nulls are considered equal, and a null object is considered to
        /// have a null FullName.</returns>
        public static int CompareByFullName(IComparableByFullName theOne, IComparableByFullName theOther)
        {
            if (theOne != null) return theOne.CompareByFullNameTo(theOther);
            else if (theOther != null) return theOther.CompareByFullNameTo(null);
            else return 1;
        }

        /// <summary>
        /// Static dual-argument EqualsInFullName, analogous to
        /// the EqualsInFullName method required by the
        /// IComparableByFullName interface.
        /// </summary>
        /// <returns>Truth of statement "the two FullNames are equal," where two
        /// nulls are considered equal, and a null object is considered to
        /// have a null FullName.</returns>
        public static bool AreEqualInFullName(IComparableByFullName theOne, IComparableByFullName theOther)
        {
            return (CompareByFullName(theOne, theOther) == 0);
        }

        /// <summary>
        /// EqualsInFullName method required by the
        /// IComparableByFullName interface.
        /// </summary>
        /// <remarks>Two null FullNames are considered equal,
        /// and a null object is considered to have a null FullName.</remarks>
        /// <returns>Truth of statement "the two FullNames are equal."</returns>
        public bool EqualsInFullName(IComparableByFullName other)
        {
            return CompareByFullNameTo(other) == 0;
        }

        /// <summary>
        /// CompareByFullNameTo method required by the
        /// IComparableByFullName interface.
        /// </summary>
        /// <remarks>Two null FullNames are considered equal,
        /// and a null object is considered to have a null FullName.</remarks>
        /// <returns>0 if it is considered true that "the two FullNames are equal,"
        /// -1 if this's FullName is alphabetically prior to the other's FullName,
        /// and 1 if this's FullName is alphabetically subsequent to the other's FullName.</returns>
        public int CompareByFullNameTo(IComparableByFullName other)
        {
            if (FullName != null)
            {
                if (other == null) return -1;
                else return FullName.CompareTo(other.FullName);
            }
            else if (other == null) return 0;
            else if (other.FullName == null) return 0;
            else return 1;
        }

    }

}
