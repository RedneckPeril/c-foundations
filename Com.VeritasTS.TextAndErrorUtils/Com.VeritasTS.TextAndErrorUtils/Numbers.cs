﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace Com.VeritasTS.TextAndErrorUtils.Inflection
{

    /// <summary>
    /// This class generates a set of singleton,
    /// invariant INumbers covering each of the "grammatical numbers"
    /// encountered in most of the world's languages.
    /// </summary>
    /// <remarks>
    /// Along with the set of enumerated values (each of which
    /// is an invariant INumber), there is also a method
    /// Numbers producing a List of INumbers, as
    /// well as ObservableAllNumbers that returns an
    /// ObservableCollection rather than a List.
    /// <para>
    /// As this is expected to be a highly stable class, 
    /// we have optimized it for maximal laziness and performance
    /// at run-time. All properties are inlined constants,
    /// each grammatical number is a singleton, and no grammatical number is loaded
    /// if not used.
    /// </para>
    /// </remarks>
    public sealed class Numbers
    {
        Numbers() { }

        private static List<INumber> sortedByClassName;

        /// <summary>NotApplicable.</summary>
        public static INumber NotApplicable { get { return NotApplicableGenerator.instance; } }
        /// <summary>The Singular grammatical number.</summary>
        public static INumber Singular { get { return SingularGenerator.instance; } }
        /// <summary>The Dual grammatical number.</summary>
        public static INumber Dual { get { return DualGenerator.instance; } }
        /// <summary>The Plural grammatical number.</summary>
        public static INumber Plural { get { return PluralGenerator.instance; } }

        // Encapsulates reflection logic to access enumeration of static public properties,
        //   which is to say, the list of grammatical numbers. 
        static void InitializeSortedByClassName()
        {
            sortedByClassName = null;

            SortedDictionary<string, INumber> sorter = new SortedDictionary<string, INumber>();
            foreach (
                PropertyInfo property in (new Numbers()).GetType().GetRuntimeProperties().TakeWhile(
                    property
                    => property.GetMethod.IsStatic
                        & (property.PropertyType == typeof(INumber))
                    )
                )
            {
                INumber number = (INumber)(property.GetValue(null, null));
                sorter.Add(number.GetType().Name, number);
            }

            sortedByClassName = new List<INumber>();
            foreach (KeyValuePair<string, INumber> keyValuePair in sorter)
                sortedByClassName.Add(keyValuePair.Value);

        }

        /// <summary>
        /// A list of all defined grammatical numbers, in alphabetical order by type name.
        /// </summary>
        /// <returns>List of all defined grammatical numbers,
        /// in alphabetical order by class name.</returns>
        public static List<INumber> AllNumbers
        {
            get
            {
                if (sortedByClassName == null) InitializeSortedByClassName();
                return new List<INumber>(sortedByClassName);
            }
        }

        /// <summary>
        /// An ObservableCollection of all defined grammatical numbers, in alphabetical order by type name.
        /// </summary>
        /// <returns>List of all defined grammatical numbers,
        /// in alphabetical order by class name.</returns>
        public static ObservableCollection<INumber> ObservableAllNumbers
        {
            get
            {
                if (sortedByClassName == null) InitializeSortedByClassName();
                return new ObservableCollection<INumber>(sortedByClassName);
            }
        }

        /// <summary>
        /// The number of defined grammatical numbers.
        /// </summary>
        /// <returns>Count of defined grammatical numbers.</returns>
        public static int CountOfNumbers
        {
            get
            {
                if (sortedByClassName == null) InitializeSortedByClassName();
                return sortedByClassName.Count;
            }
        }

        class NotApplicableGenerator
        {
            class NotApplicable : INumber
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "not-applicable grammatical number";
                }
            }
            static NotApplicableGenerator() { }
            internal static readonly INumber instance = new NotApplicable();
        }
        class SingularGenerator
        {
            class Singular : INumber
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "singular number";
                }
            }
            static SingularGenerator() { }
            internal static readonly INumber instance = new Singular();
        }
        class DualGenerator
        {
            class Dual : INumber
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "dual number";
                }
            }
            static DualGenerator() { }
            internal static readonly INumber instance = new Dual();
        }
        class PluralGenerator
        {
            class Plural : INumber
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "plural number";
                }
            }
            static PluralGenerator() { }
            internal static readonly INumber instance = new Plural();
        }

    }
}
