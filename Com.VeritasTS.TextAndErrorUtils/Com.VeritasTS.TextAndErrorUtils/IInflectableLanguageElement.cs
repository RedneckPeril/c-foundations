﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.TextAndErrorUtils.Inflection
{

    /// <summary>
    /// A word or phrase that is subject to inflection
    /// and can provide an altered version of itself
    /// given a requested gender / number / tense, etc.
    /// </summary>
    /// <remarks>
    /// This interface is used merely to provide type-checking
    /// in method argument lists; there is no contract as
    /// it promises to do nothing.</remarks>
    public interface IInflectableLanguageElement : ISelfFormatter, ISafeMemberOfImmutableOwner, IImmutable
    {

        /// <summary>
        /// The current form of the language element.
        /// </summary>
        /// <remarks>This is what is normally returned by all forms of ToString.
        /// <para>
        /// This method never returns null. (Test method:
        /// CurrentFormIsNotNull.)
        /// </para>
        /// </remarks>
        string CurrentForm { get; }
    }

}
