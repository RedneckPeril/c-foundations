﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.TextAndErrorUtils
{

    /// <summary>
    /// ILanguage implements the very generic conception of
    /// "language" that qualifies for an ISO-639 two-character
    /// code -- which means that it can actually be a 
    /// language family. A descendant interface IDialect
    /// represents the combination of an ILanguage and
    /// a geographical region, normally a country. In other
    /// words, you should generally be able to establish
    /// an ILanguage from the first element of a CultureInfo
    /// three-field designator, and an IDialect from the first
    /// two.
    /// </summary>
    /// <remarks>
    /// </remarks>
    public interface ILanguage
    {

        /// <summary>The most common form of the name used
        /// by native speakers, preferably in the language itself
        /// and also preferably in its native script,
        /// but only where the native script is not in the
        /// private-use region of Unicode.
        /// </summary>
        /// <remarks>
        /// The Name property is a standard non-null, non-empty, trimmed
        /// string property without private-use Unicode characters.
        /// Thus the following contract must be honored
        /// by all classes implementing the interface, with respect to 
        /// the Name property:
        /// <list type="table">
        /// <listheader>
        /// <term>Contract provision</term><description>Test method</description>
        /// </listheader>
        /// <item><term>Name is not null.</term><description>NameIsNotNull</description></item>
        /// <item><term>Name is not an empty string.</term><description>NameIsNotEmptyString</description></item>
        /// <item><term>Name does not begin with whitespace.</term><description>NameDoesNotBeginWithWhitespace</description></item>
        /// <item><term>Name does not end with whitespace.</term><description>NameDoesNotEndWithWhitespace</description></item>
        /// <item><term>Name contains no private-use Unicode characters.</term><description>NameContainsNoPrivateUseUnicodeCharacters</description></item>
        /// </list> 
        /// </remarks>
        /// <returns>string: the name of the language</returns>
        string Name { get; }

        /// <summary>The Name property,
        /// transliterated into English script
        /// (with free use of diacriticals) or
        /// International Phonetic Alphabet if not already expressed
        /// that way. If the Name property is already
        /// expressed in a Latinate alphabet or the IPA,
        /// TransliteratedName == Name.
        /// </summary>
        /// <remarks>
        /// The TransliteratedName property is a standard non-null, non-empty, trimmed
        /// string property without private-use Unicode characters.
        /// Thus the following contract must be honored
        /// by all classes implementing the interface, with respect to 
        /// the Name property:
        /// <list type="table">
        /// <listheader>
        /// <term>Contract provision</term><description>Test method</description>
        /// </listheader>
        /// <item><term>TransliteratedName is not null.</term><description>TransliteratedNameIsNotNull</description></item>
        /// <item><term>TransliteratedName is not an empty string.</term><description>TransliteratedNameIsNotEmptyString</description></item>
        /// <item><term>TransliteratedName does not begin with whitespace.</term><description>TransliteratedNameDoesNotBeginWithWhitespace</description></item>
        /// <item><term>TransliteratedName does not end with whitespace.</term><description>TransliteratedNameDoesNotEndWithWhitespace</description></item>
        /// <item><term>TransliteratedName contains no private-use Unicode characters.</term><description>TransliteratedNameContainsNoPrivateUseUnicodeCharacters</description></item>
        /// </list> 
        /// </remarks>
        /// <returns>string: the transliterated name of the language</returns>
        string TransliteratedName { get; }

        /// <summary>
        /// The most common form of the name
        /// in English. Normally does not include
        /// accents and diacritical marks and enhanced
        /// Latinate characters, but this
        /// is not always the case (e.g. "Provençal, Old (to 1500)").
        /// </summary>
        /// <remarks>
        /// The EnglishName property is a standard non-null, non-empty, trimmed
        /// string property without private-use Unicode characters.
        /// Furthermore, it is a grammatical rule in English
        /// that the names of languages are considered proper names and are always
        /// capitalized, and the only language that is an exception to this rule
        /// is Klingon. (Which really and truly is recognized as a language by
        /// the ISO standards board.) Thus the following contract must be honored
        /// by all classes implementing the interface, with respect to 
        /// the Name property:
        /// <list type="table">
        /// <listheader>
        /// <term>Contract provision</term><description>Test method</description>
        /// </listheader>
        /// <item><term>EnglishName is not null.</term><description>EnglishNameIsNotNull</description></item>
        /// <item><term>EnglishName is not an empty string.</term><description>EnglishNameIsNotEmptyString</description></item>
        /// <item><term>EnglishName does not begin with whitespace.</term><description>EnglishNameDoesNotBeginWithWhitespace</description></item>
        /// <item><term>EnglishName does not end with whitespace.</term><description>EnglishNameDoesNotEndWithWhitespace</description></item>
        /// <item><term>EnglishName contains no private-use Unicode characters.</term><description>EnglishNameContainsNoPrivateUseUnicodeCharacters</description></item>
        /// <item>
        ///   <term>EnglishName begins with a capital letter unless it is tlhIngan-Hol (Klingon).</term>
        ///   <description>EnglishNameBeginsWithCapitalLetterUnlessTlhInganHol</description>
        /// </item>
        /// </list> 
        /// </remarks>
        /// <returns>string: the English name of the language</returns>
        string EnglishName { get; }

        /// <summary>
        /// The most common form of the name
        /// in English. Normally does not include
        /// accents and diacritical marks and enhanced
        /// Latinate characters, but this
        /// is not always the case (e.g. "Provençal, Old (to 1500)").
        /// </summary>
        /// <remarks>
        /// Not every language will have such a code; so null is a valid
        /// value of the property. If the language has a code, however,
        /// then it must be exactly two letters long, each letter being
        /// one of the twenty-six standard letters of the English alphabet,
        /// in lower case. The test method that validates this is
        /// TwoLetterISOCodeIsNullOrHasTwoLowerCaseStandardEnglishLetters.
        /// <para>
        /// This method corresponds to CultureInfo.TwoLetterISOLanguageName().
        /// </para>
        /// </remarks>
        /// <returns>Either null, or the two-letter ISO code.</returns>
        string TwoLetterISOCode { get; }

        /// <summary>
        /// Either the real three-letter ISO code,
        /// or one we made up for a custom language. Where there are
        /// two valid such codes recognized by the ISO standard,
        /// this is the more common code, the other being
        /// returned by the separate property AlternateThreeLetterISOCode.
        /// </summary>
        /// <remarks>
        /// Note that this is NOT necessarily the three-letter
        /// code by which the Windows OS designates the language,
        /// as the Windows OS codes are not ISO-compliant.
        /// <para>
        /// This property cannot be null. 
        /// It must be exactly three letters long, each letter being
        /// one of the twenty-six standard letters of the English alphabet,
        /// in lower case. The test method that validates this is
        /// ThreeLetterISOCodeHasThreeLowerCaseStandardEnglishLetters.
        /// </para><para>
        /// This method corresponds to CultureInfo.ThreeLetterISOLanguageName().
        /// </para>
        /// </remarks>
        /// <returns>Either null, or the two-letter ISO code.</returns>
        /// <returns>Three-letter ISO code.</returns>
        string ThreeLetterISOCode { get; }

        /// <summary>
        /// Where there are
        /// two valid three-letter codes recognized by the ISO standard,
        /// this is the less common code, the other being
        /// returned by the separate property ThreeLetterISOCode. If 
        /// the ISO standard provides only one three-letter code,
        /// this property is null.
        /// </summary>
        /// <remarks>
        /// Not every language will have such a code; so null is a valid
        /// value of the property. If the language has a code, however,
        /// then it must be exactly three letters long, each letter being
        /// one of the twenty-six standard letters of the English alphabet,
        /// in lower case. The test method that validates this is
        /// AlternateThreeLetterISOCodeIsNullOrHasThreeLowerCaseStandardEnglishLetters.
        /// </remarks>
        /// <returns>Either null, or the alternate three-letter ISO code.</returns>
        string AlternateThreeLetterISOCode { get; }

        /// <summary>The most common form of the name used
        /// by native speakers, in its native script,
        /// where the native script requires the
        /// private-use region of Unicode. If the native
        /// script name does not use Unicode private-use characters,
        /// this property is null.
        /// </summary>
        /// <remarks>
        /// This property can certainly be null (it almost always will be,
        /// in fact). If it is not null, then it must contain
        /// at least one character from a private-use region of Unicode,
        /// and it can neither begin nor end with whitespace.
        /// <para>
        /// As this information is useless to a caller without the additional
        /// information as to which custom font must be used to render the
        /// name, it must always be true that, of PrivateUseCharacterName
        /// and PrivateUseCharacterNameFontName, either both are null or neither is.</para>
        /// <list type="table">
        /// <listheader>
        /// <term>Contract provision</term><description>Test method</description>
        /// </listheader>
        /// <item>
        ///   <term>If not null, PrivateUseCharacterName contains at least one private-use Unicode character.</term>
        ///   <description>PrivateUseCharacterNameIsNullOrContainsPrivateUseUnicodeCharacter</description></item>
        /// <item>
        ///   <term>PrivateUseCharacterName does not begin with whitespace.</term>
        ///   <description>PrivateUseCharacterNameDoesNotBeginWithWhitespace</description>
        /// </item>
        /// <item>
        ///   <term>PrivateUseCharacterName does not end with whitespace.</term>
        ///   <description>PrivateUseCharacterNameDoesNotEndWithWhitespace</description>
        /// </item>
        /// <item>
        ///   <term>If PrivateUseCharacterName is null, so is PrivateUseCharacterNameFontName.</term>
        ///   <description>LanguageWithoutPrivateUseCharacterNameHasNoNonstandardFontName</description>
        /// </item>
        /// <item>
        ///   <term>If PrivateUseCharacterName is not null, neither is PrivateUseCharacterNameFontName.</term>
        ///   <description>LanguageWithoutPrivateUseCharacterNameHasNoNonstandardFontName</description>
        /// </item>
        /// </list> 
        /// </remarks>
        /// <returns>string: the private-use character name, if one
        /// exists; else null.</returns>
        string PrivateUseCharacterName { get; }

        /// <summary>If the PrivateUseCharacterName property is not null,
        /// this method holds the name of the font required for proper
        /// display. Otherwise this property is null.
        /// </summary>
        /// <remarks>
        /// This property can certainly be null (it almost always will be,
        /// in fact). If it is not null, then it must not be an empty string,
        /// and it can neither begin nor end with whitespace, nor can it
        /// contain private-use Unicode characters.
        /// <para>
        /// It must always be true that, of PrivateUseCharacterName
        /// and PrivateUseCharacterNameFontName, either both are null or neither is.</para>
        /// <list type="table">
        /// <listheader>
        /// <term>Contract provision</term><description>Test method</description>
        /// </listheader>
        /// <item>
        ///   <term>PrivateUseCharacterNameFontName is not an empty string.</term>
        ///   <description>PrivateUseCharacterNameFontNameIsNotEmptyString</description></item>
        /// <item>
        ///   <term>PrivateUseCharacterNameFontName does not begin with whitespace.</term>
        ///   <description>PrivateUseCharacterNameDoesNotBeginWithWhitespace</description>
        /// </item>
        /// <item>
        ///   <term>PrivateUseCharacterNameFontName does not end with whitespace.</term>
        ///   <description>PrivateUseCharacterNameDoesNotEndWithWhitespace</description>
        /// </item>
        /// <item>
        ///   <term>PrivateUseCharacterNameFontName does not end with whitespace.</term>
        ///   <description>PrivateUseCharacterNameFontNameContainsNoPrivateUseUnicodeCharacters</description>
        /// </item>
        /// <item>
        ///   <term>If PrivateUseCharacterNameFontName is null, so is PrivateUseCharacterName.</term>
        ///   <description>LanguageWithoutPrivateUseCharacterNameFontNameHasNoPrivateUseCharacterName</description>
        /// </item>
        /// <item>
        ///   <term>If PrivateUseCharacterNameFontName is not null, neither is PrivateUseCharacterName.</term>
        ///   <description>LanguageWithPrivateUseCharacterNameFontNameHasPrivateUseCharacterName</description>
        /// </item>
        /// </list> 
        /// </remarks>
        /// <returns>string: the name of the font required to 
        /// display the private-use character name, if one
        /// exists; else null.</returns>
        string PrivateUseCharacterNameFontName { get; }

    }
}
