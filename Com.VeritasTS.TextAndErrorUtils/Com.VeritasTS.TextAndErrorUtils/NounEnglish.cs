﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.TextAndErrorUtils.Inflection
{

    /// <summary>
    /// A noun that always has exactly the
    /// same form in all circumstances.
    /// </summary>
    public class NounEnglish : INoun
    {

        private string _currentForm;
        private string _nominativeSingular;
        private string _nominativePlural;
        private string _possessiveSingular;
        private string _possessivePlural;
        private INumber _number;
        private ICase _case;
        private IGender _gender;
        private bool _numberIsFixed;
        private NounEnglish _masculineSpecificForm;
        private NounEnglish _feminineSpecificForm;

        public NounEnglish(
            string currentForm
            , string nominativeSingular = null
            , string nominativePlural = null
            , string possessiveSingular = null
            , string possessivePlural = null
            , INumber number = null
            , ICase aCase = null
            , IGender gender = null
            , bool numberIsFixed = false
            , NounEnglish masculineSpecificForm = null
            , NounEnglish feminineSpecificForm = null
            )
        {
            _currentForm = currentForm;
            _number = number != null ? number : Numbers.Singular;
            _case = aCase != null ? aCase : Cases.Nominative;
            _gender = gender != null ? gender : Genders.NotApplicable;
            if (number.Equals(Numbers.Plural))
            {
                if (aCase.Equals(Cases.Nominative))
                {
                    _nominativePlural = currentForm;
                    if (nominativeSingular != null)
                        _nominativeSingular = nominativeSingular;
                    else if (currentForm.Substring(currentForm.Length - 3).Equals("es"))
                        _nominativeSingular = currentForm.Substring(currentForm.Length - 2);
                    else if (currentForm.Substring(currentForm.Length - 2).Equals("s"))
                        _nominativeSingular = currentForm.Substring(currentForm.Length - 1);
                    else
                        _nominativeSingular = currentForm;
                }
                else
                {
                    _possessivePlural = currentForm;
                    if (nominativeSingular != null)
                        _nominativeSingular = nominativeSingular;
                    else if (new Regex("es['’]$").IsMatch(currentForm))
                        _nominativeSingular = currentForm.Substring(currentForm.Length - 3);
                    else if (new Regex("s['’]$").IsMatch(currentForm))
                        _nominativeSingular = currentForm.Substring(currentForm.Length - 2);
                    else
                        _nominativeSingular = currentForm;
                }
            } else 
            {
                if (aCase.Equals(Cases.Nominative))
                    _nominativeSingular = currentForm;
                else
                {
                    _possessivePlural = currentForm;
                    if (nominativeSingular != null)
                        _nominativeSingular = nominativeSingular;
                    else if (new Regex("s['’]$").IsMatch(currentForm))
                        _nominativeSingular = currentForm.Substring(currentForm.Length - 1);
                    else if (new Regex("['’]s$").IsMatch(currentForm))
                        _nominativeSingular = currentForm.Substring(currentForm.Length - 2);
                    else
                        _nominativeSingular = currentForm;
                }
            };
            if (_nominativePlural == null)
            {
                if (nominativePlural != null)
                    _nominativePlural = nominativePlural;
                else if (new Regex("[a-zA-Z]y$").IsMatch(_nominativeSingular))
                    _nominativePlural
                        = _nominativeSingular.Substring(_nominativeSingular.Length - 1)
                        + "ies";
                else if (new Regex("([sxz]|([cs]h))$").IsMatch(_nominativeSingular))
                    _nominativePlural = _nominativeSingular + "es";
                else
                    _nominativePlural = _nominativeSingular + "s";
            }
            if (_possessiveSingular == null)
            {
                if (possessiveSingular != null)
                    _possessiveSingular = possessiveSingular;
                else if (_nominativeSingular.Equals("Jesus"))
                    _nominativeSingular = "Jesus’";
                else
                    _possessiveSingular = _nominativeSingular + "’s";
            }
            if (_possessivePlural == null)
            {
                if (possessivePlural != null)
                    _possessiveSingular = possessivePlural;
                else if (!new Regex("s$").IsMatch(_nominativePlural))
                    _possessivePlural = _nominativePlural + "’";
                else
                    _possessivePlural = _nominativePlural + "’s";
            }
            _numberIsFixed = numberIsFixed;
            _masculineSpecificForm = masculineSpecificForm;
            _feminineSpecificForm = feminineSpecificForm;
        }

        /// <summary>
        /// Implements
        /// <see cref="INoun.Number"/>. 
        /// </summary>
        /// <returns>The grammatical number specified
        /// at the time of construction.</returns>
        INumber Number { get { return _number; } }

        /// <summary>
        /// Implements
        /// <see cref="INoun.Case"/>. 
        /// </summary>
        /// <returns>The grammatical case specified
        /// at the time of construction.</returns>
        ICase Case { get { return _case; } }

        /// <summary>
        /// Implements
        /// <see cref="INoun.Gender"/>. 
        /// </summary>The grammatical gender specified
        /// at the time of construction
        IGender Gender { get { return Genders.NotApplicable; } }

        /// <summary>
        /// Implements
        /// <see cref="INoun.Gender"/>. 
        /// </summary>
        /// <returns>Nearest valid form.</returns>
        INoun TransformedInto(INumber number = null, ICase aCase = null, IGender gender = null)
        {
            INoun retval = null;
            if (
                (number == null || _number.Equals(number))
                && (aCase == null || _case.Equals(aCase))
                && (Gender == null || _gender.Equals(gender))
                )
                retval = this;
            else
            {
                IGender newGender = gender == null ? gender : _gender;
                if (
                    _feminineSpecificForm != null
                    && newGender.Equals(Genders.Feminine)
                    && !_gender.Equals(Genders.Feminine)
                    )
                    retval
                        = _feminineSpecificForm.TransformedInto(
                            number
                            , aCase
                            , newGender
                            );
                else if (
                    _masculineSpecificForm != null
                    && newGender.Equals(Genders.Masculine)
                    && !_gender.Equals(Genders.Masculine)
                    )
                    retval
                        = _masculineSpecificForm.TransformedInto(
                            number
                            , aCase
                            , newGender
                            );
                else
                {
                    string newCurrentForm;
                    INumber newNumber 
                        = _numberIsFixed || number == null ? _number
                        : number;
                    ICase newCase = aCase != null ? aCase : _case;
                    if (Numbers.Singular.Equals(newNumber))
                    {
                        if (Cases.Nominative.Equals(newCase))
                            newCurrentForm = _nominativeSingular;
                        else
                            newCurrentForm = _possessiveSingular;
                    }
                    else
                    {
                        if (Cases.Nominative.Equals(newCase))
                            newCurrentForm = _nominativePlural;
                        else
                            newCurrentForm = _possessivePlural;
                    }
                    retval = new NounEnglish(
                        newCurrentForm
                        , _nominativeSingular
                        , _nominativePlural
                        , _possessiveSingular
                        , _possessivePlural
                        , newNumber
                        , newCase
                        , newGender
                        , _numberIsFixed
                        , _masculineSpecificForm
                        , _feminineSpecificForm
                        );
                }
            }
        }

        /// <summary>
        /// Implements
        /// <see cref="INoun.TransformedInto(INumber, ICase, IGender)"/>. 
        /// </summary>
        /// <returns>Itself, unchanged, unless the caller tries to force it
        /// into a form different from its current form.</returns>
        INoun TransformedIntoExactly(INumber number = null, ICase aCase = null, IGender gender = null)
        {
            if (
                (number != null && number != _number)
                || (aCase != null && aCase != Cases.NotApplicable)
                || (aGender != null && aGender != Genders.NotApplicable)
                )
                throw new NoSuchFormExistsException(
                    this
                    , new Dictionary<Type, IInflectionCategory>
                    {
                        [typeof(INumber)] = number
                        ,
                        [typeof(ICase)] = aCase
                        ,
                        [typeof(IGender)] = aGender
                    }
                    );
            return this;
        }
    }


}
