﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace Com.VeritasTS.TextAndErrorUtils
{

    /// <summary>
    /// This class provides a very simple implementation of an AudiencePreferenceSet.
    /// </summary>
    public class ImmutableAudiencePreferenceSet  : IAudiencePreferenceSet
    {
        private CultureInfo cultureInfo;
        private ILanguage language;

        /// <summary>
        /// Exception class dealing with case where ImmutableAudiencePreferenceSet
        /// is initialized from either a null cultureInfo or else from one whose
        /// two-letter ISO code does not refer to a valid language.
        /// </summary>
        /// <remarks>
        /// The class is invariant and threadsafe.
        /// </remarks>
        public class IndecipherableLanguageException: Exception
        {
            private static string NewMessage( CultureInfo cultureInfo )
            {
                if (cultureInfo == null) return "Cannot derive language from null CultureInfo.";
                else
                    return "Could not derive language from two-letter code "
                        + cultureInfo.TwoLetterISOLanguageName
                        + "drawn from CultureInfo.TwoLetterISOLanguageName.";
            }
            private CultureInfo cultureInfo;
            private IndecipherableLanguageException() { }

            /// <summary>
            /// Constructor taking the CultureInfo argument that could not
            /// be used to determine the language; this is the only valid constructor.
            /// </summary>
            /// <remarks>
            /// <list type="table">
            /// <listheader><term>Specification</term><description>Test method in ImmutableAudiencePreferenceSetTester class</description></listheader>
            /// <item>
            ///   <term>IndecipherableLanguageException can be constructed from null cultureInfo argument.</term>
            ///   <description>IndecipherableLanguageExceptionConstructedFromNullHasNullCultureInfo</description>
            /// </item>
            /// <item>
            ///   <term>IndecipherableLanguageException can be constructed from non-null cultureInfo argument.</term>
            ///   <description>CultureInfoOfIndecipherableLanguageExceptionEqualsConstructionArgument</description>
            /// </item>
            /// </list> 
            /// </remarks>
            /// <param name="cultureInfo">CultureInfo: the CultureInfo argument
            /// from which it proved impossible to determine the language.</param>
            public IndecipherableLanguageException( CultureInfo cultureInfo ):
                base( NewMessage( cultureInfo ) )
            {
                this.cultureInfo = cultureInfo;
            }

            /// <summary>
            /// Read-only copy of the CultureInfo argument
            /// from which it proved impossible to determine the language. 
            /// </summary>
            /// <remarks>
            /// <list type="table">
            /// <listheader><term>Specification</term><description>Test method in ImmutableAudiencePreferenceSetTester class</description></listheader>
            /// <item>
            ///   <term>IndecipherableLanguageException constructed from null cultureInfo has null CultureInfo property.</term>
            ///   <description>IndecipherableLanguageExceptionConstructedFromNullHasNullCultureInfo</description>
            /// </item>
            /// <item>
            ///   <term>IndecipherableLanguageException constructed from non-null cultureInfo has CultureInfo property that Equals original argument.</term>
            ///   <description>CultureInfoOfIndecipherableLanguageExceptionEqualsConstructionArgument</description>
            /// </item>
            /// <item>
            ///   <term>IndecipherableLanguageException constructed from non-null cultureInfo has CultureInfo property that does not point to original argument.</term>
            ///   <description>CultureInfoOfIndecipherableLanguageExceptionIsNotSameInstanceAsConstructionArgument</description>
            /// </item>
            /// </list> 
            /// </remarks>
            public CultureInfo CultureInfo {
                get
                {
                    if (cultureInfo == null) return null;
                    //else return new CultureInfo( cultureInfo.DisplayName );
                    else return (CultureInfo)(cultureInfo.Clone());
                }
            }

        }

        private void InitFromCultureInfo(CultureInfo cultureInfo)
        {
            this.cultureInfo = cultureInfo;
            if ( cultureInfo == null ) this.cultureInfo = CultureInfo.CurrentCulture;
            this.language = ISOLanguages.GetLanguageWithTwoOrThreeLetterISOCode(this.cultureInfo.TwoLetterISOLanguageName);
            if (language == null) throw new IndecipherableLanguageException(this.cultureInfo);
        }

        /// <summary>
        /// Constructs itself using CurrentCulture.
        /// </summary>
        /// <remarks>
        /// <list type="table">
        /// <listheader><term>Specification</term><description>Test method in ImmutableAudiencePreferenceSetTester class</description></listheader>
        /// <item>
        ///   <term>Object generated by default constructor has CultureInfo that Equals CurrentCulture.</term>
        ///   <description>CultureInfoEqualsCurrentCultureWhenDefaultConstructed</description>
        /// </item>
        /// </list>
        /// </remarks>
        /// <exception cref="IndecipherableLanguageException">Throws ImmutableAudiencePreferenceSet.IndecipherableLanguageException
        /// if no ISO language can be derived from the current culture's two-character ISO language code.</exception>
        public ImmutableAudiencePreferenceSet()
        {
            InitFromCultureInfo( null );
        }

        /// <summary>
        /// Constructs itself using caller-supplied CultureInfo.
        /// </summary>
        /// <remarks>
        /// <list type="table">
        /// <listheader><term>Specification</term><description>Test method in ImmutableAudiencePreferenceSetTester class</description></listheader>
        /// <item>
        ///   <term>Object generated by non-default constructor with null argument as CultureInfo that Equals CurrentCulture.</term>
        ///   <description>CultureInfoEqualsCurrentCultureWhenNullConstructorArgumentSupplied</description>
        /// </item>
        /// <item>
        ///   <term>Object generated by non-default constructor has CultureInfo that Equals construction argument.</term>
        ///   <description>CultureInfoEqualsConstructionArgumentWhenNonnullArgumentSupplied</description>
        /// </item>
        /// <item>
        ///   <term>Object generated by non-default constructor has CultureInfo that does not point to construction argument.</term>
        ///   <description>CultureInfoIsNotConstructionArgumentWhenNonnullArgumentSupplied</description>
        /// </item>
        /// <item>
        ///   <term>Attempt to construct from CultureInfo with indecipherable language fails with IndecipherableLanguageException.</term>
        ///   <description>AttemptToConstructFromCultureInfoWithBadLanguageCausesIndecipherableLanguageException</description>
        /// </item>
        /// </list>
        /// </remarks>
        /// <param name="cultureInfo">CultureInfo: CultureInfo object to be used to derive audience preferences and language.</param>
        /// <exception cref="IndecipherableLanguageException">Throws ImmutableAudiencePreferenceSet.IndecipherableLanguageException
        /// if no ISO language can be derived from the cultureInfo argument's two-character ISO language code.</exception>
        public ImmutableAudiencePreferenceSet(CultureInfo cultureInfo)
        {
            InitFromCultureInfo(cultureInfo);
        }

        /// <summary>
        /// The CultureInfo preferred by the audience.
        /// </summary>
        /// <remarks>Protects invariance of private member.
        /// <list type="table">
        /// <listheader><term>Specification</term><description>Test method in ImmutableAudiencePreferenceSetTester class</description></listheader>
        /// <item>
        ///   <term>CultureInfo property is not null even when null constructor argument supplied.</term>
        ///   <description>CultureInfoEqualsCurrentCultureWhenNullConstructorArgumentSupplied</description>
        /// </item>
        /// <item>
        ///   <term>Object has CultureInfo that Equals construction argument.</term>
        ///   <description>CultureInfoEqualsConstructionArgumentWhenNonnullArgumentSupplied</description>
        /// </item>
        /// <item>
        ///   <term>Object has CultureInfo that does not point to construction argument.</term>
        ///   <description>CultureInfoIsNotConstructionArgumentWhenNonnullArgumentSupplied</description>
        /// </item>
        /// </list>
        /// </remarks>
        /// <return>Read-only version of the cultureInfo member.</return>
        public CultureInfo CultureInfo => CultureInfo.ReadOnly(cultureInfo);

        /// <summary>
        /// The language preferred by the audience.
        /// </summary>
        /// <remarks>As the ISO languages are invariant, it
        /// is safe to return the handle of the private member.
        /// <list type="table">
        /// <item>
        ///   <term>Language property Equals language derived from original construction argument, when original argument
        /// has two-character language code.</term>
        ///   <description>LanguageMatchesCultureInfoWithTwoLetterCode</description>
        /// </item>
        /// <item>
        ///   <term>Language property Equals language derived from original construction argument, when original argument
        /// has three-character language code.</term>
        ///   <description>LanguageMatchesCultureInfoWithThreeLetterCode</description>
        /// </item>
        /// </list>
        /// </remarks>
        /// <return>The private (and invariant) language member.</return>
        public ILanguage Language => language;

        /// <summary>
        /// Returns an object that provides formatting services for the specified type.
        /// </summary>
        /// <remarks>Simply forwards the call to its cultureInfo member.</remarks>
        /// <param name="formatType">An object that specifies the type of format object to return.</param>
        /// <return>An instance of the object specified by formatType, if the 
        /// cultureInfo member can supply that type of object; otherwise, null.</return>
        public object GetFormat(Type formatType)
        {
            return cultureInfo.GetFormat(formatType);
        }

    }
}
