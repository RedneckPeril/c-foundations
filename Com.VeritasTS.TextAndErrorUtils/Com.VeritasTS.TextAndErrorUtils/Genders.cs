﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace Com.VeritasTS.TextAndErrorUtils.Inflection
{

    /// <summary>
    /// This class generates a set of singleton,
    /// invariant IGenders covering each of the grammatical genders
    /// encountered in most of the world's languages.
    /// </summary>
    /// <remarks>
    /// Along with the set of enumerated values (each of which
    /// is an invariant IGender), there is also a method
    /// Genders producing a List of IGenders, as
    /// well as ObservableAllGenders that returns an
    /// ObservableCollection rather than a List.
    /// <para>
    /// As this is expected to be a highly stable class, 
    /// we have optimized it for maximal laziness and performance
    /// at run-time. All properties are inlined constants,
    /// each grammatical gender is a singleton, and no grammatical gender is loaded
    /// if not used.
    /// </para>
    /// </remarks>
    public sealed class Genders
    {
        Genders() { }

        private static List<IGender> sortedByClassName;

        /// <summary>The "n/a (not applicable)" gender, for uninflected languages.</summary>
        public static IGender NotApplicable { get { return NotApplicableGenerator.instance; } }
        /// <summary>The masculine gender.</summary>
        public static IGender Masculine { get { return MasculineGenerator.instance; } }
        /// <summary>The feminine gender.</summary>
        public static IGender Feminine { get { return FeminineGenerator.instance; } }
        /// <summary>The neuter gender.</summary>
        public static IGender Neuter { get { return NeuterGenerator.instance; } }

        // Encapsulates reflection logic to access enumeration of static public properties,
        //   which is to say, the list of grammatical genders. 
        static void InitializeSortedByClassName()
        {
            sortedByClassName = null;

            SortedDictionary<string, IGender> sorter = new SortedDictionary<string, IGender>();
            foreach (
                PropertyInfo property in (new Genders()).GetType().GetRuntimeProperties().TakeWhile(
                    property
                    => property.GetMethod.IsStatic
                        & (property.PropertyType == typeof(IGender))
                    )
                )
            {
                IGender theGender = (IGender)(property.GetValue(null, null));
                sorter.Add(theGender.GetType().Name, theGender);
            }

            sortedByClassName = new List<IGender>();
            foreach (KeyValuePair<string, IGender> keyValuePair in sorter)
                sortedByClassName.Add(keyValuePair.Value);

        }

        /// <summary>
        /// A list of all defined grammatical genders, in alphabetical order by type name.
        /// </summary>
        /// <returns>List of all defined grammatical genders,
        /// in alphabetical order by class name.</returns>
        public static List<IGender> AllGenders
        {
            get
            {
                if (sortedByClassName == null) InitializeSortedByClassName();
                return new List<IGender>(sortedByClassName);
            }
        }

        /// <summary>
        /// An ObservableCollection of all defined grammatical genders, in alphabetical order by type name.
        /// </summary>
        /// <returns>List of all defined grammatical genders,
        /// in alphabetical order by class name.</returns>
        public static ObservableCollection<IGender> ObservableAllGenders
        {
            get
            {
                if (sortedByClassName == null) InitializeSortedByClassName();
                return new ObservableCollection<IGender>(sortedByClassName);
            }
        }

        /// <summary>
        /// The number of defined grammatical genders.
        /// </summary>
        /// <returns>Count of defined grammatical genders.</returns>
        public static int CountOfGenders
        {
            get
            {
                if (sortedByClassName == null) InitializeSortedByClassName();
                return sortedByClassName.Count;
            }
        }

        class NotApplicableGenerator
        {
            class NotApplicable : IGender
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "not-applicable gender";
                }
            }
            static NotApplicableGenerator() { }
            internal static readonly IGender instance = new NotApplicable();
        }
        class MasculineGenerator
        {
            class Masculine : IGender
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "masculine gender";
                }
            }
            static MasculineGenerator() { }
            internal static readonly IGender instance = new Masculine();
        }
        class FeminineGenerator
        {
            class Feminine : IGender
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "feminine gender";
                }
            }
            static FeminineGenerator() { }
            internal static readonly IGender instance = new Feminine();
        }
        class NeuterGenerator
        {
            class Neuter : IGender
            {
                public string ToString(IAudiencePreferenceSet aps, IEnumerable<KeyValuePair<string, ISelfFormatter>> variables)
                {
                    return "neuter gender";
                }
            }
            static NeuterGenerator() { }
            internal static readonly IGender instance = new Neuter();
        }

    }
}
