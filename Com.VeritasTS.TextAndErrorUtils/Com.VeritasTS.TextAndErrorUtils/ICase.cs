﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.TextAndErrorUtils.Inflection
{

    /// <summary>
    /// The grammatical category "case," as used in inflected
    /// languages.
    /// </summary>
    /// <remarks>
    /// This interface is used merely to provide type-checking
    /// in method argument lists; there is no contract other
    /// than the standard ISelfFormatter contract adhered
    /// to by all IInflectionCategories.
    /// </remarks>
    public interface ICase : IInflectionCategory { }
}
