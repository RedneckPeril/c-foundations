﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.TextAndErrorUtils
{

    /// <summary>
    /// Exception throws when an argument to a method
    /// is required to be non-null but somebody passes a null
    /// in anyway.
    /// </summary>
    public class NullArgumentException : ArgumentException
    {

        private static string ConstructionMessage(
            string paramName
            , string callingMethodName
            )
        {
            string retval = "Caller attempted to call method ";
            if (callingMethodName == null) retval += "[null -- this is itself an error]";
            else retval += callingMethodName;
            retval += " using a null value for the ";
            if (paramName == null) retval += "[null -- this is itself an error]";
            else retval += paramName;
            retval += " parameter, which cannot be null.";
            return retval;
        }

        private NullArgumentException() { }

        /// <summary>
        /// Constructor taking name of parameter and full name of method
        /// </summary>
        public NullArgumentException(
            string paramName
            , string callingMethodName
            ) : base(ConstructionMessage(paramName, callingMethodName), paramName)
        {
        }

        /// <summary>
        /// Constructor taking name of parameter, full name of method and explicit message
        /// </summary>
        public NullArgumentException(
            string paramName
            , string callingMethodName
            , string message
            ) : base(message, paramName)
        {
        }

    }

    /// <summary>
    /// Exception throws when an argument to a class constructor
    /// is required to be non-null but somebody passes a null
    /// in anyway.
    /// </summary>
    public class NullConstructorArgumentException : NullArgumentException
    {

        private string className;

        private static string ConstructionMessage(
            string paramName
            , string className
            )
        {
            string retval = "Caller attempted to construct an object of class ";
            if (className == null) retval += "[null -- this is itself an error]";
            else retval += className;
            retval += " using a null argument for the ";
            if (paramName == null) retval += "[null -- this is itself an error]";
            else retval += paramName;
            retval += " parameter, which cannot be null.";
            return retval;
        }

        private NullConstructorArgumentException(): base("","") { }

        /// <summary>
        /// Constructor taking name of parameter and full name of method
        /// </summary>
        public NullConstructorArgumentException(
            string paramName
            , string className
            ) : base(ConstructionMessage(paramName, className), paramName)
        {
            if ( className != null ) this.className = className;
            else this.className = "";
        }

        /// <summary>
        /// Name of class whose construction failed.
        /// </summary>
        public string ClassName {  get { return className; } }

    }

}
