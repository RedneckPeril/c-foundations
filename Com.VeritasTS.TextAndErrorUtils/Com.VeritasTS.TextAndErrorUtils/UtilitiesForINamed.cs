﻿using System;
using System.Text.RegularExpressions;

namespace Com.VeritasTS.TextAndErrorUtils
{
    public class UtilitiesForINamed
    {
        private static Regex _nonWhitespace;
        public static bool HasNonWhitespace( string value )
        {
            if (_nonWhitespace == null) _nonWhitespace = new Regex("\\S");
            return _nonWhitespace.IsMatch(value);
        }

        public static bool NonnullNamedParameterIsValid(INamed param, string objectTypeName, string paramName, string callingMethod)
        {
            if (param == null)
                throw new ArgumentNullException(
                    paramName: paramName
                    , message: ""
                    + "Cannot provide null as '" + paramName + "' argument to "
                    + callingMethod + "."
                    );
            if (param.Name == null)
                throw new ArgumentNullException(
                    paramName: paramName + ".Name"
                    , message: ""
                    + "Cannot provide " + objectTypeName + " with null Name as '" + paramName + "' argument to "
                    + callingMethod + "."
                    );
            return true;
        }

        //public static bool NamedIsUsable(INamed obj, string objectTypeName )
        //{
        //    if (obj == null)
        //        throw new NotUsableException(
        //            obj
        //            , message: "Named object "
        //            + ( objectTypeName == null
        //            ? ""
        //            : ( objectTypeName.Length == 0 ? "" : "of type '" + objectTypeName + "' " )
        //            )
        //            + "is not usable because it is null."
        //            );
        //    if (obj.Name == null)
        //        throw new NotUsableException(
        //            obj
        //            , message: "Named object "
        //            + (objectTypeName == null
        //            ? ""
        //            : (objectTypeName.Length == 0 ? "" : "of type '" + objectTypeName + "' ")
        //            )
        //            + "is not usable because its name is null."
        //            );
        //    if (obj.Name.Length == 0)
        //        throw new NotUsableException(
        //            obj
        //            , message: "Named object "
        //            + (objectTypeName == null
        //            ? ""
        //            : (objectTypeName.Length == 0 ? "" : "of type '" + objectTypeName + "' ")
        //            )
        //            + "is not usable because its name is an empty string."
        //            );
        //    if (!(new Regex("\\S").IsMatch(obj.Name)))
        //        throw new NotUsableException(
        //            obj
        //            , message: "Named object "
        //            + (objectTypeName == null
        //            ? ""
        //            : (objectTypeName.Length == 0 ? "" : "of type '" + objectTypeName + "' ")
        //            )
        //            + "is not usable because its name contains nothing but whitespace."
        //            );
        //    return true;
        //}

        public static bool ObjectWithNonnullNamedPropertyIsValidParameter(
            INamed obj
            , string objectTypeName
            , string paramName
            , string namedPropertyName
            , string namedPropertyType
            , bool namedIsProperty
            , string callingMethod
            )
        {
            string propertyDescriptor = namedPropertyName + " " + (namedIsProperty ? "property" : "method");
            string namedPropertyFullDescription
                = paramName + "." + namedPropertyName;
            if (obj == null)
                throw new ArgumentNullException(
                    paramName: namedPropertyFullDescription
                    , message: ""
                    + "Cannot provide " + objectTypeName 
                    + " with null " + propertyDescriptor
                    + " as '" + paramName + "' argument to "
                    + callingMethod + "."
                    );
            if (obj.Name == null)
                throw new ArgumentNullException(
                    paramName: namedPropertyFullDescription + ".Name"
                    , message: ""
                    + "Cannot provide " + objectTypeName
                    + " whose " + propertyDescriptor
                    + " has null Name as '" + paramName + "' argument to "
                    + callingMethod + "."
                    );
            return true;
        }

        public static string NameOf(INamed obj, string defaultValue = null)
        {
            if (obj == null) return defaultValue;
            else if (obj.Name != null) return obj.Name;
            else return defaultValue;
        }

    }

    public class NullNameException : Exception
    {
        public NullNameException() : base(
                message: "Cannot set Name property of IDataProc object to null."
                )
        { }
    }

    public class EmptyStringNameException : Exception
    {
        public EmptyStringNameException() : base(
                message: "Cannot set Name property of IDataProc object to empty string."
                )
        { }
    }

    public class WhitespaceOnlyNameException : Exception
    {
        private WhitespaceOnlyNameException() : base() { }
        public WhitespaceOnlyNameException(
            string name
            ) : base(
                message: "Cannot set Name property of IDataProc "
                + "object to whitespace-only string '"
                + name + "'."
                )
        {
            Name = name;
        }
        public string Name { get; }
    }

}
