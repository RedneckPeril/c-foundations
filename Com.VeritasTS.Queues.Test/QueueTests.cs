﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Com.VeritasTS.DatabaseUtilities;
using Com.VeritasTS.DataTableDef;
using Com.VeritasTS.DatabaseComm;


namespace Com.VeritasTS.Queues.Test
{
    [TestFixture]

    public class QueueTests
    {
        Database _db = new Database(new Server(new Vendor { Name = "SQLServer" }))
        { Name = "Mazi_test", ConnectionString = @"Data Source=SERVER02\SQLEXPRESS;Initial Catalog=Mazi_test;Integrated Security=True" };

        [Test]
        public void GetAddIntoQueuesSqlTest()
        {
            //IQueue queue = new TestQueue(new testQueueTable()) { Database = _db };
            IQueue queue = new testQueueTableQ() { Database = _db };
            string columns = "Name, JobsVableName, QisVableName, AcceptableDelayInMinutes, TargetJobSize, LogName, maxRowsInLog, blacklistVableName, MaxOwnerCount";
            string inputs = "'name', 'JobsVableName', 'QisVableName', 5.0, 500, 'LogName', 1000, 'blacklistVableName', 2";
            string sql = SqlTranslatorSqlServer2014.GetAddIntoQueuesSql(columns, inputs, queue.Schema.Name);
            System.Diagnostics.Debug.Print(sql);
            Assert.IsTrue(sql.Length > 0);
        }

        [Test]
        public void EnsureExistenceTest()
        {
            IQueue queue = new testQueueTableQ() { Database = _db, TriggerTable = new testQueueTable()};
            //Assert.IsFalse(queue.Exists());

            queue.EnsureExistence();
            Assert.IsTrue(queue.Exists());

            //string sql = "delete from vtsQs.queueProcInfo where name = '" + queue.Name + "'";
            //queue.Database.SqlExecuteNonQuery(sql);
            //sql = "delete from vtsQs.queues where name = '" + queue.Name + "'";
            //queue.Database.SqlExecuteNonQuery(sql);
            _db.DisposeDbConnection();
        }
        [Test]
        public void EnsureExistenceTest2()
        {
            IQueue queue = new TestQueue1 { Database = _db };
            //Assert.IsFalse(queue.Exists());

            queue.EnsureExistence();
            Assert.IsTrue(queue.Exists());

            //string sql = "delete from vtsQs.queueProcInfo where name = '" + queue.Name + "'";
            //queue.Database.SqlExecuteNonQuery(sql);
            //sql = "delete from vtsQs.queues where name = '" + queue.Name + "'";
            //queue.Database.SqlExecuteNonQuery(sql);
            _db.DisposeDbConnection();
        }
        //[Test]
        //public void MakeJobsTest()
        //{
        //    IQueue queue = new testQueueTableQ() { Database = _db, TriggerTable = new testQueueTable() };
        //    ITable table = queue.TriggerTable;
        //    string sql = "create table ##temp_i ";
        //    string tableColumns = "(";
        //    foreach (var col in table.Columns)
        //    {
        //        if (!col.IsIdentity && !col.IsCalc)
        //            tableColumns += col.Name + " " + col.DataType + " null,";
        //    }
        //    tableColumns = tableColumns.Substring(0, tableColumns.Length - 1) + ");";
        //    sql += tableColumns;
        //    queue.Database.SqlExecuteNonQuery(sql);

        //    sql = "create table ##temp_d " + tableColumns;
        //    queue.Database.SqlExecuteNonQuery(sql);

        //    //queue.MakeJobs("##temp_i", "##temp_d");
        //    //queue.MakeJobs("##temp_i", "");
        //    //queue.MakeJobs("", "##temp_d");
        //}


        [Test]
        public void ProcessJobMakeJobsTest() // 1st tier queue process
        {
            IQueue queue = new testQueueTableQ { Database = _db };
            queue.ProcessJob();
            string sql = "select count(*) cnt from TestQueue1jobs where status= 'U'";
            Assert.IsTrue(Convert.ToInt32(queue.Database.SqlExecuteSingleValue(sql)) > 0);
            queue.Database.DisposeDbConnection();
        }

        [Test]
        public void SecondTierQueueProcTest() // 2nd tier queue process
        {
            IQueue queue = new TestQueue1 { Database = _db };
            queue.ProcessJob();
           // queue.StartProcess(ref jobNum, ref qiId);
           // Assert.IsTrue(jobNum > 0 && qiId > 0);
            queue.Database.DisposeDbConnection();
        }
        [Test]
        public void GetQueuesToMakeTest()
        {
            IQueue queue = new testQueueTableQ { Database = _db };
            queue.GetQueuesToMake();
            Assert.IsTrue(queue.QueuesToMake.Count > 0);
            queue.Database.DisposeDbConnection();
        }
        //[Test]
        //public void UpdateQueueProcInfoJobsReadyTest()
        //{
        //    testQueueTableQ queue = new testQueueTableQ { Database = _db, TriggerTable = new testQueueTable() };
        //    queue.SendQueueProcRequest();
        //    queue.Database.DisposeDbConnection();
        //}
    }

}
