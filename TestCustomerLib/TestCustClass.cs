﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InterfaceChangeResponder;

namespace TestCustomerLib
{
    public class TestCustClass : IChangeResponder
    {
        //public bool IsQueue { get; set; } = false;

        public void RespondToChange(int jobNum, int qiId, object TriggerQueue = null)
        {
            System.Diagnostics.Debug.Print("jobNum = " + jobNum.ToString() + ", qiId = " + qiId.ToString());
        }
    }

    public class TestClass
    {
        int n = 0;
    }
}
