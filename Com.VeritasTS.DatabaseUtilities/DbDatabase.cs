﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.SqlServer.Server;


using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.DatabaseUtilities
{
    public interface IDatabase : IComparable, IComparableByFullName, ISafelyCloneable
    {
        string Name { get; set; }
        IServer Server { get; set; }
        string ConnectionString { get; set; }
        bool OpenDbConnection();
        SqlConnection Connection { set; }
        List<string[]> SqlExecuteReader(string sql, SqlTransaction trans = null);
        object SqlGetSingleValue(string sql, SqlTransaction trans = null);
        void SqlExecNonQuery(string sql, SqlTransaction trans = null);
        object SqlExecScalar(string sql, SqlTransaction trans = null);
        List<GenNameValue> ExecStoredProcedure(string procedure, SqlTransaction trans = null);
        void AddStoredProcedureParameter(string name, SqlDbType dataType, object value, ParameterDirection direction, int size = -1, bool refresh = false);
        SqlTransaction BeginTransaction();
        void DisposeDbConnection();

        string GetTableExistsSql(ITable table);
        string GetColumnExistsSql(IColumn column);
        string GetTriggerExistsSql(ITrigger trigger);
        string GetKeyExistsSql(ITable table, string keyName, bool isUnique);
        string GetIndexExistsSql(IIndex index);
        string GetProcedureExistsSql(IStoredProcedure proc);
        string GetTableColumnsSql(ITable table);
        string GetTableIdentityColumnSql(ITable table);
        string GetTableComputedColumnsSql(ITable table);
        string GetAddTableColumnSql(IColumn column);
        string GetDropTableColumnSql(IColumn column);
        string GetAddUniqueKeySql(IUniqueKey key);
        string GetDropKeySql(ITable table, string keyName);
        string GetTableForeignKeyDependentsSql(ITable table);
        string GetAddForeignKeySql(IForeignKey key);
        string GetAddPrimaryKeySql(IPrimaryKey key);
        string GetAddIndexSql(IIndex index);
        string GetDropIndexSql(IIndex index);
        string GetCheckConstraintExistsSql(ICheckConstraint constraint);
        string GetAddCheckConstraintSql(ICheckConstraint constraint);
        string GetDropCheckConstraintSql(ICheckConstraint constraint);
        string GetDefaultExistsSql(IDefault theDefault);
        string GetAddDefaultExistsSql(IDefault theDefault);
        string GetDropDefaultExistsSql(IDefault theDefault);
        string GetAddIntoQueuesSql(string columnList, string inputList, string schema);
        string GetFalseSQL();
        string GetTrueSQL();
        string GetBooleanType();
    }

    public class Database : IComparableByName, IDatabase
    {
        public string Name { get; set; }
        public IServer Server { get; set; }
        public string ConnectionString { get; set; }
        public SqlConnection Connection { set { _conn = value; } }
        public string FullName { get; set; }
        public bool IsSingleton { get; set; } = false;
        public bool IsInvariant { get; set; } = true;

        private SqlConnection _conn;
        private List<SqlParameter> _parameters = new List<SqlParameter>();
        private List<SqlParameter> _outParameters = new List<SqlParameter>();
        public Database(IServer server)
        {
            Server = (IServer)server.GetThreadsafeClone();
        }
        public int CompareToByName(INamed named)
        {
            return Name.CompareTo(named);
        }

        public int CompareTo(object other)
        {
            return CompareToByName((IComparableByName)other);
        }
        public int CompareToByFullName(IFullNamed named)
        {
            return Name.CompareTo(named);
        }
        public object GetClone() { return GetThreadsafeClone(); }
        public object GetThreadsafeClone()
        {
            Server retval = new Server(Server.Vendor) { Name = this.Name };
            return retval;
        }

        public bool OpenDbConnection()
        {
            _conn = new SqlConnection(ConnectionString);
            try
            {
                _conn.Open();
            }
            catch (Exception e)
            {
#if DEBUG
                Console.WriteLine("Database.OpenDbConnection:" + e.Message);
                System.Diagnostics.Debug.WriteLine("Database.OpenDbConnection:" + e.Message);
#endif
                return false;
            }
            return true;
        }
        public List<string[]> SqlExecuteReader(string sql, SqlTransaction trans = null)
        {
            if (!IsConnectionOpen())
                OpenDbConnection();
            using (SqlCommand cmd = new SqlCommand(sql, _conn))
            {
                if (trans != null) cmd.Transaction = trans;
                List<string[]> list = new List<string[]>();
                try
                {
                    int fieldCount = 0;
                    SqlDataReader r = cmd.ExecuteReader();
                    if (r.Read())
                    {
                        fieldCount = r.FieldCount;
                        do
                        {
                            string[] row = new string[fieldCount];
                            for (int i = 0; i < fieldCount; i++)
                            {
                                if (!DBNull.Value.Equals(r.GetValue(i)))
                                    row[i] = r.GetValue(i).ToString();
                                else
                                    row[i] = "";
                            }
                            list.Add(row);
                        } while (r.Read());
                    }
                    r.Close();
                    return list;
                }
                catch (Exception e)
                {
#if DEBUG
                    Console.WriteLine("Database.SqlExecuteReader:" + e.Message);
                    System.Diagnostics.Debug.WriteLine("Database.SqlExecuteReader:" + e.Message);
#endif
                    throw (e);
                }
            }
        }
        public object SqlGetSingleValue(string sql, SqlTransaction trans = null)
        {
            if (!IsConnectionOpen())
                OpenDbConnection();
            object obj = null;

            using (SqlCommand cmd = new SqlCommand(sql, _conn))
            {
                if (trans != null) cmd.Transaction = trans;
                using (SqlDataReader r = cmd.ExecuteReader())
                {
                    if (r.Read()) obj = r[0];
                }
            }
            return obj;
        }
        public void SqlExecNonQuery(string sql, SqlTransaction trans = null)
        {
            if (!IsConnectionOpen())
                OpenDbConnection();
#if !DEBUG
        SqlPipe sp = SqlContext.Pipe;
        sp.Send("SqlExecNonQuery.sql = " + sql);
#endif
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = sql;
                cmd.Connection = _conn;
                if (trans != null) cmd.Transaction = trans;
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
#if DEBUG
                    Console.WriteLine("Database.SqlExecNonQuery:" + e.Message);
                    System.Diagnostics.Debug.WriteLine("Database.SqlExecNonQuery:" + e.Message);
#endif
                    throw (e);
                }

            }
        }

        public object SqlExecScalar(string sql, SqlTransaction trans = null)
        {
            if (!IsConnectionOpen())
                OpenDbConnection();
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = sql;
                cmd.Connection = _conn;
                if (trans != null) cmd.Transaction = trans;
                object ret = null;
                try
                {
                    ret = cmd.ExecuteScalar();
                }
                catch (Exception e)
                {
#if DEBUG
                    Console.WriteLine("Database.SqlExecWithIdReturn:" + e.Message);
#endif
                    throw (e);
                }
                return ret;
            }
        }

        public SqlTransaction BeginTransaction()
        {
            if (!IsConnectionOpen())
                OpenDbConnection();
            return _conn.BeginTransaction();
        }

        public void DisposeDbConnection()
        {
            try
            {
                _conn.Close();
                _conn.Dispose();
            }
            catch (Exception e) {
#if DEBUG
                Console.WriteLine("Database.DisposeDbConnection:" + e.Message);
#endif
                throw (e);
            }
        }

        public void AddStoredProcedureParameter(string name, SqlDbType dataType, object value, ParameterDirection direction, int size = -1, bool refresh = false)
        {
            if (refresh)
            {
                _parameters.Clear();
                _outParameters.Clear();
            }

            SqlParameter parameter = new SqlParameter(name, dataType);
            if (size > 0) parameter.Size = size;
            parameter.Direction = direction;
            if (direction == ParameterDirection.Input) parameter.Value = value;
            else _outParameters.Add(parameter);
            _parameters.Add(parameter);
        }


        public List<GenNameValue> ExecStoredProcedure(string procedure, SqlTransaction trans = null)
        {
            if (!IsConnectionOpen())
                OpenDbConnection();
            try
            {
                using (SqlCommand cmd = new SqlCommand(procedure, _conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (trans != null)
                        cmd.Transaction = trans;
                    foreach (var pram in _parameters)
                        cmd.Parameters.Add(pram);

                    cmd.ExecuteNonQuery();

                    List<GenNameValue> output = new List<GenNameValue>(); 

                    foreach (var p in _outParameters)
                        output.Add(new GenNameValue { Name = p.ParameterName, Value = cmd.Parameters[p.ParameterName].Value });

                    return output;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Database.GetStoredProcedureCommand:" + e.Message);
                return null;
            }
        }


        private bool IsConnectionOpen()
        {
            if (_conn == null) return false;
            if (!_conn.State.HasFlag(ConnectionState.Open)) return false;
            return true;
        }

        public string GetTableExistsSql(ITable table)
        {
            return "select count(*) cnt from information_schema.tables where table_name = '" + table.Name + "' and table_schema = '" + table.Schema.Name + "'";
        }
        public string GetColumnExistsSql(IColumn column)
        {
            return "select count(*) cnt from information_schema.columns where column_name = '" + column.Name + "' and table_name = '" + column.Owner.Name 
                    + "' and table_schema = '" + column.Owner.Schema.Name + "'";
        }
        public string GetTriggerExistsSql(ITrigger trigger)
        {
            return "select count(*) cnt from sys.triggers where name = '" + trigger.Name + "' and object_schema_name(object_id) = '" + trigger.Owner.Schema.Name + "'";
        }
        public string GetProcedureExistsSql(IStoredProcedure proc)
        {
            return "select count(*) cnt from sys.procedures where name = '" + proc.Name + "' and object_schema_name(object_id) = '" + proc.Schema.Name + "'";
        }
        public string GetKeyExistsSql(ITable table, string keyName, bool isUnique)
        {
            string sql = "select count(*) cnt from information_schema.table_constraints where ";
            if (isUnique)
                sql += "constraint_type = 'UNIQUE' and ";
            sql += "constraint_name = '" + keyName + "' and table_name = '" + table.Name + "' and constraint_schema = '" + table.Schema.Name + "'";
            return sql;
        }
        public string GetIndexExistsSql(IIndex index)
        {
            return "select count(*) cnt from sys.indexes i join sys.tables t on i.object_id = t.object_id join sys.schemas s on s.schema_id = t.schema_id "
                    + "where i.name = '" + index.Name + "' and t.name = '" + index.Owner.Name + "' and t.type_desc = 'USER_TABLE' and s.name = '" + index.Owner.Schema.Name + "'";
        }

        public string GetTableColumnsSql(ITable table)
        {
            return "select column_name, is_nullable, data_type, character_maximum_length from information_schema.columns where table_name = '" + table.Name + "' and table_schema = '" + table.Schema.Name + "'";
        }

        public string GetTableIdentityColumnSql(ITable table)
        {
            return "select name from sys.columns where object_name(object_id) = '" + table.Name + "' and object_schema_name(object_id) = '" + table.Schema.Name + "' and is_identity = 1";
        }
        public string GetTableComputedColumnsSql(ITable table)
        {
            return "select name, columnproperty(object_id, name, 'IsComputed') f from sys.columns where is_computed = 1 and object_name(object_id) = '" + table.Name + "' and object_schema_name(object_id) = '" + table.Schema.Name + "'";
        }
        public string GetAddTableColumnSql(IColumn column)
        {
            string sql = "alter table " + column.Owner.Schema.Name + "." + column.Owner.Name + " add " + column.Name + " ";
            if (!column.IsCalc)
            {
                sql += column.DataType;
                if (column.IsIdentity) sql += " identity(1,1)";
                if (column.IsNullable) sql += " null";
                else sql += " not null";
            }
            else sql += "as (" + column.CalcFormula + ")";

            return sql;
        }
        public string GetDropTableColumnSql(IColumn column)
        {
            return "alter table " + column.Owner.Schema.Name + "." + column.Owner.Name + " drop column " + column.Name + ";";
        }
        public string GetAddUniqueKeySql(IUniqueKey key)
        {
            string sql = "alter table " + key.Owner.Schema.Name + "." + key.Owner.Name + " add constraint " + key.Name + " unique (";
            foreach (var col in key.ColumnNames)
            {
                sql += col + ",";
            }
            sql = sql.Substring(0, sql.Length - 1) + ");";
            return sql;
        }
        public string GetDropKeySql(ITable table, string keyName)
        {
            return "alter table " + table.Schema.Name + "." + table.Name + " drop constraint " + keyName;
        }
        public string GetTableForeignKeyDependentsSql(ITable table)
        {
            return "select object_name(parent_object_id) DependentTable, c.name DependentTableColumn, object_schema_name(parent_object_id) DependentTableSchema , object_name(constraint_object_id) KeyName "
                + "from sys.foreign_key_columns  k join sys.columns c on c.object_id = k.parent_object_id and c.column_id = k.parent_column_id "
                + "where k.referenced_object_id = object_id('" + table.Name + "','U') and object_schema_name(k.referenced_object_id) = '" + table.Schema.Name + "'";
        }
        public string GetAddForeignKeySql(IForeignKey key)
        {
            return "alter table " + key.Owner.Schema.Name + "." + key.Owner.Name + " add constraint " + key.Name + " foreign key ("
                    + key.KeyColumn.Name + ") references " + key.ReferenceTable.Schema.Name + "." + key.ReferenceTable.Name + "(" + key.ReferenceColumn.Name + ")";

        }
        public string GetAddPrimaryKeySql(IPrimaryKey key)
        {
            string sql = "alter table " + key.Owner.Schema.Name + "." + key.Owner.Name + " add constraint " + key.Name + " primary key (";
            foreach (var col in key.ColumnNames)
            {
                sql += col + ",";
            }
            sql = sql.Substring(0, sql.Length - 1) + ");";
            return sql;
        }
        public string GetAddIndexSql(IIndex index)
        {
            string sql = "create ";
            if (index.IsUnique) sql += "unique ";
            if (index.IsClustered) sql += "clustered";
            else sql += "nonclustered";
            sql += " index " + index.Name + " on " + index.Owner.Schema.Name + "." + index.Owner.Name + " (";
            foreach (var col in index.ColumnNames)
            {
                sql += col + ",";
            }
            sql = sql.Substring(0, sql.Length - 1) + ");";
            return sql;
        }
        public string GetDropIndexSql(IIndex index)
        {
            return "drop index " + index.Name + " on " + index.Owner.Schema.Name + "." + index.Owner.Name;
        }
        public string GetCheckConstraintExistsSql(ICheckConstraint constraint)
        {
            return "select count(*) cnt from information_schema.check_constraints where constraint_name = '" + constraint.Name + "' and constraint_schema = '" + constraint.Owner.Schema.Name + "';";
        }
        public string GetAddCheckConstraintSql(ICheckConstraint constraint)
        {
            return "alter table " + constraint.Owner.Schema.Name + "." + constraint.Owner.Name + " add constraint " + constraint.Name + " check (" + constraint.ConstraintText + ");";
        }
        public string GetDropCheckConstraintSql(ICheckConstraint constraint)
        {
            return "alter table " + constraint.Owner.Schema.Name + "." + constraint.Owner.Name + " drop constraint " + constraint.Name;
        }

        public string GetDefaultExistsSql(IDefault theDefault)
        {
            return "select count(*) cnt from sys.default_constraints where name = '" + theDefault.Name + "' and object_name(parent_object_id) = '"
                + theDefault.Owner.Name + "' and schema_name(schema_id) = '" + theDefault.Owner.Schema.Name + "'";
        }
        public string GetAddDefaultExistsSql(IDefault theDefault)
        {
            return "alter table " + theDefault.Owner.Schema.Name + "." + theDefault.Owner.Name + " add constraint " + theDefault.Name
                + " default " + theDefault.DefaultValue.ToString() + " for " + theDefault.ColumnWithDefault.Name;
        }
        public string GetDropDefaultExistsSql(IDefault theDefault)
        {
            return "alter table " + theDefault.Owner.Schema.Name + "." + theDefault.Owner.Name + " drop constraint " + theDefault.Name;
        }

        public string GetAddIntoQueuesSql(string columnList, string inputList, string schema)
        {
            string sql = "declare @p table(col1 int); "
                    + "insert into " + schema + ".Queues (" + columnList + ") output INSERTED.id "
                    + "into @p values (" + inputList + "); "
                    + "select * from @p;";
            return sql;
        }

        public string GetFalseSQL()
        {
            return "0";
        }

        public string GetTrueSQL()
        {
            return "1";
        }

        public string GetBooleanType()
        {
            return "bit";
        }
    }


}
