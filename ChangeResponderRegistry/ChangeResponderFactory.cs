﻿using System;
using System.Collections.Generic;
using System.Linq;
using InterfaceChangeResponder;
using Com.VeritasTS.DatabaseUtilityInterfaces;
using Com.VeritasTS.Queues;
using TestCustomerLib;


namespace ChangeResponderRegistry
{
    public class ChangeResponderFactory
    {
        private IDatabase _database;
        private string _schema;
        List<KeyValuePair<string, IChangeResponder>> _changeResponders = null;

        public List<KeyValuePair<string, IChangeResponder>> ChangeResponders
        {
            get { return _changeResponders; }
        }
        public ChangeResponderFactory(IDatabase db, string schema)
        {
            IQueue queue = new aQueue();
            queue = null;
            _database = db;
            _schema = schema;
            _changeResponders = GetAllQueue2ndTiers();
        }

        private List<KeyValuePair<string, IChangeResponder>> GetAllQueue2ndTiers()
        {
            string sql = "select queue1Tier, changeResponder, 1 isQueue from " + _schema + ".queues2Tier "
                + " union select  queue1Tier, changeResponder, 0 from " + _schema + ".custChangeResponders";
            var regists = _database.SqlExecuteReader(sql);

            var list = new List<KeyValuePair<string, IChangeResponder>>();

            foreach (var r in regists)
            {
                string tier1Name = r[0];
                string tier2Name = r[1];
                var tier2 = (IChangeResponder)CommUtils.GetObjectByObjectName(tier2Name);
                if (tier2 != null)
                    list.Add(new KeyValuePair<string, IChangeResponder>(tier1Name, tier2));
            }
            return list;
        }

        public List<IChangeResponder> GetQueue2ndTiers(string tier1Queue)
        {
            var list = _changeResponders.Where(p => p.Key == tier1Queue);
            if (list.Count() == 0) return null;

            var queues = new List<IChangeResponder>();
            foreach (var p in list)
                queues.Add(p.Value);

            return queues;
        }

        public void Dispose()
        {
            _changeResponders.RemoveAll(p => !p.Key.Equals(""));
            _changeResponders = null;
        }

    }

    class aQueue : QueueBase
    {
        public aQueue ()
        {
            Name = "aQueue";
            TestCustClass c = new TestCustClass();
            c = null;
        }
    }
}
