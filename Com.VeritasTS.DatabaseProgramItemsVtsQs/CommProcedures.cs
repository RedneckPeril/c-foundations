using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;

public partial class StoredProcedures
{
    static readonly string _schema = "vtsQs.";
    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void QiPing(int qi, string qiTable = "", int queueID = -1)
    {
        if (qiTable.Length == 0 && queueID == -1)
            throw (new Exception("not enough info to ping the queue"));

        //using (SqlConnection conn = new SqlConnection(@"Data Source=SERVER02\SQLEXPRESS;Initial Catalog=Mazi_test;Integrated Security=True"))
        using (SqlConnection conn = new SqlConnection("context connection=true"))
        {
            conn.Open();
            if (qiTable.Length == 0)
                qiTable = GetQueueInfo<string>(conn, "select qisVableName from " + _schema + "queues where id = " + queueID);
            string sql = "update " + qiTable + " set lastHeardFrom = getdate() where id = " + qi.ToString();
            ExecNonQuery(conn, sql);
        }
    }

    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void LogFromQueue(int jobNum, int qi, string msg, string logTable = "", int queueID = -1)
    {
        if (logTable.Length == 0 && queueID == -1)
            throw (new Exception("not enough info to log the queue"));

        using (SqlConnection conn = new SqlConnection("context connection=true"))
        {
            conn.Open();
            if (logTable.Length == 0)
                logTable = GetQueueInfo<string>(conn, "select logName from " + _schema + "queues where id = " + queueID);

            string sql = "insert into " + logTable + " (qi, jobNum, msg, dtm) values (" + qi.ToString() + ", "
                + jobNum.ToString() + ",'" + msg + "', getdate())";
            ExecNonQuery(conn, sql);
        }
    }

    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void killQI(int qi, string qiTable = "", int queueID = -1)
    {
        if (qiTable.Length == 0 && queueID == -1)
            throw (new Exception("not enough info to ping the queue"));

        using (SqlConnection conn = new SqlConnection("context connection=true"))
        {
            conn.Open();
            if (qiTable.Length == 0)
                qiTable = GetQueueInfo<string>(conn, "select qisVableName from " + _schema + "queues where id = " + queueID);
            string sql = "delete from " + qiTable + " where id = " + qi.ToString();
            ExecNonQuery(conn, sql);
        }
    }


    public static void ExecNonQuery(SqlConnection conn, string sql)
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            try
            {
                cmd.CommandText = sql;
                cmd.Connection = conn;
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
#if DEBUG
                Console.WriteLine("ExecNonQuery: sql: " + sql);
                System.Diagnostics.Debug.WriteLine("ExecNonQuery: sql: " + sql);
#endif
                throw (e);
            }
        }
    }

    public static T GetQueueInfo<T>(SqlConnection conn, string sql)
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            T ret = default(T);
            try
            {
                cmd.CommandText = sql;
                cmd.Connection = conn;
                using (SqlDataReader r = cmd.ExecuteReader())
                {
                    if (r.Read())
                        ret = (T)r.GetValue(0);
                }
            }
            catch (Exception e)
            {
#if DEBUG
                Console.WriteLine("GetQueueInfo: sql: " + sql);
                System.Diagnostics.Debug.WriteLine("GetQueueInfo: sql: " + sql);
#endif
                throw (e);
            }
            return ret;
        }
    }

}
