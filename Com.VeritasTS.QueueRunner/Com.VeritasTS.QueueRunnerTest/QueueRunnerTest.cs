﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Com.VeritasTS.QueueRunner;
using Com.VeritasTS.Queues;
using Com.VeritasTS.DatabaseComm;
using ChangeResponderRegistry;



namespace Com.VeritasTS.QueueRunnerTest
{
    [TestFixture]
    public class QueueRunnerTest
    {
        [Test]
        public void GetQueueByQueueNameTest()
        {
            IQueue queue = (IQueue)CommUtils.GetObjectByObjectName("testQueueTableQ");
            Assert.True(queue != null);
        }
        [Test]
        public void GetChangeResponderFactoryTest()
        {
            Database db = new Database(new Server(new Vendor { Name = "SQLServer" }));
            db.ConnectionString = @"Data Source=SERVER02\SQLEXPRESS;Initial Catalog=Mazi_test;Integrated Security=True";
            ChangeResponderFactory ChangeResponderFactory = new ChangeResponderFactory(db, "vtsQs");
            db.DisposeDbConnection();
            Assert.True(ChangeResponderFactory.ChangeResponders.Count > 0);
            ChangeResponderFactory.Dispose();
            ChangeResponderFactory = null;
        }
        [Test]
        public void GetQueue2ndTiersTest()
        {
            Database db = new Database(new Server(new Vendor { Name = "SQLServer" }));
            db.ConnectionString = @"Data Source=SERVER02\SQLEXPRESS;Initial Catalog=Mazi_test;Integrated Security=True";
            ChangeResponderFactory ChangeResponderFactory = new ChangeResponderFactory(db, "vtsQs");
            db.DisposeDbConnection();
            string queueStr = "testQueueTableQ";
            IQueue queue = (IQueue)CommUtils.GetObjectByObjectName(queueStr);
            queue.QueuesToMake = ChangeResponderFactory.GetQueue2ndTiers(queueStr);
            Assert.True(queue.QueuesToMake.Count > 0);
            ChangeResponderFactory.Dispose();
            ChangeResponderFactory = null;
        }
        [Test]
        public void NeedToProcessTest()
        {
            QueueRunnerProcessor runner = new QueueRunnerProcessor("T1");
            runner.ConnectionString = @"Data Source=SERVER02\SQLEXPRESS;Initial Catalog=Mazi_test;Integrated Security=True";
            runner.OpenDB();
            bool b = runner.NeedToProcess("1", "dbo.testQueueTableJobs", "dbo.testQueueTableQIs", true, 500, 1);
            Assert.True(b);
            runner.CloseDB();
        }
        [Test]
        public void GetWaitingQueueTest()
        {
            QueueRunnerProcessor runner = new QueueRunnerProcessor("T4");
            runner.ConnectionString = @"Data Source=SERVER02\SQLEXPRESS;Initial Catalog=Mazi_test;Integrated Security=True";
            runner.nSingleQueueMax = 3;
            runner.OpenDB();
            runner.GetWaitingQueue();
            runner.CloseDB();
        }

    }
}
