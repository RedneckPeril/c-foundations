using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using NamedPipesModule;

public partial class StoredProcedures
{
    // if get publish assembly changed error, do the following first in sql server
    //drop procedure vtsQs.SendQueueJobsReadyMsg
    //DROP ASSEMBLY[QueueRequestSender]
    //DROP ASSEMBLY[NamedPipesModule]

    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void SendQueueJobsReadyMsg(string queueRunnerServer)
    {
//        string queueRunnerServer = "";
//        using (SqlConnection conn = new SqlConnection("context connection=true"))
//        {
//            conn.Open();
//            string sql = "select " + schemaName + ".queueRunnerServer() as queueRunnerServer";
//            using (SqlCommand cmd = new SqlCommand(sql, conn))
//            {
//                using (SqlDataReader r = cmd.ExecuteReader())
//                {
//                    if (r.Read()) queueRunnerServer = r[0].ToString();
//                }
//            }
//        }
//#if !DEBUG
//            if (queueRunnerServer.Length == 0)
//                sp.Send("didn't get queueRunnerServer");
//#endif
//        if (queueRunnerServer.Length == 0) return;

        IInterProcessConnection clientConnection = null;
        string now = DateTime.Now.ToShortTimeString();
        try
        {
            clientConnection = new ClientPipeConnection("QueueRunnerPipe", queueRunnerServer);
            clientConnection.Connect();
            clientConnection.Write(now + ": new jobs ready");
            clientConnection.Close();
        }
        catch (Exception ex)
        {
            clientConnection.Dispose();
#if !DEBUG
            SqlPipe sp = SqlContext.Pipe;
            sp.Send("SendQueueJobsReadyMsg: " + ex.Message);
#endif
        }
    }
}
