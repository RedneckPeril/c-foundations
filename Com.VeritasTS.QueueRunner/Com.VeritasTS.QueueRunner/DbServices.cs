﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using Com.VeritasTS.DatabaseComm;

namespace Com.VeritasTS.QueueRunner
{
    class DbServices : Database
    {
        //private string server;
        //private string db;
        //private string _connectionString;
        //private SqlConnection _conn;
        //private Statement statement;
       // private List<SqlParameter> _parameters = new List<SqlParameter>();
       // private List<SqlParameter> _outParameters = new List<SqlParameter>();

        public DbServices(IServer server) : base(server) { }

        //public DbServices(string connectionString)
        //{
        //    _connectionString = connectionString;
        //    //server = sServer;
        //    //db = sDB;
        //    //_connectionString = "jdbc:sqlserver://" + server + ";databaseName=" + db + ";integratedSecurity=true";
        //}

        //public bool OpenDB()
        //{
        //    _conn = new SqlConnection(_connectionString);
        //    try
        //    {
        //        _conn.Open();
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine("Database.OpenDbConnection:" + e.Message);
        //        System.Diagnostics.Debug.WriteLine("Database.OpenDbConnection:" + e.Message);
        //        return false;
        //    }
        //    return true;
        //}

        //public bool dbConnectionActive()
        //{
        //    if (_conn == null) return false;
        //    if (!_conn.State.HasFlag(ConnectionState.Open)) return false;
        //    return true;
        //}


        //public object GetSingleValue(string sql, SqlTransaction trans = null)
        //{
        //    if (!dbConnectionActive())
        //        OpenDB();
        //    object obj = null;

        //    using (SqlCommand cmd = new SqlCommand(sql, _conn))
        //    {
        //        if (trans != null) cmd.Transaction = trans;
        //        using (SqlDataReader r = cmd.ExecuteReader())
        //        {
        //            if (r.Read()) obj = r[0];
        //        }
        //    }
        //    return obj;
        //}


        //public int getIntFromDB(string sql)
        //{
        //    int iRet = -1;
        //    try
        //    {
        //        ResultSet r = statement.executeQuery(sql);
        //        if (r.next())
        //            iRet = r.getInt(1);
        //        r.close();
        //    }
        //    catch (SQLException es)
        //    {
        //        System.out.println(es.getMessage());
        //    }
        //    return iRet;
        //}

        //public string getstringFromDB(string sql)
        //{
        //    string sRet = "";
        //    try
        //    {
        //        ResultSet r = statement.executeQuery(sql);
        //        if (r.next())
        //            sRet = r.getstring(1);
        //        r.close();
        //    }
        //    catch (SQLException es)
        //    {
        //        System.out.println(es.getMessage());
        //    }
        //    return sRet;
        //}



        //public bool updateDB(string sql, SqlTransaction trans = null)
        //{
        //    if (!IsConnectionOpen())
        //        OpenDbConnection();
        //    using (SqlCommand cmd = new SqlCommand())
        //    {
        //        cmd.CommandText = sql;
        //        cmd.Connection = _conn;
        //        if (trans != null) cmd.Transaction = trans;
        //        try
        //        {
        //            cmd.ExecuteNonQuery();
        //        }
        //        catch (Exception e)
        //        {
        //            Console.WriteLine("DbServices.updateDB:" + e.Message);
        //            System.Diagnostics.Debug.WriteLine("DbServices.updateDB:" + e.Message);
        //            return false;
        //        }
        //    }
        //    return true;
        //}

        public List<string[]> getTableData(string sql, SqlTransaction trans = null)
        {
            if (!IsConnectionOpen())
                OpenDbConnection();
            List<string[]> data = new List<string[]>();

            using (SqlCommand cmd = new SqlCommand(sql, _conn))
            {
                if (trans != null) cmd.Transaction = trans;
                try
                {
                    int fieldCount = 0;
                    SqlDataReader r = cmd.ExecuteReader();
                    if (r.Read())
                    {
                        fieldCount = r.FieldCount;
                        do
                        {
                            string[] row = new string[fieldCount];
                            for (int i = 0; i < fieldCount; i++)
                            {
                                row[i] = dataStr(r.GetDataTypeName(i), r, i);
                            }
                            data.Add(row);
                        } while (r.Read());
                    }
                    r.Close();
                    r = null;
                    return data;
                }
                catch (Exception e)
                {
                    Console.WriteLine("DbServices.getTableData:" + e.Message);
                    System.Diagnostics.Debug.WriteLine("DbServices.getTableData:" + e.Message);
                    throw (e);
                }
            }

        }

        private string dataStr(string dataType, SqlDataReader r, int iCol, bool csv = false)
        {
            if (DBNull.Value.Equals(r.GetValue(iCol)))
                return "";

            string sData = "";
            try
            {
                if (dataType.Equals("datetime"))
                {
                    DateTime d = Convert.ToDateTime(r.GetValue(iCol));
                    return d.ToShortDateString() + " " + d.ToShortTimeString();
                }
                if (dataType.Equals("date"))
                    return Convert.ToDateTime(r.GetValue(iCol)).ToShortDateString();
                if (dataType.Equals("time"))
                    return Convert.ToDateTime(r.GetValue(iCol)).ToLongTimeString();
                if (dataType.Equals("decimal"))
                    return Decimal.Round(Convert.ToDecimal(r.GetValue(iCol)), 6).ToString();

                sData = r.GetValue(iCol).ToString();
                //if (dataType.Equals("bit")) sData = r.GetValue(iCol).ToString();
                //if (dataType.Equals("nvarchar")) sData = r.GetValue(iCol).ToString();
                //if (dataType.Equals("varchar")) sData = r.GetValue(iCol).ToString();
                //if (dataType.Equals("int")) sData = r.GetValue(iCol).ToString();
                //if (dataType.Equals("float")) sData = r.GetValue(iCol).ToString();
                if (csv && sData.IndexOf(",") >= 0) sData = "\"" + sData + "\"";
            }
            catch (Exception e)
            {
                Console.WriteLine("DbServices.dataStr:" + e.Message);
                System.Diagnostics.Debug.WriteLine("DbServices.dataStr:" + e.Message);
            }
            return sData;
        }


        public bool runLinearDealComponentsQueueFromExcel()
        {
            if (!IsConnectionOpen())
                OpenDbConnection();
            try
            {
                float acctMints = 2;
               // int iterations = 10;
                using (SqlCommand cmd = new SqlCommand("dbo.runLinearDealComponentsQueueFromExcel", _conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@acceptableDelayInMinutes", DbType = DbType.Int32, Value = acctMints, Direction = ParameterDirection.Input });
                    //cstmt.setInt("@iterations", iterations);

                    cmd.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("DbServices.runLinearDealComponentsQueueFromExcel:" + e.Message);
                return false;
            }
        }

        //public bool runQueueProcWOReturn(string sProcdure, float acctMints)
        //{
        //    try
        //    {
        //        updateDB("while @@TRANCOUNT > 0 commit transaction;");
        //        //float acctMints = 8;
        //        int iterations = 1;
        //        CallableStatement cstmt = _conn.prepareCall("{call " + sProcdure + "(?, ?)}");
        //        cstmt.setFloat("acceptableDelayInMinutes", acctMints);
        //        cstmt.setInt("iterations", iterations);
        //        cstmt.execute();
        //        updateDB("while @@TRANCOUNT > 0 commit transaction;");
        //    }
        //    catch (SQLException es)
        //    {
        //        string se = es.getMessage();
        //        System.out.println(se);
        //        updateDB("while @@TRANCOUNT > 0 commit transaction;");
        //        return false;
        //    }
        //    return true;
        //}

        //public void AddStoredProcedureParameter(string name, SqlDbType dataType, object value, ParameterDirection direction, int size = -1, bool refresh = false)
        //{
        //    if (refresh)
        //    {
        //        _parameters.Clear();
        //        _outParameters.Clear();
        //    }

        //    SqlParameter parameter = new SqlParameter(name, dataType);
        //    if (size > 0) parameter.Size = size;
        //    parameter.Direction = direction;
        //    if (direction == ParameterDirection.Input) parameter.Value = value;
        //    else _outParameters.Add(parameter);
        //    _parameters.Add(parameter);
        //}


        public object runSQLProcedure(string sProc, SqlTransaction trans = null)
        {
            if (!IsConnectionOpen())
                OpenDbConnection();
            try
            {
                using (SqlCommand cmd = new SqlCommand(sProc, _conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (trans != null)
                        cmd.Transaction = trans;
                    foreach (var pram in _parameters)
                        cmd.Parameters.Add(pram);

                    cmd.ExecuteNonQuery();

                    List<GenNameValue> output = new List<GenNameValue>();

                    foreach (var p in _outParameters)
                        output.Add(new GenNameValue { Name = p.ParameterName, Value = cmd.Parameters[p.ParameterName].Value });

                    return output;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("DbServices.runSQLProcedure:" + e.Message);
                return null;
            }

        }

        //public int acquireNewQI(int iQueueID, float acctMints)
        //{
        //    int iQI = -1;
        //    try
        //    {
        //        int iterations = 1;
        //        CallableStatement cstmt = _conn.prepareCall("{call vtsQs.acquireNewQI(?, ?, ?, ?)}");
        //        cstmt.setInt("queueID", iQueueID);
        //        cstmt.setFloat("acceptableDelayInMinutes", acctMints);
        //        cstmt.setInt("iterations", iterations);
        //        cstmt.registerOutParameter("newQI", java.sql.Types.INTEGER);
        //        cstmt.execute();
        //        iQI = cstmt.getInt("newQI");
        //    }
        //    catch (SQLException es)
        //    {
        //        string se = es.getMessage();
        //        System.out.println(se);
        //        updateDB("while @@TRANCOUNT > 0 commit transaction;");
        //    }
        //    return iQI;
        //}

        //public int claimNextJob(int iQueueID, int iQi)
        //{
        //    int iJob = -1;
        //    try
        //    {
        //        CallableStatement cstmt = _conn.prepareCall("{call vtsQs.claimNextJob(?, ?, ?)}");
        //        cstmt.setInt("queueID", iQueueID);
        //        cstmt.setInt("qi", iQi);
        //        cstmt.registerOutParameter("jobNum", java.sql.Types.INTEGER);
        //        cstmt.execute();
        //        iJob = cstmt.getInt("jobNum");
        //    }
        //    catch (SQLException es)
        //    {
        //        string se = es.getMessage();
        //        System.out.println(se);
        //        updateDB("while @@TRANCOUNT > 0 commit transaction;");
        //    }
        //    return iJob;
        //}

        public bool logFromQueue(int iQueueID, int iJob, int iQi, string sMsg, string logTable = "")
        {
            if (!IsConnectionOpen())
                OpenDbConnection();
            try
            {
                using (SqlCommand cmd = new SqlCommand("vtsQs.logFromQueue", _conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@queueID", SqlDbType = SqlDbType.Int, Value = iQueueID, Direction = ParameterDirection.Input });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@jobNum", SqlDbType = SqlDbType.Int, Value = iJob, Direction = ParameterDirection.Input });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@qi", SqlDbType = SqlDbType.Int, Value = iQi, Direction = ParameterDirection.Input });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@msg", SqlDbType = SqlDbType.NVarChar, Value = sMsg, Direction = ParameterDirection.Input });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@logTable", SqlDbType = SqlDbType.NVarChar, Value = logTable, Direction = ParameterDirection.Input });

                    cmd.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("DbServices.logFromQueue:" + e.Message);
                return false;
            }
        }

        public void qiPing(string sProc, int iQueueID, int iQi, string qiTable = "")
        {
            if (!IsConnectionOpen())
                OpenDbConnection();
            try
            {
                using (SqlCommand cmd = new SqlCommand("vtsQs.QiPing", _conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@queueID", SqlDbType = SqlDbType.Int, Value = iQueueID, Direction = ParameterDirection.Input });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@qi", SqlDbType = SqlDbType.Int, Value = iQi, Direction = ParameterDirection.Input });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@qiTable", SqlDbType = SqlDbType.NVarChar, Value = qiTable, Direction = ParameterDirection.Input });
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("DbServices.qiPing:" + e.Message);
            }

        }
    }
    public class GenNameValue
    {
        public string Name;
        public object Value;
    }

}
