﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.VeritasTS.Queues;
using Com.VeritasTS.DatabaseComm;

namespace Com.VeritasTS.QueueRunner
{
    public class QueueRunnerProcessor
    {
        public string ConnectionString { get; set; }
        public string sThreadBase { get; set; }
        public int nSingleQueueMax { get; set; }

        private DbServices _db = new DbServices(new Server(new Vendor { Name = "SQLServer" }));
        private bool _bRun = true;

        private string _sCallThread;
        private string _sQueue;
        private string _sProc;
        private string _sJobsTable;
        private string _sQITable;
        private string _sQueueID;
        private float _fRunMints;
        private string _sRunFrom;
        private bool _bOwner;

        public QueueRunnerProcessor(string sThread)
        {
            _sCallThread = sThread;
            _sProc = "";
            _sQueue = "";
            _bOwner = false;
            _db.Name = "QueueRunnerDb" + sThread;
        }

        public bool GetContinueProcess() { return _bRun;}

        public void Process()
        {
            //Console.WriteLine("Thread " + _sCallThread + ": enter Process"); 
            if (!_db.IsConnectionOpen())
            {
                Console.WriteLine(_sCallThread + ": db connection is inavtive");
                CloseDB();
                _db = null;
                _db = new DbServices(new Server(new Vendor { Name = "SQLServer" })) {Name = "QueueRunnerDb" + _sCallThread };
                OpenDB();
                DataClean();
            }
            GetWaitingQueue();
            ProcessQueue();
            SetStopProcess();
            Console.WriteLine(_sCallThread + ": GetContinueProcess = " + _bRun.ToString());
            if (!_bRun)
            {
                _sQueue = "";
                Console.WriteLine(_sCallThread + ": _sQueue = '" + _sQueue + "', DataClean");
                DataClean();
            }
        }

        public void GetWaitingQueue()
        {
            //Console.WriteLine("Thread " + _sCallThread + ": enter GetWaitingQueue");
            if (_sQueue.Length > 0) return;
            _bRun = false;

            string sCk = "ck " + _sCallThread;
            string sql = "update p set p.threadRun = '" + sCk
                    + "' from " + frmQueueRunner.SchemaName + ".queueProcInfo p join " + frmQueueRunner.SchemaName + ".queues q on p.queueid = q.id where p.threadRun is null and rtrim(ltrim(coalesce(p.helper,''))) = '' and q.isactive = 1 and coalesce(p.checkHelper,0) = 0";
            if (!_db.SqlExecNonQuery(sql, byQueueRunner: true)) return;
            Console.WriteLine(_sCallThread + ": updated for checking");

            sql = "select i.name, q.jobsVableName, q.acceptableDelayInMinutes, q.id, q.qisvablename, q.targetJobSize, coalesce(q.maxOwnerCount,1000) maxOwnerCount "
                    + "from " + frmQueueRunner.SchemaName + ".queueProcInfo i join " + frmQueueRunner.SchemaName + ".queues q on q.id = i.queueID where i.threadRun = '" + sCk + "' order by q.rawPriority";
            var data = _db.getTableData(sql);
            int n = data.Count;
            Console.WriteLine(_sCallThread + ": selected available queues. n = " + n);

            if (n > 0)
            {
                foreach (var row in data)
                {
                    string sTable = row[1];
                    string sQID = row[3];
                    string sQI = row[4];
                    int nJobSize = Convert.ToInt32(row[5]);
                    int nQThs = Convert.ToInt32(row[6]);

                    if (NeedToProcess(sQID, sTable, sQI, true, nJobSize, nQThs))
                    {
                        _sQueue = row[0];
                        _fRunMints = Convert.ToSingle(row[2]);
                        _sJobsTable = sTable;
                        _sQITable = sQI;
                        _sQueueID = sQID;
                        sql = "update " + frmQueueRunner.SchemaName + ".queueProcInfo set threadRun = '" + _sCallThread + "' where name = '" + _sQueue + "'";
                        if (!_db.SqlExecNonQuery(sql, byQueueRunner: true)) return;
                        _bOwner = true;
                        _bRun = true;
                        Console.WriteLine(_sCallThread + ": updated for the chosen queue " + _sQueue);

                        break;
                    }

                }
                sql = "update " + frmQueueRunner.SchemaName + ".queueProcInfo set threadRun = null where threadRun = '" + sCk + "'";
                _db.SqlExecNonQuery(sql, byQueueRunner: true);
                Console.WriteLine(_sCallThread + ": updated for relasing other available queues");
            }
            data.Clear();


            // helper
            if (!_bOwner)
            {
                sql = "select i.name, q.jobsVableName, q.acceptableDelayInMinutes, q.id, q.qisvablename, q.targetJobSize, coalesce(q.maxOwnerCount,1000) maxOwnerCount "
                        + "from " + frmQueueRunner.SchemaName + ".queueProcInfo i join " + frmQueueRunner.SchemaName + ".queues q on q.name = i.name where threadRun like '"
                        + sThreadBase + "%' and threadRun not like 'ck %' and threadRun != '" + _sCallThread + "' and coalesce(checkHelper,0) = 0 order by q.rawPriority";
                //sql = "select queue, jobTable, runProc, acceptableDelayInMinutes from " + frmQueueRunner.SchemaName + ".queueProcInfo where threadRun like '" + sThreadBase + "%' and threadRun != '" + sCallThread + "'";
                data = _db.getTableData(sql);
                n = data.Count;
                foreach (var row in data)
                {
                    _sQueue = row[0];
                    string sTable = row[1];
                    string sQID = row[3];
                    string sQI = row[4];
                    int nJobSize = Convert.ToInt32(row[5]);
                    int nQThs = Convert.ToInt32(row[6]);

                    if (NeedToProcess(sQID, sTable, sQI, false, nJobSize, nQThs))
                    {
                        _fRunMints = Convert.ToSingle(row[2]);
                        _sJobsTable = sTable;
                        _sQITable = sQI;
                        _sQueueID = sQID;
                        sql = "update " + frmQueueRunner.SchemaName + ".queueProcInfo set helper = coalesce(helper,'') + '" + _sCallThread + ",', checkHelper = 0 where name = '" + _sQueue + "'";
                        if (!_db.SqlExecNonQuery(sql, byQueueRunner: true)) return;
                        _bRun = true;
                        Console.WriteLine(_sCallThread + ": updated for help proc " + _sProc);
                        break;
                    }
                    else
                    {
                        sql = "update " + frmQueueRunner.SchemaName + ".queueProcInfo set checkHelper = 0 where name = '" + _sQueue + "'";
                        if (!_db.SqlExecNonQuery(sql, byQueueRunner: true)) return;
                    }
                }
                data.Clear();
            }
        }

        private void ProcessQueue ()
        {
            //Console.WriteLine(_sCallThread + ": enter ProcessQueue");
            if (!_bRun) return;

            IQueue queue = (IQueue)CommUtils.GetObjectByObjectName(_sQueue);

            if (queue != null)
            {
                //Console.WriteLine(_sCallThread + ": queue: " + queue.Name );
                //Console.WriteLine(_sCallThread + ": ChangeResponderFactory: " + frmQueueRunner.ChangeResponderFactory.ToString());
                queue.QueuesToMake = frmQueueRunner.ChangeResponderFactory.GetQueue2ndTiers(_sQueue);
                Console.WriteLine(_sCallThread + ": start to run " + _sQueue + " --- " + GetNowTimestring());
                queue.Database = _db;
                queue.ProcessJob();
                Console.WriteLine(_sCallThread + ": finished to run " + _sQueue + " --- " + GetNowTimestring());
            }
            else
                Console.WriteLine(_sCallThread + ": can not get queue " + _sQueue + " -- - " + GetNowTimestring()); ;

            if (_bOwner)
            {
                if (CkJobsTableAfterProcess() == 0)
                {
                    //_sProc = "";
                    DataClean();
                    _sQueue = "";
                    _bOwner = false;
                }
            }
            else
            {
                //_sProc = "";
                _sQueue = "";
                DataClean();
            }
            Console.WriteLine(_sCallThread + ": updated queue " + _sQueue + " to finish");
        }

        //public IQueue GetQueueByQueueName(string qName)
        //{
        //    Type type = Type.GetType(qName);
        //    if (type == null)
        //    {
        //        foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
        //        {
        //            string aName = a.FullName.Substring(0, a.FullName.IndexOf(","));
        //            type = a.GetType(aName + "." + qName);
        //            if (type != null)
        //                break;
        //        }
        //    }
        //    if (type != null)
        //        return (IQueue)Activator.CreateInstance(type);
        //    else
        //        return null;
        //}

        private void runProcedure()
        {
            string sTime = GetNowTimestring();
            Console.WriteLine(_sCallThread + ": start to run " + _sProc + " --- " + sTime);
            if (_sRunFrom.Equals("SQL"))
            {
                //!!!!!!!!!!!!
                //run queue.Process
                //!!!!!!!!!!!!
                //if (!db.runQueueProcWOReturn(sProc, fRunMints))
                //    Console.WriteLine(sCallThread + ": queue " + sProc + " failed");
            }
            else
            {
                Console.WriteLine(_sCallThread + ": we don't have non-SQL yet");
                // create doQueueJob based obj and run from there
                // the queues.primaryMethod will be the doQueueJob based class name
                // like the following:
                //!DoQueueJob doJobs = null;
                //if (sProc.Equals("doBestavailable")) doJobs = new doBestavailable(sQueueID);
                //if (sProc.Equals("dbo.runRateScheduleCurveFactsQueueFromExcel")) doJobs = new doRateScheduleCurveFacts(Integer.valueOf(sQueueID), db);
                //!if (!doJobs.processJobs()) Console.WriteLine(_sCallThread + ": queue " + _sProc + " failed");
            }
            sTime = GetNowTimestring();
            Console.WriteLine(_sCallThread + ": finished to run " + _sProc + " --- " + sTime);

            //string sql;
            if (_bOwner)
            {
                if (CkJobsTableAfterProcess() == 0)
                {
                    _sProc = "";
                    DataClean();
                    _sQueue = "";
                    _bOwner = false;
                }
            }
            else
            {
                _sProc = "";
                _sQueue = "";
                DataClean();
            }
            Console.WriteLine(_sCallThread + ": updated queue " + _sProc + " to finish");
        }

        public int CkJobsTableAfterProcess()
        {
            string sql = "select count(*) cnt from " + frmQueueRunner.SchemaName + ".queueProcInfo i join " + frmQueueRunner.SchemaName + ".queues q on q.id = i.queueID where i.name = '" + _sQueue + "' and q.isActive = 1";
            if ((int)_db.SqlGetSingleValue(sql) > 0)
            {
                int n = 0;
                if (NeedToProcess(_sQueueID, _sJobsTable, _sQITable, true, 1, 1000))
                    n = 1;
                //sql = "select count(*) cnt from (select top 1 *  from " + sJobsTable + " where status in ('U', 'P', 'E') and owner is null) t";
                //int n = db.getIntFromDB(sql);
                Console.WriteLine(_sCallThread + ": " + _sQueue + ", ckJobsTableAfterProcess = " + n);
                return n;
            }
            else return 0;
        }

        private string GetNowTimestring()
        {
            return DateTime.Now.ToLongTimeString();
        }


        public void DataClean()
        {
            string sql = "update " + frmQueueRunner.SchemaName + ".queueProcInfo set threadRun = null where threadRun in ('" + _sCallThread + "', 'ck " + _sCallThread + "')";
            if (_sQueue.Length == 0)
                if (!_db.SqlExecNonQuery(sql, byQueueRunner: true))
                    Console.WriteLine(_sCallThread + ": can't update queueProcInfo after ProcessQueue");

            sql = "update " + frmQueueRunner.SchemaName + ".queueProcInfo set helper = replace(helper, '" + _sCallThread + ",','') where charindex('" + _sCallThread + "', helper) > 0";
            if (!_db.SqlExecNonQuery(sql, byQueueRunner: true))
                Console.WriteLine(_sCallThread + ": can't update queueProcInfo after ProcessQueue");
        }

        public bool NeedToProcess(string sQueueID, string sJTable, string sQiTable, bool bMain, int nJobSize, int nMaxThreads)
        {
            //Console.WriteLine(_sCallThread + ": enter  NeedToProcess");
            string sql;
            if (!bMain)
            {
                sql = "select count(*) cnt from " + frmQueueRunner.SchemaName + ".queueProcInfo where name = '" + _sQueue + "' and coalesce(checkHelper, 0) = 1";
                //Console.WriteLine(_sCallThread + ": sql = " + sql);
                if ((int)_db.SqlGetSingleValue(sql) > 0) return false;
                sql = "update " + frmQueueRunner.SchemaName + ".queueProcInfo set checkHelper = 1 where name = '" + _sQueue + "'";
                //Console.WriteLine(_sCallThread + ": sql = " + sql);
                if (!_db.SqlExecNonQuery(sql, byQueueRunner: true))
                {
                    Console.WriteLine(_sCallThread + ": can't update queueProcInfo for checkHelper");
                    return false;
                }

                sql = "select len(threads) - len(replace(threads,',','')) cnt from ( select "
                        + " coalesce(case when threadRun like 'ck %' then null else threadRun end + ',', '') + coalesce(helper,'') threads from " + frmQueueRunner.SchemaName + ".queueprocinfo where name = '"
                        + _sQueue + "') t";
                int n = (int)_db.SqlGetSingleValue(sql);
                Console.WriteLine(_sCallThread + ": threads running for " + _sQueue + ": " + n.ToString());
                if (n >= nSingleQueueMax || n >= nMaxThreads) return false;
            }

            sql = "select count(*) cnt from " + sJTable + " where status in ('U', 'P') and owner is null";
           // Console.WriteLine(_sCallThread + ": sql = " + sql);
            int cnt = (int)_db.SqlGetSingleValue(sql);
            if (cnt > 0)
            {
                sql = "select count(*) cnt from (select top 1 j.owner from " + sJTable + " j join " + sQiTable
                        + " i on j.owner = i.id where j.status = 'P' and j.owner is not null and j.priorStatus = 'E' and dateadd(minute, i.acceptableDelayInMinutes, i.lastHeardFrom) > getdate()) t";
               // Console.WriteLine(_sCallThread + ": sql = " + sql);
                int n = (int)_db.SqlGetSingleValue(sql);
                if (n == 0)
                {
                    if (!bMain)
                    {
                        Console.WriteLine(_sCallThread + ": cnt/ nJobSize = " + ((float)cnt / nJobSize).ToString());
                        if ((float)cnt / nJobSize >= 1.0) return true;
                        else return false;
                    }
                    else return true;
                }
                else return false;
            }

            sql = "select count(*) cnt from (select top 1 id from " + sQiTable + " where queueID = " + sQueueID + " and dateadd(minute, acceptableDelayInMinutes, lastHeardFrom) < getdate()) t";
           // Console.WriteLine(_sCallThread + ": sql = " + sql);
            cnt = (int)_db.SqlGetSingleValue(sql);
            if (cnt > 0)
            {
                sql = "select count(*) cnt from (select top 1 j.owner from " + sJTable + " j join " + sQiTable
                        + " i on j.owner = i.id where j.status = 'P' and j.owner is not null and j.priorStatus = 'E' and dateadd(minute, i.acceptableDelayInMinutes, i.lastHeardFrom) > getdate()) t";
               // Console.WriteLine(_sCallThread + ": sql = " + sql);
                cnt = (int)_db.SqlGetSingleValue(sql);
                if (cnt == 0) return true;
                else return false;
            }

            if (bMain)
            {
                sql = "select count(*) cnt from (select top 1 * from " + sJTable + " where status = 'E' and owner is null) t";
               // Console.WriteLine(_sCallThread + ": sql = " + sql);
                cnt = (int)_db.SqlGetSingleValue(sql);
                if (cnt == 0) return false;
                else
                {
                    sql = "select count(*) cnt from (select top 1 j.owner from " + sJTable + " j join " + sQiTable
                            + " i on j.owner = i.id where j.status = 'P' and j.owner is not null and dateadd(minute, i.acceptableDelayInMinutes, i.lastHeardFrom) > getdate()) t";
                   // Console.WriteLine(_sCallThread + ": sql = " + sql);
                    cnt = (int)_db.SqlGetSingleValue(sql);
                    if (cnt == 0) return true;
                    else return false;
                }
            }
            else return false;
        }

        private void SetStopProcess()
        {
            Console.WriteLine(_sCallThread + ": stopProcess = " + frmQueueRunner.StopProcess.ToString());
            if (frmQueueRunner.StopProcess)
            {
                _bRun = false;
                Console.WriteLine(_sCallThread + ": stopProcess");
                _sQueue = "";
            }
        }


        public bool OpenDB()
        {
            _db.ConnectionString = ConnectionString;
            if (!_db.OpenDbConnection())
            {
                Console.WriteLine(_sCallThread + ": can't open DB");
                return false;
            }
            Console.WriteLine(_sCallThread + ": opened DB");
            return true;
        }

        public void CloseDB()
        {
            try
            {
                _db.DisposeDbConnection();
            }
            catch (Exception e)
            {
                Console.WriteLine(_sCallThread + ": can't close db " + e.Message);
            }

        }

    }
}
