﻿using System;
using System.Windows.Forms;
using NamedPipesModule;
using System.Configuration;
using System.Timers;
using Com.VeritasTS.DatabaseComm;
using ChangeResponderRegistry;

namespace Com.VeritasTS.QueueRunner
{
    public partial class frmQueueRunner : Form
    {
        public static IChannelManager PipeManager;
        public static TextBox ActivityRef;
        public static TextBox MaxThreadsRef;
        public static Label ThreadsRef;

        public static string DbConnectionString;
        public static string ThreadBase;
        public static string SingleQueueMaxThreads;
        public static string SchemaName;

        public static bool StopProcess = false;

        private System.Timers.Timer _startTimer;
        private System.Timers.Timer _stopTimer;

        public static ChangeResponderFactory ChangeResponderFactory;

        [STAThread]
        static void Main()
        {
            Application.Run(new frmQueueRunner());
        }

        public frmQueueRunner()
        {
            AllocConsole();
            InitializeComponent();

            txtMaxThreads.Text = ConfigurationManager.AppSettings["TotalThreads"];
            DbConnectionString = ConfigurationManager.AppSettings["DatabaseConnectionString"];
            ThreadBase = ConfigurationManager.AppSettings["ThreadBase"];
            SingleQueueMaxThreads = ConfigurationManager.AppSettings["SingleQueueMaxThreads"];
            SchemaName = ConfigurationManager.AppSettings["SchemaName"];

            ActivityRef = txtActivity;
            MaxThreadsRef = txtMaxThreads;
            ThreadsRef = lblThreads;
            PipeManager = new PipeManager();
            PipeManager.Initialize();
            Console.WriteLine("start,  DbConnectionString = " + DbConnectionString);
            SetStartTimer();
        }

        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        private static extern bool AllocConsole();

        private void frmQueueRunner_Closing(object sender, FormClosingEventArgs e)
        {
            PipeManager?.Stop();
        }

        private void cmdStop_Click(object sender, EventArgs e)
        {
            if (StopProcess)
            {
                StopProcess = false;
                cmdStop.Text = "Stop";
            }
            else
            {
                StopProcess = true;
                cmdStop.Text = "Start";
                SetStopTimer();
            }
            Console.WriteLine("frmQueueRunner.cmdStop_Click: StopProcess = " + StopProcess.ToString());
        }

        // 2 seconds after the window pops up, start all threads
        private void SetStartTimer()
        {
            _startTimer = new System.Timers.Timer(2000);
            _startTimer.Elapsed += OnStartTimedEvent;
            _startTimer.AutoReset = true;
            _startTimer.Enabled = true;
        }
        private void OnStartTimedEvent(object source, ElapsedEventArgs e)
        {
            _startTimer.Stop();
            _startTimer.Close();
            _startTimer.Dispose();

            GetChangeResponderFactory();

            StartProcess();
        }

        private void GetChangeResponderFactory()
        {
            Database db = new DbServices(new Server(new Vendor { Name = "SQLServer" }));
            db.ConnectionString = DbConnectionString;
            ChangeResponderFactory = new ChangeResponderFactory(db, "vtsQs");
            db.DisposeDbConnection();
        }


        private void StartProcess()
        {
            int totalThreads = Convert.ToInt32(MaxThreadsRef.Text);
            for (int i = 0; i < totalThreads; i++)
            {
                PipeManager.HandleRequest("Start thread " + (i+1).ToString());
            }
        }

        // set timer to check if all threads are done
        private void SetStopTimer()
        {
            _stopTimer = new System.Timers.Timer(1000);
            _stopTimer.Elapsed += OnStopTimedEvent;
            _stopTimer.AutoReset = true;
            _stopTimer.Enabled = true;
        }
        private void OnStopTimedEvent(object source, ElapsedEventArgs e)
        {
            // if stopProcess has been changed back to false, re-start all
            if (!StopProcess)
            {
                cmdStop.Text = "Stop";
                _stopTimer.Stop();
                _stopTimer.Close();
                _stopTimer.Dispose();
                StartProcess();
                return;
            }

            // wait until all threads are done, then close the window
            int numThreads = Convert.ToInt32(lblThreads.Text);
            if (numThreads == 0)
            {
                ChangeResponderFactory?.Dispose();
                ChangeResponderFactory = null;
                this.Close();
            }
        }
    }
}
