﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.QueueRunner
{
    public class QueueProcessor
    {
        private string _threadName;
        //private string _dbConnectionString;
        //private string _threadBase;
        //private string _singleQueueMaxThreads;

        public QueueProcessor(string threadNum)
        {
            //_dbConnectionString = dbConnectionString;
            //_threadBase = threadBase;
            //_singleQueueMaxThreads = singleQueueMaxThreads;
            _threadName = frmQueueRunner.ThreadBase + threadNum;
        }

        public void Process()
        {
            QueueRunnerProcessor runner = new QueueRunnerProcessor(_threadName);
            runner.ConnectionString = frmQueueRunner.DbConnectionString;
            runner.sThreadBase = frmQueueRunner.ThreadBase;
            runner.nSingleQueueMax = Convert.ToInt32(frmQueueRunner.SingleQueueMaxThreads);

            if (!runner.OpenDB())
            {
                Console.WriteLine("Thread " + _threadName + " can't open db");
                return;
            }
            try
            {
                while (runner.GetContinueProcess())
                    runner.Process();
            }
            catch (Exception e)
            {
                Console.WriteLine("QueueProcessor.Process:" + e.Message);
            }
            runner.CloseDB();
        }
    }
}
