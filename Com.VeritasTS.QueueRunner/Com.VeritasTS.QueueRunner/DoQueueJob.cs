﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.VeritasTS.QueueRunner
{
    class DoQueueJob
    {
        protected int iQueueID;
        protected string sJobTable, sLogTable, sBLTable;
        protected float fMinutes;
        protected DbServices db;

        protected int iJobNum, iQi = -1;

        public bool bQueueObjCreated = false;

        public DoQueueJob(int iQueueIDin, DbServices database)
        {
            db = database;
            iQueueID = iQueueIDin;
            string sql = "select jobsVableName, logName, blacklistVableName, acceptableDelayInMinutes from vtsQs.queues where id = " + iQueueID.ToString();
            var data = db.getTableData(sql);
            if (data != null)
            {
                var row = data.FirstOrDefault();
                sJobTable = row[0];
                sLogTable = row[1];
                sBLTable = row[2];
                fMinutes = Convert.ToSingle(row[3]);
                bQueueObjCreated = true;
                data.Clear();
            }
            data = null;
        }

        protected bool claimNextJobs()
        {
            string sProc = "vtsQs.acquireNewQI";
            string[] prams = { "queueID", "acceptableDelayInMinutes", "iterations", "newQI" };
            string[] pramTypes = { "int", "float", "int", "int" };
            Object[] pramVal = { iQueueID, fMinutes, 1 };
            Object obj = db.runSQLProcedure(sProc);
            if (obj != null) iQi = (int)obj;
            else return false;

            sProc = "vtsQs.claimNextJob";
            string[] prams1 = { "queueID", "qi", "jobNum" };
            string[] pramTypes1 = { "int", "int", "int" };
            Object[] pramVal1 = { iQueueID, iQi };
            obj = db.runSQLProcedure(sProc);
            if (obj != null) iJobNum = (int)obj;
            else return false;

            return true;
        }

        protected void logFromQueue(string sMsg)
        {
            //string sProc = "vtsQs.logFromQueue";
            //string[] prams = { "queueID", "jobNum", "qi", "msg" };
            //string[] pramTypes = { "int", "int", "int", "nvarchar" };
            //Object[] pramVal = { iQueueID, iJobNum, iQi, sMsg };
            //db.runSQLProcedure(sProc, prams, pramTypes, pramVal, false);
            db.logFromQueue(iQueueID, iJobNum, iQi, sMsg);
        }

        protected void qiPing()
        {
            qiTableProc("vtsQs.qiPing");
        }

        protected void killQI()
        {
            qiTableProc("vtsQs.killQI");
        }

        private void qiTableProc(string sProc)
        {
            //string[] prams = { "queueID", "qi" };
            //string[] pramTypes = { "int", "int" };
            //Object[] pramVal = { iQueueID, iQi };
            db.AddStoredProcedureParameter("@queueID", SqlDbType.Int, iQueueID, ParameterDirection.Input, refresh: true);
            db.AddStoredProcedureParameter("@qi", SqlDbType.Int, iQi, ParameterDirection.Input);
            db.runSQLProcedure(sProc);
        }

        protected void handleErroredJobs(string errMsg)
        {
            string sProc = "vtsQs.handleErroredJobs";
            //string[] prams = { "queueID", "jobNum", "qi", "jobsVableName", "sysErrorMessage" };
            //string[] pramTypes = { "int", "int", "int", "nvarchar", "nvarchar" };
            //Object[] pramVal = { iQueueID, iJobNum, iQi, sJobTable, errMsg };
            db.AddStoredProcedureParameter("@queueID", SqlDbType.Int, iQueueID, ParameterDirection.Input, refresh: true);
            db.AddStoredProcedureParameter("@jobNum", SqlDbType.Int, iJobNum, ParameterDirection.Input);
            db.AddStoredProcedureParameter("@qi", SqlDbType.Int, iQi, ParameterDirection.Input);
            db.AddStoredProcedureParameter("@jobsVableName", SqlDbType.NVarChar, sJobTable, ParameterDirection.Input);
            db.AddStoredProcedureParameter("@sysErrorMessage", SqlDbType.NVarChar, errMsg, ParameterDirection.Input);
            db.runSQLProcedure(sProc);
        }

        // child will call this, and override
        public bool processJobs()
        {
            if (!claimNextJobs())
            {
                if (iQi > 0) killQI();
                return false;
            }
            return true;
        }
    }
}
