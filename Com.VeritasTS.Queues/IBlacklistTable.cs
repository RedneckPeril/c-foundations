﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//using Com.VeritasTS.DatabaseUtilityInterfaces;


//namespace Com.VeritasTS.Queues
//{

//    public interface IBlacklistTable : ITable { }

//    public interface IBlacklistTableMutable : IBlacklistTable, ITableMutable { }

//    /// <summary>
//    /// Boilerplate implementation of ITableMutable.
//    /// </summary>
//    /// <remarks>
//    /// Descendant classes should override:
//    /// <list type="bullet">
//    /// <item>Object method overrides and overridden GetHasCode assistants
//    ///   <list type="bullet">
//    ///   <item>GetHashCode().</item>
//    ///   <item>HighestPrimeUsedInHashCode.</item>
//    ///   </list>
//    /// </item>
//    /// <item>DataProcObjectMutable method overrides
//    ///   <list type="bullet">
//    ///   <item>PassesSelfAssertion(), usually.</item>
//    ///   <item>MembersMatch(), if the class adds new members that should
//    /// affect determination of equality with other objects.</item>
//    ///   <item>GetSafeReference(), always.</item>
//    ///   </list>
//    /// </item>
//    /// <item>DatabaseObjectMutable method overrides
//    ///   <list type="bullet">
//    ///   <item>ValidOwnerTypesWhenNotOwner, occasionally if not abstract type
//    ///   and if Owner is not required to be ParentObject.</item>
//    ///   <item>RetrieveIDsFromDatabase, if new child IDatabaseObjects have been
//    /// added as members.</item>
//    ///   </list>
//    /// </item>
//    /// <item>ObjectInSchema method overrides
//    ///   <list type="bullet">
//    ///   <item>ParentObjectMustBeOwner, if true rather than false.</item>
//    ///   <item>PropagateSelfAsParentObjectToAllChildren, if new child IDatabaseObjects have been
//    /// added as members.</item>
//    ///   <item>GetCopyForParentObject, always unless abstract class.</item>
//    ///   </list>
//    /// </item>
//    /// </list>
//    /// </remarks>
//    public class BlacklistTableMutable : TableMutable, IBlacklistTableMutable
//    {

//        public BlacklistTableMutable(
//            string name
//            , ISqlTranslator sqlTranslator = null
//            , IDataProcObject owner
//            , ISchema schema
//            , List<IMappedColumn> keyColumns
//            , int? id = null
//            , bool retrieveIDsFromDatabase = false
//            ) : base(
//                (name == null ? owner.Name + "Bl" : name)
//                , owner: owner
//                , schema: schema
//                , id: id
//                )



//            string name
//            , ISqlTranslator sqlTranslator = null
//            , ISchema parentObject = null
//            , IObjectType objectType = null
//            , IDataProcObject owner = null
//            , bool allowsUpdates = true
//            , bool allowsDeletes = true
//            , Dictionary<string, Tuple<int, IColumn>> columns = null
//            , Dictionary<string, IUniqueKey> uniqueKeys = null
//            , Dictionary<string, IForeignKey> foreignKeys = null
//            , Dictionary<string, IIndex> indexes = null
//            , IPrimaryKey primaryKey = null
//            , IDatabaseSequence primaryKeySequence = null
//            , Dictionary<string, ITableTrigger> triggers = null
//            , Dictionary<string, ICheckConstraint> checkConstraints = null
//            , Dictionary<string, IDefaultConstraint> defaultConstraints = null
//            , bool isAudited = false
//            , string auditTableName = null
//            , bool publishesChanges = false
//            , string changeTrackingTableName = null
//            , ITable parentTable = null
//            , IParentPassThroughInfo parentPassThrough = null
//            , bool canHaveChildren = false
//            , string leavesTableName = null
//            , bool maintainChildVablesTable = false
//            , string childVablesTableName = null
//            , bool hasIDColumn = false
//            , string idColumnName = null
//            , bool hasInputterColumn = false
//            , string inputtersColumnName = null
//            , string validInputtersTableName = null
//            , bool hasLastUpdateDtmColumn = false
//            , bool hasLastUpdaterColumn = false
//            , string nameWhenPrefixed = null
//            , int? id = default(int?)
//            , bool? retrieveIDsFromDatabase = false
//            , bool assert = true
//        {
//            AddOrOverwriteColumn(new IDColumnMutable("blacklistRowID", parentObject: this));
//            foreach (IMappedColumn c in keyColumns)
//                AddOrOverwriteColumn((IMappedColumn)c.GetReferenceForParentObject(this));
//            AddOrOverwriteColumn(new ColumnMutable("errorNum", "int", parentObject: this, isNullable: false));
//            AddOrOverwriteColumn(new ColumnMutable("codeLocation", "nvarchar(500)", parentObject: this, isNullable: false));
//            AddOrOverwriteColumn(new ColumnMutable("description", "nvarchar(max)", parentObject: this, isNullable: false));
//            AddOrOverwriteColumn(new ColumnMutable("recommendation", "nvarchar(max)", parentObject: this, isNullable: false));
//            CompleteConstructionIfType(typeof(BlacklistTableMutable), retrieveIDsFromDatabase);
//        }

//    public override ISafeMemberOfImmutableOwner GetSafeReference()
//    {
//        List<IMappedColumn> keyColumns = new List<IMappedColumn>();
//        foreach (
//            KeyValuePair<string, Tuple<int, IColumn>> pair
//            in Columns.Where(p => typeof(IMappedColumn).IsAssignableFrom(p.Value.Item2.GetType()))
//            )
//            keyColumns.Add((IMappedColumn)pair.Value.Item2);
//        return new BlacklistTableMutable(
//            Name
//            , GetOwnerSafely()
//            , GetSchemaSafely()
//            , keyColumns
//            , ID
//            );
//    }

//}


//public class BlacklistTableImmutable : TableImmutable, IBlacklistTable
//{
//    private IBlacklistTable Delegee { get { return (IBlacklistTable)_delegee; } }

//    //Disable default constructor
//    private BlacklistTableImmutable() : base(delegee: null) { }

//    public BlacklistTableImmutable(
//        string name
//        , IDataProcObject owner
//        , ISchema schema
//        , List<IMappedColumn> keyColumns
//        , int? id = null
//        , bool retrieveIDsFromDatabase = false
//        ) : base(delegee: null)
//    {
//        _delegee =
//            new BlacklistTableMutable(
//                name
//                , owner
//                , schema
//                , keyColumns
//                , id
//                , retrieveIDsFromDatabase
//                );
//        CompleteConstructionIfType(typeof(BlacklistTableImmutable), retrieveIDsFromDatabase: false);
//    }

//    public BlacklistTableImmutable(IBlacklistTable delegee) : base(delegee: null)
//    {
//        _delegee = delegee;
//        CompleteConstructionIfType(typeof(BlacklistTableImmutable), retrieveIDsFromDatabase: false);
//    }

//}

//}