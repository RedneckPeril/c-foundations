﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.VeritasTS.DatabaseUtilities;
using Com.VeritasTS.DataTableDef;
using Com.VeritasTS.DatabaseComm;
using InterfaceChangeResponder;
// !!!!! customer process object library !!!!!
//using TestCustomerLib;


namespace Com.VeritasTS.Queues
{
    public class testQueueTableQ : QueueBase, IChangeResponder  // 1st tier queue
    {
        public testQueueTableQ()
        {
            // !!!!! load customer process object library !!!!!
            //TestClass c = new TestClass();
            //c = null;
            // !!!!! load customer process object library !!!!!
            TriggerTable = new testQueueTable();
            Name = TriggerTable.Name + "Q";// "testQueueTableQ"; 
            Schema = TriggerTable.Schema; // new Schema { Name = "dbo" };
            SettingsSchema = new Schema { Name = "vtsQs" };
            AcceptableDelayInMinutes = 5;
            TargetJobSize = 500;
            MaxRowsInLog = 1000;
            MaxOwnerCount = 2;
            //QueuesToMake = new List<IQueue>() { new TestQueue1() };
            //QueueKeyColumns = new List<IMappedColumn>() { new MappedColumn { Name = "testQueueId", DataType = "int", IsNullable = false, SourceColumn = new Column { Name = "Id" } } };
        }
    }
    public class TestQueue1 : QueueBase, IChangeResponder // 2nd tier queue
    {
        public TestQueue1()
        {
            Name = "TestQueue1";
            Schema = new Schema { Name = "dbo" };
            SettingsSchema = new Schema { Name = "vtsQs" };
            //ClaimNextJobProcedureName = "claimNextJobTest";
            AcceptableDelayInMinutes = 5;
            TargetJobSize = 500;
            MaxRowsInLog = 1000;
            MaxOwnerCount = 2;
            QueueKeyColumns = new List<IMappedColumn>() { new MappedColumn("testQueueId", null,  "int", isNullable: false)};
        }

        public override void MakeJobs(IQueue triggerQueue, int jobNum, int qiId)
        {
            base.MakeJobs(triggerQueue, jobNum, qiId);

            string sql = "insert into " + JobsTable.Schema.Name + "." + JobsTable.Name + " (JobNum, JobDtm, Status, Action, testQueueId) select -1, getdate(), 'U', 'action' "; 
            if (triggerQueue.Name.Equals("testQueueTableQ")) MakeJobsTest1(sql, jobNum, qiId);
            if (triggerQueue.Name.Equals("TestQueueOther")) MakeJobsTest2(sql);
            //SendQueueProcRequest();
        }

        private void MakeJobsTest1(string sqlIn, int jobNum, int qiId)
        {
            testQueueTableQ queue = new testQueueTableQ();
            string jobs = queue.JobsTable.Schema.Name + "." + queue.JobsTable.Name;
            string qis = queue.QisTable.Schema.Name + "." + queue.QisTable.Name;
            string keys = "";
            foreach (var key in queue.QueueKeyColumns)
                keys += ", " + key.Name;
            var trans = Database.BeginTransaction();
            string sql = sqlIn + keys + " from " + jobs + " where jobNum = " + jobNum.ToString() + " and owner = " + qiId.ToString();
            Database.SqlExecuteNonQuery(sql, trans);
            sql = "delete from " + jobs + " where jobNum = " + jobNum.ToString() + " and owner = " + qiId.ToString();
            Database.SqlExecuteNonQuery(sql, trans);
            sql = "delete from " + qis + " where id = " + qiId.ToString();
            Database.SqlExecuteNonQuery(sql, trans);
            trans.Commit();
        }
        private void MakeJobsTest2(string sqlIn)
        {
            var trans = Database.BeginTransaction();
            //string sql = sqlIn + "dbo.testQueueTableQjobs";
            //Database.SqlExecuteNonQuery(sql, trans);
            trans.Commit();
        }


        public override void ProcessJob()
        {
            int jobNum = -1, qiId = -1;
            StartProcess(ref jobNum, ref qiId);

            // business do logic

            return;
        }
    }
}
