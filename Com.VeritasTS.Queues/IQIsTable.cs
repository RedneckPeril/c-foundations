﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using Com.VeritasTS.DatabaseUtilityInterfaces;


//namespace Com.VeritasTS.Queues
//{

//    public interface IQIsTable : ITable { }
//    public interface IQIsTableMutable : IQIsTable, ITableMutable { }

//    public class QIsTableMutable : TableMutable, IQIsTableMutable
//    {
//        public QIsTableMutable(
//            string name
//            , IDataProcObject owner
//            , bool retrieveIDsFromDatabase = false
//            )
//            : base(
//                  (
//                  name == null
//                  ? owner.Name + (owner.Name.Substring(owner.Name.Length - 1, 1).Equals("Q") ? "Is" : "QIs")
//                  : name
//                  )
//                  , owner: owner
//                  )
//        {
//            SetSchema(owner.Schema);
//            AddOrOverwriteColumn(new IDColumnMutable("id", this));
//            AddOrOverwriteColumn(new ColumnMutable("queueID", "int", this, isNullable: false));
//            AddOrOverwriteColumn(new ColumnMutable("acceptableDelayInMinutes", "float", this, isNullable: false));
//            AddOrOverwriteColumn(new ColumnMutable("lastHeardFrom", "datetime", this, isNullable: false));
//            AddOrOverwriteColumn(new ColumnMutable("targetJobSize", "int", this));
//            string falseSql = Database.SqlTranslator.FalseSql();
//            string trueSql = Database.SqlTranslator.TrueSql();
//            string boolSql = Database.SqlTranslator.BooleanTypeSql();
//            AddOrOverwriteColumn(
//                new ColumnMutable(
//                    "IsAlive"
//                    , dataType: boolSql
//                    , parentObject: this
//                    , calcFormula:
//                    "cast( case when datediff(year,lastHeardFrom,getdate())>(2) then " + falseSql
//                    + " when datediff(second,lastHeardFrom,getdate())<(60)*acceptableDelayInMinutes then " + trueSql
//                    + " else " + falseSql + " end as " + boolSql + " )"
//                    , isPersisted: true
//                    )
//                );
//            SetPrimaryKey(
//                new PrimaryKeyMutable(
//                    Database.SqlTranslator.TableConstraintName("_pk", Name)
//                    , this
//                    , new List<string> { "id" }
//                    )
//                );
//            CompleteConstructionIfType(typeof(QIsTableMutable), retrieveIDsFromDatabase);
//        }
//    }
//}
