﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using Com.VeritasTS.DatabaseUtilityInterfaces;


//namespace Com.VeritasTS.Queues
//{

//    public interface IQueueLogTable : ITable { }
//    public interface IQueueLogTableMutable : IQueueLogTable, ITableMutable { }

//    public class QueueLogTable : Table, IQueueLogTableMutable
//    {
//        public QueueLogTable(IDataProcObject owner) : base()
//        {
//            Owner = owner;
//            Name = Owner.Name;
//            Name += Name.Substring(Name.Length - 1, 1).Equals("Q") ? "Log" : "QLog";
//            Schema = Owner.Schema;
//            Database = Owner.Database;
//            _tableMutable.LoadColumn(new Column("Id", "int", this, isIdentity: true, isNullable: false));
//            _tableMutable.LoadColumn(new Column("Qi", "int", this));
//            _tableMutable.LoadColumn(new Column("JobNum", "int", this));
//            _tableMutable.LoadColumn(new Column("Msg", "nvarchar(max)", this));
//            _tableMutable.LoadColumn(new Column("Dtm", "datetime", this, isNullable: false));
//            _tableMutable.PrimaryKey = new PrimaryKey(Name + "_pk", new List<string> { "Id" }, this);
//            _tableMutable.LoadDefault(new Default { Name = this.Name + "Dtm" + "_default", Owner = this, ColumnWithDefault = new Column("Dtm"), DefaultValue = "getdate()" });
//        }
//    }