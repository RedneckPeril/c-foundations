﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//using Com.VeritasTS.TextAndErrorUtils;
//using Com.VeritasTS.DatabaseUtilityInterfaces;

//namespace Com.VeritasTS.Queues
//{
//    /// <summary>
//    /// Interface for a table that is populated at least partly
//    /// by a queue.
//    /// </summary>
//    /// <remarks>
//    /// We have to be prepared for the most complex type of table,
//    /// which is a table that accepts input from multiple queues
//    /// and possibly a UI as well. Such a table will have an
//    /// array of inputters along with a column that specifies 
//    /// which inputter owns a particular column. For each inputter,
//    /// specific columns on the queue will serve as the key columns
//    /// that drive input jobs.
//    /// </remarks>
//    public interface IQueuePopulatedTable : ITable
//    {
//    }
//    public interface IQueuePopulatedTableMutable : IQueuePopulatedTable, ITableMutable
//    {
//    }
//}
