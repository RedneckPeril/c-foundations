﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//using Com.VeritasTS.TextAndErrorUtils;
//using Com.VeritasTS.DatabaseUtilityInterfaces;
//using System.Data;
//using InterfaceChangeResponder;


//namespace Com.VeritasTS.Queues
//{
//    public interface IQueue : IDataProcObject, IChangeResponder
//    {
//        ITable TriggerTable { get; set; }
//        ISchema SettingsSchema { get; set; }
//        List<IChangeResponder> QueuesToMake { get; set; }
//        List<IMappedColumn> QueueKeyColumns { get; set; }
//        //List<IColumn> QueueKeyColumns { get; set; }
//        ITable JobsTable { get; }
//        ITable QisTable { get; }
//        float AcceptableDelayInMinutes { get; set; }
//        int TargetJobSize { get; set; }
//        ITable LogTable { get; }
//        int MaxRowsInLog { get; set; }
//        ITable BlacklistTable { get; }
//        int Id { get; set; }
//        int MaxOwnerCount { get; set; }
//        string ClaimNextJobProcedureName { get; set; }

//        void GetQueuesToMake();
//        void MakeJobs(IQueue triggerQueue, int jobNum, int qiId);
//        //void MakeJobs(string tempTableI, string tempTableD);
//        void StartProcess(ref int jobNum, ref int qiId);
//        void ProcessJob();
//    }
//    public abstract class QueueBase : IQueue
//    {
//        protected IQueueJobsTable _jobsTable;
//        protected IQIsTable _qiTable;
//        protected IBlackListTable _blackListTable;
//        protected QueueLogTable _logTable;
//        protected List<IMappedColumn> _queueKeyColumns;

//        public string Name { get; set; }
//        public ISchema Schema { get; set; }
//        public IDatabase Database { get; set; }
//        public ITable TriggerTable { get; set; }
//        public ISchema SettingsSchema { get; set; }
//        public List<IChangeResponder> QueuesToMake { get; set; }
//        public float AcceptableDelayInMinutes { get; set; }
//        public int TargetJobSize { get; set; }
//        public int MaxRowsInLog { get; set; }
//        public int MaxOwnerCount { get; set; }
//        public int Id { get; set; }
//        public string ClaimNextJobProcedureName { get; set; } = "claimNextJob";

//        public List<IMappedColumn> QueueKeyColumns {
//            get {
//                if (_queueKeyColumns != null) return _queueKeyColumns;
//                if (TriggerTable != null)
//                    _queueKeyColumns = TriggerTable.QueueKeyColumns;
//                return _queueKeyColumns;
//            }
//            set {_queueKeyColumns = value;}
//        }


//        public IDatabase GetDatabase()
//        {
//            if (Database != null) return Database;
//            else
//            {
//                if (TriggerTable != null)
//                    Database = TriggerTable.Database;
//                return Database;
//            }
//        }

//        public void RespondToChange(int jobNum, int qiId, object TriggerQueue = null)
//        {
//            if (TriggerTable == null)
//                MakeJobs((IQueue)TriggerQueue, jobNum, qiId);
//        }

//        public virtual void MakeJobs(IQueue triggerQueue, int jobNum, int qiId)
//        {
//            if (Database == null) Database = triggerQueue.Database;
//        } 
//        //public void MakeJobs(string tempTableI, string tempTableD)
//        //{
//        //    ITable table = TriggerTable;
//        //    string jobs = JobsTable.Schema.Name + "." + JobsTable.Name;
//        //    string KeyColumnSrcNames = "";
//        //    string KeyColumnSrcNamesI = "";

//        //    string sql = "insert into " + jobs + " (JobNum, JobDtm, Status, Action, " ;

//        //    foreach (var c in QueueKeyColumns)
//        //    {
//        //        sql += c.Name + ", ";
//        //        KeyColumnSrcNames += c.SourceColumn.Name + ", ";
//        //        KeyColumnSrcNamesI += "i." + c.SourceColumn.Name + ", ";
//        //    }

//        //    if (tempTableD.Length == 0)
//        //    {
//        //        sql += CommUtils.GetColumnList(table.InsertableColumns, "New") + ") "
//        //            + "select -1, getdate(), 'U', 'insert', " + KeyColumnSrcNames + CommUtils.GetColumnList(table.InsertableColumns, "")
//        //            + " from " + tempTableI;
//        //    }

//        //    if (tempTableI.Length == 0)
//        //    {
//        //        sql += CommUtils.GetColumnList(table.InsertableColumns, "Old") + ") "
//        //            + "select -1, getdate(), 'U', 'delete', " + KeyColumnSrcNames + CommUtils.GetColumnList(table.InsertableColumns, "")
//        //            + " from " + tempTableD;
//        //    }

//        //    if (tempTableD.Length > 0 && tempTableI.Length > 0)
//        //    {
//        //        sql += CommUtils.GetColumnList(table.InsertableColumns, "New") + "," + CommUtils.GetColumnList(table.InsertableColumns, "Old") + ") "
//        //            + "select -1, getdate(), 'U', 'update', " + KeyColumnSrcNamesI 
//        //            + CommUtils.GetColumnList(table.InsertableColumns, "i.") + ", " + CommUtils.GetColumnList(table.InsertableColumns, "d.")
//        //            + " from " + tempTableI + " i"
//        //            + " join " + tempTableD + " d"
//        //            + " on i.id = d.id";
//        //    }
//        //    GetDatabase().SqlExecuteNonQuery(sql);
//        //}



//        // don't need SendQueueProcRequest any more
//        //public void SendQueueProcRequest()
//        //{
//        //    string schemaName = SettingsSchema.Name;
//        //    var db = GetDatabase();
//        //   // db.AddStoredProcedureParameter("@schemaName", SqlDbType.NVarChar, schemaName, ParameterDirection.Input, 100, true);
//        //    db.SqlExecuteStoredProcedure(schemaName + ".SendQueueJobsReadyMsgCaller");

//        //    //string sql = "select count(*) cnt from " + SettingsSchema.Name + ".queueProcInfo where name = '" + Name + "' and jobsReady = 0";
//        //    //int cnt = Convert.ToInt32(GetDatabase().SqlExecuteSingleValue(sql));
//        //    //if (cnt > 0)
//        //    //{
//        //    //    sql = "update " + SettingsSchema.Name + ".queueProcInfo set jobsReady = 1 where name = '" + Name + "'";
//        //    //    Database.SqlExecuteNonQuery(sql);
//        //    //}
//        //}

//        public virtual void ProcessJob()
//        {
//            int jobNum = 0;
//            int qiId = 0;
//            StartProcess(ref jobNum, ref qiId);
//            if (jobNum <= 0 || qiId <= 0) return;

//            GetQueuesToMake();

//            foreach (var q in QueuesToMake)
//            {
//                q.RespondToChange(jobNum, qiId, (object)this);
//                //if (q.IsQueue)
//                //    q.RespondToChange(jobNum, qiId, this);
//                //else
//                //    q.RespondToChange(jobNum, qiId);
//            }
//            //q.MakeJobs(this, jobNum, qiId);
//        }


//        public ITable JobsTable
//        {
//            get
//            {
//                if (_jobsTable == null)
//                {
//                    if (TriggerTable != null)
//                        _jobsTable = TriggerTable.QueueJobsTable;
//                        //_jobsTable = new QueueJobsTable(this, QueueKeyColumns, TriggerTable.InsertableColumns);
//                    else
//                        _jobsTable = new QueueJobsTable(this, QueueKeyColumns, null);
//                }
//                return _jobsTable;
//            }
//        }
//        public ITable QisTable { get {
//                if (_qiTable == null)
//                {
//                    if (TriggerTable != null)
//                        _qiTable = TriggerTable.QIsTable;
//                    else
//                        _qiTable = new QIsTable(this);
//                }
//                return _qiTable;
//            } }
//        public ITable BlacklistTable {
//            get
//            {
//                if (_blackListTable == null)
//                {
//                    if (TriggerTable != null)
//                        _blackListTable = TriggerTable.BlackListTable;
//                    else
//                    {
//                        List<IMappedColumn> keyColumns = new List<IMappedColumn>();
//                        foreach (var column in QueueKeyColumns)
//                            keyColumns.Add((IMappedColumn)column.GetThreadsafeClone());
//                        _blackListTable = new BlackListTable(this, keyColumns);
//                    }
//                }
//                return _blackListTable;
//            }
//        }
//        public ITable LogTable { get
//            {
//                if (_logTable == null)
//                {
//                    if (TriggerTable != null)
//                        _logTable = TriggerTable.QueueLogTable;
//                    else
//                        _logTable = new QueueLogTable(this);
//                }
//                return _logTable;
//            } }


//        public void EnsureExistence()
//        {
//            IDatabase db = GetDatabase();
//            if (!Exists())
//            {
//                string sql = "select id from " + SettingsSchema.Name + ".Queues where Name = '" + Name + "'";
//                var ret = db.SqlExecuteSingleValue(sql);
//                var trans = db.BeginTransaction();
//                if (ret != null)
//                    Id = Convert.ToInt32(ret);
//                else
//                {
//                    string columns = "Name, JobsVableName, QisVableName, AcceptableDelayInMinutes, TargetJobSize, LogName, maxRowsInLog, blacklistVableName, MaxOwnerCount";
//                    string inputs = "'" + Name + "','" + JobsTable.Schema.Name + "." + JobsTable.Name + "','"
//                            + QisTable.Schema.Name + "." + QisTable.Name + "'," + AcceptableDelayInMinutes.ToString()
//                            + "," + TargetJobSize.ToString() + ",'" + LogTable.Schema.Name + "." + LogTable.Name + "',"
//                            + MaxRowsInLog.ToString() + ",'" + BlacklistTable.Schema.Name + "." + BlacklistTable.Name + "', " + MaxOwnerCount.ToString();
//                    sql = SqlTranslatorSqlServer2014.GetAddIntoQueuesSql(columns, inputs, SettingsSchema.Name);
//                    Id = Convert.ToInt32(db.SqlExecuteSingleValue(sql, trans));
//                }
//                sql = "insert into " + SettingsSchema.Name + ".QueueProcInfo (queueID, name) values (" + Id.ToString() + ",'" + Name + "');";
//                db.SqlExecuteNonQuery(sql, trans);
//                trans.Commit();
//            }
//            if (TriggerTable == null)
//            {
//                JobsTable.EnsureExistence();
//                JobsTable.MakeTable();
//                QisTable.EnsureExistence();
//                QisTable.MakeTable();
//                BlacklistTable.EnsureExistence();
//                BlacklistTable.MakeTable();
//                LogTable.EnsureExistence();
//                LogTable.MakeTable();
//            }
//        }
//        public bool Exists()
//        {
//            string sql = "select q.id from " + SettingsSchema.Name + ".Queues q join " + SettingsSchema.Name + ".QueueProcInfo i on i.queueID = q.id where q.Name = '" + Name + "'";
//            var ret = GetDatabase().SqlExecuteSingleValue(sql);
//            if (ret != null)
//            {
//                Id = Convert.ToInt32(ret);
//                return true;
//            }
//            return false;
//        }

//        public void StartProcess(ref int jobNum, ref int qiId)
//        {
//            if (!(Id > 0))
//                if (!Exists()) EnsureExistence();

//            string keyColumnJoin = "";
//            string keyColumSelect = "";
//            string keyColumSelectJ = "";
//            string keyColumTable = "";
//            foreach (var c in QueueKeyColumns)
//            {
//                keyColumnJoin += ((keyColumnJoin.Length > 0) ? " and " : "") + "j." + c.Name + " = t." + c.Name;
//                keyColumSelect += ((keyColumSelect.Length > 0) ? ", " : "") + c.Name;
//                keyColumSelectJ += ((keyColumSelectJ.Length > 0)? ", " : "") + "j." + c.Name;
//                keyColumTable += ((keyColumTable.Length > 0) ? ", " : "") + c.Name + " " + c.DataType + " null";
//            }

//            string sql = "declare @p table(col1 int); ";
//            sql += "insert into " + QisTable.Schema.Name + "." + QisTable.Name + " (queueID, acceptableDelayInMinutes, LastHeardFrom) output INSERTED.id into @p values ("
//                    + Id.ToString() + ", " + AcceptableDelayInMinutes.ToString() + ", getdate()); ";
//            sql += "select * from @p;";
//            qiId = Convert.ToInt32(Database.SqlExecuteSingleValue(sql));

//            Database.AddStoredProcedureParameter("@queueID", SqlDbType.Int, Id, ParameterDirection.Input, refresh: true);
//            Database.AddStoredProcedureParameter("@qi", SqlDbType.Int, qiId, ParameterDirection.Input);
//            Database.AddStoredProcedureParameter("@keyColumnJoin", SqlDbType.NVarChar, keyColumnJoin, ParameterDirection.Input, 1000);
//            Database.AddStoredProcedureParameter("@keyColumSelect", SqlDbType.NVarChar, keyColumSelect, ParameterDirection.Input, 1000);
//            Database.AddStoredProcedureParameter("@keyColumSelectJ", SqlDbType.NVarChar, keyColumSelectJ, ParameterDirection.Input, 1000);
//            Database.AddStoredProcedureParameter("@keyColumTable", SqlDbType.NVarChar, keyColumTable, ParameterDirection.Input, 1000);
//            Database.AddStoredProcedureParameter("@jobNum", SqlDbType.Int, -1, ParameterDirection.Output);

//            var ret = Database.SqlExecuteStoredProcedure(SettingsSchema.Name + "." + ClaimNextJobProcedureName);

//            if (ret == null)
//                throw (new Exception("can not get job number"));

//            jobNum = Convert.ToInt32(ret.FirstOrDefault().Value);
//        }

//        public void GetQueuesToMake()
//        {
//            string sql = "select changeResponder, 1 isQueue from " + SettingsSchema.Name + ".queues2Tier where queue1Tier = '" + Name
//                + "' union select changeResponder, 0 from " + SettingsSchema.Name + ".custChangeResponders where queue1Tier = '" + Name + "'";
//            var queues = GetDatabase().SqlExecuteReader(sql);

//            QueuesToMake = null;
//            QueuesToMake = new List<IChangeResponder>();
//            int num = queues.Count;
//            foreach (var q in queues)
//            {
//                string name = q[0];
//                bool isQueue = q[1] == "1" ? true : false;
//                IChangeResponder queue = GetChangeResponderByName(name);
//                if (queue != null)
//                    QueuesToMake.Add(queue);
//            }
//        }

//        protected IChangeResponder GetChangeResponderByName(string objName)
//        {
//            Type type = CommUtils.GetTypeByName(objName);
//            if (type != null)
//                return (IChangeResponder)Activator.CreateInstance(type);
//            else
//                return null;
//        }
//    }
//}
