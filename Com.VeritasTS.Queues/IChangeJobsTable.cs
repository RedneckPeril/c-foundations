﻿//using System;
//using System.Diagnostics;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//using Com.VeritasTS.DatabaseUtilityInterfaces;
//using Com.VeritasTS.TextAndErrorUtils;

//namespace Com.VeritasTS.Queues
//{

//    public interface IChangeJobsTable : IQueueJobsTable, IChangeTrackingTable { }

//    public interface IChangeJobsTableMutable : IChangeJobsTable, IQueueJobsTableMutable, IChangeTrackingTableMutable { }

//    public class ChangeJobsTableMutable : QueueJobsTableMutable, IChangeJobsTableMutable
//    {

//        private ITable _trackedTable;

//        public ChangeJobsTableMutable(
//            string name
//            , IDataProcObject owner
//            , ISchema schema
//            , ITable trackedTable
//            , string idColumnName = null
//            , string idColumnNameWhenPrefixed = null
//            , List<IMappedColumn> keyColumns = null
//            , int? id = null
//            , bool retrieveIDsFromDatabase = false
//            ) : base(
//                name: (name == null ? owner.Name + "CJobs" : name)
//                , owner: owner
//                , schema: schema
//                , idColumnName: (idColumnName == null ? "cJobsRowID" : idColumnName)
//                , idColumnNameWhenPrefixed: idColumnNameWhenPrefixed
//                , keyColumns: keyColumns
//                , id: id
//                )
//        {
//            UtilitiesForChangeTrackingTables.AddAuditColumns(
//                table: this
//                , trackedTable: trackedTable
//                , sql: null
//                , retrieveIDsFromDatabase: false
//                );
//            CompleteConstructionIfType(typeof(ChangeJobsTableMutable), retrieveIDsFromDatabase);
//        }

//        // Object method overrides

//        public override int GetHashCode()
//        {
//            return base.GetHashCode()
//                + 31 * (TrackedTable == null ? 0 : TrackedTable.GetHashCode());
//        }

//        // IDataProcObject method overrides
//        public override bool PassesSelfAssertion(Type testableType = null)
//        {
//            if (IsFullyConstructed)
//            {
//                Debug.Assert(base.PassesSelfAssertion());
//                Debug.Assert(
//                    TrackedTable != null
//                    , "A ChangeJobs table must have a non-null tracked table."
//                    );
//                Debug.Assert(
//                    IDColumns.Count == 1
//                    , "A ChangeJobs table must have exactly one ID column."
//                    );
//            }
//            return true;
//        }

//        public override bool MembersMatch(object obj)
//        {
//            if (!base.MembersMatch(obj)) return false;
//            if (!typeof(IChangeJobsTable).IsAssignableFrom(obj.GetType())) return false;
//            IChangeJobsTable other = (IChangeJobsTable)obj;
//            if (!EqualityUtilities.EqualOrBothNull(this.TrackedTable, other.TrackedTable)) return false;
//            return true;
//        }

//        public override ISafeMemberOfImmutableOwner GetSafeReference()
//        {
//            IIDColumn idColumn = IDColumns.First().Value.Item2;
//            return new ChangeJobsTableMutable(
//                Name
//                , (Owner == null ? null : GetOwnerSafely())
//                , (Schema == null ? null : GetSchemaSafely())
//                , (TrackedTable == null ? null : GetTrackedTableSafely())
//                , (idColumn == null ? null : idColumn.Name)
//                , (idColumn == null ? null : idColumn.NameWhenPrefixed)
//                , GetQueueKeyColumnsSafely()
//                , ID
//                );
//        }

//        // IDatabaseObjectMutable method overrides

//        public override void RetrieveIDsFromDatabase(bool assert = true)
//        {
//            if (Database != null)
//                if (TrackedTable != null)
//                    if (typeof(ITableMutable).IsAssignableFrom(TrackedTable.GetType()))
//                        ((ITableMutable)TrackedTable).RetrieveIDsFromDatabase(assert: false);
//            base.RetrieveIDsFromDatabase(assert);
//            if (assert) Debug.Assert(PassesSelfAssertion());
//        }

//        // ICheckConstraint properties and methods
//        public ITable TrackedTable { get { return _trackedTable; } set { SetTrackedTable(value); } }
//        public ITable GetTrackedTableSafely()
//        {
//            return (ITable)(_trackedTable == null ? null : _trackedTable.GetSafeReference());
//        }

//        // IChangeJobsTableMutable properties and methods

//        public virtual void SetTrackedTable(ITable value, bool assert = true)
//        {
//            _trackedTable = value;
//            if (assert) Debug.Assert(PassesSelfAssertion());
//        }

//    }

//}
