﻿//using System;
//using System.Linq;
//using System.Collections.Generic;
//using Com.VeritasTS.DatabaseUtilityInterfaces;

//namespace Com.VeritasTS.Queues
//{

//    public interface IQueueJobsTable : ITable
//    {
//        List<IMappedColumn> QueueKeyColumns { get; }
//        List<IMappedColumn> GetQueueKeyColumnsSafely();
//    }
//    public interface IQueueJobsTableMutable : IQueueJobsTable, ITableMutable
//    {
//        void SetQueueKeyColumns(List<IMappedColumn> queueKeyColumns);
//    }

//    public static class UtilitiesForQueueJobsTables
//    {
//        public static void AddStandardQueueJobsColumns(ITableMutable table, ISqlTranslator sql = null, bool retrieveIDsFromDatabase = false)
//        {
//            if (sql == null) sql = table.Database.SqlTranslator;
//            table.AddOrOverwriteColumn(
//                new ColumnMutable(
//                    name: "jobNum"
//                    , dataType: "int"
//                    , isNullable: false
//                    , defaultConstraint:
//                    new DefaultConstraintMutable(
//                        name: sql.TableConstraintName("_dJN", table.Name)
//                        , definition: "0"
//                        )
//                    )
//                );
//            table.AddOrOverwriteColumn(new ColumnMutable("owner", "int"));
//            table.AddOrOverwriteColumn(new ColumnMutable("jobDtm", "datetime", isNullable: false));
//            string statusCheckName = sql.TableConstraintName("_ckS", table.Name);
//            table.AddOrOverwriteColumn(
//                new ColumnMutable(
//                    name: "status"
//                    , dataType: "varchar(1)"
//                    , isNullable: false
//                    , checkConstraints:
//                    new Dictionary<string, ICheckConstraint>
//                    {
//                        [statusCheckName] = new CheckConstraintMutable(
//                            name: statusCheckName
//                            , constraintText: "status in ( 'U', 'P', 'E', 'W', 'B', 'X', 'S', 'C' )"
//                            )
//                    }
//                    )
//                );
//            string priorStatusCheckName = sql.TableConstraintName("_ckPS", table.Name);
//            table.AddOrOverwriteColumn(
//                new ColumnMutable(
//                    name: "priorStatus"
//                    , dataType: "varchar(1)"
//                    , isNullable: false
//                    , checkConstraints:
//                    new Dictionary<string, ICheckConstraint>
//                    {
//                        [statusCheckName] = new CheckConstraintMutable(
//                            name: statusCheckName
//                            , constraintText: "priorStatus in ( 'U', 'P', 'E', 'W', 'B', 'X', 'S', 'C' )"
//                            )
//                    }
//                    )
//                );
//            table.AddOrOverwriteColumn(new ColumnMutable("erroredJob", "int", isNullable: true));
//            table.AddOrOverwriteColumn(new ColumnMutable("errorDtm", "datetime", isNullable: true));
//            table.AddOrOverwriteCheckConstraint(
//                new CheckConstraintMutable(
//                    sql.TableConstraintName("_ckEJED", table.Name)
//                    , constraintText: "( erroredJob is null and errorDtm is null ) or ( erroredJob is not null and errorDtm is not null )"
//                    )
//                );
//            table.AddOrOverwriteIndex(new IndexMutable(table.Name + "_iJS", owner: null, columnNames: new List<string> { "jobNum", "status" }));
//            table.AddOrOverwriteIndex(new IndexMutable(table.Name + "_iSJDtm", owner: null, columnNames: new List<string> { "status", "jobDtm" }));
//            if (retrieveIDsFromDatabase) table.RetrieveIDsFromDatabase();
//        }
//    }

//    public class QueueJobsTableMutable : TableMutable, IQueueJobsTableMutable
//    {

//        public QueueJobsTableMutable(
//            string name
//            , IDataProcObject owner
//            , ISchema schema = null
//            , string idColumnName = "qJobsTableID"
//            , string idColumnNameWhenPrefixed = null
//            , List<IMappedColumn> keyColumns = null
//            , int? id = null
//            , bool retrieveIDsFromDatabase = false
//            ) : base(
//                name: (name != null ? name : owner.Name + "Jobs")
//                , owner: owner
//                , schema: schema
//                , id: id
//                )
//        {
//            ISqlTranslator sql = Database.SqlTranslator;
//            AddOrOverwriteColumn(
//                new IDColumnMutable(
//                    (idColumnName == null ? "qJobsTableID" : idColumnName)
//                    , nameWhenPrefixed: idColumnNameWhenPrefixed
//                    )
//                  );
//            UtilitiesForQueueJobsTables.AddStandardQueueJobsColumns(this, sql, retrieveIDsFromDatabase: false);
//            SetQueueKeyColumns(keyColumns);
//            SetPrimaryKey(
//                new PrimaryKeyMutable(
//                    sql.TableConstraintName("_pk", Name)
//                    , owner: null
//                    , columnNames: new List<string> { idColumnName }
//                    ));
//            AddOrOverwriteTrigger(new TriggerMutable(name: null, owner: this, afterInsert: true));
//        }

//        public List<IMappedColumn> QueueKeyColumns
//        {
//            get
//            {
//                List<IMappedColumn> retval = new List<IMappedColumn>();
//                foreach (KeyValuePair<string, Tuple<int, IColumn>> pair in Columns.Where(p => typeof(IMappedColumn).IsAssignableFrom(p.Value.Item2.GetType())))
//                    retval.Add((IMappedColumn)pair.Value.Item2);
//                return retval;
//            }
//            set { SetQueueKeyColumns(value, isFullyConstructed: true); }
//        }

//        public List<IMappedColumn> GetQueueKeyColumnsSafely()
//        {
//            List<IMappedColumn> retval = new List<IMappedColumn>();
//            foreach (KeyValuePair<string, Tuple<int, IColumn>> pair in Columns.Where(p => typeof(IMappedColumn).IsAssignableFrom(p.Value.Item2.GetType())))
//                retval.Add((IMappedColumn)pair.Value.Item2.GetSafeReference());
//            return retval;
//        }

//        public void SetQueueKeyColumns(List<IMappedColumn> value)
//        {
//            SetQueueKeyColumns(value, isFullyConstructed: true);
//        }
//        protected void SetQueueKeyColumns(List<IMappedColumn> value, bool isFullyConstructed = true)
//        {
//            if (value == null)
//                foreach (KeyValuePair<string, Tuple<int, IColumn>> pair in Columns.Where(p => typeof(IMappedColumn).IsAssignableFrom(p.Value.Item2.GetType())))
//                    RemoveColumn(pair.Key);
//            else
//                foreach (IMappedColumn c in value)
//                    AddOrOverwriteColumn(c);
//        }
//    }


//    public class QueueJobsTableImmutable : TableImmutable, IQueueJobsTable
//    {
//        private IQueueJobsTable Delegee { get { return (IQueueJobsTable)_delegee; } }

//        //Disable default constructor
//        private QueueJobsTableImmutable() : base(delegee: null) { }

//        public QueueJobsTableImmutable(
//            string name
//            , IDataProcObject owner
//            , ISchema schema = null
//            , string idColumnName = "qJobsTableID"
//            , string idColumnNameWhenPrefixed = null
//            , List<IMappedColumn> keyColumns = null
//            , int? id = null
//            , bool retrieveIDsFromDatabase = false
//            ) : base(delegee: null)
//        {
//            _delegee =
//                new QueueJobsTableMutable(
//                    name
//                    , owner
//                    , schema
//                    , idColumnName
//                    , idColumnNameWhenPrefixed
//                    , keyColumns
//                    , id
//                    , retrieveIDsFromDatabase
//                    );
//            CompleteConstructionIfType(typeof(QueueJobsTableImmutable), retrieveIDsFromDatabase: false);
//        }

//        public QueueJobsTableImmutable(IQueueJobsTable delegee) : base(delegee: null)
//        {
//            _delegee = delegee;
//            CompleteConstructionIfType(typeof(QueueJobsTableImmutable), retrieveIDsFromDatabase: false);
//        }

//        // IQueueJobsTable properties and methods
//        public List<IMappedColumn> QueueKeyColumns { get { return Delegee.QueueKeyColumns; } }
//        public List<IMappedColumn> GetQueueKeyColumnsSafely() { return Delegee.GetQueueKeyColumnsSafely(); }

//    }

//}