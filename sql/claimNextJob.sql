if exists ( select * from sys.procedures where name = 'claimNextJob' and object_schema_name(object_id) = 'vtsQs' )
drop procedure [vtsQs].[claimNextJob]
go
create procedure [vtsQs].[claimNextJob]
    @queueID            int
  , @jobNum             int output
  , @qi                 int
  
--  , @keyColumn          nvarchar(100)
  , @keyColumnJoin      nvarchar(1000)
  , @keyColumSelect     nvarchar(1000)
  , @keyColumSelectJ    nvarchar(1000)
  , @keyColumTable      nvarchar(1000)
as
begin

 -- set nocount, ansi_defaults, xact_abort on;
  -- Not callable from within nested transaction
  while @@TRANCOUNT > 0 commit transaction;
  declare @jobsVableName      sysname  ;
  declare @qiTable            nvarchar(100);
  declare @logTable           nvarchar(100);
/*
  declare @keyColType         varchar(16);
  declare @keyColumnJoin      nvarchar(1000);
  declare @keyColumSelect     nvarchar(1000);
  declare @keyColumSelectJ    nvarchar(1000);
  declare @keyColumTable      nvarchar(1000);   */
  declare @priorityKeyColumns nvarchar(500);    
  declare @isAuditing         tinyint;
  declare @jobsType           varchar(1) = 'U';
  declare @num                varchar(100);
  
  declare @sql                nvarchar(max) = null;
  declare @procSize           varchar(10);
  declare @logRows            varchar(10);
  declare @UNCLAIMED          varchar(1) = 'U';
  declare @PROCESSING         varchar(1) = 'P';
  declare @ERRORED            varchar(1) = 'E';
  declare @EXCLUDED           varchar(1) = 'X';

  begin try    
    --exec vtsQs.logFromQueue @queueID, null, @qi, 'begin claimNextJob';
    
    select @jobsVableName      = jobsVableName
         , @qiTable            = qisVableName
         , @procSize           = cast(targetjobsize as varchar(10))
         , @logRows            = cast(maxrowsinlog as varchar(10))
         , @logTable           = logName
         , @isAuditing         = jobsVableIsAudited
         , @priorityKeyColumns = priorityKeyColumns
    from vtsQs.queues  
    where id =  @queueID;
	while @@TRANCOUNT > 0 commit transaction;
    --exec vtsQs.logFromQueue @logTable, null, @qi, 'claimNextJob: got queue config 1';
    exec vtsQs.logFromQueue @logTable = @logTable, @jobNum = 0, @qi = @qi, @msg = 'claimNextJob: got queue config 1';
    
    -- clean the dead jobs
    exec dropTempTableIfExists '#tempQis';
    create table #tempQis (id int null);
    set @sql = 'insert into #tempQis '
             + 'select id from ' + @qiTable 
             + ' where queueID = @queueID' 
             + ' and id != @qi' 
             + ' and dateadd(minute, acceptableDelayInMinutes, lastHeardFrom) < getdate()'
             ;
    execute sp_executeSQL @sql, N'@qi int, @queueID int', @qi = @qi, @queueID = @queueID;
	while @@TRANCOUNT > 0 commit transaction;
    exec vtsQs.logFromQueue @logTable = @logTable, @jobNum = 0, @qi = @qi, @msg = 'claimNextJob: got dead queues';
    
    if exists (select top 1 * from #tempQis) begin
        set @sql = 'update j set j.owner = null, j.status = ''U'' '
                 + 'from ' + @jobsVableName
                 + ' j join #tempQis t on t.id = j.owner'
                 ;
        execute sp_executeSQL @sql;
        while @@TRANCOUNT > 0 commit transaction;
        exec vtsQs.logFromQueue @logTable = @logTable, @jobNum = 0, @qi = @qi, @msg = 'claimNextJob: updated dead jobs';
        
        set @sql = 'delete q from ' + @qiTable + ' q join #tempQis t on t.id = q.id';
        execute sp_executeSQL @sql;
        while @@TRANCOUNT > 0 commit transaction;
        exec vtsQs.logFromQueue @logTable = @logTable, @jobNum = 0, @qi = @qi, @msg = 'claimNextJob: deleted dead instances';
    end;
    drop table #tempQis;
    
   /*
    -- get jobs table key columns
    declare cs cursor for
    select keyColumnName, keyColumnType
    from vtsQs.queueKeyColumns
    where queueID = @queueID
    ;
    open cs
    fetch next from cs into @keyColumn, @keyColType;
    if @@FETCH_STATUS = 0 begin
        set @keyColumnJoin = 'j.' + @keyColumn + ' = t.' + @keyColumn;
        set @keyColumSelect = @keyColumn;
        set @keyColumSelectJ = 'j.' + @keyColumn;
        set @keyColumTable = @keyColumn + ' ' + @keyColType + ' null';
        fetch next from cs into @keyColumn, @keyColType;
    end;
    
    while @@FETCH_STATUS = 0 begin
        set @keyColumnJoin = @keyColumnJoin + ' and j.' + @keyColumn + ' = t.' + @keyColumn;
        set @keyColumSelect = @keyColumSelect + ', ' + @keyColumn;
        set @keyColumSelectJ = @keyColumSelectJ + ', j.' + @keyColumn;
        set @keyColumTable = @keyColumTable + ', ' + @keyColumn + ' ' + @keyColType + ' null';
        fetch next from cs into @keyColumn, @keyColType;
    end;
    close cs;
    deallocate cs;
	while @@TRANCOUNT > 0 commit transaction;
    exec vtsQs.logFromQueue @logTable, null,  @qi, 'claimNextJob: got queue config 2';
    
    if not @priorityKeyColumns is null begin
        declare @c  nvarchar(100);
		declare cs1 cursor for
        select priorityKeyColumnName
        from vtsQs.additionalQueuePriorityKeyColumns 
        where queueID = @queueID
        ;
        open cs1  
        fetch next from cs1 into @c;
        while @@FETCH_STATUS = 0 begin
            set @priorityKeyColumns = @priorityKeyColumns + ',' + @c;
            fetch next from cs1 into @c;
        end;
        close cs1;
        deallocate cs1;
        while @@TRANCOUNT > 0 commit transaction;
        exec vtsQs.logFromQueue @logTable, null,  @qi, 'claimNextJob: got queue config 3';
    end;
   */
    -- handle X jobs
    exec dropTempTableIfExists '#tempExclJobs';
    create table #tempExclJobs (nonsense bit);
    set @sql = 'alter table #tempExclJobs add ' + @keyColumTable;
    execute sp_executeSQL @sql;
    alter table #tempExclJobs drop column nonsense;
    
    set @sql = 'insert into #tempExclJobs select distinct ' + @keyColumSelect
                + ' from ' + @jobsVableName 
                + ' where status = ''X'''
    ;
    execute sp_executeSQL @sql;
	while @@TRANCOUNT > 0 commit transaction;
    exec vtsQs.logFromQueue @logTable = @logTable, @jobNum = 0, @qi = @qi, @msg = 'claimNextJob: got X jobs';
    exec vtsQs.qiPing @qi = @qi, @qiTable = @qiTable;    
	
    if exists (select top 1 * from #tempExclJobs) begin
        set @sql = 'update j set j.status = ''X'' '
                 + 'from ' + @jobsVableName + ' j join #tempExclJobs t on '
                 + @keyColumnJoin
                 + ' where j.status != ''X'' and j.owner is null'
                 ;
        execute sp_executeSQL @sql;
        while @@TRANCOUNT > 0 commit transaction;
        exec vtsQs.logFromQueue @logTable = @logTable, @jobNum = 0, @qi = @qi, @msg = 'claimNextJob: updated X jobs';
        exec vtsQs.qiPing @qi = @qi, @qiTable = @qiTable;    
    end;
	drop table #tempExclJobs;
	
    -- get procSize jobs         
    exec dropTempTableIfExists '#tempProcJobs0';
    create table #tempProcJobs0 (id  int not null);
    set @sql = 'alter table #tempProcJobs0 add ' + @keyColumTable;
    execute sp_executeSQL @sql;
    
    set @sql = 'insert into #tempProcJobs0 select top ' + @procSize + ' max(id)  
                , ' + @keyColumSelect
                + ' from ' + @jobsVableName 
                + ' where owner is null
                    and status in ( ''U'', ''P'') group by ' + @keyColumSelect
    ;
    if not @priorityKeyColumns is null 
        set @sql = @sql + ' order by ' + @priorityKeyColumns;
    --else
    --    set @sql = @sql + ' order by id';

    execute sp_executeSQL @sql;    
	while @@TRANCOUNT > 0 commit transaction;
    exec vtsQs.logFromQueue @logTable = @logTable, @jobNum = 0, @qi = @qi, @msg = 'claimNextJob: got candidates';
    exec vtsQs.qiPing @qi = @qi, @qiTable = @qiTable;    
    
	if not exists (select top 1 * from #tempProcJobs0) begin
	    declare @nPJobs int;
 	    set @sql = 'select @nPJobs = count(*) from (select top 1 * from ' + @jobsVableName 
 	             + ' where owner is not null and status = ''P'') t ';
 	    execute sp_executeSQL @sql, N'@nPJobs int output', @nPJobs = @nPJobs output;
        while @@TRANCOUNT > 0 commit transaction;
        set @num = 'claimNextJob: E status running = ' + cast(@nPJobs as varchar(1)); 
        exec vtsQs.logFromQueue @logTable = @logTable, @jobNum = 0, @qi = @qi, @msg = @num;	    
        
 	    if @nPJobs = 0 begin
          -- handle E jobs
            set @jobsType = 'E';
            set @sql = 'select @jobNum = jobNum from (select top 1 * from ' + @jobsVableName 
                     + ' where owner is null
                           and status = ''E'') t
                       ';
            execute sp_executeSQL @sql, N'@jobNum int output', @jobNum = @jobNum output;
            while @@TRANCOUNT > 0 commit transaction;
            exec vtsQs.logFromQueue @logTable = @logTable, @jobNum = 0, @qi = @qi, @msg = 'claimNextJob: got E jobNum';
            
            if not @jobNum is null begin
                set @sql = 'insert into #tempProcJobs0 select top ' + @procSize + '
                             id, ' + @keyColumSelect
                                 + ' from ' + @jobsVableName 
                                 + '  where owner is null
                                      and jobNum = @jobNum
                                      and status = ''E'''
                ;
                execute sp_executeSQL @sql, N'@jobNum int', @jobNum = @jobNum;    
                while @@TRANCOUNT > 0 commit transaction;
                exec vtsQs.logFromQueue @logTable = @logTable, @jobNum = 0, @qi = @qi, @msg = 'claimNextJob: got E candidates';
                exec vtsQs.qiPing @qi = @qi, @qiTable = @qiTable;    
            end;
        end;
 	end;
 	
 	if not exists (select top 1 * from #tempProcJobs0) begin
        set @jobNum = -1;
        while @@TRANCOUNT > 0 commit transaction;
        exec vtsQs.logFromQueue @logTable = @logTable, @jobNum = 0, @qi = @qi, @msg = 'claimNextJob: no candidates - return';
        return 0;
 	end;
    
  	-- get the most recent job for each key
    exec dropTempTableIfExists '#tempProcJobs';
    create table #tempProcJobs (id  int not null);
    set @sql = 'alter table #tempProcJobs add ' + @keyColumTable;
    execute sp_executeSQL @sql;
 	
 	if @jobsType = 'U'  begin
        set @sql = 'insert into #tempProcJobs
                    select id, ' + @keyColumSelect
                 + ' from #tempProcJobs0'
                 ;
        execute sp_executeSQL @sql;
        while @@TRANCOUNT > 0 commit transaction;
        exec vtsQs.logFromQueue @logTable = @logTable, @jobNum = 0, @qi = @qi, @msg = 'claimNextJob: got #tempProcJobs';
        
        truncate table #tempProcJobs0;
        while @@TRANCOUNT > 0 commit transaction;
        exec vtsQs.qiPing @qi = @qi, @qiTable = @qiTable;
        
        set @sql = 'insert into #tempProcJobs0 
                    select j.id, ' + @keyColumSelectJ 
                 + ' from ' + @jobsVableName + ' j with(nolock) join #tempProcJobs t on ' + @keyColumnJoin
                 + ' where j.owner is null 
                      and  j.id != t.id
                   ';
        execute sp_executeSQL @sql;    
        while @@TRANCOUNT > 0 commit transaction;
        exec vtsQs.logFromQueue @logTable = @logTable, @jobNum = 0, @qi = @qi, @msg = 'claimNextJob: got all the records with the same key';
        exec vtsQs.qiPing @qi = @qi, @qiTable = @qiTable;   
 	end;
 	else  begin
        -- get the all jobs for the seleted keys
        set @sql = 'insert into #tempProcJobs0 
                    select j.id, ' + @keyColumSelectJ 
                 + ' from ' + @jobsVableName + ' j with(nolock) join #tempProcJobs0 t on ' + @keyColumnJoin
                 + ' where j.owner is null 
                      and  j.id != t.id
                   ';
        execute sp_executeSQL @sql;    
        while @@TRANCOUNT > 0 commit transaction;
        exec vtsQs.logFromQueue @logTable = @logTable, @jobNum = 0, @qi = @qi, @msg = 'claimNextJob: got all the records with the same key';
        exec vtsQs.qiPing @qi = @qi, @qiTable = @qiTable;    

        set @sql = 'insert into #tempProcJobs
                    select max(id) id, ' + @keyColumSelect
                 + ' from #tempProcJobs0'
                 + ' group by ' + @keyColumSelect
                 ;
        execute sp_executeSQL @sql;
        while @@TRANCOUNT > 0 commit transaction;
        exec vtsQs.logFromQueue @logTable = @logTable, @jobNum = 0, @qi = @qi, @msg = 'claimNextJob: got #tempProcJobs';
        exec vtsQs.qiPing @qi = @qi, @qiTable = @qiTable;    

        -- #tempProcJobs0 is only for the old jobs
        delete t0
        from #tempProcJobs0 t0 left join #tempProcJobs t
          on t0.id = t.id
        where t.id is not null
        ;
        while @@TRANCOUNT > 0 commit transaction;
        exec vtsQs.logFromQueue @logTable = @logTable, @jobNum = 0, @qi = @qi, @msg = 'claimNextJob: got #tempProcJobs0 records to be removed';
    end;
    
    select @num = cast(count(*) as varchar(100)) from #tempProcJobs;
	while @@TRANCOUNT > 0 commit transaction;
	set @num = 'claimNextJob: before removing #tempProcJobs owned by others: ' + @num;
    exec vtsQs.logFromQueue @logTable = @logTable, @jobNum = 0, @qi = @qi, @msg = @num;

    
    -- delete if any selected is now with another one
    set @sql = 'delete t from #tempProcJobs t join ' + @jobsVableName
             + ' j on j.id = t.id and j.owner is not null'
     ;
    execute sp_executeSQL @sql;
	while @@TRANCOUNT > 0 commit transaction;
    exec vtsQs.qiPing @qi = @qi, @qiTable = @qiTable;  
    
    if exists (select top 1 * from #tempProcJobs) begin
        -- update physcal jobs for the seleted 
        set @jobNum = rand() * 100000000;
        --execute vtsQs.acquireNewQueueJobNum 0, @jobNum    output;
        set @sql = '
        update j set j.jobNum = @jobNum, j.owner = @qi, j.priorStatus   = j.status, j.status = ''P''
        from ' + @jobsVableName + ' j join 	#tempProcJobs t 
        on t.id = j.id
        where j.owner is null'
        ;
        execute sp_executeSQL @sql
          , N'@jobNum      int        
            , @qi          int'
          ,   @jobNum      = @jobNum  
          ,   @qi          = @qi
        ;
        while @@TRANCOUNT > 0 commit transaction;
        exec vtsQs.logFromQueue @logTable = @logTable, @jobNum = 0, @qi = @qi, @msg = 'claimNextJob: updated the jobs to be processed';
 
        set @sql = 'select @num = cast(count(*) as varchar(100)) from ' + @jobsVableName + ' where owner = ' + cast(@qi as varchar(8));
        execute sp_executeSQL @sql, N'@num varchar(100) output', @num = @num output;
        while @@TRANCOUNT > 0 commit transaction;
        set @num = 'claimNextJob: after removing #tempProcJobs owned by others: ' + @num;
        exec vtsQs.logFromQueue @logTable = @logTable, @jobNum = 0, @qi = @qi, @msg = @num;
        
        set @sql = 'update ' + @jobsVableName + ' set jobNum = 0 where jobNum = @jobNum and owner is null';  
        execute sp_executeSQL @sql, N'@jobNum  int ', @jobNum  = @jobNum;
        while @@TRANCOUNT > 0 commit transaction;
    end;
    else begin
        set @jobNum = -1;
        while @@TRANCOUNT > 0 commit transaction;
        exec vtsQs.logFromQueue @logTable = @logTable, @jobNum = 0, @qi = @qi, @msg = 'claimNextJob: recs are all with other qis - return';
    end; 
    drop table #tempProcJobs; 
    exec vtsQs.qiPing @qi = @qi, @qiTable = @qiTable;  

    -- delete the old jobs from the physical jobs table
    if exists (select top 1 * from #tempProcJobs0) begin
        --exec createTempTableIndex '#tempProcJobs0_i', '#tempProcJobs0', 'id', null, null;
        create index #tempProcJobs0_i on #tempProcJobs0 (id);
        if @isAuditing = 1 begin
            set @sql = 'update j set j.status = ''S'' '
                     + 'from ' + @jobsVableName
                     + ' j join #tempProcJobs0 t on j.id = t.id'
                     + ' where j.owner is null'
                     ;                              
            execute sp_executeSQL @sql;
            while @@TRANCOUNT > 0 commit transaction;
            exec vtsQs.logFromQueue @logTable = @logTable, @jobNum = 0, @qi = @qi, @msg = 'claimNextJob: set old duplicates to S';
        end;
        
        set @sql = 'delete j from ' + @jobsVableName
                 + ' j join #tempProcJobs0 t on j.id = t.id'
                 + ' where j.owner is null'
        ;            
        execute sp_executeSQL @sql;
        while @@TRANCOUNT > 0 commit transaction;
        exec vtsQs.logFromQueue @logTable = @logTable, @jobNum = 0, @qi = @qi, @msg = 'claimNextJob: removed old duplicates';
    end;
    drop table #tempProcJobs0;
    
    -- delete log
    exec dropTempTableIfExists '#tempProclog';
    create table #tempProclog (id  int not null);
    set @sql = 'insert into #tempProclog '
             + 'select top ' + @logRows + ' id from ' + @logTable
             + ' order by id desc';
    execute sp_executeSQL @sql;
    while @@TRANCOUNT > 0 commit transaction;
    set @sql = 'delete l from ' + @logTable 
             + ' l left join #tempProclog t on t.id = l.id'
             + ' where t.id is null';
    execute sp_executeSQL @sql;
    while @@TRANCOUNT > 0 commit transaction;
    exec vtsQs.logFromQueue @logTable = @logTable, @jobNum = 0, @qi = @qi, @msg = 'claimNextJob: removed old log recs';     
    drop table #tempProclog;
    
    return 0;
  end try
    begin catch
      if XACT_STATE() != 0 rollback transaction;
      declare @cnt int;
      set @sql = 'select @cnt = count(*) from (select top 1 * from ' + @jobsVableName + ' where owner = ' + cast(@qi as varchar(8)) + '  and status = ''P'') t';
      execute sp_executeSQL @sql, N'@cnt int output', @cnt = @cnt output;
      if @cnt > 0 begin
        set @sql = 'update ' + @jobsVableName + ' set owner = null, status = ''U'' '
                 + ' where owner = ' + cast(@qi as varchar(8)) 
                  ;
        execute sp_executeSQL @sql;
        while @@TRANCOUNT > 0 commit transaction;
        exec vtsQs.logFromQueue @logTable = @logTable, @jobNum = 0, @qi = @qi, @msg = 'claimNextJob: catch, roll back'; 
        
        exec vtsQs.killQI @qi = @qi, @qiTable = @qiTable; 
      end;
      while @@TRANCOUNT > 0 commit transaction;
     -- exec rethrowCurrentError;
      raisError('claimNextJob error', 16, 1) ;
      return 1
    end catch
end
go

if exists ( select * from sys.procedures where name = 'handleErroredJobs' and object_schema_name(object_id) = 'vtsQs' )
drop procedure [vtsQs].[handleErroredJobs]
go
create procedure vtsQs.handleErroredJobs
    @queueID            int
  , @jobNum             int 
  , @qi                 int
  , @jobsVableName      nvarchar(100)
as
begin
    while @@TRANCOUNT > 0 commit transaction;
    exec vtsQs.qiPing @qi = @qi, @queueID = @queueID;   
    
    declare @totalRecs int, @recs1 int;
    declare @statusError varchar(1) = 'E';
    declare @sql nvarchar(max) = null;
    declare @msg nvarchar(1000);
    
    begin try
    
		set @sql = 'select @totalRecs = count(*) from ' + @jobsVableName
				 + ' where owner  = @qi and jobNum = @jobNum';
		execute sp_executeSQL @sql, N'@totalRecs int output, @qi int, @jobNum int', @totalRecs = @totalRecs output, @qi = @qi, @jobNum = @jobNum;
		while @@TRANCOUNT > 0 commit transaction;
		set @msg = 'handleErroredJobs: got @totalRecs = ' + cast(@totalRecs as varchar(8));
    exec vtsQs.logFromQueue @queueID = @queueID, @jobNum = @jobNum, @qi = @qi, @msg = @msg;
		exec vtsQs.qiPing @qi = @qi, @queueID = @queueID;  
		
		if @totalRecs = 0 return;
	         
		if @totalRecs = 1 
			set @statusError = 'X';
		else begin
		   declare @newJobNum  int;
		   --exec acquireNewQueueJobNum @queueID, @newJobNum output;
       set @newJobNum = rand() * 100000000;
	       
		   set @recs1 = floor(@totalRecs / 2);
	        
		   exec dropTempTableIfExists '#tempErrorIDs';
		   create table #tempErrorIDs (id int not null);
		   while @@TRANCOUNT > 0 commit transaction;
       exec vtsQs.logFromQueue @queueID = @queueID, @jobNum = @jobNum, @qi = @qi, @msg = 'handleErroredJobs: created #tempErrorIDs';
	       
		   set @sql = 'insert into #tempErrorIDs'
					+ ' select top ' + cast(@recs1 as varchar(10)) + ' id from ' + @jobsVableName
					+ ' where owner  = @qi and jobNum = @jobNum'
			;
		   execute sp_executeSQL @sql, N'@qi int, @jobNum int', @qi = @qi, @jobNum = @jobNum;
		   while @@TRANCOUNT > 0 commit transaction;
       exec vtsQs.logFromQueue @queueID = @queueID, @jobNum = @jobNum, @qi = @qi, @msg = 'handleErroredJobs: inserted into #tempErrorIDs';
		   exec vtsQs.qiPing @qi = @qi, @queueID = @queueID; 
	       
		   set @sql = 'update j set j.jobNum = @newJobNum
					   , j.owner         = null
					   , j.status        = ''E''
					   , j.priorStatus   = status
					   , j.erroredJob    = j.jobNum
					   , j.errorDtm      = current_timestamp
		   from ' + @jobsVableName + ' j join #tempErrorIDs t on j.id = t.id';
		   execute sp_executeSQL @sql, N'@newJobNum int', @newJobNum = @newJobNum;   
		   drop table #tempErrorIDs;
		   while @@TRANCOUNT > 0 commit transaction;
		   set @msg = 'handleErroredJobs: updated ' + @jobsVableName + ' for top ' + cast(@recs1 as varchar(8));
       exec vtsQs.logFromQueue @queueID = @queueID, @jobNum = @jobNum, @qi = @qi, @msg = @msg;
		   exec vtsQs.qiPing @qi = @qi, @queueID = @queueID;   
		end;
	       
		set @sql = 'update ' + @jobsVableName 
				 + ' set owner = null
			 , status        = ''' + @statusError + '''
			 , priorStatus   = status
			 , erroredJob    = jobNum
			 , errorDtm      = current_timestamp
		 where owner  = @qi and jobNum = @jobNum'
		;
		execute sp_executeSQL @sql, N'@qi int, @jobNum int', @qi = @qi, @jobNum = @jobNum;
		while @@TRANCOUNT > 0 commit transaction;
		set @msg = 'handleErroredJobs: finished updating ' + @jobsVableName
    exec vtsQs.logFromQueue @queueID = @queueID, @jobNum = @jobNum, @qi = @qi, @msg = @msg;
	end try
	begin catch
		if XACT_STATE() != 0 rollback transaction;
		while @@TRANCOUNT > 0 commit transaction;
		--exec rethrowCurrentError;
    raisError('handleErroredJobs error', 16, 1) ;
	end catch
end
go
