﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Com.VeritasTS.DatabaseUtilityInterfaces;

namespace Com.VeritasTS.DbUtilitiesSqlServer
{
    public class TriggerLauncher
    {


        [Microsoft.SqlServer.Server.SqlProcedure]
        public static void SP_LaunchTrigger( string triggerNameWithSchema )
        {
#if !DEBUG
        SqlPipe sp = SqlContext.Pipe;
        sp.Send("Insert " + tableName + ", temptable = " + tempTable);
#endif
            using (SqlConnection rawConn = new SqlConnection("context connection=true"))
            {
                rawConn.Open();
                if (
                    new SqlCommand("select top(1) 1 from inserted", rawConn)
                    .ExecuteReader()
                    .HasRows
                    )
                {

                    ISqlTranslator sqlTranslator = new SqlTranslatorSqlServer2014();
                    IConnection conn = new ConnectionSqlServer(rawConn);
                    IDatabaseSafe db
                        = new DatabaseSafeMutable(
                            conn.DatabaseName()
                            , null
                            , null
                            , sqlTranslator
                            );
                    ITriggerClassRegistry tcr = db.TriggerClassRegistry;
                    ITableTrigger trigger = tcr.Trigger(triggerNameWithSchema, conn);
                    if (trigger == null)
                    {
#if !DEBUG
            sp.Send("CreateInstance: can not create the trigger object");
#endif
                        throw (new Exception("can not create the trigger object"));
                    }
                    trigger.ExecuteLogic(conn);
                }
            }
        }
}
