﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Com.VeritasTS.TextAndErrorUtils;
using Com.VeritasTS.DatabaseUtilityInterfaces;


namespace Com.VeritasTS.DbUtilitiesSqlServer
{
    public class SqlTranslatorSqlServer2014 : ISqlTranslator
    {

        public SqlTranslatorSqlServer2014() { }

        /*** 
         * ISafeMemberOfImmutableObject methods
         ***/
        public ISafeMemberOfImmutableOwner GetSafeReference() { return this; }

        /*** 
         * ISelfAsserter methods
         ***/
        public bool PassesSelfAssertion(Type testableType = null) { return true; }

        /*** 
         * DatabaseImmutable manipulation methods
         ***/

        public IVendor Vendor { get { return Vendors.SQLServer(); } }
        public bool UsesForEachRowTriggers { get { return false; } }
        public bool RequiresIdentityColumn { get { return true; } }
        public string SingletonVableSql { get { return "(select 1 as x)"; } }
        public bool SupportsInMemoryTempTables { get { return true; } }

        public string TempTableName(
            string tempTableNameBase
            , bool globallyAvailable
            , bool inMemoryIfPossible
            )
        {
            if (globallyAvailable)
                return "##" + tempTableNameBase;
            else if (inMemoryIfPossible)
                return "@" + tempTableNameBase;
            else
                return "#" + tempTableNameBase;
        }

        public string TempTableCreateSqlSnip(
            string tempTableName
            , bool globallyAvailable
            , bool inMemoryIfPossible
            )
        {
            if (globallyAvailable)
            {
                Debug.Assert(
                    tempTableName.Substring(0, 2) == "##"
                    , "A globally available temporary table in SQL Server 2014 "
                    + "must have a name beginning with ##."
                    );
                return "create table " + tempTableName;
            }
            else if (inMemoryIfPossible)
            {
                Debug.Assert(
                    tempTableName.Substring(0, 2) == "##"
                    , "An in-memory temporary table in SQL Server 2014 "
                    + "must have a name beginning with @."
                    );
                return "declare " + tempTableName + " table";
            }
            else
            {
                Debug.Assert(
                    tempTableName.Substring(0, 1) == "#"
                    && tempTableName.Substring(1,1) != "#"
                    , "An in-memory temporary table in SQL Server 2014 "
                    + "must have a name beginning with # but not ##."
                    );
                return "create table " + tempTableName;
            }
        }

        /// <summary>
        /// Returns the SQL statement needed to find the ID
        /// of the database with the specified name, living
        /// on the specified server. 
        /// </summary>
        /// <remarks>
        /// <para>
        /// If the server is null, then the assumption is that
        /// the database is located on the server that is
        /// local to the connection on which the SQL is executed.
        /// </para>
        /// </remarks>
        /// <param name="name">The name of the database.</param>
        /// <param name="server">The server on which the database
        /// should live. If null, the implication is that the
        /// database should live on the server that hosts
        /// the connection on which the SQL is executed.</param>
        /// <returns>The ID of the database if it exists, null
        /// if it does not exist.</returns>
        public List<string> IDOfDatabaseSql(string name, IServer server = null, int spacesToIndent = 0, bool terminatesStatement = true)
        {
            string retval;
            if (name == null)
                throw new ArgumentNullException(
                    paramName: "name"
                    , message: ""
                    + "Cannot provide null as 'name' argument to "
                    + this.GetType().FullName
                    + ".IDOfDatabaseSql(name, server, spacesToIndent, terminatesStatement)."
                    );
            string leadingSpaces = new string(' ', spacesToIndent);
            if (name == null) retval = leadingSpaces + "select cast( null as int )";
            if (server == null) retval = leadingSpaces + "select DB_ID('" + name + "')";
            else if (server.Name == null) retval = leadingSpaces + "select DB_ID('" + name + "')";
            else
            {
                string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
                retval = ""
                    + lfAndLeadingSpaces + "select min( id ) "
                    + lfAndLeadingSpaces + "  from " + server.Name + ".sys.databases "
                    + lfAndLeadingSpaces + " where name = '" + SqlStringLiteral(name)
                    ;
                if (terminatesStatement)
                    retval += lfAndLeadingSpaces;
            }
            if (terminatesStatement)
                retval += ";";
            return ListFromString( retval );
        }
        private string EnsureDatabaseExistsSqlString(IDatabase database, int spacesToIndent = 0)
        {
            string retval;
            if (database == null)
                throw new ArgumentNullException(
                    paramName: "database"
                    , message: ""
                    + "Cannot provide null as 'database' argument to "
                    + this.GetType().FullName
                    + ".EnsureDatabaseExistsSql(database)."
                    );
            else if (database.Name == null)
                throw new ArgumentNullException(
                    paramName: "database.Name"
                    , message: ""
                    + "Cannot provide database with null Name property as 'database' argument to "
                    + this.GetType().FullName
                    + ".EnsureDatabaseExistsSql(database)."
                    );
            else
            {
                string nameSql = SqlStringLiteral(database.Name);
                string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
                retval = ""
                    + lfAndLeadingSpaces + "if DB_ID( " + nameSql + " ) is null "
                    + lfAndLeadingSpaces + "  begin "
                    + lfAndLeadingSpaces + "    create database " + nameSql + ";"
                    + lfAndLeadingSpaces + "  end ";
            }
            return retval;
        }
        public List<string> EnsureDatabaseExistsSql(IDatabase database, int spacesToIndent = 0)
        {
            return ListFromString(EnsureDatabaseExistsSqlString(database, spacesToIndent));
        }
        public List<string> DropDatabaseIfExistsSql(IDatabase database, int spacesToIndent = 0)
        {
            string retval =
                EnsureDatabaseExistsSqlString(database, spacesToIndent)
                .Replace(") is null", ") is not null")
                .Replace("create database", "drop database")
                ;
            return ListFromString( retval );
        }

        // Types
        public string TypeSql( ISqlDataType theType )
        {
            Type t = theType.GetType();
            if (typeof(ISqlBoolean).IsAssignableFrom(t))
                return "bit";
            if (typeof(ISqlTinyint).IsAssignableFrom(t))
                return "tinyint";
            if (typeof(ISqlSmallint).IsAssignableFrom(t))
                return "smallint";
            if (typeof(ISqlIntType).IsAssignableFrom(t))
                return "int";
            if (typeof(ISqlDecimal).IsAssignableFrom(t))
            {
                ISqlDecimal dec = (ISqlDecimal)theType;
                return "decimal(" + dec.Precision + ", " + dec.Scale + ")";
            }
            if (typeof(ISqlDate).IsAssignableFrom(t))
                return "date";
            if (typeof(ISqlTime).IsAssignableFrom(t))
                return "time";
            if (typeof(ISqlDateTime).IsAssignableFrom(t))
                return "datetime";
            if (typeof(ISqlReal).IsAssignableFrom(t))
                return "real";
            if (typeof(ISqlFloat).IsAssignableFrom(t))
                return "float";
            if (typeof(ISqlNVarChar).IsAssignableFrom(t))
            {
                ISqlNVarChar stringType = (ISqlNVarChar)theType;
                return "nvarchar(" + stringType.MaxLength + ")";
            }
            if (typeof(ISqlNChar).IsAssignableFrom(t))
            {
                ISqlNChar stringType = (ISqlNChar)theType;
                return "nchar(" + stringType.MaxLength + ")";
            }
            if (typeof(ISqlVarChar).IsAssignableFrom(t))
            {
                ISqlVarChar stringType = (ISqlVarChar)theType;
                return "varchar(" + stringType.MaxLength + ")";
            }
            if (typeof(ISqlChar).IsAssignableFrom(t))
            {
                ISqlChar stringType = (ISqlChar)theType;
                return "char(" + stringType.MaxLength + ")";
            }
            if (typeof(ISqlBlob).IsAssignableFrom(t))
                return "varbinary(max)";
            if (typeof(ISqlClob).IsAssignableFrom(t))
                return "varchar(max)";
            if (typeof(ISqlNClob).IsAssignableFrom(t))
                return "nvarchar(max)";
            throw new BadSqlTypeException(theType);
        }

        // CheckConstraint
        private void ValidateCheckConstraintAsArgument(ICheckConstraint constraint, string callingMethod)
        {
            ValidateTableElementAsArgument(
                constraint
                , "check constraint"
                , "constraint"
                , callingMethod + "(constraint, spacesToIndent)"
                );
        }
        public List<string> IDOfCheckConstraintSql(
            string name
            , string tableName
            , string schemaName = null
            , IDatabase database = null
            , int spacesToIndent = 0
            , bool terminatesStatement = true
            )
        {
            ValidateArgumentsOfVableElementIDFunction(name, tableName, "tableName", "IDOfCheckConstraintSql(name, tableName, schema, spacesToIndent, terminatesStatement)");
            string retval;
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            schemaName = Coalesce(schemaName, "dbo");
            string schemaLiteral = SqlStringLiteral( Coalesce( schemaName, "dbo" ) );
            string databaseFullNameWithDot = FullNameOf( database );
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            retval = ""
                + lfAndLeadingSpaces + "select min( id ) "
                + lfAndLeadingSpaces + "  from " + databaseFullNameWithDot + "sys.check_constraints "
                + lfAndLeadingSpaces + " where name = " + SqlStringLiteral(name)
                + lfAndLeadingSpaces + "   and parent_object_id = object_id( " + SqlStringLiteral(tableName) + " ) "
                + lfAndLeadingSpaces + "   and schema_id = schema_id( " + SqlStringLiteral(schemaName) + " ) "
                ;
            if (terminatesStatement)
                retval += lfAndLeadingSpaces + ";";
            return ListFromString(retval);
        }
        public List<string> EnsureCheckConstraintExistsSql(ICheckConstraint constraint, int spacesToIndent = 0)
        {
            ValidateCheckConstraintAsArgument( constraint, "EnsureCheckConstraintExistsSql");
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string nameLiteral = SqlStringLiteral(constraint.Name);
            string tableLiteral = SqlStringLiteral(constraint.Owner.Name);
            string schemaLiteral = SqlStringLiteral(Coalesce(NameOf(constraint.Schema), "dbo"));
            string databaseFullNameWithDot = FullNameOf(constraint.Database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            return ListFromString(
                ""
                + lfAndLeadingSpaces + "if not exists ( "
                + "select top(1) 1 "
                + "  from " + databaseFullNameWithDot + "sys.check_constraints "
                + " where name = " + nameLiteral
                + "   and parent_object_id = object_id( " + tableLiteral + " ) "
                + "   and schema_id = schema_id( " + schemaLiteral + " ) "
                + ") "
                + lfAndLeadingSpaces + "  begin "
                + lfAndLeadingSpaces + "    alter table " + constraint.Owner.FullName
                + lfAndLeadingSpaces + "      add constraint " + constraint.Name
                + lfAndLeadingSpaces + "            check( " + constraint.ConstraintText + " )"
                + lfAndLeadingSpaces + "    ; "
                + lfAndLeadingSpaces + "  end "
                );
        }
        public List<string> EnsureCheckConstraintExistsAsDesignedSql(ICheckConstraint constraint, int spacesToIndent = 0)
        {
            List<string> retval = new List<string>();
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            retval.Add(CommitFullySql());
            retval.Add(DropCheckConstraintIfExistsSqlString(constraint, spacesToIndent));
            // Note that DropCheckConstraintIfExistsSql validates the parameters so we do not have to do that again.
            retval.Add(CommitFullySql());
            retval.Add(
                ""
                + lfAndLeadingSpaces + "alter table " + constraint.Owner.FullName
                + lfAndLeadingSpaces + "  add constraint " + constraint.Name
                + lfAndLeadingSpaces + "        check( " + constraint.ConstraintText + " )"
                + lfAndLeadingSpaces + "; "
                );
            retval.Add(CommitFullySql());
            return retval;
        }
        private string DropCheckConstraintIfExistsSqlString(ICheckConstraint constraint, int spacesToIndent = 0)
        {
            return DropConstraintIfExistsSqlString(constraint, "sys.check_constraints", spacesToIndent);
        }
        public List<string> DropCheckConstraintIfExistsSql(ICheckConstraint constraint, int spacesToIndent = 0)
        {
            return ListFromString(DropCheckConstraintIfExistsSqlString(constraint, spacesToIndent));
        }

        // Column
        public List<string> IDOfColumnSql(
            string name
            , string vableName
            , string schemaName = null
            , IDatabase database = null
            , int spacesToIndent = 0
            , bool terminatesStatement = true
            )
        {
            ValidateArgumentsOfVableElementIDFunction(name, vableName, "vableName", "IDOfTableColumnSql(name, vableName, schema, spacesToIndent, terminatesStatement)");
            string retval;
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            schemaName = Coalesce(schemaName, "dbo");
            string schemaLiteral = SqlStringLiteral(Coalesce(schemaName, "dbo"));
            string databaseFullNameWithDot = FullNameOf(database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            retval = ""
                + lfAndLeadingSpaces + "select min( id ) "
                + lfAndLeadingSpaces + "  from " + databaseFullNameWithDot + "sys.columns "
                + lfAndLeadingSpaces + " where name = " + SqlStringLiteral(name)
                + lfAndLeadingSpaces + "   and object_id = object_id( " + SqlStringLiteral(vableName) + " ) "
                + lfAndLeadingSpaces + "   and schema_id = schema_id( " + SqlStringLiteral(schemaName) + " ) "
                ;
            if (terminatesStatement)
                retval += lfAndLeadingSpaces + ";";
            return ListFromString(retval);
        }
        private void ValidateTableColumnAsArgument(IColumn column, string callingMethod)
        {
            ValidateTableElementAsArgument(
                column
                , "column"
                , "column"
                , callingMethod + "(column, spacesToIndent)"
                );
            if (((IDatabaseObject)column.Owner).ObjectType == null)
                throw new ArgumentNullException(
                    paramName: "column.ObjectType"
                    , message: ""
                    + "Cannot provide column whose Owner has null ObjectType as 'column' argument to "
                    + this.GetType().FullName + "."
                    + callingMethod + "(column, spacesToIndent)."
                    );
            if (((IDatabaseObject)column.Owner).ObjectType.Equals(ObjectTypes.View()))
                throw new BadObjectTypeParameterException(
                    paramName: "column.ObjectType"
                    , badObjectType: ObjectTypes.View()
                    , message: ""
                    + "Cannot provide column whose is a view "
                    + " as 'column' argument to "
                    + this.GetType().FullName + "."
                    + callingMethod + "(column, spacesToIndent). Instead, "
                    + "call EnsureViewExistsAsDesignedSql on the parent view."
                    );
            if (!((IDatabaseObject)column.Owner).ObjectType.Equals(ObjectTypes.Table()))
                throw new BadObjectTypeParameterException(
                    paramName: "column.ObjectType"
                    , badObjectType: ((IDatabaseObject)column.Owner).ObjectType
                    , message: ""
                    + "Cannot provide column whose Owner has ObjectType of "
                    + ((IDatabaseObject)column.Owner).ObjectType.Name.ToString()
                    + " as 'column' argument to "
                    + this.GetType().FullName + "."
                    + callingMethod + "(column, spacesToIndent)."
                    );
        }
        public List<string> IDOfTableColumnSql(
            string name
            , string tableName
            , string schemaName = null
            , IDatabase database = null
            , int spacesToIndent = 0
            , bool terminatesStatement = true
            )
        {
            ValidateArgumentsOfVableElementIDFunction(name, tableName, "tableName", "IDOfTableColumnSql(name, tableName, schema, spacesToIndent, terminatesStatement)");
            string retval;
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            schemaName = Coalesce(schemaName, "dbo");
            string schemaLiteral = SqlStringLiteral(Coalesce(schemaName, "dbo"));
            string databaseFullNameWithDot = FullNameOf(database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            retval = ""
                + lfAndLeadingSpaces + "select min( id ) "
                + lfAndLeadingSpaces + "  from " + databaseFullNameWithDot + "sys.columns "
                + lfAndLeadingSpaces + " where name = " + SqlStringLiteral(name)
                + lfAndLeadingSpaces + "   and object_id = object_id( " + SqlStringLiteral(tableName) + " ) "
                + lfAndLeadingSpaces + "   and schema_id = schema_id( " + SqlStringLiteral(schemaName) + " ) "
                ;
            if (terminatesStatement)
                retval += lfAndLeadingSpaces + ";";
            return ListFromString(retval);
        }
        private string TableColumnSql( ITableColumn column, bool forceNullability = false, string leadingSpaces = "", bool includeDefault = true )
        {
            string retval = column.Name + " ";
            if (!column.IsComputed)
            {
                retval += column.DataType;
                if (column.IsIdentity) retval += " identity(1,1)";
                if (column.IsNullable || forceNullability) retval += " null";
                else retval += " not null";
                IDefaultConstraint dflt = column.DefaultConstraint;
                if (dflt != null && includeDefault)
                {
                    retval += " constraint " + dflt.Name + " default( ";
                    retval += ObjectValueToSql( dflt.ConstraintText, column.DataType);
                    retval += " )";
                }
            }
            else
            {
                string calcFormula = column.CalcFormula.Replace("\n", "\n" + leadingSpaces);
                retval += "as " + calcFormula;
                if (column.IsPersisted)
                {
                    if ( calcFormula.Contains("\n") ) retval += "\n" + leadingSpaces;
                    retval += " persisted";
                }
            }
            return retval;
        }
        private List<string> EnsureNullableTableColumnExistsSql(ITableColumn column, int spacesToIndent = 0, bool ensureCheckConstraintsExist = true)
        {
            List<string> retval = new List<string>();
            ValidateTableColumnAsArgument(column, "EnsureTableColumnExistsSql");
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string nameLiteral = SqlStringLiteral(column.Name);
            string tableLiteral = SqlStringLiteral(column.Owner.Name);
            string schemaLiteral = SqlStringLiteral(Coalesce(NameOf(column.Schema), "dbo"));
            string databaseFullNameWithDot = FullNameOf(column.Database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            retval.Add(
                ""
                + lfAndLeadingSpaces + "if not exists ( "
                + "select top(1) 1 "
                + "  from " + databaseFullNameWithDot + "sys.columns "
                + " where name = " + nameLiteral
                + "   and parent_object_id = object_id( " + tableLiteral + " ) "
                + "   and schema_id = schema_id( " + schemaLiteral + " ) "
                + ") "
                + lfAndLeadingSpaces + "  begin "
                + lfAndLeadingSpaces + "    alter table " + column.Owner.FullName
                + lfAndLeadingSpaces + "      add column " + TableColumnSql(column, false, new string(' ', spacesToIndent + 17 + column.Name.Length))
                + lfAndLeadingSpaces + "    ; "
                + lfAndLeadingSpaces + "  end "
                )
                ;
            foreach (ICheckConstraint c in column.CheckConstraints.Values)
            {
                retval.Add(CommitFullySql());
                foreach( string s in EnsureCheckConstraintExistsSql(c, spacesToIndent))
                    retval.Add(s);
            }
            return retval;
        }
        private List<string> EnsureNonnullableTableColumnExistsSql(ITableColumn column, int spacesToIndent = 0, List<string> nonnullPopulationScript = null, bool ensureCheckConstraintsExist = true)
        {
            ValidateTableColumnAsArgument(column, "EnsureTableColumnExistsSql");
            List<string> retval = EnsureNullableTableColumnExistsSql(column, spacesToIndent, ensureCheckConstraintsExist);
            retval.Add(CommitFullySql());
            foreach (string s in nonnullPopulationScript) retval.Add(s);
            retval.Add(CommitFullySql());
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string nameLiteral = SqlStringLiteral(column.Name);
            string tableLiteral = SqlStringLiteral(column.Owner.Name);
            string schemaLiteral = SqlStringLiteral(Coalesce(NameOf(column.Schema), "dbo"));
            string databaseFullNameWithDot = FullNameOf(column.Database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            retval.Add(
                ""
                + lfAndLeadingSpaces + "alter table " + column.Owner.FullName
                + lfAndLeadingSpaces + "alter column " + TableColumnSql(column, false, new string(' ', spacesToIndent + 13 + column.Name.Length ) ) + ";"
                );
            return retval;
        }
        private List<string> EnsureColumnExistsSqlAsList(ITableColumn column, int spacesToIndent = 0, List<string> nonnullPopulationScript = null, bool ensureCheckConstraintsExist = true)
        {
            if (column.IsNullable) return EnsureNullableTableColumnExistsSql(column, spacesToIndent);
            else return EnsureNonnullableTableColumnExistsSql(column, spacesToIndent, nonnullPopulationScript);
        }
        public List<string> EnsureTableColumnExistsSql(ITableColumn column, int spacesToIndent = 0, List<string> nonnullPopulationScript = null)
        {
            ValidateTableColumnAsArgument(column, "EnsureTableColumnExistsSql");
            return EnsureColumnExistsSqlAsList(column, spacesToIndent, nonnullPopulationScript);
        }
        public List<string> EnsureTableColumnExistsAsDesignedSql(ITableColumn column, int spacesToIndent = 0, List<string> nonnullPopulationScript = null)
        {
            ValidateTableColumnAsArgument(column, "EnsureTableColumnExistsSql");
            List<string> retval;
            if (!column.IsNullable)
            {
                retval = EnsureColumnExistsSqlAsList(column, spacesToIndent, nonnullPopulationScript, ensureCheckConstraintsExist: false);
            }
            else
            {
                retval = EnsureNullableTableColumnExistsSql(column, spacesToIndent, ensureCheckConstraintsExist: false);
                string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
                retval.Add(CommitFullySql());
                retval.Add(
                ""
                + lfAndLeadingSpaces + "alter table " + column.Owner.FullName
                + lfAndLeadingSpaces + "alter column " + TableColumnSql(column, false, new string(' ', spacesToIndent + 13 + column.Name.Length)) + ";"
                );
                retval.Add(CommitFullySql());
            }
            if (column.CheckConstraints != null)
            {
                retval.Add(CommitFullySql());
                foreach (ICheckConstraint c in column.CheckConstraints.Values)
                {
                    foreach (string s in EnsureCheckConstraintExistsSql(c, spacesToIndent))
                    {
                        retval.Add(s);
                        retval.Add(CommitFullySql());
                    }
                }
            }
            return retval;
        }
        public List<string> DropTableColumnIfExistsSql(ITableColumn column, int spacesToIndent = 0)
        {
            ValidateTableColumnAsArgument(column, "EnsureTableColumnExistsSql");
            List<String> retval = new List<String>();
            foreach (ICheckConstraint c in column.CheckConstraints.Values)
            {
                foreach (string s in EnsureCheckConstraintExistsSql(c, spacesToIndent))
                {
                    retval.Add(s);
                    retval.Add(CommitFullySql());
                }
            }
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string nameLiteral = SqlStringLiteral(column.Name);
            string tableLiteral = SqlStringLiteral(column.Owner.Name);
            string schemaLiteral = SqlStringLiteral(Coalesce(NameOf(column.Schema), "dbo"));
            string databaseFullNameWithDot = FullNameOf(column.Database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            retval.Add(
                ""
                + lfAndLeadingSpaces + "if exists ( "
                + lfAndLeadingSpaces + "          select top(1) 1 "
                + lfAndLeadingSpaces + "            from " + databaseFullNameWithDot + "sys.columns "
                + lfAndLeadingSpaces + "           where name = " + nameLiteral
                + lfAndLeadingSpaces + "             and parent_object_id = object_id( " + tableLiteral + " ) "
                + lfAndLeadingSpaces + "             and schema_id = schema_id( " + schemaLiteral + " ) "
                + lfAndLeadingSpaces + "          ) "
                + lfAndLeadingSpaces + "  begin "
                + lfAndLeadingSpaces + "    alter table " + column.Owner.FullName
                + lfAndLeadingSpaces + "     drop column " + column.Name
                + lfAndLeadingSpaces + "    ; "
                + lfAndLeadingSpaces + "  end "
                );
            return retval;
        }
        public string SysnameTypeSql { get { return "sysname"; } }
        public string StringTypeSql { get { return "varchar"; } }
        public string UnicodeStringTypeSql { get { return "nvarchar"; } }

        // DefaultConstraint

        private void ValidateDefaultConstraintAsArgument(IDefaultConstraint constraint, string callingMethod)
        {
            ValidateTableElementAsArgument(
                constraint
                , "default constraint"
                , "constraint"
                , callingMethod + "(constraint, spacesToIndent)"
                );
        }
        public List<string> IDOfDefaultConstraintSql(
            string name
            , string tableName
            , string schemaName = null
            , IDatabase database = null
            , int spacesToIndent = 0
            , bool terminatesStatement = true
            )
        {
            ValidateArgumentsOfVableElementIDFunction(name, tableName, "tableName", "IDOfDefaultConstraintSql(name, tableName, schema, spacesToIndent, terminatesStatement)");
            string retval;
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            schemaName = Coalesce(schemaName, "dbo");
            string schemaLiteral = SqlStringLiteral(Coalesce(schemaName, "dbo"));
            string databaseFullNameWithDot = FullNameOf(database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            retval = ""
                + lfAndLeadingSpaces + "select min( id ) "
                + lfAndLeadingSpaces + "  from " + databaseFullNameWithDot + "sys.default_constraints "
                + lfAndLeadingSpaces + " where name = " + SqlStringLiteral(name)
                + lfAndLeadingSpaces + "   and parent_object_id = object_id( " + SqlStringLiteral(tableName) + " ) "
                + lfAndLeadingSpaces + "   and schema_id = schema_id( " + SqlStringLiteral(schemaName) + " ) "
                ;
            if (terminatesStatement)
                retval += lfAndLeadingSpaces + ";";
            return ListFromString(retval);
        }
        public List<string> EnsureDefaultConstraintExistsSql(IDefaultConstraint constraint, int spacesToIndent = 0)
        {
            ValidateDefaultConstraintAsArgument(constraint, "EnsureDefaultConstraintExistsSql");
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string nameLiteral = SqlStringLiteral(constraint.Name);
            string tableLiteral = SqlStringLiteral(constraint.Owner.Name);
            string schemaLiteral = SqlStringLiteral(Coalesce(NameOf(constraint.Schema), "dbo"));
            string databaseFullNameWithDot = FullNameOf(constraint.Database);
            string defaultValueSql = ObjectValueToSql(constraint.ConstraintText, constraint.ConstrainedColumn.DataType);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            return ListFromString(
                ""
                + lfAndLeadingSpaces + "if not exists ( "
                + "select top(1) 1 "
                + "  from " + databaseFullNameWithDot + "sys.default_constraints "
                + " where name = " + nameLiteral
                + "   and parent_object_id = object_id( " + tableLiteral + " ) "
                + "   and schema_id = schema_id( " + schemaLiteral + " ) "
                + ") "
                + lfAndLeadingSpaces + "  begin "
                + lfAndLeadingSpaces + "    alter table " + constraint.Owner.FullName
                + lfAndLeadingSpaces + "      add constraint " + constraint.Name
                + lfAndLeadingSpaces + "            default( " + defaultValueSql + " )"
                + lfAndLeadingSpaces + "    ; "
                + lfAndLeadingSpaces + "  end "
                );
        }
        public List<string> EnsureDefaultConstraintExistsAsDesignedSql(IDefaultConstraint constraint, int spacesToIndent = 0)
        {
            List<string> retval = new List<string>();
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string defaultValueSql = ObjectValueToSql(constraint.ConstraintText, constraint.ConstrainedColumn.DataType);
            retval.Add(CommitFullySql());
            retval.Add(DropDefaultConstraintIfExistsSqlString(constraint, spacesToIndent));
            // Note that DropDefaultConstraintIfExistsSql validates the parameters so we do not have to do that again.
            retval.Add(CommitFullySql());
            retval.Add(
                ""
                + lfAndLeadingSpaces + "alter table " + constraint.Owner.FullName
                + lfAndLeadingSpaces + "  add constraint " + constraint.Name
                + lfAndLeadingSpaces + "        default( " + defaultValueSql + " )"
                + lfAndLeadingSpaces + "; "
                );
            retval.Add(CommitFullySql());
            return retval;
        }
        private string DropDefaultConstraintIfExistsSqlString(IDefaultConstraint constraint, int spacesToIndent = 0)
        {
            return DropConstraintIfExistsSqlString(constraint, "sys.default_constraints", spacesToIndent);
        }
        public List<string> DropDefaultConstraintIfExistsSql(IDefaultConstraint constraint, int spacesToIndent = 0)
        {
            return ListFromString(DropDefaultConstraintIfExistsSqlString(constraint, spacesToIndent));
        }

        // ForeignKey

        private void ValidateForeignKeyAsArgument(IForeignKey key, string callingMethod)
        {
            ValidateTableElementAsArgument(
                key
                , "foreign key"
                , "key"
                , callingMethod + "(key, spacesToIndent)"
                );
        }
        public List<string> IDOfForeignKeySql(
            string name
            , string tableName
            , string schemaName = null
            , IDatabase database = null
            , int spacesToIndent = 0
            , bool terminatesStatement = true
            )
        {
            ValidateArgumentsOfVableElementIDFunction(name, tableName, "tableName", "IDOfForeignKeySql(name, tableName, schema, spacesToIndent, terminatesStatement)");
            string retval;
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            schemaName = Coalesce(schemaName, "dbo");
            string schemaLiteral = SqlStringLiteral(Coalesce(schemaName, "dbo"));
            string databaseFullNameWithDot = FullNameOf(database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            retval = ""
                + lfAndLeadingSpaces + "select min( id ) "
                + lfAndLeadingSpaces + "  from " + databaseFullNameWithDot + "sys.foreign_keys "
                + lfAndLeadingSpaces + " where name = " + SqlStringLiteral(name)
                + lfAndLeadingSpaces + "   and parent_object_id = object_id( " + SqlStringLiteral(tableName) + " ) "
                + lfAndLeadingSpaces + "   and schema_id = schema_id( " + SqlStringLiteral(schemaName) + " ) "
                ;
            if (terminatesStatement)
                retval += lfAndLeadingSpaces + ";";
            return ListFromString(retval);
        }
        private string EnsureForeignKeyExistsSqlString(IForeignKey key, bool forceManualCascades, int spacesToIndent = 0)
        {
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string nameLiteral = SqlStringLiteral(key.Name);
            string tableLiteral = SqlStringLiteral(key.Owner.Name);
            string schemaLiteral = SqlStringLiteral(Coalesce(NameOf(key.Schema), "dbo"));
            string onDeleteCascadeClause = "";
            string onUpdateCascadeClause = "";
            if (!forceManualCascades)
            {
                if (key.AutoCascadeOnDeleteIfPossible)
                    onDeleteCascadeClause = lfAndLeadingSpaces + "            on delete cascade";
                if (key.AutoCascadeOnUpdateIfPossible)
                    onUpdateCascadeClause = lfAndLeadingSpaces + "            on update cascade";
            }
            string databaseFullNameWithDot = FullNameOf(key.Database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            return
                ""
                + lfAndLeadingSpaces + "if not exists ( "
                + "select top(1) 1 "
                + "  from " + databaseFullNameWithDot + "sys.foreign_keys "
                + " where name = " + nameLiteral
                + "   and parent_object_id = object_id( " + tableLiteral + " ) "
                + "   and schema_id = schema_id( " + schemaLiteral + " ) "
                + ") "
                + lfAndLeadingSpaces + "  begin "
                + lfAndLeadingSpaces + "    alter table " + key.Owner.FullName
                + lfAndLeadingSpaces + "      add constraint " + key.Name
                + lfAndLeadingSpaces + "            foreign key( " + CommaDelimited(key.KeyColumnNames) + " )"
                + lfAndLeadingSpaces + "            references " + key.ReferencedTable.Name + "( " + CommaDelimited(key.ReferenceColumnNames) + " )"
                + onDeleteCascadeClause
                + onUpdateCascadeClause
                + lfAndLeadingSpaces + "    ; "
                + lfAndLeadingSpaces + "  end "
                ;
        }
        public List<string> EnsureForeignKeyExistsSql(IForeignKey key, bool cascadeManually, int spacesToIndent = 0)
        {
            ValidateForeignKeyAsArgument(key, "EnsureForeignKeyExistsSql");
            return ListFromString(EnsureForeignKeyExistsSqlString(key, cascadeManually, spacesToIndent));
        }
        public List<string> EnsureForeignKeyExistsAsDesignedSql(IForeignKey key, bool cascadeManually, int spacesToIndent = 0)
        {
            List<string> retval = new List<string>();
            retval.Add(DropForeignKeyIfExistsSqlString(key, spacesToIndent));
            retval.Add(CommitFullySql());
            retval.Add(EnsureForeignKeyExistsSqlString(key, cascadeManually, spacesToIndent));
            return retval;
        }
        private string DropForeignKeyIfExistsSqlString(IForeignKey key, int spacesToIndent = 0)
        {
            return DropConstraintIfExistsSqlString(key, "sys.foreign_keys", spacesToIndent);
        }
        public List<string> DropForeignKeyIfExistsSql(IForeignKey key, int spacesToIndent = 0)
        {
            return ListFromString(DropForeignKeyIfExistsSqlString(key, spacesToIndent));
        }

        // Index

        private void ValidateIndexAsArgument(IIndex index, string callingMethod)
        {
            ValidateTableElementAsArgument(
                index
                , "index"
                , "index"
                , callingMethod + "(index, spacesToIndent)"
                );
        }
        public List<string> IDOfIndexSql(
            string name
            , string vableName
            , string schemaName = null
            , IDatabase database = null
            , int spacesToIndent = 0
            , bool terminatesStatement = true
            )
        {
            ValidateArgumentsOfVableElementIDFunction(name, vableName, "vableName", "IDOfIndexSql(name, tableName, schema, spacesToIndent, terminatesStatement)");
            string retval;
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            schemaName = Coalesce(schemaName, "dbo");
            string schemaLiteral = SqlStringLiteral(Coalesce(schemaName, "dbo"));
            string databaseFullNameWithDot = FullNameOf(database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            retval = ""
                + lfAndLeadingSpaces + "select min( id ) "
                + lfAndLeadingSpaces + "  from " + databaseFullNameWithDot + "sys.indexes "
                + lfAndLeadingSpaces + " where name = " + SqlStringLiteral(name)
                + lfAndLeadingSpaces + "   and parent_object_id = object_id( " + SqlStringLiteral(vableName) + " ) "
                + lfAndLeadingSpaces + "   and schema_id = schema_id( " + SqlStringLiteral(schemaName) + " ) "
                ;
            if (terminatesStatement)
                retval += lfAndLeadingSpaces + ";";
            return ListFromString(retval);
        }
        private string EnsureIndexExistsSqlString(IIndex index, int spacesToIndent = 0)
        {
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string nameLiteral = SqlStringLiteral(index.Name);
            string tableLiteral = SqlStringLiteral(index.Owner.Name);
            string schemaLiteral = SqlStringLiteral(Coalesce(NameOf(index.Schema), "dbo"));
            string databaseFullNameWithDot = FullNameOf(index.Database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            string createIndexLine = "create";
            if (index.IsUnique) createIndexLine += "unique ";
            if (index.IsClustered) createIndexLine += "clustered";
            else createIndexLine += "nonclustered";
            createIndexLine += " index " + index.Name;
            return
                ""
                + lfAndLeadingSpaces + "if not exists ( "
                + "select top(1) 1 "
                + "  from " + databaseFullNameWithDot + "sys.indexes idx "
                + "       join " + databaseFullNameWithDot + "sys.objects tbl "
                + "         on tbl.object_id = object_id( " + tableLiteral + " ) "
                + "        and tbl.schema_id = schema_id( " + schemaLiteral + " ) "
                + "        and tbl.object_id = idx.object_id "
                + " where idx.name = " + nameLiteral
                + ") "
                + lfAndLeadingSpaces + "  begin "
                + lfAndLeadingSpaces + "    " + createIndexLine
                + lfAndLeadingSpaces + "        on " + index.Owner.FullName + "( " + CommaDelimited(index.ColumnNames) + " )"
                + lfAndLeadingSpaces + "    ; "
                + lfAndLeadingSpaces + "  end "
                ;
        }
        public List<string> EnsureIndexExistsSql(IIndex index, int spacesToIndent = 0)
        {
            ValidateIndexAsArgument(index, "EnsureIndexExistsSql");
            return ListFromString(EnsureIndexExistsSqlString(index, spacesToIndent));
        }
        public List<string> EnsureIndexExistsAsDesignedSql(IIndex index, int spacesToIndent = 0)
        {
            List<string> retval = new List<string>();
            retval.Add(DropIndexIfExistsSqlString(index, spacesToIndent));
            retval.Add(CommitFullySql());
            retval.Add(EnsureIndexExistsSqlString(index, spacesToIndent));
            return retval;
        }
        private string DropIndexIfExistsSqlString(IIndex index, int spacesToIndent = 0)
        {
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string nameLiteral = SqlStringLiteral(index.Name);
            string tableLiteral = SqlStringLiteral(index.Owner.Name);
            string schemaLiteral = SqlStringLiteral(Coalesce(NameOf(index.Schema), "dbo"));
            string databaseFullNameWithDot = FullNameOf(index.Database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            return
                ""
                + lfAndLeadingSpaces + "if exists ( "
                + "select top(1) 1 "
                + "  from " + databaseFullNameWithDot + "sys.indexes idx "
                + "       join " + databaseFullNameWithDot + "sys.objects tbl "
                + "         on tbl.object_id = object_id( " + tableLiteral + " ) "
                + "        and tbl.schema_id = schema_id( " + schemaLiteral + " ) "
                + "        and tbl.object_id = idx.object_id "
                + " where idx.name = " + nameLiteral
                + ") "
                + lfAndLeadingSpaces + "  begin "
                + lfAndLeadingSpaces + "    drop index " + index.Name
                + lfAndLeadingSpaces + "        on " + index.Owner.FullName 
                + lfAndLeadingSpaces + "    ; "
                + lfAndLeadingSpaces + "  end "
                ;
        }
        public List<string> DropIndexIfExistsSql(IIndex index, int spacesToIndent = 0)
        {
            return ListFromString(DropIndexIfExistsSqlString(index, spacesToIndent));
        }

        public string CreateTempTableIndexSql(
            string tempTableIndexName
            , string tempTableName
            , string columnList
            , bool isUnique = false
            , bool isClustered = false
            , int spacesToIndent = 0
            )
        {
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string options
                = (isUnique ? "unique " : "")
                + (isClustered ? "clustered " : "");
            string sql = ""
                + lfAndLeadingSpaces + "create " + options + " index " + tempTableIndexName
                + lfAndLeadingSpaces + "  on " + tempTableName + "( " + columnList + " ); ";
            return sql;
        }

        // PrimaryKey

        private void ValidatePrimaryKeyAsArgument(IPrimaryKey key, string callingMethod)
        {
            ValidateTableElementAsArgument(
                key
                , "primary key"
                , "key"
                , callingMethod + "(key, spacesToIndent)"
                );
        }
        public List<string> IDOfPrimaryKeySql(
            string name
            , string tableName
            , string schemaName = null
            , IDatabase database = null
            , int spacesToIndent = 0
            , bool terminatesStatement = true
            )
        {
            ValidateArgumentsOfVableElementIDFunction(name, tableName, "tableName", "IDOfPrimaryKeySql(name, tableName, schema, spacesToIndent, terminatesStatement)");
            string retval;
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            schemaName = Coalesce(schemaName, "dbo");
            string schemaLiteral = SqlStringLiteral(Coalesce(schemaName, "dbo"));
            string databaseFullNameWithDot = FullNameOf(database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            retval = ""
                + lfAndLeadingSpaces + "select min( id ) "
                + lfAndLeadingSpaces + "  from " + databaseFullNameWithDot + "sys.primary_keys "
                + lfAndLeadingSpaces + " where name = " + SqlStringLiteral(name)
                + lfAndLeadingSpaces + "   and parent_object_id = object_id( " + SqlStringLiteral(tableName) + " ) "
                + lfAndLeadingSpaces + "   and schema_id = schema_id( " + SqlStringLiteral(schemaName) + " ) "
                ;
            if (terminatesStatement)
                retval += lfAndLeadingSpaces + ";";
            return ListFromString(retval);
        }
        private string EnsurePrimaryKeyExistsSqlString(IPrimaryKey key, int spacesToIndent = 0)
        {
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string nameLiteral = SqlStringLiteral(key.Name);
            string tableLiteral = SqlStringLiteral(key.Owner.Name);
            string schemaLiteral = SqlStringLiteral(Coalesce(NameOf(key.Schema), "dbo"));
            string databaseFullNameWithDot = FullNameOf(key.Database);
            string primaryKeySpec = "primary key ";
            if (key.IsClustered) primaryKeySpec += "clustered(";
            else primaryKeySpec += "unclustered(";
            primaryKeySpec += CommaDelimited(key.ColumnNames) + " )";
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            return
                ""
                + lfAndLeadingSpaces + "if not exists ( "
                + "select top(1) 1 "
                + "  from " + databaseFullNameWithDot + "sys.primary_keys "
                + " where name = " + nameLiteral
                + "   and parent_object_id = object_id( " + tableLiteral + " ) "
                + "   and schema_id = schema_id( " + schemaLiteral + " ) "
                + ") "
                + lfAndLeadingSpaces + "  begin "
                + lfAndLeadingSpaces + "    alter table " + key.Owner.FullName
                + lfAndLeadingSpaces + "      add constraint " + key.Name
                + lfAndLeadingSpaces + "            " + primaryKeySpec
                + lfAndLeadingSpaces + "    ; "
                + lfAndLeadingSpaces + "  end "
                ;
        }
        public List<string> EnsurePrimaryKeyExistsSql(IPrimaryKey key, int spacesToIndent = 0)
        {
            ValidatePrimaryKeyAsArgument(key, "EnsurePrimaryKeyExistsSql");
            return ListFromString(EnsurePrimaryKeyExistsSqlString(key, spacesToIndent));
        }
        public List<string> EnsurePrimaryKeyExistsAsDesignedSql(IPrimaryKey key, int spacesToIndent = 0)
        {
            List<string> retval = new List<string>();
            retval.Add(DropPrimaryKeyIfExistsSqlString(key, spacesToIndent));
            retval.Add(CommitFullySql());
            retval.Add(EnsurePrimaryKeyExistsSqlString(key, spacesToIndent));
            return retval;
        }
        private string DropPrimaryKeyIfExistsSqlString(IPrimaryKey key, int spacesToIndent = 0)
        {
            return DropConstraintIfExistsSqlString(key, "sys.primary_keys", spacesToIndent);
        }
        public List<string> DropPrimaryKeyIfExistsSql(IPrimaryKey key, int spacesToIndent = 0)
        {
            return ListFromString(DropPrimaryKeyIfExistsSqlString(key, spacesToIndent));
        }

        // ScalarFunction
        public List<string> IDOfScalarFunctionSql(string name, string schemaName = null, IDatabase database = null, int spacesToIndent = 0)
        {
            throw new NotImplementedException();
        }
        public List<string> ScalarFunctionExistsSql(IScalarFunction function, int spacesToIndent = 0)
        {
            throw new NotImplementedException();
        }
        public List<string> EnsureScalarFunctionExistsSql(IScalarFunction function, int spacesToIndent = 0)
        {
            throw new NotImplementedException();
        }
        public List<string> EnsureScalarFunctionExistsAsDesignedSql(IScalarFunction function, int spacesToIndent = 0)
        {
            throw new NotImplementedException();
        }
        public List<string> DropScalarFunctionIfExistsSql(IScalarFunction function, int spacesToIndent = 0)
        {
            throw new NotImplementedException();
        }

        // Schema
        private void ValidateSchemaAsArgument(ISchema schema, string callingMethod)
        {
            if (schema == null)
                throw new ArgumentNullException(
                    paramName: "schema"
                    , message: ""
                    + "Cannot provide null as 'schema' argument to "
                    + this.GetType().FullName + "."
                    + callingMethod + "."
                    );
            if (schema.Name == null)
                throw new ArgumentNullException(
                    paramName: "index.Name"
                    , message: ""
                    + "Cannot provide schema with null Name as 'schema' argument to "
                    + this.GetType().FullName + "."
                    + callingMethod + "."
                    );
        }
        public List<string> IDOfSchemaSql(
            string name
            , IDatabase database = null
            , int spacesToIndent = 0
            , bool terminatesStatement = true
            )
        {
            string retval;
            if (name == null)
                throw new ArgumentNullException(
                    paramName: "name"
                    , message: ""
                    + "Cannot provide null as 'name' argument to "
                    + this.GetType().FullName
                    + ".IDOfSchemaSql(name, database, spacesToIndent, terminatesStatement)."
                    );
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string databaseFullNameWithDot = FullNameOf(database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            retval = ""
                + lfAndLeadingSpaces + "select min( id ) "
                + lfAndLeadingSpaces + "  from " + databaseFullNameWithDot + "sys.schemas "
                + lfAndLeadingSpaces + " where name = " + SqlStringLiteral(name)
                ;
            if (terminatesStatement)
                retval += lfAndLeadingSpaces + ";";
            return ListFromString(retval);
        }
        public List<string> EnsureSchemaExistsSql(ISchema schema, int spacesToIndent = 0)
        {
            ValidateSchemaAsArgument(schema, "EnsureSchemaExistsSql( schema, spacesToIndent )");
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string nameLiteral = SqlStringLiteral(schema.Name);
            string databaseFullNameWithDot = FullNameOf(schema.Database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            return ListFromString(
                ""
                + lfAndLeadingSpaces + "if not exists ( "
                + "select top(1) 1 "
                + "  from " + databaseFullNameWithDot + "sys.schemas "
                + " where name = " + nameLiteral
                + ") "
                + lfAndLeadingSpaces + "  begin "
                + lfAndLeadingSpaces + "    create schema " + schema.FullName + ";"
                + lfAndLeadingSpaces + "  end "
                );
        }
        public List<string> DropSchemaIfExistsSql(ISchema schema, int spacesToIndent = 0)
        {
            ValidateSchemaAsArgument(schema, "DropSchemaIfExistsSql( schema, spacesToIndent )");
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string nameLiteral = SqlStringLiteral(schema.Name);
            string databaseFullNameWithDot = FullNameOf(schema.Database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            return ListFromString(
                ""
                + lfAndLeadingSpaces + "if not exists ( "
                + "select top(1) 1 "
                + "  from " + databaseFullNameWithDot + "sys.schemas "
                + " where name = " + nameLiteral
                + ") "
                + lfAndLeadingSpaces + "  begin "
                + lfAndLeadingSpaces + "    drop schema " + schema.FullName + ";"
                + lfAndLeadingSpaces + "  end "
                );
        }

        // Sequence
        public bool SupportsNativeDatabaseSequences { get { return false; } }
        public List<string> IDOfSequenceSql(string name, string schemaName = null, IDatabase database = null, int spacesToIndent = 0)
        {
            // SQL Server does not support the concept of a sequence natively
            return IDOfTableSql(name, schemaName, database, spacesToIndent);
        }
        public List<string> EnsureSequenceExistsSql(IDatabaseSequence sequence, int spacesToIndent = 0)
        {
            if (sequence.PhysicalObjectType != ObjectTypes.Table())
                // SQL Server does not support the concept of a sequence natively
                throw new NotImplementedException();
            return EnsureTableExistsSql((ITable)sequence, spacesToIndent);
        }
        public List<string> EnsureSequenceExistsAsDesignedSql(IDatabaseSequence sequence, int spacesToIndent = 0)
        {
            if (sequence.PhysicalObjectType != ObjectTypes.Table())
                // SQL Server does not support the concept of a sequence natively
                throw new NotImplementedException();
            return EnsureTableExistsAsDesignedSql((ITable)sequence, spacesToIndent);
        }
        public List<string> DropSequenceIfExistsSql(IDatabaseSequence sequence, int spacesToIndent = 0)
        {
            if (sequence.PhysicalObjectType != ObjectTypes.Table())
                // SQL Server does not support the concept of a sequence natively
                throw new NotImplementedException();
            return DropTableIfExistsSql((ITable)sequence, spacesToIndent);
        }
        public List<string> GetNextKeySql(IDatabaseSequence sequence, int spacesToIndent = 0)
        {
            return GetFirstKeyInNextBlockOfNKeysSql(sequence, 1, spacesToIndent);
        }
        public List<string> GetFirstKeyInNextBlockOfNKeysSql(IDatabaseSequence sequence, int n, int spacesToIndent = 0)
        {
            if (sequence == null)
                throw new NullArgumentException(
                    "sequence"
                    , GetType().FullName + ".GetNextKeySql( sequence, spacesToIndent )"
                    );
            if (sequence.PhysicalObjectType != ObjectTypes.Table())
                // SQL Server does not support the concept of a sequence natively
                throw new NotImplementedException();
            if (!typeof(ITableAsSequence).IsAssignableFrom(sequence.GetType()))
                // SQL Server does not support the concept of a sequence natively
                throw new NotImplementedException();
            ITableAsSequence table = (ITableAsSequence)sequence;
            string tableName = table.FullName;
            string idColumnName = table.IDColumn.Name;
            string dummyColumnName = table.DummyInsertColumn.Name;
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            List<string> retval = new List<string> { lfAndLeadingSpaces + CommitFullySql() };
            string sql =
                ""
                + lfAndLeadingSpaces + "begin transaction;"
                + lfAndLeadingSpaces + "declare @output table( id int );"
                + lfAndLeadingSpaces + "select top(1) 1 from " + tableName + " with (tablockx, holdlock);"
                + lfAndLeadingSpaces + "merge into " + tableName + " as seq"
                + lfAndLeadingSpaces + "using        ( values(1)) as t(i)"
                + "        on 0 = 1 "
                + lfAndLeadingSpaces + "when not matched then "
                + lfAndLeadingSpaces + "  insert( " + dummyColumnName + " ) values( 'x' ) "
                + lfAndLeadingSpaces + "  output inserted" + idColumnName + " into @output "
                + lfAndLeadingSpaces + ";";
            if (n > 1 )
            {
                sql += ""
                + lfAndLeadingSpaces + "insert into " + tableName + "( " + dummyColumnName + " )"
                + lfAndLeadingSpaces
                + NRowedTableSql(
                    n - 1
                    , "1 i"
                    , spacesToIndent: spacesToIndent
                    );
            }
            sql += ""
                + lfAndLeadingSpaces + "delete from " + tableName + ";"
                + lfAndLeadingSpaces + "while @@TRANCOUNT > 0 commit transaction;"
                + lfAndLeadingSpaces + "select id from @output; ";
            retval.Add(sql);
            return retval;
        }
        public List<string> GetKeysForTableRowsSql(
            IDatabaseSequence sequence
            , string tempTableName
            , string whereClause
            , int spacesToIndent = 0
            )
        {
            if (sequence == null)
                throw new NullArgumentException(
                    "sequence"
                    , GetType().FullName + ".GetNextKeySql( sequence, spacesToIndent )"
                    );
            if (sequence.PhysicalObjectType != ObjectTypes.Table())
                // SQL Server does not support the concept of a sequence natively
                throw new NotImplementedException();
            if (!typeof(ITableAsSequence).IsAssignableFrom(sequence.GetType()))
                // SQL Server does not support the concept of a sequence natively
                throw new NotImplementedException();
            ITableAsSequence table = (ITableAsSequence)sequence;
            string tableName = table.FullName;
            string idColumnName = table.IDColumn.Name;
            string dummyColumnName = table.DummyInsertColumn.Name;
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            List<string> retval = new List<string> { lfAndLeadingSpaces + CommitFullySql() };
            string sql =
                ""
                + lfAndLeadingSpaces + "begin transaction;"
                + lfAndLeadingSpaces + "declare @output table( id int );"
                + lfAndLeadingSpaces + "select top(1) 1 from " + tableName + " with (tablockx, holdlock);"
                + lfAndLeadingSpaces + "merge into " + tableName + " as seq"
                + lfAndLeadingSpaces + "using        ( values(1)) as t(i)"
                + lfAndLeadingSpaces + "        on 0 = 1 "
                + lfAndLeadingSpaces + "when not matched then "
                + lfAndLeadingSpaces + "  insert( " + dummyColumnName + " ) values( 'x' ) "
                + lfAndLeadingSpaces + "  output inserted" + idColumnName + " into @output "
                + lfAndLeadingSpaces + ";"
                + lfAndLeadingSpaces + "insert into " + tableName + "( " + dummyColumnName + " )"
                + lfAndLeadingSpaces + "select 0"
                + lfAndLeadingSpaces + "  from " + tempTableName;
            if (whereClause != null)
            {
                if (!new Regex("^\\s*where\\b").IsMatch(whereClause.ToUpper()))
                    whereClause = " where " + whereClause;
                sql += ""
                + lfAndLeadingSpaces + whereClause;
            }
            sql += ""
                + lfAndLeadingSpaces + ";"
                + lfAndLeadingSpaces + "delete from " + tableName + ";"
                + lfAndLeadingSpaces + "while @@TRANCOUNT > 0 commit transaction;"
                + lfAndLeadingSpaces + "select id from @output; ";
            retval.Add(sql);
            return retval;
        }

        // StoredProcedure
        public List<string> IDOfStoredProcedureSql(
            string name
            , string schemaName = null
            , IDatabase database = null
            , int spacesToIndent = 0
            , bool terminatesStatement = true
            )
        {
            string retval;
            if (name == null)
                throw new ArgumentNullException(
                    paramName: "name"
                    , message: ""
                    + "Cannot provide null as 'name' argument to "
                    + this.GetType().FullName
                    + ".IDOfStoredProcedureSql( name, schemaName, database, spacesToIndent, terminatesStatement )."
                    );
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string schemaLiteral = SqlStringLiteral(Coalesce(schemaName, "dbo"));
            string databaseFullNameWithDot = FullNameOf(database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            retval = ""
                + lfAndLeadingSpaces + "select min( id ) "
                + lfAndLeadingSpaces + "  from " + databaseFullNameWithDot + "sys.procedures "
                + lfAndLeadingSpaces + " where name = " + SqlStringLiteral(name)
                + lfAndLeadingSpaces + "   and schema_id = schema_id( " + SqlStringLiteral(schemaName) + " ) "
                ;
            if (terminatesStatement)
                retval += lfAndLeadingSpaces + ";";
            return ListFromString(retval);
        }
        private string EnsureStoredProcedureExistsSqlString(IStoredProcedure proc, int spacesToIndent = 0)
        {
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string nameLiteral = SqlStringLiteral(proc.Name);
            string schemaLiteral = SqlStringLiteral(Coalesce(NameOf(proc.Owner), "dbo"));
            string databaseFullNameWithDot = FullNameOf(proc.Database);
            string paramList = "";
            string paramLeader = lfAndLeadingSpaces + "         ";
            foreach (
                ISqlParameter p 
                in proc.Parameters.Values.OrderBy(t=>t.Item1)
                )
            {
                string paramLine = paramLeader + "@" + p.Name + " " + p.DataType;
                if (p.DefaultValue != null)
                    paramLine += " = " + ObjectValueToSql(p.DefaultValue, p.DataType);
                switch (p.Protocol )
                {
                    case SPParamProtocol.Output:
                        paramLine += " output";
                        break;
                    case SPParamProtocol.ReadOnly:
                        paramLine += " readonly";
                        break;
                    default: break;
                }
                paramLeader = lfAndLeadingSpaces + "       , ";
                paramList += paramLine;
            }
            string bodyText = proc.BodyText.Replace("\n", lfAndLeadingSpaces + "  ");
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            return
                ""
                + lfAndLeadingSpaces + "create or alter procedure " + proc.FullName + paramList
                + lfAndLeadingSpaces + "as "
                + lfAndLeadingSpaces + "begin "
                + lfAndLeadingSpaces + "  " + bodyText
                + lfAndLeadingSpaces + "end "
                ;
        }
        public List<string> EnsureStoredProcedureExistsSql(IStoredProcedure proc, int spacesToIndent = 0)
        {
            if (proc == null)
                throw new ArgumentNullException(
                    paramName: "proc"
                    , message: ""
                    + "Cannot provide null as 'proc' argument to "
                    + this.GetType().FullName
                    + ".EnsureStoredProcedureExistsSql(proc, spacesToIndent)."
                    );
            if (proc.Name == null)
                throw new ArgumentNullException(
                    paramName: "proc.Name"
                    , message: ""
                    + "Cannot provide stored procedure with null Name as 'proc' argument to "
                    + this.GetType().FullName
                    + ".EnsureStoredProcedureExistsSql(proc, spacesToIndent)."
                    );
            return ListFromString(EnsureStoredProcedureExistsSqlString(proc, spacesToIndent));
        }
        public List<string> EnsureStoredProcedureExistsAsDesignedSql(IStoredProcedure proc, int spacesToIndent = 0)
        {
            return EnsureStoredProcedureExistsSql(proc, spacesToIndent);
        }
        private string DropStoredProcedureIfExistsSqlString(IStoredProcedure proc, int spacesToIndent = 0)
        {
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            return lfAndLeadingSpaces + "drop procedure if exists " + proc.FullName + ";";
        }
        public List<string> DropStoredProcedureIfExistsSql(IStoredProcedure proc, int spacesToIndent = 0)
        {
            if (proc == null)
                throw new ArgumentNullException(
                    paramName: "proc"
                    , message: ""
                    + "Cannot provide null as 'proc' argument to "
                    + this.GetType().FullName
                    + ".EnsureStoredProcedureExistsSql(proc, spacesToIndent)."
                    );
            if (proc.Name == null)
                throw new ArgumentNullException(
                    paramName: "proc.Name"
                    , message: ""
                    + "Cannot provide stored procedure with null Name as 'proc' argument to "
                    + this.GetType().FullName
                    + ".EnsureStoredProcedureExistsSql(proc, spacesToIndent)."
                    );
            return ListFromString(DropStoredProcedureIfExistsSqlString(proc, spacesToIndent));
        }

        // Table
        private void ValidateVableParameter( IVable vable, string paramName, string callingMethod )
        {
            if (vable == null)
                throw new ArgumentNullException(
                    paramName: paramName
                    , message: ""
                    + "Cannot provide null as '" + paramName + "' argument to "
                    + this.GetType().FullName + "." + callingMethod + "."
                    );
            if (vable.Name == null)
                throw new ArgumentNullException(
                    paramName: paramName + ".Name"
                    , message: ""
                    + "Cannot provide vable with null Name as '" + paramName + "' argument to "
                    + this.GetType().FullName + "." + callingMethod + "."
                    );
        }
        public List<string> IDOfTableSql(
            string name
            , string schemaName = null
            , IDatabase database = null
            , int spacesToIndent = 0
            , bool terminatesStatement = true
            )
        {
            string retval;
            if (name == null)
                throw new ArgumentNullException(
                    paramName: "name"
                    , message: ""
                    + "Cannot provide null as 'name' argument to "
                    + this.GetType().FullName
                    + ".IDOfTableSql( name, schemaName, database, spacesToIndent, terminatesStatement )."
                    );
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string schemaLiteral = SqlStringLiteral(Coalesce(schemaName, "dbo"));
            string databaseFullNameWithDot = FullNameOf(database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            retval = ""
                + lfAndLeadingSpaces + "select min( id ) "
                + lfAndLeadingSpaces + "  from " + databaseFullNameWithDot + "sys.tables "
                + lfAndLeadingSpaces + " where name = " + SqlStringLiteral(name)
                + lfAndLeadingSpaces + "   and schema_id = schema_id( " + SqlStringLiteral(schemaName) + " ) "
                ;
            if (terminatesStatement)
                retval += lfAndLeadingSpaces + ";";
            return ListFromString(retval);
        }
        private string EnsureTableExistsSqlString(ITable table, int spacesToIndent = 0)
        {
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string nameLiteral = SqlStringLiteral(table.Name);
            string schemaLiteral = SqlStringLiteral(Coalesce(NameOf(table.Owner), "dbo"));
            string databaseFullNameWithDot = FullNameOf(table.Database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            return
                ""
                + lfAndLeadingSpaces + "if not exists ( "
                + "select top(1) 1 "
                + "  from " + databaseFullNameWithDot + "sys.tables "
                + " where name = " + nameLiteral
                + "   and schema_id = schema_id( " + schemaLiteral + " ) "
                + ") "
                + lfAndLeadingSpaces + "  begin "
                + lfAndLeadingSpaces + "    create table " + table.FullName + "( toBeDeleted int );"
                + lfAndLeadingSpaces + "  end "
                ;
        }
        public List<string> EnsureTableExistsSql(ITable table, int spacesToIndent = 0)
        {
            ValidateVableParameter(table, "table", "EnsureTableExistsSql(table, spacesToIndent)");
            return ListFromString(EnsureTableExistsSqlString(table, spacesToIndent));
        }
        public List<string> EnsureTableExistsAsDesignedSql(ITable table, int spacesToIndent = 0, bool includeForeignKeys = true )
        {
            ValidateVableParameter(table, "table", "EnsureTableExistsAsDesignedSql(table, spacesToIndent)");
            List<string> retval = new List<string>();
            retval.Add(EnsureTableExistsSqlString(table, spacesToIndent));
            retval.Add(CommitFullySql());
            foreach (Tuple<int,IColumn> t in table.Columns.Values)
            {
                foreach (string s in EnsureTableColumnExistsAsDesignedSql((ITableColumn)t.Item2, spacesToIndent))
                    retval.Add(s);
                retval.Add(CommitFullySql());
            }
            if (table.PrimaryKey != null)
            {
                foreach (string s in EnsurePrimaryKeyExistsAsDesignedSql(table.PrimaryKey, spacesToIndent))
                    retval.Add(s);
                retval.Add(CommitFullySql());
            }
            foreach (IUniqueKey key in table.UniqueKeys.Values)
            {
                foreach (string s in EnsureUniqueKeyExistsAsDesignedSql(key, spacesToIndent))
                    retval.Add(s);
                retval.Add(CommitFullySql());
            }
            foreach (ICheckConstraint constraint in table.CheckConstraints.Values)
            {
                foreach (string s in EnsureCheckConstraintExistsAsDesignedSql(constraint, spacesToIndent))
                    retval.Add(s);
                retval.Add(CommitFullySql());
            }
            foreach (IDefaultConstraint constraint in table.DefaultConstraints.Values)
            {
                foreach (string s in EnsureDefaultConstraintExistsAsDesignedSql(constraint, spacesToIndent))
                    retval.Add(s);
                retval.Add(CommitFullySql());
            }
            foreach (IIndex index in table.Indexes.Values)
            {
                foreach (string s in EnsureIndexExistsAsDesignedSql(index, spacesToIndent))
                    retval.Add(s);
                retval.Add(CommitFullySql());
            }
            if (includeForeignKeys)
            {
                foreach (string s in EnsureForeignKeysExistAsDesignedSql(table, spacesToIndent ))
                    retval.Add(s);
                retval.Add(CommitFullySql());
            }
            return retval;
        }
        public List<string> EnsureForeignKeysExistAsDesignedSql(ITable table, int spacesToIndent = 0)
        {
            ValidateVableParameter(table, "table", "EnsureTableExistsAsDesignedSql(table, spacesToIndent)");
            List<string> retval = new List<string>();
            foreach (IForeignKey key in table.ForeignKeys.Values)
            {
                foreach (string s in EnsureForeignKeyExistsAsDesignedSql(key, false, spacesToIndent))
                    retval.Add(s);
                retval.Add(CommitFullySql());
            }
            return retval;
        }
        private string DropTableIfExistsSqlString(ITable table, int spacesToIndent = 0)
        {
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string nameLiteral = SqlStringLiteral(table.Name);
            string schemaLiteral = SqlStringLiteral(Coalesce(NameOf(table.Owner), "dbo"));
            string databaseFullNameWithDot = FullNameOf(table.Database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            return
                ""
                + lfAndLeadingSpaces + "if exists ( "
                + lfAndLeadingSpaces + "          select top(1) 1 "
                + lfAndLeadingSpaces + "            from " + databaseFullNameWithDot + "sys.tables "
                + lfAndLeadingSpaces + "           where name = " + nameLiteral
                + lfAndLeadingSpaces + "             and schema_id = schema_id( " + schemaLiteral + " ) "
                + lfAndLeadingSpaces + "          ) "
                + lfAndLeadingSpaces + "  begin "
                + lfAndLeadingSpaces + "    drop table " + table.FullName + ";"
                + lfAndLeadingSpaces + "  end "
                ;
        }

        public List<string> DropTableIfExistsSql(ITable table, int spacesToIndent = 0)
        {
            ValidateVableParameter(table, "table", "DropTableIfExistsSql(table, spacesToIndent)");
            return ListFromString(DropTableIfExistsSqlString(table, spacesToIndent));
        }
        public Dictionary<string, int> TableColumnsOutputStructure()
        {
            Dictionary<string, int> retval = new Dictionary<string, int>();
            retval["column_id"] = 0;
            retval["column_name"] = retval["column_id"] + 1;
            retval["data_type"] = retval["column_name"] + 1;
            retval["character_max_length"] = retval["data_type"] + 1;
            retval["is_identity"] = retval["character_max_length"] + 1;
            retval["is_nullable"] = retval["is_identity"] + 1;
            retval["is_computed"] = retval["is_nullable"] + 1;
            retval["definition"] = retval["is_computed"] + 1;
            retval["is_last_update_dtm"] = retval["definition"] + 1;
            retval["is_last_updater"] = retval["is_last_update_dtm"] + 1;
            retval["is_persisted"] = retval["is_last_updater"] + 1;
            retval["default_id"] = retval["is_persisted"] + 1;
            retval["default_name"] = retval["default_id"] + 1;
            retval["default_definition"] = retval["default_name"] + 1;
            retval["check_id"] = retval["default_definition"] + 1;
            retval["check_name"] = retval["check_id"] + 1;
            retval["check_definition"] = retval["check_name"] + 1;
            return retval;
        }
        private List<string> TableColumnsSql(ITable table, int spacesToIndent, string additionalConditions, int maxRowCount = 0)
        {
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string topNClause = (maxRowCount > 0 ? "top(" + maxRowCount.ToString() + ") " : "");
            string additionalConditionsLines
                = (additionalConditions == null ? "" : additionalConditions.Replace("\n", lfAndLeadingSpaces + "   "));
            string nameLiteral = SqlStringLiteral(table.Name);
            string schemaLiteral = SqlStringLiteral(Coalesce(NameOf(table.Owner), "dbo"));
            string databaseFullNameWithDot = FullNameOf(table.Database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            return ListFromString(
                ""
                + lfAndLeadingSpaces + "select " + topNClause + "col.column_id "
                + lfAndLeadingSpaces + "     , col.name as column_name "
                + lfAndLeadingSpaces + "     , dtype.name as data_type "
                + lfAndLeadingSpaces + "     , case "
                + lfAndLeadingSpaces + "           when col.max_length <> -1 "
                + lfAndLeadingSpaces + "             then case substring( dtype.name, 1, 1 ) "
                + " when 'n' then col.max_length / 2 "
                + " else col.max_length "
                + " end "
                + lfAndLeadingSpaces + "           else -1 "
                + lfAndLeadingSpaces + "           end "
                + lfAndLeadingSpaces + "         as character_max_length "
                + lfAndLeadingSpaces + "     , col.is_identity "
                + lfAndLeadingSpaces + "     , col.is_nullable "
                + lfAndLeadingSpaces + "     , col.is_computed "
                + lfAndLeadingSpaces + "     , cc.definition "
                + lfAndLeadingSpaces + "     , cast( case lower( col.name ) when 'LASTUPDATEDTM' then 1 else 0 end as bit ) as is_last_update_dtm "
                + lfAndLeadingSpaces + "     , cast( case lower( col.name ) when 'LASTUPDATER' then 1 else 0 end as bit ) as is_last_updater "
                + lfAndLeadingSpaces + "     , coalesce( cc.is_persisted, 0 ) as is_persisted "
                + lfAndLeadingSpaces + "     , dflt.object_id as default_id "
                + lfAndLeadingSpaces + "     , dflt.name as default_name "
                + lfAndLeadingSpaces + "     , dflt.definition as default_definition "
                + lfAndLeadingSpaces + "     , chck.object_id as check_id "
                + lfAndLeadingSpaces + "     , chck.name as check_name "
                + lfAndLeadingSpaces + "     , chck.definition as check_definition "
                + lfAndLeadingSpaces + "  from " + databaseFullNameWithDot + "sys.columns col "
                + lfAndLeadingSpaces + "         join " + databaseFullNameWithDot + "sys.types dtype "
                + lfAndLeadingSpaces + "           on dtype.user_type_id = col.user_type_id "
                + lfAndLeadingSpaces + "         left join " + databaseFullNameWithDot + "sys.computed_columns cc "
                + lfAndLeadingSpaces + "           on cc.column_id = col.column_id "
                + lfAndLeadingSpaces + "          and cc.object_id = col.object_id "
                + lfAndLeadingSpaces + "         left join " + databaseFullNameWithDot + "sys.default_constraints dflt "
                + lfAndLeadingSpaces + "           on dflt.parent_column_id = col.column_id "
                + lfAndLeadingSpaces + "          and dflt.parent_object_id = col.object_id "
                + lfAndLeadingSpaces + " where col.object_id = object_id( " + nameLiteral + " ) "
                + lfAndLeadingSpaces + "   and col.schema_id = schema_id( " + schemaLiteral + " ) "
                + additionalConditionsLines
                + lfAndLeadingSpaces + " order by col.column_id "
                );
        }
        public List<string> TableColumnsSql(ITable table, int spacesToIndent = 0)
        {
            ValidateVableParameter(table, "table", "TableColumnsSql(table)");
            return TableColumnsSql(table, spacesToIndent, null);
        }

        public List<string> TableIdentityColumnSql(ITable table, int spacesToIndent = 0)
        {
            ValidateVableParameter(table, "table", "TableColumnsSql(table)");
            return TableColumnsSql(
                table
                , spacesToIndent
                , "and col.is_identity = 1"
                , 1
                );
        }

        public List<string> TableIdentityColumnNameSql(ITable table, int spacesToIndent = 0)
        {
            ValidateVableParameter(table, "table", "TableColumnsSql(table)");
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string nameLiteral = SqlStringLiteral(table.Name);
            string schemaLiteral = SqlStringLiteral(Coalesce(NameOf(table.Owner), "dbo"));
            string databaseFullNameWithDot = FullNameOf(table.Database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            return ListFromString(
                ""
                + lfAndLeadingSpaces + "select top(1) col.name "
                + lfAndLeadingSpaces + "  from " + databaseFullNameWithDot + "sys.columns col "
                + lfAndLeadingSpaces + " where col.is_identity = 1 "
                + lfAndLeadingSpaces + "   and col.object_id = object_id( " + nameLiteral + " ) "
                + lfAndLeadingSpaces + "   and col.schema_id = schema_id( " + schemaLiteral + " ) "
                );
        }
        
        public List<string> TableComputedColumnsSql(ITable table, int spacesToIndent = 0)
        {
            ValidateVableParameter(table, "table", "TableColumnsSql(table)");
            return TableColumnsSql(
                table
                , spacesToIndent
                , "and col.is_computed = 1"
                );
        }

        public Dictionary<string, int> VableForeignKeyDependentsOutputStructure()
        {

            Dictionary<string, int> retval = new Dictionary<string, int>();
            retval["dependent_id"] = 0;
            retval["dependent_name"] = retval["dependent_id"] + 1;
            retval["dependent_col_name"] = retval["dependent_name"] + 1;
            retval["dependent_schema_id"] = retval["dependent_col_name"] + 1;
            retval["dependent_schema_name"] = retval["dependent_schema_id"] + 1;
            retval["foreign_key_name"] = retval["dependent_schema_name"] + 1;
            retval["referenced_col_name"] = retval["foreign_key_name"] + 1;
            retval["cascade_on_delete"] = retval["referenced_col_name"] + 1;
            retval["cascade_on_update"] = retval["cascade_on_delete"] + 1;
            return retval;
        }
        public List<string> VableForeignKeyDependentsSql(IVable vable, int spacesToIndent = 0)
        {
            ValidateVableParameter(vable, "vable", "VableForeignKeyDependentsSql(table, spacesToIndent)");
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string nameLiteral = SqlStringLiteral(vable.Name);
            string schemaLiteral = SqlStringLiteral(Coalesce(NameOf(vable.Owner), "dbo"));
            string databaseFullNameWithDot = FullNameOf(vable.Database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            return ListFromString(
                ""
                + lfAndLeadingSpaces + "select fk.parent_object_id                                        as dependent_id "
                + lfAndLeadingSpaces + "     , object_name( fk.parent_object_id )                         as dependent_name "
                + lfAndLeadingSpaces + "     , dependent_col.name                                         as dependent_col_name "
                + lfAndLeadingSpaces + "     , object_schema_id( fk.parent_object_id )                    as dependent_schema_id "
                + lfAndLeadingSpaces + "     , object_schema_name( fk.parent_object_id )                  as dependent_schema_name "
                + lfAndLeadingSpaces + "     , object_name(fk.constraint_object_id)                       as foreign_key_name "
                + lfAndLeadingSpaces + "     , referenced_col.name                                        as referenced_col_name "
                + lfAndLeadingSpaces + "     , case fk.delete_referential_action when 1 then 1 else 0 end as cascade_on_delete "
                + lfAndLeadingSpaces + "     , case fk.update_referential_action when 1 then 1 else 0 end as cascade_on_update "
                + lfAndLeadingSpaces + "  from " + databaseFullNameWithDot + "sys.foreign_key_columns fk "
                + lfAndLeadingSpaces + "         join " + databaseFullNameWithDot + "sys.columns dependent_col "
                + lfAndLeadingSpaces + "           on dependent_col.object_id = fk.parent_object_id "
                + lfAndLeadingSpaces + "          and dependent_col.column_id = fk.parent_column_id "
                + lfAndLeadingSpaces + "         join " + databaseFullNameWithDot + "sys.columns referenced_col "
                + lfAndLeadingSpaces + "           on referenced_col.object_id = fk.referenced_object_id "
                + lfAndLeadingSpaces + "          and referenced_col.column_id = fk.referenced_column_id "
                + lfAndLeadingSpaces + " where fk.referenced_object_id = object_id( " + nameLiteral + ", 'U' ) "
                + lfAndLeadingSpaces + "   and object_schema_name( fk.referenced_object_id ) = " + schemaLiteral + " ) "
                );
        }

        // Trigger

        private void ValidateTriggerAsArgument(ITableTrigger trigger, string callingMethod)
        {
            ValidateTableElementAsArgument(
                trigger
                , "trigger"
                , "trigger"
                , callingMethod + "(trigger, spacesToIndent)"
                );
        }
        public List<string> IDOfTriggerSql(
            string name
            , string tableName
            , string schemaName = null
            , IDatabase database = null
            , int spacesToIndent = 0
            , bool terminatesStatement = true
            )
        {
            ValidateArgumentsOfVableElementIDFunction(name, tableName, "tableName", "IDOfTriggerSql(name, tableName, schema, spacesToIndent, terminatesStatement)");
            string retval;
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            schemaName = Coalesce(schemaName, "dbo");
            string schemaLiteral = SqlStringLiteral(Coalesce(schemaName, "dbo"));
            string databaseFullNameWithDot = FullNameOf(database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            retval = ""
                + lfAndLeadingSpaces + "select min( id ) "
                + lfAndLeadingSpaces + "  from " + databaseFullNameWithDot + "sys.triggers "
                + lfAndLeadingSpaces + " where name = " + SqlStringLiteral(name)
                + lfAndLeadingSpaces + "   and parent_object_id = object_id( " + SqlStringLiteral(tableName) + " ) "
                + lfAndLeadingSpaces + "   and schema_id = schema_id( " + SqlStringLiteral(schemaName) + " ) "
                ;
            if (terminatesStatement)
                retval += lfAndLeadingSpaces + ";";
            return ListFromString(retval);
        }
        private string EnsureTriggerExistsSqlString(ITableTrigger trigger, int spacesToIndent = 0)
        {
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string nameLiteral = SqlStringLiteral(trigger.Name);
            string tableLiteral = SqlStringLiteral(trigger.ParentObject.Name);
            string schemaLiteral = SqlStringLiteral(Coalesce(NameOf(trigger.Schema), "dbo"));
            string databaseFullNameWithDot = FullNameOf(trigger.Database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            string sql =
                ""
                + lfAndLeadingSpaces + "if not exists ( "
                + lfAndLeadingSpaces + "              select top(1) 1 "
                + lfAndLeadingSpaces + "                from " + databaseFullNameWithDot + "sys.triggers "
                + lfAndLeadingSpaces + "               where name = " + nameLiteral
                + lfAndLeadingSpaces + "                 and parent_object_id = object_id( " + tableLiteral + " ) "
                + lfAndLeadingSpaces + "                 and schema_id = schema_id( " + schemaLiteral + " ) "
                + lfAndLeadingSpaces + "              ) "
                + lfAndLeadingSpaces + "begin "
                + lfAndLeadingSpaces + "  create trigger " + trigger.Name
                + lfAndLeadingSpaces + "      on " + trigger.Owner.FullName;
            if (trigger.TriggerTiming.Equals(TriggerTimings.BeforeInsert))
                sql += lfAndLeadingSpaces + "  instead of insert";
            if (trigger.TriggerTiming.Equals(TriggerTimings.BeforeUpdate))
                sql += lfAndLeadingSpaces + "  instead of update";
            if (trigger.TriggerTiming.Equals(TriggerTimings.BeforeDelete))
                sql += lfAndLeadingSpaces + "  instead of delete";
            if (trigger.TriggerTiming.Equals(TriggerTimings.AfterInsert))
                sql += lfAndLeadingSpaces + "  after insert";
            if (trigger.TriggerTiming.Equals(TriggerTimings.AfterAnything))
                sql += lfAndLeadingSpaces + "  after insert, update, delete";
            sql += ""
                + lfAndLeadingSpaces + "  as"
                + lfAndLeadingSpaces + "  begin"
                + lfAndLeadingSpaces + trigger.TriggerText.Replace("\n", "\n    ")
                + lfAndLeadingSpaces + "  end"
                + lfAndLeadingSpaces + "end";
            return sql;
        }
        public List<string> EnsureTriggerExistsSql(ITableTrigger trigger, int spacesToIndent = 0)
        {
            ValidateTriggerAsArgument(trigger, "EnsureTriggerExistsSql");
            return ListFromString(EnsureTriggerExistsSqlString(trigger, spacesToIndent));
        }
        public List<string> EnsureTriggerExistsAsDesignedSql(ITableTrigger trigger, int spacesToIndent = 0)
        {
            List<string> retval = new List<string>();
            retval.Add(DropTriggerIfExistsSqlString(trigger, spacesToIndent));
            retval.Add(CommitFullySql());
            retval.Add(EnsureTriggerExistsSqlString(trigger, spacesToIndent));
            return retval;
        }
        private string DropTriggerIfExistsSqlString(ITableTrigger trigger, int spacesToIndent = 0)
        {
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string nameLiteral = SqlStringLiteral(trigger.Name);
            string tableLiteral = SqlStringLiteral(trigger.ParentObject.Name);
            string schemaLiteral = SqlStringLiteral(Coalesce(NameOf(trigger.Schema), "dbo"));
            string databaseFullNameWithDot = FullNameOf(trigger.Database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            string sql =
                ""
                + lfAndLeadingSpaces + "if exists ( "
                + lfAndLeadingSpaces + "          select top(1) 1 "
                + lfAndLeadingSpaces + "            from " + databaseFullNameWithDot + "sys.triggers "
                + lfAndLeadingSpaces + "           where name = " + nameLiteral
                + lfAndLeadingSpaces + "             and parent_object_id = object_id( " + tableLiteral + " ) "
                + lfAndLeadingSpaces + "             and schema_id = schema_id( " + schemaLiteral + " ) "
                + lfAndLeadingSpaces + "          ) "
                + lfAndLeadingSpaces + "begin "
                + lfAndLeadingSpaces + "  drop trigger " + trigger.FullName + ";"
                + lfAndLeadingSpaces + "end";
            return sql;
        }
        public List<string> DropTriggerIfExistsSql(ITableTrigger trigger, int spacesToIndent = 0)
        {
            return ListFromString(DropTriggerIfExistsSqlString(trigger, spacesToIndent));
        }

        string DefaultBeforeInsertTriggerTextSql(
            ITable table
            , int spacesToIndent = 0
            )
        {
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            return ""
                + lfAndLeadingSpaces + "set nocount on;"
                + lfAndLeadingSpaces + "set ansi_warnings, ansi_defaults, xact_abort on;"
                + lfAndLeadingSpaces + "if exists(select top(1) 1 from inserted )"
                + lfAndLeadingSpaces + "begin"
                + lfAndLeadingSpaces + "  exec SPCalledByInsertTrigger "
                + lfAndLeadingSpaces + "         '" + table.Schema.Name + "'"
                + lfAndLeadingSpaces + "       , '" + table.FullName + "'"
                + lfAndLeadingSpaces + "  ;"
                + lfAndLeadingSpaces + "end;";
        }

        public string DefaultCheckNonnullInsertedTriggerTableIDsAgainstParentSql(
            ITable table
            , string tempTableName
            , int spacesToIndent = 0
            )
        {
            if (table == null)
                throw new NullArgumentException(
                    "table"
                    , FullTypeNameForCodeErrors
                    + "CheckInsertedIDsAgainstParentSql(table, tempTableName, timing, spacesToIndent)"
                    );
            if (tempTableName == null)
                throw new NullArgumentException(
                    "tempTableName"
                    , FullTypeNameForCodeErrors
                    + "CheckInsertedIDsAgainstParentSql(table, tempTableName, timing, spacesToIndent)"
                    );
            if (table.ParentTable == null)
                throw new NullArgumentException(
                    "table.ParentTable"
                    , FullTypeNameForCodeErrors
                    + "CheckInsertedIDsAgainstParentSql(table, tempTableName, timing, spacesToIndent)"
                    , "An ITable with a null ParentTable property was passed "
                    + "as the 'table' parameter to the method "
                    + FullTypeNameForCodeErrors
                    + "CheckInsertedIDsAgainstParentSql(table, tempTableName, timing, spacesToIndent). "
                    + "This method is only applicable to tables "
                    + "that are non-root members of a polymorphic hierarchy "
                    + "and that therefore have a parent table."
                    );
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string idColumnName = table.IDColumnName;
            string tableNameWithSchema = table.NameWithSchema;
            ITable parentTable = table.ParentTable;
            string parentTableFullName = parentTable.FullName;
            string parentTableIDColumnName = parentTable.IDColumnName;
            string parentTableHomeVableColumnName 
                = parentTable.HomeVableNameColumn != null ? parentTable.HomeVableNameColumn.Name
                : null;
            string sql = ""
                + lfAndLeadingSpaces + "select top(1) "
                + lfAndLeadingSpaces + "        i." + idColumnName + " as id"
                + lfAndLeadingSpaces + "      , existing." + parentTableHomeVableColumnName + " as homeVableName"
                + lfAndLeadingSpaces + "   from " + tempTableName + " i"
                + lfAndLeadingSpaces + "          left join " + parentTableFullName + " existing"
                + lfAndLeadingSpaces + "            on existing." + parentTableHomeVableColumnName + " = i." + idColumnName
                + lfAndLeadingSpaces + "  where i." + idColumnName + " is not null"
                + lfAndLeadingSpaces + "    and ("
                + lfAndLeadingSpaces + "        existing.id is null"
                + lfAndLeadingSpaces + "        or existing." + parentTableHomeVableColumnName + " != '" + tableNameWithSchema + "'"
                + lfAndLeadingSpaces + "        );"
                ;
            return sql;
        }









        








        public void ResetIdentity(int newValue, IDatabaseSafe db, IConnection conn)
        {
            db.SqlExecuteNonQuery(
                ""
                + "\n create table #identityResetter "
                + "\n       ( "
                + "\n       i int identity(" + newValue.ToString() + ", 1) not null "
                + "\n     , x int not null "
                + "\n       ) "
                + "\n ; "
                + "\n insert into #identityResetter( x ) values( 0 ); "
                + "\n drop table #identityResetter; "
                , conn
                );
        }

        // UniqueKey

        private void ValidateUniqueKeyAsArgument(IUniqueKey key, string callingMethod)
        {
            ValidateTableElementAsArgument(
                key
                , "unique key"
                , "key"
                , callingMethod + "(key, spacesToIndent)"
                );
        }
        public List<string> IDOfUniqueKeySql(
            string name
            , string tableName
            , string schemaName = null
            , IDatabase database = null
            , int spacesToIndent = 0
            , bool terminatesStatement = true
            )
        {
            ValidateArgumentsOfVableElementIDFunction(name, tableName, "tableName", "IDOfUniqueKeySql(name, tableName, schema, spacesToIndent, terminatesStatement)");
            string retval;
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            schemaName = Coalesce(schemaName, "dbo");
            string schemaLiteral = SqlStringLiteral(Coalesce(schemaName, "dbo"));
            string databaseFullNameWithDot = FullNameOf(database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            retval = ""
                + lfAndLeadingSpaces + "select min( id ) "
                + lfAndLeadingSpaces + "  from " + databaseFullNameWithDot + "sys.unique_keys "
                + lfAndLeadingSpaces + " where name = " + SqlStringLiteral(name)
                + lfAndLeadingSpaces + "   and parent_object_id = object_id( " + SqlStringLiteral(tableName) + " ) "
                + lfAndLeadingSpaces + "   and schema_id = schema_id( " + SqlStringLiteral(schemaName) + " ) "
                ;
            if (terminatesStatement)
                retval += lfAndLeadingSpaces + ";";
            return ListFromString(retval);
        }
        private string EnsureUniqueKeyExistsSqlString(IUniqueKey key, int spacesToIndent = 0)
        {
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string nameLiteral = SqlStringLiteral(key.Name);
            string tableLiteral = SqlStringLiteral(key.Owner.Name);
            string schemaLiteral = SqlStringLiteral(Coalesce(NameOf(key.Schema), "dbo"));
            string databaseFullNameWithDot = FullNameOf(key.Database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            string uniqueKeySpec = "unique key ";
            if (key.IsClustered) uniqueKeySpec += "clustered(";
            else uniqueKeySpec += "unclustered(";
            uniqueKeySpec += CommaDelimited(key.ColumnNames) + " )";
            return
                ""
                + lfAndLeadingSpaces + "if not exists ( "
                + "select top(1) 1 "
                + "  from " + databaseFullNameWithDot + "sys.unique_keys "
                + " where name = " + nameLiteral
                + "   and parent_object_id = object_id( " + tableLiteral + " ) "
                + "   and schema_id = schema_id( " + schemaLiteral + " ) "
                + ") "
                + lfAndLeadingSpaces + "  begin "
                + lfAndLeadingSpaces + "    alter table " + key.Owner.FullName
                + lfAndLeadingSpaces + "      add constraint " + key.Name
                + lfAndLeadingSpaces + "            " + uniqueKeySpec
                + lfAndLeadingSpaces + "    ; "
                + lfAndLeadingSpaces + "  end "
                ;
        }
        public List<string> EnsureUniqueKeyExistsSql(IUniqueKey key, int spacesToIndent = 0)
        {
            ValidateUniqueKeyAsArgument(key, "EnsureUniqueKeyExistsSql");
            return ListFromString(EnsureUniqueKeyExistsSqlString(key, spacesToIndent));
        }
        public List<string> EnsureUniqueKeyExistsAsDesignedSql(IUniqueKey key, int spacesToIndent = 0)
        {
            List<string> retval = new List<string>();
            retval.Add(DropUniqueKeyIfExistsSqlString(key, spacesToIndent));
            retval.Add(CommitFullySql());
            retval.Add(EnsureUniqueKeyExistsSqlString(key, spacesToIndent));
            return retval;
        }
        private string DropUniqueKeyIfExistsSqlString(IUniqueKey key, int spacesToIndent = 0)
        {
            return DropConstraintIfExistsSqlString(key, "sys.unique_keys", spacesToIndent);
        }
        public List<string> DropUniqueKeyIfExistsSql(IUniqueKey key, int spacesToIndent = 0)
        {
            return ListFromString(DropUniqueKeyIfExistsSqlString(key, spacesToIndent));
        }

        // View
        public List<string> IDOfViewSql(
            string name
            , string schemaName = null
            , IDatabase database = null
            , int spacesToIndent = 0
            , bool terminatesStatement = true
            )
        {
            string retval;
            if (name == null)
                throw new ArgumentNullException(
                    paramName: "name"
                    , message: ""
                    + "Cannot provide null as 'name' argument to "
                    + this.GetType().FullName
                    + ".IDOfViewSql( name, schemaName, database, spacesToIndent, terminatesStatement )."
                    );
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string schemaLiteral = SqlStringLiteral(Coalesce(schemaName, "dbo"));
            string databaseFullNameWithDot = FullNameOf(database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            retval = ""
                + lfAndLeadingSpaces + "select min( id ) "
                + lfAndLeadingSpaces + "  from " + databaseFullNameWithDot + "sys.views "
                + lfAndLeadingSpaces + " where name = " + SqlStringLiteral(name)
                + lfAndLeadingSpaces + "   and schema_id = schema_id( " + SqlStringLiteral(schemaName) + " ) "
                ;
            if (terminatesStatement)
                retval += lfAndLeadingSpaces + ";";
            return ListFromString(retval);
        }
        private string EnsureViewExistsSqlString(IView view, int spacesToIndent = 0)
        {
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string nameLiteral = SqlStringLiteral(view.Name);
            string schemaLiteral = SqlStringLiteral(Coalesce(NameOf(view.Owner), "dbo"));
            string databaseFullNameWithDot = FullNameOf(view.Database);
            string paramList = "";
            string paramLeader = lfAndLeadingSpaces + "         ";
            string selectStatement = view.SelectStatement.Replace("\n", lfAndLeadingSpaces + "  ");
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            return
                ""
                + lfAndLeadingSpaces + "create or alter view " + view.FullName + paramList
                + lfAndLeadingSpaces + "as "
                + lfAndLeadingSpaces + selectStatement
                + lfAndLeadingSpaces + ";"
                ;
        }
        public List<string> EnsureViewExistsSql(IView view, int spacesToIndent = 0)
        {
            if (view == null)
                throw new ArgumentNullException(
                    paramName: "view"
                    , message: ""
                    + "Cannot provide null as 'view' argument to "
                    + this.GetType().FullName
                    + ".EnsureViewExistsSql(view, spacesToIndent)."
                    );
            if (view.Name == null)
                throw new ArgumentNullException(
                    paramName: "view.Name"
                    , message: ""
                    + "Cannot provide view with null Name as 'view' argument to "
                    + this.GetType().FullName
                    + ".EnsureViewExistsSql(view, spacesToIndent)."
                    );
            return ListFromString(EnsureViewExistsSqlString(view, spacesToIndent));
        }
        public List<string> EnsureViewExistsAsDesignedSql(IView view, int spacesToIndent = 0)
        {
            return EnsureViewExistsSql(view, spacesToIndent);
        }
        private string DropViewIfExistsSqlString(IView view, int spacesToIndent = 0)
        {
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            return lfAndLeadingSpaces + "drop view if exists " + view.FullName + ";";
        }
        public List<string> DropViewIfExistsSql(IView view, int spacesToIndent = 0)
        {
            if (view == null)
                throw new ArgumentNullException(
                    paramName: "view"
                    , message: ""
                    + "Cannot provide null as 'view' argument to "
                    + this.GetType().FullName
                    + ".EnsureViewExistsSql(view, spacesToIndent)."
                    );
            if (view.Name == null)
                throw new ArgumentNullException(
                    paramName: "view.Name"
                    , message: ""
                    + "Cannot provide view with null Name as 'view' argument to "
                    + this.GetType().FullName
                    + ".EnsureViewExistsSql(view, spacesToIndent)."
                    );
            return ListFromString(DropViewIfExistsSqlString(view, spacesToIndent));
        }

        // Queues

        public string AddIntoQueuesSql(string columnList, string inputList, string schema)
        {
            string sql = "declare @p table(col1 int); "
                    + "insert into " + schema + ".Queues (" + columnList + ") output INSERTED.id "
                    + "into @p values (" + inputList + "); "
                    + "select * from @p;";
            return sql;
        }

        // Boolean
        public string FalseSql() { return "0"; }
        public string TrueSql() { return "1"; }
        public string BooleanTypeSql() { return "bit"; }

        // Transaction management
        public string CommitFullySql() { return "while @@TRANCOUNT > 0 commit transaction;"; }
        public string RollbackSql() { return "rollback;"; }

        // General utility
        public string CurrentTimestampSql { get { return "current_timestamp"; } }
        public DateTime CurrentTimestamp( IDatabase db, IConnection conn )
        {
            return
                (DateTime)
                db.SqlExecSingleValue("select current_timestamp", null, conn);
        }
        public string CurrentUserSql { get { return "original_login"; } }
        public string CurrentUser(IDatabase db, IConnection conn)
        {
            return
                (string)
                db.SqlExecSingleValue("select original_login", conn);
        }
        public string TrimSql(string toBeTrimmed) { return "trim(" + toBeTrimmed + ")"; }
        public int TrimSqlAdditionalLength() { return 6; } // "trim()".Length
        public string NRowedTableSql(
            int n
            , string selectedExpression = null
            , int? adder = null
            , string columnName = null
            , int spacesToIndent = 0 
            )
        {
            // HERE HERE HERE throw exception if n <= 0
            int leftmostPlace = (int)Math.Log10(n);
            int m = (int)Math.Pow(10, leftmostPlace);
            int leftmostDigit = n / m;
            if (selectedExpression == null)
            {
                selectedExpression
                    = (adder == null ? "0" : adder.ToString());
                for (int i = leftmostPlace; i >= 0; i--)
                    selectedExpression
                        += " + 1" + new string('0', i) + "*i";
                selectedExpression
                    = " " + (columnName != null ? columnName : "i");
            }
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string sql = "select 1 as x"
                + lfAndLeadingSpaces + "  from ";
            string fromRow = "";
            string values = "( values( 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 )) as d";
            for (int i = 0; i < leftmostPlace; i++)
            {
                fromRow
                    = i > 0 ? "     , " : "       "
                    + values
                    + i
                    + "(i)";
                sql += lfAndLeadingSpaces + fromRow;
            }
            fromRow
                = (m > 0 ? "     , " : "       ")
                + "( values( 0";
            for (int i = 1; i < leftmostDigit; i++)
                fromRow += ", " + i;
            fromRow += " )) as d" + leftmostPlace;
            sql += lfAndLeadingSpaces + fromRow;
            if (leftmostPlace  > 0)
            {
                sql += ""
                    + lfAndLeadingSpaces + "union "
                    + lfAndLeadingSpaces 
                    + NRowedTableSql( 
                        n % m
                        , selectedExpression
                        , selectedExpression != null
                        ? null
                        : n * m + ( adder == null ? 0 : adder )
                        , columnName
                        , spacesToIndent
                        );
            }
            return sql;
        }
        public string ObjectValueToSql(Object obj, string dataType)
        {
            if (dataType.Contains("char")) return SqlStringLiteral(obj.ToString());
            else if (dataType.Equals("date")) return ((DateTime)obj).ToString("yyyyMMdd");
            else if (dataType.Equals("time")) return ((DateTime)obj).ToString("HH:mm:ss.fff");
            else if (dataType.Contains("date")) return ((DateTime)obj).ToString("yyyyMMdd HH:mm:ss.fff");
            else return obj.ToString();
        }
        public bool IsValidObjectName(string name)
        {
            return new Regex("^#{0,2}(\\w|_)+$").IsMatch(name);
        }
        public string ValidObjectNameFrom(string name)
        {
            int leadingHashCount = new Regex("^#{0,2}").Match(name).Length;
            string working = name.Substring(leadingHashCount);
            working = Regex.Replace(working, "\\W", "");
            return new string('#', leadingHashCount) + working;
        }
        public string TableConstraintName(string root, string tableName)
        {
            return ValidObjectNameFrom(root);
        }
        public string TrimmingConstraintText(string name)
        {
            return name + " not like ' %' and " + name + " not like '% ' ";
        }
        public IObjectType SequencePhysicalObjectType()
        {
            return ObjectTypes.Table();
        }

        /*** 
         * Convenience methods
         ***/
        private string FullTypeNameForCodeErrors { get { return typeof(SqlTranslatorSqlServer2014).FullName; } }
        private string CommaDelimited(List<string> list)
        {
            if (list == null) return null;
            else
            {
                string retval = "";
                foreach (string s in list) retval += ", " + s;
                return retval.Substring(2);
            }
        }
        private string LFAndLeadingSpaces(int spacesToIndent)
        {
            return "\n" + new string(' ', spacesToIndent);
        }
        private List<string> ListFromString(string s)
        {
            List<string> retval = new List<string>();
            retval.Add(s);
            return retval;
        }
        private string Coalesce( string s, string valueIfNull )
        {
            if (s != null) return s;
            else return valueIfNull;
        }
        private string DropConstraintIfExistsSqlString(IDatabaseObject constraint, string systemViewName, int spacesToIndent = 0)
        {
            // Since it's private we will assume we do not have to worry that
            //   a database object that isn't actually a constraint will
            //   be passed in.
            List<string> script = new List<string>();
            string lfAndLeadingSpaces = LFAndLeadingSpaces(spacesToIndent);
            string nameLiteral = SqlStringLiteral(constraint.Name);
            string tableLiteral = SqlStringLiteral(constraint.Owner.Name);
            string schemaLiteral = SqlStringLiteral(Coalesce(NameOf(constraint.Schema), "dbo"));
            string databaseFullNameWithDot = FullNameOf(constraint.Database);
            if (databaseFullNameWithDot != null) databaseFullNameWithDot += ".";
            else databaseFullNameWithDot = "";
            return ""
                + lfAndLeadingSpaces + "if exists ( "
                + lfAndLeadingSpaces + "          select top(1) 1 "
                + lfAndLeadingSpaces + "            from " + databaseFullNameWithDot + systemViewName
                + lfAndLeadingSpaces + "           where name = " + nameLiteral
                + lfAndLeadingSpaces + "             and parent_object_id = object_id( " + tableLiteral + " ) "
                + lfAndLeadingSpaces + "             and schema_id = schema_id( " + schemaLiteral + " ) "
                + lfAndLeadingSpaces + "          ) "
                + lfAndLeadingSpaces + "  begin "
                + lfAndLeadingSpaces + "    alter table " + constraint.Owner.FullName
                + lfAndLeadingSpaces + "     drop constraint " + constraint.Name
                + lfAndLeadingSpaces + "    ; "
                + lfAndLeadingSpaces + "  end "
                ;
        }
        private string FullNameOf(IFullNamed obj, string defaultValue = null)
        {
            return UtilitiesForIFullNamed.FullNameOf(obj, defaultValue);
        }
        private string If( bool condition, string valueIfTrue, string valueIfFalse )
        {
            if (condition) return valueIfTrue;
            else return valueIfFalse;
        }
        private string NameOf(INamed obj, string defaultValue = null)
        {
            return UtilitiesForINamed.NameOf(obj, defaultValue);
        }
        private string SqlStringLiteral( string s ) { return "'" + s.Replace("'", "''") + "'"; }

        private void ValidateTableElementAsArgument(IDatabaseObject element, string objectTypeName, string paramName, string callingMethod)
        {
            UtilitiesForINamed.NonnullNamedParameterIsValid(
                element
                , objectTypeName
                , paramName
                , this.GetType().FullName + "." + callingMethod
                );
            UtilitiesForINamed.NonnullNamedParameterIsValid(
                element.Owner
                , objectTypeName
                , paramName + ".Owner"
                , this.GetType().FullName + "." + callingMethod
                );
        }

        private void ValidateArgumentsOfVableElementIDFunction(string name, string vableName, string vableNameParamName, string callingMethod)
        {
            if (name == null)
                throw new ArgumentNullException(
                    paramName: "name"
                    , message: ""
                    + "Cannot provide null as 'name' argument to "
                    + this.GetType().FullName + "." + callingMethod + "."
                    );
            if (vableName == null)
                throw new ArgumentNullException(
                    paramName: vableNameParamName
                    , message: ""
                    + "Cannot provide null as '" + vableNameParamName + "' argument to "
                    + this.GetType().FullName + "." + callingMethod + "."
                    );
        }

    }

    public class BadSqlTypeException: Exception
    {
        private BadSqlTypeException() { }
        public BadSqlTypeException( 
            ISqlDataType badType
            , string message = null
            ): base(
                message != null ? message
                : "Data type "
                + ( badType == null ? "[unknown]" : badType.GetType().FullName )
                + " is not supported by SQL Server 2014."
                )
        {
            BadType = badType;
        }
        ISqlDataType BadType { get; }
    }

}
