﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.SqlServer.Server;
using Com.VeritasTS.DatabaseUtilityInterfaces;

namespace Com.VeritasTS.DbUtilitiesSqlServer
{

    public class TransactionSqlServer : ITransaction
    {

        public TransactionSqlServer(SqlTransaction transaction, IConnection connection)
        {
            Transaction = transaction;
            Connection = connection;
        }

        ~TransactionSqlServer()
        {
            if (Transaction != null) Transaction.Rollback();
        }

        public SqlTransaction Transaction { get; }

        public IConnection Connection { get; }

        public void Rollback() { Transaction.Rollback(); }

        public void Commit() { Transaction.Commit(); }

    }
}
