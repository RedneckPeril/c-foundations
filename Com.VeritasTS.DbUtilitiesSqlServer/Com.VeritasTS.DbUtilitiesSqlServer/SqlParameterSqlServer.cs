﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Microsoft.SqlServer.Server;
using Com.VeritasTS.DatabaseUtilityInterfaces;

namespace Com.VeritasTS.DbUtilitiesSqlServer
{

    public class SqlParameterSqlServer : ISqlParameter
    {

        public SqlParameterSqlServer(SqlParameter parameter)
        {
            Parameter = parameter;
        }

        public SqlParameterSqlServer(string name, SqlDbType dataType, object value, ParameterDirection direction, int size = -1)
        {
            Parameter = new SqlParameter(name, dataType);
            Parameter.Value = value;
            Parameter.Direction = direction;
            if (size > 0) Parameter.Size = size;
        }

        public SqlParameterSqlServer(string name, string dataType, object value, ParameterDirection direction, int size = -1)
        {
            Parameter = new SqlParameter(name, dataType);
            Parameter.Value = value;
            Parameter.Direction = direction;
            if (size > 0) Parameter.Size = size;
        }

        public SqlParameter Parameter { get; }

        public string Name { get { return Parameter.ParameterName; } }

        public bool IsOutput
        {
            get
            {
                if (Parameter == null) return false;
                else return = (Parameter.Direction == ParameterDirection.Input || Parameter.Direction == ParameterDirection.Output);
            }
        }

        public ISafeMemberOfImmutableOwner GetSafeReference()
        {
            SqlParameter newNative =
                new SqlParameter(
                    Parameter.ParameterName
                    , Parameter.SqlDbType
                    , Parameter.Size
                    , Parameter.Direction
                    , Parameter.IsNullable
                    , Parameter.Precision
                    , Parameter.Scale
                    , Parameter.SourceColumn
                    , Parameter.SourceVersion
                    , Parameter.Value
                    )
                {
                    CompareInfo = Parameter.CompareInfo
                    ,
                    SqlValue = Parameter.SqlValue
                    ,
                    TypeName = Parameter.TypeName
                    ,
                    UdtTypeName = Parameter.UdtTypeName
                };
            return new SqlParameterSqlServer(newNative);
        }

    }

}
