﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Microsoft.SqlServer.Server;

using Com.VeritasTS.DatabaseUtilityInterfaces;

namespace Com.VeritasTS.DbUtilitiesSqlServer
{

    public class DataReaderSqlServer : IDataReader
    {

        private SqlDataReader _dataReader;

        public DataReaderSqlServer(SqlDataReader dataReader)
        {
            _dataReader = dataReader;
        }

        ~DataReaderSqlServer()
        {
            Dispose();
        }

        public SqlDataReader DataReader { get { return _dataReader; } }

        public bool Read() { return _dataReader.Read(); }

        public int FieldCount { get { return _dataReader.FieldCount; } }

        public object GetValue(int i) { return _dataReader.GetValue(i); }

        public void Close()
        {
            if (_dataReader != null)
            {
                _dataReader.Close();
            }
        }

        public void Dispose()
        {
            Close();
            _dataReader = null;
        }
    }

}
