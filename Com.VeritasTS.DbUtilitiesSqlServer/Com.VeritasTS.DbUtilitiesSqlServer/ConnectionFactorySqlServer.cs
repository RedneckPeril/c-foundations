﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Com.VeritasTS.DatabaseUtilityInterfaces;

namespace Com.VeritasTS.DbUtilitiesSqlServer
{

    public class ConnectionFactorySqlServer : IConnectionSupplierSafe
    {
        public ConnectionFactorySqlServer(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public string ConnectionString { get; }

        public bool ConnectionStringMatches(ConnectionFactorySqlServer other)
        {
            if (ConnectionString != null)
            {
                if (other == null) return false;
                else return (ConnectionString.Equals(other.ConnectionString));
            }
            else if (other == null) return true;
            else return (other.ConnectionString == null);
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            else if (!obj.GetType().Equals(this.GetType())) return false;
            else return ConnectionStringMatches((ConnectionFactorySqlServer)obj);
        }

        public override int GetHashCode()
        {
            if (ConnectionString != null) return ConnectionString.GetHashCode();
            else return 0;
        }

        /*** 
         * ISelfAsserter methods
         ***/
        public bool PassesSelfAssertion(Type testableType = null) { return true; }

        public IConnection GetAConnection()
        {
            IConnection retval = new ConnectionSqlServer(ConnectionString);
            try
            {
                retval.Open();
            }
            catch (Exception e)
            {
#if DEBUG
                Console.WriteLine("DatabaseMutable.OpenDbConnection:" + e.Message);
                System.Diagnostics.Debug.WriteLine("DatabaseMutable.OpenDbConnection:" + e.Message);
#endif
                throw;
            }
            return retval;
        }

        public void ResumeControlOfConnection(IConnection conn, ITransaction trans = null)
        {
            conn.Close();
            conn.Dispose();
        }

        public ISafeMemberOfImmutableOwner GetSafeReference()
        {
            return new ConnectionFactorySqlServer(ConnectionString);
        }

        public string DatabaseName()
        {
            get
            {

                using (SqlConnection conn = new SqlConnection("context connection=true"))
                //using (SqlConnection conn = new SqlConnection(@"Data Source=SERVER02\SQLEXPRESS;Initial Catalog=Mazi_test;Integrated Security=True"))
                {
                    conn.Open();
                }
        }
    }

}
