﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Microsoft.SqlServer.Server;
using Com.VeritasTS.DatabaseUtilityInterfaces;

namespace Com.VeritasTS.DbUtilitiesSqlServer
{

    public class ConnectionSqlServer : IConnection
    {

        public ConnectionSqlServer(SqlConnection connection)
        {
            Connection = new SqlConnection();
        }

        public ConnectionSqlServer(string connectionString)
        {
            Connection = new SqlConnection();
            Connection.ConnectionString = connectionString;
        }

        ~ConnectionSqlServer()
        {
            if (Connection != null) Connection.Dispose();
        }

        public SqlConnection Connection { get; }

        public bool IsOpen
        {
            get
            {
                if (Connection == null) return false;
                if (!Connection.State.HasFlag(ConnectionState.Open)) return false;
                return true;
            }
        }

        public void Open() { Connection.Open(); }

        public void Close() { Connection.Close(); }

        public void Dispose()
        {
            if (Connection != null)
            {
                Connection.Close();
                Connection.Dispose();
            }
        }

        public ICommand GetNewCommand(
            string commandText
            , ITransaction transaction = null
            , List<ISqlParameter> parameters
            )
        {
            SqlCommand nativeCommand = new SqlCommand(commandText, Connection);
            foreach (ISqlParameter p in parameters)
            {
                if (typeof(SqlParameterSqlServer).IsAssignableFrom(p.GetType()))
                    nativeCommand.Parameters.Add(p.);
            }
            return new CommandSqlServer(nativeCommand, this, transaction);
        }

        public IStoredProcedureCommand GetNewStoredProcedure(string commandText, List<ISqlParameter> parameters, ITransaction transaction = null)
        {
            SqlCommand nativeCommand = new SqlCommand(commandText, Connection);
            nativeCommand.CommandType = CommandType.StoredProcedure;
            return new StoredProcedureSqlServer(nativeCommand, parameters, this, transaction);
        }

        public ITransaction BeginTransaction()
        {
            return new TransactionSqlServer(Connection.BeginTransaction(), this);
        }

        public string DatabaseName()
        {
            bool wasOpen = IsOpen;
            if (!wasOpen) Open();
            string retval = Connection.Database;
            if (!wasOpen) Close();
            return retval;
        }

        public virtual bool PassesSelfAssertion(Type testableType = null)
        {
            if (!SkipSelfAssertionUnlessThisType(testableType))
            {
                Debug.Assert(
                    Connection != null
                    , "Connection method of ConnectionSqlServer object "
                    + "cannot be null."
                    );
            }
            return true;
        }

        public bool SkipSelfAssertionUnlessThisType(Type testableType = null)
        {
            if (testableType == null) return false;
            return (!testableType.Equals(this.GetType()));
        }

    }

}
