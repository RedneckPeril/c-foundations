﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Microsoft.SqlServer.Server;

using Com.VeritasTS.DatabaseUtilityInterfaces;

namespace Com.VeritasTS.DbUtilitiesSqlServer
{

    public class CommandSqlServer : ICommand
    {
        private SqlCommand _command;
        private 
        private SqlTransaction _transaction;
        public CommandSqlServer(
            SqlCommand command
            , IConnection connection
            , List<ISqlParameter> parameters = null
            , ITransaction transaction = null
            )
        {
            _command = command;
            Connection = connection;
            _transaction = transaction;
            if (transaction != null) _command.Transaction = ((TransactionSqlServer)transaction).Transaction;
        }
        public SqlCommand Command { get { return _command; } }
        public string CommandText { get { return Command.CommandText; } }
        public IConnection Connection { get; }
        public ITransaction Transaction { get; }
        public IDataReader ExecuteReader()
        {
            return new DataReaderSqlServer(Command.ExecuteReader());
        }
        public void ExecuteNonQuery() { Command.ExecuteNonQuery(); }
        public object ExecuteSingleValue() { return Command.ExecuteScalar(); }
        public void Dispose()
        {
            if (_command != null)
            {
                _command.Dispose();
                _command = null;
            }
        }
    }

}
