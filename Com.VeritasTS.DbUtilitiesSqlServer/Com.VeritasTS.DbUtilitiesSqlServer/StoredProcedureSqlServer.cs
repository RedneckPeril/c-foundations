﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Microsoft.SqlServer.Server;

using Com.VeritasTS.DatabaseUtilityInterfaces;

namespace Com.VeritasTS.DbUtilitiesSqlServer
{

    public class StoredProcedureSqlServer : CommandSqlServer, IStoredProcedureCommand
    {

        public StoredProcedureSqlServer(
            SqlCommand command
            , List<ISqlParameter> parameters
            , IConnection connection
            , ITransaction transaction = null) : base(command, connection, transaction)
        {
            SetParameters(parameters);
        }

        public StoredProcedureSqlServer(
            SqlCommand command
            , List<SqlParameter> parameters
            , IConnection connection
            , ITransaction transaction = null) : base(command, connection, transaction)
        {
            SetParameters(parameters);
        }

        public List<ISqlParameter> Parameters
        {
            get
            {
                if (Command.Parameters != null)
                {
                    List<ISqlParameter> retval = new List<ISqlParameter>();
                    foreach (SqlParameter parameter in Command.Parameters)
                        retval.Add(new SqlParameterSqlServer(parameter));
                    return retval;
                }
                else return null;
            }

        }

        public List<GenNameValue> Execute()
        {

            Command.ExecuteNonQuery();

            List<GenNameValue> retval = new List<GenNameValue>();

            foreach (SqlParameter p in Command.Parameters )
            {
                if (p.Direction == ParameterDirection.InputOutput || p.Direction == ParameterDirection.Output)
                    retval.Add(
                        new GenNameValue {
                            Name = p.ParameterName
                            , Value = Command.Parameters[p.ParameterName].Value
                        }
                        );
            }

            return retval;
        }

        private void SetParameters(List<ISqlParameter> parameters)
        {
            foreach (ISqlParameter parameter in parameters)
                Command.Parameters.Add(((SqlParameterSqlServer)parameter).Parameter);
        }

        private void SetParameters(List<SqlParameter> parameters)
        {
            foreach (SqlParameter parameter in parameters)
                Command.Parameters.Add(parameter);
        }

    }

}
